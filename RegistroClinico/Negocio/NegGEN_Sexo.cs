﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace RegistroClinico
{
    public class NegGEN_Sexo : GEN_Sexo
    {
        public NegGEN_Sexo()
        {
        }
        public NegGEN_Sexo(int iGEN_idSexo)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Sexo WHERE GEN_idSexo = @id");
            //sb.AppendLine("SELECT * FROM GEN_Sexo WHERE GEN_idSexo = " + iGEN_idSexo.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idSexo.ToString() }/*, new string[] { "", "" } */};

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                char c = '\0';
                DataRow dtr = dataTable.Rows[0];
                GEN_idSexo = int.Parse(dtr["GEN_idSexo"].ToString().Trim());
                GEN_codSexo = int.Parse(dtr["GEN_codSexo"].ToString().Trim());
                GEN_nomSexo = dtr["GEN_nomSexo"].ToString().Trim();
                if (char.TryParse(dtr["GEN_codSexoOrden"].ToString().Trim(), out c))
                    GEN_codSexoOrden = char.Parse(dtr["GEN_codSexoOrden"].ToString().Trim());
                GEN_estadoSexo = dtr["GEN_estadoSexo"].ToString().Trim();
            }
        }
        public void GetCombo(ref DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("-Seleccione-", "0"));
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder("SELECT GEN_idSexo, GEN_nomSexo FROM GEN_Sexo");
            DataTable dt = new DataTable();
            dt = con.GetDataTable(sb.ToString());

            foreach (DataRow dtr in dt.Rows)
                ddl.Items.Add(new ListItem(dtr["GEN_nomSexo"].ToString().Trim(), dtr["GEN_idSexo"].ToString().Trim()));
        }
    }
}