﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace RegistroClinico
{
    public class NegGEN_Tipo_Estados_Sistemas : GEN_Tipo_Estados_Sistemas
    {
        public void GetEstadoIPD(ref DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("-Seleccione-", "0"));
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder("SELECT GEN_nombreTipo_Estados_Sistemas, GEN_idTipo_Estados_Sistemas FROM GEN_Tipo_Estados_Sistemas WHERE GEN_idTipo_Estados_Sistemas IN(63,64,72)");
            DataTable dt = new DataTable();
            dt = con.GetDataTable(sb.ToString());

            foreach (DataRow dtr in dt.Rows)
                ddl.Items.Add(new ListItem(dtr["GEN_nombreTipo_Estados_Sistemas"].ToString().Trim(), dtr["GEN_idTipo_Estados_Sistemas"].ToString().Trim()));
        }
    }
}