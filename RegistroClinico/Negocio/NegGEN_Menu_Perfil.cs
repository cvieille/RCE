﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace RegistroClinico
{
    public class NegGEN_Menu_Perfil : GEN_Menu_Perfil
    {
        public void GetMenu(ref DataTable dt, int iGEN_idPerfil)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT GEN_Menu.GEN_idMenu, GEN_tituloMenu, GEN_contenidoMenu");
            sb.AppendLine("FROM GEN_Menu_Perfil");
            sb.AppendLine("INNER JOIN GEN_Menu ON GEN_Menu_Perfil.GEN_idMenu = GEN_Menu.GEN_idMenu");
            sb.AppendLine("WHERE GEN_idPerfil = @id AND");
            //sb.AppendLine("WHERE GEN_idPerfil = " + iGEN_idPerfil.ToString() + " AND");
            sb.AppendLine("GEN_estadoMenu_Perfil = 'ACTIVO' AND GEN_estadoMenu = 'ACTIVO' AND GEN_nivelMenu = 1");
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idPerfil.ToString() }/*, new string[] { "", "" } */};

            dt = con.GetDataTable(sb.ToString(), sArray);
        }

        public void GetMenuPerfil(ref DataTable dt, int iGEN_idMenu_padre, int GEN_nivelMenu, int iGEN_IdSistemas)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT");
            sb.AppendLine("GEN_Menu.GEN_idMenu, GEN_Menu.GEN_tituloMenu, GEN_Menu.GEN_contenidoMenu,");
            sb.AppendLine("GEN_Menu.GEN_seleccionableMenu, GEN_Menu.GEN_TargetMenu");
            sb.AppendLine("FROM GEN_Menu_Niveles");
            sb.AppendLine("INNER JOIN GEN_Menu ON GEN_Menu_Niveles.GEN_idMenu_hijo = GEN_Menu.GEN_idMenu");
            sb.AppendLine("WHERE");
            sb.AppendLine("(GEN_Menu_Niveles.GEN_idMenu_padre = @id1) AND");
            //sb.AppendLine("(GEN_Menu_Niveles.GEN_idMenu_padre = " + iGEN_idMenu_padre.ToString() + ") AND");
            sb.AppendLine("GEN_Menu_Niveles.GEN_idMenu_hijo IN (");
            sb.AppendLine("SELECT GEN_Menu.GEN_idMenu AS GEN_idMenu_hijo");
            sb.AppendLine("FROM GEN_Perfil");
            sb.AppendLine("RIGHT OUTER JOIN GEN_Menu_Perfil ON GEN_Perfil.GEN_idPerfil = GEN_Menu_Perfil.GEN_idPerfil");
            sb.AppendLine("LEFT OUTER JOIN GEN_Menu ON GEN_Menu_Perfil.GEN_idMenu = GEN_Menu.GEN_idMenu");
            sb.AppendLine("WHERE");
            sb.AppendLine("GEN_Menu_Perfil.GEN_estadoMenu_Perfil = 'ACTIVO' AND");
            sb.AppendLine("GEN_Menu.GEN_estadoMenu = 'ACTIVO' AND");
            sb.AppendLine("GEN_Perfil.GEN_idPerfil = @id2 AND");
            //sb.AppendLine("GEN_Perfil.GEN_idPerfil = " + GEN_idPerfil.ToString() + " AND");
            sb.AppendLine("GEN_Perfil.GEN_IdSistemas = @id3 AND");
            //sb.AppendLine("GEN_Perfil.GEN_IdSistemas = " + iGEN_IdSistemas.ToString() + " AND");
            sb.AppendLine("GEN_Menu.GEN_nivelMenu = @id4)");
            //sb.AppendLine("GEN_Menu.GEN_nivelMenu = " + GEN_nivelMenu.ToString() + ")");

            string[][] sArray = new string[][] {
                new string[] { "@id1", iGEN_idMenu_padre.ToString() },
                new string[] { "@id2", GEN_idPerfil.ToString() },
                new string[] { "@id3", iGEN_IdSistemas.ToString() },
                new string[] { "@id4", GEN_nivelMenu.ToString() } };

            dt = con.GetDataTable(sb.ToString(), sArray);
        }
    }
}