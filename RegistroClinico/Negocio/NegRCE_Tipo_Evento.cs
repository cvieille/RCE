﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;

namespace RegistroClinico
{
    public class NegRCE_Tipo_Evento : RCE_Tipo_Evento
    {
        public void GetComboTipo_Evento(ref DropDownList ddl)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/RCE_Tipo_Evento");
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Tipo_Evento> jsonList = JsonConvert.DeserializeObject<List<Tipo_Evento>>(sRes, settings);
            IEnumerable<Tipo_Evento> lActivo = jsonList.Where(x => x.RCE_estadoTipo_Evento.ToLower() == "activo");

            ddl.DataSource = lActivo;
            ddl.DataTextField = "RCE_descripcionTipo_Evento";
            ddl.DataValueField = "RCE_idTipo_Evento";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("-Seleccione-", "0"));
        }
        //public void GetCombo(ref DropDownList ddl)
        //{
        //    ddl.Items.Clear();
        //    ddl.Items.Add(new ListItem("-Seleccione-", "0"));
        //    ConexionSQL con = new ConexionSQL();

        //    StringBuilder sb = new StringBuilder("SELECT RCE_idTipo_Evento, RCE_descripcionTipo_Evento FROM RCE_Tipo_Evento");
        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable(sb.ToString());

        //    foreach (DataRow dtr in dt.Rows)
        //        ddl.Items.Add(new ListItem(dtr["RCE_descripcionTipo_Evento"].ToString().Trim(), dtr["RCE_idTipo_Evento"].ToString().Trim()));
        //}
    }
}