﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;

namespace RegistroClinico
{
    public class NegRCE_Tipo_IPD : RCE_Tipo_IPD
    {
        public void GetComboTipo_IPD(ref DropDownList ddl)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/RCE_Tipo_IPD");
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Tipo_IPD> jsonList = JsonConvert.DeserializeObject<List<Tipo_IPD>>(sRes, settings);
            IEnumerable<Tipo_IPD> lActivo = jsonList.Where(x => x.RCE_estadoTipo_IPD.ToLower() == "activo");

            ddl.DataSource = lActivo;
            ddl.DataTextField = "RCE_descripcionTipo_IPD";
            ddl.DataValueField = "RCE_idTipo_IPD";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("-Seleccione-", "0"));
        }

        public NegRCE_Tipo_IPD()
        {
        }
        public NegRCE_Tipo_IPD(int iRCE_idTipo_IPD)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM RCE_Tipo_IPD WHERE RCE_idTipo_IPD = @id");
            string[][] sArray = new string[][] { new string[] { "@id", iRCE_idTipo_IPD.ToString() } };

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                {
                    RCE_idTipo_IPD = int.Parse(dtr["RCE_idTipo_IPD"].ToString().Trim());
                    RCE_descripcionTipo_IPD = dtr["RCE_descripcionTipo_IPD"].ToString().Trim();
                    RCE_estadoTipo_IPD = dtr["RCE_estadoTipo_IPD"].ToString().Trim();
                }
            }
        }
        //public void GetCombo(ref DropDownList ddl)
        //{
        //    ddl.Items.Clear();
        //    ddl.Items.Add(new ListItem("-Seleccione-", "0"));
        //    ConexionSQL con = new ConexionSQL();

        //    StringBuilder sb = new StringBuilder("SELECT * FROM RCE_Tipo_IPD WHERE RCE_estadoTipo_IPD = 'Activo'");
        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable(sb.ToString());

        //    foreach (DataRow dtr in dt.Rows)
        //        ddl.Items.Add(new ListItem(dtr["RCE_descripcionTipo_IPD"].ToString().Trim(), dtr["RCE_idTipo_IPD"].ToString().Trim()));
        //}
    }
}