﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;

namespace RegistroClinico
{
    public class NegGEN_Prevision_Tramo : GEN_Prevision_Tramo
    {
        public void GetComboPrevision_Tramo(ref DropDownList ddl, int iIndex)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/GEN_Prevision_Tramo/GEN_idPrevision/" + iIndex.ToString());
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Prevision_Tramo> jsonList = JsonConvert.DeserializeObject<List<Prevision_Tramo>>(sRes, settings);
            IEnumerable<Prevision_Tramo> lActivo = jsonList.Where(x => x.GEN_estadoPrevision_Tramo.ToLower() == "activo");

            ddl.DataSource = lActivo;
            ddl.DataTextField = "GEN_descripcionPrevision_Tramo";
            ddl.DataValueField = "GEN_idPrevision_Tramo";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("-Seleccione-", "0"));
        }

        public NegGEN_Prevision_Tramo()
        {
        }
        public NegGEN_Prevision_Tramo(int iGEN_idPrevision_Tramo)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Prevision_Tramo WHERE GEN_idPrevision_Tramo = @id");
            //sb.AppendLine("SELECT * FROM GEN_Sexo WHERE GEN_idSexo = " + iGEN_idSexo.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idPrevision_Tramo.ToString() }/*, new string[] { "", "" } */};

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                GEN_idPrevision_Tramo = int.Parse(dtr["GEN_idPrevision_Tramo"].ToString().Trim());
                GEN_descripcionPrevision_Tramo = dtr["GEN_descripcionPrevision_Tramo"].ToString().Trim();
                GEN_idPrevision = int.Parse(dtr["GEN_idPrevision"].ToString().Trim());
                GEN_estadoPrevision_Tramo = dtr["GEN_estadoPrevision_Tramo"].ToString().Trim();
            }
        }
        //public void GetCombo(ref DropDownList ddl)
        //{
        //    ddl.Items.Clear();
        //    ddl.Items.Add(new ListItem("-Seleccione-", "0"));
        //    ConexionSQL con = new ConexionSQL();

        //    StringBuilder sb = new StringBuilder("SELECT GEN_idPrevision_Tramo, GEN_descripcionPrevision_Tramo FROM GEN_Prevision_Tramo");
        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable(sb.ToString());

        //    foreach (DataRow dtr in dt.Rows)
        //        ddl.Items.Add(new ListItem(dtr["GEN_descripcionPrevision_Tramo"].ToString().Trim(), dtr["GEN_idPrevision_Tramo"].ToString().Trim()));
        //}
    }
}