﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace RegistroClinico
{
    public class NegGEN_Especialidad : GEN_Especialidad
    {
        public void GetComboEspecialidad(ref DropDownList ddl)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/GEN_Especialidad");
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Especialidad> jsonList = JsonConvert.DeserializeObject<List<Especialidad>>(sRes, settings);
            IEnumerable<Especialidad> lActivo = jsonList.Where(x => x.GEN_estadoEspecialidad.ToLower() == "activo");

            ddl.DataSource = lActivo;
            ddl.DataTextField = "GEN_nombreEspecialidad";
            ddl.DataValueField = "GEN_idEspecialidad";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("-Seleccione-", "0"));
        }
        
        public class Especialidad
        {
            [JsonProperty("GEN_idEspecialidad")]
            public string GEN_idEspecialidad { get; set; }
            [JsonProperty("GEN_codigoEspecialidad")]
            public string GEN_codigoEspecialidad { get; set; }
            [JsonProperty("GEN_nombreEspecialidad")]
            public string GEN_nombreEspecialidad { get; set; }
            [JsonProperty("GEN_operaEspecialidad")]
            public string GEN_operaEspecialidad { get; set; }
            [JsonProperty("GEN_estadoEspecialidad")]
            public string GEN_estadoEspecialidad { get; set; }
        }


        public NegGEN_Especialidad()
        {
        }
        public NegGEN_Especialidad(int iGEN_idEspecialidad)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Especialidad WHERE GEN_idEspecialidad = @id");
            //sb.AppendLine("SELECT * FROM GEN_Especialidad WHERE GEN_idEspecialidad = " + iGEN_idEspecialidad.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idEspecialidad.ToString() }/*, new string[] { "", "" } */};

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                GEN_idEspecialidad = int.Parse(dtr["GEN_idEspecialidad"].ToString().Trim());
                GEN_nombreEspecialidad = dtr["GEN_nombreEspecialidad"].ToString().Trim();
                GEN_operaEspecialidad = dtr["GEN_operaEspecialidad"].ToString().Trim();
                GEN_estadoEspecialidad = dtr["GEN_estadoEspecialidad"].ToString().Trim();
            }
        }
        //public void GetCombo(ref DropDownList ddl)
        //{
        //    ddl.Items.Clear();
        //    ddl.Items.Add(new ListItem("-Seleccione-", "0"));
        //    ConexionSQL con = new ConexionSQL();

        //    StringBuilder sb = new StringBuilder("SELECT * FROM GEN_Especialidad WHERE GEN_estadoEspecialidad = 'ACTIVO' ORDER BY GEN_idEspecialidad");
        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable(sb.ToString());

        //    foreach (DataRow dtr in dt.Rows)
        //        ddl.Items.Add(new ListItem(dtr["GEN_nombreEspecialidad"].ToString().Trim(), dtr["GEN_idEspecialidad"].ToString().Trim()));
        //}
        public bool GetEspecialidadIdProfesional(int GEN_idProfesional) {

            try
            {
                ConexionSQL conexion = new ConexionSQL();
                StringBuilder sql = new StringBuilder();
                
                sql.AppendLine("SELECT *");
                sql.AppendLine("FROM");
                sql.AppendLine("GEN_Especialidad AS E");
                sql.AppendLine("INNER JOIN");
                sql.AppendLine("GEN_Pro_Especialidad AS PE");
                sql.AppendLine("ON E.GEN_idEspecialidad = PE.GEN_idEspecialidad");
                sql.AppendLine("INNER JOIN");
                sql.AppendLine("GEN_Profesional AS P");
                sql.AppendLine("ON P.GEN_idProfesional = PE.GEN_idProfesional");
                sql.AppendLine("WHERE");
                sql.AppendLine("P.GEN_idProfesional = @id");
                sql.AppendLine("AND P.GEN_estadoProfesional = 'Activo'");
                sql.AppendLine("AND PE.GEN_estadoPro_Especialidad = 'Activo'");
                sql.AppendLine("AND E.GEN_estadoEspecialidad = 'Activo'");

                string[][] sArray = new string[][] { new string[] { "@id", GEN_idProfesional.ToString() }/*, new string[] { "", "" } */};

                DataTable dt = conexion.GetDataTable(sql.ToString(), sArray);

                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    GEN_idEspecialidad = int.Parse(row["GEN_idEspecialidad"].ToString());
                    GEN_nombreEspecialidad = row["GEN_nombreEspecialidad"].ToString();
                    GEN_operaEspecialidad = row["GEN_operaEspecialidad"].ToString();
                    GEN_estadoEspecialidad = row["GEN_estadoEspecialidad"].ToString();
                    return true;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }

    }
}