﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;

namespace RegistroClinico
{
    public class NegGEN_Establecimiento : GEN_Establecimiento
    {
        public void GetComboEstablecimiento(ref DropDownList ddl, int iIndex)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/GEN_Establecimiento/GEN_idServicio_Salud/" + iIndex.ToString());
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Establecimiento> jsonList = JsonConvert.DeserializeObject<List<Establecimiento>>(sRes, settings);
            IEnumerable<Establecimiento> lActivo = jsonList.Where(x => x.GEN_estadoEstablecimiento.ToLower() == "activo");

            ddl.DataSource = lActivo;
            ddl.DataTextField = "GEN_nombreEstablecimiento";
            ddl.DataValueField = "GEN_idEstablecimiento";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("-Seleccione-", "0"));
        }

        public NegGEN_Establecimiento()
        {
        }
        public NegGEN_Establecimiento(int iGEN_idEstablecimiento)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Establecimiento WHERE GEN_idEstablecimiento = @id");
            //sb.AppendLine("SELECT * FROM GEN_Establecimiento WHERE GEN_idEstablecimiento = " + iGEN_idEstablecimiento.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idEstablecimiento.ToString() }/*, new string[] { "", "" } */};

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                {
                    int iRes = 0;
                    char cRes = '\0';
                    GEN_idEstablecimiento = int.Parse(dtr["GEN_idEstablecimiento"].ToString().Trim());
                    if (int.TryParse(dtr["GEN_codigo_deisEstablecimiento"].ToString().Trim(), out iRes))
                        GEN_codigo_deisEstablecimiento = int.Parse(dtr["GEN_codigo_deisEstablecimiento"].ToString().Trim());
                    GEN_rutEstablecimiento = dtr["GEN_rutEstablecimiento"].ToString();
                    if (char.TryParse(dtr["GEN_digitoEstablecimiento"].ToString().Trim(), out cRes))
                        GEN_digitoEstablecimiento = char.Parse(dtr["GEN_digitoEstablecimiento"].ToString());
                    GEN_nombreEstablecimiento = dtr["GEN_nombreEstablecimiento"].ToString();
                    GEN_direccionEstablecimiento = dtr["GEN_direccionEstablecimiento"].ToString();
                    if (int.TryParse(dtr["GEN_idCiudad"].ToString().Trim(), out iRes))
                        GEN_idCiudad = int.Parse(dtr["GEN_idCiudad"].ToString().Trim());
                    GEN_estadoEstablecimiento = dtr["GEN_estadoEstablecimiento"].ToString();
                    GEN_idServicio_Salud = int.Parse(dtr["GEN_idServicio_Salud"].ToString());
                    if (char.TryParse(dtr["GEN_prefijo_anexoEstablecimiento"].ToString().Trim(), out cRes))
                        GEN_prefijo_anexoEstablecimiento = char.Parse(dtr["GEN_prefijo_anexoEstablecimiento"].ToString());
                }
            }
        }

        //public void GetCombo(ref DropDownList ddl)
        //{
        //    ddl.Items.Clear();
        //    ddl.Items.Add(new ListItem("-Seleccione-", "0"));
        //    ConexionSQL con = new ConexionSQL();

        //    StringBuilder sb = new StringBuilder("SELECT GEN_idEstablecimiento, GEN_codigo_deisEstablecimiento, GEN_nombreEstablecimiento, GEN_idServicio_Salud FROM GEN_Establecimiento WHERE GEN_estadoEstablecimiento = 'ACTIVO' AND GEN_idServicio_Salud = @id ORDER BY GEN_idEstablecimiento");
        //    //StringBuilder sb = new StringBuilder("SELECT GEN_idEstablecimiento, GEN_codigo_deisEstablecimiento, GEN_nombreEstablecimiento, GEN_idServicio_Salud FROM GEN_Establecimiento WHERE GEN_estadoEstablecimiento = 'ACTIVO' AND GEN_idServicio_Salud = " + GEN_idServicio_Salud.ToString() + " ORDER BY GEN_idEstablecimiento");
        //    string[][] sArray = new string[][] { new string[] { "@id", GEN_idServicio_Salud.ToString() }/*, new string[] { "", "" } */};

        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable(sb.ToString(), sArray);

        //    foreach (DataRow dtr in dt.Rows)
        //        ddl.Items.Add(new ListItem(dtr["GEN_nombreEstablecimiento"].ToString().Trim(), dtr["GEN_idEstablecimiento"].ToString().Trim()));
        //}
    }
}