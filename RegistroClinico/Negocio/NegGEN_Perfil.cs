﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace RegistroClinico
{
    public class NegGEN_Perfil : GEN_Perfil
    {
        public NegGEN_Perfil()
        {
        }

        public NegGEN_Perfil(int iGEN_idPerfil)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Perfil WHERE GEN_idPerfil = @id");
            //sb.AppendLine("SELECT * FROM GEN_Perfil WHERE GEN_idPerfil = " + iGEN_idPerfil.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idPerfil.ToString() }/*, new string[] { "", "" } */};

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);

            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                {
                    GEN_idPerfil = int.Parse(dtr["GEN_idPerfil"].ToString().Trim());
                    GEN_codigoPerfil = int.Parse(dtr["GEN_codigoPerfil"].ToString().Trim());
                    GEN_descripcionPerfil = dtr["GEN_descripcionPerfil"].ToString().Trim();
                    GEN_idSistemas = int.Parse(dtr["GEN_idSistemas"].ToString().Trim());
                }
            }
        }

        public void NegGEN_PerfilCodigo(int iGEN_codigoPerfil, int iGEN_idSistemas)
        {
            ConexionSQL con = new ConexionSQL();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Perfil WHERE GEN_codigoPerfil= @id1");
            sb.AppendLine("AND GEN_idSistemas = @id2");

            string[][] sArray = new string[][] { new string[] { "@id1", iGEN_codigoPerfil.ToString() }, new string[] { "@id2", iGEN_idSistemas.ToString() } };

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                {
                    GEN_idPerfil = int.Parse(dtr["GEN_idPerfil"].ToString().Trim());
                    GEN_codigoPerfil = int.Parse(dtr["GEN_codigoPerfil"].ToString().Trim());
                    GEN_descripcionPerfil = dtr["GEN_descripcionPerfil"].ToString().Trim();
                    GEN_idSistemas = int.Parse(dtr["GEN_idSistemas"].ToString().Trim());
                }
            }
        }
    }
}