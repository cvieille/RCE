﻿using RegistroClinico.Datos;
using System.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static RegistroClinico.WebApi;
using System.Text;
using Newtonsoft.Json;

namespace RegistroClinico.Negocio
{
    public class NegGEN_Ubicacion : GEN_Ubicacion
    {
        public NegGEN_Ubicacion(int GEN_idUbicacion) {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/GEN_Ubicacion/" + GEN_idUbicacion.ToString());
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            Ubicacion jsonList = JsonConvert.DeserializeObject<Ubicacion>(sRes, settings);
            {

                Ubicacion ub = jsonList;

                GEN_idUbicacion = ub.GEN_idUbicacion;
                GEN_nombreUbicacion = ub.GEN_nombreUbicacion;
                GEN_idUbicacionPadre = ub.GEN_idUbicacionPadre;
                GEN_estadoUbicacion = ub.GEN_estadoUbicacion;

            }
        }
    }
}