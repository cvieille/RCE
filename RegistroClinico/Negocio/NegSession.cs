﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;

namespace RegistroClinico.Negocio
{
    public class NegSession
    {
        public readonly Page _page;

        public NegSession(Page page) 
        {
            _page = page;
        }

        public void ValidarPermisosSession() 
        {
            if (!_page.IsPostBack)
            {
                if (/*_page.Request.Cookies.Get("DATA-TOKEN")*/_page.Session["TOKEN"] == null) 
                { 
                    _page.Response.Redirect("~/Vista/Default.aspx", false);
                    _page.Response.End();
                }
                else
                {
                    ValidarToken401();
                    ValidarMenu();
                }
            }
        }

        private void ValidarMenu()
        {

            string url = _page.Request.Url.AbsoluteUri.Replace("%C3%B1", "ñ");
            bool esValido = false;
            string path = HttpContext.Current.Server.MapPath("~/AppSetting.json");

            Dictionary<string, object> json = Funciones.ObtenerJsonFile(path);
            JsonSerializerSettings setting = new JsonSerializerSettings();
            setting.Culture = new System.Globalization.CultureInfo("es-ES");
            List<Dictionary<string, object>> list = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RutasAcceso"].ToString(), setting);

            string codPerfil = Convert.ToString(_page.Session["CODIGO_PERFIL"]);/*_page.Request.Cookies["DATA-CODIGO_PERFIL"].Value*/

            if (codPerfil != "")
            {
                foreach (Dictionary<string, object> j in list)
                {
                    if (url.Contains(j["Ruta"].ToString()))
                    {
                        List<int> listPerfilAcceso = JsonConvert.DeserializeObject<List<int>>(j["PerfilAcceso"].ToString());
                        foreach (int cod in listPerfilAcceso)
                        {
                        
                            if (int.Parse(codPerfil) == cod)
                            {
                                esValido = true;
                                break;
                            }
                        }
                        if (esValido) break;
                    }
                }
            }


            if (!esValido)
            {
                _page.Response.Redirect("~/Vista/Default.aspx", false);
                _page.Response.End();
            }
        }

        private void ValidarToken401()
        {

            //GEN_Usuarios/LOGEADO
            if (_page.Session["TOKEN"] /*_page.Request.Cookies.Get("DATA-TOKEN")*/ != null)
            {
                HttpContext context = HttpContext.Current;
                string dominio = Funciones.GetUrlWebApi();
                string url = string.Format("{0}GEN_Usuarios/LOGEADO", dominio);
                string token = string.Format("Bearer {0}", _page.Session["TOKEN"] /*_page.Request.Cookies["DATA-TOKEN"].Value*/);

                try
                {
                    using (System.Net.WebClient WebClient = new System.Net.WebClient())
                    {
                        WebClient.Headers.Add("Authorization", token);
                        WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                        string result = WebClient.DownloadString(url);
                    }
                }
                catch (System.Net.WebException ex)
                {
                    System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)ex.Response;
                    // VALIDA SI NO HA TERMINADO EL TIEMPO DEL TOKEN
                    if (response != null && response.StatusCode == System.Net.HttpStatusCode.Unauthorized) 
                    {
                        _page.Response.Redirect("~/Vista/Default.aspx", false);
                        _page.Response.End();
                    }
                    else if(response == null)
                    {
                        _page.Response.Redirect("~/Vista/Default.aspx", false);
                        _page.Response.End();
                    }
                }
            }

        }

    }
}