﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace RegistroClinico
{
    public class NegRCE_Movimientos_Eventos : RCE_Movimientos_Eventos
    {
        public int Insert()
        {
            ConexionSQL conexionSQL = new ConexionSQL();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO RCE_Movimientos_Eventos(RCE_idEventos, GEN_idUsuarios, RCE_fechaMovimientos_Eventos, GEN_idTipo_Movimientos_Sistemas)");
            sb.AppendLine("VALUES(");
            sb.AppendLine("@id1,");
            sb.AppendLine("@id2,");
            sb.AppendLine("@id3,");
            sb.AppendLine("@id4)");
            //sb.AppendLine(RCE_idEventos.ToString() + ",");
            //sb.AppendLine(GEN_idUsuarios.ToString() + ",");
            //sb.AppendLine("'" + RCE_fechaMovimientos_Eventos.ToString() + "',");
            //sb.AppendLine(GEN_idTipo_Movimientos_Sistemas.ToString() + ")");
            string[][] sArray = new string[][] {
                new string[] { "@id1", RCE_idEventos.ToString() },
                new string[] { "@id2", GEN_idUsuarios.ToString() },
                new string[] { "@id3", RCE_fechaMovimientos_Eventos.ToString() },
                new string[] { "@id4", GEN_idTipo_Movimientos_Sistemas.ToString() }
            };

            return conexionSQL.InsertQuery(sb.ToString(), false, sArray);
        }
    }
}