﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace RegistroClinico
{
    public class NegGEN_Ciudad : GEN_Ciudad
    {
        public NegGEN_Ciudad()
        {
        }
        public NegGEN_Ciudad(int iGEN_idCiudad)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Ciudad WHERE GEN_idCiudad = @id");
            //sb.AppendLine("SELECT * FROM GEN_Ciudad WHERE GEN_idCiudad = " + iGEN_idCiudad.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idCiudad.ToString() }/*, new string[] { "", "" } */};

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                GEN_idCiudad = int.Parse(dtr["GEN_idCiudad"].ToString().Trim());
                GEN_nombreCiudad = dtr["GEN_nombreCiudad"].ToString().Trim();
                GEN_codigoCiudad = int.Parse(dtr["GEN_codigoCiudad"].ToString().Trim());
                GEN_idComuna = int.Parse(dtr["GEN_idComuna"].ToString().Trim());
                GEN_estadoCiudad = dtr["GEN_nombreCiudad"].ToString().Trim();
            }
        }
        public void GetCombo(ref DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("-Seleccione-", "0"));
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder("SELECT GEN_idCiudad, GEN_nombreCiudad FROM GEN_Ciudad");
            DataTable dt = new DataTable();
            dt = con.GetDataTable(sb.ToString());

            foreach (DataRow dtr in dt.Rows)
                ddl.Items.Add(new ListItem(dtr["GEN_nombreCiudad"].ToString().Trim(), dtr["GEN_idCiudad"].ToString().Trim()));
        }
    }
}