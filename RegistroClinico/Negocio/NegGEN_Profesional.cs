﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace RegistroClinico
{
    public class NegGEN_Profesional : GEN_Profesional
    {
        public NegGEN_Profesional()
        {
        }

        public NegGEN_Profesional(int iGEN_idProfesional)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Profesional WHERE GEN_idProfesional = @id");
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idProfesional.ToString() }/*, new string[] { "", "" } */};

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                {
                    char c;
                    GEN_idProfesional = int.Parse(dtr["GEN_idProfesional"].ToString());
                    GEN_rutProfesional = dtr["GEN_rutProfesional"].ToString().Trim();
                    if (char.TryParse(dtr["GEN_digitoProfesional"].ToString().Trim(), out c))
                        GEN_digitoProfesional = char.Parse(dtr["GEN_digitoProfesional"].ToString().Trim());
                    GEN_nombreProfesional = dtr["GEN_nombreProfesional"].ToString().Trim();
                    GEN_apellidoProfesional = dtr["GEN_apellidoProfesional"].ToString().Trim();
                    GEN_sapellidoProfesional = dtr["GEN_sapellidoProfesional"].ToString().Trim();
                    GEN_idSexo = int.Parse(dtr["GEN_idSexo"].ToString());
                    GEN_dependenciaProfesional = dtr["GEN_dependenciaProfesional"].ToString().Trim();
                    GEN_estadoProfesional = dtr["GEN_estadoProfesional"].ToString().Trim();
                    GEN_emailProfesional = dtr["GEN_emailProfesional"].ToString().Trim();
                }
            }
        }

        public DataTable GetProfesionalRut(string sRut)
        {
            ConexionSQL con = new ConexionSQL();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Profesional WHERE GEN_rutProfesional = @id");
            string[][] sArray = new string[][] { new string[] { "@id", sRut.ToString() }};
            
            return dt = con.GetDataTable(sb.ToString(), sArray);
        }

        public bool GetProfesionalIdUsuario(int iGEN_IdUsuario)
        {
            ConexionSQL con = new ConexionSQL();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT GEN_Profesional.*");
            sb.AppendLine("FROM GEN_Profesional");
            sb.AppendLine("INNER JOIN GEN_Usuarios ON GEN_Usuarios.GEN_rutUsuarios = GEN_Profesional.GEN_rutProfesional AND GEN_Usuarios.GEN_digitoUsuarios = GEN_Profesional.GEN_digitoProfesional");
            sb.AppendLine("WHERE");
            sb.AppendLine("GEN_Usuarios.GEN_idUsuarios = @id");
            //sb.AppendLine("GEN_Usuarios.GEN_idUsuarios = " + iGEN_IdUsuario.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_IdUsuario.ToString() }/*, new string[] { "", "" } */};

            dt = con.GetDataTable(sb.ToString(), sArray);

            if (dt.Rows.Count > 0)
            {
                DataRow dtr = dt.Rows[0];
                {
                    char c;
                    this.GEN_idProfesional = int.Parse(dtr["GEN_idProfesional"].ToString().Trim());
                    GEN_rutProfesional = dtr["GEN_rutProfesional"].ToString().Trim();
                    if (char.TryParse(dtr["GEN_digitoProfesional"].ToString().Trim(), out c))
                        GEN_digitoProfesional = char.Parse(dtr["GEN_digitoProfesional"].ToString().Trim());
                    GEN_nombreProfesional = dtr["GEN_nombreProfesional"].ToString().Trim();
                    GEN_apellidoProfesional = dtr["GEN_apellidoProfesional"].ToString().Trim();
                    GEN_sapellidoProfesional = dtr["GEN_sapellidoProfesional"].ToString().Trim();
                    GEN_idSexo = int.Parse(dtr["GEN_idSexo"].ToString().Trim());
                    GEN_dependenciaProfesional = dtr["GEN_dependenciaProfesional"].ToString().Trim();
                    GEN_estadoProfesional = dtr["GEN_estadoProfesional"].ToString().Trim();
                    GEN_emailProfesional = dtr["GEN_emailProfesional"].ToString().Trim();
                }

                return true;
            }
            return false;
        }


        public void GetProfesionalIPD(ref DropDownList ddl)
        {
            ConexionSQL con = new ConexionSQL();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT GEN_Profesional.GEN_idProfesional, ");
            sb.AppendLine("GEN_Profesional.GEN_nombreProfesional + ' ' + ISNULL(GEN_Profesional.GEN_apellidoProfesional,'') + ' ' + ISNULL(GEN_Profesional.GEN_sapellidoProfesional,'') GEN_nombreProfesional,");
            sb.AppendLine("GEN_Profesional.GEN_rutProfesional + '-' + GEN_Profesional.GEN_digitoProfesional GEN_rutProfesional");
            sb.AppendLine("FROM GEN_Profesional");
            sb.AppendLine("INNER JOIN GEN_Pro_Profesion ON GEN_Pro_Profesion.GEN_idProfesional = GEN_Profesional.GEN_idProfesional");
            sb.AppendLine("INNER JOIN GEN_Profesion ON GEN_Pro_Profesion.GEN_idProfesion = GEN_Profesion.GEN_idProfesion");
            sb.AppendLine("INNER JOIN RCE_IPD ON RCE_IPD.GEN_idProfesional = GEN_Profesional.GEN_idProfesional");
            sb.AppendLine("WHERE ");
            sb.AppendLine("GEN_Pro_Profesion.GEN_estadoPro_Profesion = 'ACTIVO' AND GEN_Profesion.GEN_estadoProfesion = 'ACTIVO'");
            sb.AppendLine("AND GEN_Profesional.GEN_estadoProfesional = 'ACTIVO' ");
            sb.AppendLine("AND GEN_Profesion.GEN_idProfesion = 1");
            sb.AppendLine("GROUP BY GEN_Profesional.GEN_idProfesional, ");
            sb.AppendLine("GEN_Profesional.GEN_nombreProfesional + ' ' + ISNULL(GEN_Profesional.GEN_apellidoProfesional,'') + ' ' + ISNULL(GEN_Profesional.GEN_sapellidoProfesional,''),");
            sb.AppendLine("GEN_Profesional.GEN_rutProfesional + '-' + GEN_Profesional.GEN_digitoProfesional");
            dt = con.GetDataTable(sb.ToString());

            ddl.Items.Clear();
            //ddl.Items.Add(new ListItem("-Seleccione-", "0"));
            foreach (DataRow dtr in dt.Rows)
                ddl.Items.Add(new ListItem(dtr["GEN_nombreProfesional"].ToString().Trim(), dtr["GEN_idProfesional"].ToString().Trim()));
        }
    }
}