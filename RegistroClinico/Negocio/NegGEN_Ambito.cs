﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace RegistroClinico
{
    public class NegGEN_Ambito : GEN_Ambito
    {
        public void GetComboAmbito(ref DropDownList ddl)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/GEN_Ambito");
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Ambito> jsonList = JsonConvert.DeserializeObject<List<Ambito>>(sRes, settings);
            IEnumerable<Ambito> lActivo = jsonList.Where(x => x.GEN_estadoAmbito.ToLower() == "activo");

            ddl.DataSource = lActivo;
            ddl.DataTextField = "GEN_nombreAmbito";
            ddl.DataValueField = "GEN_idAmbito";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("-Seleccione-", "0"));
        }

        public class Ambito
        {
            [JsonProperty("GEN_idAmbito")]
            public string GEN_idAmbito { get; set; }
            [JsonProperty("GEN_nombreAmbito")]
            public string GEN_nombreAmbito { get; set; }
            [JsonProperty("GEN_estadoAmbito")]
            public string GEN_estadoAmbito { get; set; }
        }


        public NegGEN_Ambito()
        {
        }
        public NegGEN_Ambito(int iGEN_idAmbito)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Ambito WHERE GEN_idAmbito = @id");
            //sb.AppendLine("SELECT * FROM GEN_Ambito WHERE GEN_idAmbito = " + iGEN_idAmbito.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idAmbito.ToString() }/*, new string[] { "", "" } */};
            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                GEN_idAmbito = int.Parse(dtr["GEN_idAmbito"].ToString().Trim());
                GEN_nombreAmbito = dtr["GEN_nombreAmbito"].ToString().Trim();
                GEN_estadoAmbito = dtr["GEN_estadoAmbito"].ToString().Trim();
            }
        }
        //public void GetCombo(ref DropDownList ddl)
        //{
        //    ddl.Items.Clear();
        //    ddl.Items.Add(new ListItem("-Seleccione-", "0"));
        //    ConexionSQL con = new ConexionSQL();

        //    StringBuilder sb = new StringBuilder("SELECT * FROM GEN_Ambito WHERE GEN_estadoAmbito = 'ACTIVO' ORDER BY GEN_idAmbito");

        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable(sb.ToString());

        //    foreach (DataRow dtr in dt.Rows)
        //        ddl.Items.Add(new ListItem(dtr["GEN_nombreAmbito"].ToString().Trim(), dtr["GEN_idAmbito"].ToString().Trim()));
        //}
    }
}