﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace RegistroClinico.Negocio
{
    public class ConexionMYSQL
    {
        string connectionString;
        public ConexionMYSQL()
        {
            //connectionString = ConfigurationManager.ConnectionStrings["ConnectionStringSqlServer"].ConnectionString;
            //connectionString = ConfigurationManager.ConnectionStrings["localCASA"].ConnectionString;
            //connectionString = ConfigurationManager.ConnectionStrings["Connection175"].ConnectionString;
            connectionString = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;
        }


        public void insertQuery(string query)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteNonQuery();

            connection.Close();
        }

        public DataTable getDataTable(string query)
        {
            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();

                MySqlCommand cmd = new MySqlCommand(query, connection);

                MySqlDataReader dataReader = cmd.ExecuteReader();

                DataTable dataTable = new DataTable();
                dataTable.Load(dataReader);

                dataReader.Close();
                connection.Close();

                return dataTable;
            }
            catch (Exception)
            {
                return new DataTable();
                throw;
            }
        }
    }
}