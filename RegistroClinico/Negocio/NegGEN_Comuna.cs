﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace RegistroClinico
{
    public class NegGEN_Comuna : GEN_Comuna
    {
        public NegGEN_Comuna()
        {
        }
        public NegGEN_Comuna(int iGEN_idComuna)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Comuna WHERE GEN_idComuna = @id");
            //sb.AppendLine("SELECT * FROM GEN_Comuna WHERE GEN_idComuna = " + iGEN_idComuna.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idComuna.ToString() }/*, new string[] { "", "" } */};

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];

                GEN_idComuna = int.Parse(dtr["GEN_idComuna"].ToString().Trim());
                GEN_nombreComuna = dtr["GEN_nombreComuna"].ToString().Trim();
                GEN_codigoComuna = int.Parse(dtr["GEN_codigoComuna"].ToString().Trim());
                GEN_idRegion = int.Parse(dtr["GEN_idRegion"].ToString().Trim());
                GEN_estadoComuna = dtr["GEN_estadoComuna"].ToString().Trim();
            }
        }
        public void GetCombo(ref DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("-Seleccione-", "0"));
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder("SELECT GEN_idComuna, GEN_nombreComuna FROM GEN_Comuna");
            DataTable dt = new DataTable();
            dt = con.GetDataTable(sb.ToString());

            foreach (DataRow dtr in dt.Rows)
                ddl.Items.Add(new ListItem(dtr["GEN_nombreComuna"].ToString().Trim(), dtr["GEN_idComuna"].ToString().Trim()));
        }
    }
}