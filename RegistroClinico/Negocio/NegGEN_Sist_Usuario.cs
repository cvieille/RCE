﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace RegistroClinico
{
    public class NegGEN_Sist_Usuario : GEN_Sist_Usuario
    {
        /// <summary>
        /// Metodo devuelve un count de los perfiles del usuario
        /// </summary>
        /// <param name="iGEN_idUsuarios"></param>
        /// <param name="iGEN_idSistemas"></param>
        /// <returns></returns>

        public int GetCantidadPerfiles(int iGEN_idUsuarios, int iGEN_idSistemas)
        {
            ConexionSQL con = new ConexionSQL();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT COUNT(*) ");
            sb.AppendLine("FROM GEN_Sist_Usuario ");
            sb.AppendLine("WHERE GEN_idUsuarios = @id1");
            sb.AppendLine("AND GEN_idSistemas = @id2");
            string[][] sArray = new string[][] { new string[] { "@id1", iGEN_idUsuarios.ToString() }, new string[] { "@id2", iGEN_idSistemas.ToString() } };

            int iCount = con.GetCount(sb.ToString(), sArray);
            return iCount;
        }
        /// <summary>
        /// Metodo que retorna un datatable con perfiles
        /// </summary>
        /// <param name="iGEN_idUsuario"></param>
        /// <param name="iGEN_idSistemas"></param>
        /// <returns></returns>
        public DataTable GetDatatablePerfiles(int iGEN_idUsuario, int iGEN_idSistemas)
        {
            DataTable dt = new DataTable();
            ConexionSQL con = new ConexionSQL();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT P.GEN_codigoPerfil, P.GEN_descripcionPerfil FROM GEN_Sist_Usuario AS SU ");
            sb.AppendLine("INNER JOIN GEN_Perfil AS P ON P.GEN_idPerfil = SU.GEN_idPerfil");
            sb.AppendLine("WHERE SU.GEN_idUsuarios= @id1 AND SU.GEN_idSistemas= @id2 AND SU.GEN_estadoSist_Usuario='Activo'");
            string[][] sArray = new string[][] { new string[] { "@id1", iGEN_idUsuario.ToString() }, new string[] { "@id2", iGEN_idSistemas.ToString() } };

            dt = con.GetDataTable(sb.ToString(), sArray);
            return dt;
        }
        /// <summary>
        /// Metodo que llena el combo de perfiles
        /// </summary>
        /// <param name="iGEN_idUsuario"></param>
        /// <param name="iGEN_idSistemas"></param>
        /// <param name="ddl"></param>
        public void GetComboPerfiles(int iGEN_idUsuario, int iGEN_idSistemas, ref DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("-Seleccione-", "0"));
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT P.GEN_codigoPerfil, P.GEN_descripcionPerfil FROM GEN_Sist_Usuario AS SU ");
            sb.AppendLine("INNER JOIN GEN_Perfil AS P ON P.GEN_idPerfil = SU.GEN_idPerfil");
            sb.AppendLine("WHERE SU.GEN_idUsuarios= @id1 AND SU.GEN_idSistemas= @id2 AND SU.GEN_estadoSist_Usuario='Activo'");
            string[][] sArray = new string[][] { new string[] { "@id1", iGEN_idUsuario.ToString() }, new string[] { "@id2", iGEN_idSistemas.ToString() } };

            DataTable dt = new DataTable();
            dt = con.GetDataTable(sb.ToString(), sArray);

            foreach (DataRow dtr in dt.Rows)
                ddl.Items.Add(new ListItem(dtr["GEN_descripcionPerfil"].ToString().Trim(), dtr["GEN_codigoPerfil"].ToString().Trim()));
        }
        public bool GetUsuarioIdUsuario(int iGEN_idUsuarios, int iGEN_idSistemas)
        {
            ConexionSQL con = new ConexionSQL();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Sist_Usuario WHERE GEN_idUsuarios = @id1 AND GEN_idSistemas = @id2");
            //sb.AppendLine("SELECT * FROM GEN_Sist_Usuario WHERE GEN_idUsuarios = " + iGEN_idUsuarios.ToString() + " AND GEN_idSistemas = " + iGEN_idSistemas.ToString());
            sb.AppendLine("AND GEN_estadoSist_Usuario = 'Activo'");
            string[][] sArray = new string[][] { new string[] { "@id1", iGEN_idUsuarios.ToString() }, new string[] { "@id2", iGEN_idSistemas.ToString() } };

            DataTable dt = con.GetDataTable(sb.ToString(), sArray);
            if (dt.Rows.Count > 0)
            {
                DateTime dRes;
                DataRow dtr = dt.Rows[0];
                GEN_idSist_Usuario = int.Parse(dtr["GEN_idSist_Usuario"].ToString());
                GEN_idUsuarios = int.Parse(dtr["GEN_idUsuarios"].ToString());
                GEN_idSistemas = int.Parse(dtr["GEN_idSistemas"].ToString());
                GEN_idPerfil = int.Parse(dtr["GEN_idPerfil"].ToString());
                GEN_estadoSist_Usuario = dtr["GEN_estadoSist_Usuario"].ToString();
                if (DateTime.TryParse(dtr["GEN_ultimoAccesoSist_Usuario"].ToString(), out dRes))
                    GEN_ultimoAccesoSist_Usuario = DateTime.Parse(dtr["GEN_ultimoAccesoSist_Usuario"].ToString());
                return true;
            }
            return false;
        }

        public void Update()
        {
            ConexionSQL con = new ConexionSQL();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE GEN_Sist_Usuario SET");
            //sb.AppendLine("GEN_idUsuarios = " + GEN_idUsuarios.ToString() + ",");
            //sb.AppendLine("GEN_idSistemas = " + GEN_idSistemas.ToString() + ",");
            //sb.AppendLine("GEN_idPerfil = " + GEN_idPerfil.ToString() + ",");
            //sb.AppendLine("GEN_estadoSist_Usuario = '" + GEN_estadoSist_Usuario + "',");
            //sb.AppendLine("GEN_ultimoAccesoSist_Usuario = '" + GEN_ultimoAccesoSist_Usuario.ToString() + "'");
            //sb.AppendLine("WHERE GEN_idSist_Usuario = " + GEN_idSist_Usuario.ToString());
            sb.AppendLine("GEN_idUsuarios = @id1,");
            sb.AppendLine("GEN_idSistemas = @id2,");
            sb.AppendLine("GEN_idPerfil = @id3,");
            sb.AppendLine("GEN_estadoSist_Usuario = @id4,");
            sb.AppendLine("GEN_ultimoAccesoSist_Usuario = @id5");
            sb.AppendLine("WHERE GEN_idSist_Usuario = @id6");
            string[][] sArray = new string[][] {
                new string[] { "@id1", GEN_idUsuarios.ToString() },
                new string[] { "@id2", GEN_idSistemas.ToString() },
                new string[] { "@id3", GEN_idPerfil.ToString() },
                new string[] { "@id4", GEN_estadoSist_Usuario },
                new string[] { "@id5", GEN_ultimoAccesoSist_Usuario.ToString() },
                new string[] { "@id6", GEN_idSist_Usuario.ToString() }};

            con.UpdateQuery(sb.ToString(), sArray);
        }

    }
}