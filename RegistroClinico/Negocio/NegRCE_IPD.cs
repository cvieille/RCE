﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace RegistroClinico
{
    public class NegRCE_IPD : RCE_IPD
    {
        public int Insert()
        {
            ConexionSQL conexionSQL = new ConexionSQL();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO RCE_IPD(RCE_fechaIPD, GEN_idEstablecimiento, GEN_idEspecialidad, RCE_diagnosticoIPD,");
            sb.AppendLine("RCE_fundamentoIPD, RCE_tratamientoIPD, RCE_inicio_tratamientoIPD, RCE_idEventos, GEN_idAmbito, RCE_idGrupo_Patologia_GES,");
            sb.AppendLine("RCE_idSubgrupo_Patologia_GES, GEN_idTipo_Estados_Sistemas, RCE_estadoIPD, RCE_idTipo_IPD, GEN_idProfesional)");
            sb.AppendLine("OUTPUT INSERTED.RCE_idIPD");
            sb.AppendLine("VALUES(");
            sb.AppendLine("@id1,@id2,@id3,");
            sb.AppendLine("@id4,@id5,@id6,");
            sb.AppendLine("@id7,");
            sb.AppendLine("@id8,");
            sb.AppendLine("@id9,");
            sb.AppendLine("@id10,");
            sb.AppendLine("@id11,");
            sb.AppendLine("@id12,@id13,@id14,");
            sb.AppendLine("@id15)");
            //sb.AppendLine("'" + RCE_fechaIPD.ToString() + "'," + GEN_idEstablecimiento.ToString() + "," + GEN_idEspecialidad.ToString() + ",");
            //sb.AppendLine("'" + RCE_diagnosticoIPD + "','" + RCE_fundamentoIPD + "','" + RCE_tratamientoIPD + "',");
            //sb.AppendLine("'" + RCE_inicio_tratamientoIPD.ToString() + "',");
            //sb.AppendLine((RCE_idEventos == 0 ? "NULL," : RCE_idEventos.ToString() + ","));
            //sb.AppendLine("" + GEN_idAmbito.ToString() + ",");
            //sb.AppendLine((RCE_idGrupo_Patologia_GES == 0 ? "NULL," : RCE_idGrupo_Patologia_GES.ToString() + ","));
            //sb.AppendLine((RCE_idSubgrupo_Patologia_GES == 0 ? "NULL," : RCE_idSubgrupo_Patologia_GES.ToString() + ","));
            //sb.AppendLine("" + GEN_idTipo_Estados_Sistemas.ToString() + ",'" + RCE_estadoIPD + "', " + RCE_idTipo_IPD.ToString() + ")");
            string[][] sArray = new string[][] {
                new string[] { "@id1", RCE_fechaIPD.ToString() },
                new string[] { "@id2", GEN_idEstablecimiento.ToString() },
                new string[] { "@id3", GEN_idEspecialidad.ToString() },
                new string[] { "@id4", RCE_diagnosticoIPD },
                new string[] { "@id5", RCE_fundamentoIPD },
                new string[] { "@id6", RCE_tratamientoIPD },
                new string[] { "@id7", RCE_inicio_tratamientoIPD.ToString() },
                new string[] { "@id8", (RCE_idEventos == 0 ? "NULL" : RCE_idEventos.ToString()) },
                new string[] { "@id9", GEN_idAmbito.ToString() },
                new string[] { "@id10", (RCE_idGrupo_Patologia_GES == 0 ? "NULL" : RCE_idGrupo_Patologia_GES.ToString()) },
                new string[] { "@id11", (RCE_idSubgrupo_Patologia_GES == 0 ? "NULL" : RCE_idSubgrupo_Patologia_GES.ToString()) },
                new string[] { "@id12", GEN_idTipo_Estados_Sistemas.ToString() },
                new string[] { "@id13", RCE_estadoIPD },
                new string[] { "@id14", RCE_idTipo_IPD.ToString() },
                new string[] { "@id15", GEN_idProfesional.ToString() }
            };

            return conexionSQL.InsertQuery(sb.ToString(), true, sArray);
        }

        public void Update()
        {
            ConexionSQL con = new ConexionSQL();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE RCE_IPD SET");
            sb.AppendLine("RCE_fechaIPD = @id1,");
            sb.AppendLine("GEN_idEstablecimiento = @id2,");
            sb.AppendLine("GEN_idEspecialidad = @id3,");
            sb.AppendLine("RCE_diagnosticoIPD = @id4,");
            sb.AppendLine("RCE_fundamentoIPD = @id5,");
            sb.AppendLine("RCE_tratamientoIPD = @id6,");
            sb.AppendLine("RCE_inicio_tratamientoIPD = @id7,");
            sb.AppendLine("RCE_idEventos = @id8,");
            sb.AppendLine("GEN_idAmbito = @id9,");
            sb.AppendLine("RCE_idGrupo_Patologia_GES = @id10,");
            sb.AppendLine("RCE_idSubgrupo_Patologia_GES = @id11,");
            sb.AppendLine("GEN_idTipo_Estados_Sistemas = @id12,");
            sb.AppendLine("RCE_estadoIPD = @id13,");
            sb.AppendLine("RCE_idTipo_IPD = @id14,");
            sb.AppendLine("GEN_idProfesional = @id15");
            sb.AppendLine("WHERE RCE_idIPD = @id16");
            //sb.AppendLine("RCE_fechaIPD = '" + RCE_fechaIPD.ToString() + "',");
            //sb.AppendLine("GEN_idEstablecimiento = " + GEN_idEstablecimiento.ToString() + ",");
            //sb.AppendLine("GEN_idEspecialidad = " + GEN_idEspecialidad.ToString() + ",");
            //sb.AppendLine("RCE_diagnosticoIPD = '" + RCE_diagnosticoIPD + "',");
            //sb.AppendLine("RCE_fundamentoIPD = '" + RCE_fundamentoIPD + "',");
            //sb.AppendLine("RCE_tratamientoIPD = '" + RCE_tratamientoIPD + "',");
            //sb.AppendLine("RCE_inicio_tratamientoIPD = '" + RCE_inicio_tratamientoIPD.ToString() + "',");
            //sb.AppendLine("RCE_idEventos = " + (RCE_idEventos == 0 ? "null" : RCE_idEventos.ToString()) + ",");
            //sb.AppendLine("GEN_idAmbito = " + GEN_idAmbito.ToString() + ",");
            //sb.AppendLine("RCE_idGrupo_Patologia_GES = " + (RCE_idGrupo_Patologia_GES == 0 ? "null" : RCE_idGrupo_Patologia_GES.ToString()) + ",");
            //sb.AppendLine("RCE_idSubgrupo_Patologia_GES = " + (RCE_idSubgrupo_Patologia_GES == 0 ? "null" : RCE_idSubgrupo_Patologia_GES.ToString()) + ",");
            //sb.AppendLine("GEN_idTipo_Estados_Sistemas = " + GEN_idTipo_Estados_Sistemas.ToString() + ",");
            //sb.AppendLine("RCE_estadoIPD = '" + RCE_estadoIPD + "',");
            //sb.AppendLine("RCE_idTipo_IPD = " + RCE_idTipo_IPD.ToString() + "");
            //sb.AppendLine("WHERE RCE_idIPD = " + RCE_idIPD.ToString());
            string[][] sArray = new string[][] {
                new string[] { "@id1", RCE_fechaIPD.ToString() },
                new string[] { "@id2", GEN_idEstablecimiento.ToString() },
                new string[] { "@id3", GEN_idEspecialidad.ToString() },
                new string[] { "@id4", RCE_diagnosticoIPD },
                new string[] { "@id5", RCE_fundamentoIPD },
                new string[] { "@id6", RCE_tratamientoIPD },
                new string[] { "@id7", RCE_inicio_tratamientoIPD.ToString() },
                new string[] { "@id8", (RCE_idEventos == 0 ? "NULL" : RCE_idEventos.ToString()) },
                new string[] { "@id9", GEN_idAmbito.ToString() },
                new string[] { "@id10", (RCE_idGrupo_Patologia_GES == 0 ? "NULL" : RCE_idGrupo_Patologia_GES.ToString()) },
                new string[] { "@id11", (RCE_idSubgrupo_Patologia_GES == 0 ? "NULL" : RCE_idSubgrupo_Patologia_GES.ToString()) },
                new string[] { "@id12", GEN_idTipo_Estados_Sistemas.ToString() },
                new string[] { "@id13", RCE_estadoIPD },
                new string[] { "@id14", RCE_idTipo_IPD.ToString() },
                new string[] { "@id15", GEN_idProfesional.ToString() },
                new string[] { "@id16", RCE_idIPD.ToString() }
            };
            con.UpdateQuery(sb.ToString(), sArray);
        }

        public DataTable GetIPDProfesional(int iGEN_idProfesional, string sRut, string sNombre, string sApe, string sSApe, int iEstadoSistema, int iGrupo, int iSubGrupo)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT RCE_IPD.RCE_idIPD, RCE_IPD.RCE_fechaIPD,");
            sb.AppendLine("PAC.GEN_nombrePaciente + ' ' + ISNULL(PAC.GEN_ape_paternoPaciente,'') + ' ' + ISNULL(PAC.GEN_ape_maternoPaciente,'') PACIENTE,");
            sb.AppendLine("REP.GEN_nombrePaciente + ' ' + ISNULL(REP.GEN_ape_paternoPaciente,'') + ' ' + ISNULL(REP.GEN_ape_maternoPaciente,'') REPRESENTANTE,");
            sb.AppendLine("RCE_IPD.RCE_idTipo_IPD,");
            sb.AppendLine("RCE_Tipo_IPD.RCE_descripcionTipo_IPD,");
            sb.AppendLine("RCE_IPD.GEN_idTipo_Estados_Sistemas,");
            sb.AppendLine("GEN_Tipo_Estados_Sistemas.GEN_nombreTipo_Estados_Sistemas,");
            sb.AppendLine("RCE_Constancia_GES.RCE_idConstancia_GES");
            sb.AppendLine("FROM RCE_IPD");
            sb.AppendLine("INNER JOIN RCE_Eventos ON RCE_Eventos.RCE_idEventos = RCE_IPD.RCE_idEventos");
            sb.AppendLine("LEFT JOIN RCE_Constancia_GES ON RCE_Constancia_GES.RCE_idIPD = RCE_IPD.RCE_idIPD");
            sb.AppendLine("INNER JOIN GEN_Paciente PAC ON PAC.GEN_idPaciente = RCE_Eventos.GEN_idPaciente");
            sb.AppendLine("LEFT JOIN GEN_Paciente REP ON REP.GEN_idPaciente = RCE_Constancia_GES.GEN_id_representantePaciente");
            sb.AppendLine("INNER JOIN RCE_Tipo_IPD ON RCE_Tipo_IPD.RCE_idTipo_IPD = RCE_IPD.RCE_idTipo_IPD");
            sb.AppendLine("INNER JOIN GEN_Tipo_Estados_Sistemas ON GEN_Tipo_Estados_Sistemas.GEN_idTipo_Estados_Sistemas = RCE_IPD.GEN_idTipo_Estados_Sistemas");
            sb.AppendLine("WHERE");
            sb.AppendLine("PAC.GEN_numero_documentoPaciente LIKE '' + @id1 + '%' AND");
            sb.AppendLine("PAC.GEN_nombrePaciente LIKE '%' + @id2 + '%' AND PAC.GEN_ape_paternoPaciente LIKE '%' + @id3 + '%' AND PAC.GEN_ape_maternoPaciente LIKE '%' + @id4 + '%' AND");
            sb.AppendLine("RCE_IPD.GEN_idProfesional = @id5");
            //sb.AppendLine("PAC.GEN_numero_documentoPaciente LIKE '" + sRut + "%' AND");
            //sb.AppendLine("PAC.GEN_nombrePaciente LIKE '%" + sNombre + "%' AND PAC.GEN_ape_paternoPaciente LIKE '%" + sApe + "%' AND PAC.GEN_ape_maternoPaciente LIKE '%" + sSApe + "%' AND");
            //sb.AppendLine("RCE_Eventos.GEN_idProfesional = " + iGEN_idProfesional.ToString());
            sb.AppendLine("AND (RCE_Constancia_GES.RCE_estadoConstancia_GES = 'Activo' OR RCE_Constancia_GES.RCE_estadoConstancia_GES IS NULL)");

            if (iEstadoSistema != 0)
                sb.AppendLine("AND RCE_IPD.GEN_idTipo_Estados_Sistemas = @id6");
            else
            {
                sb.AppendLine("AND RCE_IPD.GEN_idTipo_Estados_Sistemas != @id6");
                iEstadoSistema = 72;
            }
            if (iGrupo != 0)
                sb.AppendLine("AND RCE_IPD.RCE_idGrupo_Patologia_GES = @id7");
            if (iSubGrupo != 0)
                sb.AppendLine("AND RCE_IPD.RCE_idSubgrupo_Patologia_GES = @id8");

            sb.AppendLine("ORDER BY RCE_IPD.RCE_fechaIPD DESC");
            string[][] sArray = new string[][] {
                new string[] { "@id1", sRut },
                new string[] { "@id2", sNombre },
                new string[] { "@id3", sApe },
                new string[] { "@id4", sSApe },
                new string[] { "@id5", iGEN_idProfesional.ToString() },
                new string[] { "@id6", iEstadoSistema.ToString() },
                new string[] { "@id7", iGrupo.ToString() },
                new string[] { "@id8", iSubGrupo.ToString() }
            };
            return con.GetDataTable(sb.ToString(), sArray);
        }

        public DataTable GetIPDGes(string sRut, string sNombre, string sApe, string sSApe, int iProfesional, int iEstadoSistema, int iGrupo, int iSubGrupo)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT RCE_IPD.RCE_idIPD, RCE_IPD.RCE_fechaIPD,");
            sb.AppendLine("PAC.GEN_nombrePaciente + ' ' + ISNULL(PAC.GEN_ape_paternoPaciente,'') + ' ' + ISNULL(PAC.GEN_ape_maternoPaciente,'') PACIENTE,");
            sb.AppendLine("REP.GEN_nombrePaciente + ' ' + ISNULL(REP.GEN_ape_paternoPaciente,'') + ' ' + ISNULL(REP.GEN_ape_maternoPaciente,'') REPRESENTANTE,");
            sb.AppendLine("RCE_IPD.RCE_idTipo_IPD,");
            sb.AppendLine("RCE_Tipo_IPD.RCE_descripcionTipo_IPD,");
            sb.AppendLine("RCE_IPD.GEN_idTipo_Estados_Sistemas,");
            sb.AppendLine("GEN_Tipo_Estados_Sistemas.GEN_nombreTipo_Estados_Sistemas,");
            sb.AppendLine("RCE_Constancia_GES.RCE_idConstancia_GES");
            sb.AppendLine("FROM RCE_IPD");
            sb.AppendLine("INNER JOIN RCE_Eventos ON RCE_Eventos.RCE_idEventos = RCE_IPD.RCE_idEventos");
            sb.AppendLine("LEFT JOIN RCE_Constancia_GES ON RCE_Constancia_GES.RCE_idIPD = RCE_IPD.RCE_idIPD");
            sb.AppendLine("INNER JOIN GEN_Paciente PAC ON PAC.GEN_idPaciente = RCE_Eventos.GEN_idPaciente");
            sb.AppendLine("LEFT JOIN GEN_Paciente REP ON REP.GEN_idPaciente = RCE_Constancia_GES.GEN_id_representantePaciente");
            sb.AppendLine("INNER JOIN RCE_Tipo_IPD ON RCE_Tipo_IPD.RCE_idTipo_IPD = RCE_IPD.RCE_idTipo_IPD");
            sb.AppendLine("INNER JOIN GEN_Tipo_Estados_Sistemas ON GEN_Tipo_Estados_Sistemas.GEN_idTipo_Estados_Sistemas = RCE_IPD.GEN_idTipo_Estados_Sistemas");
            sb.AppendLine("WHERE");
            sb.AppendLine("(RCE_Constancia_GES.RCE_estadoConstancia_GES = 'Activo' OR RCE_Constancia_GES.RCE_estadoConstancia_GES IS NULL) ");
            sb.AppendLine("AND PAC.GEN_numero_documentoPaciente LIKE '' + @id1 + '%' ");
            sb.AppendLine("AND PAC.GEN_nombrePaciente LIKE '%' + @id2 + '%' AND PAC.GEN_ape_paternoPaciente LIKE '%' + @id3 + '%' AND PAC.GEN_ape_paternoPaciente LIKE '%' + @id4 + '%' ");
            if (iProfesional != 0)
                sb.AppendLine("AND RCE_IPD.GEN_idProfesional = @id5");
            if (iEstadoSistema != 0)
                sb.AppendLine("AND RCE_IPD.GEN_idTipo_Estados_Sistemas= @id6");
            if (iGrupo != 0)
                sb.AppendLine("AND RCE_IPD.RCE_idGrupo_Patologia_GES = @id7");
            if (iSubGrupo != 0)
                sb.AppendLine("AND RCE_IPD.RCE_idSubgrupo_Patologia_GES = @id8");
            string[][] sArray = new string[][] {
                new string[] { "@id1", sRut },
                new string[] { "@id2", sNombre },
                new string[] { "@id3", sApe },
                new string[] { "@id4", sSApe },
                new string[] { "@id5", iProfesional.ToString() },
                new string[] { "@id6", iEstadoSistema.ToString() },
                new string[] { "@id7", iGrupo.ToString() },
                new string[] { "@id8", iSubGrupo.ToString() }
            };
            return con.GetDataTable(sb.ToString(), sArray);
        }

        public NegRCE_IPD()
        {

        }

        public NegRCE_IPD(int iRCE_idIPD)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM RCE_IPD WHERE RCE_idIPD = @id");
            string[][] sArray = new string[][] { new string[] { "@id", iRCE_idIPD.ToString() } };

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                {
                    int iRes = 0;

                    RCE_idIPD = int.Parse(dtr["RCE_idIPD"].ToString().Trim());
                    RCE_fechaIPD = DateTime.Parse(dtr["RCE_fechaIPD"].ToString().Trim());
                    GEN_idEstablecimiento = int.Parse(dtr["GEN_idEstablecimiento"].ToString().Trim());
                    GEN_idEspecialidad = int.Parse(dtr["GEN_idEspecialidad"].ToString().Trim());
                    RCE_diagnosticoIPD = dtr["RCE_diagnosticoIPD"].ToString().Trim();
                    RCE_fundamentoIPD = dtr["RCE_fundamentoIPD"].ToString().Trim();
                    RCE_tratamientoIPD = dtr["RCE_tratamientoIPD"].ToString().Trim();
                    RCE_inicio_tratamientoIPD = DateTime.Parse(dtr["RCE_inicio_tratamientoIPD"].ToString().Trim());

                    if (int.TryParse(dtr["RCE_idEventos"].ToString().Trim(), out iRes))
                        RCE_idEventos = int.Parse(dtr["RCE_idEventos"].ToString().Trim());

                    GEN_idAmbito = int.Parse(dtr["GEN_idAmbito"].ToString().Trim());

                    if (int.TryParse(dtr["RCE_idGrupo_Patologia_GES"].ToString().Trim(), out iRes))
                        RCE_idGrupo_Patologia_GES = int.Parse(dtr["RCE_idGrupo_Patologia_GES"].ToString().Trim());
                    if (int.TryParse(dtr["RCE_idSubgrupo_Patologia_GES"].ToString().Trim(), out iRes))
                        RCE_idSubgrupo_Patologia_GES = int.Parse(dtr["RCE_idSubgrupo_Patologia_GES"].ToString().Trim());

                    GEN_idTipo_Estados_Sistemas = int.Parse(dtr["GEN_idTipo_Estados_Sistemas"].ToString().Trim());
                    RCE_estadoIPD = dtr["RCE_estadoIPD"].ToString().Trim();

                    if (int.TryParse(dtr["RCE_idTipo_IPD"].ToString().Trim(), out iRes))
                        RCE_idTipo_IPD = int.Parse(dtr["RCE_idTipo_IPD"].ToString().Trim());
                }
            }
        }

        /// <summary>
        /// Obtiene todo el formulario del IPD, evento, movimiento ipd, servicio de salud, etc
        /// </summary>
        /// <returns></returns>
        public DataTable GetIPDFormulario(int iIdIPD)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("GEN_Establecimiento.GEN_idCiudad CI_ES, GEN_Paciente.GEN_dir_callePaciente + ' ' + GEN_Paciente.GEN_dir_numeroPaciente GEN_dir_callePaciente,");
            sb.AppendLine("GEN_Paciente.GEN_idComuna, RCE_IPD.RCE_fechaIPD, RCE_Constancia_GES.RCE_fechaConstancia_GES,");
            sb.AppendLine("RCE_IPD.RCE_idIPD, GEN_Servicio_Salud.GEN_idServicio_Salud, RCE_IPD.GEN_idEstablecimiento,");
            sb.AppendLine("RCE_IPD.GEN_idEspecialidad, RCE_IPD.GEN_idAmbito,");
            sb.AppendLine("GEN_Paciente.GEN_idPaciente, GEN_Paciente.GEN_numero_documentoPaciente, GEN_Paciente.GEN_digitoPaciente,");
            sb.AppendLine("GEN_Paciente.GEN_idIdentificacion,");
            sb.AppendLine("GEN_Paciente.GEN_nombrePaciente, GEN_Paciente.GEN_ape_paternoPaciente, GEN_Paciente.GEN_ape_maternoPaciente,");
            sb.AppendLine("GEN_Paciente.GEN_idSexo, GEN_Paciente.GEN_fec_nacimientoPaciente,");
            sb.AppendLine("GEN_Paciente.GEN_idPrevision, GEN_Paciente.GEN_idPrevision_Tramo,");
            sb.AppendLine("GEN_Paciente.GEN_emailPaciente, gen_paciente.GEN_telefonoPaciente, GEN_Paciente.GEN_otros_fonosPaciente,");
            sb.AppendLine("RCE_IPD.RCE_idGrupo_Patologia_GES, RCE_Grupo_Patologia_GES.RCE_codigoGrupo_Patologia_GES, RCE_IPD.RCE_idSubgrupo_Patologia_GES,");
            sb.AppendLine("RCE_IPD.RCE_diagnosticoIPD, RCE_IPD.RCE_fundamentoIPD, RCE_IPD.RCE_idTipo_IPD,");
            sb.AppendLine("RCE_IPD.RCE_tratamientoIPD, rce_ipd.RCE_inicio_tratamientoIPD,");
            sb.AppendLine("RCE_IPD.RCE_idEventos, PROEVN.GEN_idProfesional PROEVN,");
            sb.AppendLine("RCE_Movimientos_IPD.GEN_idUsuarios, GEN_Usuarios.GEN_rutUsuarios, GEN_Usuarios.GEN_digitoUsuarios,");
            sb.AppendLine("GEN_Usuarios.GEN_nombreUsuarios, GEN_Usuarios.GEN_apellido_paternoUsuarios, GEN_Usuarios.GEN_apellido_maternoUsuarios,");
            sb.AppendLine("RCE_Constancia_GES.RCE_idConstancia_GES");
            sb.AppendLine("FROM RCE_IPD");
            sb.AppendLine("INNER JOIN GEN_Establecimiento ON GEN_Establecimiento.GEN_idEstablecimiento = RCE_IPD.GEN_idEstablecimiento");
            sb.AppendLine("INNER JOIN GEN_Servicio_Salud ON GEN_Servicio_Salud.GEN_idServicio_Salud = GEN_Establecimiento.GEN_idServicio_Salud");
            sb.AppendLine("INNER JOIN RCE_Eventos ON RCE_Eventos.RCE_idEventos = RCE_IPD.RCE_idEventos");
            sb.AppendLine("INNER JOIN GEN_Profesional PROEVN ON PROEVN.GEN_idProfesional = RCE_Eventos.GEN_idProfesional");
            sb.AppendLine("INNER JOIN GEN_Paciente ON RCE_Eventos.GEN_idPaciente = GEN_Paciente.GEN_idPaciente");
            sb.AppendLine("LEFT JOIN RCE_Movimientos_IPD ON RCE_Movimientos_IPD.RCE_idIPD = RCE_IPD.RCE_idIPD");
            sb.AppendLine("LEFT JOIN GEN_Usuarios ON GEN_Usuarios.GEN_idUsuarios = RCE_Movimientos_IPD.GEN_idUsuarios");
            sb.AppendLine("LEFT JOIN RCE_Constancia_GES ON RCE_Constancia_GES.RCE_idIPD = RCE_IPD.RCE_idIPD");
            sb.AppendLine("LEFT JOIN RCE_Grupo_Patologia_GES ON RCE_Grupo_Patologia_GES.RCE_idGrupo_Patologia_GES = RCE_IPD.RCE_idGrupo_Patologia_GES");
            sb.AppendLine("WHERE RCE_IPD.RCE_idIPD = @id AND RCE_IPD.RCE_estadoIPD = 'Activo'");
            sb.AppendLine("GROUP BY");
            sb.AppendLine("GEN_Establecimiento.GEN_idCiudad, GEN_Paciente.GEN_dir_callePaciente + ' ' + GEN_Paciente.GEN_dir_numeroPaciente,");
            sb.AppendLine("GEN_Paciente.GEN_idComuna, RCE_IPD.RCE_fechaIPD, RCE_Constancia_GES.RCE_fechaConstancia_GES,");
            sb.AppendLine("RCE_IPD.RCE_idIPD, GEN_Servicio_Salud.GEN_idServicio_Salud, RCE_IPD.GEN_idEstablecimiento,");
            sb.AppendLine("RCE_IPD.GEN_idEspecialidad, RCE_IPD.GEN_idAmbito,");
            sb.AppendLine("GEN_Paciente.GEN_idPaciente, GEN_Paciente.GEN_numero_documentoPaciente, GEN_Paciente.GEN_digitoPaciente,");
            sb.AppendLine("GEN_Paciente.GEN_idIdentificacion,");
            sb.AppendLine("GEN_Paciente.GEN_nombrePaciente, GEN_Paciente.GEN_ape_paternoPaciente, GEN_Paciente.GEN_ape_maternoPaciente,");
            sb.AppendLine("GEN_Paciente.GEN_idSexo, GEN_Paciente.GEN_fec_nacimientoPaciente,");
            sb.AppendLine("GEN_Paciente.GEN_idPrevision, GEN_Paciente.GEN_idPrevision_Tramo,");
            sb.AppendLine("GEN_Paciente.GEN_emailPaciente, gen_paciente.GEN_telefonoPaciente, GEN_Paciente.GEN_otros_fonosPaciente,");
            sb.AppendLine("RCE_IPD.RCE_idGrupo_Patologia_GES, RCE_Grupo_Patologia_GES.RCE_codigoGrupo_Patologia_GES, RCE_IPD.RCE_idSubgrupo_Patologia_GES,");
            sb.AppendLine("RCE_IPD.RCE_diagnosticoIPD, RCE_IPD.RCE_fundamentoIPD, RCE_IPD.RCE_idTipo_IPD,");
            sb.AppendLine("RCE_IPD.RCE_tratamientoIPD, rce_ipd.RCE_inicio_tratamientoIPD,");
            sb.AppendLine("RCE_IPD.RCE_idEventos, PROEVN.GEN_idProfesional,");
            sb.AppendLine("RCE_Movimientos_IPD.GEN_idUsuarios, GEN_Usuarios.GEN_rutUsuarios, GEN_Usuarios.GEN_digitoUsuarios,");
            sb.AppendLine("GEN_Usuarios.GEN_nombreUsuarios, GEN_Usuarios.GEN_apellido_paternoUsuarios, GEN_Usuarios.GEN_apellido_maternoUsuarios,");
            sb.AppendLine("RCE_Constancia_GES.RCE_idConstancia_GES");
            string[][] sArray = new string[][] { new string[] { "@id", iIdIPD.ToString() } };

            return con.GetDataTable(sb.ToString(), sArray);
        }
    }
}