﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RegistroClinico.Datos;
using System.Net;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;
using System.Text;

namespace RegistroClinico.Negocio
{
    public class NegRCE_Tipo_Interconsulta : RCE_Tipo_Interconsulta
    {
        public NegRCE_Tipo_Interconsulta(int RCE_idTipo_Interconsulta) {

            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/RCE_Tipo_Interconsulta/" + RCE_idTipo_Interconsulta.ToString());
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            Tipo_Interconsulta tIC = JsonConvert.DeserializeObject<Tipo_Interconsulta>(sRes, settings);
            {
                RCE_idTipo_Interconsulta = tIC.RCE_idTipo_Interconsulta;
                RCE_DescripcionTipo_Interconsulta = tIC.RCE_DescripcionTipo_Interconsulta;
                RCE_estadoTipo_Interconsulta = tIC.RCE_estadoTipo_Interconsulta;
            }

        }
    }
}