﻿using RegistroClinico.Datos;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;
using System.Text;

namespace RegistroClinico.Negocio
{
    public class NegRCE_Interconsulta : RCE_Interconsulta
    {

        public NegRCE_Interconsulta() { }

        public NegRCE_Interconsulta(int iRCE_idInterconsulta) {

            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/RCE_Interconsulta/" + iRCE_idInterconsulta.ToString());
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            Interconsulta jsonList = JsonConvert.DeserializeObject<List<Interconsulta>>(sRes, settings).ElementAt(0);
            {

                Interconsulta ic = jsonList;

                RCE_idInterconsulta = ic.RCE_idInterconsulta;
                GEN_idUbicacion_enviadoInterconsulta = ic.GEN_idUbicacion_enviadoInterconsulta;
                GEN_idUbicacion_derivadoInterconsulta = ic.GEN_idUbicacion_derivadoInterconsulta;
                RCE_diagnosticoInterconsulta = ic.RCE_diagnosticoInterconsulta;
                RCE_sintomatologiaInterconsulta = ic.RCE_sintomatologiaInterconsulta;
                RCE_solicitudInterconsulta = ic.RCE_solicitudInterconsulta;
                RCE_fecha_hora_creacionInterconsulta = ic.RCE_fecha_hora_creacionInterconsulta;
                GEN_idProfesional = ic.GEN_idProfesional;
                RCE_idEventos = ic.RCE_idEventos;
                RCE_estadoInterconsulta = ic.RCE_estadoInterconsulta;
                GEN_idEspecialidad_enviadaInterconsulta = ic.GEN_idEspecialidad_enviadaInterconsulta;
                GEN_idEspecialidad_derivadaInterconsulta = ic.GEN_idEspecialidad_derivadaInterconsulta;
                GEN_idTipo_Estados_Sistemas = ic.GEN_idTipo_Estados_Sistemas;
                RCE_idInterconsulta_GES = ic.RCE_idInterconsulta_GES;

            }
        }

        public List<Dictionary<string, object>> ObtenerJsonInterconsulta(int RCE_idInterconsulta)
        {

            try
            {
                HttpContext contexto = HttpContext.Current;
                string url = Funciones.GetPropiedad(ref contexto, "urlWebApi") + "RCE_Interconsulta/Imprimir/" + RCE_idInterconsulta.ToString();
                return JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(Funciones.ObtenerJsonWebApi(url));
            }
            catch (Exception ex)
            {
                string fail = ex.Message + ex.StackTrace;
            }

            return null;
        }

    }
}