﻿using RegistroClinico.Datos;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;
using System.Web;
using System.Text;

namespace RegistroClinico.Negocio
{
    public class NegRCE_Interconsulta_GES : RCE_Interconsulta_GES
    {
        public NegRCE_Interconsulta_GES(int? RCE_idInterconsulta_GES)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/RCE_Interconsulta_GES/" + RCE_idInterconsulta_GES.ToString());
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            Interconsulta_GES icGES = JsonConvert.DeserializeObject<Interconsulta_GES>(sRes, settings);
            {
                this.RCE_idInterconsulta_GES = icGES.RCE_idInterconsulta_GES;
                RCE_idGrupo_Patologia_GES = icGES.RCE_idGrupo_Patologia_GES;
                RCE_idSubgrupo_Patologia_GES = icGES.RCE_idSubgrupo_Patologia_GES;
                RCE_idTipo_Interconsulta = icGES.RCE_idTipo_Interconsulta;
                RCE_fundamentos_diagnosticoInterconsulta_GES = icGES.RCE_fundamentos_diagnosticoInterconsulta_GES;
                RCE_tratamientoInterconsulta_GES = icGES.RCE_tratamientoInterconsulta_GES;
                RCE_estadoInterconsulta_GES = icGES.RCE_estadoInterconsulta_GES;
                RCE_idInterconsulta = icGES.RCE_idInterconsulta;
                RCE_otros_tipoInterconsulta_GES = icGES.RCE_otros_tipoInterconsulta_GES;
            }
        }
    }
}