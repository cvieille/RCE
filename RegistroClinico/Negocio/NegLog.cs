﻿using RegistroClinico.Datos;
using System;
using System.IO;
using System.Web;

namespace RegistroClinico.Negocio
{
    public class NegLog : Log
    {
        
        public static void GuardarLog(string title, string message)
        {

            string FilePath = HttpContext.Current.Server.MapPath("~/log.txt");
            var fileStream = new FileStream(FilePath, FileMode.Append);
            var writter = new StreamWriter(fileStream);
            writter.WriteLine(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + " " + title);
            writter.WriteLine(message);
            writter.Close();

        }

    }
}