﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace RegistroClinico
{
    public class NegRCE_Constancia_GES : RCE_Constancia_GES
    {
        public int Insert()
        {
            ConexionSQL conexionSQL = new ConexionSQL();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO RCE_Constancia_GES(RCE_idIPD,GEN_idProfesional, RCE_fechaConstancia_GES, GEN_id_representantePaciente,");
            sb.AppendLine("GEN_idTipo_Representante, GEN_idTipo_Estados_Sistemas, RCE_estadoConstancia_GES)");
            sb.AppendLine("OUTPUT INSERTED.RCE_idConstancia_GES");
            sb.AppendLine("VALUES(");
            sb.AppendLine("@id1,");
            sb.AppendLine("@id2,");
            sb.AppendLine("@id3,");
            sb.AppendLine("@id4,");
            sb.AppendLine("@id5,");
            sb.AppendLine("@id6,");
            sb.AppendLine("@id7)");
            string[][] sArray = new string[][] {
                new string[] { "@id1", RCE_idIPD.ToString() },
                new string[] { "@id2", GEN_idProfesional.ToString() },
                new string[] { "@id3", RCE_fechaConstancia_GES.ToString() },
                new string[] { "@id4", GEN_id_representantePaciente.ToString() },
                new string[] { "@id5", (GEN_idTipo_Representante == 0 ? "null" : GEN_idTipo_Representante.ToString()) },
                new string[] { "@id6", GEN_idTipo_Estados_Sistemas.ToString() },
                new string[] { "@id7", RCE_estadoConstancia_GES.ToString() }
            };

            return conexionSQL.InsertQuery(sb.ToString(), true, sArray);
        }


        public void Update()
        {
            ConexionSQL con = new ConexionSQL();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE RCE_Constancia_GES SET");
            //sb.AppendLine("RCE_idIPD = " + RCE_idIPD.ToString() + ",");
            //sb.AppendLine("GEN_idProfesional = " + GEN_idProfesional.ToString() + ",");
            //sb.AppendLine("RCE_fechaConstancia_GES = '" + RCE_fechaConstancia_GES.ToString() + "',");
            //sb.AppendLine("GEN_id_representantePaciente = " + GEN_id_representantePaciente.ToString() + ",");
            //sb.AppendLine("GEN_idTipo_Representante = " + (GEN_idTipo_Representante == 0 ? "null" : GEN_idTipo_Representante.ToString()) + ",");
            //sb.AppendLine("GEN_idTipo_Estados_Sistemas = " + GEN_idTipo_Estados_Sistemas.ToString() + ",");
            //sb.AppendLine("RCE_estadoConstancia_GES = '" + RCE_estadoConstancia_GES + "'");
            //sb.AppendLine("WHERE RCE_idConstancia_GES = " + RCE_idConstancia_GES.ToString());
            sb.AppendLine("RCE_idIPD = @id1,");
            sb.AppendLine("GEN_idProfesional = @id2,");
            sb.AppendLine("RCE_fechaConstancia_GES = @id3,");
            sb.AppendLine("GEN_id_representantePaciente = @id4,");
            sb.AppendLine("GEN_idTipo_Representante = @id5,");
            sb.AppendLine("GEN_idTipo_Estados_Sistemas = @id6,");
            sb.AppendLine("RCE_estadoConstancia_GES = @id7");
            sb.AppendLine("WHERE RCE_idConstancia_GES = @id8");
            string[][] sArray = new string[][] {
                new string[] { "@id1", RCE_idIPD.ToString() },
                new string[] { "@id2", GEN_idProfesional.ToString() },
                new string[] { "@id3", RCE_fechaConstancia_GES.ToString() },
                new string[] { "@id4", GEN_id_representantePaciente.ToString() },
                new string[] { "@id5", (GEN_idTipo_Representante == 0 ? "null" : GEN_idTipo_Representante.ToString()) },
                new string[] { "@id6", GEN_idTipo_Estados_Sistemas.ToString() },
                new string[] { "@id7", RCE_estadoConstancia_GES },
                new string[] { "@id8", RCE_idConstancia_GES.ToString() }
            };
            con.UpdateQuery(sb.ToString(), sArray);
        }


        /// <summary>
        /// Obtiene toda la información visible de la constancia
        /// </summary>
        /// <returns></returns>
        public DataTable GetConstanciaIPD(int iIdIPD)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT");
            sb.AppendLine("RCE_Constancia_GES.RCE_idIPD, RCE_Constancia_GES.RCE_idConstancia_GES, RCE_Constancia_GES.RCE_fechaConstancia_GES,");
            sb.AppendLine("RCE_Constancia_GES.GEN_id_representantePaciente, RCE_Constancia_GES.GEN_idProfesional,");
            sb.AppendLine("GEN_Profesional.GEN_rutProfesional + '-' + GEN_Profesional.GEN_digitoProfesional RUTPRO,");
            sb.AppendLine("GEN_Profesional.GEN_nombreProfesional + ' ' + ISNULL(GEN_Profesional.GEN_apellidoProfesional,'') + ' ' + ISNULL(GEN_Profesional.GEN_sapellidoProfesional,'') NOMBREPRO,");
            sb.AppendLine("REP.GEN_numero_documentoPaciente + '-' + ISNULL(REP.GEN_digitoPaciente,'') RUTREP,");
            sb.AppendLine("REP.GEN_nombrePaciente + ' ' + ISNULL(REP.GEN_ape_paternoPaciente,'') + ' ' + ISNULL(REP.GEN_ape_maternoPaciente,'') NOMBREREP,");
            sb.AppendLine("RCE_Constancia_GES.GEN_idTipo_Representante");
            sb.AppendLine("FROM RCE_Constancia_GES");
            sb.AppendLine("INNER JOIN GEN_Profesional ON GEN_Profesional.GEN_idProfesional = RCE_Constancia_GES.GEN_idProfesional");
            sb.AppendLine("INNER JOIN GEN_Paciente REP ON REP.GEN_idPaciente = RCE_Constancia_GES.GEN_id_representantePaciente");
            //sb.AppendLine("WHERE RCE_idIPD = " + iIdIPD.ToString() + " AND RCE_estadoConstancia_GES = 'Activo'");
            sb.AppendLine("WHERE RCE_idIPD = @id AND RCE_estadoConstancia_GES = 'Activo'");
            string[][] sArray = new string[][] { new string[] { "@id", iIdIPD.ToString() } };

            return con.GetDataTable(sb.ToString(), sArray);
        }
    }
}