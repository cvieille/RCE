﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;

namespace RegistroClinico
{
    public class NegRCE_Subgrupo_Patologia_GES : RCE_Subgrupo_Patologia_GES
    {
        public void GetComboSubGrupo(ref DropDownList ddl, int iIndex)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/RCE_subGrupo_Patologia_GES/RCE_idGrupo_Patologia_GES/" + iIndex.ToString());
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Subgrupo_Patologia_GES> jsonList = JsonConvert.DeserializeObject<List<Subgrupo_Patologia_GES>>(sRes, settings);
            IEnumerable<Subgrupo_Patologia_GES> lActivo = jsonList.Where(x => x.RCE_estado_Subgrupo_Patologia_GES.ToLower() == "activo");

            ddl.DataSource = lActivo;
            ddl.DataTextField = "RCE_descripcion_Subgrupo_Patologia_GES";
            ddl.DataValueField = "RCE_idSubgrupo_Patologia_GES";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("-Seleccione-", "0"));
        }

        public NegRCE_Subgrupo_Patologia_GES()
        {
        }
        public NegRCE_Subgrupo_Patologia_GES(int iRCE_idSubgrupo_Patologia_GES)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM RCE_Subgrupo_Patologia_GES WHERE RCE_idSubgrupo_Patologia_GES = @id");
            string[][] sArray = new string[][] { new string[] { "@id", iRCE_idSubgrupo_Patologia_GES.ToString() } };

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                {
                    RCE_idSubgrupo_Patologia_GES = int.Parse(dtr["RCE_idSubgrupo_Patologia_GES"].ToString().Trim());
                    RCE_descripcion_Subgrupo_Patologia_GES = dtr["RCE_descripcion_Subgrupo_Patologia_GES"].ToString().Trim();
                    RCE_estado_Subgrupo_Patologia_GES = dtr["RCE_estado_Subgrupo_Patologia_GES"].ToString().Trim();
                    RCE_idGrupo_Patologia_GES = int.Parse(dtr["RCE_idGrupo_Patologia_GES"].ToString().Trim());
                }
            }
        }
        //public void GetCombo(ref DropDownList ddl)
        //{
        //    ddl.Items.Clear();
        //    ddl.Items.Add(new ListItem("-Seleccione-", "0"));
        //    ConexionSQL con = new ConexionSQL();

        //    StringBuilder sb = new StringBuilder("SELECT * FROM RCE_Subgrupo_Patologia_GES WHERE RCE_idGrupo_Patologia_GES = @id AND RCE_estado_Subgrupo_Patologia_GES = 'ACTIVO'");
        //    string[][] sArray = new string[][] { new string[] { "@id", RCE_idGrupo_Patologia_GES.ToString() } };

        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable(sb.ToString(), sArray);

        //    foreach (DataRow dtr in dt.Rows)
        //        ddl.Items.Add(new ListItem(dtr["RCE_descripcion_Subgrupo_Patologia_GES"].ToString().Trim(), dtr["RCE_idSubgrupo_Patologia_GES"].ToString().Trim()));
        //}
    }
}