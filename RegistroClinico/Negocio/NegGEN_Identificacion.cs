﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;

namespace RegistroClinico
{
    public class NegGEN_Identificacion
    {
        public void GetComboIdentificacion(ref DropDownList ddl)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/GEN_Identificacion");
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Identificacion> jsonList = JsonConvert.DeserializeObject<List<Identificacion>>(sRes, settings);
            IEnumerable<Identificacion> lActivo = jsonList.Where(x => x.GEN_estadoIdentificacion.ToLower() == "activo");

            ddl.DataSource = lActivo;
            ddl.DataTextField = "GEN_nombreIdentificacion";
            ddl.DataValueField = "GEN_idIdentificacion";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("-Seleccione-", "0"));
        }

        //public void GetCombo(ref DropDownList ddl)
        //{
        //    ddl.Items.Clear();
        //    ConexionSQL con = new ConexionSQL();

        //    StringBuilder sb = new StringBuilder();

        //    sb.AppendLine("SELECT GEN_idIdentificacion, GEN_nombreIdentificacion FROM GEN_Identificacion");
        //    sb.AppendLine("WHERE GEN_estadoIdentificacion = 'Activo'");

        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable(sb.ToString());

        //    foreach (DataRow dtr in dt.Rows)
        //        ddl.Items.Add(new ListItem(dtr["GEN_nombreIdentificacion"].ToString().Trim(), dtr["GEN_idIdentificacion"].ToString().Trim()));
        //}
    }
}