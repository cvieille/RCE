﻿using RegistroClinico.Datos;
using System;
using System.Net;
using System.Collections.Generic;
using static RegistroClinico.WebApi;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Text;

namespace RegistroClinico.Negocio
{
    public class NegRCE_Hoja_Evolucion : RCE_Hoja_Evolucion 
    {
        
        public List<Dictionary<string, object>> ObtenerJsonHojaEvolucion(int RCE_idHoja_Evolucion)
        {

            try
            {
                HttpContext contexto = HttpContext.Current;
                string url = Funciones.GetPropiedad(ref contexto, "urlWebApi") + "RCE_Hoja_Evolucion/" + RCE_idHoja_Evolucion.ToString();
                return JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(Funciones.ObtenerJsonWebApi(url));
            }
            catch (Exception ex)
            {
                string fail = ex.Message + ex.StackTrace;
            }

            return null;
        }

    }
}