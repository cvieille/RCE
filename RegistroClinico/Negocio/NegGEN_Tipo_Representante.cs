﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;

namespace RegistroClinico
{
    public class NegGEN_Tipo_Representante : GEN_Tipo_Representante
    {
        public void GetComboTipo_Representante(ref DropDownList ddl)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/GEN_Tipo_Representante");
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Tipo_Representante> jsonList = JsonConvert.DeserializeObject<List<Tipo_Representante>>(sRes, settings);
            //IEnumerable<Tipo_Representante> lActivo = jsonList.Where(x => x.RCE_estadoTipo_IPD.ToLower() == "activo");

            ddl.DataSource = jsonList;
            ddl.DataTextField = "GEN_nombreTipo_Representante";
            ddl.DataValueField = "GEN_idTipo_Representante";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("-Seleccione-", "0"));
        }

        public NegGEN_Tipo_Representante()
        {
        }
        public NegGEN_Tipo_Representante(int iGEN_idTipo_Representante)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Tipo_Representante WHERE GEN_idTipo_Representante = @id");
            //sb.AppendLine("SELECT * FROM GEN_Tipo_Representante WHERE GEN_idTipo_Representante = " + iGEN_idTipo_Representante.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idTipo_Representante.ToString() } };

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];

                GEN_idTipo_Representante = int.Parse(dtr["GEN_idTipo_Representante"].ToString().Trim());
                GEN_nombreTipo_Representante = dtr["GEN_nombreTipo_Representante"].ToString().Trim();
            }
        }
        //public void GetCombo(ref DropDownList ddl)
        //{
        //    ddl.Items.Clear();
        //    ddl.Items.Add(new ListItem("-Seleccione-", "0"));
        //    ConexionSQL con = new ConexionSQL();

        //    StringBuilder sb = new StringBuilder("SELECT * FROM GEN_Tipo_Representante");
        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable(sb.ToString());

        //    foreach (DataRow dtr in dt.Rows)
        //        ddl.Items.Add(new ListItem(dtr["GEN_nombreTipo_Representante"].ToString().Trim(), dtr["GEN_idTipo_Representante"].ToString().Trim()));
        //}
    }
}