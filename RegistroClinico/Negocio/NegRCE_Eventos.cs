﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;

namespace RegistroClinico
{
    public class NegRCE_Eventos : RCE_Eventos
    {
        public NegRCE_Eventos(){}

        public NegRCE_Eventos(int RCE_idEvento) {

            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/RCE_Eventos/" + RCE_idEvento.ToString());
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            Eventos jsonList = JsonConvert.DeserializeObject<Eventos>(sRes, settings);
            {

                Eventos ev = jsonList;

                RCE_idEventos = ev.RCE_idEventos;
                RCE_idTipo_Evento = ev.RCE_idTipo_Evento;
                RCE_anamnesisEventos = ev.RCE_anamnesisEventos;
                RCE_diagnosticoEventos = ev.RCE_diagnosticoEventos;
                RCE_alta_pacienteEventos = DateTime.Parse(ev.RCE_alta_pacienteEventos);
                GEN_idPaciente = ev.GEN_idPaciente;
                GEN_idProfesional = ev.GEN_idProfesional;
                RCE_fecha_inicioEventos = DateTime.Parse(ev.RCE_fecha_inicioEventos);
                RCE_fecha_terminoEventos = DateTime.Parse(ev.RCE_fecha_terminoEventos);
                RCE_causaEventos = ev.RCE_causaEventos;

            }
        }

        public int Insert()
        {
            Eventos e = new Eventos();
            e.RCE_idTipo_Evento = RCE_idTipo_Evento;
            e.RCE_anamnesisEventos = RCE_anamnesisEventos;
            e.RCE_diagnosticoEventos = RCE_diagnosticoEventos;
            e.RCE_alta_pacienteEventos = (RCE_alta_pacienteEventos == DateTime.MinValue ? "NULL" : RCE_alta_pacienteEventos.ToString("yyyy-MM-ddTh:mm:ss"));
            e.GEN_idPaciente = GEN_idPaciente;
            e.GEN_idProfesional = GEN_idProfesional;
            e.RCE_fecha_inicioEventos = RCE_fecha_inicioEventos.ToString("yyyy-MM-ddTh:mm:ss");
            e.RCE_fecha_terminoEventos = (RCE_fecha_terminoEventos == DateTime.MinValue ? "NULL" : RCE_fecha_terminoEventos.ToString("yyyy-MM-ddTh:mm:ss"));
            e.RCE_causaEventos = RCE_causaEventos;
            e.RCE_estadoEventos = "Activo";
            string sRes = JsonConvert.SerializeObject(e);


            WebClient WebClient = new WebClient();
            WebClient.Headers[HttpRequestHeader.ContentType] = "application/json";
            sRes = WebClient.UploadString("http://10.6.180.216/WebApi/api/RCE_Eventos/", sRes);

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;
            e = JsonConvert.DeserializeObject<Eventos>(sRes, settings);
            return e.RCE_idEventos;



            //ConexionSQL conexionSQL = new ConexionSQL();
            //StringBuilder sb = new StringBuilder();
            //sb.AppendLine("INSERT INTO RCE_Eventos(RCE_idTipo_Evento, RCE_anamnesisEventos, RCE_diagnosticoEventos,");
            //sb.AppendLine("RCE_alta_pacienteEventos, GEN_idPaciente, GEN_idProfesional, RCE_fecha_inicioEventos, RCE_fecha_terminoEventos, RCE_causaEventos)");
            //sb.AppendLine("OUTPUT INSERTED.RCE_idEventos");
            //sb.AppendLine("VALUES(");
            //sb.AppendLine("@id1,");
            //sb.AppendLine("@id2,");
            //sb.AppendLine("@id3,");
            //sb.AppendLine("@id4,");
            //sb.AppendLine("@id5,");
            //sb.AppendLine("@id6,");
            //sb.AppendLine("@id7,");
            //sb.AppendLine("@id8,");
            //sb.AppendLine("@id9)");
            ////sb.AppendLine("" + RCE_idTipo_Evento.ToString() + ",");
            ////sb.AppendLine("'" + RCE_anamnesisEventos + "',");
            ////sb.AppendLine("'" + RCE_diagnosticoEventos + "',");
            ////sb.AppendLine((RCE_alta_pacienteEventos == DateTime.MinValue ? "NULL," : "'" + RCE_alta_pacienteEventos.ToString() + "',"));
            ////sb.AppendLine("" + GEN_idPaciente.ToString() + ",");
            ////sb.AppendLine("" + GEN_idProfesional.ToString() + ",");
            ////sb.AppendLine("'" + RCE_fecha_inicioEventos.ToString() + "',");
            ////sb.AppendLine((RCE_fecha_terminoEventos == DateTime.MinValue ? "NULL)" : "'" + RCE_fecha_terminoEventos.ToString() + "',"));
            ////sb.AppendLine("'" + RCE_causaEventos + "')");
            //string[][] sArray = new string[][] {
            //    new string[] { "@id1", RCE_idTipo_Evento.ToString() },
            //    new string[] { "@id2", RCE_anamnesisEventos },
            //    new string[] { "@id3", RCE_diagnosticoEventos },
            //    new string[] { "@id4", (RCE_alta_pacienteEventos == DateTime.MinValue ? "NULL" : RCE_alta_pacienteEventos.ToString()) },
            //    new string[] { "@id5", GEN_idPaciente.ToString() },
            //    new string[] { "@id6", GEN_idProfesional.ToString() },
            //    new string[] { "@id7", RCE_fecha_inicioEventos.ToString() },
            //    new string[] { "@id8", (RCE_fecha_terminoEventos == DateTime.MinValue ? "NULL" : RCE_fecha_terminoEventos.ToString()) },
            //    new string[] { "@id9", RCE_causaEventos }
            //};

            //return conexionSQL.InsertQuery(sb.ToString(), true, sArray);
        }

        public List<Eventos> GetEventosPaciente(int iGEN_idPaciente)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/RCE_Eventos/GEN_idPaciente/" + iGEN_idPaciente.ToString());
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Eventos> jsonList = JsonConvert.DeserializeObject<List<Eventos>>(sRes, settings);
            foreach (Eventos e in jsonList)
            {
                DateTime d;
                if (DateTime.TryParse(e.RCE_alta_pacienteEventos, out d))
                    e.RCE_alta_pacienteEventos = Funciones.InvertirFecha(e.RCE_alta_pacienteEventos.ToString().Split('T')[0]);
                if (DateTime.TryParse(e.RCE_fecha_inicioEventos, out d))
                    e.RCE_fecha_inicioEventos = Funciones.InvertirFecha(e.RCE_fecha_inicioEventos.ToString().Split('T')[0]);
                if (DateTime.TryParse(e.RCE_fecha_terminoEventos, out d))
                    e.RCE_fecha_terminoEventos = Funciones.InvertirFecha(e.RCE_fecha_terminoEventos.ToString().Split('T')[0]);
            }
            //IEnumerable<Eventos> lActivo = jsonList.Where(x => x.RCE_estadoTipo_Evento.ToLower() == "activo");
            return jsonList;



            //ConexionSQL con = new ConexionSQL();

            //StringBuilder sb = new StringBuilder();
            //sb.AppendLine("SELECT RCE_Eventos.RCE_idEventos, RCE_Tipo_Evento.RCE_descripcionTipo_Evento, RCE_Eventos.RCE_anamnesisEventos, RCE_Eventos.RCE_diagnosticoEventos,");
            //sb.AppendLine("RCE_Eventos.RCE_alta_pacienteEventos, RCE_Eventos.RCE_fecha_inicioEventos, RCE_Eventos.RCE_fecha_terminoEventos,");
            //sb.AppendLine("GEN_Profesional.GEN_nombreProfesional + ' ' + GEN_Profesional.GEN_apellidoProfesional + ' ' +  isnull(GEN_Profesional.GEN_sapellidoProfesional,'') GEN_nombreProfesional");
            //sb.AppendLine("FROM RCE_Eventos");
            //sb.AppendLine("INNER JOIN RCE_Tipo_Evento ON RCE_Eventos.RCE_idTipo_Evento = RCE_Tipo_Evento.RCE_idTipo_Evento");
            //sb.AppendLine("INNER JOIN GEN_Profesional ON RCE_Eventos.GEN_idProfesional = GEN_Profesional.GEN_idProfesional");
            //sb.AppendLine("WHERE RCE_Eventos.GEN_idPaciente = @id");
            ////sb.AppendLine("WHERE RCE_Eventos.GEN_idPaciente = " + iGEN_idPaciente.ToString());
            //if (!bTodos)
            //    sb.AppendLine("AND RCE_Eventos.RCE_fecha_inicioEventos >= DATEADD(YEAR,-1,GETDATE())");
            //string[][] sArray = new string[][] { new string[] { "@id", iGEN_idPaciente.ToString() } };

            //return con.GetDataTable(sb.ToString(), sArray);
        }
    }
}