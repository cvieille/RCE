﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace RegistroClinico
{
    public class NegGEN_Servicio_Salud : GEN_Servicio_Salud
    {
        public NegGEN_Servicio_Salud()
        {
        }
        public NegGEN_Servicio_Salud(int iGEN_idServicio_Salud)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Servicio_Salud WHERE GEN_idServicio_Salud = @id");
            //sb.AppendLine("SELECT * FROM GEN_Servicio_Salud WHERE GEN_idServicio_Salud = " + iGEN_idServicio_Salud.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idServicio_Salud.ToString() }/*, new string[] { "", "" } */};

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                {
                    int iRes = 0;
                    GEN_idServicio_Salud = int.Parse(dtr["GEN_idServicio_Salud"].ToString().Trim());
                    if (int.TryParse(dtr["GEN_codigo_deisServicio_Salud"].ToString().Trim(), out iRes))
                        GEN_codigo_deisServicio_Salud = int.Parse(dtr["GEN_codigo_deisServicio_Salud"].ToString().Trim());
                    GEN_nombreServicio_Salud = dtr["GEN_nombreServicio_Salud"].ToString();
                    GEN_estadoServicio_Salud = dtr["GEN_estadoServicio_Salud"].ToString();
                }
            }
        }

        public void GetCombo(ref DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("-Seleccione-", "0"));
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder("SELECT * FROM GEN_Servicio_Salud WHERE GEN_estadoServicio_Salud = 'ACTIVO' ORDER BY GEN_idServicio_Salud");

            DataTable dt = new DataTable();
            dt = con.GetDataTable(sb.ToString());

            foreach (DataRow dtr in dt.Rows)
                ddl.Items.Add(new ListItem(dtr["GEN_nombreServicio_Salud"].ToString().Trim(), dtr["GEN_idServicio_Salud"].ToString().Trim()));
        }
    }
}