﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace RegistroClinico
{
    public class NegGEN_Usuarios : GEN_Usuarios
    {
        public NegGEN_Usuarios()
        {
        }

        public NegGEN_Usuarios(int GEN_idUsuarios)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Usuarios WHERE GEN_idUsuarios = @id");
            //sb.AppendLine("SELECT * FROM GEN_Usuarios WHERE GEN_idUsuarios = " + GEN_idUsuarios.ToString() + "");
            string[][] sArray = new string[][] { new string[] { "@id", GEN_idUsuarios.ToString() } };

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                {
                    this.GEN_idUsuarios = int.Parse(dtr["GEN_idUsuarios"].ToString().Trim());
                    GEN_loginUsuarios = dtr["GEN_loginUsuarios"].ToString().Trim();
                    GEN_claveUsuarios = dtr["GEN_claveUsuarios"].ToString().Trim();
                    GEN_nombreUsuarios = dtr["GEN_nombreUsuarios"].ToString().Trim();
                    GEN_apellido_paternoUsuarios = dtr["GEN_apellido_paternoUsuarios"].ToString().Trim();
                    GEN_apellido_maternoUsuarios = dtr["GEN_apellido_maternoUsuarios"].ToString().Trim();
                    GEN_rutUsuarios = dtr["GEN_rutUsuarios"].ToString().Trim();
                    GEN_digitoUsuarios = char.Parse(dtr["GEN_digitoUsuarios"].ToString().Trim());
                    GEN_telefonoUsuarios = dtr["GEN_telefonoUsuarios"].ToString().Trim();
                    GEN_recibe_notificacionesUsuarios = dtr["GEN_recibe_notificacionesUsuarios"].ToString().Trim();
                    GEN_emailUsuarios = dtr["GEN_emailUsuarios"].ToString().Trim();
                }
            }
        }

        public bool LoginUsuario(string sLogin, char[] sPass)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Usuarios WHERE GEN_loginUsuarios = @id1 AND GEN_claveUsuarios = @id2");
            //sb.AppendLine("SELECT * FROM GEN_Usuarios WHERE GEN_loginUsuarios = '" + sLogin + "' AND GEN_claveUsuarios = '" + new string(sPass) + "'");
            string[][] sArray = new string[][] {
                new string[] { "@id1", sLogin },
                new string[] { "@id2", new string(sPass) }
            };

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                {
                    GEN_idUsuarios = int.Parse(dtr["GEN_idUsuarios"].ToString().Trim());
                    GEN_loginUsuarios = dtr["GEN_loginUsuarios"].ToString().Trim();
                    GEN_claveUsuarios = dtr["GEN_claveUsuarios"].ToString().Trim();
                    GEN_nombreUsuarios = dtr["GEN_nombreUsuarios"].ToString().Trim();
                    GEN_apellido_paternoUsuarios = dtr["GEN_apellido_paternoUsuarios"].ToString().Trim();
                    GEN_apellido_maternoUsuarios = dtr["GEN_apellido_maternoUsuarios"].ToString().Trim();
                    GEN_rutUsuarios = dtr["GEN_rutUsuarios"].ToString().Trim();
                    GEN_digitoUsuarios = char.Parse(dtr["GEN_digitoUsuarios"].ToString().Trim());
                    GEN_telefonoUsuarios = dtr["GEN_telefonoUsuarios"].ToString().Trim();
                    GEN_recibe_notificacionesUsuarios = dtr["GEN_recibe_notificacionesUsuarios"].ToString().Trim();
                    GEN_emailUsuarios = dtr["GEN_emailUsuarios"].ToString().Trim();
                }
                return true;
            }
            return false;
        }
    }
}