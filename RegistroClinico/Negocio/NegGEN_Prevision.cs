﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;

namespace RegistroClinico
{
    public class NegGEN_Prevision : GEN_Prevision
    {
        public void GetComboPrevision(ref DropDownList ddl)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/GEN_Prevision");
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Prevision> jsonList = JsonConvert.DeserializeObject<List<Prevision>>(sRes, settings);
            IEnumerable<Prevision> lActivo = jsonList.Where(x => x.GEN_estadoPrevision.ToLower() == "activo");

            ddl.DataSource = lActivo;
            ddl.DataTextField = "GEN_nombrePrevision";
            ddl.DataValueField = "GEN_idPrevision";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("-Seleccione-", "0"));
        }

        public NegGEN_Prevision()
        {
        }
        public NegGEN_Prevision(int iGEN_idPrevision)
        {
            ConexionSQL con = new ConexionSQL();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Prevision WHERE GEN_idPrevision = @id");
            //sb.AppendLine("SELECT * FROM GEN_Sexo WHERE GEN_idSexo = " + iGEN_idSexo.ToString());
            string[][] sArray = new string[][] { new string[] { "@id", iGEN_idPrevision.ToString() }/*, new string[] { "", "" } */};

            DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
            if (dataTable.Rows.Count > 0)
            {
                DataRow dtr = dataTable.Rows[0];
                GEN_idPrevision = int.Parse(dtr["GEN_idPrevision"].ToString().Trim());
                GEN_nombrePrevision = dtr["GEN_nombrePrevision"].ToString().Trim();
                GEN_estadoPrevision = dtr["GEN_estadoPrevision"].ToString().Trim();
            }
        }

        //public void GetCombo(ref DropDownList ddl)
        //{
        //    ddl.Items.Clear();
        //    ddl.Items.Add(new ListItem("-Seleccione-", "0"));
        //    ConexionSQL con = new ConexionSQL();

        //    StringBuilder sb = new StringBuilder("SELECT GEN_idPrevision, GEN_nombrePrevision FROM GEN_Prevision");
        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable(sb.ToString());

        //    foreach (DataRow dtr in dt.Rows)
        //        ddl.Items.Add(new ListItem(dtr["GEN_nombrePrevision"].ToString().Trim(), dtr["GEN_idPrevision"].ToString().Trim()));
        //}
    }
}