﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using static RegistroClinico.WebApi;

namespace RegistroClinico
{
    public class NegGEN_Paciente : GEN_Paciente
    {
        public NegGEN_Paciente()
        {
        }

        public NegGEN_Paciente(int iGEN_idPaciente)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/WebApi/api/GEN_Paciente/" + iGEN_idPaciente.ToString());
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            Paciente jsonList = JsonConvert.DeserializeObject<Paciente>(sRes, settings);
            {
                Paciente p = jsonList;
                char cChar;
                DateTime dRes;

                GEN_idPaciente = p.GEN_idPaciente;
                GEN_numero_documentoPaciente = p.GEN_numero_documentoPaciente;
                if (char.TryParse(p.GEN_digitoPaciente, out cChar))
                    GEN_digitoPaciente = cChar;
                GEN_nombrePaciente = p.GEN_nombrePaciente;
                GEN_ape_paternoPaciente = p.GEN_ape_paternoPaciente;
                GEN_ape_maternoPaciente = p.GEN_ape_maternoPaciente;
                GEN_dir_callePaciente = p.GEN_dir_callePaciente;
                GEN_dir_numeroPaciente = p.GEN_dir_numeroPaciente;
                GEN_dir_ruralidadPaciente = p.GEN_dir_ruralidadPaciente;
                GEN_idPais = p.GEN_idPais;
                GEN_idRegion = p.GEN_idRegion;
                GEN_idComuna = p.GEN_idComuna;
                GEN_idCiudad = p.GEN_idCiudad;
                GEN_telefonoPaciente = p.GEN_telefonoPaciente;
                GEN_idSexo = p.GEN_idSexo;
                if (DateTime.TryParse(p.GEN_fec_nacimientoPaciente, out dRes))
                    GEN_fec_nacimientoPaciente = dRes;
                GEN_otros_fonosPaciente = p.GEN_otros_fonosPaciente;
                GEN_emailPaciente = p.GEN_emailPaciente;
                GEN_idPrevision = p.GEN_idPrevision;
                GEN_idPrevision_Tramo = p.GEN_idPrevision_Tramo;
                GEN_nuiPaciente = p.GEN_nuiPaciente;
                GEN_pac_pac_numeroPaciente = p.GEN_pac_pac_numeroPaciente;
                GEN_idIdentificacion = p.GEN_idIdentificacion;
                if (DateTime.TryParse(p.GEN_fec_actualizacionPaciente, out dRes))
                    GEN_fec_actualizacionPaciente = dRes;
                GEN_PraisPaciente = p.GEN_PraisPaciente;
                GEN_estadoPaciente = p.GEN_estadoPaciente;
                if (DateTime.TryParse(p.GEN_fecha_fallecimientoPaciente, out dRes))
                    GEN_fecha_fallecimientoPaciente = dRes;
            }
        }



        //public NegGEN_Paciente(int iGEN_idPaciente)
        //{
        //    ConexionSQL con = new ConexionSQL();

        //    StringBuilder sb = new StringBuilder();
        //    sb.AppendLine("SELECT * FROM GEN_Paciente WHERE GEN_idPaciente = @id");
        //    //sb.AppendLine("SELECT * FROM GEN_Paciente WHERE GEN_idPaciente = " + iGEN_idPaciente.ToString());
        //    string[][] sArray = new string[][] { new string[] { "@id", iGEN_idPaciente.ToString() }/*, new string[] { "", "" } */};

        //    DataTable dataTable = con.GetDataTable(sb.ToString(), sArray);
        //    if (dataTable.Rows.Count > 0)
        //    {
        //        DataRow dtr = dataTable.Rows[0];
        //        {
        //            int iRes;
        //            char cChar;
        //            DateTime dRes;

        //            GEN_idPaciente = int.Parse(dtr["GEN_idPaciente"].ToString().Trim());
        //            GEN_numero_documentoPaciente = dtr["GEN_numero_documentoPaciente"].ToString().Trim();
        //            if (char.TryParse(dtr["GEN_digitoPaciente"].ToString().Trim(), out cChar))
        //                GEN_digitoPaciente = char.Parse(dtr["GEN_digitoPaciente"].ToString().Trim());
        //            GEN_nombrePaciente = dtr["GEN_nombrePaciente"].ToString().Trim();
        //            GEN_ape_paternoPaciente = dtr["GEN_ape_paternoPaciente"].ToString().Trim();
        //            GEN_ape_maternoPaciente = dtr["GEN_ape_maternoPaciente"].ToString().Trim();
        //            GEN_dir_callePaciente = dtr["GEN_dir_callePaciente"].ToString().Trim();
        //            GEN_dir_numeroPaciente = dtr["GEN_dir_numeroPaciente"].ToString().Trim();
        //            GEN_dir_ruralidadPaciente = dtr["GEN_dir_ruralidadPaciente"].ToString().Trim();
        //            if (int.TryParse(dtr["GEN_idPais"].ToString().Trim(), out iRes))
        //                GEN_idPais = int.Parse(dtr["GEN_idPais"].ToString().Trim());
        //            if (int.TryParse(dtr["GEN_idRegion"].ToString().Trim(), out iRes))
        //                GEN_idRegion = int.Parse(dtr["GEN_idRegion"].ToString().Trim());
        //            if (int.TryParse(dtr["GEN_idComuna"].ToString().Trim(), out iRes))
        //                GEN_idComuna = int.Parse(dtr["GEN_idComuna"].ToString().Trim());
        //            if (int.TryParse(dtr["GEN_idCiudad"].ToString().Trim(), out iRes))
        //                GEN_idCiudad = int.Parse(dtr["GEN_idCiudad"].ToString().Trim());
        //            GEN_telefonoPaciente = dtr["GEN_telefonoPaciente"].ToString().Trim();
        //            if (int.TryParse(dtr["GEN_idSexo"].ToString().Trim(), out iRes))
        //                GEN_idSexo = int.Parse(dtr["GEN_idSexo"].ToString().Trim());
        //            if (DateTime.TryParse(dtr["GEN_fec_nacimientoPaciente"].ToString().Trim(), out dRes))
        //                GEN_fec_nacimientoPaciente = dRes;
        //            GEN_otros_fonosPaciente = dtr["GEN_otros_fonosPaciente"].ToString().Trim();
        //            GEN_emailPaciente = dtr["GEN_emailPaciente"].ToString().Trim();
        //            if (int.TryParse(dtr["GEN_idPrevision"].ToString().Trim(), out iRes))
        //                GEN_idPrevision = iRes;
        //            if (int.TryParse(dtr["GEN_idPrevision_Tramo"].ToString().Trim(), out iRes))
        //                GEN_idPrevision_Tramo = iRes;
        //            if (int.TryParse(dtr["GEN_nuiPaciente"].ToString().Trim(), out iRes))
        //                GEN_nuiPaciente = iRes;
        //            if (int.TryParse(dtr["GEN_pac_pac_numeroPaciente"].ToString().Trim(), out iRes))
        //                GEN_pac_pac_numeroPaciente = iRes;
        //            GEN_idIdentificacion = int.Parse(dtr["GEN_idIdentificacion"].ToString().Trim());
        //            if (DateTime.TryParse(dtr["GEN_fec_actualizacionPaciente"].ToString().Trim(), out dRes))
        //                GEN_fec_actualizacionPaciente = DateTime.Parse(dtr["GEN_fec_actualizacionPaciente"].ToString().Trim());
        //            GEN_PraisPaciente = dtr["GEN_PraisPaciente"].ToString().Trim();
        //            GEN_estadoPaciente = dtr["GEN_estadoPaciente"].ToString().Trim();
        //            if (DateTime.TryParse(dtr["GEN_fecha_fallecimientoPaciente"].ToString().Trim(), out dRes))
        //                GEN_fecha_fallecimientoPaciente = dRes;
        //        }
        //    }
        //}

        public bool GetPacienteRut(string sRut, int iIdentificacion = 1)
        {
            WebClient WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

            string sRes = WebClient.DownloadString("http://10.6.180.216/webapi/api/GEN_Paciente/BuscarporDocumento?GEN_idIdentificacion=1&GEN_numero_documentoPaciente=" + sRut.ToString());
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Paciente> pList = JsonConvert.DeserializeObject<List<Paciente>>(sRes, settings);
            if (pList.Count > 0)
            {
                DateTime dRes;
                char cRes;
                Paciente p = pList[0];
                GEN_idPaciente = p.GEN_idPaciente;
                GEN_numero_documentoPaciente = p.GEN_numero_documentoPaciente;
                if (char.TryParse(p.GEN_digitoPaciente, out cRes))
                    GEN_digitoPaciente = cRes;
                GEN_nombrePaciente = p.GEN_nombrePaciente;
                GEN_ape_paternoPaciente = p.GEN_ape_paternoPaciente;
                GEN_ape_maternoPaciente = p.GEN_ape_maternoPaciente;
                GEN_dir_callePaciente = p.GEN_dir_callePaciente;
                GEN_dir_numeroPaciente = p.GEN_dir_numeroPaciente;
                GEN_dir_ruralidadPaciente = p.GEN_dir_ruralidadPaciente;
                GEN_idPais = p.GEN_idPais;
                GEN_idRegion = p.GEN_idRegion;
                GEN_idComuna = p.GEN_idComuna;
                GEN_idCiudad = p.GEN_idCiudad;
                GEN_telefonoPaciente = p.GEN_telefonoPaciente;
                GEN_idSexo = p.GEN_idSexo;
                if (DateTime.TryParse(p.GEN_fec_nacimientoPaciente, out dRes))
                    GEN_fec_nacimientoPaciente = dRes;
                GEN_otros_fonosPaciente = p.GEN_otros_fonosPaciente;
                GEN_emailPaciente = p.GEN_emailPaciente;
                GEN_idPrevision = p.GEN_idPrevision;
                GEN_idPrevision_Tramo = p.GEN_idPrevision_Tramo;
                GEN_nuiPaciente = p.GEN_nuiPaciente;
                GEN_pac_pac_numeroPaciente = p.GEN_pac_pac_numeroPaciente;
                GEN_idIdentificacion = p.GEN_idIdentificacion;
                if (DateTime.TryParse(p.GEN_fec_actualizacionPaciente, out dRes))
                    GEN_fec_actualizacionPaciente = dRes;
                GEN_PraisPaciente = p.GEN_PraisPaciente;
                GEN_estadoPaciente = p.GEN_estadoPaciente;
                if (DateTime.TryParse(p.GEN_fecha_fallecimientoPaciente, out dRes))
                    GEN_fecha_fallecimientoPaciente = dRes;
                return true;
            }
            return false;
        }
    }
}