﻿
$(document).ready(function () {

    if (getSession().ES_CAMBIO_CONTRASEÑA === "true")
        $("#pnl_menuModulos a").removeAttr("href");
    
    var exp = new RegExp("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,16})$");

    $('#txtContraseñaNueva').on('input', function () {

        var parent = $('#txtContraseñaNueva').parent();
        parent.next(".color-error").remove();
        parent.next(".color-success").remove();

        if ($('#txtContraseñaNueva').val() !== "") {

            if (exp.test(this.value)) {
                $('#txtContraseñaNueva').removeClass("invalid-input");
                parent.after("<label class='ml-5 mb-1 mt-1 color-success'><i class='fa fa-check'> Clave Correcta.</label>");
            } else {
                $('#txtContraseñaNueva').addClass("invalid-input");
                parent.after("<label class='ml-5 mb-1 mt-1 color-error'><i class='fa fa-times'> La clave no cumple con los requisitos mínimos.</label>");
            }

        } else $('#txtContraseñaNueva').removeClass("invalid-input");
    });

    $("#txtContraseñaNuevaRepetida").on('input', function () {

        var parent = $('#txtContraseñaNuevaRepetida').parent();
        parent.next(".color-error").remove();
        parent.next(".color-success").remove();

        if ($("#txtContraseñaNuevaRepetida").val() !== '') {
            if ($("#txtContraseñaNuevaRepetida").val() === $('#txtContraseñaNueva').val()) {
                $('#txtContraseñaNuevaRepetida').removeClass("invalid-input");
                parent.after("<label class='ml-5 mb-1 mt-1 color-success'><i class='fa fa-check'> Las claves coinciden</label>");
            } else {
                $('#txtContraseñaNuevaRepetida').addClass("invalid-input");
                parent.after("<label class='ml-5 mb-1 mt-1 color-error'><i class='fa fa-times'> Las claves no coinciden</label>");
            }
        } else $('#txtContraseñaNuevaRepetida').removeClass("invalid-input");

    });

    $("#txtContraseñaActual").on('input', function () {
        $('#txtContraseñaActual').removeClass("invalid-input");
        var parent = $('#txtContraseñaActual').parent();
        parent.next(".color-error").remove();
        parent.next(".color-success").remove();
    });

    $("#btnCambiarContraseña").click(function () {

        if (validarCampos("#divReinicioContraseña", false) && exp.test($('#txtContraseñaNueva').val()) &&
            ($("#txtContraseñaNuevaRepetida").val() === $('#txtContraseñaNueva').val())) {

            var parent = $('#txtContraseñaActual').parent();
            parent.next(".color-error").remove();
            parent.next(".color-success").remove();

            var sSession = getSession();
            var json = {
                "GEN_claveUsuariosNueva": '' + CryptoJS.MD5($("#txtContraseñaNueva").val()),
                "GEN_claveUsuariosAntigua": '' + CryptoJS.MD5($('#txtContraseñaActual').val())
            };

            $.ajax({
                type: 'PUT',
                url: `${GetWebApiUrl()}GEN_Usuarios/${sSession.ID_USUARIO}/CambiarClave`,
                data: JSON.stringify(json),
                contentType: "application/json",
                dataType: "json",
                async: false,
                success: function (data, status, jqXHR) {

                    toastr.success('La contraseña se ha actualizado correctamente.');

                    if (sSession.ES_CAMBIO_CONTRASEÑA === "true") {

                        setSession("ES_CAMBIO_CONTRASEÑA", false);
                        let sMenu = (sSession.nMODULO.indexOf(",") === -1) ?
                                        parseInt(sSession.nMODULO) :
                                        parseInt(sSession.nMODULO.split(",")[0]);
                        let sRuta = GetUrlRedirect(sMenu);
                        window.location.replace(sRuta);

                    }

                },
                error: function (jqXHR, status) {
                    if (jqXHR.status === 409) {
                        $('#txtContraseñaActual').addClass("invalid-input");
                        parent.after(`<label class='ml-5 mb-1 mt-1 color-error'><i class='fa fa-times'> ${JSON.parse(jqXHR.responseText).Message}</label>`);
                    }
                }
            });

        }

        return false;

    });

    ShowModalCargando(false);
    
});
