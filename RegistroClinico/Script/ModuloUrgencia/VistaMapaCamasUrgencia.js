﻿


$(document).ready(function () {
    ShowModalCargando(false);
    sSession = getSession();
    //RevisarAcceso(false, sSession);
    //Editar después para que distintos perfiles tengan acceso
    //Carga tipo de atencion: matron, adulto, pediatrico
    cargarComboTipoAtencion();
    //cargarCamasUrgencia(true);
    $("#btnBuscarCamas").on('click', function () {
        let idTipoBusqueda = $("#selectTipoAtencion").val();
        let esValido = validarCampos(("#DivSelectTipoAtencion"), $("#selectTipoAtencion"));
        if (esValido)
            $("#infoInicial").empty();
            cargarCamasUrgencia(true, idTipoBusqueda);
    })
});
function cargarCamasUrgencia(esCargaInicial, idBusqueda) {
    if (idBusqueda) {
        var htmlError = "<h1 class='text-center w-100'><b><i class='fa fa-info-circle'></i>No hay información encontrada</b></h1>";
        var arrayBoxesPadre = [];
        var arrayBoxeshijo = [];
        var url = GetWebApiUrl() + "URG_Atenciones_Urgencia/Mapa/" + idBusqueda;
        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (data, status, jqXHR) {
                //Extrae las camas de y crea un arreglo independiente.
                arrayBoxeshijo = data.map((e) => {
                    return e['Box'];
                });
                //dibuja los DAU de atencion
                printBoxes(data);
                //Dibujar las camas de cada DAU de atencion
                printCamas(data, arrayBoxeshijo);

            },
            error: function (jqXHR, status) {
                console.log("Error al Cargar Mapa de Camas: " + JSON.stringify(jqXHR));
                $("#divGeneralMapaCama").html(htmlError);
            }
        });
    }
}


function printBoxes(data) {
    
    var boxesAtencion = "";
    var boxesHijos = "";
    data.map((d) => {

        let camasOcupadas = 0;
        let camasDisponibles = 0;
        let camasDeshabilitadas = 0;
        let camasBloqueadas = 0;

        for (var q in d.Box) {
            
            
            switch (d.Box[q].IdTipoEstado) {
                case 84:
                    camasDisponibles++;

                    break;
                case 85:
                        camasOcupadas++;
                    break;
                case 86:
                        camasBloqueadas++;
                    break;
            }
        }
        boxesAtencion += `
                        <div class="col-md-5  m-3" >
                            <div class="card card-primary " >
                                <div class='card-header ' style="cursor:pointer;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4>
                                             <a
                                                 id="boxPadre`+ d.Id + `"
                                                 data-toggle="collapse"
                                                 href="#BoxHijo`+ d.Id + `"
                                                 aria-expanded="false"
                                                 aria-controls="collapseOne">` + d.Valor + `
                                              </a>
                                        </h4>
                                    </div>
                                    <!--*****conteo de camas******-->

                                    <div class="col-md-6 d-flex flex-row-reverse">
                                        <div class="d-inline">
                                            Disponibles: <span class="badge badge-success">${camasDisponibles}</span> |
                                            Ocupadas: <span class="badge badge-ocupada">${camasOcupadas}</span> |
                                            Bloqueadas: <span class="badge badge-bloqueada">${camasBloqueadas}</span> |
                                            Deshabilitadas: <span class="badge badge-deshabilitada">${camasDeshabilitadas}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- ***Fin conteo de camas ***-->
                                </div>
                                <div class="card-body  collapse" id="BoxHijo`+ d.Id + `" >
                                    
                                </div>
                            </div>
                        </div>
                        `;
            $("#ContCamasUrg").html(boxesAtencion);
 
    });
    
}


function printCamas(data, arrayBoxeshijos) {
    //Obtener sesión
    let SesionActual = getSession();
    let idPerfil = SesionActual.ID_PERFIL;
    let deshabilitarBoton = "";
    var codigo;
    var tipoPill;
    var color = "black";
    //Boolean si tiene categorización
    let categorizacion = true; 
    //Verificar que es enfermero urgencia o tens de urgencia

    if (idPerfil != 190 && idPerfil != 191 && idPerfil != 149) {
        var disabled = "disabled";
        deshabilitarBoton = "d-none";
    }
    var fechaActual = GetFechaActual();
    var htmlSinInfo = "<h1 class='text-center w-100'><b><i class='fa fa-info-circle'></i> Sin Información</b></h1>";
    let icon = "";
    let clase = "";
    if (arrayBoxeshijos) {
        for (var i in arrayBoxeshijos) {
            let nombrePadre = "";
            let idPadre = "";
            nombrePadre = data[i].Valor;
            idPadre = data[i].Id;
            boxesHijos = "";
            
            for (var a in arrayBoxeshijos[i]) {
                //Gestionar el estado de las camas
                switch (arrayBoxeshijos[i][a].IdTipoEstado) {
                    // Cama Disponible
                    case 84:
                        icon = "fa fa-check-circle";
                        clase = "cama disponible";
                        break;
                    // Cama Ocupada
                    case 85:
                        icon = "fa fa-user-circle";
                        clase = "cama ocupada";
                        break;
                    // Cama Bloqueada
                    case 86:
                        icon = "fa fa-lock";
                        clase = "cama bloqueada";
                        break;
                    // Cama Deshabilitada
                    case 99:
                        icon = "fa fa-ban";
                        clase = "cama deshabilitada";
                        break;
                    // Sin Estado
                    default:
                        icon = "fa fa-question-circle";
                        card = "card-sin-estado";
                        break;

                }
                //Categorias
                arrayBoxeshijos[i][a].Paciente.map((e) => {
                    if (e.Categorizacion.length > 0) {
                        switch (e.Categorizacion[0].Codigo) {
                            case "C1":
                                color = "#dc3545";
                                codigo = e.Categorizacion[0].Codigo;
                                tipoPill = "danger";
                                break
                            case "C2":
                                color = "#fd7e14";
                                codigo = e.Categorizacion[0].Codigo;
                                tipoPill = "orange";
                                break
                            case "C3":
                                color = "#ffc107";
                                codigo = e.Categorizacion[0].Codigo;
                                tipoPill = "warning";
                                break
                            case "C4":
                                color = "#28a745";
                                codigo = e.Categorizacion[0].Codigo;
                                tipoPill = "success";
                                break
                            case "C5":
                                color = "#17a2b8";
                                codigo = e.Categorizacion[0].Codigo;
                                tipoPill = "info";
                                break
                            default:
                                color = "black";
                                codigo = "SC";
                                tipoPill = "default";
                                break
                        }
                    } else {
                        color = "black";
                        codigo = "SC";
                        tipoPill = "default";
                        categorizacion = false;
                    }
                })

                //Dibuja las camas
                boxesHijos += ` <div class="m-2" style="float:left;">
                                    <div class="">
                                        <strong> ${arrayBoxeshijos[i][a].Valor}</strong>
                                    </div>
                                    <button type="button" style="border:none;  background-color:white;" data-toggle="modal" data-target="#idModalPaciente_${(i + 1) + "" + arrayBoxeshijos[i][a].Id}">
                                    <div id='divCama_${arrayBoxeshijos[i][a].Id}'>
                                    <a id='aCama_${arrayBoxeshijos[i][a].Id}' class='cama-a'>
                                        <div class='${clase}' >
                                            <img src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                            <i id='iCama_${arrayBoxeshijos[i][a].Id}' class='fa ${icon}' style='z-index:1;  ${arrayBoxeshijos[i][a].IdTipoEstado==85 ? `color:${color}`: ""}'></i>
                                        </div>
                                     </a>
                                     </div>
                                    </button>
                                </div>
                    `;
                //Comprobar si la cama tiene paciente, true = tiene paciente.
                var comprobar = arrayBoxeshijos[i][a].IdTipoEstado == 84 ? false : true;
                //Cama disponible
                if (!comprobar) {
                    //Dibuja modales sin información (cuando la cama está disponible)
                    boxesHijos += ` <!--Modal -->
                    <div class="modal fade"  id="idModalPaciente_${(i + 1) + "" + arrayBoxeshijos[i][a].Id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style="background-color:lightyellow;">
                                    <h5 class="modal-title" id="exampleModalLongTitle"><strong>${nombrePadre} | ${arrayBoxeshijos[i][a].Valor} </strong></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5>Cama disponible, no registra información de paciente</h5>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                </div>
                            </div>
                        </div>
                    </div>`;
                } else {
                    
                    
                    //Dibuja modal con la informaci+on del paciente
                    for (var s in arrayBoxeshijos[i][a]['Paciente']) {

                        let nombrePaciente = arrayBoxeshijos[i][a]['Paciente'][s].Paciente.Nombre + " " + arrayBoxeshijos[i][a]['Paciente'][s].Paciente.ApellidoPaterno + " " + arrayBoxeshijos[i][a]['Paciente'][s].Paciente.ApellidoMaterno;
                        var fechaIngreso = "";
                        //Manejo de fechas y tiempos transcurridos
                        if (arrayBoxeshijos[i][a]['Paciente'][s].Atencion.Fecha != null) {
                            fechaIngreso = arrayBoxeshijos[i][a]['Paciente'][s].Atencion.Fecha;
                            fechaIngreso = moment(fechaIngreso);
                            tiempoTranscurridoHoras = Math.abs(fechaIngreso.diff(fechaActual, 'hours'));
                            tiempoTranscurridoDias = Math.abs(fechaIngreso.diff(fechaActual, 'days'));
                            fechaIngreso = fechaIngreso.format("DD-MM-YY HH:mm");
                        } else {
                            tiempoTranscurridoDias = 0
                            tiempoTranscurridoHoras = 0
                            fechaIngreso = " No se registro fecha de ingreso";
                        }
                        //calcular edad
                        let fechaNac = moment(arrayBoxeshijos[i][a]['Paciente'][s].Paciente.FechaNacimiento);
                        let edadPaciente = CalcularEdad(moment(fechaNac).format("YYYY-MM-DD")).edad;
                        

                        //Acceso a acompañante
                        let telefonoAcompañante;
                        let rutAcompañante;
                        let nombreAcompañante;
                        var htmlAcomp = "";
                        let acompañante = true;
                        //No registra acompañantes en la ultima atención
                        if (arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.IdPersona == null) {
                            htmlAcomp = "<p>No se ha registrado información de acompañante</p>";
                            acompañante = false;
                        } else {
                            //Si tiene acompañante
                            //Valida nombre
                            if (arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Nombre) {
                                nombreAcompañante = arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Nombre +" "+ arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.ApellidoPaterno +" "+ arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.ApellidoMaterno;
                            } else {
                                nombreAcompañante = "No registro nombre el acompañante";
                            }
                            //rut invalido
                            if (arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.NumeroDocumento == undefined || arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Digito == undefined) {
                                rutAcompañante = "No registro numero documento";
                            } else {
                                rutAcompañante = arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.NumeroDocumento +"-"+ arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Digito;
                            }
                            //Valida telefono
                            if (arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Telefono) {
                                telefonoAcompañante = arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Telefono;
                                
                            } else {
                                telefonoAcompañante = "No registro telefono";
                            }
                            
                        }
                        boxesHijos += ` <!--Modal -->
                    <div class="modal fade"  id="idModalPaciente_${(i + 1) + "" + arrayBoxeshijos[i][a].Id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style="background-color:lightyellow;">
                                    <h5 class="modal-title col-md-9" id="exampleModalLongTitle">
                                    <strong> ${nombrePadre} | ${arrayBoxeshijos[i][a].Valor} </strong> 
                                    </h5>

                                    <span class="col-md-2 mt-1 badge badge-pill bg-${tipoPill} badge-count" style="font-size:1.1em; float:rigth;">${codigo}</span>
                                    <button type="button" class="close col-md-1 mr-1" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <strong><p>Nombre paciente: ${nombrePaciente} </p></strong>
                                    ${arrayBoxeshijos[i][a]['Paciente'][s].Paciente.NombreSocial ? `<p><b>Nombre Social:</b> ${arrayBoxeshijos[i][a]['Paciente'][s].Paciente.NombreSocial}</p>` : ""}
                                    <p><b>Numero de documento:</b> ${arrayBoxeshijos[i][a]['Paciente'][s].Paciente.NumeroDocumento}-${arrayBoxeshijos[i][a]['Paciente'][s].Paciente.Digito}</p>
                                    <p><b>Edad: </b> ${edadPaciente} </p>
                                    <p><b>Genero :</b> ${arrayBoxeshijos[i][a]['Paciente'][s].Paciente.NombreGenero}</p>
                                    <p><b>Estado del paciente:</b> ${arrayBoxeshijos[i][a]['Paciente'][s].Atencion.NombreTipoEstado} </p>
                                    ${arrayBoxeshijos[i][a]['Paciente'][s].Atencion.MotivoAtencion ? ` <p><b>Estado del paciente:</b> ${arrayBoxeshijos[i][a]['Paciente'][s].Atencion.MotivoAtencion} </p>` : ""}
                                    <p><b>Fecha de ingreso: </b>${fechaIngreso} </p>
                                    <p><b>Tiempo atención transcurrido:</b> En días: ${tiempoTranscurridoDias} días, En horas: ${tiempoTranscurridoHoras} horas</p>
                                    <h5><b>Información de acompañante</b></h5>
                                    ${acompañante == false ? htmlAcomp : ""}
                                    <div id='divInfoAcomp' class="${acompañante == false ? "d-none" : ""}">
                                    <p><b>Nombre acompañante:</b> ${nombreAcompañante}</p>
                                    <p><b>Rut acompañante:</b> ${rutAcompañante}</p>
                                    <p><b>Telefono acompañante: </b>${telefonoAcompañante}</p>
                                    ${arrayBoxeshijos[i][a]['Paciente'][s].TipoAcompañante.Valor ? `<p><b>Tipo acompañante: </b>${arrayBoxeshijos[i][a]['Paciente'][s].TipoAcompañante.Valor}</p>` : ""}
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="button" ${disabled} id="botonMdlEnviaraBox"  class="btn btn-primary ${deshabilitarBoton}" data-id-categorizacion='${categorizacion ? arrayBoxeshijos[i][a]['Paciente'][s].Categorizacion.Codigo : ""}' data-nombrePaciente="${nombrePaciente}" data-dismiss="modal" onClick='llenarModalBox(this)'  data-id='${arrayBoxeshijos[i][a]['Paciente'][s].Atencion.Id}' data-toggle="modal" data-target="#mdlPasaraBox">Movimiento de paciente</button>
                                </div>
                            </div>
                        </div>
                    </div>`;

                        //Información del acompañante

                        //$("#divInfoAcomp").append(htmlAcomp);
                    }
                    
                    
                }
                
                
            }
            
            //comprobacion de que existan camas

            //si no hay camas, escribe mensaje "sin información"
            arrayBoxeshijos[i].length > 0 ? $("#BoxHijo" + idPadre).html(boxesHijos) : $("#BoxHijo" + idPadre).html(htmlSinInfo);

        }
    }
    
}

function llenarModalBox(e) {
    
    linkAsignarBox(e);
    //Cargar combo tipo atencion
    
}
