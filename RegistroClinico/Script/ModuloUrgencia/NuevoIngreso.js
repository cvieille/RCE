﻿
var ingresoUrgencia = {}, profesional = {}, niSession = null;
var medicamentosAtencion = [], tipoGuardado = null;
const TIPO_GUARDADO = { Medico: 1, Admision: 16, AtencionClinica: 23 }; 

var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
};

$(document).ready(function () {

    // Hay que revisar el acceso a este formulario
    // RevisarAcceso(false, niSession); 
    const IniciarComponentes = async () => {
        ShowModalCargando(true);
        ValidarPerfiles();
        CargarCombos();
        CargarIngresoUrgencia();
    }

    IniciarComponentes().then(() => ShowModalCargando(false));

});

// INICIAR COMPONENTES
function ValidarPerfiles() {

    niSession = getSession();
    InicializarComponentes();

    // 1 = Médico
    // 25 = Matronería Urgencia / Se valida si es un ingreso nuevo (Matroneria tiene permisos para ser admisión)
    if (niSession.CODIGO_PERFIL == 1 || (niSession.CODIGO_PERFIL == 25 && niSession.ID_INGRESO_URGENCIA != undefined)) { // 1 = MÉDICO

        tipoGuardado = TIPO_GUARDADO.Medico;
        InicializarDatosClinicos();
        $("#aMedicamentoBox").on('click', (e) => { modalInsumosMedicamentos(); });
        $("#aLaboratorio").on('click', (e) => { modalExamenesLaboratorio(true); });
        $("#aInterconsultor").on('click', (e) => { CargarInterconsultistas(); });
        $("#aImagenologia").on('click', (e) => { modalExamenesImagenologia(true) });
        

    } else if (niSession.CODIGO_PERFIL == 16 || niSession.CODIGO_PERFIL == 25) { // 16 = ADMISIÓN URGENCIA
        tipoGuardado = TIPO_GUARDADO.Admision;
        $(`#liDatosClinicos, #liDatosAtencionClinica, #divDatosAtencionAdm .aSiguienteIngresoUrgencia, #divAtencionClinica`).hide();
    }

    if (localStorage.getItem("ES_ATENCION_CLINICA")) { // 22 = ENFERMERA DE URGENCIA

        tipoGuardado = TIPO_GUARDADO.AtencionClinica;
        InicializarDatosAtencionClinica();
        if (niSession.CODIGO_PERFIL == 22)
            $("#aMedicamentoBox, #aLaboratorio, #aInterconsultor").css("cursor", "not-allowed");
        localStorage.removeItem("ES_ATENCION_CLINICA");

    }

    InicializarDatosPaciente();
    InicializarDatosAdministrativos();

}
function InicializarComponentes() {
    $('#txtFechaLlegada').val(moment(GetFechaActual()).format('DD-MM-YYYY HH:mm:ss'));
    $("textarea").val("");
    ActivarAcompañante();
    $("#txtnumerotPac").on('input', function () {
        $("#sltTipoAtencion").val('0');
    });
}
function InicializarDatosPaciente() {

    var html = $("#divPrevision").html();
    $("#divPrevision").remove();
    $("#divDatosPaciente .card-body").append(`<div id='divPrevision'>${html}</div>`);
    $("#sltProvincia, #sltCiudad").attr("disabled", "disabled");
    $('#sltPrevision').change(function (e) {
        comboPrevisionTramo(parseInt($('#sltPrevision').val()));
        $('#sltPrevisionTramo').val('0');
    });
    DeshabilitarPaciente(true);

    $(".show-more-body button").on('click', function () {
        let parent = $(this).parent().parent();
        if (parent.hasClass("show-more")) {
            parent.removeClass("show-more").addClass("show-less");
            $(this).html("<i class='fas fa-arrow-up'></i> Ver menos");
        } else {
            parent.removeClass("show-less").addClass("show-more");
            $(this).html("<i class='fas fa-arrow-down'></i> Ver más");
        }
    });
}
function InicializarDatosAdministrativos() {

    $('#sltTipoAccidente').change(function (e) {

        if ($(this).val() == '0') {
            $('#lblErrorAccidente').hide();
            $('#txtLugarAccidente').attr('disabled', true).val('');
            $('#txtLugarAccidente').attr('data-required', false);
        } else {
            $('#lblErrorAccidente').show();
            $('#txtLugarAccidente').removeAttr('disabled');
            $('#txtLugarAccidente').attr('data-required', true);
        }

        ReiniciarRequired();

    });
    $('.aGuardarIngresoUrgencia').click(function (e) {
        $(this).prop("disabled", true);
        GuardarDAU();
        e.preventDefault();
    });
    $(".aAtrasIngresoUrgencia").click(function () {

        var before = $(`${$(this).attr("data-li")}`).prev();
        $(`#${$(before).attr("id")} a`).trigger("click");

        if (!$(`#${$(before).attr("id")}`).is(":hidden"))
            $(`#${$(before).attr("id")} a`).trigger("click");
        else {
            while ($(`#${$(before).attr("id")}`).is(":hidden")) {
                before = $(`#${$(before).attr("id")}`).prev();
                if (!$(`#${$(before).attr("id")}`).is(":hidden")) {
                    $(`#${$(before).attr("id")} a`).trigger("click");
                    break;
                } else
                    break;
            }
        }

    });
    $(".aSiguienteIngresoUrgencia").click(function () {

        var next = $(`${$(this).attr("data-li")}`).next();

        if (!$(`#${$(next).attr("id")}`).is(":hidden"))
            $(`#${$(next).attr("id")} a`).trigger("click");
        else {
            while ($(`#${$(next).attr("id")}`).is(":hidden")) {
                next = $(`#${$(next).attr("id")}`).next();
                if (!$(`#${$(next).attr("id")}`).is(":hidden")) {
                    $(`#${$(next).attr("id")} a`).trigger("click");
                    break;
                }
            }
        }

    });
    $('#sltTipoAtencion').change(function (e) {

        $("#divDatosGinecoObstetrico").hide();
        $(`#txtUltimaRegla,#txtAlturaUterinaObstetrico,#txtLatidosCardioFetalesObstetrico,
            #txtTactoVaginalObstetrico,#txtEspeculoscopiaObstetrico,#txtEcografiaObstetrico,
            #txtActividadUterinaObstetrico`).attr("data-required", "false");

        if ($(this).val() == '3') { // Pediatrico
            if (!$("#chkAcompañante").bootstrapSwitch("state")) {
                $("#chkAcompañante").bootstrapSwitch("disabled", false);
                $("#chkAcompañante").bootstrapSwitch("state", true);
                $('#sltTipoAtencion').data("value-previous", $(this).val());
            }
            $("#chkAcompañante").bootstrapSwitch("disabled", true);
        } else {
            if ($('#sltTipoAtencion').data("value-previous") == '3') {
                $("#chkAcompañante").bootstrapSwitch("disabled", false);
                $("#chkAcompañante").bootstrapSwitch("state", false);
                $('#sltTipoAtencion').data("value-previous", $(this).val());
            }
            $("#chkAcompañante").bootstrapSwitch("disabled", ($.isEmptyObject(GetPaciente())));
        }


        $("#divSolicitudInterconsultor, #divExamenesImagenologia").hide();
        ReiniciarRequired();

    });

    if (tipoGuardado == TIPO_GUARDADO.Medico || tipoGuardado == TIPO_GUARDADO.AtencionClinica) {
        $('a[href="#divDatosClinicos"]').trigger("click");
    }

    ReiniciarRequired();

    ShowModalCargando(false);
}
function InicializarDatosClinicos() {

    $("#chkAlcoholemia").bootstrapSwitch('state', false);
    $("input.bootstrapSwitch").bootstrapSwitch();
    $("#divSignosVitales, #divDatosGinecoObstetrico, #tblProfesionDescripsion").hide();
    $(`#liDatosAtencionClinica, #divDatosAtencionAdm .aGuardarIngresoUrgencia, #divDatosClinicos .aSiguienteIngresoUrgencia`).hide();

    (tipoGuardado == TIPO_GUARDADO.Medico || tipoGuardado == TIPO_GUARDADO.AtencionClinica) ? $("#divMedico").show() : $("#divMedico").hide();
    $("#divAlcolemia").hide();

    $("#tblProfesionDescripcion, #divDatosGinecoObstetrico, #divDatosProfesionales").hide();
    $("#divProfesionDescripcion").show();

    $("textarea").val("");
    window.addEventListener('beforeunload', bunload, false);

    $("#aGuardarInterconsultor").data("id", "0");
    $("#divSolicitudInterconsultor, #divMedicamentos, #divDetalleAtencionInterconsultor, #tblMedicamentos").hide();
    $("#divProcedimientos, #divExamenesImagenologia, #divExamenesLaboratorio").hide();
    $("#aProcedimientos").on('click', (e) => { modalProcedimientos(true); });
    $("#chkAlcoholemia").on('switchChange.bootstrapSwitch', function (event, state) {

        if ($(this).bootstrapSwitch('state')) {
            var fechaHoy = GetFechaActual();
            $("#divAlcolemia").show();
            $("#txtNumeroFrasco, #txtFechaAlcoholemia, #txtHoraAlcoholemia").attr("data-required", true);
            $("#txtFechaAlcoholemia").val(moment(fechaHoy).format("YYYY-MM-DD"));
            $("#txtHoraAlcoholemia").val(moment(fechaHoy).format("HH:mm"));
        } else {
            $("#divAlcolemia").hide();
            $("#txtNumeroFrasco, #txtFechaAlcoholemia, #txtHoraAlcoholemia").attr("data-required", false);
        }

        ReiniciarRequired();

    });

}
function InicializarDatosAtencionClinica() {

    InicializarDatosClinicos();
    $("#chkAlcoholemia").bootstrapSwitch("disabled", true);
    $("#divMedico input, #divMedico select, #divMedico textarea").attr("disabled", "disabled");
    if (sSession.CODIGO_PERFIL == 1 || sSession.CODIGO_PERFIL == 25)
        $("#divDatosGinecoObstetrico input, #divDatosGinecoObstetrico select, #divDatosGinecoObstetrico textarea").removeAttr("disabled");
    $("#aProcedimientos").prop('disabled', true);
    
}

// Funcion valida la cantidad de palabras para no romper el pdf. Atencion urgencia... lugar del accidente( input), motivo consulta (input)
function checkWords(slt) {
    let numeroPalabrasValidar;
    //Reiniciar estado para hacer comprobaciones
    $(slt).removeClass("invalid-input")
    $(slt).next().remove();
    //Boton habilitado para guardar
    $("#aaGuardarIngresoUrgencia").attr('disabled', false);
    //Por cada 8 caracteres va requerir una palabra(Espacio)
    numeroPalabrasValidar = (parseInt(slt.value.length / 8));
    //Comprueba que nigun input tenga problemas de validación
    var hasClase = $("#txtMotivoConsulta").hasClass('invalid-input');
    var hasClase2 = $("#txtLugarAccidente").hasClass('invalid-input');



    textoAreaDividido = slt.value.split(" ");
    //Cuenta número de palabras
    numeroPalabras = textoAreaDividido.length;

    //El contenido no tiene mas de 3 palabras
    if (numeroPalabras < 3) {
        $(slt).addClass("invalid-input");
        if ($(slt).next().prop('nodeName') == undefined)
            $(slt).after(`<span class='invalid-input'>Se necesita más información, cantidad mínima de palabras:4.</span>`);
        $("#aaGuardarIngresoUrgencia").attr('disabled', true);
    } else {

        //Y se valida la cantidad de palabras
        if (numeroPalabras < numeroPalabrasValidar) {
            $(slt).addClass("invalid-input");
            if ($(slt).next().prop('nodeName') == undefined)
                $(slt).after(`<span class='invalid-input'>Formato inválido, separe las palabras con espacios en blanco, cantidad mínima de palabras: ${numeroPalabrasValidar}.</span>`);
            $("#aaGuardarIngresoUrgencia").attr('disabled', true);
        } else {
            //De lo contrario input y button habilitado  :D
            $(slt).next().remove();
            $(slt).removeClass("invalid-input");
            $("#aaGuardarIngresoUrgencia").attr('disabled', false);
        }
    }
    //problemas de validación =  disabled button
    if (hasClase == true || hasClase2 == true) {
        $("#aaGuardarIngresoUrgencia").attr('disabled', true);
    }
}

// COMBOS
function CargarCombos() {
    comboTipoAtencion();
    comboPrevision();
    comboPrevisionTramo(0);
    comboMediosTransporte();
    comboTiposAccidente();
    CombosServiciosSalud();
    comboEstablecimiento("1");
    CargarCombosUbicacionPaciente();
}
function comboTiposAccidente() {
    let url = `${GetWebApiUrl()}/URG_Tipos_Accidente/combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoAccidente'));
}
function comboMediosTransporte() {
    let url = `${GetWebApiUrl()}/URG_Medios_Transporte/combo`;
    setCargarDataEnCombo(url, false, $('#sltMedioTransporte'));
}
function comboPrevision() {
    let url = `${GetWebApiUrl()}GEN_Prevision/Combo`;
    setCargarDataEnCombo(url, false, $("#sltPrevision"));
}
function comboPrevisionTramo(GEN_idPrevision) {
    $('#sltPrevisionTramo').empty().append(`<option value='0'>Seleccione</option>`);

    if (GEN_idPrevision != 0) {
        let url = `${GetWebApiUrl()}GEN_Prevision_Tramo/GEN_idPrevision/${GEN_idPrevision}`;
        setCargarDataEnCombo(url, false, $("#sltPrevisionTramo"));
    }
}
function comboTipoAtencion() {
    let url = `${GetWebApiUrl()}URG_Tipo_Atencion/Combo`;
    setCargarDataEnCombo(url, false, "#sltTipoAtencion");
}
function CombosServiciosSalud() {
    let url = `${GetWebApiUrl()}GEN_Servicio_Salud/Combo`;
    setCargarDataEnCombo(url, false, "#sltServicioSalud");
    $("#sltServicioSalud").val('1').attr("disabled", "disabled");
}
function comboEstablecimiento(idServicioSalud) {

    var sUrl = `${GetWebApiUrl()}GEN_Establecimiento/ServicioSalud/${idServicioSalud}`;
    $("#sltEstablecimiento").empty();
    $("#sltEstablecimiento").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: sUrl,
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function () {
                $("#sltEstablecimiento").append("<option value='" + this.Id + "'>" +
                    this.Valor + "</option>");
            });
            $("#sltEstablecimiento").val("1").attr("disabled", "disabled");
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Establecimiento ${JSON.stringify(jqXHR)}`);
        }
    });

}
function comboProfesionEvolucionAtencion() {

    $("#sltProfesionEvolucionAtencion").empty();
    $("#sltProfesionEvolucionAtencion").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/${sSession.ID_PROFESIONAL}/Profesion`,
        success: function (data, status, jqXHR) {

            data.forEach((item, index) => {
                $("#sltProfesionEvolucionAtencion").append(`<option value='${item.GEN_idProfesion}'>${item.GEN_nombreProfesion}</option>`);
            });

            if ($("#sltProfesionEvolucionAtencion option").length == 2) {
                $("#sltProfesionEvolucionAtencion option:eq(1)").attr('selected', 'selected');
                $("#sltProfesionEvolucionAtencion").attr("disabled", "disabled");
                $("#spnProfesionEvolucionAtencion").text($("#sltProfesionEvolucionAtencion :selected").text());
            }
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Profesion de Profesional Atención clínica ${JSON.stringify(jqXHR)}`);
        }
    });


}

// INGRESO ADMISIÓN DAU
function GetJsonIngresoUrgencia(URG_idAtenciones_Urgencia) {

    var json = null;
    const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${URG_idAtenciones_Urgencia}?`
                + `extens=ATENCIONMEDICA&`
                + `extens=SIGNOSVITALES&`
                + `extens=CATEGORIZACION&`
                + `extens=MEDICO&`
                + `extens=GINECOOBSTETRICO&`
                + `extens=INDICACIONESCLINICAS&`
                + `extens=ANTECEDENTESCLINICOS`;
    
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            console.log(data)
            json = data;
        }
    });

    return json;
}
function CargarJsonIngresoUrgencia() {
    console.info(tipoGuardado)
    if (tipoGuardado == TIPO_GUARDADO.Admision) {

        ingresoUrgencia.MotivoConsulta = valCampo($('#txtMotivoConsulta').val());
        ingresoUrgencia.FechaLlegada = moment($('#txtFechaLlegada').val(), 'DD-MM-YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        ingresoUrgencia.IdPrevision = valCampo( parseInt($("#sltPrevision").val()));
        ingresoUrgencia.IdPrevision_Tramo = valCampo(parseInt($("#sltPrevisionTramo").val()));
        ingresoUrgencia.IdTipoAtencion = valCampo(parseInt($('#sltTipoAtencion').val()));
        ingresoUrgencia.IdPaciente = valCampo(parseInt(paciente.GEN_idPaciente));
        ingresoUrgencia.IdMediosTransporte = valCampo(parseInt($('#sltMedioTransporte').val()));
        ingresoUrgencia.IdTiposAccidente = valCampo(parseInt($('#sltTipoAccidente').val()));
        ingresoUrgencia.LugarAccidente = valCampo($('#txtLugarAccidente').val());
        ingresoUrgencia.IdEstablecimiento = valCampo($('#sltEstablecimiento').val());
        ingresoUrgencia.IdTipoRepresentante = (GetIdAcompañante() != null) ? valCampo(parseInt($('#sltTipoRepresentante').val())) : null;

    } else if (tipoGuardado == TIPO_GUARDADO.Medico || tipoGuardado == TIPO_GUARDADO.AtencionClinica) {

        if (tipoGuardado == TIPO_GUARDADO.Medico) {

            // Alcolemia
            ingresoUrgencia.Alcolemia = ($("#chkAlcoholemia").bootstrapSwitch('state')) ? "SI" : "NO"
            ingresoUrgencia.NumeroFrascoAlcolemia = ($("#chkAlcoholemia").bootstrapSwitch('state')) ? valCampo($("#txtNumeroFrasco").val()) : null;
            ingresoUrgencia.FechaHoraAlcolemia = ($("#chkAlcoholemia").bootstrapSwitch('state')) ?
                `${$("#txtFechaAlcoholemia").val()} ${$("#txtHoraAlcoholemia").val()}` : null;


            if ($('#sltTipoAtencion').val() == 2)
                CargarJsonAtencionUrgenciaObstetrica();

        } else {
            CargarJsonAtencionClinica();
        }

        // Atención Médica
        if ($("#divIngresoMedico").is(":visible") && (sSession.CODIGO_PERFIL == 1 || sSession.CODIGO_PERFIL == 25)) {

            ingresoUrgencia.AtencionUrgenciaMedico = {
                HipotesisDiagnostica: valCampo($("#txtDiagnostico").val()),
                IndicacionesMedicas: valCampo($("#txtIndicacionesMedicas").val()),
                ExamenFisico: valCampo($("#txtExamenFisico").val()),
                Anamnesis: valCampo($("#txtAnamnesis").val())
            }

        } else {
            ingresoUrgencia.AtencionUrgenciaMedico = null;
        }

        CargarArrayInterconsultistas();
        CargarArrayProcedimientos();
        CargarArrayMedicamentos();
        cargarArrayExamenesLaboratorio();
        cargarArrayExamenesImageonologia()
        
       
        //Agregar imageonologia

    }

}
function CargarIngresoUrgencia() {

    if (niSession.ID_INGRESO_URGENCIA != undefined) {

        var json = GetJsonIngresoUrgencia(niSession.ID_INGRESO_URGENCIA);
        ingresoUrgencia.URG_idAtenciones_Urgencia = niSession.ID_INGRESO_URGENCIA;
        deleteSession('ID_INGRESO_URGENCIA');

        // ESTABLECIMIENTO
        $("#sltServicioSalud").val(json.Establecimiento.IdServicioSalud);
        $("#sltEstablecimiento").val(json.Establecimiento.IdEstablecimiento);

        // PACIENTE
        buscarPacientePorId(json.Paciente.IdPaciente);
        DeshabilitarPaciente(false);
        ValidarRequired(true);
        ValTipoCampo($('#sltPrevision'), json.AtencionAdministrativa.IdPrevision);

        if (json.AtencionAdministrativa.IdPrevision != null)
            comboPrevisionTramo(json.AtencionAdministrativa.IdPrevision);

        ValTipoCampo($('#sltPrevisionTramo'), json.AtencionAdministrativa.IdPrevisionTramo);
        
        // DATOS DE ATENCIÓN
        $('#sltTipoAtencion').val(json.AtencionAdministrativa.IdTipoAtencion);

        if (json.Acompañante != null) {
            $('#chkAcompañante').bootstrapSwitch('state', true);
            SetIdAcompañante(json.Acompañante.Id);
            CargarPersona(json.Acompañante.Id, null, null);
            ValTipoCampo($("#sltTipoRepresentante"), json.Acompañante.IdTipoRepresentante);
        }

        $('#sltMedioTransporte').val(json.AtencionAdministrativa.IdMedioTransporte);

        if (json.AtencionAdministrativa.IdTipoAccidente == null) {
            $('#sltTipoAccidente').val('0');
            $('#txtLugarAccidente').attr('disabled', true);
            $('#lblErrorAccidente').hide();
        } else {
            $('#sltTipoAccidente').val(json.AtencionAdministrativa.IdTipoAccidente);
            $('#txtLugarAccidente').removeAttr('disabled');
            $('#lblErrorAccidente').show();
        }

        $('#txtFechaLlegada').val(moment(json.AtencionAdministrativa.FechaLlegada).format("DD-MM-YYYY HH:mm:ss"));
        $('#txtLugarAccidente').val(json.AtencionAdministrativa.LugarAccidente);
        $('#txtMotivoConsulta').val(json.AtencionAdministrativa.MotivoConsulta);
        $('#divMotivoConsulta').html(`<p class='text-black'>${json.AtencionAdministrativa.MotivoConsulta}</p>`);
        $('#txtLugarAccidente').val(json.AtencionAdministrativa.LugarAccidente);

        // DATOS CLÍNICOS
        if (niSession.CODIGO_PERFIL == 1 || niSession.CODIGO_PERFIL == 25 || tipoGuardado == TIPO_GUARDADO.AtencionClinica) {

            BloquearPaciente();
            $(`#divPrevision select, #divDatosAtencion input, #divDatosAtencion select, #divDatosAtencion textarea`).attr("disabled", "disabled").removeAttr("data-required");
            
            if (json.AtencionMedica.Alcolemia == "SI") {

                $("#chkAlcoholemia").bootstrapSwitch('state', true);
                $("#txtNumeroFrasco").val(json.AtencionMedica.NumeroFrascosAlcolemia);
                $("#txtFechaAlcoholemia").val(moment(json.AtencionMedica.FechaHoraAlcolemia).format("YYYY-MM-DD"));
                $("#txtHoraAlcoholemia").val(moment(json.AtencionMedica.FechaHoraAlcolemia).format("HH:mm"));
                $("#divAlcolemia").show();
                $(`#txtNumeroFrasco, #txtFechaAlcoholemia, #txtHoraAlcoholemia`).attr("data-required", true);

            } else if (json.AtencionMedica.AlcolemiaAtenciones_Urgencia == "NO") {

                $("#chkAlcoholemia").bootstrapSwitch('state', false);
                $(`#txtNumeroFrasco, #txtFechaAlcoholemia, #txtHoraAlcoholemia`).attr("data-required", false);

            }

            CargarIngresosMedicos(json.AtencionMedica.Descripcion);
            CargarAntecedentesClinicosPaciente(json);

            // Cargar Signos Vitales
            CargarSignosVitalesArray(json);

            if (json.Categorizacion.length > 0) {
                $("#divCategorizacion").append(`<h1 class='text-black text-center m-0'><strong>${json.Categorizacion[0].Codigo}</strong></h1>`);
                $("#divCategorizacionVacio").hide();

                if (json.Categorizacion[0].Observaciones != null)
                    $("#divCategorizacion").append(`<label>Observaciones</label>
                                                    <span class='form-control form-control-no-height'>${json.Categorizacion[0].Observaciones}</span>`);

                $("#divCategorizacion").show();
            }

            $("#divPaciente").html(($("#txtNombreSocial").val() != "") ?
                `${$("#txtNombreSocial").val()} (${$("#txtnombrePac").val()} ${$("#txtApePat").val()} ${$("#txtApeMat").val()}) | ${$("#txtEdadPac").val()}`
                : `${$("#txtnombrePac").val()} ${$("#txtApePat").val()} ${$("#txtApeMat").val()} | ${$("#txtEdadPac").val()}`);

            CargarAtencionObstetrica(json.AtencionMedica.AtencionObstetrica, json.AtencionAdministrativa.IdTipoAtencion);
            cargarIndicacionesUrg(json.IndicacionesClinicas);

            // ATENCIONES CLÍNICAS
            DibujarEvolucionesPaciente(json.IndicacionesClinicas.Evoluciones);
            DibujarProfesionalAtencion(json.AtencionMedica.MedicoAtencion);

            $("#divAtencionClinica").hide();

            if ((json.AtencionAdministrativa.IdTipoClasificacion == 4 && tipoGuardado === TIPO_GUARDADO.AtencionClinica)
                || (tipoGuardado === TIPO_GUARDADO.AtencionClinica)) {
                $("#divMedico input, #divMedico select, #divMedico textarea").attr("data-required", false);
                $("#divAtencionClinica").show();
            }

        }

    } else if (tipoGuardado == TIPO_GUARDADO.Medico || tipoGuardado == TIPO_GUARDADO.AtencionClinica) {
        SalirIngresoUrgencia();
    }
}
async function GuardarDAU() {

    const valido = await ValidarDAU();
    if (valido)
        GuardarIngresoUrgencia();
    else
        $(".aGuardarIngresoUrgencia").prop("disabled", false);

}
async function ValidarDAU() {

    if (tipoGuardado != TIPO_GUARDADO.AtencionClinica) {

        $('a[href="#divAntecedentesClinicos"]').trigger("click");
        await sleep(250);

        // PACIENTE
        if (!validarCampos("#divDPaciente", true) | ($("#chkAcompañante").bootstrapSwitch("state") && !validarCampos("#divAcompañante", true)))
            return false;

        $('a[href="#divDatosAtencionAdm"]').trigger("click");
        await sleep(250);

        // DATOS ADMINISTRATIVOS
        if (!validarCampos("#divDatosAtencion", true))
            return false;

        if (tipoGuardado == TIPO_GUARDADO.Medico) {

            if ($("#sltTipoAtencion").val() == "2") {

                // DATOS CLÍNICOS
                $('a[href="#divDatosClinicos"]').trigger("click");
                await sleep(250);
                if (!validarCampos("#divMedico", true) && $("#divIngresoMedico").is(":visible"))
                    return false;

                // DATOS GINECOOBSTETRICO
                $('a[href="#divDatosGinecoObstetrico"]').trigger("click");
                await sleep(250);
                if (!validarCampos("#divDatosGinecoObstetrico", true))
                    return false;

            } else {

                // DATOS CLÍNICOS
                $('a[href="#divDatosClinicos"]').trigger("click");
                await sleep(250);
                if (!validarCampos("#divMedico", true) && $("#divIngresoMedico").is(":visible"))
                    return false;

            }

            // DATOS CLÍNICOS
            $('a[href="#divDatosClinicos"]').trigger("click");
            await sleep(250);
            if (!validarCampos("#divMedico", true) && $("#divIngresoMedico").is(":visible"))
                return false;

        }

    }

    return true;

}
function GuardarIngresoUrgencia() {

    let method = (ingresoUrgencia.URG_idAtenciones_Urgencia !== undefined) ? "PUT" : "POST";
    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia`;

    if (tipoGuardado == TIPO_GUARDADO.Medico) {
        url += `/${ingresoUrgencia.URG_idAtenciones_Urgencia}/Medico`;
        delete ingresoUrgencia.IdAcompañante;
    } else if (tipoGuardado == TIPO_GUARDADO.Admision) {
        GuardarPersona();
        ingresoUrgencia.IdAcompañante = GetIdAcompañante();
        GuardarPaciente();
        url += (method == "PUT") ? `/${ingresoUrgencia.URG_idAtenciones_Urgencia}/Admision` : "";
    } else if (tipoGuardado == TIPO_GUARDADO.AtencionClinica) {
        url += `/${ingresoUrgencia.URG_idAtenciones_Urgencia}/AtencionClinica`;
        delete ingresoUrgencia.IdAcompañante;
    }

    CargarJsonIngresoUrgencia();
    delete ingresoUrgencia.URG_idAtenciones_Urgencia;
    
    $.ajax({
        type: method,
        url: url,
        contentType: 'application/json',
        data: JSON.stringify(ingresoUrgencia),
        dataType: 'json',
        success: function (data) {

            if (method == "POST")
                ingresoUrgencia.URG_idAtenciones_Urgencia = data.AtencionAdministrativa.IdAtencionUrgencia;

            Swal.fire({
                position: 'center', //
                icon: 'success', //
                title: 'Ingreso Correcto',
                showConfirmButton: false, //
                timer: 2000,
                allowOutsideClick: false
            }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    SalirIngresoUrgencia();
                }
            });

        },
        error: function (jqXHR, status) {
            $(".aGuardarIngresoUrgencia").prop("disabled", false);
            console.log(`Error al guardar Atención Urgencia ${JSON.stringify(jqXHR)}`);
        }
    });

}
function CargarIngresosMedicos(arrayIngresosMedicos) {

    $("#btnCancelarEdicionIngresoMedico").hide();

    let index = 0;

    for (const item of arrayIngresosMedicos) {

        const html =
            `
                <button id='btnIM_${index}' data-toggle="collapse" class="btn btn-outline-primary container-fluid mt-3" data-target="#divIngresoMedico_${index}"
                    onclick="return false;">
                    ${moment(item.FechaHora).format("DD-MM-YYYY HH:mm:ss")} | ${item.Profesional}
                </button>
                <div id="divIngresoMedico_${index}" class="collapse p-2">

                    <h4>${moment(item.FechaHora).format("DD-MM-YYYY HH:mm:ss")} | ${item.Profesional}</h4>

                    <div class="row mt-2">
                        <div class="col-12 col-sm-12 col-md-6 col-xl-4">
                            <label>Anamnesis</label>
                            <textarea id="txtAnamnesis_${index}" class="form-control" rows="5" disabled>
                            </textarea>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-xl-4">
                            <label>Examen Físico</label>
                            <textarea id="txtExamenFisico_${index}" class="form-control" rows="5" disabled>
                            </textarea>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-xl-4">
                            <label>Indicaciones al inicio de la atención</label>
                            <textarea id="txtIndicacionesMedicas_${index}" class="form-control" placeholder="Escriba indicaciones médicas" rows="5" disabled>
                            </textarea>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-12">
                            <label>Hipótesis diagnóstica</label>
                            <textarea id="txtDiagnostico_${index}" class="form-control" rows="5" disabled>
                            </textarea>
                        </div>
                    </div>

                </div>`;

        $("#divIngresosMedicos").append(html);

        $(`#txtAnamnesis_${index}`).val(item.Anamnesis);
        $(`#txtExamenFisico_${index}`).val(item.ExamenFisico);
        $(`#txtIndicacionesMedicas_${index}`).val(item.IndicacionMedicas);
        $(`#txtDiagnostico_${index}`).val(item.HipotesisDiagnostica);
        index++;

    }

    $(`#btnIM_${index - 1}`).trigger('click');
    $("#divIngresoMedico").hide();

    if ((niSession.CODIGO_PERFIL == 1 || niSession.CODIGO_PERFIL == 25) && arrayIngresosMedicos.length > 0) {

        $("#divIngresosMedicos").append(
            `<a id='btnIngresoMedico' class='btn btn-success btn-lg btn-block'><i class="fas fa-pencil-alt"></i> Editar Información Médica</a>`);
        $("#btnIngresoMedico").unbind().click(function () {

            $(`#divIngresosMedicos .collapse`).removeClass("show");
            $("#divIngresoMedico, #btnCancelarEdicionIngresoMedico").show();
            $("#btnIngresoMedico").hide();
            $(`#txtAnamnesis, #txtExamenFisico, #txtIndicacionesMedicas, #txtDiagnostico`).attr("data-required", true).removeAttr("disabled");

            const jsonIM = arrayIngresosMedicos[arrayIngresosMedicos.length - 1];

            $(`#txtAnamnesis`).val(jsonIM.Anamnesis);
            $(`#txtExamenFisico`).val(jsonIM.ExamenFisico);
            $(`#txtIndicacionesMedicas`).val(jsonIM.IndicacionMedicas);
            $(`#txtDiagnostico`).val(jsonIM.HipotesisDiagnostica);

            $("#btnCancelarEdicionIngresoMedico").unbind().click(
                function () {
                    if (!$(`#divIngresoMedico_${arrayIngresosMedicos.length - 1}`).hasClass("show"))
                        $(`#divIngresoMedico_${arrayIngresosMedicos.length - 1}`).addClass("show");
                    $(`#txtAnamnesis, #txtExamenFisico, #txtIndicacionesMedicas, #txtDiagnostico`).attr("data-required", true);
                    $("#divIngresoMedico").hide();
                    $("#btnIngresoMedico").show();
                });
        });
    } else if (niSession.CODIGO_PERFIL == 1 || niSession.CODIGO_PERFIL == 25) {
        $("#divIngresoMedico").show();
    }

}

// OBSTETRICA 
function CargarJsonAtencionUrgenciaObstetrica() {

    ingresoUrgencia.AtencionUrgenciaObstetrica.UltimaRegla = $("#txtUltimaRegla").val();
    ingresoUrgencia.AtencionUrgenciaObstetrica.AlturaUterina = valCampo(parseInt($("#txtAlturaUterinaObstetrico").val()));
    ingresoUrgencia.AtencionUrgenciaObstetrica.LatidosCardioFetales = valCampo(parseInt($("#txtLatidosCardioFetalesObstetrico").val()));
    ingresoUrgencia.AtencionUrgenciaObstetrica.FlujoGenital = null;

    if ($("#rdoFlujoGenitalObstetricoSi").bootstrapSwitch('state'))
        ingresoUrgencia.AtencionUrgenciaObstetrica.FlujoGenital = "SI";
    else if ($("#rdoFlujoGenitalObstetricoNo").bootstrapSwitch('state'))
        ingresoUrgencia.AtencionUrgenciaObstetrica.FlujoGenital = "NO";

    ingresoUrgencia.AtencionUrgenciaObstetrica.TactoVaginal = valCampo($("#txtTactoVaginalObstetrico").val());
    ingresoUrgencia.AtencionUrgenciaObstetrica.Especuloscopia = valCampo($("#txtEspeculoscopiaObstetrico").val());
    ingresoUrgencia.AtencionUrgenciaObstetrica.Ecografia = valCampo($("#txtEcografiaObstetrico").val());
    ingresoUrgencia.AtencionUrgenciaObstetrica.ActividadUterina = valCampo($("#txtActividadUterinaObstetrico").val());
    ingresoUrgencia.AtencionUrgenciaObstetrica.Otros = valCampo($("#txtOtrosGinecoObstetrico").val());

}
function CargarAtencionObstetrica(atencionObstetrica, idTipoAtencion) {

    let fechaActual = null;

    if (idTipoAtencion == 2) { // Ginecologico
        ingresoUrgencia.AtencionUrgenciaObstetrica = {};
        $("#divDatosGinecoObstetrico").show();
        $(`#txtUltimaRegla`).attr("data-required", true);
        fechaActual = GetFechaActual();
        $(`#spnFechaHoraAtencionObstetricia`).text(moment(fechaActual).format('DD-MM-YYYY HH:mm:ss'));
        ingresoUrgencia.AtencionUrgenciaObstetrica.FechaHoraAtencion = fechaActual;
    } else {
        $("#divDatosGinecoObstetrico").hide();
        $(`#txtUltimaRegla`).attr("data-required", false);
    }

    if (idTipoAtencion == 2 && atencionObstetrica.length > 0) {

        ingresoUrgencia.AtencionUrgenciaObstetrica = {};
        const jsonObs = atencionObstetrica[0];
        ingresoUrgencia.AtencionUrgenciaObstetrica.FechaHoraAtencion = jsonObs.FechaHora;
        $(`#spnFechaHoraAtencionObstetricia`).text(moment(jsonObs.FechaHora).format('DD-MM-YYYY HH:mm:ss'));
        $("#txtUltimaRegla").val(jsonObs.UltimaRegla);
        $("#txtAlturaUterinaObstetrico").val(jsonObs.AlturaUterina);
        $("#txtLatidosCardioFetalesObstetrico").val(jsonObs.LatidosCardioFetales);

        if (jsonObs.FlujoGenital === "SI")
            $("#rdoFlujoGenitalObstetricoSi").bootstrapSwitch('state', true);
        else if (jsonObs.FlujoGenital === "NO")
            $("#rdoFlujoGenitalObstetricoNo").bootstrapSwitch('state', true);

        if (sSession.CODIGO_PERFIL != 1 && sSession.CODIGO_PERFIL != 25) {
            $("#rdoFlujoGenitalObstetricoSi").bootstrapSwitch('disabled', true);
            $("#rdoFlujoGenitalObstetricoNo").bootstrapSwitch('disabled', true);
        }

        $("#txtTactoVaginalObstetrico").val(jsonObs.TactoVaginal);
        $("#txtEspeculoscopiaObstetrico").val(jsonObs.Especuloscopia);
        $("#txtEcografiaObstetrico").val(jsonObs.Ecografia);
        $("#txtActividadUterinaObstetrico").val(jsonObs.ActividadUterina);
        $("#txtOtrosGinecoObstetrico").val(jsonObs.Otros);

    }

}

// ANTECEDENTE CLINICOS PACIENTE (CATEGORIZACIÓN)
function CargarAntecedentesClinicosPaciente(json) {

    $("#divAntecedentesPacienteVacio, #divAntecedentesPaciente").hide();

    if (json.AtencionAdministrativa.IdTipoClasificacion != null) {

        $("#divAntecedentesPaciente").show();
        $("#divDescripcionAlergias, #divAntecedentesMorbidos, #divAntecedentesQx, #divAntecedentesGinecoObstetrico").hide();
        $("#spnClasificacion").text(json.AtencionAdministrativa.TipoClasificacion);
        $("#spnAlergias").text(json.AntecedentesClinicos.TipoAlternativaAntecedentes);

        if (json.AntecedentesClinicos.TipoAlternativaAntecedentes === "SI") {
            $("#divDescripcionAlergias").show();
            $("#spnDetalleAlergias").text(json.AntecedentesClinicos.Alergias);
        }

        if (json.AntecedentesClinicos.AntecedentesMorbidos != null) {
            $("#divAntecedentesMorbidos").show();
            $("#spnAntecedentesMorbidos").text(json.AntecedentesClinicos.AntecedentesMorbidos)
        }

        if (json.AntecedentesClinicos.AntecedentesQx != null) {
            $("#divAntecedentesQx").show();
            $("#spnAntecedentesQx").text(json.AntecedentesClinicos.AntecedentesQx);
        }

        if (json.AntecedentesClinicos.AntecedentesGinecoObstetrico != null) {
            $("#divAntecedentesGinecoObstetrico").show();
            $("#spnAntecedentesGinecoObstetrico").text(json.AntecedentesClinicos.AntecedentesGinecoObstetrico);
        }

    } else
        $("#divAntecedentesPacienteVacio").show();

}

// PROFESIONAL 
function CargarJsonProfesional(idProfesional) {

    var json = {};

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/${idProfesional}`,
        async: false,
        success: function (data, status, jqXHR) {
            json = data;
        },
        error: function (jqXHR, status) {
            console.log(`Error al cargar Profesional: ${JSON.stringify(jqXHR)}`);
        }
    });

    return json;

}
function CargarProfesional(idProfesional) {

    var data = CargarJsonProfesional(idProfesional);

    profesional.GEN_idProfesional = data.GEN_idProfesional;
    $("#txtNumeroDocumentoProfesional").val(data.GEN_rutProfesional +
        (data.GEN_digitoProfesional !== null ? `-${data.GEN_digitoProfesional}` : ''));
    $("#txtNombreProfesional").val(data.GEN_nombreProfesional);
    $("#txtApePatProfesional").val(data.GEN_apellidoProfesional);
    $("#txtApeMatProfesional").val(data.GEN_sapellidoProfesional);

}
function DibujarProfesionalAtencion(profesional) {

    comboProfesionEvolucionAtencion();

    if (sSession.CODIGO_PERFIL == 25)
        $("#divDatosProfesionales h4").html("Datos de Profesional Matronería");

    if (profesional === null) {

        // Se valida si se está
        if (niSession.CODIGO_PERFIL == 1 || niSession.CODIGO_PERFIL == 25) {
            CargarProfesional(niSession.ID_PROFESIONAL);
            $("#divMedico, #divDatosProfesionales").show();
        } else
            $("#divDatosProfesionales").hide();
            
    } else {
        $("#txtNumeroDocumentoProfesional").val(profesional.NumeroDocumento);
        $("#txtNombreProfesional").val(profesional.Nombre);
        $("#txtApePatProfesional").val(profesional.ApellidoPaterno);
        $("#txtApeMatProfesional").val(profesional.ApellidoMaterno);
        $("#divMedico, #divDatosProfesionales").show();

    }

}

// EVOLUCIÓN
function CargarJsonAtencionClinica() {

    ingresoUrgencia.Evolucion = null;

    if (valCampo($("#txtEvolucionAtencion").val()) != null)
        ingresoUrgencia.Evolucion = {
            DescripcionEvolucion: valCampo($("#txtEvolucionAtencion").val()),
            IdProfesion: parseInt($("#sltProfesionEvolucionAtencion").val())
        };

}
function DibujarEvolucionesPaciente(arrayEvoluciones) {

    if (arrayEvoluciones.length > 0) {
        arrayEvoluciones.forEach((item, index) => {
            $("#divResumeEvolucion").append(`
                                <strong>${moment(item.FechaEvolucion).format("DD-MM-YYYY HH:mm:ss")} | ${item.NombreProfesional} | ${item.NombreProfesion}</strong>
                                <span class='form-control form-control-no-height'>${item.DescricionEvolucion}</span>`);
        });
    } else {
        $("#divResumeEvolucion").html(`<div class="alert alert-warning text-center">
                                            <strong>
                                                <i class="fa fa-exclamation-triangle"></i> No existe información ingresada.
                                            </strong>
                                        </div>`);
    }



}

// OPCIONES DE SALIDA
function CancelarIngresoUrgencia() {

    ShowModalAlerta('CONFIRMACION',
        'Salir del Ingreso de urgencia',
        '¿Está seguro que desea cancelar el registro?',
        SalirIngresoUrgencia);

}
function SalirIngresoUrgencia() {
    window.removeEventListener('beforeunload', bunload, false);
    window.location.href = `${ObtenerHost()}/Vista/ModuloUrgencia/BandejaUrgencia.aspx`;
}