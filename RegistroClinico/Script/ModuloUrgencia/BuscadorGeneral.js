﻿
var sSession, idDiv = '';

$(document).ready(function () {

    sSession = getSession();
    RevisarAcceso(false, sSession);

    //$("#txtnumeroDocPaciente").blur(function () {

    //    var parent = $(this).parent().parent().parent().parent();

    //    // SI ES RUT O RUT MATERNO
    //    if ($(`#sltIdentificacionPaciente`).val() == '1' || $(`#sltIdentificacionPaciente`).val() == '4') {
    //        var dig = ObtenerVerificador($(this).val());
    //        (dig !== null) ? $(`#txtDigPaciente`).val(dig) : $(`#txtDigPaciente, #txtnumeroDocPaciente`).val("");
    //    }

    //});

    //$("#sltIdentificacionPaciente").change(function () {

    //    $(`label[for='${$(this).attr("id")}']`).text($(this).children("option:selected").text());
    //    var parent = $(this).parent().parent().parent();
    //    ($(this).val() == '1' || $(this).val() == '4') ? $(`#${parent.attr("id")} .digito`).show() : $(`#${parent.attr("id")} .digito`).hide();

    //    $("#txtnumeroDocPaciente, #txtDigPaciente").val("");

    //});

    $("#btnFiltroUrg").trigger('click');

    CargarComboIdentificadores();
    CargarBandejaBuscadorGeneral([]);

    $("#btnUrgFiltro").click(function () {

        var adataset = [];
        var url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/Bandeja/General?numerodocumento=${
            ($.trim($("#txtnumeroDocPaciente").val()) !== "") ? $.trim($("#txtnumeroDocPaciente").val()) : ""}&nombre=${
            ($.trim($("#txtnombrePaciente").val()) !== "") ? $.trim($("#txtnombrePaciente").val()) : ""}&apellidopaterno=${
            ($.trim($("#txtApePatPaciente").val()) !== "") ? $.trim($("#txtApePatPaciente").val()) : ""}&apellidomaterno=${
            ($.trim($("#txtApeMatPaciente").val()) !== "") ? $.trim($("#txtApeMatPaciente").val()) : ""}&ididentificacion=${
            $("#sltIdentificacionPaciente").val()}&fechaInicial=${
            ($.trim($("#txtFechaInicio").val()) !== "") ? $.trim($("#txtFechaInicio").val()) : ""}&fechaFinal=${
            ($.trim($("#txtFechaTermino").val()) !== "") ? $.trim($("#txtFechaTermino").val()) : ""}`;

        $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            success: function (data) {
                $.each(data, function () {
                    adataset.push([
                    /*0*/ this.URG_idAtenciones_Urgencia,
                    /*1*/ this.numeroDocumento,
                    /*2*/ this.nombrePaciente,
                    /*3*/ edad(this.GEN_fec_nacimientoPaciente),
                    /*4*/ this.URG_motivo_consultaAtenciones_Urgencia,
                    /*5*/ this.URG_fecha_llegadaAtenciones_Urgencia == null ? this.URG_fecha_llegadaAtenciones_Urgencia : moment(this.URG_fecha_llegadaAtenciones_Urgencia).format('DD-MM-YYYY'),
                    /*6*/ this.URG_idTipo_Atencion,
                    /*7*/ this.GEN_nombreTipo_Estados_Sistemas,
                    /*8*/ '',
                    /*9*/ this.Acciones.Imprimir,
                    /*10*/ this.Acciones.AtencionClinica,
                    /*11*/ this.Acciones.VerMovimientos,
                    /*12*/ (this.URG_nomCategorizacion == null) ? '' : this.URG_nomCategorizacion
                    ]);

                });
                CargarBandejaBuscadorGeneral(adataset);
            }
        });
    });

    $("#btnUrgLimpiarFiltro").click(function () {

        $("#txtnumeroDocPaciente, #txtDigPaciente, #txtNombrePaciente, #txtApePatPaciente, #txtApeMatPaciente").val("");
        $("#sltIdentificacionPaciente").val("1");

        $(`label[for='${$("#sltIdentificacionPaciente").attr("id")}']`).text($("#sltIdentificacionPaciente").children("option:selected").text());
        var parent = $("#sltIdentificacionPaciente").parent().parent().parent();
        ($("#sltIdentificacionPaciente").val() == '1' || $("#sltIdentificacionPaciente").val() == '4') ? $(`#${parent.attr("id")} .digito`).show() : $(`#${parent.attr("id")} .digito`).hide();

        $("#txtnumeroDocPaciente, #txtDigPaciente").val("");

    });

    $("#aVolverAtencionesClinicas").click(function () {
        $('a[href="#divAtencionesClinica"]').trigger("click");
    });

});

function CargarBandejaBuscadorGeneral(adataset) {

    var json = {};
    
    $('#tblUrgencia').DataTable({
        data: adataset,
        "aaSorting": [[0, 'desc']],
        columns: [
            /*0*/ { title: 'ID' },
            /*1*/ { title: 'N° Doc' },
            /*2*/ { title: 'Nombre' },
            /*3*/ { title: 'Edad' },
            /*4*/ { title: 'Motivo consulta' },
            /*5*/ { title: 'Fecha llegada' },
            /*6*/ { title: 'Estados' },
            /*7*/ { title: 'Estado' },
            /*8*/ { title: '' },
            /*9*/ { title: 'Imprimir' },
            /*10*/ { title: 'Atención clínica' },
            /*11*/ { title: 'Ver Movimientos' },
            /*12*/ { title: 'Categorización' }
        ],
        columnDefs: [
            { targets: [9, 10, 11, 12], visible: false, searchable: false },
            { type: 'date-uk', targets: 5 },
            { targets: [6],
                render: function (data, type, row, meta) {

                    var fila = meta.row;
                    var botones;

                    if (adataset[fila][6] == 1)
                        botones =
                            `<h5>
                                <span id="bgEstado" class="badge badge-pill btn-success badge-count">A</span>
                            </h5>`;
                    else if (adataset[fila][6] == 2)
                        botones =
                            `<h5>
                                <span id="bgEstado" class="badge badge-pill btn-warning badge-count">G</span>
                            </h5>`;
                    else if (adataset[fila][6] == 3)
                        botones =
                            `<h5>
                                <span id="bgEstado" class="badge badge-pill btn-info badge-count">P</span>
                            </h5>`;

                    if (adataset[fila][12] != undefined && adataset[fila][12] != "") {
                        var color = "";
                        if (adataset[fila][12] == "C1")
                            botones +=
                                `<h5>
                                    <span id="bgEstado" class="badge badge-pill btn-danger badge-count">
                                        ${adataset[fila][12]}
                                    </span>
                                </h5>`;
                        if (adataset[fila][12] == "C2")
                            botones +=
                                `<h5>
                                    <span id="bgEstado" class="badge badge-pill badge-count" style="color: #ffffff; background-color: orange;">
                                        ${adataset[fila][12]}
                                    </span>
                                </h5>`;

                        if (adataset[fila][12] == "C3")
                            botones +=
                                `<h5>
                                    <span id="bgEstado" class="badge badge-pill btn-warning badge-count">
                                        ${adataset[fila][12]}
                                    </span>
                                </h5>`;

                        if (adataset[fila][12] == "C4")
                            botones +=
                                `<h5>
                                    <span id="bgEstado" class="badge badge-pill btn-success badge-count">
                                        ${adataset[fila][12]}
                                    </span>
                                </h5>`;

                        if (adataset[fila][12] == "C5")
                            botones +=
                                `<h5>
                                    <span id="bgEstado" class="badge badge-pill btn-info badge-count">
                                        ${adataset[fila][12]}
                                    </span>
                                </h5>`;
                    }

                    return botones;
                },
            },
            {
                targets: 8,
                data: null,
                orderable: false,
                render: function (data, type, row, meta) {

                    var fila = meta.row;
                    var botones =
                        `
                            <button id="#div_accionesUrgencia${fila}" class='btn btn-primary btn-circle' 
                                onclick='toggle("#div_accionesUrgencia${fila}"); return false;' data-toggle="tooltip" data-placement="top" 
                                title="Acciones">
                                <i class="fa fa-list" style="font-size:15px;"></i>
                            </button>
                            <div id='div_accionesUrgencia${fila}' class='rounded-actions' style="display: none;">
                        `;

                    if (adataset[fila][9]) { // Imprimir
                        botones +=
                            `
                                <a id='linkImprimir_${fila}' data-id='${adataset[fila][0]}' class='btn btn-info btn-circle btn-lg' href='#/' 
                                    data-print='true' onclick='linkImprimir(this)' data-toggle="tooltip" data-placement="left" 
                                    title="Imprimir">
                                    <i class='fas fa-print'></i>
                                </a><br>
                            `;
                    }

                    if (adataset[fila][10]) { // Atención clínica
                        botones +=
                            `
                                <a id='linkAtencionClinica_${fila}' data-id='${adataset[fila][0]}' 
                                    class='btn btn-primary btn-circle btn-lg' href='#/' onclick='linkVerAtencionesClinicas(this)' data-toggle='tooltip'
                                    data-placement='left' title='Ver atenciones clínicas'><i class="fa fa-clipboard-list"></i>
                                </a><br>
                            `;
                    }

                    if (adataset[fila][11]) { // Movimientos
                        botones +=
                            `
                                <a id='linkMovimientosIngresoUrg_${fila}' data-id='${adataset[fila][0]}' 
                                    class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkMovimientosIngresoUrgencia(this)' 
                                    data-toggle="tooltip" data-placement="left" title="Movimientos">
                                    <i class='fa fa-eye'></i>
                                </a><br>`;
                    }

                    return `${botones}</div>`;

                }
            },
        ],
        bDestroy: true
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function edad(b) {

    var a = moment();
    b = moment(b);
    var years = a.diff(b, 'year');
    b.add(years, 'years');
    var months = a.diff(b, 'months');
    b.add(months, 'months');
    var days = a.diff(b, 'days');
    let sedad = "";
    if (years > 0)
        sedad = `${years} años `;
    else if (years == 1)
        sedad = `${years} año `;

    if (months > 0)
        sedad += `${months} meses `;
    else if (months == 1)
        sedad += `${months} mes `;

    if (days > 0)
        sedad += `${days} días `;
    else if (months == 1)
        sedad += `${days} día `;

    return sedad;

}

function toggle(id) {

    if (idDiv == '') {
        $(id).fadeIn();
        idDiv = id;
    } else if (idDiv == id) {
        $(id).fadeOut();
        idDiv = '';
    } else {
        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }

}


function linkMovimientosIngresoUrgencia(sender) {

    var adataset = [];
    var id = $(sender).data("id");

    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/Movimientos`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            $.each(data, function (i, r) {
                adataset.push([
                    moment(r.fecha).format('DD-MM-YYYY HH:mm:ss'),
                    r.usuario,
                    r.descripcion
                ]);
            });

        }
    });

    if (adataset.length == 0)
        $('#lnkExportarMovIngresoUrgencia').addClass('disabled');
    else
        $('#lnkExportarMovIngresoUrgencia').removeClass('disabled');

    $('#tblMovimientosIngresoUrgencia').addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: "Fecha Movimiento" },
            { title: "Usuario" },
            { title: "Movimiento" }
        ],
        "columnDefs": [
            {
                "targets": 1,
                "sType": "date-ukLong"
            }
        ],
        "bDestroy": true
    });

    $('#tblMovimientosIngresoUrgencia').css("width", "100%");
    $('#mdlMovimientosIngresoUrgencia').modal('show');

}

function linkImprimir(e) {

    var id = $(e).data("id");
    $('#modalCargando').show();
    $('#frameDAU').attr('src', 'ImprimirDAU.aspx?id=' + id);
    $('#mdlImprimir').modal('show');

}

function linkVerAtencionesClinicas(e) {
    var id = $(e).data('id');
    $("#aNuevaAtencionClinica").data("id", id);
    CargarTablaAtencionesClinicas(id);
    $("#mdlAtencionesClinicas").modal("show");
}

function CargarTablaAtencionesClinicas(id) {

    var adataset = [];

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/atencionesClinicas`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $.each(data, function () {

                adataset.push([
                /*0*/ this.URG_idAtenciones_Urgencia_Tipo_Estamento,
                /*1*/ moment(this.URG_fecha_horaAtenciones_Urgencia_Tipo_Estamento).format("DD-MM-YYYY HH:mm:ss"),
                /*2*/ this.URG_nombreTipo_Estamento,
                /*3*/ this.GEN_nombreProfesional,
                /*4*/ this.Acciones.Ver
                ]);

            });
        }
    });

    $('#tblAtencionesClinicas').DataTable({
        data: adataset,
        columns: [
        /*0*/{ title: 'ID' },
        /*1*/{ title: 'Fecha atención' },
        /*2*/{ title: 'Estamento' },
        /*3*/{ title: 'Profesional' },
        /*4*/{ title: '' }
        ],
        columnDefs: [
            {
                targets: [0],
                visible: false,
                searchable: false
            },
            {
                targets: 4,
                data: null,
                orderable: false,
                render: function (data, type, row, meta) {

                    var fila = meta.row;
                    if (adataset[fila][4])
                        return `<a id='linkVer_${fila}' data-id='${adataset[fila][0]}' data-toggle='tooltip'
                                    class='btn btn-info' href='#/' onclick='linkVerAtencionClinica(this);' 
                                    data-placement='left' title='Ver'><i class='fa fa-eye'></i>
                                </a>`;;

                }
            }
        ],
        bDestroy: true
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function linkVerAtencionClinica(e) {

    let id = $(e).data("id");
    let json = CargarJsonAtencionClinica(id);

    $("#txtVerFechaAtencionClinica, #txtVerEstamentoAtencionClinica").val("");
    $("#divVerDescripcionAtencionClinica, #divVerAranceles, #divVerProfesional").html("");
    $("#txtVerFechaAtencionClinica").val(moment(json.URG_fecha_horaAtenciones_Urgencia_Tipo_Estamento).format("DD-MM-YYYY HH:mm:ss"));
    $("#txtVerEstamentoAtencionClinica").val(json.URG_nombreTipo_Estamento);
    $("#divVerDescripcionAtencionClinica").append(json.URG_descripcionAtenciones_Urgencia_Tipo_Estamento);
    $("#divVerProfesional").append(json.GEN_nombreProfesional);

    $.each(json.GEN_Arancel, function () {
        $("#divVerAranceles").append(`${this.GEN_detalleArancel}<br />`);
    });

    $('a[href="#divVistaAtencionClinica"]').trigger("click");

}