﻿$(document).ready(function () {
    getUrgenciasApi();
});
function getUrgenciasApi() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/Dashboard/SituacionActual`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            getUrgenciasPorEstado(data);
            getUrgenciasPorTipoAtencion(data);
            getUrgenciasPorCategorizacion(data);
        },
        error: function (jqXHR, status) {
            console.log(jqXHR);
        }
    });
    ShowModalCargando(false);
}
function getUrgenciasPorEstado(data) {
    let html;
    let cabecera = `<tr>
                        <th>Estado</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    <tr>`;
    $('#tblEstadosUrgencia').append(cabecera);
    let contador = 0;
    let filtro = [100, 101, 102, 103, 104, 105];
    filtro.forEach(function (key, i) {
        let registros = data.filter(d => d.IdTipoEstado == key);
        if (registros.length > 0) {
            html = `<tr>
                    <td>${registros[0].NombreTipoEstado}</td>
                    <td>${registros.length}</td>
                    <td>
                       <a id="aVerDetalleEstsado_${i}" onclick="getDetalleEstado(${key})" class="btn  btn-outline-info">Ver</a></td>
                <tr>`;
            $('#tblEstadosUrgencia').append(html);
            contador += registros.length;
        }
        $('#spTotalEstadosUrgencia').text(contador);
    });
}
function getUrgenciasPorTipoAtencion(data) {
    let html;
    let cabecera = `<tr>
                        <th>Tipo</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    <tr>`;
    $('#tblTipoUrgencia').append(cabecera);
    let contador = 0;
    let filtro = [1, 2, 3];
    filtro.forEach(function (key, i) {
        let registros = data.filter(d => d.IdTipoAtencion == key);
        if (registros.length > 0) {
            html = `<tr>
                    <td>${registros[0].DescripcionTipoAtencion}</td>
                    <td>${registros.length}</td>
                    <td>
                       <a id="aVerDetalleEstado_${i}" onclick="getDetalleTipoAtencion(${key})" class="btn  btn-outline-info">Ver</a></td>
                <tr>`;
            $('#tblTipoUrgencia').append(html);
            contador += registros.length;
        }
        $('#spTotalTipoUrgencia').text(contador);
    });
}
function getUrgenciasPorCategorizacion(data) {
    let html;
    let cabecera = `<tr>
                        <th>Categorizacion</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    <tr>`;
    $('#tblCategorizacionUrgencia').append(cabecera);
    let contador = 0;
    let filtro = ["C1", "C2", "C3", "C4", "C5"];
    filtro.forEach(function (key, i) {

        let registros = data.filter(d => (d.Categorizacion != null ? d.Categorizacion.Codigo : "") == key);
        if (registros.length > 0) {
            html = `<tr>
                    <td>${registros[0].Categorizacion.Codigo}</td>
                    <td>${registros.length}</td>
                    <td>
                       <a id="aVerDetalleEstsado_${i}" onclick="getDetalleUrgenciaApi(${key})" 
                            class="btn  btn-outline-info">Ver</a></td>
                <tr>`;
            $('#tblCategorizacionUrgencia').append(html);
            contador += registros.length;
        }

        $('#spTotalCategorizados').text(contador);
    });
}
function getDetalleEstado(id) {
    let parametro = `idTipoEstadoSistema=${id}`;
    getDetalleAtencionUrgenciaApi(parametro)
}
function getDetalleTipoAtencion(id) {
    let parametro = `IdTipoAtencion=${id}`;
    getDetalleAtencionUrgenciaApi(parametro)
}
function getDetalleAtencionUrgenciaApi(parametros) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/Buscar?${parametros}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            var adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    moment(val.FechaAdmision).format('YYYY-MM-DD hh:mm'),
                    val.IdTipoEstadoSistema,
                    val.Paciente.TipoIdentificacion,
                    val.Paciente.NumeroIdentificacion,
                    (val.Paciente.Nombre + " " ?? "") +
                    (val.Paciente.ApellidoPaterno ?? "") + " " +
                    (val.ApellidoMaterno ?? ""),
                    val.NombreTipoAtencion,
                    val.Categorizacion != null ? val.Categorizacion.CodigoCategorizacion : "",
                    val.NombreTipoEstadoSistema,
                    ''
                ]);
            });
            $('#tblDetalle').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [1, 2], visible: false, searchable: false },
                        {
                            targets: [-1],
                            render: function (data, type, row, meta) {

                                var fila = meta.row;

                                let dateOne = new Date(adataset[fila][1]);
                                let dateTwo = new Date();

                                let msdiferencia = dateTwo - dateOne;
                                let minutos = Math.floor(msdiferencia / 1000 / 60);
                                let horas = 0;
                                if (minutos > 60) {
                                    horas = Math.floor(minutos / 60);
                                    minutos = minutos - (horas * 60);
                                }

                                return (horas < 10 ? "0" + horas : horas) + ":" + (minutos < 10 ? "0" + minutos : minutos);
                            }
                        },
                    ],
                columns: [
                    { title: 'DAU' },
                    { title: 'Fecha DAU' },
                    { title: 'IdTipoEstadoSistema' },
                    { title: 'Dcto' },
                    { title: 'Número' },
                    { title: 'Nombre' },
                    { title: 'Tipo' },
                    { title: 'Categorización' },
                    { title: 'Estado' },
                    { title: 'Tiempo' }

                ],
                bDestroy: true
            });
            $("#mdlDetalle").modal("show");
        }
    });
}