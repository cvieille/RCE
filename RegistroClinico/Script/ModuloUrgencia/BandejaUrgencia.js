﻿
let sSession, vCodigoPerfil, vIdAtencion, vCategorizacion, vRecategorizar, URG_idCategorizaciones_Atencion;
let bDatosVitales, vEstado, vDiasPaciente, vOpcion, vEpidemiologico, idDiv;
let diagnosticosCIE10;
let selectedDiagnostics = [];
let datosExportarURG = [];

$(document).ready(function () {
    InicializarComponentes();
    InicializarFiltros();
    InicializarCategorizacion();
    InicializarAlta();
    InicializarDetallesAlergias();
});

// INICIALIZAR COMPONENTES
function InicializarComponentes() {

    sSession = getSession();
    //RevisarAcceso(false, sSession);
    vCodigoPerfil = sSession.CODIGO_PERFIL;
    $("textarea").val("");
    idDiv = '';
    $("input.bootstrapSwitch").bootstrapSwitch();

    if (vCodigoPerfil == 16 || vCodigoPerfil == 25)
        $('#lblNuevoIngreso').show();
    comboIdentificacion("#sltIdentificacionPaciente")
    //CargarComboIdentificadores();
    CargarFiltroAdmision().then(() => {

        MostrarSeleccionTipoAtencion().then(() => {
            CargarTablaUrgencia();
        });

    });

    $('#btnAnular').click(function (e) {

        if (validarCampos("#mdlAnularAtencion", false)) {

            let jsonAnular = {
                Observacion: $('#txtMotivoAnulacion').val()
            };

            $.ajax({
                type: 'DELETE',
                url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${vIdAtencion}`,
                contentType: 'application/json',
                data: JSON.stringify(jsonAnular),
                success: function (data) {
                    CargarTablaUrgencia();
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: "Se ha anulado la atención",
                        showConfirmButton: false,
                        timer: 3000
                    });
                    $('#mdlAnularAtencion').modal('hide');
                },
                error: function (jqXHR, status) {
                    console.log(`Error al anular atencion urgencia: ${JSON.stringify(jqXHR)}`);
                }
            });
        }

        e.preventDefault();
    });
    $("#btnUrgFiltro").click(function () {

        deleteSession("FiltrosBandejaUrgencia")
        const _CargarTablaUrgencia = async () => {
            ShowModalCargando(true);
            await sleep(1);
            CargarTablaUrgencia();
        }

        _CargarTablaUrgencia().then(() => ShowModalCargando(false));

    });
    $("#btnUrgLimpiarFiltro").click(function () {
        deleteSession("FiltrosBandejaUrgencia")
        $("#sltTipoUrg, #sltPrioridad").selectpicker("val", []);
        $("#sltEstado").val("0");
        $("#txtnumeroDocPaciente, #txtDigPaciente, #txtNombrePaciente, #txtApePatPaciente, #txtApeMatPaciente").val("");
        CargarTablaUrgencia();
    });
    $("#aGuardarSignoVital").click(function () {
        GuardarSignosVitales($(this).data("id"));
    });

}
function InicializarFiltros() {

    $("#txtnumeroDocPaciente").blur(function () {
        // SI ES RUT O RUT MATERNO
        if ($(`#selTipoIdentificacion`).val() == '1' || $(`#selTipoIdentificacion`).val() == '4') {
            var dig = ObtenerVerificador($(this).val());
            (dig !== null) ? $(`#txtDigPaciente`).val(dig) : $(`#txtDigPaciente, #txtnumeroDocPaciente`).val("");
        }
    });
    $("#selTipoIdentificacion").change(function () {

        $(`label[for='${$(this).attr("id")}']`).text($(this).children("option:selected").text());
        var parent = $(this).parent().parent().parent();
        ($(this).val() == '1' || $(this).val() == '4') ? $(`#${parent.attr("id")} .digito`).show() : $(`#${parent.attr("id")} .digito`).hide();

        $("#txtnumeroDocPaciente, #txtDigPaciente").val("");

    });
    comboTipoEstado();

}
function InicializarCategorizacion() {

    $('#btnRecategorizarC2').click(function (e) {
        vRecategorizar = true;
        categorizar(2);
        e.preventDefault();
    });
    $('#btnRecategorizarC3').click(function (e) {
        vRecategorizar = true;
        categorizar(3);
        e.preventDefault();
    });
    $('#btnRecategorizarC4').click(function (e) {
        vRecategorizar = true;
        categorizar(4);
        e.preventDefault();
    });
    $('#btnRecategorizarC5').click(function (e) {
        vRecategorizar = true;
        categorizar(5);
        e.preventDefault();
    });

    $("#btnRefrescar").click(function (e) {
        CargarTablaUrgencia();
    });
    $('#btnSiAmenaza').click(function (e) {

        $('#divOverlayCat1').show();
        $('#tCat').html('Categorización: C1');
        vCategorizacion = 1;
        $('#tEstimado').html('La atención debe ser inmediata');

        // Reemplazar esto con una llamada a la api
        $('#categorizarPaciente').modal('hide');
        $("#categorizarPaciente").on('hidden.bs.modal', function (e) {
            // A modo de prueba
            $('#pacienteCategorizado').modal('show');
            $(e.currentTarget).unbind();
        });

    });
    $('#btnNoAmenaza').click(function (e) {
        $('#divOverlayCat1').show();
        $('#item01').slideDown('fast');
        $('#item02').slideDown('fast');
        $('#item03').slideDown('fast');
        $('#divOverlayCat2').hide();
    });
    $('#btnNinguno').click(function (e) {

        $('#divOverlayCat3').show();

        $('#tCat').html('Categorización: C5');
        vCategorizacion = 5;
        $('#tEstimado').html('Paciente categorizado');

        // Reemplazar esto con una llamada a la api
        $('#categorizarPaciente').modal('hide');
        $("#categorizarPaciente").on('hidden.bs.modal', function (e) {
            $('#pacienteCategorizado').modal('show');
            $(e.currentTarget).unbind();
        });

    });
    $('#btnUno').click(function (e) {

        $('#divOverlayCat3').show();

        $('#tCat').html('Categorización: C4');
        vCategorizacion = 4;
        $('#tEstimado').html('Se debe atender dentro de 180 minutos');

        // Reemplazar esto con una llamada a la api
        $('#categorizarPaciente').modal('hide');
        $("#categorizarPaciente").on('hidden.bs.modal', function (e) {
            $('#pacienteCategorizado').modal('show');
            $(e.currentTarget).unbind();
        });

    });
    $('#btnMasdeuno').click(function (e) {
        $('#divOverlayCat3').show();
        $('#divOverlayCat4').hide();
        $('#divOverlayCat6').hide();
    });
    $('#btnAceptarAnte').click(function (e) {

        if (vOpcion == 1) {

            $('#divOverlayCat5').show();
            $('#tCat').html('Categorización: C3');
            vCategorizacion = 3;
            $('#tEstimado').html('Se debe atender dentro de 90 minutos');

            // Reemplazar esto con una llamada a la api
            $('#categorizarPaciente').modal('hide');
            $('#categorizarPaciente').on('hidden.bs.modal', function (e) {
                $('#pacienteCategorizado').modal('show');
                $(e.currentTarget).unbind();
            });
            $('#divCatEnfermero').show();

            $('#divRecategorizarC4').show();
            $('#divRecategorizarC5').show();

        } else if (vOpcion == 2) {

            if ($('#switchEpidemiologicos').prop('checked')) {

                $('#divOverlayCat5').show();
                $('#tCat').html('Categorización: C3');
                vCategorizacion = 3;
                $('#tEstimado').html('Se debe atender dentro de 90 minutos');

                // Reemplazar esto con una llamada a la api
                $('#categorizarPaciente').modal('hide');
                $('#categorizarPaciente').on('hidden.bs.modal', function (e) {
                    $('#pacienteCategorizado').modal('show');
                    $(e.currentTarget).unbind();
                });
                $('#divCatEnfermero').show();

                $('#divRecategorizarC4').show();
                $('#divRecategorizarC5').show();
            } else {
                if ($('#checkRequiereRec').prop('checked')) {
                    $('#tCat').html('Categorización: C4');
                    vCategorizacion = 4;
                    $('#tEstimado').html('Se debe atender dentro de 180 minutos');

                    // Reemplazar esto con una llamada a la api
                    $('#categorizarPaciente').modal('hide');
                    $("#categorizarPaciente").on('hidden.bs.modal', function (e) {
                        $('#pacienteCategorizado').modal('show');
                        $(e.currentTarget).unbind();
                    });
                } else {
                    $('#tCat').html('Categorización: C5');
                    vCategorizacion = 5;
                    $('#tEstimado').html('Paciente categorizado');

                    // Reemplazar esto con una llamada a la api
                    $('#categorizarPaciente').modal('hide');
                    $("#categorizarPaciente").on('hidden.bs.modal', function (e) {
                        $('#pacienteCategorizado').modal('show');
                        $(e.currentTarget).unbind();
                    });
                }
            }
        }
        e.preventDefault();
    });
    $('#switchEpidemiologicos').on('switchChange.bootstrapSwitch', function () {
        if (vOpcion == 2) {
            if ($('#switchEpidemiologicos').prop('checked'))
                $('#divOverlayEpidemio').show();
            else
                $('#divOverlayEpidemio').hide();
        }
    });

    $('#btnAceptarDatos').click(function (e) {
        ValidarCategorizacion();
        e.preventDefault();
    });
    $('.clickCat2').mouseup(function (e) {

        setTimeout(function () {

            if ($('#toggle01').children('.active').length > 0 && $('#toggle02').children('.active').length > 0 && $('#toggle03').children('.active').length > 0) {
                $('#divOverlayCat2').show();

                var v1 = $('#toggle01').children('.active').children().attr('cat');
                var v2 = $('#toggle02').children('.active').children().attr('cat');
                var v3 = $('#toggle03').children('.active').children().attr('cat');

                if (v1 == '1' && v2 == '1' && v3 == '2') {

                    if (vDiasPaciente > 1095) {
                        $('#divOverlayCat3').hide();
                    } else {
                        $('#divOverlayCat3').show();
                        $('#divOverlayCat4').hide();
                        $('#divOverlayCat6').hide();
                    }

                } else {
                    $('#tCat').html('Categorización: C2');
                    vCategorizacion = 2;
                    $('#tEstimado').html('Se debe atender dentro de 30 minutos');

                    //Reemplazar esto con una llamada a la api
                    $('#categorizarPaciente').modal('hide');
                    $("#categorizarPaciente").on('hidden.bs.modal', function (e) {

                        // A modo de prueba
                        $('#pacienteCategorizado').modal('show');
                        $(e.currentTarget).unbind();

                    });
                }

            }

        }, 100);
    });
    $('#btnNuevo').click(function (e) {

        $("#item01, #item02, #item03, #divOverlayCat1, #divEpidemio").hide();
        $("#divOverlayCat2, #divOverlayCat3, #divOverlayCat4, #divOverlayCat5, #divOverlayEpidemio").show();
        $("#toggle00, #toggle01, #toggle02, #toggle03, #toggle04").children().removeClass('active');
        $("#txtSat, #txtFR, #txtFC, #txtTemp").removeClass('is-invalid');
        $("#txtSat, #txtFR, #txtFC, #txtTemp").val('');
        $("#checkRequiereRec").prop('checked', true);
        $("#checkNoRequiereRec").prop('checked', false);
        $("#switchEpidemiologicos").bootstrapSwitch('state', true);
        vEpidemiologico = false;
        $("#categorizarPaciente").animate({
            scrollTop: $('#catC1').offset().top
        }, 250);
        e.preventDefault();

    });

    $('#btnCancelarCategorizacion').click(function (e) {
        $("#pacienteCategorizado").modal('hide');
        $("#categorizarPaciente").modal('show');
        $('#btnNuevo').trigger("click");
        e.preventDefault();
    });

    $('#btnAceptarCategorizacion').click(function (e) {
        categorizar(vCategorizacion);
        e.preventDefault();
    });

}
function InicializarAlta() {

    $("input[name='TE']").on('switchChange.bootstrapSwitch', function (event, state) {

        const id = $(this).attr("id");
        ReiniciarAlta();

        if (id === "rdoTipoEgresoNormal" && $(this).bootstrapSwitch('state')) {

            if ($("#sltProcedimiento").val() == "NO") {
                if ($("#divInfoClinicaUrgencia").is(":hidden")) {

                    $("#rdoTipoEgresoNormal").bootstrapSwitch('radioAllOff', true);
                    $("#rdoTipoEgresoNormal").bootstrapSwitch('state', false, true);
                    $("#rdoTipoEgresoNormal").bootstrapSwitch('radioAllOff', false);

                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: "El DAU no posee un ingreso médico.",
                        showConfirmButton: false,
                        timer: 3000
                    })

                    return;
                }
            }

            if (vCodigoPerfil == 1) {

                $("#divCondicionPaciente").show();
                $("#divEgresoNormal, #divEgresoIrregular").hide();

            } else if (vCodigoPerfil == 25) {

                if ($("#divEgresoMatroneria").data("pendientes") == true) {
                    $("#divEgresoNormal").show();
                    $("#btnEgresarAlta").hide();
                } else if (!$("#divEgresoMatroneria").data("pendiente-alta-medica")) {
                    alert($("#txtDNumeroDau").val());
                    ShowMensajeEgreso($("#txtDNumeroDau").val());
                } else {
                    $("#divCondicionPaciente").show();
                    $("#divEgresoNormal, #divEgresoIrregular").hide();
                    $("#btnEgresarAlta").show();
                }

            } else {

                if ($("#divAtencionClinica").data("pendientes") == true) {
                    $("#btnEgresarAlta").hide();
                } else {
                    $("#btnEgresarAlta").show();
                }

                $("#divAtencionClinica").show();
                $("#divEgresoNormal, #divEgresoIrregular, #divIndicadoresIndicaciones").hide();

            }

        } else {
            $("#divEgresoIrregular, #btnEgresarAlta").show();
            $("#divEgresoNormal, #divCondicionPaciente").hide();
            $("#rdoVivo, #rdoFallecido").bootstrapSwitch('radioAllOff', true);
            $("#rdoVivo, #rdoFallecido").bootstrapSwitch('state', false);
            $("#rdoVivo, #rdoFallecido").bootstrapSwitch('radioAllOff', false);
        }

    });

    $("input[name='CP']").on('switchChange.bootstrapSwitch', function (event, state) {

        const id = $(this).attr("id");
        $("#btnEgresarAlta").show();

        if ($("#rdoTipoEgresoNormal").bootstrapSwitch('state'))
            $("#divEgresoNormal").show();

        if (id === "rdoFallecido" && $(this).bootstrapSwitch('state')) {
            $("#sltDestinoAltaPaciente option[value='5']").show();
            $("#sltDestinoAltaPaciente").val("5").attr("disabled", "disabled").change();
        } else {
            $("#sltDestinoAltaPaciente").val("0").removeAttr("disabled");
            $("#sltDestinoAltaPaciente option[value='5']").hide();
        }

    });

}
function InicializarDetallesAlergias() {

    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").on('switchChange.bootstrapSwitch', function (event, state) {

        const id = $(this).attr("id");

        if (id === "rdoAlergiaSi" && $(this).bootstrapSwitch('state')) {
            $("#divDetalleAlergias").show();
            $("#txtDetalleAlergias").attr("data-required", true);
        } else if ((id === "rdoAlergiaNo" && $(this).bootstrapSwitch('state'))
            || (id === "rdoAlergiaDesconocido" && $(this).bootstrapSwitch('state'))) {
            $("#divDetalleAlergias").hide();
            $("#txtDetalleAlergias").attr("data-required", false);
        }

        ReiniciarRequired();

    });

    $("#divDetalleAlergias").hide();

}
async function MostrarSeleccionTipoAtencion() {

    switch (sSession.CODIGO_PERFIL) {
        case 25:
            $("#sltTipoUrg").selectpicker("val", '2');
            break;
        case 16: case 17: case 18: case 21: case 22: case 23:
            //$("#sltTipoUrg").selectpicker("val", ['1', '3']);
            if (localStorage.getItem("TIPO_ATENCION") == null || localStorage.getItem("TIPO_ATENCION") == undefined) {

                const { value: tipoAtencion } = await Swal.fire({
                    title: 'Seleccione Tipo de atención',
                    input: 'radio',
                    allowOutsideClick: false,
                    inputOptions: {
                        '1': 'Urgencia General',
                        '2': 'Urgencia Gineco-obstétrico'
                    },
                    inputValidator: (value) => {
                        if (!value) {
                            return 'Debe seleccionar una opción'
                        }
                    }
                })

                localStorage.setItem("TIPO_ATENCION", tipoAtencion);
            }

            $("#sltTipoUrg").selectpicker("val", (localStorage.getItem("TIPO_ATENCION") === "1" ) ? ['1', '3'] : '2');

            break;
    }
}

// COMBOS
async function CargarFiltroAdmision() {
    await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/Bandeja/Filtros`,
        success: function (data, status, jqXHR) {
            comboTipoUrgencia(data.TiposAtencion);
            comboPrioridad(data.TiposCategorizacion);
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar campos de filtros: ${JSON.stringify(jqXHR)}`);
        }
    });
}

function comboPrioridad(array) {

    $.each(array, function (i, r) {

        var option = "";

        switch (r.Valor) {
            case "C1":
                option =
                    `<option data-content="<span class='badge badge-pill badge-danger badge-count'>C1</span>" 
                        value='${r.Id}'>
                        ${r.Valor}
                    </option>`;
                break;
            case "C2":
                option =
                    `<option data-content="<span class='badge badge-pill badge-orange badge-count'>C2</span>" 
                        value='${r.Id}'>
                        ${r.Valor}
                    </option>`;
                break;
            case "C3":
                option =
                    `<option data-content="<span class='badge badge-pill badge-warning badge-count'>C3</span>" 
                        value='${r.Id}'>
                        ${r.Valor}
                    </option>`;
                break;
            case "C4":
                option =
                    `<option data-content="<span class='badge badge-pill badge-success badge-count'>C4</span>" 
                        value='${r.Id}'>
                        ${r.Valor}
                    </option>`;
                break;
            case "C5":
                option =
                    `<option data-content="<span class='badge badge-pill badge-info badge-count'>C5</span>" 
                        value='${r.Id}'>
                        ${r.Valor}
                    </option>`;
                break;
            case "S/C":
                if (sSession.CODIGO_PERFIL != 1)
                    option =
                        `<option data-content="<span class='badge badge-pill badge-dark badge-count'>S/C</span>" 
                            value='${r.Id}'>
                            ${r.Valor}
                        </option>`;
                break;
        }

        $("#sltPrioridad").append(option);
    });

    $('#sltPrioridad').selectpicker('refresh');
}
function comboEstablecimiento(sender) {

    let url = `${GetWebApiUrl()}GEN_Establecimiento/Combo`;
    setCargarDataEnCombo(url, false, sender);
    sender.val('0');
}
function comboTipoBox(idUbicacion) {
    $("#inpIdUbicacion").data("id-ubicacion",idUbicacion);
    //Escribe el nombre en el modal.
    //if (vCodigoPerfil == 25)
    let url = `${GetWebApiUrl()}URG_Tipo_Box/Combo/TipoAtencion/${idUbicacion}`;
    setCargarDataEnCombo(url, false, $("#sltTipoBox"));
}
//Esta funcion posiblemente quede obsoleta
function comboServicioBox(nombrePaciente) {
    $("#lblPasaraBox").empty();
    $("#lblPasaraBox").append(nombrePaciente);
    $("#sltServicioBox").empty();
    //Cargar combo tipo atencion
    if ($("#sltServicioBox").has('option').length === 0) {
        let url = `${GetWebApiUrl()}/URG_Tipo_Atencion/Combo`;
        setCargarDataEnCombo(url, true, $("#sltServicioBox"));
    }
    $("#sltServicioBox").unbind().change(function () {

        if ($("#sltServicioBox").val() != '0' && $("#sltServicioBox").val() != null) {
            comboTipoBox($("#sltServicioBox").val());
            $('#sltTipoBox').removeAttr('disabled');
            $('#sltNombreBox').attr('disabled', 'disabled').val('0');
        } else {
            $('#sltTipoBox').attr('disabled', 'disabled').val('0');
            $('#sltNombreBox').attr('disabled', 'disabled').val('0');
        }
    
    });

}
function comboNombreBox() {

    if ($("#sltTipoBox").val() != '0' && $("#sltTipoBox").val() != null) {
        let url = `${GetWebApiUrl()}URG_Box/Combo/Tipo/${$("#sltTipoBox").val()}/TipoAtencion/${$("#sltServicioBox").val()}`;
        setCargarDataEnCombo(url, true, $("#sltNombreBox"));
        $('#sltNombreBox').removeAttr('disabled');
    } else {
        $('#sltNombreBox').val('0').attr('disabled', 'disabled');
    }

}
function comboPrevision() {

    $('#selPrevision').empty().append(`<option value='0'>-Seleccione-</option>`);

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Prevision/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (i, r) {
                $('#selPrevision').append(`<option value='${r.Id}'>${r.Valor}</option>`);
            });
        }
    });
}
function comboPrevisionTramo(e) {
    let url = `${GetWebApiUrl()}GEN_Prevision_Tramo/GEN_idPrevision/${e}`;
    setCargarDataEnCombo(url, false, $("#selPrevisionTramo"));

}
function comboCategorizacionObstetrico() {

    const url = `${GetWebApiUrl()}URG_Categorizacion_Obstetrica/Combo`;
            $("#sltCategorizacionObstetrico").empty();

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            $("#sltCategorizacionObstetrico").append("<option value='0'>Seleccione</option>");
            let optGroup = null, htmlOptions = "";

            data.forEach(function (val, index) {

                let tiempoEstimado = "";
                switch (val.IdCategorizacion) {
                    case 1:
                        tiempoEstimado = `Es evaluada de inmediato por profesional matrona y derivada a medico según la urgencia.`;
                        break;
                    case 2:
                        tiempoEstimado = `Categorización dentro de los 10 minutos desde la llegada de la paciente y atención por profesional ` +
                            `matrona y/o médico dentro de los 30 minutos desde la hora de categorización.`;
                        break;
                    case 3:
                        tiempoEstimado = `Categorización dentro de los 30 minutos desde la llegada de la paciente y atención por profesional ` +
                            `matrona y/o médico dentro de los 90 minutos desde la hora de categorización o en su defecto reevaluar. `;
                        break;
                    case 4:
                        tiempoEstimado = `Categorización dentro de los 60 minutos desde la llegada de la paciente y atención por profesional ` +
                            `matrona y/o médico dentro de los 180 minutos desde la hora de categorización o en su defecto reevaluar.`;
                        break;
                    case 5:
                        tiempoEstimado = `Paciente espera hasta que se resuelva la atención de las usuarias categorizadas como C1, C2, C3 y C4. ` +
                            `Es una consulta que no debió haber llegado a este nivel de complejidad de la atención en red. Se debe educar para ` +
                            `consulta en Atención Primaria de Salud.`;
                        break;
                }

                if (optGroup != val.DescripcionCategorizacion)
                    $("#sltCategorizacionObstetrico").append(`<optgroup label="${val.DescripcionCategorizacion}"></optgroup>`);

                $(`#sltCategorizacionObstetrico optgroup[label="${val.DescripcionCategorizacion}"]`).append(
                    `<option value='${val.Id}'>${val.Valor}</option>`);

                $(`#sltCategorizacionObstetrico option[value='${val.Id}']`).data("id-Categorizacion", val.IdCategorizacion);
                $(`#sltCategorizacionObstetrico option[value='${val.Id}']`).data("tiempo-estimado", tiempoEstimado);
                $(`#sltCategorizacionObstetrico option[value='${val.Id}']`).data("descripcion-categorizacion", val.DescripcionCategorizacion);

                optGroup = val.DescripcionCategorizacion;

            });


        },
        error: function (jqXHR, status) {
            console.log(`Error URL: ${url} ${JSON.stringify(jqXHR)}`);
        }
    });

}
function comboTipoEstado() {
    $("#sltEstado").empty();
    $("#sltEstado").append(`<option value="0">Seleccione</option>`);
    $("#sltEstado").append(`<option value="100">Paciente con DAU</option>`);
    $("#sltEstado").append(`<option value="102">Paciente Categorizado</option>`);
    $("#sltEstado").append(`<option value="103">Paciente en Box</option>`);
    $("#sltEstado").append(`<option value="104">Paciente en Atencion</option>`);
    $("#sltEstado").append(`<option value="105">Paciente dado de alta</option>`);
}

// CARGAR TABLA URGENCIA
function CargarTablaUrgencia() {
    //los filtros de busqueda de la bandeja de urgencia se guardan en la sesion segun requerimientos
    $("#avisoFiltros").empty()
    //Obtener filtros que se guardan en la sesion
    let niSession = getSession();
    let posiblesFiltros = niSession.FiltrosBandejaUrgencia
    let textFiltros = "", filtros
    //Existen filtros almacenados
    if (posiblesFiltros && posiblesFiltros.length > 0) {
        let separador =""
        textFiltros += "<span class='alert alert-warning'> <b><i class='fa fa-info-circle fa-lg'> </i>&nbsp;Filtrando por"
        $("#divFiltroUrg").addClass('show')
        posiblesFiltros.map((a,index) => {
            //Separador de palabras
            index == posiblesFiltros.length - 1 ? separador = "." : separador=","
            let nombre = " "+a.Nombre
            textFiltros += nombre + separador
            //Setear valores almacenados en la sesion  en los inputs 
            if (a.Clase == 'selectpicker') {
                $("#" + a.Id).selectpicker('val', a.Valor)
            } else {
                $("#" + a.Id).val(a.Valor)
            }
        })
        filtros = true
        textFiltros += "</b></span>"
        $("#avisoFiltros").append(textFiltros)
    } else {
        //No hay filtros de busqueda almacenados en la sesion
        filtros = false
        $("#avisoFiltros").empty()
    }

    var adataset = [];
    let valoresInputs = []
    let valoresSelect = []
    var url =
        `${GetWebApiUrl()}URG_Atenciones_Urgencia/Bandeja?` +
        `idcategorizacion=${($("#sltPrioridad").selectpicker("val") !== null) ? $("#sltPrioridad").selectpicker("val") : ""}` +
        `&idtipo=${($("#sltTipoUrg").selectpicker("val") !== null) ? $("#sltTipoUrg").selectpicker("val") : ""}` +
        `&numerodocumento=${($.trim($("#txtnumeroDocPaciente").val()) !== "") ? $.trim($("#txtnumeroDocPaciente").val()) : ""}` +
        `&nombre=${($.trim($("#txtNombrePaciente").val()) !== "") ? $.trim($("#txtNombrePaciente").val()) : ""}` +
        `&apellidopaterno=${($.trim($("#txtApePatPaciente").val()) !== "") ? $.trim($("#txtApePatPaciente").val()) : ""}` +
        `&apellidomaterno=${($.trim($("#txtApeMatPaciente").val()) !== "") ? $.trim($("#txtApeMatPaciente").val()) : ""}` +
        `&ididentificacion=${($("#selTipoIdentificacion").val() !== "0") ? $("#selTipoIdentificacion").val() : ""}` +
        `&idEstado=${($("#sltEstado").val() !== "0") ? $("#sltEstado").val() : ""}`;
    //Obtener inputs de filtro de busqueda bandeja urgencia
    let input = $("#divFiltroUrg").find('input')
    let selects = $("#divFiltroUrg").find('select')
    valoresInputs = input.map((index, e) => {
        if (e.value != "" && e.value != 0 && e.value != '<empty string>' && e.value != undefined) {
            return { Id: e.id, Nombre: e.name, Valor: e.value, Clase: $(e).attr('class') }
        }
    })
    valoresSelect = selects.map((index, e) => {
        //Omitir el tipo identificacion, porque viene con carga, no tiene el valor <seleccione>
        if ($(e).attr("name") != "Identificacion") {
            if (e.value != "" && e.value != 0 && e.value != '<empty string>' && e.value != undefined) {
                return { Id: e.id, Nombre: $(e).attr("name"), Valor: $(e).val(), Clase: $(e).attr('class') }
            }
        }
    })
    //Concatenar filtros de input con filtros de select
    let valoresFinales = [...valoresInputs].concat([...valoresSelect])
       //Si no hay filtros aun en el local storage, pero si se han ingresado filtros,//
      //se usan los filtros del arreglo  que se envia a la local storage.-----------//
     //Esto soluciona que no se puede acceder a los filtros almacenados en---------//
    //la local storage a menos que la pagina se recargue--------------------------//
    if (filtros == false) {
        $("#avisoFiltros").empty()
        textFiltros = ""
        let nombre = ""
        //accediendo al arreglo que se envió a la sesion
        valoresFinales.map((a, index) => {
            let separador = ""
            textFiltros = "<span class='alert alert-warning'> <b><i class='fa fa-info-circle fa-lg'> </i>&nbsp;Filtrando por"
            
            index == valoresFinales.length - 1 ? separador = "." : separador = ","
            nombre += " " + a.Nombre+separador
            textFiltros += nombre
            
        })
        textFiltros += "</b></span>"
        $("#avisoFiltros").append(textFiltros)
    }
    console.log(valoresFinales)
    setSession('FiltrosBandejaUrgencia', valoresFinales);
    // `&ididentificacion=${($("#selTipoIdentificacion").val() !== "0") ? $("#selTipoIdentificacion").val() : ""}`;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (i, r) {
                let nombrePaciente = (this.Paciente.Nombre != null) ? this.Paciente.Nombre : "";
                nombrePaciente += (this.Paciente.ApellidoPaterno != null) ? ` ${this.Paciente.ApellidoPaterno}` : "";
                nombrePaciente += (this.Paciente.ApellidoMaterno != null) ? ` ${this.Paciente.ApellidoMaterno}`  : "";

                let identificacionPaciente = (this.Paciente.IdIdentificacion == 5) ? "NN" : this.Paciente.NumeroDocumento;
                identificacionPaciente += this.Paciente.Digito != null ? "-" + this.Paciente.Digito : "";
                
                adataset.push([
                    
                /*0*/ r.Id,
                /*1*/ r.IdCategorizacion,
                /*2*/ (r.Categorizacion === null) ? '' : r.Categorizacion.IdCategorizacionAtencion,
                /*3*/ identificacionPaciente,
                /*4*/ $.trim(nombrePaciente),
                /*5*/ (r.FechaNacimientoPaciente != null) ? edad(r.FechaNacimientoPaciente) : "",
                /*6*/ (r.FechaNacimientoPaciente != null) ? edadDias(r.FechaNacimientoPaciente) : "",
                /*7*/ r.FechaAdmision == null ? r.FechaAdmision : moment(r.FechaAdmision).format('DD-MM-YYYY HH:mm'),
                /*8*/ (r.Prevision == null) ? '' : r.Prevision,
                /*9*/ (r.PrevisionTramo == null) ? '' : r.PrevisionTramo,
                /*10*/ r.IdTipoAtencion,
                /*11*/ r.MotivoConsulta,
                /*12*/ r.Categorizacion,
                /*13*/ r.Estado,
                /*14*/ '',
                /*15*/ r.Acciones.Anular,
                /*16*/ r.Acciones.Editar,
                /*17*/ r.Acciones.DarAlta,
                /*18*/ r.Acciones.Categorizar,
                /*19*/ r.Acciones.EnviaraBox,
                /*20*/ r.Acciones.AtenderPorMedico,
                /*21*/ r.Acciones.TomarSignosVitales,
                /*22*/ r.Acciones.AtencionClinica,
                /*23*/ r.Box,
                /*24*/ r.Acompañante,
                /*25*/ r.TipoClasificacion,
                /*26*/ r.Acciones.LiberarBox,
                /*27*/ r.FechaAltaMedica,
                /*28*/ r.FechaAltaEnfermeria,
                /*29*/ r.Acciones.EgresarAdministrativo,
                /*30*/ r.Acciones.ImprimirExamenes,
                /*31*/ r.Acciones.LiquidarCuenta,
                /*32*/ r.Paciente.IdPaciente
                ]);
            });
            setJsonExportarURG(data);
            $('#tblUrgencia').DataTable({
                data: adataset,
                responsive: {
                    details: false,
                    type: 'column'
                },
                "aaSorting": [[0, 'desc']],
                columns: [
                /*0*/ { title: 'ID', className: "text-center font-weight-bold rce-tray-id" },
                /*1*/ { title: 'IDCategorizacion' },
                /*2*/ { title: 'IDDatosCategorizacion' },
                /*3*/ { title: 'Nº Dcto.' },
                /*4*/ { title: 'Nombre paciente' },
                /*5*/ { title: 'Edad paciente', className: "text-center" },
                /*6*/ { title: 'EdadDias' },
                /*7*/ { title: 'Fecha Admisión' },
                /*8*/ { title: 'Previsión' },
                /*9*/ { title: 'Tramo' },
                /*10*/{ title: 'Alertas', className: "text-center" },
                /*11*/{ title: 'Motivo consulta' },
                /*12*/{ title: 'Categorización' },
                /*13*/{ title: 'Estado' },
                /*14*/{ title: '', className: "text-center" },
                /*15*/{ title: 'Anulable' },
                /*16*/{ title: 'Editable' },
                /*17*/{ title: 'Alta' },
                /*18*/{ title: 'Categorizable' },
                /*19*/{ title: 'Ingresable a box' },
                /*20*/{ title: 'ingreso Medico Urgencia' },
                /*21*/{ title: 'Signos vitales' },
                /*22*/{ title: 'Atención clínica' },
                /*23*/{ title: 'Box' },
                /*24*/{ title: 'Acompañante' },
                /*25*/{ title: 'Procedimiento' },
                /*26*/{ title: 'LiberarBox' },
                /*27*/{ title: 'Fecha Alta médica' },
                /*28*/{ title: 'Fecha Alta Enfermeria' },
                /*29*/{ title: 'Egreso Administrativo' },
                /*30*/{ title: 'Imprimir Examenes' },
                /*31*/{ title: 'Liquidar Cuenta' },
                /*32*/{ title: 'id-paciente' }
                ],
                columnDefs: [
                    { targets: [1, 2, 6, 12, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,30, 31, 32], visible: false, searchable: false },
                    { type: 'date-uk', targets: 7 },
                    { targets: [5], visible: vCodigoPerfil != '17', searchable: vCodigoPerfil != '17' },
                    { targets: [8, 9], visible: vCodigoPerfil == '16' ? true : false, searchable: vCodigoPerfil == '16' ? true : false },
                    {
                        targets: [10],
                        render: function (data, type, row, meta) {

                            var fila = meta.row;
                            var botones;

                            // Dibuja Icono de Tipo Atencion
                            botones = dibujarIconoTipoAtencion(adataset[fila][10]);

                            if (adataset[fila][24] == "SI")
                                botones += `<i class="fa fa-user" aria-hidden="true"></i>`;

                            if (adataset[fila][25] == "Procedimiento")
                                botones += `<i class="fa fa-medkit fa-lg text-red" aria-hidden="true"></i>`;

                            if (adataset[fila][12] != null)
                                botones += dibujarIconoCategorizacion(adataset[fila][12]);

                            //Nombre (Tipo) de Box
                            if (adataset[fila][23] != null)
                                botones += `<h6 class="text-primary">${adataset[fila][23].NombreBox}</h6>`;
                            //Alta enfermeria matroneria
                            if (adataset[fila][27] != null)
                                botones += `<div class='mb-2'><span class="badge badge-pill btn-success" style='font-size:15px'><i class="fas fa-check"></i> Alta Médica</span></div>`;

                            if (adataset[fila][28] != null)
                                botones += `<span class="badge badge-pill btn-success" style='font-size:15px'><i class="fas fa-check"></i> Alta Enfermería</span>`;

                            //alta matroneria
                            return botones;
                        }
                    },
                    {
                        targets: 14,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {

                            var fila = meta.row;
                            var botones =
                                `
                                    <button class='btn btn-primary btn-circle' 
                                        onclick='toggle("#div_accionesURG${fila}"); return false;'>
                                        <i class="fa fa-list" style="font-size:15px;"></i>
                                    </button>
                                    <div id='div_accionesURG${fila}' class='btn-container' style="display: none;">
                                        <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                        <div class="rounded-actions">`;
                            //Editable?
                            if (adataset[fila][16]) {
                                botones +=
                                    `
                                        <a id='linkEditar_${fila}' data-id='${adataset[fila][0]}' data-count-box='${adataset[fila][23] == null ? 0 : 1}'
                                            class='btn btn-success btn-circle btn-lg' href='#/' onclick='linkEditar(this)' data-toggle='tooltip'
                                            data-placement='left' title='Editar'><i class='fa fa-edit'></i>
                                        </a><br />`;
                            }
                            //Signos vitales
                            if (adataset[fila][21]) {
                                botones +=
                                    `
                                        <a id='linkSignosVitales_${fila}' data-id='${adataset[fila][0]}' class='btn btn-primary btn-circle btn-lg' 
                                            href='#/' onclick='linkSignosVitales(this)' data-toggle="tooltip" data-placement="left" 
                                            title="Signos vitales" data-pac='${adataset[fila][4]}' data-categorizacion='${adataset[fila][12]}'>
                                            <i class='fa fa-heartbeat'></i>
                                        </a><br />`;
                            }
                            //Categorizable?
                            if (adataset[fila][18]) {

                                if (adataset[fila][1] == null || adataset[fila][1] == '' || adataset[fila][1] == undefined) {
                                    botones +=
                                        `
                                            <a id='linkCategorizar_${fila}' data-id='${adataset[fila][0]}' data-pac='${adataset[fila][4]}' 
                                                data-dias='${adataset[fila][6]}' class='btn btn-info btn-circle btn-lg' href='#/' 
                                                onclick='linkCategorizar(this)' data-toggle='tooltip' data-placement='left' title='Categorizar'>
                                                <i class='fas fa-plus'></i>
                                            </a><br />`;
                                } else {
                                    botones +=
                                        `
                                            <a id='linkverCategorizacion_${fila}' data-pac='${adataset[fila][4]}' data-dat='${adataset[fila][2]}' 
                                                class='btn btn-info btn-circle btn-lg' href='#/' onclick='verCategorizacion(this)'
                                                data-toggle='tooltip' data-placement='left' title='Ver detalles'>
                                                <i class='fab fa-elementor'></i>
                                            </a><br />`;
                                }

                            }
                            //Ingresar a box? 
                            if (adataset[fila][19]) {
                                
                                botones +=
                                    `
                                        <a id='linkAsignarBox_${fila}' data-id='${adataset[fila][0]}' data-estado='${adataset[fila][10]}'
                                            data-id-categorizacion='${adataset[fila][12]}' data-nombrepaciente='${adataset[fila][4]}' class='btn btn-warning btn-circle btn-lg' href='#/'
                                            onclick='linkAsignarBox(this)' data-toggle='tooltip' data-placement='left' title='Enviar a Box'>
                                            <i class="fas fa-procedures"></i>
                                        </a><br />`;
                            }
                            //Atención clínica
                            if (adataset[fila][22]) {
                                botones +=
                                    `
                                        <a id='linkAtencionClinica_${fila}' data-id='${adataset[fila][0]}' class='btn btn-success btn-circle btn-lg'
                                            href='#/' onclick='linkAtencionClinica(this)' data-toggle='tooltip' data-count-box='${adataset[fila][23] == null ? 0 : 1}'
                                            data-placement='left' title='Atención clínica'><i class="far fa-file-alt"></i>
                                        </a><br />`;
                            }

                            //Atención Medico Urgencia
                            if (adataset[fila][20]) {

                                botones +=
                                    `
                                        <a id='linkIngresarMedicoUrgencia_${fila}' data-id='${adataset[fila][0]}' data-count-box='${adataset[fila][23] == null ? 0 : 1}'
                                            class='btn btn-success btn-circle btn-lg' href='#/' onclick='linkEditar(this)' data-toggle='tooltip'
                                            data-placement='left' title='${vCodigoPerfil == 25 ? 'Atención Matronería Urgencia' : 'Atención Médica Urgencia'}'>
                                            <i class="fas fa-stethoscope"></i>
                                        </a><br />`;

                            }

                            //Agregar Atención Matroneria

                            //Liberar Box
                            if (adataset[fila][26]) {
                                botones +=
                                    `
                                        <a id='linkAsignarBox_${fila}' data-id='${adataset[fila][0]}' class='btn btn-warning btn-circle btn-lg' href='#/' 
                                            onclick='linkLiberarBox(this)' data-toggle='tooltip' data-placement='left' title='Liberar Box'>
                                            <i class="fas fa-sign-out-alt"></i>
                                        </a><br />`;
                            }

                            //Egreso / Alta
                            if (adataset[fila][17]) {
                                botones +=
                                    `
                                        <a id='linkAltaMedica_${fila}' data-id='${adataset[fila][0]}' class='btn btn-info btn-circle btn-lg' 
                                            data-toggle='tooltip' data-placement='left'
                                            href='#/' onclick='linkEgresoIngresoUrgencia(this)' title='Egresar / Dar alta'>
                                            <i class="fas fa-angle-double-right"></i>
                                        </a><br />`;
                            }
                            //Anulable?
                            if (adataset[fila][15]) {
                                botones +=
                                    `
                                        <a id='linkAnular_${fila}' data-id='${adataset[fila][0]}' class='btn btn-danger btn-circle btn-lg' 
                                            href='#/' onclick='linkAnular(this)' data-toggle='tooltip' data-placement='left' title='Anular'>
                                            <i class='fa fa-ban'></i>
                                        </a><br />`;
                            }

                            //Egreso Administrativo
                            if (adataset[fila][29]) {
                                botones +=
                                    `<a id='linkEgresoAdministrativo_${fila}' data-id='${adataset[fila][0]}' class='btn btn-info btn-circle btn-lg'
                                            data-toggle='tooltip' data-placement='left'
                                            href='#/' onclick='linkEgresoAdministrativo(this)' title='Egresar Paciente'>
                                            <i class="fas fa-angle-double-right"></i>
                                        </a><br />`;
                            }

                            if (adataset[fila][30]) {
                                botones +=
                                    `
                                    <a id='linkImprimir_${fila}' data-id='${adataset[fila][0]}' class='btn btn-info btn-circle btn-lg' href='#/' 
                                        data-print='true' onclick='linkImprimirExamenes(${adataset[fila][0]})' data-toggle="tooltip" data-placement="left"
                                        title="Imprimir Exámenes">
                                        <i class="far fa-file-pdf"></i>
                                    </a><br />`;
                            }

                            if (adataset[fila][31]) {
                                botones +=
                                    `
                                    <a id='linkImprimir_${fila}' data-id='${adataset[fila][0]}' class='btn btn-info btn-circle btn-lg' href='#/' 
                                        data-print='true' onclick='linkImprimirExamenes(${adataset[fila][0]})' data-toggle="tooltip" data-placement="left"
                                        title="Imprimir Exámenes">
                                        <i class="far fa-file-pdf"></i>
                                    </a><br />`;
                            }
                            //VER HISTORIAL HOSPITALIZACIONES DEL PACIENTE
                            botones +=
                                `
                                    <a id='linkHistorialHosp' data-idpac='${adataset[fila][32]}' data-nompac='${adataset[fila][4]}'
                                        class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkHistorialHosp(this);' 
                                        data-toggle="tooltip" data-placement="left" title="Historial de Hospitalizaciones">
                                        <i class="fas fa-history"></i>
                                    </a><br>`;

                            //Imprimir
                            botones +=
                                `
                                    <a id='linkImprimir_${fila}' data-id='${adataset[fila][0]}' class='btn btn-info btn-circle btn-lg' href='#/' 
                                        data-print='true' onclick='linkImprimir(${adataset[fila][0]})' data-toggle="tooltip" data-placement="left"
                                        title="Imprimir">
                                        <i class='fas fa-print'></i>
                                    </a><br />`;

                            botones +=
                                `
                                        <a id='linkMovimientosIngresoUrg_${fila}' data-id='${adataset[fila][0]}' 
                                            class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkMovimientosIngresoUrgencia(this)' 
                                            data-toggle="tooltip" data-placement="left" title="Movimientos">
                                            <i class='fa fa-eye'></i>
                                        </a><br>
                                    </div>
                                </div>`;

                            return botones;

                        }
                    },
                    //ocultar columnas en responsivo.
                    { responsivePriority: 1, targets: 0 }, // Id
                    { responsivePriority: 2, targets: 14 }, // botonera
                    { responsivePriority: 3, targets: 4 }, // nombre
                    { responsivePriority: 4, targets: 10 }, // alertas
                    { responsivePriority: 5, targets: 11 }, // motivo consulta
                    { responsivePriority: 6, targets: 13 }, // Estado
                    { responsivePriority: 7, targets: 7 }, // Fecha de ingreso
                ],

                bDestroy: true
            });

            $('[data-toggle="tooltip"]').tooltip();
        }
    });
}
function dibujarIconoTipoAtencion(idTipoAtencion) {
    let classIcono;
    let letraIcono;

    if (idTipoAtencion == 1) {
        classIcono = "badge badge-pill btn-success badge-count";
        letraIcono = "A";
    }
    else if (idTipoAtencion == 2) {
        classIcono = "badge badge-pill btn-warning badge-count";
        letraIcono = "G";
    }
    else if (idTipoAtencion == 3) {
        classIcono = "badge badge-pill btn-info badge-count";
        letraIcono = "P";
    }

    return `<h5><span id="bgAtencion" 
                class="${classIcono}">${letraIcono}</span></h5>`;
}
function toggle(id) {
    if (idDiv == '') {
        $(id).fadeIn();
        idDiv = id;
    } else if (idDiv == id) {
        $(id).fadeOut();
        idDiv = '';
    } else {
        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }
}
function limpiarCampos() {

    $("#alertSignosVitales, #divOverlayCat1, #divOverlayCat5, #divOverlayCat6, #item01, #item02, #item03, #divEpidemio").hide();
    $("#catC1, #catC2, #catC3, #catC4, #divOverlayCat2, #divOverlayCat3, #divOverlayCat4, #divOverlayEpidemio").show();
    $("#toggle00, #toggle01, #toggle02, #toggle03, #toggle04").children().removeClass('active');
    $("#checkRequiereRec").prop('checked', true);
    $("#checkNoRequiereRec").prop('checked', false);
    $("#switchEpidemiologicos").bootstrapSwitch('state', true);
    vEpidemiologico = false;
    $("#txtSat, #txtFR, #txtFC, #txtTemp").removeClass('is-invalid');
    $("#txtSat, #txtFR, #txtFC, #txtTemp").val('');

}
function ActualizarAtencionUrgencia(json) {

    var esValido = false;

    $.ajax({
        type: "PUT",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${json.URG_idAtenciones_Urgencia}`,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            esValido = true;
        },
        error: function (jqXHR, status) {
            console.log("Error al guardar Egresar, derivar o cerrar: " + JSON.stringify(jqXHR));
        }
    });

    return esValido;

}

// CATEGORIZAR
function ValidarCategorizacion() {

    var bValido = true;
    if ($('#txtFR').val() == null || $('#txtFR').val() == undefined || $('#txtFR').val() == '' || $('#txtFR').val() < 5 || $('#txtFR').val() > 100) {
        $('#txtFR').addClass('is-invalid', true);
        bValido = false;
    }

    if ($('#txtFC').val() == null || $('#txtFC').val() == undefined || $('#txtFC').val() == '' || $('#txtFC').val() < 50 || $('#txtFC').val() > 200) {
        $('#txtFC').addClass('is-invalid', true);
        bValido = false;
    }

    if ($('#txtSat').val() == null || $('#txtSat').val() == undefined || $('#txtSat').val() == '' || $('#txtSat').val() < 50 || $('#txtSat').val() > 100) {
        $('#txtSat').addClass('is-invalid', true);
        bValido = false;
    }

    if (bValido) {
        bDatosVitales = true;
        if (vDiasPaciente <= 91) {//hasta 3 meses

            if ($('#txtTemp').val() == null || $('#txtTemp').val() == undefined || $('#txtTemp').val() == '' || $('#txtTemp').val() < 20 || $('#txtTemp').val() > 60) {
                $('#txtTemp').addClass('is-invalid', true);
                bValido = false;
            }

            if (bValido) {
                if ($('#txtFR').val() >= 50 || $('#txtFC').val() >= 180 || $('#txtSat').val() < 92 || $('#txtTemp').val() >= 38) {
                    DarResultadoCategorizacion({
                        IdTipoCategorización: 2, TipoCategorización: "C2", TiempoEstimado: 'Se debe atender dentro de 30 minutos'
                    });
                } else {
                    DarResultadoCategorizacion({
                        IdTipoCategorización: 3, TipoCategorización: "C3", TiempoEstimado: 'Se debe atender dentro de 90 minutos'
                    });
                }
            }

        } else if (vDiasPaciente >= 92 && vDiasPaciente <= 1095) {//3 meses - 3 años 

            // Paciente entre 3 meses a 3 años
            // C2
            if ($('#txtFR').val() >= 40 || $('#txtFC').val() >= 160 || $('#txtSat').val() < 92) {
                $('#divOverlayCat6').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 2, TipoCategorización: "C2", TiempoEstimado: 'Se debe atender dentro de 30 minutos'
                });
            }
            // C3
            if ($('#txtFR').val() < 40 && $('#txtFC').val() < 160 && $('#txtSat').val() >= 92) {
                if ($('#txtTemp').val() == null || $('#txtTemp').val() == undefined || $('#txtTemp').val() == '' || $('#txtTemp').val() < 20 || $('#txtTemp').val() > 60) {
                    $('#txtTemp').addClass('is-invalid', true);
                } else {
                    if ($('#txtTemp').val() >= 39) {
                        vEpidemiologico = true;
                        $('#divEpidemio').slideDown('fast');
                        $('#divOverlayCat6').show();
                        $('#divOverlayCat5').hide();
                        vOpcion = 1;
                    }
                    else {
                        vEpidemiologico = true;
                        $('#divEpidemio').slideDown('fast');
                        $('#divOverlayCat6').show();
                        $('#divOverlayCat5').hide();
                        vOpcion = 2;
                    }
                }
            }
            // 3 años 8 años
        } else if (vDiasPaciente >= 1096 && vDiasPaciente <= 2920) {

            if ($('#txtFR').val() >= 30 || $('#txtFC').val() >= 140 || $('#txtSat').val() < 92) {
                $('#divOverlayCat4').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 2, TipoCategorización: "C2", TiempoEstimado: 'Se debe atender dentro de 30 minutos'
                });
            } else {
                $('#divOverlayCat4').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 3, TipoCategorización: "C3", TiempoEstimado: 'Se debe atender dentro de 90 minutos'
                });
            }

        } else if (vDiasPaciente >= 2921) {

            // Mayor a 8 años
            if ($('#txtFR').val() < 20 && $('#txtFC').val() < 100 && $('#txtSat').val() >= 92) {

                $('#divOverlayCat4').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 3, TipoCategorización: "C3", TiempoEstimado: 'Se debe atender dentro de 90 minutos'
                });

            } else if (($('#txtFR').val() >= 21 && $('#txtFR').val() <= 24) && ($('#txtFC').val() >= 101 && $('#txtFC').val() <= 119) && ($('#txtSat').val() >= 88 && $('#txtSat').val() <= 91)) {

                $('#divOverlayCat4').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 2, TipoCategorización: "C2", TiempoEstimado: 'Se debe atender dentro de 30 minutos'
                });

            } else if ($('#txtFR').val() >= 25 || $('#txtFC').val() >= 120 || $('#txtSat').val() < 88) {

                $('#divOverlayCat4').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 2, TipoCategorización: "C2", TiempoEstimado: 'Se debe atender dentro de 30 minutos'
                });

            } else {

                DarResultadoCategorizacion({
                    IdTipoCategorización: 3, TipoCategorización: "C3", TiempoEstimado: 'Se debe atender dentro de 90 minutos'
                });
                $('#divCatEnfermero').show();
                $('#divRecategorizarC2').show();

            }
        }
    }

}
function categorizar(categorizacion) {

    if (validarCampos("#divCategorizacion", false)) {

        let json = {};
        json.IdAtencion = vIdAtencion;
        json.IdCategorizacion = categorizacion;
        json.FrecuenciaCardiaca = null;
        json.FrecuenciaRespiratoria = null;
        json.Saturacion = null;
        json.Temperatura = null;
        json.AntecedentesEpidemiologicos = null;
        json.Recursos = null;
        json.Observaciones = valCampo($("#txtObservacionesCategorizacion").val());
        json.Alergias = null;
        json.IdCategorizacionObstetrico = null;
        json.AntecedentesGinecoObstetrico = null;

        if (sSession.CODIGO_PERFIL == 25) {
            json.IdCategorizacionObstetrico = valCampo(parseInt($("#sltCategorizacionObstetrico").val()));
            json.AntecedentesGinecoObstetrico = valCampo($("#txtAntecedentesGinecoObstetrico").val());
        }

        if ($("#rdoAlergiaSi").bootstrapSwitch("state"))
            json.IdTipoAlternativaAntecedentes = 1;
        else if ($("#rdoAlergiaNo").bootstrapSwitch("state"))
            json.IdTipoAlternativaAntecedentes = 2;
        else if ($("#rdoAlergiaDesconocido").bootstrapSwitch("state"))
            json.IdTipoAlternativaAntecedentes = 3;

        json.Alergias = $("#rdoAlergiaSi").bootstrapSwitch("state") ? valCampo($("#txtDetalleAlergias").val()) : null;
        json.MotivoConsulta = valCampo($("#txtMotivoConsultaCat").val());
        json.IdTipoClasificacion = valCampo($("#sltTipoClasificacion").val());
        json.AntecedentesMorbidos = valCampo($("#txtAntecedentesMorbidos").val());
        json.AntecedentesQx = valCampo($("#txtAntecedentesQx").val());

        if (bDatosVitales) {

            json.FrecuenciaCardiaca = $('#txtFC').val();
            json.FrecuenciaRespiratoria = $('#txtFR').val();
            json.Saturacion = $('#txtSat').val();
            json.Temperatura = ($('#txtTemp').val() == null || $('#txtTemp').val() == undefined || $('#txtTemp').val() == '') ? null : $('#txtTemp').val();

            if (vEpidemiologico) {
                json.AntecedentesEpidemiologicos = $('#switchEpidemiologicos').prop('checked') ? 'SI' : 'NO';
                json.Recursos = $('#checkRequiereRec').prop('checked') ? 1 : 0;
            }

        }

        $.ajax({
            type: 'POST',
            url: `${GetWebApiUrl()}URG_Categorizaciones_Atencion`,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#pacienteCategorizado').modal('hide');
                CargarTablaUrgencia();
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: `El paciente ha sido categorizado`,
                    showConfirmButton: false,
                    timer: 3000
                });
            },
            error: function (e) {
                console.log("Error al guardar categorizacion: " + JSON.stringify(e));
            }
        });

    }

}
function verCategorizacion(e) {

    var dat = $(e).data('dat');
    limpiarCampos();
    $('#divOverlayCat1').show();
    $('#lblPacienteCat').html($(e).data('pac'));

    if (dat != null && dat != 'undefined' && dat != '') {

        $("#item01, #item02, #item03").show();
        $("#opcion01, #opcion05, #opcion08").parent().addClass('active');
        $("#btnMasdeuno, #btnNoAmenaza").addClass('active');
        $("#divEpidemio").show();

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Datos_Categorizacion/${dat}`,
            contentType: 'application/json',
            dataType: 'json',
            //async: false,
            success: function (data) {

                $('#txtFR').val(data[0].URG_frecuencia_respiratoriaDatos_Categorizacion);
                $('#txtFC').val(data[0].URG_frecuencia_cardiacaDatos_Categorizacion);
                $('#txtSat').val(data[0].URG_saturacionDatos_Categorizacion);
                $('#txtTemp').val(data[0].URG_temperaturaDatos_Categorizacion);

                if (data[0].URG_antecedentesEpidemiologicosDatos_Categorizacion != null &&
                    data[0].URG_antecedentesEpidemiologicosDatos_Categorizacion != undefined &&
                    data[0].URG_antecedentesEpidemiologicosDatos_Categorizacion != '') {
                    if (data[0].URG_antecedentesEpidemiologicosDatos_Categorizacion == 'SI')
                        $('#switchEpidemiologicos').bootstrapSwitch('state', true);
                    else
                        $('#switchEpidemiologicos').bootstrapSwitch('state', false);
                } else
                    $('#divEpidemio').hide();

                if (data[0].URG_recursosDatos_Categorizacion != null &&
                    data[0].URG_recursosDatos_Categorizacion != undefined &&
                    data[0].URG_recursosDatos_Categorizacion != '') {
                    if (data[0].URG_recursosDatos_Categorizacion == '1') {
                        $('#checkRequiereRec').prop('checked', true);
                        $('#checkNoRequiereRec').prop('checked', false);
                    }
                    else {
                        $('#checkNoRequiereRec').prop('checked', true);
                        $('#checkRequiereRec').prop('checked', false);
                    }
                } else
                    $('#divEpidemio').hide();

            }
        });
    } else {
        $("#alertSignosVitales").show();
        $("#catC1, #catC2, #catC3, #catC4").hide();
    }

    $("#btnNuevo").attr('disabled', true);
    $("#categorizarPaciente").modal("show");

}
function linkCategorizar(e) {

    const id = $(e).data('id');
    const esValidoSignosVitalesMatroneria = CargarInfoCategorizacion(id);
    if (esValidoSignosVitalesMatroneria) {
        bDatosVitales = false;
        vIdAtencion = id;

        if (sSession.CODIGO_PERFIL == 25) {

            $("#divCategorizacionObstetrico").show();
            $("#divCategorizacionNormal").hide();
            comboCategorizacionObstetrico();
            $("#btnCategorizacionObstetrico").unbind().on('click', (e) => {

                if (validarCampos("#divCategorizacionObstetrico", false)) {

                    const optSel = $("#sltCategorizacionObstetrico").find(":selected");
                    DarResultadoCategorizacion({
                        IdTipoCategorización: optSel.data("id-Categorizacion"),
                        TipoCategorización: optSel.data("descripcion-categorizacion"),
                        TiempoEstimado: optSel.data("tiempo-estimado")
                    });

                }

                e.preventDefault();
            });

        } else {

            $("#divCategorizacionNormal").show();
            $("#divCatEnfermero, #divCategorizacionObstetrico").hide();
            $("#btnNuevo").removeAttr("disabled");

            vDiasPaciente = $(e).data('dias');
            if (vDiasPaciente <= 91)//hasta 3 meses
                $('#txtTemp').removeAttr('disabled');
            else if (vDiasPaciente >= 92 && vDiasPaciente <= 1095)//3 meses - 3 años
                $('#txtTemp').removeAttr('disabled');
            else if (vDiasPaciente >= 1096 && vDiasPaciente <= 2920)//3 años 8 años
                $('#txtTemp').attr('disabled', true);
            else if (vDiasPaciente >= 2921)
                $('#txtTemp').attr('disabled', true);

            limpiarCampos();

        }

        // Nombre de Paciente
        $('#lblPacienteCat').html($(e).data('pac'));
        $('#categorizarPaciente').modal('show');
        $("#txtObservacionesCategorizacion").val("");

    } else {

        Swal.fire({
            position: 'center',
            icon: 'error',
            title: `Para hacer la categorización, la paciente debe tener signos vitales registrados.`,
            showConfirmButton: false,
            timer: 3000
        });

    }

}

function dibujarIconoCategorizacion(json) {
    let iconoCategorizacion;
    if (json.NombreCategorizacion == "C1")
        iconoCategorizacion =
            `<h5>
                <span id="bgEstadoC" class="badge badge-pill btn-danger badge-count">
                    ${json.NombreCategorizacion}
                </span>
            </h5>`;

    else if (json.NombreCategorizacion == "C2")
        iconoCategorizacion =
            `<h5>
                <span id="bgEstadoC" class="badge badge-pill badge-count" style="color: #ffffff; background-color: orange;">
                    ${json.NombreCategorizacion}
                </span>
            </h5>`;

    else if (json.NombreCategorizacion == "C3")
        iconoCategorizacion =
            `<h5>
                <span id="bgEstadoC" class="badge badge-pill btn-warning badge-count">
                    ${json.NombreCategorizacion}
                </span>
            </h5>`;

    else if (json.NombreCategorizacion == "C4")
        iconoCategorizacion =
            `<h5>
                <span id="bgEstadoC" class="badge badge-pill btn-success badge-count">
                    ${json.NombreCategorizacion}
                </span>
            </h5>`;

    else if (json.NombreCategorizacion == "C5")
        iconoCategorizacion =
            `<h5>
                <span id="bgEstadoC" class="badge badge-pill btn-info badge-count">
                    ${json.NombreCategorizacion}
                </span>
            </h5>`;

    else if (json.NombreCategorizacion == "S/C")
        iconoCategorizacion =
            `<h5>
                <span id="bgEstadoC" class="badge badge-pill btn-default badge-count">
                    ${json.NombreCategorizacion}
                </span>
            </h5>`;
    
    return iconoCategorizacion;
}
function limpiarCategorizacion(IdTipoAtencion) {

    comboTipoClasificacion(IdTipoAtencion);
    $("#divDetalleAlergias").hide();
    $(`#txtDetalleAlergias, #txtAntecedentesMorbidos, #txtAntecedentesQx, #txtObservacionesCategorizacion`).val("");
    $(`#sltTipoClasificacion`).val("0");
    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").bootstrapSwitch('radioAllOff', true);
    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").bootstrapSwitch('state', false, true);
    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").bootstrapSwitch('radioAllOff', false);

}
function comboTipoClasificacion(IdTipoAtencion) {
    let url = `${GetWebApiUrl()}URG_Tipo_Clasificacion/Combo/${IdTipoAtencion}`;
    setCargarDataEnCombo(url, true, $("#sltTipoClasificacion"));    
}
function CargarInfoCategorizacion(id) {

    const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}?extens=ANTECEDENTESCLINICOS&extens=SIGNOSVITALES`;
    let esValido = false;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {

            const json = data;

            if (sSession.CODIGO_PERFIL == 25 && json.SignosVitales.length == 0) {
                return;
            } else {

                limpiarCategorizacion(json.AtencionAdministrativa.IdTipoAtencion);
                esValido = true;
                $("#txtDetalleAlergias").val("");

                if (json.AntecedentesClinicos.IdTipoAlternativaAntecedentes === 1) {
                    $("#txtDetalleAlergias").val(json.AntecedentesClinicos.Alergias);
                    $("#rdoAlergiaSi").bootstrapSwitch('state', true);
                } else if (json.AntecedentesClinicos.IdTipoAlternativaAntecedentes === 2) {
                    $("#rdoAlergiaNo").bootstrapSwitch('state', true);
                } else if (json.AntecedentesClinicos.IdTipoAlternativaAntecedentes === 3) {
                    $("#rdoAlergiaDesconocido").bootstrapSwitch('state', true);
                }

                if (json.AtencionAdministrativa.IdTipoClasificacion === null) {
                    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").bootstrapSwitch('radioAllOff', true);
                    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido, #rdoAlergiaDesconocido").bootstrapSwitch('state', false, true);
                    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").bootstrapSwitch('radioAllOff', false);
                } else
                    $(`#sltTipoClasificacion`).val(json.AtencionAdministrativa.IdTipoClasificacion);

                $('#txtMotivoConsultaCat').val(json.AtencionAdministrativa.MotivoConsulta);
                $(`#txtAntecedentesMorbidos`).val(json.AntecedentesClinicos.AntecedentesMorbidos);
                $(`#txtAntecedentesQx`).val(json.AntecedentesClinicos.AntecedentesQx);

                if (sSession.CODIGO_PERFIL == 25) {
                    $("#divAntecedentesGinecoObstetrico").show();
                    $("#txtAntecedentesGinecoObstetrico").val(json.AntecedentesClinicos.AntecedentesGinecoObstetrico);
                } else {
                    $("#divAntecedentesGinecoObstetrico").hide();
                }

            }

        },
        error: function (err) {
            console.log(`Error al cargar DAU desde carga de categorización: ` + JSON.stringify(err));
        }
    });

    return esValido;
}
function DarResultadoCategorizacion(jsonCategorizacion) {

    $('#tCat').html(`Categorización: ${jsonCategorizacion.TipoCategorización}`);

    vCategorizacion = jsonCategorizacion.IdTipoCategorización;
    $('#tEstimado').html(jsonCategorizacion.TiempoEstimado);
    // 'Se debe atender dentro de 30 minutos'

    // Reemplazar esto con una llamada a la api
    $('#categorizarPaciente').modal('hide');
    $('#categorizarPaciente').on('hidden.bs.modal', function (e) {
        $('#pacienteCategorizado').modal('show');
        $(e.currentTarget).unbind();
    });

}

// EDAD 
function edad(b) {
    var a = moment();
    b = moment(b);
    var years = a.diff(b, 'year');
    b.add(years, 'years');
    var months = a.diff(b, 'months');
    b.add(months, 'months');
    var days = a.diff(b, 'days');
    let response = "";
    if (years > 0) {
        response += `${years} ${(years == 1 ? ' año ' : ' años ')}`;
    }
    if (months > 0) {
        response += `${months} ${(months == 1 ? ' mes ' : ' meses ')}`;
    }
    if (days > 0) {
        response += `${days} ${(days == 1 ? ' día ' : ' días ')}`;
    }
    return response;

}
function edadDias(b) {
    var a = moment();
    var days = a.diff(b, 'days');
    return days;
}

// ACCIONES DE TABLA GENERAL
function linkImprimir(id) {

    $('#modalCargando').show();
    //$('#frameDAU').attr('src', 'ImprimirDAU.aspx?id=112');
    $('#frameDAU').attr('src', 'ImprimirDAU.aspx?id=' + id);
    $('#mdlImprimir').modal('show');

}
function linkImprimirExamenes(id) {

    const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}?extens=INDICACIONESCLINICAS`;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            if (data.IndicacionesClinicas.ExamenesLaboratorio.length > 0) {
                $('#modalCargando').show();
                $('#frameDAU').attr('src', `${ObtenerHost()}/Vista/ModuloExamenes/ImprimirExamenesLaboratorio.aspx?id=${id}`);
                $('#mdlImprimir').modal('show');
            } else {
                Swal.fire({
                    position: 'center',
                    icon: 'warning',
                    title: 'El paciente no posee exámenes registrados.',
                    showConfirmButton: false,
                    timer: 2000
                });
            }

        }, error: function (e, r) {
            console.log(`Error al cargar Json Imprimir Urgencia: ${JSON.stringify(e)}`);
        }
    });

}
function linkAnular(e) {

    var id = $(e).data('id');
    vIdAtencion = id;
    $('#lblAtencionAnular').html(id);
    $('#txtMotivoAnulacion').val("");
    $('#mdlAnularAtencion').modal('show');

}
function linkMovimientosIngresoUrgencia(sender) {

    var adataset = [];
    var id = $(sender).data("id");

    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/Movimientos`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, r) {
                adataset.push([
                    moment(r.Fecha).format('DD-MM-YYYY HH:mm:ss'),
                    r.Usuario,
                    r.Descripcion
                ]);

            });
            if (adataset.length == 0)
                $('#lnkExportarMovIngresoUrgencia').addClass('disabled');
            else
                $('#lnkExportarMovIngresoUrgencia').removeClass('disabled');

            $('#tblMovimientosIngresoUrgencia').addClass("nowrap").DataTable({
                data: adataset,
                order: [],
                columns: [
                    { title: "Fecha Movimiento" },
                    { title: "Usuario" },
                    { title: "Movimiento" }
                ],
                "columnDefs": [
                    {
                        "targets": 1,
                        "sType": "date-ukLong"
                    }
                ],
                "bDestroy": true
            });

            $('#tblMovimientosIngresoUrgencia').css("width", "100%");
            $('#mdlMovimientosIngresoUrgencia').modal('show');
        }
    });
}
function linkEditar(e) {

    const countBox = parseInt($(e).data("count-box"));
    if ((sSession.CODIGO_PERFIL != 1) || ((sSession.CODIGO_PERFIL == 1 || sSession.CODIGO_PERFIL == 25) && countBox > 0)) {
        const id = $(e).data('id');
        setSession('ID_INGRESO_URGENCIA', id);
        window.location.replace(`${ObtenerHost()}/Vista/ModuloUrgencia/NuevoIngreso.aspx`);
    } else {
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Debe asignar un box para este paciente.',
            showConfirmButton: false,
            timer: 2000
        })
    }
}
function linkAtencionClinica(sender) {

    const countBox = parseInt($(sender).data("count-box"));
    if (countBox > 0) {
        irAtencionClinica($(sender).data("id"));
    } else {
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Debe asignar un box para este paciente.',
            showConfirmButton: false,
            timer: 2000
        })
    }
}
function irAtencionClinica(id) {
    localStorage.setItem("ES_ATENCION_CLINICA", true);
    setSession('ID_INGRESO_URGENCIA', id);
    window.location.replace(`${ObtenerHost()}/Vista/ModuloUrgencia/NuevoIngreso.aspx`);
}

// ALTA
function linkEgresoIngresoUrgencia(sender) {

    $("#rdoTipoEgresoNormal, #rdoTipoEgresoIrregular, #rdoFallecido, #rdoVivo").bootstrapSwitch('disabled', false);
    $("#rdoTipoEgresoNormal, #rdoTipoEgresoIrregular").bootstrapSwitch('radioAllOff', true);
    $("#rdoTipoEgresoNormal, #rdoTipoEgresoIrregular").bootstrapSwitch('state', false);
    $("#rdoTipoEgresoNormal, #rdoTipoEgresoIrregular").bootstrapSwitch('radioAllOff', false);
    $(`#txtDNumeroDau, #txtDPaciente, #txtDMedicoTratante, #txaHipotesis, #txaExamenFisico, #txaAnamnesis, #txaIndicacionesAlta`).val("");

    ReiniciarAlta();
    $("#btnEgresarAlta, #sltDestinoAltaPaciente").unbind();
    $("#btnEgresarAlta").click(function () {
        EgresarDarAltaPaciente(sender);
    });
    ChangeDestinoAlta();
    CargarInfoDAU(sender);

    $('#mdlEgresarAtencion').modal('show');

}
function ReiniciarAlta() {

    if (vCodigoPerfil == 1 || vCodigoPerfil == 25)
        CargarDatosMedicoAlta();

    comboMotivoCierre();

    $(`#sltMotivoCierre, #sltDestinoAltaPaciente, #sltEstablecimientoAltaPaciente, #sltUbicacionDerivado,
        #sltDestinoDerivacion, #sltPronosticoMedicoLegal`).val("0");

    $("#txtMotivoCierrePaciente").val("");

    $("#tblDiagnosticos tbody").empty();
    $("#divEgresoNormal, #divEgresoIrregular, #divCondicionPaciente, #btnEgresarAlta, #divAtencionClinica").hide();
    $(`#sltEstablecimientoAltaPaciente, #sltUbicacionDerivado, #sltDestinoDerivacion`).parent().hide();

    limpiarDiagnosticos();

}
function ComboDestinoAltaPaciente() {
    if ($('#sltDestinoAltaPaciente').has('option').length === 0) {
        let url = `${GetWebApiUrl()}URG_Destino_Alta_Paciente/Combo`;
        setCargarDataEnCombo(url, true, $("#sltDestinoAltaPaciente"));
    }
}
function ComboDestinoDerivacion() {
    if ($('#sltDestinoDerivacion').has('option').length === 0) {
        let url = `${GetWebApiUrl()}URG_Tipo_Destino_Derivacion/Combo`;
        setCargarDataEnCombo(url, true, $("#sltDestinoDerivacion"));
    }
}
function comboTipoUrgencia(array) {
    if ($('#sltTipoUrg').has('option').length === 0) {
        $('#sltTipoUrg').empty();
        $.each(array, function (i, r) {
            $('#sltTipoUrg').append(`<option value='${r.Id}'>${r.Valor}</option>`);
        });

        $('#sltTipoUrg').selectpicker('refresh');
    }
}
function comboMotivoCierre() {
    if ($('#sltMotivoCierre').has('option').length === 0) {
        let url = `${GetWebApiUrl()}URG_Tipo_Motivo_Cierre/Combo`;
        setCargarDataEnCombo(url, true, $("#sltMotivoCierre"));
    }
}
function ComboPronosticoMedicoLegal() {
    if ($('#sltPronosticoMedicoLegal').has('option').length === 0) {
        let url = `${GetWebApiUrl()}URG_Pronostico_Medico_Legal/Combo`;
        setCargarDataEnCombo(url, true, $("#sltPronosticoMedicoLegal"));
    }
}
function cargarComboTipoAtencion() {
    if($("#selectTipoAtencion").has('option').length === 0) {
        let url = `${GetWebApiUrl()}/URG_Tipo_Atencion/Combo`;
        setCargarDataEnCombo(url, true, $("#selectTipoAtencion"));
    }
}
function CargarComboUbicacion() {
    if ($('#sltUbicacionDerivado').has('option').length === 0) {
        let url = GetWebApiUrl() + "GEN_Ubicacion/Hospitalizacion/1";
        setCargarDataEnCombo(url, true, $("#sltUbicacionDerivado"));
    }
}
function CargarDiagnosticosCIE10() {

    if ($(".typeahead__result").length === 0) {

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Diagnostico_CIE10/Combo`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                $("#txtDiagnosticoCIE10").typeahead(setTypeAhead(data));
            }
        });

    }


}
function CargarDatosMedicoAlta() {
    CargarDiagnosticosCIE10();
    ComboDestinoAltaPaciente();
    ComboDestinoDerivacion();
    ComboPronosticoMedicoLegal();
    CargarComboUbicacion();
}
function CargarInfoDAU(sender) {

    $(`#divEgresoNormal select, #divEgresoNormal textarea, #divEgresoNormal input`).removeAttr("disabled");
    $(`#divEgresoMatroneria`).empty();
    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${$(sender).data("id")}?extens=ATENCIONMEDICA&extens=GINECOOBSTETRICO&extens=ALTA&extens=ALTAENFERMERIA`;

    if (vCodigoPerfil == 18 || vCodigoPerfil == 22 || vCodigoPerfil == 25)
        url += `&extens=INDICACIONESCLINICAS`;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            $("#txtDMedicoTratante, #txaHipotesis, #txaExamenFisico, #txaAnamnesis").val("");
            $("#txtDNumeroDau").val(data.AtencionAdministrativa.IdAtencionUrgencia);
            $("#txtDPaciente").val(data.Paciente.Nombre + ' ' + data.Paciente.ApellidoPaterno + ' ' + data.Paciente.ApellidoMaterno);
            $("#sltProcedimiento").val((data.AtencionAdministrativa.IdTipoClasificacion == 4) ? "SI" : "NO");
            
            if (data.AtencionMedica == null) {

                $("#divInfoClinicaUrgencia").hide();
                $("#divAltaPadre .alert").show();

            } else {

                $("#divInfoClinicaUrgencia").show();
                $("#divAltaPadre .alert").hide();

                if (vCodigoPerfil == 25)
                    $("#txtDMedicoTratante").parent().children("label").html("Matron/na tratante");

                if (data.AtencionMedica.MedicoAtencion != null)
                    $("#txtDMedicoTratante").val(`${data.AtencionMedica.MedicoAtencion.Nombre} ${data.AtencionMedica.MedicoAtencion.ApellidoPaterno} ${data.AtencionMedica.MedicoAtencion.ApellidoMaterno} | ${data.AtencionMedica.MedicoAtencion.NumeroDocumento}`);

                if (data.AtencionMedica.Descripcion.length > 0) {
                    const jsonIM = data.AtencionMedica.Descripcion[data.AtencionMedica.Descripcion.length - 1];
                    $("#txaHipotesis").val(jsonIM.HipotesisDiagnostica);
                    $("#txaExamenFisico").val(jsonIM.ExamenFisico);
                    $("#txaAnamnesis").val(jsonIM.Anamnesis);
                }

            }

            if (vCodigoPerfil == 18 || vCodigoPerfil == 22) {

                const response = ValidarAtencionesClinicas(data.IndicacionesClinicas);
                $("#divAtencionClinica .alert").remove();

                if (data.IndicacionesClinicas !== null) {

                    if (!response.valido) {
                        $("#divAtencionClinica").append(GetHtmlAtencionClinicasPendientes(response, $(sender).data("id")));
                        $("#divAtencionClinica").data("pendientes", true);
                        return;
                    }

                }

                $("#divAtencionClinica").append(
                    `<div class='alert alert-success text-center mt-2'>
                        <strong><i class="fas fa-check"></i> Las indicaciones están resueltas.</strong> <br />
                        Cantidad de Indicaciones: <span class="badge badge-light">${response.cantidad}/${response.cantidad}</span>
                    </div>`);

                if (data.AltaEnfermeria == null) {
                    $("#divAtencionClinica").data("pendientes", false);
                } else {
                    $("#rdoTipoEgresoNormal").bootstrapSwitch('state', true);
                    $("#rdoTipoEgresoNormal, #rdoTipoEgresoIrregular").bootstrapSwitch('disabled', true);
                    $("#divAtencionClinica .alert").append(
                        `<br /><strong><i class="fas fa-check"></i> El Alta de enfermería ya fue realizada.</strong> <br />
                        Fecha de egreso: ${moment(data.AltaEnfermeria.Fecha).format("DD-MM-YYYY HH:mm:ss")}<br />
                        Profesional: ${data.AltaEnfermeria.Profesional}`);
                }

            } else if (vCodigoPerfil == 25) {

                if (data.AltaEnfermeria == null) {

                    const response = ValidarAtencionesClinicas(data.IndicacionesClinicas);
                    
                    if (!response.valido) {
                        const html = GetHtmlAtencionClinicasPendientes(response, $(sender).data("id"));
                        $("#divEgresoMatroneria").append(html);
                        $("#divEgresoMatroneria").data("pendientes", true);
                        $("#divEgresoMedico").hide();
                    } else {
                        $("#divEgresoMatroneria").data("pendiente-alta-medica", (data.Alta == null));
                        $("#divEgresoMatroneria").data("pendientes", false);
                    }

                }

            } else if (vCodigoPerfil == 1 && data.Alta != null) {
                CargarInfoEgresoMedico(data);
            }

        }, error: function (e, r) {
            console.log(`Error al cargar Json ingreso urgencia: ${JSON.stringify(e)}`);
        }
    });

}
function CargarInfoEgresoMedico(data) {

    $("#rdoTipoEgresoNormal").bootstrapSwitch('state', true);

    if (data.Alta.Destino.Id === 3)
        $("#rdoFallecido").bootstrapSwitch('state', true);
    else
        $("#rdoVivo").bootstrapSwitch('state', true);

    $("#rdoTipoEgresoNormal, #rdoTipoEgresoIrregular, #rdoFallecido, #rdoVivo").bootstrapSwitch('disabled', true);
    $(`#divEgresoNormal select, #divEgresoNormal textarea, #divEgresoNormal input`).attr("disabled", "disabled");
    $("#btnEgresarAlta").hide();

    //Destino Alta: sltDestinoAltaPaciente
    $("#sltDestinoAltaPaciente").val(data.Alta.Destino.Id).change();
    //Pronóstico: sltPronosticoMedicoLegal
    $("#sltPronosticoMedicoLegal").val(data.Alta.Pronostico.Id);
    //Indicaciones al alta: txaIndicacionesAlta
    $("#txaIndicacionesAlta").val(data.Alta.Indicaciones)

    //Diagnóstico: tblDiagnosticos
    if (data.Alta.DiagnosticosAlta.length > 0) {
        $("#tblDiagnosticos").show();
        data.Alta.DiagnosticosAlta.forEach((item, index) => {
            $("#tblDiagnosticos tbody").append(
                `<tr class='text-center'>
                    <td class='pt-1 pb-1' style='font-size:20px !important;'>${item.Valor}</td>
                    <td class='text-center pt-1 pb-1'><br /></td>
                </tr>`);
        });
    }

    //Establecimiento: sltEstablecimientoAltaPaciente
    if (data.Alta.Destino.OtroEstablecimiento.Id != null)
        $("#sltEstablecimientoAltaPaciente").val(data.Alta.Destino.OtroEstablecimiento.Id);

    //Ubicación: sltUbicacionDerivado
    if (data.Alta.Destino.Hospitalizacion.Id != null)
        $("#sltUbicacionDerivado").val(data.Alta.Destino.Hospitalizacion.Id);

    //Destino: sltDestinoDerivacion
    if (data.Alta.Destino.Derivacion.Id != null)
        $("#sltDestinoDerivacion").val(data.Alta.Destino.Derivacion.Id);

}
function GetHtmlAtencionClinicasPendientes(response, id) {

    const html = `<div class='alert alert-warning text-center mt-2'>
            <strong><i class="fas fa-exclamation-triangle"></i> ${(response.pedientes == 1)
            ? `Existe ${response.pedientes} indicación`
            : `Existen ${response.pedientes} indicaciones`} sin resolver.</strong><br />
            Cantidad de Indicaciones: <span class="badge badge-light">${(response.cantidad - response.pedientes)}/${response.cantidad}</span><br />
            <a class='btn btn-primary mt-2' onClick="irAtencionClinica(${id})">
                Click aquí para completar Indicaciones pendientes <span class="badge badge-light">${(response.cantidad - response.pedientes)}/${response.cantidad}</span>
            </a>
        </div>`;

    return html;
}
function ChangeDestinoAlta() {

    $("#sltDestinoAltaPaciente").change(function () {

        ///Traslado
        if ($(this).val() == '3') {

            comboEstablecimiento($('#sltEstablecimientoAltaPaciente'));
            $('#sltEstablecimientoAltaPaciente').parent().show();
            $('#sltEstablecimientoAltaPaciente').attr("data-required", true);
            $('#sltUbicacionDerivado, #sltDestinoDerivacion').parent().hide();
            $('#sltUbicacionDerivado, #sltDestinoDerivacion').val('0').attr("data-required", false);

        } else if ($(this).val() == '2') {

            $('#sltEstablecimientoAltaPaciente, #sltDestinoDerivacion').parent().hide();
            $('#sltEstablecimientoAltaPaciente, #sltDestinoDerivacion').val('0').attr("data-required", false);
            $('#sltUbicacionDerivado').parent().show();
            $('#sltUbicacionDerivado').val('0').attr("data-required", true);

        } else if ($(this).val() == '4') {

            $('#sltUbicacionDerivado, #sltEstablecimientoAltaPaciente').parent().hide();
            $('#sltUbicacionDerivado, #sltEstablecimientoAltaPaciente').val('0').attr("data-required", false);
            $('#sltDestinoDerivacion').parent().show();
            $('#sltDestinoDerivacion').val('0').attr("data-required", true);

        } else {

            $('#sltEstablecimientoAltaPaciente, #sltUbicacionDerivado, #sltDestinoDerivacion').parent().hide();
            $('#sltEstablecimientoAltaPaciente, #sltUbicacionDerivado, #sltDestinoDerivacion').val('0').attr("data-required", false);

        }

        ReiniciarRequired();
    });

}
function EgresarDarAltaPaciente(sender) {

    if (((vCodigoPerfil == 1 || vCodigoPerfil == 25) &&
        $("#rdoTipoEgresoNormal").bootstrapSwitch('state') &&
        (validarCampos("#divEgresoNormal", false) & validarDiagnosticos()))
        || ((vCodigoPerfil == 18 || vCodigoPerfil == 22) && $("#rdoTipoEgresoNormal").bootstrapSwitch('state'))
        || ($("#rdoTipoEgresoIrregular").bootstrapSwitch('state') && validarCampos("#divEgresoIrregular", false))) {
        ShowMensajeEgreso($(sender).data("id"));
    }

}
function ShowMensajeEgreso(id) {

    let nombreUsuario = obtenerNombreUsuario();

    Swal.fire({
        title: 'Está a punto de dar de Alta al paciente ' + $("#txtDPaciente").val(),
        text: 'Asegúrese de revisar que la siguiente información sea correcta: Nro. de Atención de Urgencia #' + $("#txtDNumeroDau").val() + '. '
            + 'El usuario que realiza la acción es ' + nombreUsuario + '. '
            + 'La Atención ya no será visible en su Bandeja y no se podrá modificar. Puede confirmar o ignorar la acción',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {

        if (result.value) {

            let json = {};
            let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}`;
            let mensajeFinal = "";

            if ($(`#rdoTipoEgresoNormal`).bootstrapSwitch('state')) {

                url += `/altaDAU`;
                mensajeFinal = "El paciente se ha dado de alta.";

                if (vCodigoPerfil == 1 || vCodigoPerfil == 25) {

                    json = {
                        IdDestinoAlta: parseInt($("#sltDestinoAltaPaciente").val()),
                        IdEstablecimientoDerivado: valCampo(parseInt($("#sltEstablecimientoAltaPaciente").val())),
                        IdUbicacion: valCampo(parseInt($("#sltUbicacionDerivado").val())),
                        IdDestinoDerivacion: valCampo(parseInt($("#sltDestinoDerivacion").val())),
                        IdPronosticoMedicoLegal: valCampo(parseInt($("#sltPronosticoMedicoLegal").val())),
                        IdDiagnostico: selectedDiagnostics,
                        IndicacionesAlta: valCampo($("#txaIndicacionesAlta").val())
                    }

                } else if (vCodigoPerfil == 18 || vCodigoPerfil == 22) {
                    json = { LiberarBox: "SI" }
                }

            } else {

                url += `/cierreDAU`;
                mensajeFinal = "Se ha cerrado el DAU.";
                json = {
                    IdMotivoCierre: valCampo(parseInt($("#sltMotivoCierre").val())),
                    Observaciones: valCampo($("#txtMotivoCierrePaciente").val())
                }

            }

            $.ajax({
                type: "PATCH",
                url: url,
                data: JSON.stringify(json),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, jqXHR) {
                    CargarTablaUrgencia();
                    $('#mdlEgresarAtencion').modal('hide');
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: mensajeFinal,
                        showConfirmButton: false,
                        timer: 3000
                    })
                    if (vCodigoPerfil == 18 || vCodigoPerfil == 22 || vCodigoPerfil == 25) {
                        linkImprimir(id); 
                    }
                },
                error: function (jqXHR, status) {
                    console.log("Error al guardar Egresar, derivar o cerrar: " + JSON.stringify(jqXHR));
                }
            });

        }

    });

}
function ValidarAtencionesClinicas(json) {

    let response = { valido: true, mensaje: "", cantidad: 0, pedientes: 0 }

    if (json != null) {

        // Interconsulta
        for (let item of json.Interconsultor) {
            if (item.AtencionConsultorCerrada != "SI") {
                response.mensaje += `<strong><i class="fas fa-ban"></i> Interconsulta:</strong> ${item.DescripcionTipoConsultor}<br />`;
                response.valido = false;
                response.pedientes++;
            }
            response.cantidad++;
        }

        // Procedimientos
        for (const item of json.Procedimientos) {
            if (item.AccionRealizada == null) {
                response.mensaje += `<strong><i class="fas fa-ban"></i> Procedimiento:</strong> ${item.Descripcion}<br />`;
                response.valido = false;
                response.pedientes++;
            }
            response.cantidad++;
        }

        // ExamenesLaboratorio
        for (const item of json.ExamenesLaboratorio) {
            if (item.AccionRealizada == null) {
                response.mensaje += `<strong><i class="fas fa-ban"></i> Examen laboratorio:</strong> ${item.Descripcion}<br />`;
                response.valido = false;
                response.pedientes++;
            }
            response.cantidad++;
        }

        // Medicamentos
        for (const item of json.Medicamentos) {
            if (item.AccionRealizada == null) {
                response.mensaje += `<strong><i class="fas fa-ban"></i> Medicamento:</strong> ${item.DescripcionMedicamento}<br />`;
                response.valido = false;
                response.pedientes++;
            }
            response.cantidad++;
        }
    }


    return response;
}
function linkEgresoAdministrativo(sender) {

    const id = parseInt($(sender).data("id"));
    ShowModalAlerta("CONFIRMACION", "Confirmación", "¿Está seguro que quiere egresar al paciente?",
        function () {
            $.ajax({
                type: "PATCH",
                url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/altaDAU`,
                data: JSON.stringify({}),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, jqXHR) {

                    $("#modalAlerta").modal('hide');
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: `El paciente ha sido egresado.`,
                        showConfirmButton: false,
                        timer: 3000
                    });
                    CargarTablaUrgencia();

                },
                error: function (jqXHR, status) {
                    console.log("Error al guardar Egresar Administrativo: " + JSON.stringify(jqXHR));
                }
            });
        }, function () {
            $('#modalAlerta').modal('hide');
        }, "Aceptar", "Cancelar");

}

// ASIGNAR BOX
function linkAsignarBox(e) {
    if ($(e).data("id-categorizacion") === "" || $(e).data("id-categorizacion") == "null" || $(e).data("id-categorizacion") == null) {
        ShowModalAlerta("CONFIRMACION", "Confirmación", "Este paciente no se ha categorizado, ¿Está seguro que quiere enviarla a Box?",
            function () {
                AsignarBox(e);
            }, function () {
                $('#mdlPasaraBox').modal('hide');
                $('#modalAlerta').modal('hide');
            }, "Aceptar", "Cancelar");
    } else
        AsignarBox(e);


}
function AsignarBox(e) {
    nombrePaciente = $(e).data('nombrepaciente');
    comboServicioBox(nombrePaciente);
    $("#modalAlerta").modal("hide");
    $("#aEnviarABox").data("id", $(e).data('id'));
    $("#sltTipoBox").attr("disabled", "disabled").val("0");
    $('#sltNombreBox').attr('disabled', 'disabled').val('0');
    CargarTablaTraslados($(e).data('id'));
    $('#mdlPasaraBox').modal('show');
    $("#aEnviarABox").unbind().click(function () {

        let host = ObtenerHost();
        host += "/Vista/ModuloUrgencia/BandejaUrgencia.aspx#/";
        if (validarCampos("#divBox", false)) {
            $.ajax({
                type: 'PUT',
                url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${parseInt($("#aEnviarABox").data("id"))}/box/${parseInt($("#sltNombreBox").val())}`,
                contentType: 'application/json',
                data: JSON.stringify({}),
                dataType: 'json',
                success: function (data) {
                    //en urgencia carga la tabla urgencia
                    if (window.location.href == host) {
                        CargarTablaUrgencia();
                    } else {

                        let idUbicacion = $("#inpIdUbicacion").data("id-ubicacion");
                        // en mapa de camas urgencias carga el mapa
                        cargarCamasUrgencia(false, idUbicacion);
                    }
                    $('#mdlPasaraBox').modal('hide');
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'El paciente ha sido enviado al box.',
                        showConfirmButton: false,
                        timer: 3000
                    })


                },
                error: function (jqXHR, status) {
                    console.log(`Error al enviar paciente.`);
                }
            });

        } else {
            console.log("Error validando los campos, hay campos vacíos");
        }

    });

}
function linkLiberarBox(e) {
    const id = parseInt($(e).data("id"));
    ShowModalAlerta("CONFIRMACION", "Confirmación", "¿Está seguro que quiere liberar el Box?",
        function () {
            LiberarBox(id);
        }, function () {
            $('#modalAlerta').modal('hide');
        }, "Aceptar", "Cancelar");

}
function LiberarBox(id) {
    $.ajax({
        type: 'PATCH',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/LiberarBox`,
        contentType: 'application/json',
        data: JSON.stringify({}),
        dataType: 'json',
        success: function (data) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'El Box fue liberado.',
                showConfirmButton: false,
                timer: 3000
            });
            $("#modalAlerta").modal('hide');
            CargarTablaUrgencia();
        },
        error: function (jqXHR, status) {
            console.log(`Error al enviar paciente.`);
        }
    });

}

// TRASLADOS
function CargarTablaTraslados(id) {

    var adataset = [];
    //ID de la atención de urgencia, Tipo Box, Box y fecha y hora
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/traslados`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            $("#lblPasaraBox").text(data.AtencionUrgencia[0].GEN_nombrePaciente);
            $.each(data.Traslados, function (i, r) {
                adataset.push([
                    r.NombreTipoBox,
                    r.NombreBox,
                    moment(r.Fecha).format('DD-MM-YYYY HH:mm:ss')
                ]);
            });

        }
    });

    $('#tblTrasladosUrgencia').addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: "Tipo box" },
            { title: "Box" },
            { title: "Fecha traslado" }
        ],
        "columnDefs": [
            {
                "targets": 1,
                "sType": "date-ukLong"
            }
        ],
        "bDestroy": true
    });

    $('#tblTrasladosUrgencia').css("width", "100%");

}
function GetJsonProfesional(id) {

    var json = null;
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            json = {
                "GEN_rutProfesional": data.GEN_rutProfesional,
                "GEN_digitoProfesional": data.GEN_digitoProfesional,
                "GEN_nombreProfesional": data.GEN_nombreProfesional,
                "GEN_apellidoProfesional": data.GEN_apellidoProfesional,
                "GEN_sapellidoProfesional": data.GEN_sapellidoProfesional
            };
        }, error: function (e, r) {
            console.log(`Error al cargar Json Profesional: ${JSON.stringify(e)}`);
        }
    });

    return json;

}

// INDICACIONES
function CargarIndicacionesUrg(id) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${URG_idAtenciones_Urgencia}?extens=INDICACIONESCLINICAS`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            cargarIndicacionesUrg(data);
            $("#mdlIndicaciones").modal('show');
        }
    });
}

// SIGNOS VITALES
function linkSignosVitales(sender) {

    if (sSession.CODIGO_PERFIL != 25 && $(sender).data("categorizacion") === null) {
        ShowModalAlerta("CONFIRMACION", "Confirmación", "Este paciente no se ha categorizado, ¿Está seguro que quiere registrar los signos vitales?",
            function () {
                MostrarSignosVitales(sender);
            }, function () {
                $('#modalAlerta').modal('hide');
            }, "Aceptar", "Cancelar");
    } else {

        if (sSession.CODIGO_PERFIL == 25 && $(sender).data("categorizacion") === null) {

            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${$(sender).data("id")}`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {

                    Swal.fire({
                        position: 'center',
                        icon: 'info',
                        title: 'Motivo de consulta',
                        input: 'textarea',
                        inputPlaceholder: 'Especifique el motivo de la consulta',
                        inputValue: data.AtencionAdministrativa.MotivoConsulta,
                        inputAttributes: {
                            'aria-label': 'Motivo de consulta',
                            maxlength: 200,
                        },
                        showConfirmButton: true,
                        confirmButtonText: "Guardar",
                        showCancelButton: false
                    }).then((result) => {
                        if ($.trim(result.value) !== "") {
                            data.AtencionAdministrativa.MotivoConsulta = $.trim(result.value);
                            GuardarMotivoAtencion(sender, data);
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                title: "El Motivo de consulta no puede estar vacío",
                                showConfirmButton: false,
                                timer: 1500
                            }).then(() => {
                                linkSignosVitales(sender);
                            });
                        }
                    });

                }
            });

        } else 
            MostrarSignosVitales(sender);
    }

}
function MostrarSignosVitales(sender) {

    $("#modalAlerta").modal("hide");
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}GEN_Tipo_Medida/Combo`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            $('#lblPacienteSignosVitales').html($(sender).data('pac'));

            $("#divSignosVitales").empty();
            $("#divSignosVitales").append(
                `
                    <h5 class="mb-3"><strong><i class='fa fa-heartbeat'></i> Signos vitales</strong></h5>
                    <div class='row'></div>
                `);

            $.each(data, function (i, r) {

                let type = "number";
                if (r.Formato === "str") {
                    type = "text";
                }

                $("#divSignosVitales .row").append(
                    `<div class='col-md-2 mt-2'>
                        <label>${r.Valor}</label>
                        <input id='sltSignoVital_${r.Id}' type='${type}' maxlength='${r.Maximo}' class='form-control ${type}' placeholder='${r.Valor}' />
                    </div>`);

                $(`#sltSignoVital_${r.Id}`).data("id", r.Id);

            });

            $(".number").unbind();
            ValidarNumeros();
            $("#aGuardarSignoVital").data("id", $(sender).data('id'));
            $("#mdlSignosVitales").modal("show");

        }
    });

}
function GuardarSignosVitales(id) {
    let array = [];
    const arrayInputs = $("#divSignosVitales input").toArray();
    let BloodPressureRE = new RegExp(/^\d{1,3}\/\d{1,2}$/);

    if ($.trim($("#sltSignoVital_2").val()) != "" && !(BloodPressureRE.test($("#sltSignoVital_2").val()))) {
        $("#sltSignoVital_2").addClass("is-invalid");
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: "La presión arterial debe ser ingresada en formato 0-999/0-99!",
            showConfirmButton: false,
            timer: 3000
        })
        return;
    }

    for (var i = 0; i < $(arrayInputs).length; i++) {
        if ($.trim($(arrayInputs[i]).val()) != "") {
            array.push({
                "Id": $(arrayInputs[i]).data("id"),
                "Value": $(arrayInputs[i]).val()
            });
        }
    }
    if (array.length > 0) {
        $.ajax({
            type: 'POST',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/Medicion`,
            contentType: 'application/json',
            data: JSON.stringify(array),
            success: function (data) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Los signos vitales se han guardado correctamente.",
                    showConfirmButton: false,
                    timer: 3000
                })
                $("#mdlSignosVitales").modal("hide");
            },
            error: function () {
                console.log("Error al guardar signos vitales.");
            }
        });
    } else
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: "Llene por al menos un campo.",
            showConfirmButton: false,
            timer: 3000
        });

}
function GuardarMotivoAtencion(sender, data) {

    const json =
        {
            IdAcompañante: (data.Acompañante != null) ? data.Acompañante.Id : null,
            MotivoConsulta : data.AtencionAdministrativa.MotivoConsulta,
            FechaLlegada : data.AtencionAdministrativa.FechaLlegada,
            IdPrevision : data.AtencionAdministrativa.IdPrevision,
            IdPrevision_Tramo : data.AtencionAdministrativa.IdPrevisionTramo,
            IdTipoAtencion : data.AtencionAdministrativa.IdTipoAtencion,
            LugarAccidente : data.AtencionAdministrativa.LugarAccidente,
            IdMediosTransporte : data.AtencionAdministrativa.IdMedioTransporte,
            IdTiposAccidente : data.AtencionAdministrativa.IdTipoAccidente,
            IdPaciente : data.Paciente.IdPaciente,
            IdEstablecimiento : data.Establecimiento.IdEstablecimiento,
            IdTipoRepresentante: (data.Acompañante != null) ? data.Acompañante.IdTipoRepresentante : null
        };

    $.ajax({
        type: "PUT",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${data.AtencionAdministrativa.IdAtencionUrgencia}/Admision`,
        contentType: 'application/json',
        data: JSON.stringify(json),
        dataType: 'json',
        success: function (data) {

            Swal.fire({
                position: 'center', //
                icon: 'success', //
                title: 'Motivo de consulta actualizado',
                showConfirmButton: false, //
                timer: 1500,
                allowOutsideClick: false
            }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    MostrarSignosVitales(sender);
                }
            });

        },
        error: function (jqXHR, status) {
            console.log(`Error al actualizar Motivo de consulta en Atención Urgencia ${JSON.stringify(jqXHR)}`);
        }
    });

}

// DIAGNÓSTICO CIE 10 ALTA MÉDICA
function diagnosticExists(id) {
    var exist = false;
    $("#tblDiagnosticos tbody tr").each(function (index) {
        if ($(this).attr("data-idDiagnostico") == id)
            exist = true;
    });
    return exist;
}
function setTypeAhead(data) {

    var typeAhObject = {
        input: '#txtDiagnosticoCIE10',
        minLength: 3,
        maxItem: 12,
        maxItemPerGroup: 10,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el diagnóstico especificado ({{query}})",
        group: {
            key: "GEN_descripcion_padreDiagnostico_CIE10",
            template: function (item) {
                return item.GEN_descripcion_padreDiagnostico_CIE10;
            }
        },
        display: ["GEN_descripcionDiagnostico_CIE10"],
        template:
            '<span>' +
            '<span class="GEN_descripcionDiagnostico_CIE10">{{GEN_descripcionDiagnostico_CIE10}}</span>' +
            '</span>',
        correlativeTemplate: true,
        source: {
            GEN_descripcionDiagnostico_CIE10: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                if (!diagnosticExists(item.GEN_idDiagnostico_CIE10))
                    addRow(item);
                var time = setTimeout(function () {
                    $("#txtDiagnosticoCIE10").val("");
                    clearTimeout(time);
                }, 10);
            }
        }
    };

    $("#txtDiagnosticoCIE10").on('input', function () {
        if ($('.typeahead__list.empty').length > 0)
            $(".typeahead__result .typeahead__list").attr("style", "display: none !important;");
        else
            $(".typeahead__result .typeahead__list").removeAttr("style");
    });

    return typeAhObject;
}
function addRow(json) {
    var idTr = "trDiagnosticos_" + $("#tblDiagnosticos tbody").length;
    var colspan = 1;

    var tr =
        `
        <tr id='${idTr}' data-idDiagnostico = '${json.GEN_idDiagnostico_CIE10}' class='text-center'>
            <td class='pt-1 pb-1' style='font-size:20px !important;' colspan= ${colspan}>${json.GEN_descripcionDiagnostico_CIE10}</td>
            <td class='text-center pt-1 pb-1'>
                <a class='btn btn-circle-pretty-small btn-danger' data-idDiagnostico = '${json.GEN_idDiagnostico_CIE10}' style='color: #ffffff;'>
                    <i class='fa fa-times'></i>
                </a>
            </td>
        </tr>`;

    $("#tblDiagnosticos tbody").append(tr);
    $("#tblDiagnosticos").prop("style", "display: default;");
    $("#txtDiagnosticoCIE10").removeClass("invalid-input");

    selectedDiagnostics.push(json.GEN_idDiagnostico_CIE10);

    $("#" + idTr + " td a").click(function () {

        $(this).parent().parent().remove();
        for (var i = 0; i < selectedDiagnostics.length; i++) {
            if (selectedDiagnostics[i] == $(this).attr("data-idDiagnostico")) {
                selectedDiagnostics.splice(i, 1);
                break;
            }
        }

        if ($.trim($("#tblDiagnosticos tbody").html()) === "")
            $("#tblDiagnosticos").hide();

        $(this).unbind();

    });
}
function limpiarDiagnosticos() {
    $('#tblDiagnosticos').hide();
    $('#tblDiagnosticos tbody').empty();
    $('#txaIndicacionesAlta').val("");
    selectedDiagnostics = [];
}
function validarDiagnosticos() {
    if (selectedDiagnostics.length === 0) {
        $("#txtDiagnosticoCIE10").addClass("invalid-input");
        return false;
    } else {
        $("#txtDiagnosticoCIE10").removeClass("invalid-input");
        return true;
    }
}
function liquidarCuenta(id) {
    let object = getDauRecaudacion(id);
    let dau = new Proxy(object, handler);
    cargarDatosModalRecaudacion(dau);
    $("#mdlRecaudacion").modal("show");
    $("#aRecaudacionProceder").click(function () {
        //Acá una confirmación con SweetAlert
        modalConfirmarLiquidacion(id);
    });
}
function getDauRecaudacion(id) {
    let response = false;
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}`,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            response = data;
        },
        error: function (jqXHR, status) {
            console.log("Error al guardar Egresar, derivar o cerrar: " + JSON.stringify(jqXHR));
        }
    });
    return response;
}
function cargarDatosModalRecaudacion(datos) {
    $("#lblAtencionLiquidar").html(datos.AtencionAdministrativa.IdAtencionUrgencia);
    $("#lblTipoAtencionLiq").html(datos.AtencionAdministrativa.NombreTipoAtencion);
    $("#lblMotivoConsultaLiq").html(datos.AtencionAdministrativa.MotivoConsulta);
    $("#lblPacienteLiq").html(datos.Paciente.Nombre + " " + datos.Paciente.ApellidoPaterno + " " + datos.Paciente.ApellidoMaterno);
    $("#lblPrevisionLiq").html(datos.AtencionAdministrativa.NombrePrevision + " " + datos.AtencionAdministrativa.NombrePrevisionTramo);
}
function modalConfirmarLiquidacion(id) {
    $("#mdlRecaudacion").modal("hide");
    Swal.fire({
        position: 'center',
        icon: 'warning',
        title: "Está a punto de Proceder con la Liquidación de la Atención Nro. #" + id,
        text: 'Se recomienda revisar la información antes de Proceder. Puede que la acción no sea reversible. Desea continuar con la acción?',
        showConfirmButton: true,
        confirmButtonText: "Si",
        showCancelButton: true,
        cancelButtonText: "No",
    }).then((result) => {
        if (result.value) {
            hacerLiquidacionAtencion(id);
        } else if (result.dismiss) {
            $("#mdlRecaudacion").modal("show");
            console.log("Anular Cancelado!");
        }
    });
}
function hacerLiquidacionAtencion(idAtencion) {
    let json = {
        IdAtencion: idAtencion,
        Observacion: $("#txtObservacionesLiq")
    };
    //Acá se podría agregar modal Cargando
    $.ajax({
        type: 'PATCH',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencion}/Liquidar`,
        contentType: 'application/json',
        async: false,
        dataType: 'json',
        data: JSON.stringify(json),
        success: function (data) {
            CargarTablaUrgencia();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
    //Acá se puede quitar modal "Cargando"
    $("#mdlRecaudacion").modal("hide");
}
function linkHistorialHosp(event) {
    let id = $(event).data("idpac");
    let nombre = $(event).data("nompac");
    ShowModalHospitalizaciones(id, nombre);
}
function setJsonExportarURG(json) {
    json.forEach((row) => {
        let Object = new Proxy(row, handler);
        datosExportarURG.push([
            Object.Id,
            Object.Paciente.NumeroDocumento + " " + Object.Paciente.Digito,
            Object.Paciente.Nombre + " " + Object.Paciente.ApellidoPaterno + " " + Object.Paciente.ApellidoMaterno,
            Object.Paciente.Edad,
            formatDate(Object.Paciente.FechaNacimiento),
            formatDate(Object.FechaPrimeraAtencion),
            Object.Prevision,
            Object.PrevisionTramo,
            Object.MotivoConsulta,
            Object.Categorizacion,
            Object.Localizacion
        ]);
        //Indices de 0 a 8 fijos...
    });
}
