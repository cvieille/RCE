﻿function comboMedicoRemitente() {

    $('#selNombreMedicoRemitente').empty().append(`<option value='0'>Seleccione</option>`);

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/Medico`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $.each(data, function (i, r) {
                $('#selNombreMedicoRemitente').append(`<option value='${r.GEN_idProfesional}'>${r.GEN_nombrePersonas}</option>`);
            });
        }
    });
}
