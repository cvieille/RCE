﻿
$(document).ready(function () {
    let fechaHoy = moment(GetFechaActual()).format("YYYY-MM-DD");
    $("#txtFechaInicio").attr("min", AgregarDias(fechaHoy, -365));
    $("#txtFechaTermino").attr("max", fechaHoy);

    $('#txtFechaInicio').val(fechaHoy);
    $('#txtFechaTermino').val(fechaHoy);
    ShowModalCargando(false);

    $("#btnUrgLimpiar").click(function () {
        let fechaHoy = moment(GetFechaActual()).format("YYYY-MM-DD");
        $('#txtFechaInicio').val(fechaHoy);
        $('#txtFechaTermino').val(fechaHoy);
    });
    cargarComboTipoAtencion();
});

