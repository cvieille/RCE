﻿
let IdAcompañante = null;

$(document).ready(function () {

    const IniciarComponentes = async () => {
        ShowModalCargando(true);
        InicializarDatosAcompañante();
    }

    IniciarComponentes().then(() => ShowModalCargando(false));

});

// Inicio/Reinicio de Acompañante
function InicializarDatosAcompañante() {

    IdAcompañante = null;
    
    $("#chkAcompañante").on('switchChange.bootstrapSwitch', function (event, state) {
        ShowAcompañante($(this).bootstrapSwitch('state'));
    });
    $('#txtFiltroRut').blur(function (e) {
        $('#dvAcompañante').val('');
        if ($(this).val().length > 4)
            $('#dvAcompañante').val(ObtenerVerificador($(this).val()));
    });
    $("#sltIdentificacionAcompañante").change(function () {
        SltIdentificacionAcompañante_Change();
    });
    $("#txtnumeroDocAcompañante").on('input', function () {
        if ($("#sltIdentificacionAcompañante").val() === '1' || $("#sltIdentificacionAcompañante").val() === '4') {
            $("#txtDigAcompañante").val('');
            ReiniciarAcompanante();
            DeshabilitarPersona("divAcompañante", true);
        }
    });
    $("#txtnumeroDocAcompañante").on('blur', function () {

        if ($("#sltIdentificacionAcompañante").val() === '2' || $("#sltIdentificacionAcompañante").val() === '3') {
            if ($.trim($("#txtnumeroDocAcompañante").val()) !== '') {
                CargarPersona(null, $(`#sltIdentificacionAcompañante`).val(), $(`#txtnumeroDocAcompañante`).val());
                if (IdAcompañante == null)
                    ReiniciarPersona();
                DeshabilitarPersona("divAcompañante", false);
            } else {
                ReiniciarPersona();
                DeshabilitarPersona("divAcompañante", true);
            }
        }

    });
    $("#txtDigAcompañante").on('input', function () {

        ReiniciarPersona();
        DeshabilitarPersona("divAcompañante", true);

        if ($.trim($("#txtDigAcompañante").val()) != "") {

            // 1 = RUT
            // 4 = RUT MATERNO
            if ($("#sltIdentificacionAcompañante").val() === '1' || $("#sltIdentificacionAcompañante").val() === '4') {

                // VALIDA SI EL DIGITO VERIFICADOR ES CORRECTO
                let esRutCorrecto =
                    EsValidoDigitoVerificador($("#txtnumeroDocAcompañante").val(), $("#txtDigAcompañante").val());

                // SI EL RUT ES CORRECTO SE VERIFICA SI ESTE EXISTE EN BD O NO PARA 
                //TRAER LA INFO DEL PACIENTE
                if (esRutCorrecto) {
                    CargarPersona(null, $(`#sltIdentificacionAcompañante`).val(), $(`#txtnumeroDocAcompañante`).val());
                    DeshabilitarPersona("divAcompañante", false);
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'RUT Incorrecto',
                        showConfirmButton: false,
                        timer: 1500,
                        allowOutsideClick: false
                    });
                }
            }

        }
    });

}

// ACOMPAÑANTE
function CargarPersona(idPersona, idIdentificacion, numeroDocumentoPersonas) {

    var json = GetJsonCargarPersona(idPersona, idIdentificacion, numeroDocumentoPersonas);
    $("#sltTipoRepresentante").val("0");

    if (json !== null) {

        $("#txtNombreAcompañante").val(json.Nombre);
        $("#txtApePatAcompañante").val(json.ApellidoPaterno);
        $("#txtApeMatAcompañante").val(json.ApellidoMaterno);
        $("#txtTelAcompañante").val(json.Telefono);
        $("#sltSexoAcompañante").val(json.IdSexo);
        IdAcompañante = json.IdPersona;

        if (idPersona !== null) {

            DeshabilitarPersona("divAcompañante", false);
            $("#sltIdentificacionAcompañante").val(json.IdIdentificacion);
            $("#txtnumeroDocAcompañante").val(json.NumeroDocumento);
            if (json.IdIdentificacion === 1 || json.IdIdentificacion === 4) {

                if (json.Digito == null) {
                    let dig = ObtenerVerificador($("#txtnumeroDocAcompañante").val());
                    $(`#txtDigAcompañante`).val(dig);
                } else
                    $("#txtDigAcompañante").val(json.Digito);

            }

        }

    } else {
        IdAcompañante = 0;
    }

}
function GetJsonCargarPersona(idPersona, idIdentificacion, numeroDocumento) {

    var url = (idPersona === null) ?
        `${GetWebApiUrl()}GEN_Personas/Buscar?idIdentificacion=${idIdentificacion}&numeroDocumento=${numeroDocumento}` :
        `${GetWebApiUrl()}GEN_Personas/${idPersona}`;

    var d = null;

    // SI ES 0 ES PORQUE SE CREARÁ UNA PERSONA NUEVA
    if (idPersona !== 0) {

        $.ajax({
            type: "GET",
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                if ($.isArray(data)) {
                    if (data.length > 0)
                        d = data[0];
                } else {
                    d = data;
                }
            },
            error: function (httpObj, textStatus) {

                if (httpObj.status == 401) {
                    ReiniciarAcompanante();
                } else
                    console.log(`Error al cargar JSON de Persona: ${JSON.stringify(httpObj)}`);
            }
        });

    } else {

        d = {
            GEN_numero_documentoPersonas: null,
            GEN_digitoPersonas: null,
            GEN_nombrePersonas: null,
            GEN_apellido_paternoPersonas: null,
            GEN_apellido_maternoPersonas: null,
            GEN_telefonoPersonas: null,
            GEN_emailPersonas: null,
            GEN_idIdentificacion: null,
            GEN_idSexo: null,
            GEN_estadoPersonas: "Activo",
            GEN_dir_callePersonas: null,
            GEN_idCategorias_Ocupacion: null,
            GEN_idTipo_Escolaridad: null,
            GEN_fecha_nacimientoPersonas: null
        }

    }

    if (d !== null)
        d.GEN_fecha_actualizacionPersonas = GetFechaActual();

    return d;

}
function DeshabilitarPersona(divPadre, esDisabled) {
    (!esDisabled) ? $(`#${divPadre} .datos-persona`).removeAttr("disabled") : $(`#${divPadre} .datos-persona`).attr("disabled", "disabled");
}
function ReiniciarPersona() {

    $(`#divAcompañante input.datos-persona`).val("");
    $(`#divAcompañante select.datos-persona`).val("0");
    ReiniciarRequired();

}
function ReiniciarAcompanante() {

    const parent = $("#txtnumeroDocAcompañante").parent().parent().parent().parent();
    DeshabilitarPersona("divAcompañante", true);
    ReiniciarPersona();
    IdAcompañante = null;
    $("#txtDigAcompañante").val("");
    $("#sltSexoAcompañante, #sltTipoRepresentante").val("0");

}
function GuardarPersona() {

    
    if (IdAcompañante != null) {

        var url = (IdAcompañante === 0) ? `${GetWebApiUrl()}GEN_Personas` : `${GetWebApiUrl()}GEN_Personas/${IdAcompañante}`;
        var method = (IdAcompañante === 0) ? "POST" : "PUT";
        var json = GetJsonCargarPersona(IdAcompañante, null, null);

        var persona = {
            GEN_telefonoPersonas: json.GEN_telefonoPersonas,
            GEN_emailPersonas: json.GEN_emailPersonas,
            GEN_idSexo: 4,
            GEN_estadoPersonas: "Activo",
            GEN_dir_callePersonas: json.GEN_dir_callePersonas,
            GEN_idCategorias_Ocupacion: json.GEN_idCategorias_Ocupacion,
            GEN_idTipo_Escolaridad: json.GEN_idTipo_Escolaridad,
            GEN_fecha_nacimientoPersonas: json.GEN_fecha_nacimientoPersonas,
            GEN_fecha_actualizacionPersonas: json.GEN_fecha_actualizacionPersonas
        };

        persona.GEN_idIdentificacion = valCampo($("#sltIdentificacionAcompañante").val());
        persona.GEN_numero_documentoPersonas = valCampo($("#txtnumeroDocAcompañante").val());
        persona.GEN_digitoPersonas = valCampo($("#txtDigAcompañante").val());
        persona.GEN_nombrePersonas = valCampo($("#txtNombreAcompañante").val());
        persona.GEN_apellido_paternoPersonas = valCampo($("#txtApePatAcompañante").val());
        persona.GEN_apellido_maternoPersonas = valCampo($("#txtApeMatAcompañante").val());
        persona.GEN_telefonoPersonas = valCampo($("#txtTelAcompañante").val());
        persona.GEN_idSexo = valCampo($("#sltSexoAcompañante").val());

        if (IdAcompañante !== 0)
            persona.GEN_idPersonas = IdAcompañante;

        $.ajax({
            type: method,
            url: url,
            data: JSON.stringify(persona),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, status, jqXHR) {

                if (data != undefined)
                    IdAcompañante = data.GEN_idPersonas;

            },
            error: function (jqXHR, status) {
                console.log(`Error al guardar Persona: ${JSON.stringify(jqXHR)}`);
            }
        });

    }

}
function SltIdentificacionAcompañante_Change() {

    $("#txtnumeroDocAcompañante, #txtDigAcompañante").val('');
    ReiniciarPersona();
    DeshabilitarPersona("divAcompañante", true);
    $(`label[for='sltIdentificacionAcompañante']`).text($("#sltIdentificacionAcompañante").children("option:selected").text());

    switch ($("#sltIdentificacionAcompañante").val()) {
        case '2':
        case '3':
            $("#txtnumeroDocAcompañante").parent().children(".digito").hide();
            $("#txtnumeroDocAcompañante").attr("maxlength", 15);
            break;
        default:
            $("#txtnumeroDocAcompañante").parent().children(".digito").show();
            $("#txtnumeroDocAcompañante").attr("maxlength", 8);
            break;
    }

    ReiniciarRequired();
}

// COMBOS
function CargarCombosPersona() {
    ComboIdentificacionAcompañante();
    ComboSexoAcompañante();
    ComboRepresentanteAcompañante();
}
function ComboIdentificacionAcompañante() {
    $("#sltIdentificacionAcompañante").empty();
    $("#sltIdentificacionAcompañante").append($("#sltIdentificacion").html());
    $("#sltIdentificacionAcompañante").val("1");
    $(`label[for='sltIdentificacionAcompañante']`).text($("#sltIdentificacionAcompañante").children("option:selected").text());
    $("#sltIdentificacionAcompañante option[value='5']").remove();
    $("#sltIdentificacionAcompañante option[value='4']").remove();
}
function ComboSexoAcompañante() {
    let url = `${GetWebApiUrl()}/GEN_Sexo/Combo`;
    setCargarDataEnCombo(url, false, $('#sltSexoAcompañante'));
}
function ComboRepresentanteAcompañante() {
    let url = `${GetWebApiUrl()}/GEN_Tipo_Representante/Combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoRepresentante'));
}

// ACOMPAÑANTE
function GetIdAcompañante() {
    return IdAcompañante;
}
function SetIdAcompañante(IdPersona) {
    IdAcompañante = IdPersona;
}
function ShowAcompañante(esVisible) {

    IdAcompañante = null;
    if (esVisible) {
        CargarCombosPersona();
        $('#divAcompañante').show();
        $('#txtnumeroDocAcompañante, #txtDigAcompañante').val("");
        $(`#txtnumeroDocAcompañante, #txtNombreAcompañante, #txtApePatAcompañante,
            #sltSexoAcompañante, #sltTipoRepresentante`).attr("data-required", "true");
        ReiniciarPersona();
        DeshabilitarPersona("divAcompañante", true);
    } else {
        $('#divAcompañante').hide();
        $(`#txtnumeroDocAcompañante, #txtnombreAcompañante, #txtApePatAcompañante,
            #sltSexoAcompañante, #sltTipoRepresentante`).attr("data-required", "false");
        ReiniciarPersona();
    }

}
function BloquerAcompañante() {
    $('#txtnumeroDocAcompañante, #txtDigAcompañante').attr("disabled", "disabled");
    DeshabilitarPersona("divAcompañante", true);
}