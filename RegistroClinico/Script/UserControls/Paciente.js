﻿var sSession = null;
var paciente = {};

$(document).ready(function () {

    sSession = getSession();
    DeshabilitarPaciente(true);
    //Funcion CargarComboIdentificadores("#sltIdentificacion") duplicada en comboPersonas
    //Nuva forma de hacerlo: comboIdentificacion("#sltIdentificacion")
    CargarComboIdentificadores();
    CargarComboSexosPacientes();
    CargarComboGenerosPacientes();
    $("#divPacienteAcompañante, #divAcompañante").hide();
    $("#lnbEvento").hide();
    $("#txtNombreSocial").parent().hide();
    $("#chkAcompañante").bootstrapSwitch();

    // VERIFICA SI EL PERFIL ES MÉDICO 
    if (sSession.CODIGO_PERFIL == 1)
        $("#txtFecNacPac, #txtDireccion, #txtTelefono").attr("data-required", false);

    $("#txtnumerotPac").on('blur', function () {
        TxtRutPac_Blur();
    });
    $("#sltIdentificacion").on('change', function () {
        SltIdentificacion_Change();
    });
    $("#sltgeneroPac").on('change', function () {
        SltgeneroPac_Change();
    });
    $("#txtnumerotPac").on('input', function () {
        // 1 = RUT
        // 4 = RUT MATERNO
        if ($("#sltIdentificacion").val() === '1' || $("#sltIdentificacion").val() === '4') {
            LimpiarPaciente();
            $("#txtDigitoPac").val("");
            DeshabilitarPaciente(true);
        }
    });
    $("#txtDigitoPac").on('input', function () {
        TxtDigitoPac_Input();
    });
    $('#btnNuevoPacienteRN').click(function (e) {
        DeshabilitarPaciente(false);
        $('#mdlRutMaterno').modal('hide');
        LimpiarPaciente();
    });
    

});

// Carga de combos
function CargarCombosUbicacionPaciente() {

    // PAÍS
    if ($("#sltPais").length > 0) {
        ComboPais();
        $("#sltPais").on('change', function () {
            CargarComboHijo("#sltPais", "#sltRegion", ComboRegion);
            CargarComboHijo("#sltRegion", "#sltProvincia", ComboProvincia);
            CargarComboHijo("#sltProvincia", "#sltCiudad", ComboCiudad);
        });
    }

    // REGIÓN
    if ($("#sltRegion").length > 0) {
        $("#sltRegion").attr('disabled', 'disabled');
        $("#sltRegion").append("<option value='0'>Seleccione</option>");
        $("#sltRegion").change(function () {
            CargarComboHijo("#sltRegion", "#sltProvincia", ComboProvincia);
            CargarComboHijo("#sltProvincia", "#sltCiudad", ComboCiudad);
        });
    }

    // PROVINCIA
    if ($("#sltProvincia").length > 0) {
        $("#sltProvincia").attr('disabled', 'disabled');
        $("#sltProvincia").append("<option value='0'>Seleccione</option>");
        $("#sltProvincia").change(function () {
            CargarComboHijo("#sltProvincia", "#sltCiudad", ComboCiudad);
        });
    }

    if ($("#sltCiudad").length > 0) {
        $("#sltCiudad").attr('disabled', 'disabled');
        $("#sltCiudad").append("<option value='0'>Seleccione</option>");
    }
}
function CargarComboIdentificadores() {

    $("#sltIdentificacion").empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Identificacion/Combo`,
        async: false,
        success: function (data, status, jqXHR) {
            $.each(data, function (key, val) {
                $("#sltIdentificacion").append(`<option value='${val.GEN_idIdentificacion}'>${val.GEN_nombreIdentificacion}</option>`);
            });
            $("#lblIdentificacion").text($("#sltIdentificacion").children("option:selected").text());
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Seleccion de Identificación: ${JSON.stringify(jqXHR)}`);
        }
    });

}
function CargarComboSexosPacientes() {
    let url = `${GetWebApiUrl()}GEN_Sexo/Combo`;
    setCargarDataEnCombo(url, false, "#sltsexoPac");
}
function CargarComboGenerosPacientes() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Genero/Combo`;
    setCargarDataEnCombo(url, false, "#sltgeneroPac");
}
function ComboPais() {

    $("#sltPais").empty().append("<option value='0'>Seleccione</option>");
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Pais/Combo`,
        async: false,
        success: function (data, status, jqXHR) {
            $.each(data, function (key, val) {
                $("#sltPais").append(`<option value='${val.GEN_idPais}'>${val.GEN_nombrePais}</option>`);
            });
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar País: ${JSON.stringify(jqXHR)}`);
        }
    });
}
function ComboRegion(slt, idPais) {

    if (EsValidaSesionToken(slt)) {
        $(slt).removeAttr("disabled").empty().append("<option value='0'>Seleccione</option>");
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Pais/${idPais}/Region`,
            async: false,
            success: function (data, status, jqXHR) {
                //Pais extranjero sin regiones debe ocultar la region,provincia y la ciudad
                if (data.length < 1) {
                    $(slt).parent().addClass("d-none");
                    $("#sltProvincia").parent().addClass("d-none");
                    $("#sltCiudad").parent().addClass("d-none");
                } else {
                    $(slt).parent().removeClass("d-none");
                    $("#sltProvincia").parent().removeClass("d-none");
                    $("#sltCiudad").parent().removeClass("d-none");
                }
                $.each(data, function () {
                    $(slt).append(`<option value='${this.GEN_idRegion}'>${this.GEN_nombreRegion}</option>`);
                });
            },
            error: function (jqXHR, status) {
                console.log(`Error al llenar region: ${JSON.stringify(jqXHR)}`);
            }
        });
    }

}
function ComboProvincia(slt, idRegion) {

    if (idRegion != '' && idRegion != null && idRegion != undefined && EsValidaSesionToken(slt)) {

        $(slt).removeAttr("disabled").empty().append("<option value='0'>Seleccione</option>");
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Provincia/GEN_idRegion/${idRegion}`,
            async: false,
            success: function (data, status, jqXHR) {
                $.each(data, function () {
                    $(slt).append(`<option value='${this.GEN_idProvincia}'>${this.GEN_nombreProvincia}</option>`);
                });
            },
            error: function (jqXHR, status) {
                console.log(`Error al llenar provincia ${JSON.stringify(jqXHR)}`);
            }
        });
    }
}
function ComboCiudad(slt, idProvincia) {

    if (idProvincia != null && idProvincia != undefined && idProvincia != '' && EsValidaSesionToken(slt)) {

        $(slt).removeAttr("disabled").empty().append("<option value='0'>Seleccione</option>");
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Ciudad/GEN_Provincia/${idProvincia}`,
            async: false,
            success: function (data, status, jqXHR) {
                $.each(data, function () {
                    $(slt).append(`<option value='${this.GEN_idCiudad}'>${this.GEN_nombreCiudad}</option>`);
                });
            },
            error: function (jqXHR, status) {
                console.log(`Error al llenar ciudad ${JSON.stringify(jqXHR)}`);
            }
        });
    }
}

// Oyentes 
function TxtRutPac_Blur() {

    if ($("#sltIdentificacion").val() === '2' || $("#sltIdentificacion").val() === '3' && EsValidaSesionToken($("#txtnumerotPac").val())) {
        if ($.trim($("#txtnumerotPac").val()) !== '') {
            CargarPaciente(null);
            if (paciente.GEN_idPaciente == undefined)
                LimpiarPaciente();
            $(".md-form .form").addClass("active");
            DeshabilitarPaciente(false);
        } else {
            LimpiarPaciente();
            DeshabilitarPaciente(true);
        }
    }
}
function TxtRutPac_Input() {
    LimpiarPaciente();
    $("#txtDigitoPac").val('');
    $("#txtnumerotPac").next().addClass("active");
    DeshabilitarPaciente(true);
}
function TxtDigitoPac_Input() {

    LimpiarPaciente();
    DeshabilitarPaciente(true);

    if ($.trim($("#txtDigitoPac").val()) != "" && EsValidaSesionToken($("#txtDigitoPac"))) {

        // 1 = RUT
        // 4 = RUT MATERNO
        if ($("#sltIdentificacion").val() === '1' || $("#sltIdentificacion").val() === '4') {

            // VALIDA SI EL DIGITO VERIFICADOR ES CORRECTO
            let esRutCorrecto =
                EsValidoDigitoVerificador($("#txtnumerotPac").val(), $("#txtDigitoPac").val());

            // SI EL RUT ES CORRECTO SE VERIFICA SI ESTE EXISTE EN BD O NO PARA 
            //TRAER LA INFO DEL PACIENTE
            if (esRutCorrecto) {
                buscarPacientePorDocumento($("#sltIdentificacion").val(), $("#txtnumerotPac").val());
                DeshabilitarPaciente(false);
            } else {
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'RUT Incorrecto',
                    showConfirmButton: false,
                    timer: 1500,
                    allowOutsideClick: false
                });
            }        
        }

    }

}
function SltIdentificacion_Change() {

    $("#txtnumerotPac, #txtDigitoPac").val('');
    $("#txtnumerotPac").removeAttr("disabled");
    LimpiarPaciente();
    DeshabilitarPaciente(true);

    switch ($("#sltIdentificacion").val()) {
        case '2':
        case '3':
        case '5':
            $(".digito").hide();
            $("#txtnumerotPac").attr("maxlength", 15);
            break;
        default:
            $(".digito").show();
            $("#txtnumerotPac").attr("maxlength", 8);
            break;
    }

    if ($("#sltIdentificacion").val() === '5') {

        paciente = {};

        DeshabilitarPaciente(false);
        $("#txtnumerotPac").attr("disabled", "disabled");
        $("#divDatosPaciente input[data-required='true'], " +
            "#divDatosPaciente select[data-required='true']").attr("data-required", "false");

    } else if ($("#sltIdentificacion").val() === '4') {
        $("#txtApePat").attr("data-required", false);
    }
    else {

        $("#divDatosPaciente input[data-required='false'], " +
            "#divDatosPaciente select[data-required='false']").attr("data-required", "true");

        if (sSession.CODIGO_PERFIL == 1)
            $("#txtFecNacPac, #txtDireccion, #txtTelefono").attr("data-required", "false");
    }

    ReiniciarRequired();
}
function SltgeneroPac_Change() {

    //1 = Hombre
    //2 = Mujer
    //3 = Intersex
    //4 = Desconocido

    if (($("#sltgeneroPac").val() != '0') &&
        (($("#sltsexoPac").val() == '1' && $("#sltgeneroPac").val() != '2') ||
        ($("#sltsexoPac").val() == '2' && $("#sltgeneroPac").val() != '1'))) {

        $("#txtNombreSocial").parent().show();
        $("#txtNombreSocial").attr("data-required", true);

    } else if ($("#sltsexoPac").val() == '3' && $("#sltgeneroPac").val() != '0') {

        $("#txtNombreSocial").parent().show();
        $("#txtNombreSocial").attr("data-required", false);

    } else {

        $("#txtNombreSocial").parent().hide();
        $("#txtNombreSocial").attr("data-required", false);

    }

    ReiniciarRequired();

}
function TxtFecNacPac_CalculaEdad() {

    let fechaActual = moment(moment()).format("YYYY-MM-DD");
    let fechaNacimiento = moment(moment($("#txtFecNacPac").val()).toDate()).format("YYYY-MM-DD");

    if (moment(fechaActual).isSameOrAfter(fechaNacimiento) && EsValidaSesionToken($("#txtFecNacPac"))) {
        $("#txtEdadPac").val(CalcularEdad(moment($("#txtFecNacPac").val()).format("YYYY-MM-DD")).edad);
        let years = CalcularEdad(moment($("#txtFecNacPac").val()).format("YYYY-MM-DD")).años;
        if (years > 120) {
            toastr.error("Fecha de Nacimiento de Paciente no es valida");
            $("#txtEdadPac").val('');
            $("#txtFecNacPac").val('');
        }
    } else {
        $("#txtEdadPac").val('');
        $("#txtFecNacPac").val('');
    }
}

// Acciones en paciente
function BloquearPaciente() {
    $("#divDatosPaciente select, #divDatosPaciente input").attr("disabled", "disabled");
    $("#chkAcompañante").bootstrapSwitch('disabled', true);
    $("#chkAcompañante").removeAttr("disabled");
    ValidarRequired(false);
}
function ValidarRequired(bEstado) {
    $("#divDatosPaciente input[data-required], #divDatosPaciente select[data-required]").attr("data-required", bEstado);
}
function LimpiarPaciente() {

    $("#txtnombrePac, #txtApePat, #txtApeMat, #txtNombreSocial").val('');
    $("#txtFecNacPac, #txtEdadPac, #txtDireccion, #txtNumDireccionPaciente").val('');
    $("#txtTelefono, #txtOtroTelefono, #txtCorreoElectronico").val('');
    $("#sltsexoPac, #sltgeneroPac").val('0');
    $("#chkAcompañante").bootstrapSwitch('disabled', false);
    $("#chkAcompañante").bootstrapSwitch('state', false);

    $("#lblIdentificacion").text($("#sltIdentificacion").children("option:selected").text());
    $(".md-form .form").removeClass("active");
    $("#lnbEvento").hide();
    paciente = {};

    if ($("#tblHospitalizaciones").length > 0) {
        if ($("#tblHospitalizaciones tbody").length > 0) {
            $("#tblHospitalizaciones").hide();
            $("#divHospitalizaciones").show();
        }
    }

    if ($("#tblVacunasPaciente").length > 0) {
        $("#aAgregarPaciente, #sltVacunasPaciente").attr("disabled", "disabled");
        $("#aAgregarPaciente").addClass("disabled");
        if ($("#tblVacunasPaciente tbody").length > 0) {
            $("#tblVacunasPaciente").hide();
            $("#divVacunasPaciente").show();
        }
    }

    if ($("#sltPais").length > 0) {
        $("#sltPais").val('0');
    }
    if ($("#sltRegion").length > 0) {
        $("#sltRegion").empty().append("<option value='0'>-Seleccione-</option>").val(0);
    }
    if ($("#sltProvincia").length > 0) {
        $("#sltProvincia").empty().append("<option value='0'>-Seleccione-</option>").val(0);
    }
    if ($("#sltCiudad").length > 0) {
        $("#sltCiudad").empty().append("<option value='0'>-Seleccione-</option>").val(0);
    }
    if ($("#sltPrevision").length > 0) {
        $("#sltPrevision").val('0');
    }
    if ($("#sltPrevisionTramo").length > 0) {
        $("#sltPrevisionTramo").empty().append("<option value='0'>-Seleccione-</option>").val(0);
    }

}
function GetPaciente() {
    return paciente;
}
function DeshabilitarPaciente(esDisabled) {

    if (!esDisabled) {

        $("#txtnombrePac, #txtApePat, #txtApeMat, #txtNombreSocial").removeAttr("disabled");
        $("#txtFecNacPac, #txtDireccion, #txtNumDireccionPaciente").removeAttr("disabled");
        $("#txtTelefono, #txtOtroTelefono, #txtCorreoElectronico").removeAttr("disabled");
        $("#sltsexoPac, #sltgeneroPac").removeAttr('disabled');
        $("#chkAcompañante").bootstrapSwitch('disabled', false);

        if ($("#sltNacionalidad").length > 0)
            $("#sltNacionalidad").removeAttr("disabled");

        if ($("#sltPais").length > 0)
            $("#sltPais").removeAttr("disabled");

        if ($("#sltRegion").length > 0)
            $("#sltRegion").removeAttr('disabled');

        if ($("#sltProvincia").length > 0)
            $("#sltProvincia").removeAttr('disabled');

        if ($("#sltCiudad").length > 0)
            $("#sltCiudad").removeAttr('disabled');

        if ($("#sltPrevision").length > 0)
            $("#sltPrevision").removeAttr('disabled');

        if ($("#sltPrevisionTramo").length > 0)
            $("#sltPrevisionTramo").removeAttr('disabled');

    } else {

        $("#txtnombrePac, #txtApePat, #txtApeMat, #txtNombreSocial").attr("disabled", "disabled");
        $("#txtFecNacPac, #txtDireccion, #txtNumDireccionPaciente").attr("disabled", "disabled");
        $("#txtTelefono, #txtOtroTelefono, #txtCorreoElectronico").attr("disabled", "disabled");
        $("#sltsexoPac, #sltgeneroPac").attr("disabled", "disabled");
        $("#chkAcompañante").bootstrapSwitch('disabled', true);


        if ($("#sltNacionalidad").length > 0)
            $("#sltNacionalidad").attr("disabled", "disabled");

        if ($("#sltPais").length > 0)
            $("#sltPais").attr("disabled", "disabled");

        if ($("#sltRegion").length > 0)
            $("#sltRegion").attr("disabled", "disabled");

        if ($("#sltProvincia").length > 0)
            $("#sltProvincia").attr("disabled", "disabled");

        if ($("#sltCiudad").length > 0)
            $("#sltCiudad").attr("disabled", "disabled");

        if ($("#sltPrevision").length > 0)
            $("#sltPrevision").attr("disabled", "disabled");

        if ($("#sltPrevisionTramo").length > 0)
            $("#sltPrevisionTramo").attr("disabled", "disabled");
    }
}

// Obtener Información de Paciente
function GetJsonPacienteNuevo(paciente) {

    return {
        GEN_dir_numeroPaciente: valCampo(paciente.GEN_dir_numeroPaciente),
        GEN_dir_ruralidadPaciente: valCampo(paciente.GEN_dir_ruralidadPaciente),
        GEN_idPais: valCampo(paciente.GEN_idPais),
        GEN_idRegion: valCampo(paciente.GEN_idRegion),
        GEN_idComuna: valCampo(paciente.GEN_idComuna),
        GEN_idCiudad: valCampo(paciente.GEN_idCiudad),
        GEN_idPrevision: valCampo(paciente.GEN_idPrevision),
        GEN_idPrevision_Tramo: valCampo(paciente.GEN_idPrevision_Tramo),
        GEN_nuiPaciente: valCampo(paciente.GEN_nuiPaciente),
        GEN_pac_pac_numeroPaciente: valCampo(paciente.GEN_pac_pac_numeroPaciente),
        GEN_fec_actualizacionPaciente: valCampo(paciente.GEN_fec_actualizacionPaciente),
        GEN_PraisPaciente: valCampo(paciente.GEN_PraisPaciente),
        GEN_numero_documentoPaciente: valCampo(paciente.GEN_numero_documentoPaciente),
        GEN_digitoPaciente: valCampo(paciente.GEN_digitoPaciente),
        GEN_nombrePaciente: valCampo(paciente.GEN_nombrePaciente),
        GEN_ape_paternoPaciente: valCampo(paciente.GEN_ape_paternoPaciente),
        GEN_ape_maternoPaciente: valCampo(paciente.GEN_ape_maternoPaciente),
        GEN_dir_callePaciente: valCampo(paciente.GEN_dir_callePaciente),
        GEN_telefonoPaciente: valCampo(paciente.GEN_telefonoPaciente),
        GEN_idSexo: valCampo(paciente.GEN_idSexo),
        GEN_idTipo_Genero: valCampo(paciente.GEN_idTipo_Genero),
        GEN_fec_nacimientoPaciente: valCampo(paciente.GEN_fec_nacimientoPaciente),
        GEN_otros_fonosPaciente: valCampo(paciente.GEN_otros_fonosPaciente),
        GEN_emailPaciente: valCampo(paciente.GEN_emailPaciente),
        GEN_idIdentificacion: valCampo(paciente.GEN_idIdentificacion),
        GEN_idEstado_Conyugal: valCampo(paciente.GEN_idEstado_Conyugal),
        GEN_idPueblo_Originario: valCampo(paciente.GEN_idPueblo_Originario),
        GEN_idNacionalidad: valCampo(paciente.GEN_idNacionalidad),
        GEN_idTipo_Escolaridad: valCampo(paciente.GEN_idTipo_Escolaridad),
        GEN_idOcupaciones: valCampo(paciente.GEN_idOcupaciones),
        GEN_idTipo_Categoria_Ocupacion: valCampo(paciente.GEN_idTipo_Categoria_Ocupacion),
        GEN_estadoPaciente: "Activo",
        GEN_fecha_fallecimientoPaciente: valCampo(paciente.GEN_fecha_fallecimientoPaciente),
        GEN_responde_nombrePaciente: valCampo(paciente.GEN_responde_nombrePaciente)
    }
}
function GetJsonPaciente() {

    paciente.GEN_idIdentificacion = valCampo(parseInt($("#sltIdentificacion").val()));
    paciente.GEN_numero_documentoPaciente = valCampo($("#txtnumerotPac").val());
    paciente.GEN_digitoPaciente = valCampo($("#txtDigitoPac").val());
    paciente.GEN_nombrePaciente = valCampo($("#txtnombrePac").val());
    paciente.GEN_ape_paternoPaciente = valCampo($("#txtApePat").val());
    paciente.GEN_ape_maternoPaciente = valCampo($("#txtApeMat").val());
    paciente.GEN_dir_callePaciente = valCampo($("#txtDireccion").val());
    paciente.GEN_telefonoPaciente = valCampo($("#txtTelefono").val());
    paciente.GEN_idSexo = valCampo(parseInt($("#sltsexoPac").val()));
    paciente.GEN_idTipo_Genero = valCampo(parseInt($("#sltgeneroPac").val()));
    paciente.GEN_responde_nombrePaciente = valCampo($("#txtNombreSocial").val());
    paciente.GEN_fec_nacimientoPaciente = valCampo($("#txtFecNacPac").val());
    paciente.GEN_otros_fonosPaciente = valCampo($("#txtOtroTelefono").val());
    paciente.GEN_otros_fonosPaciente = valCampo($("#txtOtroTelefono").val());
    paciente.GEN_emailPaciente = valCampo($("#txtCorreoElectronico").val());
    paciente.GEN_estadoPaciente = "Activo";
    paciente.GEN_dir_numeroPaciente = valCampo($("#txtNumDireccionPaciente").val());
    paciente.GEN_fec_actualizacionPaciente = moment(GetFechaActual()).format('YYYY-MM-DD HH:mm:ss');

    if ($("#sltNacionalidad").length > 0)
        paciente.GEN_idNacionalidad = valCampo(parseInt($("#sltNacionalidad").val()));
    if ($("#sltPais").length > 0)
        paciente.GEN_idPais = valCampo(parseInt($("#sltPais").val()));
    if ($("#sltRegion").length > 0)
        paciente.GEN_idRegion = valCampo(parseInt($("#sltRegion").val()));
    if ($("#sltProvincia").length > 0)
        paciente.GEN_idProvincia = valCampo(parseInt($("#sltProvincia").val()));
    if ($("#sltCiudad").length > 0)
        paciente.GEN_idCiudad = valCampo(parseInt($("#sltCiudad").val()));
    if ($("#sltPuebloOriginario").length > 0)
        paciente.GEN_idPueblo_Originario = $("#sltPuebloOriginario").val() == 0 ? null : $("#sltPuebloOriginario").val();
    if ($("#sltCategoriaOcupacional").length > 0)
        paciente.GEN_idTipo_Categoria_Ocupacion = $("#sltCategoriaOcupacional").val() == 0 ? null : $("#sltCategoriaOcupacional").val();
    if ($("#sltOcupacion").length > 0)
        paciente.GEN_idOcupaciones = $("#sltOcupacion").val() == 0 ? null : $("#sltOcupacion").val();
    if ($("#sltEscolaridad").length > 0)
        paciente.GEN_idTipo_Escolaridad = $("#sltEscolaridad").val() == 0 ? null : $("#sltEscolaridad").val();
    if ($("#sltPrevision").length > 0)
        paciente.GEN_idPrevision = $("#sltPrevision").val() == 0 ? null : $("#sltPrevision").val();
    if ($("#sltPrevisionTramo").length > 0)
        paciente.GEN_idPrevision_Tramo = $("#sltPrevisionTramo").val() == 0 ? null : $("#sltPrevisionTramo").val();
}
async function buscarPacientePorId(idPaciente) {

    var url = `${GetWebApiUrl()}GEN_Paciente/${idPaciente}`;
    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data, status, jqXHR) {
            CargarPaciente(data[0]);
        },
        error: function (jqXHR, status) {
            console.log(`Error al buscar paciente ${JSON.stringify(jqXHR)}`);
        }
    });
}
function buscarPacientePorDocumento(idIdentificacion, numero) {
    var url = `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/${idIdentificacion}/${numero}`;
    $.ajax({
        type: 'GET',
        url: url,
        success: function (data, status, jqXHR) {
            if (data.length > 0 && idIdentificacion == 4) {
                tablaRutMaterno(idIdentificacion, numero);               
            }
            else {
                CargarPaciente(data[0]);
                DeshabilitarPaciente(false);
            }
        },
        error: function (jqXHR, status) {
            if (jqXHR.status == 401) {
                LimpiarPaciente();
                $("#txtnumerotPac, #txtDigitoPac").val("");
                DeshabilitarPaciente(true);
            } else
                console.log(`Error al cargar JSON de Persona: ${JSON.stringify(request)}`);
            //
            console.log(`Error al buscar paciente ${JSON.stringify(jqXHR)}`);
        }
    });
}
function pacienteTieneHospitalizaciones(id = null) {
    let response = false;
    let ID = id;
    if (!id) {
        ID = paciente.GEN_idPaciente
    }
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idPaciente=${ID}`;
    if (!id) {
        url += `&idTipoEstado=87&idTipoEstado=88`;
    }

    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data, status, jqXHR) {
            if (data.length > 0 && data[0].Paciente.IdPaciente == ID) {
                response = data;
            }
        },
        error: function (jqXHR, status) {
            if (jqXHR.status == 401) {
            } else
                console.log(`Error al cargar JSON: ${JSON.stringify(request)}`);
            console.log(`Error al buscar Hospitalización ${JSON.stringify(jqXHR)}`);
        }
    });
    return response;
}
function CargarPaciente(pac) {
    let id = 0;
    if ($.isEmptyObject(pac)) {
        //si no existe en base de datos, termina limpiando input 
        //para crear paciente nuevo.
        toastr.info('Se está creando un nuevo paciente.');
        paciente = {};
        if ($("#tblVacunasPaciente").length > 0) {
            $("#aAgregarPaciente, #sltVacunasPaciente").removeAttr("disabled");
            $("#aAgregarPaciente").removeClass("disabled");
        }
        $("#divSiTieneHospitalizaciones").prop("style", "display: none;");
        $("#divNoTieneHospitalizaciones").fadeIn();
    }
    else {
        paciente = {};
        $("#sltIdentificacion").val(pac.GEN_idIdentificacion);
        SltIdentificacion_Change();
        paciente = GetJsonPacienteNuevo(pac);
        paciente.GEN_idPaciente = pac.GEN_idPaciente;
        id = pac.GEN_idPaciente;
        $("#txtnumerotPac").val(pac.GEN_numero_documentoPaciente); 
        $("#txtDigitoPac").val(pac.GEN_digitoPaciente);
        $("#txtnombrePac").val(pac.GEN_nombrePaciente);
        $("#txtApePat").val(pac.GEN_ape_paternoPaciente);
        $("#txtApeMat").val(pac.GEN_ape_maternoPaciente);
        $("#txtDireccion").val(pac.GEN_dir_callePaciente);
        $("#txtNumDireccionPaciente").val(pac.GEN_dir_numeroPaciente);
        $("#txtTelefono").val(pac.GEN_telefonoPaciente);
        ValTipoCampo($("#sltsexoPac"), pac.GEN_idSexo);
        ValTipoCampo($("#sltgeneroPac"), pac.GEN_idTipo_Genero);
        $("#txtNombreSocial").val(pac.GEN_responde_nombrePaciente);
        SltgeneroPac_Change();

        if (pac.GEN_fec_nacimientoPaciente != null) {
            $("#txtFecNacPac").val(moment(pac.GEN_fec_nacimientoPaciente).format('YYYY-MM-DD'));
            let edad = CalcularEdad(moment($("#txtFecNacPac").val()).format("YYYY-MM-DD")).edad;
            $("#txtEdadPac").val(edad);
        }

        $("#txtOtroTelefono").val(pac.GEN_otros_fonosPaciente);
        $("#txtCorreoElectronico").val(pac.GEN_emailPaciente);
        $("#lblIdentificacion").text($("#sltIdentificacion").children("option:selected").text());

        //DATOS EXTRAS
        if ($("#sltPais").length > 0) {
            if (pac.GEN_idPais != null)
                $("#sltPais").val(pac.GEN_idPais).change();
            else
                $("#sltPais").val(0);
        }

        if ($("#sltRegion").length > 0) {
            if (pac.GEN_idRegion != null)
                $("#sltRegion").val(pac.GEN_idRegion).change();
            else
                $("#sltRegion").val(0);
        }

        if ($("#sltProvincia").length > 0) {
            if (pac.GEN_idProvincia != null)
                $("#sltProvincia").val(pac.GEN_idProvincia).change();
            else
                $("#sltProvincia").val(0);
        }

        if ($("#sltCiudad").length > 0) {
            if (pac.GEN_idCiudad != null)
                $("#sltCiudad").val(pac.GEN_idCiudad);
            else
                $("#sltCiudad").val(0);
        }

        if ($("#sltPuebloOriginario").length > 0) {
            if (pac.GEN_idNacionalidad != null)
                $("#sltPuebloOriginario").val(pac.GEN_idNacionalidad);
            else
                $("#sltPuebloOriginario").val(0);
        }

        if ($("#sltPuebloOriginario").length > 0) {
            if (pac.GEN_idPueblo_Originario != null)
                $("#sltPuebloOriginario").val(pac.GEN_idPueblo_Originario);
            else
                $("#sltPuebloOriginario").val(0);
        }

        if ($("#sltCategoriaOcupacional").length > 0) {
            if (pac.GEN_idTipo_Categoria_Ocupacion != null)
                $("#sltCategoriaOcupacional").val(pac.GEN_idTipo_Categoria_Ocupacion);
            else
                $("#sltCategoriaOcupacional").val(0);
        }

        if ($("#sltOcupacion").length > 0) {
            if (pac.GEN_idOcupaciones != null)
                $("#sltOcupacion").val(pac.GEN_idOcupaciones);
            else
                $("#sltOcupacion").val(0);
        }

        if ($("#sltEscolaridad").length > 0) {
            if (pac.GEN_idTipo_Escolaridad != null)
                $("#sltEscolaridad").val(pac.GEN_idTipo_Escolaridad);
            else
                $("#sltEscolaridad").val(0);
        }

        if ($("#sltPrevision").length > 0) {
            if (pac.GEN_idPrevision != null)
                $("#sltPrevision").val(pac.GEN_idPrevision).change();
            else
                $("#sltPrevision").val(0).change();
        }

        if ($("#sltPrevisionTramo").length > 0) {
            if (pac.GEN_idPrevision_Tramo != null)
                $("#sltPrevisionTramo").val(pac.GEN_idPrevision_Tramo);
            else
                $("#sltPrevisionTramo").val(0);
        }

        if ($("#sltTipoAtencion").length > 0) {
            if (pac.GEN_fec_nacimientoPaciente != null) {
                let anhos = CalcularEdad(pac.GEN_fec_nacimientoPaciente).años;
                if (anhos < 15) {
                    $("#sltTipoAtencion").val(3).change();
                    $("#msjPediatrica").prop("style", "display: block;");
                    $("#msjAdulto").prop("style", "display: none;");
                } else {
                    $("#msjAdulto").prop("style", "display: block;");
                    $("#msjPediatrica").prop("style", "display: none;");
                }
            }
        }
        $(".md-form .form").addClass("active");
    }

    
}
function CargarPacienteActualizado(pac) {
    let id = 0;
        paciente = {};
        //Inicio

        try {

            $.ajax({
                type: 'GET',
                url: url = `${GetWebApiUrl()}GEN_Paciente/${pac}`,
                async: false,
                success: function (data, status, jqXHR) {
                    pac = data[0];
                },
                error: function (jqXHR, status) {
                    console.log("Error al cargar Profesional: " + jqXHR.responseText);
                }
            });

        } catch (err) {
            alert(err.message);
        }

        //FIN
        $("#sltIdentificacion").val(pac.GEN_idIdentificacion);
        SltIdentificacion_Change();
        paciente = GetJsonPacienteNuevo(pac);
        paciente.GEN_idPaciente = pac.GEN_idPaciente;
        id = pac.GEN_idPaciente;
        $("#txtnumerotPac").val(pac.GEN_numero_documentoPaciente);
        $("#txtDigitoPac").val(pac.GEN_digitoPaciente);
        $("#txtnombrePac").val(pac.GEN_nombrePaciente);
        $("#txtApePat").val(pac.GEN_ape_paternoPaciente);
        $("#txtApeMat").val(pac.GEN_ape_maternoPaciente);
        $("#txtDireccion").val(pac.GEN_dir_callePaciente);
        $("#txtNumDireccionPaciente").val(pac.GEN_dir_numeroPaciente);
        $("#txtTelefono").val(pac.GEN_telefonoPaciente);
        ValTipoCampo($("#sltsexoPac"), pac.GEN_idSexo);
        ValTipoCampo($("#sltgeneroPac"), pac.GEN_idTipo_Genero);
        $("#txtNombreSocial").val(pac.GEN_responde_nombrePaciente);
        SltgeneroPac_Change();

        if (pac.GEN_fec_nacimientoPaciente != null) {
            $("#txtFecNacPac").val(moment(pac.GEN_fec_nacimientoPaciente).format('YYYY-MM-DD'));
            let edad = CalcularEdad(moment($("#txtFecNacPac").val()).format("YYYY-MM-DD")).edad;
            $("#txtEdadPac").val(edad);
        }

        $("#txtOtroTelefono").val(pac.GEN_otros_fonosPaciente);
        $("#txtCorreoElectronico").val(pac.GEN_emailPaciente);
        $("#lblIdentificacion").text($("#sltIdentificacion").children("option:selected").text());

        //DATOS EXTRAS
        if ($("#sltPais").length > 0) {
            if (pac.GEN_idPais != null)
                $("#sltPais").val(pac.GEN_idPais).change();
            else
                $("#sltPais").val(0);
        }

        if ($("#sltRegion").length > 0) {
            if (pac.GEN_idRegion != null)
                $("#sltRegion").val(pac.GEN_idRegion).change();
            else
                $("#sltRegion").val(0);
        }

        if ($("#sltProvincia").length > 0) {
            if (pac.GEN_idProvincia != null)
                $("#sltProvincia").val(pac.GEN_idProvincia).change();
            else
                $("#sltProvincia").val(0);
        }

        if ($("#sltCiudad").length > 0) {
            if (pac.GEN_idCiudad != null)
                $("#sltCiudad").val(pac.GEN_idCiudad);
            else
                $("#sltCiudad").val(0);
        }

        if ($("#sltPuebloOriginario").length > 0) {
            if (pac.GEN_idNacionalidad != null)
                $("#sltPuebloOriginario").val(pac.GEN_idNacionalidad);
            else
                $("#sltPuebloOriginario").val(0);
        }

        if ($("#sltPuebloOriginario").length > 0) {
            if (pac.GEN_idPueblo_Originario != null)
                $("#sltPuebloOriginario").val(pac.GEN_idPueblo_Originario);
            else
                $("#sltPuebloOriginario").val(0);
        }

        if ($("#sltCategoriaOcupacional").length > 0) {
            if (pac.GEN_idTipo_Categoria_Ocupacion != null)
                $("#sltCategoriaOcupacional").val(pac.GEN_idTipo_Categoria_Ocupacion);
            else
                $("#sltCategoriaOcupacional").val(0);
        }

        if ($("#sltOcupacion").length > 0) {
            if (pac.GEN_idOcupaciones != null)
                $("#sltOcupacion").val(pac.GEN_idOcupaciones);
            else
                $("#sltOcupacion").val(0);
        }

        if ($("#sltEscolaridad").length > 0) {
            if (pac.GEN_idTipo_Escolaridad != null)
                $("#sltEscolaridad").val(pac.GEN_idTipo_Escolaridad);
            else
                $("#sltEscolaridad").val(0);
        }

        if ($("#sltPrevision").length > 0) {
            if (pac.GEN_idPrevision != null)
                $("#sltPrevision").val(pac.GEN_idPrevision).change();
            else
                $("#sltPrevision").val(0).change();
        }

        if ($("#sltPrevisionTramo").length > 0) {
            if (pac.GEN_idPrevision_Tramo != null)
                $("#sltPrevisionTramo").val(pac.GEN_idPrevision_Tramo);
            else
                $("#sltPrevisionTramo").val(0);
        }

        if ($("#sltTipoAtencion").length > 0) {
            if (pac.GEN_fec_nacimientoPaciente != null) {
                let anhos = CalcularEdad(pac.GEN_fec_nacimientoPaciente).años;
                if (anhos < 15) {
                    $("#sltTipoAtencion").val(3).change();
                    $("#msjPediatrica").prop("style", "display: block;");
                    $("#msjAdulto").prop("style", "display: none;");
                } else {
                    $("#msjAdulto").prop("style", "display: block;");
                    $("#msjPediatrica").prop("style", "display: none;");
                }
            }
        }
    $(".md-form .form").addClass("active");
    /*
    }*/


}
function GuardarPaciente() {
    let method;
    let url = `${GetWebApiUrl()}GEN_Paciente/`;

    if (paciente.GEN_idPaciente == undefined || paciente.GEN_idPaciente == 0) {
        method = 'POST';
    }
    else {
        method = 'PUT';
        url += paciente.GEN_idPaciente;
    }

    GetJsonPaciente();
    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(paciente),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {

            if (data != undefined)
                paciente.GEN_idPaciente = data.GEN_idPaciente;

        },
        error: function (jqXHR, status) {
            console.log(`Error al guardar Paciente: ${JSON.stringify(jqXHR)} `);
        }
    });
}

function tablaRutMaterno(idIdentificacion, numeroDocumento) {
    var url = `${GetWebApiUrl()}GEN_Paciente?idIdentificacion=${idIdentificacion}&numero_documentoPaciente=${numeroDocumento}`;
    $.ajax({
        type: 'GET',
        url: url,
        success: function (data, status, jqXHR) {
            var adataset = [];
            $.each(data, function (i, r) {
                adataset.push([
                    r.Id,
                    r.Nombre,
                    r.ApellidoPaterno,
                    r.ApellidoMaterno,
                    moment(r.FechaNacimiento).format('DD-MM-YYYY')
                ]);
            });

            $('#tblIngresoExistente').addClass('nowrap').DataTable({
                data: adataset,
                order: [],
                columns: [
                    { title: 'ID paciente' },
                    { title: 'Nombre' },
                    { title: 'Apellido paterno' },
                    { title: 'apellido materno' },
                    { title: 'Fecha de nacimiento' },
                    { title: '' }
                ],                
                columnDefs: [
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var fila = meta.row;
                            let botones;
                            botones = `<a id = 'linkseleccionar' data-id='${adataset[fila][0]}' class='btn btn-success' onclick = 'linkseleccionar(this)' > <i class='fa fa-edit'></i> Seleccionar</a >`;
                            return botones;
                        }
                    },
                ],
                bDestroy: true
            });
            $('#mdlRutMaterno').modal('show');
        },
        error: function (jqXHR, status) {
            console.log(`Error al buscar paciente ${JSON.stringify(jqXHR)}`);
        }
    });
}

// RUT MATERNO
function linkseleccionar(e) {
    var id = $(e).data('id');
    buscarPacientePorId(id).then(() => {
        DeshabilitarPaciente(false);
        $('#mdlRutMaterno').modal('hide');
    });
}

// ACOMPAÑANTE
function ActivarAcompañante() {
    $("#divPacienteAcompañante").show();
}