﻿
function ShowModalHospitalizaciones(idPaciente, nombrePaciente) {
    let data = [];
    let elemento = $("#mdlVerHospitalizaciones .modal-body");
    $("#mdlVerHospitalizaciones").modal("show");
    showModalCargandoComponente(elemento);
    let response = pacienteTieneHospitalizaciones(idPaciente);
    if (response) {
        array = Array.from(response);
    } else {
        array = [];
    }
    $.each(array, function (key, hospitalizacion) {
        data.push([
            hospitalizacion.Id,
            hospitalizacion.Fecha != null ? moment(hospitalizacion.Fecha).toDate().format("dd-MM-yyyy") : "x",
            hospitalizacion.Categorizacion != null ? hospitalizacion.Categorizacion.Codigo : "x",
            hospitalizacion.Cama != null ? hospitalizacion.Cama : "x",
            hospitalizacion.Estado != null ? hospitalizacion.Estado.Valor : "x"
        ]);
    });
    cargarTablaHistorialHospitalizaciones(data);
    $("#nomPacModalVerHospitalizaciones").html(nombrePaciente);
    hideModalCargandoComponente(elemento);
}

function pacienteTieneHospitalizaciones(id) {
    let response = false;
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idPaciente=${id}`;
    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data, status, jqXHR) {
            if (data.length > 0 && data[0].Paciente.IdPaciente == id) {
                console.log("Tiene HOS!");
                response = data;
            }

        },
        error: function (jqXHR, status) {
            if (jqXHR.status == 401) {
            } else
                console.log(`Error al cargar JSON: ${JSON.stringify(request)}`);
            console.log(`Error al buscar Hospitalización ${JSON.stringify(jqXHR)}`);
        }
    });
    return response;
}

function cargarTablaHistorialHospitalizaciones(json) {
    $('#tblHistorialHospitalizacionesModal').addClass("nowrap").DataTable({
        data: json,
        columns: [
            { title: "ID Hospitalización" },
            { title: "Fecha"},
            { title: "Categorización" },
            { title: "Cama" },
            { title: "Estado" },
            { title: "" },
        ],
        columnDefs: [
            {
                targets: 5,
                data: null,
                orderable: false,
                render: function (data, type, row, meta) {
                    var fila = meta.row;
                    var botones =
                    `
                    <button class='btn btn-primary btn-circle'
                        onclick='toggle("#div_accionesMdlHisHOS${fila}"); return false;'>
                        <i class="fa fa-list" style="font-size:15px;"></i>
                    </button>
                    <div id='div_accionesMdlHisHOS${fila}' class='btn-container' style="display: none;">
                        <center>
                            <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                            <div class="rounded-actions">
                                <center>
                    `;

                    botones +=
                    `
                    <a id='linkImprimir' data-id='${json[fila][0]}' class='btn btn-info btn-circle btn-lg' href='#/'
                        onclick='linkImprimirHisHos(this);' data-toggle="tooltip" data-placement="left" title="Imprimir">
                        <i class='fa fa-print'></i>
                    </a><br>
                    `;

                    botones +=
                    `
                                </center >
                            </div >
                        </center >
                    </div >
                    `;

                    return botones;
                }
            }
        ],
        "bDestroy": true
    });
}

function linkImprimirHisHos(e) {
    $('#modalCargando').show();
    var id = $(e).data("id");
    $('#frameImpresionHis').attr('src', ObtenerHost() + '/Vista/ModuloHOS/ImprimirFormularioAdmision.aspx?id=' + id);
    $('#mdlImprimirHisHos').modal('show');
}
