
var functionEscape = null;
var cant = 0, size = 0;
var parametros = {};
var archivosAEliminar = [];

$(function () {

        $('#drag-and-drop-zone').dmUploader({ 
            url: ObtenerHost() + "/Vista/Handlers/MetodosGenerales.ashx?method=GuardarArchivo",
        maxFileSize: 5000000, // 5 Megs max
        maxAllFilesSize: function () {
            return size;
        },

        auto: false,

        queue: false,

        extraData: function () {
            return parametros;
        },

        onDragEnter: function () {
            // Happens when dragging something over the DnD area
            this.addClass('active');
        },

        onDragLeave: function () {
            // Happens when dragging something OUT of the DnD area
            this.removeClass('active');
        },

        onInit: function () { },

        onComplete: function () { },

        onNewFile: function (id, file) {

            // When a new file is added using the file selector or the DnD area
            ui_multi_add_file(id, file, true);

            if (typeof FileReader !== "undefined") {
                
                var reader = new FileReader();
                var a = $('#uploaderFile' + id).find('a');
                a.attr("download", file.name);

                reader.onload = function (e) {
                    a.attr('href', e.target.result);
                }

                reader.readAsDataURL(file);
                
            }
        },

        onBeforeUpload: function (id) {
            // about tho start uploading a file
            ui_multi_update_file_status(id, 'Subiendo', 'Subiendo...');
            ui_multi_update_file_progress(id, 0, '', true);
        },

        onUploadProgress: function (id, percent) {
            // Updating file progress
            ui_multi_update_file_progress(id, percent);
        },

        onUploadSuccess: function (id, data) {
            // A file was successfully uploaded
            ui_multi_update_file_status(id, 'success', 'Subida Completada');
            ui_multi_update_file_progress(id, 100, 'success', false);
            finalizarProceso();
        },

        onUploadCanceled: function (id) {
            // Happens when a file is directly canceled by the user.
            ui_multi_update_file_status(id, 'warning', 'Cancelado por el Usuario');
            ui_multi_update_file_progress(id, 0, 'warning', false);
        },

        onUploadError: function (id, xhr, status, message) {
            // Happens when an upload error happens
            ui_multi_update_file_status(id, 'danger', message);
            ui_multi_update_file_progress(id, 0, 'danger', false);
        },

        onFallbackMode: function () {},
        onFileSizeError: function (file) {
            alert('El Archivo "' + file.name + '" sobrepasa los 5 MB, por ende no puede ser ajuntado al formulario.');
        }

    });
    
    $('#files').on('click', 'button.cancel', function (evt) {

        evt.preventDefault();

        var id = $(this).closest('li.media-1').data('file-id');
        var sizeProp = parseInt($(this).closest('li.media-1').data('file-size'));
        
        if ($(this).closest('li.media-1').hasClass('newFile'))
            $('#drag-and-drop-zone').dmUploader('cancel', id);
        else {

            var nombreArchivo = $(this).closest('li.media-1').data('file-name');
            var moduloId = $(this).closest('li.media-1').data('modulo-id');
            var modulo = $(this).closest('li.media-1').data('modulo-nombre'); 
            
            archivosAEliminar.push({ id: moduloId, modulo: modulo, name: nombreArchivo, size: sizeProp });

        }
        
        $(this).closest('li.media-1').remove();

        size -= sizeProp;
        $('#stgNumeroArchivos').text(GetCantidadTodosArchivos() + " Archivos / " + ConvertirBytes('mb', size) + " MB.");

        if (GetCantidadTodosArchivos() === 0) {
            $('#files').children('li.empty').removeAttr('style');
            $('#stgNumeroArchivos').text("0 Archivos.");
        }

    });
    
});

// Creates a new file and add it to our list
function ui_multi_add_file(id, file, esNuevoArchivo) {

    if ((size + file.size) <= 5000000) {

        var template = $('#files-template').text();
        template = template.replace('%%filename%%', file.name);
        template = template.replace('%%size%%', ConvertirBytes("mb", file.size) + " MB.");

        template = $(template);
        template.prop('id', 'uploaderFile' + id);
        template.data('file-id', id);
        template.data('file-name', file.name);
        template.data('modulo-id', file.id);
        template.data('modulo-nombre', file.modulo);

        template.prop('size', file.size);
        template.data('file-size', file.size);

        if (!esNuevoArchivo) template.removeClass("newFile");

        $('#files').find('li.empty').fadeOut(); 
        $('#files').prepend(template);
        
        if (!esNuevoArchivo) {
            $('#uploaderFile' + id).find('a').click(function () {
                DescargarArchivo(JSON.stringify({
                    id: $(this).closest('li.media-1').data('modulo-id'),
                    modulo: $(this).closest('li.media-1').data('modulo-nombre'),
                    nombreArchivo: $(this).closest('li.media-1').data('file-name')
                }));
            });
            ui_multi_update_file_status(id, 'success', 'Subida Completada');
            ui_multi_update_file_progress(id, 100, 'success', false);
        }

        size += file.size;
        $('#stgNumeroArchivos').text(GetCantidadTodosArchivos() + " Archivos / " + ConvertirBytes('mb', size) + " MB.");

    } else
        alert('No puede sobrepasar los 5 MB en el total de archivos adjuntados.');
    
}

// Changes the status messages on our list
function ui_multi_update_file_status(id, status, message) {
    $('#uploaderFile' + id).find('span').html(message).prop('class', 'status text-' + status);
}

// Updates a file progress, depending on the parameters it may animate it or change the color.
function ui_multi_update_file_progress(id, percent, color, active) {
    color = (typeof color === 'undefined' ? false : color);
    active = (typeof active === 'undefined' ? true : active);

    var bar = $('#uploaderFile' + id).find('div.progress-bar');

    bar.width(percent + '%').attr('aria-valuenow', percent);
    bar.toggleClass('progress-bar-striped progress-bar-animated', active);

    if (percent === 0) {
        bar.html('');
    } else 
        bar.html(percent + '%');
    
    if (color !== false) {
        bar.removeClass('bg-success bg-info bg-warning bg-danger');
        bar.addClass('bg-' + color);
    }
}

function cargarArchivosExistentes1(id, modulo, bpermitirSubidaArchivo) {
    
    if (!bpermitirSubidaArchivo) {
        $("#titulo").remove();
        $(".cancel").remove();
        $("#divSubirArchivos").remove();
        $("#divListaArchivos").removeClass("col-md-6");
    }

    limpiarTodosArchivos();
    cargarArchivosExistentes(id, modulo);

}

function cargarArchivosExistentes(id, modulo) {
    
    $.ajax({
        type: "POST",
        url: ObtenerHost() + "/Vista/Handlers/MetodosGenerales.ashx?method=CargarArchivos&id=" + id + "&modulo=" + modulo,
        contentType: "application/json",
        dataType: "json",
        data: {},
        async: false,
        success: function (data) {

            if (data.length > 0) {
                $.each(data, function (index, item) {
                    ui_multi_add_file(index, item, false);
                });
            }
            
        }
    });

}

function GetCantidadTodosArchivos() {
    return $('#files').children('li.media-1').length;
}

function GetCantidadNuevosArchivos() {
    return $('#files').children('li.media-1.newFile').length;
}

function limpiarTodosArchivos() {

    if ($("li.media-1").length > 0) {

        size = 0;
        $.each($("li.media-1"), function (index, value) {
            $(this).remove();
        });
        $('#files').children('li.empty').removeAttr('style');
        $('#stgNumeroArchivos').text("0 Archivos.");

    }
    
}

function eliminarArchivos() {

    $.ajax({
        type: "POST",
        url: ObtenerHost() + "/Vista/Handlers/MetodosGenerales.ashx?method=EliminarArchivos",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({ array: archivosAEliminar}),
        async: false,
        success: function (data) {}
    });

}

function SubirTodosArchivos(id, modulo, functionEsc) {

    if (archivosAEliminar.length > 0)
        eliminarArchivos();

    if (GetCantidadNuevosArchivos() > 0) {
        functionEscape = functionEsc;
        parametros = { 'id': id, 'modulo': modulo };
        $('#drag-and-drop-zone').dmUploader('start');
        $('html, body').animate({ scrollTop: $("#files").offset().top - 40 }, 500);
    } else if (functionEsc != null) functionEsc();
}

function finalizarProceso() {

    cant++;
    if (functionEscape !== null && cant === GetCantidadNuevosArchivos())
        functionEscape();

}
