﻿
var arrayCarteraProcedimientos = [], arrayCarteraLaboratorio = [];
var arrayProcedimientosSeleccionados = [], arrayExamenesImagenologia = [], arrayMedicamentosSeleccionados = [], arrayInterconsultoresSeleccionados = [], arrayExamenesLaboratorio = [];
const TIPO_CARTERA = { Procedimiento: 51, ProcedimientoObstetricia: 158, ExamenLaboratorio: 61, Imagenologia: 60 };
//elementos para llenar la tabla de examenes.
let elementos = [];

$(document).ready(function () {

    comboViaAdministracion();

    // PROCEDIMIENTOS
    $("#txtFiltroProcedimientos").on('keyup', function () {

        let search = $.trim($(this).val().toLowerCase());

        let result = $.grep(arrayCarteraProcedimientos, function (el) {
            return el.Descripcion.toLowerCase().indexOf(search) > -1;
        });

        $("#divListaProcedimientosModal .filtro").each(function () {

            let id = parseInt($(this).data("id-cartera"));
            let component = $(this);
            let exist = result.find(item => {
                return item.IdCarteraServicio == id;
            });

            (!exist) ? component.hide() : component.show();

        });

        (result.length == 0) ? $("#divProcedimientoVacio").show() : $("#divProcedimientoVacio").hide();

    });

    $("#aAgregarMedicamento").click((evt) => {
        modificarMedicamento(evt.target);
    });

});

function comboViaAdministracion() {

    $("#sltViaAdministracion").empty();
    $("#sltViaAdministracion").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Tipo_Via_Administracion/Combo`,
        success: function (data, status, jqXHR) {
            $.each(data, function () {
                $("#sltViaAdministracion").append(`<option value='${this.Id}'>${this.Valor}</option>`);
            });
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Via de administración ${JSON.stringify(jqXHR)}`);
        }
    });
}

// CARGAR
function cargarIndicacionesUrg(jsonAtencionClinica) {

    CargarProfesionalAtencionClinica(niSession.ID_PROFESIONAL);

    // INTERCONSULTOR
    if (jsonAtencionClinica.Interconsultor.length > 0) {
        arrayInterconsultoresSeleccionados = jsonAtencionClinica.Interconsultor;
        dibujarHtmlInterconsultor();
    }

    // PROCEDIMIENTOS
    if (jsonAtencionClinica.Procedimientos.length > 0) {
        modalProcedimientos(false);
        arrayProcedimientosSeleccionados = jsonAtencionClinica.Procedimientos;
        CargarCarteraServicio(TIPO_CARTERA.Procedimiento);
        dibujarProcedimientosSeleccionados();
    }

    // MEDICAMENTOS E INSUMOS
    if (jsonAtencionClinica.Medicamentos.length > 0) {
        arrayMedicamentosSeleccionados = jsonAtencionClinica.Medicamentos;
        dibujarHtmlMedicamentos();
    }

    // EXAMENES DE LABORATORIO
    if (jsonAtencionClinica.ExamenesLaboratorio.length > 0) {
        modalExamenesLaboratorio(false);
        arrayExamenesLaboratorio = jsonAtencionClinica.ExamenesLaboratorio;
        CargarCarteraServicio(TIPO_CARTERA.ExamenLaboratorio);
        dibujarExamenesLaboratorioSeleccionados();
    }
    // EXAMENES DE IMAGENOLOGIA
    if (jsonAtencionClinica.ExamenesImagenologia.length > 0) {
        modalExamenesImagenologia(false)
        arrayExamenesImagenologia = jsonAtencionClinica.ExamenesImagenologia;
        CargarCarteraServicio(TIPO_CARTERA.Imagenologia)
        dibujarExamenesImageonologiaSeleccionados()
    }
    let arsenalMedicamentos = getJsonMedicamentosArsenal();
    $("#thMedicamentos").typeahead(setMedicamentosTypeAhead(arsenalMedicamentos));
    $("#thMedicamentos").change(() => $("#thMedicamentos").removeData("id"));
    $("#thMedicamentos").blur(() => {
        if ($("#thMedicamentos").data("id") == undefined)
            $("#thMedicamentos").val("");
    });

}
function CargarProfesionalAtencionClinica(idProfesional) {
    const data = CargarJsonProfesional(idProfesional);
    const profesional = `${data.GEN_nombreProfesional} ${data.GEN_apellidoProfesional} ${data.GEN_sapellidoProfesional} | `
        + `${data.GEN_rutProfesional + (data.GEN_digitoProfesional !== null ? `-${data.GEN_digitoProfesional}` : '')}`;
    $("#txtProfesionalEvolucionAtencion").val(profesional);
    $("#spnProfesionalEvolucionAtencion").text(profesional);
}

// PROCEDIMIENTOS
function modalProcedimientos(showModal) {

    if ($("#divListaProcedimientosModal").html() == "") {

        const idUbicacion = ($("#sltTipoAtencion").val() == "2") ? TIPO_CARTERA.ProcedimientoObstetricia : TIPO_CARTERA.Procedimiento;
        arrayCarteraProcedimientos = GetJsonCartera(idUbicacion);
        SetHtmlCarteraArancel(arrayCarteraProcedimientos, "divListaProcedimientosModal", TIPO_CARTERA.Procedimiento);
    }

    if (showModal)
        $('#mdlProcedimientos').modal('show');

}
function dibujarProcedimientosSeleccionados() {
    SetTablaCarteraArancel(TIPO_CARTERA.Procedimiento);
}
function VerProcedimiento(id, tipoCartera) {
    showCerrarCarteraServicio(id, tipoCartera, "ver");
    $("#txtObservacionesCarteraArancel").attr("disabled", "disabled");
    $("#mdlCerrarCarteraServicio .modal-footer").hide();
}
function CargarArrayProcedimientos() {
    CargarArrayCarteraServicio(TIPO_CARTERA.Procedimiento);
}
//IMAGENOLOGIA
//Muestra modal examen imagenonologia
function modalExamenesImagenologia(showModal) {
    if (showModal)
        $("#mdlImagenologia").modal("show")

    arrayCarteraLaboratorio = GetJsonCartera(TIPO_CARTERA.Imagenologia);
    //setHtmlExamenes(arrayCarteraLaboratorio, TIPO_CARTERA.ExamenLaboratorio);
    setHtmlImagenologia(arrayCarteraLaboratorio)
}

function setHtmlImagenologia(data) {
    //divModalExamenesImagenologia\
    let emptytempl = "No existe el examen especificado ({{query}})";
    let templ = `<span>
                    <span class="{{IdCarteraServicio}}">{{DescripcionCarteraServicio}}</span>
                </span>`;
    let source = "GEN_cartera";
    InicializarTypeAhead("#thExamenesImagenologia", "#divExamLabImg", "#addElementTableExamImg", data, emptytempl, templ, source, TIPO_CARTERA.Imagenologia)
    
    $("#divModalExamenesImagenologia").empty()
    let html = ""
    let idCarteraServArancel
    let examenesOrdenados = [...new Set(data.map(a => a.IdGrupoCartera))].map(e => {
        return {
            IdGrupoCartera: e,
            DescripcionGrupo: data.find(x => x.IdGrupoCartera == e).DescripcionGrupoCartera,
            Aranceles: data.filter(l => l.IdGrupoCartera == e).map(q => {
                idCarteraServArancel = q.Aranceles.map(f => f.IdCarteraServicioArancel)[0]
                return {
                    IdCarteraServicio: q.IdCarteraServicio,
                    IdCarteraServicioArancel: idCarteraServArancel,
                    DescripcionCarteraServicio: q.DescripcionCarteraServicio
                }
            })
        }
    })
    examenesOrdenados.map((a, index) => {
        html += `<div class="card" id="card-${index}">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <a class="btn btn-link"
                                href="#"
                                data-toggle="collapse"
                                data-target="#collapse-${a.IdGrupoCartera}"
                                aria-expanded="true"
                                aria-controls="collapseOne"
                                onclick="(e)=>{ e.preventDefault()}">
                              ${a.DescripcionGrupo}
                            </a>
                        </h5>
                    </div>
                    <!--collpase one-->
                    <div id="collapse-${a.IdGrupoCartera}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body row" id="card-body-${a.IdGrupoCartera}">
                            
                        </div>
                    </div>
                </div>`;
    })
    $("#divModalExamenesImagenologia").append(html)
    examenesOrdenados.map(a => {
        html = ""

        a.Aranceles.map(i => {

            console.log(i)
            //IdCarteraServicioArancel
            //Revisar esto
            html += `<div class="col-md-6 filtro" data-id-cartera="${i.IdCarteraServicio}">
                        <div class="row">
                            <h5 class="bg-light p-1" style="cursor:pointer !important; width: 100%;">
                                <button id="btn-${i.IdCarteraServicio}"
                                    class="btn btn-outline-secondary text-left"
                                type='button'
                                data-id="${i.IdCarteraServicio}"
                                data-id-cartera="${i.IdCarteraServicioArancel}"
                                data-text-servicio="${i.DescripcionCarteraServicio}"
                                data-count-aranceles="1"
                                data-id-tipo-cartera="60"
                                onclick="modificarCarteraArancel(this)">
                                ${i.DescripcionCarteraServicio}
                                </button>
                            </h5>
                    </div>
                    </div >`;
        })
        $("#card-body-" + a.IdGrupoCartera).append(html)
    })
    $("#aAceptarExamenesImagenologiaModal").unbind().click(function () {
        AgregarObservacionesCarteraArancel(TIPO_CARTERA.Imagenologia);
    });
}
// EXÁMENES LABORATORIO
function modalExamenesLaboratorio(showModal) {

    if ($("#divListaExamenesLaboratorioModal").html() == "") {

        arrayCarteraLaboratorio = GetJsonCartera(TIPO_CARTERA.ExamenLaboratorio);
        //SetHtmlCarteraArancel(arrayCarteraLaboratorio, "divListaExamenesLaboratorioModal", TIPO_CARTERA.ExamenLaboratorio);
        setHtmlExamenes(arrayCarteraLaboratorio, TIPO_CARTERA.ExamenLaboratorio);
    }

    if (showModal)
        $('#mdlExamenes').modal('show');

}
function dibujarExamenesImageonologiaSeleccionados() {
    
    SetTablaCarteraArancel(TIPO_CARTERA.Imagenologia);
}
function dibujarExamenesLaboratorioSeleccionados() {
    SetTablaCarteraArancel(TIPO_CARTERA.ExamenLaboratorio);
}
function cargarArrayExamenesImageonologia() {
    CargarArrayCarteraServicio(TIPO_CARTERA.Imagenologia);
}
function cargarArrayExamenesLaboratorio() {
    CargarArrayCarteraServicio(TIPO_CARTERA.ExamenLaboratorio);
}

// CARTERA ARANCEL
function GetJsonCartera(IdUbicacion) {

    //if ($("#sltTipoAtencion").val() != "2")
    //    IdUbicacion = 51;

    let array = [];
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Cartera_Servicio/${IdUbicacion}/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            array = data;
        }
    });

    return array;
}

//Imprime los examenes en el html
function setHtmlExamenes(arrayCarteraArancel, tipoCartera) {
    
    //Vaciar contenido para volver a rellenar
    $("#accordion").empty();
    let tipoGrupo = "";
    let categorias = [];
    //Inicializar input filtro para los examenes.

    let emptytempl = "No existe el Examen especificado ({{query}})";
    let templ = `<span>
                    <span class="{{IdCarteraServicio}}">{{DescripcionCarteraServicio}}</span>
                </span>`;
    let source = "GEN_cartera";

    InicializarTypeAhead("#thExamenesLaboratorio", "#divExamLab", "#addElementTableExam", arrayCarteraArancel, emptytempl, templ, source, TIPO_CARTERA.ExamenLaboratorio)
    //Ordenar arreglo
    if (tipoCartera === TIPO_CARTERA.ExamenLaboratorio) {
        arrayCarteraArancel = arrayCarteraArancel.sort(function (a, b) {
            return a.IdGrupoCartera - b.IdGrupoCartera;
        });
    }
    arrayCarteraArancel.map((e) => {

        //Extracción de grupos
        if (e.DescripcionGrupoCartera != null && tipoGrupo != e.DescripcionGrupoCartera) {
            categorias.push(e.DescripcionGrupoCartera);
        }
        tipoGrupo = e.DescripcionGrupoCartera;
    })
    //Dibuja los contenedores o categorias
    categorias.map((a, index) => {
        //Quita los espacio para setear id a los elementos html
        let grupo = a.replace(/ /g, "");
        html = `<div class="card" id="card-${index}">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <a class="btn btn-link"
                                href="#"
                                data-toggle="collapse"
                                data-target="#collapse-${grupo}"
                                aria-expanded="true"
                                aria-controls="collapseOne"
                                onclick="(e)=>{ e.preventDefault()}">
                              ${a}
                            </a>
                        </h5>
                    </div>
                    <!--collpase one-->
                    <div id="collapse-${grupo}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body row" id="card-body-${grupo}">
                           
                        </div>
                    </div>
                </div>`;

        $("#accordion").append(html);
    })

    //Dibuja los examenes dentro de las categorias
    arrayCarteraArancel.map((a, index) => {

        let idA = `btn-${a.IdCarteraServicio}`;
        let grupo = a.DescripcionGrupoCartera.replace(/ /g, "");
        html = `
            <div class="col-md-3 filtro" data-id-cartera="${a.IdCarteraServicio}">
                <div class="row">
                    <h5 class="bg-light p-1" style="cursor:pointer !important; width: 100%;">
                        <button id="${idA}"
                            class="btn btn-${arrayExamenesLaboratorio.indexOf(a.DescripcionCarteraServicio) != -1 ? "" : "outline-" }secondary text-left"
                            type='button'
                            data-id="${a.IdCarteraServicio}"
                            data-id-cartera="${a.Aranceles.length === 1 ? a.Aranceles[0].IdCarteraServicioArancel : null}"
                            data-text-servicio="${a.DescripcionCarteraServicio}"
                            data-count-aranceles="${a.Aranceles.length}"
                            data-id-tipo-cartera="61">
                            ${a.DescripcionCarteraServicio}
                        </button>
                    </h5>
                </div>
            </div>`;
        $(`#card-body-${grupo}`).append(html);
        //Añade evento click a los botones
        $(`#${idA}`).unbind().on('click', function () {
            if (a.Aranceles.length === 1) {
                $(this).data("id", a.Aranceles[0].IdCarteraServicioArancel);
            }

            if ($(this).data("id")) {
                modificarCarteraArancel(this);
            }
        })
    })

    $("#aAceptarExamenesLaboratorioModal").unbind().click(function () {
        AgregarObservacionesCarteraArancel(tipoCartera);
    });

}
//Imprime la tabla exámenes
function printTableExamen() {
    $("#tbodyExamLab").empty();
    let content;
    if (arrayExamenesLaboratorio.length > 0) {
        arrayExamenesLaboratorio.map((e) => {
          
            content = `<tr id="trMedicamentosModal_" class='text-center'>
                        <td>${e.Descripcion}</td>
                        <td>
                            <a class="aQuitarExamen"
                                onclick='QuitarCarteraServicio(${e.IdCarteraArancel}, 61, this)' 
                                data-id-cartera="${e.IdCarteraServicio}" 
                                data-id-tipo-cartera="61"
                                data-name="${e.Descripcion}" style="color:red; cursor:pointer;"> <i class="fa fa-trash"></i></a></td>
                        </tr>`;
            $("#tbodyExamLab").append(content);
        })
    } else {
        content = `<tr><h5>No se han agregado elementos todavia</h5></tr>`;
        $("#tbodyExamLab").append(content);
    }

}
function printTableExamenImagenologia() {

    $("#tbodyExamImg").empty();
    let content;
    if (arrayExamenesImagenologia.length > 0) {
        arrayExamenesImagenologia.map((e) => {
            content = `<tr id="trMedicamentosModal_" class='text-center'>
                        <td>${e.Descripcion}</td>
                        <td>
                            <a class="aQuitarExamen"
                                onclick='QuitarCarteraServicio(${e.IdCarteraServicio}, 60, this)' 
                                data-id-cartera="${e.IdCarteraServicio}" 
                                data-id-tipo-cartera="60"
                                data-name="${e.Descripcion}" style="color:red; cursor:pointer;"> <i class="fa fa-trash"></i></a></td>
                        </tr>`;
            $("#tbodyExamImg").append(content);
        })
    } else {
        content = `<tr><h5>No se han agregado elementos todavia</h5></tr>`;
        $("#tbodyExamImg").append(content);
    }

}
//Imprime la tabla procedimientos
function printTableProcedimientos() {
    $("#tbodyProd").empty();
    let content;
    if (arrayProcedimientosSeleccionados.length > 0) {
        arrayProcedimientosSeleccionados.map((e) => {
            content = `<tr id="trMedicamentosModal_" class='text-center'>
                        <td>${e.Descripcion}</td>
                        <td><a class="aQuitarExamen" onclick='QuitarCarteraServicio(${e.IdCarteraArancel}, 158, this)' data-id-cartera="${e.IdCarteraServicio}" data-id-tipo-cartera="61" data-name="${e.Descripcion}" style="color:red; cursor:pointer;"> <i class="fa fa-trash"></i></a></td>
                        </tr>`;
            $("#tbodyProd").append(content);
        })
    } else {
        content = `<tr><h5>No se han agregado elementos todavia</h5></tr>`;
        $("#tbodyProd").append(content);
    }

}


//función dibuja los procedimiento
function SetHtmlCarteraArancel(arrayCarteraArancel, divCarteraArancelModal, tipoCartera) {
    let tipoGrupo = "";
    let itemsSinCategoria = [];
    //Inicializar input filtro para los procedimientos.
    let element = "#thProcedimientos";
    let emptytempl = "No existe el procedimiento especificado ({{query}})";
    let templ = `<span>
                    <span class="{{IdCarteraServicio}}">{{DescripcionCarteraServicio}}</span>
                </span>`;
    let source = "GEN_cartera";
    InicializarTypeAhead("#thProcedimientos", "#divProd", "#addElementTableProced", arrayCarteraArancel, emptytempl, templ, source, TIPO_CARTERA.Procedimiento)
    
    //Ordenar arreglo
    if (tipoCartera === TIPO_CARTERA.ExamenLaboratorio) {
        arrayCarteraArancel =
            arrayCarteraArancel.sort(function (a, b) {
                return a.IdGrupoCartera - b.IdGrupoCartera;
            });
    }
    
    $.each(arrayCarteraArancel, function (index, item) {
        
        //Id del boton
        const idBtn = `${divCarteraArancelModal}_btnCarteraArancelModal_${index}`;
        let html = "";
        if (item.DescripcionGrupoCartera != null && tipoGrupo != item.DescripcionGrupoCartera) {
            html = `<div class='col-md-12'><h4 class='text-center'>${item.DescripcionGrupoCartera}</h4></div>`;
        }

        html += `
            <div class="col-md-4 filtro" data-id-cartera="${item.IdCarteraServicio}">
                <div class="row">
                    <h5 class=" p-1  m-2" style="cursor:pointer !important; width: 100%;">
                        <button id="${idBtn}"
                            class="btn btn-outline-secondary text-left"
                            type='button'
                            data-id="${item.IdCarteraServicio}"
                            data-id-cartera="${item.Aranceles.length === 1 ? item.Aranceles[0].IdCarteraServicioArancel : null}"
                            data-text-servicio="${item.DescripcionCarteraServicio}"
                            data-count-aranceles="${item.Aranceles.length}"
                            ${item.Aranceles.length == 1 ? "data-subcat=0": "data-subcat=1"}
                            data-id-tipo-cartera="${tipoCartera}">
                        </button>
                    </h5>
                </div>
            </div>`;

        $(`#${divCarteraArancelModal}`).append(html);

        $(`#${idBtn}`).html((item.Aranceles.length == 1) ? item.DescripcionCarteraServicio : `<strong><i class="fas fa-list"></i> ${item.DescripcionCarteraServicio}</strong>`);

        tipoGrupo = item.DescripcionGrupoCartera;
        //Click function para cada boton
        $(`#${idBtn}`).unbind().on('click', function () {
            
            if (item.Aranceles.length == 1) {
                //cuando solo hay una opcion de seleccion
                modificarCarteraArancel(this);
            } else {
                //Muestra las demás subopciones para seleccionar.
                //Dibuja los sub items con codigo fonasa para seleccionar.
                $("#mdlDatosAdicionales .modal-body, #mdlDatosAdicionales .modal-footer").empty()
                $("#mdlDatosAdicionales .modal-body").html(`Seleccione entre estas diferentes opciones:<br /><br />`);
                item.Aranceles.forEach((item, index) => {
                    let html = `
                        <button
                            id="btnCodigoFonasa_${index}"
                            type="button"
                            class="btn btn-outline-secondary text-left m-2"
                            data-id="${item.IdCarteraServicioArancel}"
                            data-id-arancel="${item.Id}"
                            data-id-cartera="${item.IdCarteraServicioArancel}"
                            data-text-servicio="${$(this).data("text-servicio")}"
                            data-id-btn-servicio="#${$(this).attr("id")}"
                            data-id-tipo-cartera="${tipoCartera}">
                            ${item.Valor}
                        </button><br />`;

                    $("#mdlDatosAdicionales .modal-body").append(html);
                    $(`#btnCodigoFonasa_${index}`).unbind().click(function () {
                        let nombrePadre = "";
                        let nombreHijo = "";
                        $(this).data("text-servicio", " ");
                        const btnParent = $(`${$(this).data("id-btn-servicio")}`);
                        nombrePadre = $(btnParent[0]).data("text-servicio");
                        nombreHijo = $(this).text().trim();
                        btnParent.data("id", $(this).data("id"));
                        //btnParent.data("text-servicio", nombrePadre + "|" + nombreHijo);
                        $(this).data("text-servicio", nombrePadre + "|" + nombreHijo)
                        modificarCarteraArancel($(this)[0]);
                        cerrarModalSobrePuesto($(this))
                    });
                });

                ShowModalSobrepuesto($(this));

            }
        });

    });

    let a = "";
    if (tipoCartera == TIPO_CARTERA.Procedimiento)
        a = "#aAceptarProcedimientosSeleccionados";
    else if (tipoCartera == TIPO_CARTERA.ExamenLaboratorio)
        a = "#aAceptarExamenesLaboratorioModal";

    $(a).unbind().click(function () {
        AgregarObservacionesCarteraArancel(tipoCartera);
    });

}
function ShowModalSobrepuesto(element) {
    $("#mdlProcedimientos").modal("hide");
    $("#mdlDatosAdicionales").modal("show");
}
function cerrarModalSobrePuesto(element) {
    //let elementoPadre = $(element).parent().parent().parent().parent();
    //$(elementoPadre).modal("hide")
    $("#mdlDatosAdicionales").modal("hide");
    $("#mdlProcedimientos").modal("show");
}
//Funcion que agrega o elimina elementos al arreglo de examenes o procedimientos
function modificarCarteraArancel(sender) {
    
    let array = [];
    //*-*-*-*cuando hace click en un item y Tiene sub items(solo en procedimientos)*-*-*-*//
    if ($(sender).data("subcat") == 1) {
        
    } else {
        //*-*-*-*No tiene sub item( solo en procedimientos)*-*-*-*//
        //comprueba si el sender viene de la función eliminar
        let accionEliminar = false
        accionEliminar = $(sender).data("checked");

        const IdCarteraArancel = $(sender).data("id-cartera");
        //obtener id de Examen laboratorio o procedimiento
        const idTipoCartera = $(sender).data("id-tipo-cartera");
        
        //comprobar examen lab o proced
        if (idTipoCartera === TIPO_CARTERA.Procedimiento) {
            array = arrayProcedimientosSeleccionados;
        }
        else if (idTipoCartera === TIPO_CARTERA.ExamenLaboratorio) {
            array = arrayExamenesLaboratorio;
        } else {
            array = arrayExamenesImagenologia;
        }
        //Viene el elemento para función eliminar
        if (accionEliminar == true) {
            checked = false;
        } else {
            //Viene el elemento para ser agregado
            checked = true;
        }
        //agregar el elemento al array
        if (checked) {
            //Si hay elementos busca coincidencias, para no agregarlo si existe.
            let coincide = false;
            let resultado = array.filter(i => i.Descripcion == $(sender).data("text-servicio"))
            //array.map((e) => {
            //    if (e.Descripcion == $(sender).data("text-servicio")) {
            //        coincide = true;
            //    }
            //})
            resultado.length > 0 ? coincide = true : coincide=false

            if (!coincide) {
                array.push({
                    IdCarteraArancel: IdCarteraArancel,
                    Descripcion: `${$(sender).data("text-servicio").trim()}`,
                    ProfesionalSolicitante: $("#txtProfesionalEvolucionAtencion").val(),
                    ProfesionalRealiza: null,
                    IdTipoEstado: 107,
                    IdCarteraServicio: parseInt($(sender).data("id-cartera")),
                    FechaIngreso: GetFechaActual(),
                    FechaRealizacion: null,
                    ObservacionesSolicitud: null,
                    ObservacionesCierre: null,
                    AccionRealizada: null,
                    IdProfesionSolicitante: valCampo($("#sltProfesionEvolucionAtencion").val()),
                    Acciones: {
                        Cerrar: true,
                        Quitar: true,
                        Ver: false,
                        EditarObservacionesIngreso: true,
                        Editar: false
                    }
                });
                toastr.success('Agregado a la lista');
            } else {
                toastr.warning('Elemento ya existe en la lista');
            }
        } else {
            //**************************//
            //    ELIMINAR ELEMENTO    //
            //************************//
            let indice;
            //Examenes laboratorio
            if (idTipoCartera == TIPO_CARTERA.ExamenLaboratorio) {
                //Buscar elementos
                arrayExamenesLaboratorio.find((a, index) => {
                    if (a.IdCarteraArancel == IdCarteraArancel)
                        indice = index;
                })
                //Elimina elementos
                arrayExamenesLaboratorio.splice(indice, 1);
                //Dibuja tablas
                dibujarExamenesLaboratorioSeleccionados();
                printTableExamen();
            }
            //Procedimientos
            if (idTipoCartera == TIPO_CARTERA.Procedimiento) {

                arrayProcedimientosSeleccionados.find((a, index) => {
                    if (a.IdCarteraArancel == IdCarteraArancel)
                        indice = index;
                })

                //Eliminar elementos
                arrayProcedimientosSeleccionados.splice(indice, 1);
                //Dibuja tabla
                dibujarProcedimientosSeleccionados();
                printTableProcedimientos()
            }
            if (idTipoCartera == TIPO_CARTERA.Imagenologia) {
                arrayExamenesImagenologia.find((a, index) => {
                    if (a.IdCarteraArancel == IdCarteraArancel)
                        indice = index;
                })
                //Eliminar elementos
                arrayExamenesImagenologia.splice(indice, 1);
                //Dibuja tabla
                dibujarExamenesImageonologiaSeleccionados()
                printTableExamenImagenologia()
            }
            //Imagenologia
            toastr.error('Elemento eliminado');
        }

        //Dibujar la tabla dependiendo si es un examen de laboratorio o un procedimiento

        if (idTipoCartera === TIPO_CARTERA.Procedimiento) {
            arrayProcedimientosSeleccionados = array;
            dibujarProcedimientosSeleccionados();
            printTableProcedimientos()
        } else if (idTipoCartera === TIPO_CARTERA.ExamenLaboratorio) {
            arrayExamenesLaboratorio = array;
            dibujarExamenesLaboratorioSeleccionados();
            printTableExamen();
        } else if (idTipoCartera === TIPO_CARTERA.Imagenologia) {
            arrayExamenesImagenologia = array
            printTableExamenImagenologia()
            dibujarExamenesImageonologiaSeleccionados()
        }
    }
    //*-*-*-*Fin comprobación si tiene sub items*-*-*-*//
    //console.log(array)
}

//Modifica el color de un boton dependiendo si esta seleccionado. FUNCION OBSOLETA
function checkedCarteraArancel(sender) {

    const checked = $(sender).hasClass("btn-outline-secondary");
    (checked) ? $(sender).removeClass("btn-outline-secondary").addClass("btn-secondary") :
        $(sender).removeClass("btn-secondary").addClass("btn-outline-secondary");

}
function getArrayCartera(tipoCartera) {
    if (tipoCartera == TIPO_CARTERA.Procedimiento)
        return arrayProcedimientosSeleccionados;
    else if (tipoCartera == TIPO_CARTERA.ExamenLaboratorio)
        return arrayExamenesLaboratorio;
    else
        return arrayExamenesImagenologia
}
function SetTablaCarteraArancel(tipoCartera) {
    
    let tblCarteraArancel;
    if (tipoCartera == TIPO_CARTERA.Procedimiento) {
        tblCarteraArancel = "tblProcedimientos";
    } else if (tipoCartera == TIPO_CARTERA.ExamenLaboratorio) {
        
        tblCarteraArancel = "tblExamenesLaboratorio";
    }else {
        tblCarteraArancel = "tblExamenesImagenologia";
    }

    $(`#${tblCarteraArancel} tbody`).empty();
    let casosRealizados = 0;
    let array = getArrayCartera(tipoCartera);
    


    array.forEach((item, index) => {
        
        const aProcedimientoRealizado = `<a id="a${tblCarteraArancel}_RealizaProcedimiento_${index}" data-id="${item.IdCarteraArancel}" class='btn btn-success'
                                            onclick='showCerrarCarteraServicio(${item.IdCarteraArancel}, ${tipoCartera}, "accion-realizada")' data-toggle='tooltip'
                                            data-placement='bottom' title='Acción realizada'><i class="fas fa-check"></i></a>`;

        const aProcedimientoNoRealizado = `<a id="a${tblCarteraArancel}_NoRealizaProcedimiento_${index}" data-id="${item.IdCarteraArancel}" class='btn btn-warning'
                                            onclick='showCerrarCarteraServicio(${item.IdCarteraArancel}, ${tipoCartera}, "accion-no-realizada")' data-toggle='tooltip'
                                            data-placement='bottom' title='Acción No realizada'><i class="fas fa-times"></i></a>`;

        const aQuitarCarteraServicio = `<a id="a${tblCarteraArancel}_QuitarCarteraServicio_${index}" data-id-cartera="${item.IdCarteraArancel}" class='btn btn-danger'
                                            onclick='QuitarCarteraServicio(${item.IdCarteraArancel}, ${tipoCartera}, this)'data-toggle='tooltip'
                                            data-placement='bottom' title='Quitar'><i class='fa fa-trash'></i></a>`;

        const aEditarObservaciones = `<a id="a${tblCarteraArancel}_EditarObservaciones_${index}" class='btn btn-info' data-id="${item.IdCarteraArancel}"
                                        onclick='showCerrarCarteraServicio(${item.IdCarteraArancel}, ${tipoCartera}, "editar")'data-toggle='tooltip'
                                        data-placement='bottom' title='Editar Observaciones'><i class="fas fa-pencil-alt"></i></a>`;

        const aVerObservaciones = `<a id="a${tblCarteraArancel}_VerObservaciones_${index}" class='btn btn-info' data-id="${item.IdCarteraArancel}"
                                        onclick='VerProcedimiento(${item.IdCarteraArancel}, ${tipoCartera})'data-toggle='tooltip'
                                        data-placement='bottom' title='Ver Observaciones'><i class="fas fa-eye"></i></a>`;

        const txtObservacionesIngreso = `<textarea id='txt${tblCarteraArancel}_ObservacionesIngreso_${index}' class='form-control' rows='3'
                                            placeholder='Escriba observaciones adicionales para este procedimiento'>
                                        </textarea>`;

        // background-color: rgb(223, 240, 216);
        $(`#${tblCarteraArancel} tbody`).append(
            `
                <tr id="tr${tblCarteraArancel}_Procedimiento_${index}">
                    <td>${(index + 1)}</td>
                    <td>${item.Descripcion}</td>
                    <td>
                        ${(item.Acciones.EditarObservacionesIngreso) ? txtObservacionesIngreso :
                            ((item.ObservacionesSolicitud != null) ? item.ObservacionesSolicitud : "No existen observaciones")}
                    </td>
                    <td>${moment(item.FechaIngreso).format('DD-MM-YYYY HH:mm:ss')}</td >
                    <td>${(item.FechaRealizacion != null) ? moment(item.FechaRealizacion).format('DD-MM-YYYY HH:mm:ss') : ""}</td>
                    <td class='text-center'>
                        ${(item.Acciones.Cerrar) ? `${aProcedimientoRealizado} ${aProcedimientoNoRealizado}` : ""}
                        ${(item.Acciones.Quitar) ? aQuitarCarteraServicio : ""}
                        ${(item.Acciones.Editar) ? aEditarObservaciones : ""}
                        ${(item.Acciones.Ver) ? aVerObservaciones : ""}
                    </td>
                </tr>`);

        $(`#tr${tblCarteraArancel}_Procedimiento_${index} a[data-toggle="tooltip"]`).tooltip();
        $(`#tr${tblCarteraArancel}_Procedimiento_${index} textarea`).val("");
        PintarFila(`tr${tblCarteraArancel}_Procedimiento_${index}`, item.AccionRealizada);

        if (item.Acciones.EditarObservacionesIngreso) {
            $(`#txt${tblCarteraArancel}_ObservacionesIngreso_${index}`).on('input', function () {
                item.ObservacionesSolicitud = valCampo($(this).val());
            });
            $(`#txt${tblCarteraArancel}_ObservacionesIngreso_${index}`).val(item.ObservacionesSolicitud);

        }

        if (item.FechaRealizacion != null)
            casosRealizados++;

        if (!item.Acciones.Quitar) {
            if (tipoCartera == TIPO_CARTERA.Procedimiento)
                $(`#divListaProcedimientosModal button[data-id-cartera="${item.IdCarteraServicio}"]`).attr("disabled", "disabled");
            else if (tipoCartera == TIPO_CARTERA.ExamenLaboratorio)
                $(`#divListaExamenesLaboratorioModal button[data-id-cartera="${item.IdCarteraServicio}"]`).attr("disabled", "disabled");
        }

    });

    const casosActivos = $(`#${tblCarteraArancel} tbody tr`).length;

    if (tipoCartera == TIPO_CARTERA.Procedimiento)
        actualizarContador("#aProcedimientos", "#divProcedimientos", casosRealizados, casosActivos, "#spProcedimientos", `#${tblCarteraArancel}`);
    else if (tipoCartera == TIPO_CARTERA.ExamenLaboratorio)
        actualizarContador("#aLaboratorio", "#divExamenesLaboratorio", casosRealizados, casosActivos, "#spLaboratorio", `#${tblCarteraArancel}`);
    else
        actualizarContador("#aImagenologia", "#divExamenesImagenologia", casosRealizados, casosActivos, "#spExamenesImagenologia", `#${tblCarteraArancel}`);
}
function PintarFila(id, AccionRealizada) {
    if (AccionRealizada === "SI")
        $(`#${id}`).css("background-color", "rgb(223, 240, 216)");
    else if (AccionRealizada === "NO")
        $(`#${id}`).css("background-color", "#fff3cd");
}
function AgregarObservacionesCarteraArancel(tipoCartera) {
   
    if (tipoCartera == TIPO_CARTERA.Procedimiento) {
        if (getArrayCartera(tipoCartera).length > 0) {
            $("#mdlDatosAdicionales .modal-body").empty().append("<h5><strong>Agregar observaciones adicionales</strong></h5>");
            $("#mdlDatosAdicionales .modal-footer").empty().append(`<a class='btn btn-success'>Aceptar</a>`);
            let existeCartera = false;
            getArrayCartera(tipoCartera).forEach((item, index) => {
                if (item.Acciones.Quitar) {
                    $("#mdlDatosAdicionales .modal-body").append(
                        `
                        <div class='border-dark mt-1 mb-1 p-2'>
                            <label>${(index + 1)}# Cartera: ${item.Descripcion}</label><br />
                            <label>Observaciones:</label>
                            <textarea id='txt_ObservacionesIngreso_${index}' class='form-control' rows='3'
                                placeholder='Escriba observaciones adicionales para este cartera' maxlength='500'>
                            </textarea>
                        </div>
                    `);
                    $(`#txt_ObservacionesIngreso_${index}`).val(item.ObservacionesSolicitud).unbind().on('input', function () {
                        item.ObservacionesSolicitud = valCampo($(this).val());
                    });

                    existeCartera = true;

                }

            });

            if (existeCartera) {

                $("#mdlDatosAdicionales .modal-footer a").unbind().on('click', function () {
                    
                    $("#mdlProcedimientos, #mdlExamenes, #mdlDatosAdicionales").modal('hide');
                    SetTablaCarteraArancel(tipoCartera);
                });

                $("#mdlDatosAdicionales").modal('show');
                ShowModalSobrepuesto();

            } else {
                $("#mdlDatosAdicionales, #mdlProcedimientos, #mdlExamenes").modal("hide");
            }

        } else {
            $("#mdlDatosAdicionales, #mdlProcedimientos, #mdlExamenes").modal("hide");
        }

    } else if (tipoCartera == TIPO_CARTERA.ExamenLaboratorio) {
        $("#mdlExamenes").modal("hide");
    } else {
        $("#mdlImagenologia").modal("hide");
    }

}
function showCerrarCarteraServicio(id, tipoCartera, tipoAccion) {

    $("#aCerrarCarteraArancel").data("id", id);
    $("#aCerrarCarteraArancel").data("tipo-cartera", tipoCartera);
    $("#aCerrarCarteraArancel").data("accion", tipoAccion);

    getArrayCartera(tipoCartera).forEach((item, index) => {

        if (item.IdCarteraArancel == id) {

            const profesionalRealiza = $("#txtProfesionalEvolucionAtencion").val() + " | " + $("#sltProfesionEvolucionAtencion :selected").text();

            $("#divDetalleCarteraArancel").html(
                `
                    <h5><strong>Detalles de Indicación</strong></h5>
                    <label>Solicitante:</label> <span class="form-control form-control-no-height">${(item.ProfesionalSolicitante == null) ? "" : item.ProfesionalSolicitante}</span>
                    <label>Profesional que cierra:</label>
                    <span class="form-control">${(item.ProfesionalRealiza == null) ? profesionalRealiza : item.ProfesionalRealiza}</span>
                    ${(item.ObservacionesSolicitud == null) ? "" : `<label>Observaciones al ingreso:</label> 
                                                                    <span class="form-control form-control-no-height">${item.ObservacionesSolicitud}</span>`}
                    <label>Cartera:</label> <span class="form-control form-control-no-height">${(item.Descripcion == null) ? "" : item.Descripcion}</span>
                `);

            if (item.AccionRealizada == "NO")
                $("#divDetalleCarteraArancel").append(`<div class='alert alert-warning mt-2 text-center'><i class="fas fa-exclamation-triangle"></i> Indicación no realizada</div>`);
            else if (item.AccionRealizada == "SI")
                $("#divDetalleCarteraArancel").append(`<div class='alert alert-success mt-2 text-center'><i class="fas fa-check"></i> Indicación realizada</div>`);

            $("#txtObservacionesCarteraArancel").val(item.ObservacionesCierre);
            $("#aCerrarCarteraArancel").html((item.FechaCierre != null) ? "<i class='fas fa-pencil-alt'></i> Editar" :
                `<i class="fas fa-save"></i> Guardar`);

        }

    });

    // accion-no-realizada
    // accion-realizada
    // modal-header-warning
    if (tipoAccion == "accion-realizada") {
        $("#mdlCerrarCarteraServicio .modal-header h4").html(`<i class="fas fa-check"></i> Acción Realizada`);
        $("#mdlCerrarCarteraServicio .modal-header").removeClass("modal-header-warning modal-header-info").addClass("modal-header-success");
        $("#txtObservacionesCarteraArancel").attr("data-required", false);
    } else if (tipoAccion == "accion-no-realizada") {
        $("#mdlCerrarCarteraServicio .modal-header h4").html(`<i class="fas fa-times"></i> Acción No Realizada`);
        $("#mdlCerrarCarteraServicio .modal-header").removeClass("modal-header-success modal-header-info").addClass("modal-header-warning");
        $("#txtObservacionesCarteraArancel").attr("data-required", true);
    } else if (tipoAccion == "editar" || tipoAccion == "ver") {
        $("#mdlCerrarCarteraServicio .modal-header h4").html((tipoAccion == "editar") ? `<i class="fas fa-pencil-alt"></i> Editar` : `<i class="fas fa-eye"></i> Información`);
        $("#mdlCerrarCarteraServicio .modal-header").removeClass("modal-header-success modal-header-warning").addClass("modal-header-info");
        $("#txtObservacionesCarteraArancel").attr("data-required", false);
    }

    $("#txtObservacionesCarteraArancel").removeAttr("disabled");
    $("#mdlCerrarCarteraServicio .modal-footer").show();
    $("#mdlCerrarCarteraServicio").modal("show");
    $("#aCerrarCarteraArancel").unbind().on('click', function () { CerrarCarteraServicio(); });
    ReiniciarRequired();

}
function CerrarCarteraServicio() {

    const tipoAccion = $("#aCerrarCarteraArancel").data("accion");
    if (tipoAccion == "accion-no-realizada" && !validarCampos("#divObservacionesCierre", false))
        return;

    const id = $("#aCerrarCarteraArancel").data("id");
    const tipoCartera = $("#aCerrarCarteraArancel").data("tipo-cartera");

    getArrayCartera(tipoCartera).forEach((item, index) => {
        
        if (item.IdCarteraArancel == id) {

            item.FechaRealizacion = (item.FechaRealizacion == null) ? GetFechaActual() : item.FechaRealizacion;
            item.IdProfesionRealiza = valCampo($("#sltProfesionEvolucionAtencion").val());
            item.ProfesionalRealiza = $("#txtProfesionalEvolucionAtencion").val() + " | " + $("#sltProfesionEvolucionAtencion :selected").text();
            item.ObservacionesCierre = valCampo($("#txtObservacionesCarteraArancel").val());
            item.IdTipoEstado = 113;
            item.Acciones.Cerrar = false;
            item.Acciones.Quitar = false;
            item.Acciones.Ver = false;
            item.Acciones.Editar = true;
            item.Acciones.EditarObservacionesIngreso = false;

            if (item.AccionRealizada === null)
                item.AccionRealizada = (tipoAccion == "accion-realizada") ? "SI" : "NO";

        }
    });

    if (tipoCartera == TIPO_CARTERA.Procedimiento)
        dibujarProcedimientosSeleccionados();
    else if (tipoCartera == TIPO_CARTERA.ExamenLaboratorio)
        dibujarExamenesLaboratorioSeleccionados()
    else if (tipoCartera == TIPO_CARTERA.Imagenologia)
        dibujarExamenesImageonologiaSeleccionados()
        

    $("#mdlCerrarCarteraServicio").modal("hide");

}
function QuitarCarteraServicio(id, tipoCartera, elemento) {

    ShowModalAlerta('CONFIRMACION',
        'Quitar Caso',
        '¿Está seguro que quiere quitar el caso?',
        function () {
            //Envio de este data para saber que se está eliminando
            let idTipoCartera = tipoCartera;

            $(elemento).data("id-tipo-cartera", idTipoCartera);
            $(elemento).data("checked", true);
            modificarCarteraArancel(elemento);
            
            
            $('#modalAlerta').modal('hide');

        });

}
function CargarCarteraServicio(tipoCartera) {

    let divModal = "";
    if (tipoCartera == TIPO_CARTERA.Procedimiento)
        divModal = "divListaProcedimientosModal";
        divModal = "divListaExamenesLaboratorioModal";

    getArrayCartera(tipoCartera).forEach(function (cartera, index) {

        const sender = $(`#${divModal} button[data-id-cartera="${cartera.IdCarteraServicio}"]`);
        let descripcion = "";
        $(sender).data("id", cartera.IdCarteraArancel);

        if (parseInt($(sender).data("count-aranceles")) > 1) {
            descripcion = `${cartera.Descripcion} | ${cartera.DescripcionArancel}`;
            $(sender).attr("data-text-servicio", descripcion);
        } else {
            descripcion = cartera.Descripcion;
            $(sender).attr("data-text-servicio", descripcion);
        }

        cartera.Descripcion = descripcion;
        checkedCarteraArancel(sender);

    });

}
function CargarArrayCarteraServicio(tipoCartera) {

    let array = [];

    if (getArrayCartera(tipoCartera).length > 0) {

        getArrayCartera(tipoCartera).forEach((item, index) => {

            let json = {
                IdCarteraArancel: item.IdCarteraArancel,
                IdTipoEstado: item.IdTipoEstado,
                FechaIngreso: moment(item.FechaIngreso).format("YYYY-MM-DD HH:mm:ss"),
                AccionRealizada: item.AccionRealizada,
                IdProfesionSolicitante: item.IdProfesionSolicitante
            }

            if (item.IdAtencionCarteraArancel != undefined) {
                json.IdAtencionCarteraArancel = item.IdAtencionCarteraArancel;
            }

            if (item.FechaRealizacion != undefined) {
                json.FechaRealizacion = moment(item.FechaRealizacion).format("YYYY-MM-DD HH:mm:ss");
                json.IdProfesionRealiza = item.IdProfesionRealiza;
            }

            if (item.ObservacionesSolicitud != undefined)
                json.ObservacionesSolicitud = item.ObservacionesSolicitud;

            if (item.ObservacionesCierre != undefined)
                json.ObservacionesCierre = item.ObservacionesCierre;

            array.push(json);
        });


    } else array = null;

    if (tipoCartera == TIPO_CARTERA.Procedimiento)
        ingresoUrgencia.Procedimientos = array;
    else if (tipoCartera == TIPO_CARTERA.ExamenLaboratorio)
        ingresoUrgencia.ExamenesLaboratorio = array;
    else if (tipoCartera == TIPO_CARTERA.Imagenologia)
        ingresoUrgencia.ExamenesImagenologia = array;

}

// MEDICAMENTOS
function getJsonMedicamentosArsenal() {

    let arsenalMedicamentos = [];

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Medicamento_Arsenal/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            arsenalMedicamentos = data;
        },
        error: function (e) {
            console.log(e);
        }
    });

    return arsenalMedicamentos;

}
function setMedicamentosTypeAhead(data) {
    var typeAhObject = {
        input: "#thMedicamentos",
        minLength: 3,
        maxItem: 12,
        maxItemPerGroup: 10,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el medicamento especificado ({{query}})",
        template:
            `<span>
                <span class="{{Valor}}">{{Valor}} {{Forma}} {{Presentacion}}</span>
            </span>`,
        correlativeTemplate: true,
        source: {
            GEN_Medicamento: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                $("#thMedicamentos").data('id', item.Id);
            }
        }
    };

    return typeAhObject;
}
function modalInsumosMedicamentos() {

    $("#thMedicamentos").data("id", null);
    $("#thMedicamentos, #txtDosisMedicamento, #txtFrecuenciaMedicamento").val("");
    $("#sltViaAdministracion").val("0");
    $("#aGuardarEditarMedicamento").hide();
    $("#aAgregarMedicamento, #aAceptarMedicamento, #tblMedicamentos, #aAceptarMedicamento").show();

    if ($("#tblMedicamentos tbody").html() == "")
        $("#tblMedicamentos, #aAceptarMedicamento").hide();

    $("#mdlMedicamentos").modal('show');

}
function EditarInsumoMedicamento(sender) {

    const index = $(sender).data("index");

    $("#thMedicamentos").data("id", arrayMedicamentosSeleccionados[index].IdMedicamento);
    $("#thMedicamentos").val(arrayMedicamentosSeleccionados[index].DescripcionMedicamento);
    $("#txtDosisMedicamento").val(arrayMedicamentosSeleccionados[index].Dosis)
    $("#txtFrecuenciaMedicamento").val(arrayMedicamentosSeleccionados[index].Frecuencia);
    $("#sltViaAdministracion").val(arrayMedicamentosSeleccionados[index].IdTipoViaAdministracion);
    $("#aGuardarEditarMedicamento").show();
    $("#tblMedicamentos, #aAgregarMedicamento, #aAceptarMedicamento").hide();
    $("#mdlMedicamentos").modal('show');

    $("#aGuardarEditarMedicamento").unbind().click(function (e) {
        modificarMedicamento($(sender));
    });

}
function modificarMedicamento(sender) {

    if ($(sender).data("accion") == "Agregar") {
        if (validarCampos("#divInsumosMedicamentos", false)) {
            arrayMedicamentosSeleccionados.push({
                IdMedicamento: parseInt($("#thMedicamentos").data("id")),
                DescripcionMedicamento: $("#thMedicamentos").val(),
                ProfesionalSolicitante: $("#txtProfesionalEvolucionAtencion").val(),
                ProfesionalRealiza: null,
                IdTipoEstado: 107,
                FechaIngreso: GetFechaActual(),
                FechaCierre: null,
                Observaciones: null,
                IdTipoViaAdministracion: parseInt($("#sltViaAdministracion").val()),
                DescripcionViaAdministracion: $('#sltViaAdministracion option:selected').text(),
                Dosis: $.trim($("#txtDosisMedicamento").val()),
                Frecuencia: $.trim($("#txtFrecuenciaMedicamento").val()),
                AccionRealizada: null,
                IdProfesionSolicitante: valCampo($("#sltProfesionEvolucionAtencion").val()),
                Acciones: {
                    Cerrar: true,
                    Quitar: true,
                    Ver: false,
                    Editar: true,
                    EditarCierre: false
                }
            });
            $("#thMedicamentos, #txtDosisMedicamento, #txtFrecuenciaMedicamento").val("");
            $("#sltViaAdministracion").val("0");
            dibujarHtmlMedicamentos();
        }
    } else if ($(sender).data("accion") == "Editar") {
        if (validarCampos("#divInsumosMedicamentos", false)) {
            const index = parseInt($(sender).data("index"));
            arrayMedicamentosSeleccionados[index].IdMedicamento = parseInt($("#thMedicamentos").data("id"));
            arrayMedicamentosSeleccionados[index].DescripcionMedicamento = $("#thMedicamentos").val();
            arrayMedicamentosSeleccionados[index].Dosis = $("#txtDosisMedicamento").val();
            arrayMedicamentosSeleccionados[index].Frecuencia = $("#txtFrecuenciaMedicamento").val();
            arrayMedicamentosSeleccionados[index].IdTipoViaAdministracion = parseInt($("#sltViaAdministracion").val());
            arrayMedicamentosSeleccionados[index].DescripcionViaAdministracion = $('#sltViaAdministracion option:selected').text();
            $("#mdlMedicamentos").modal('hide');
            dibujarHtmlMedicamentos();
        }
    } else {
        const indexSeleccionado = parseInt($(sender).data("index"));
        arrayMedicamentosSeleccionados.splice(indexSeleccionado, 1);
        dibujarHtmlMedicamentos();
    }

}
function dibujarHtmlMedicamentosModal(item, index, aQuitarMedicamento) {

    $("#tblMedicamentos tbody").append(
        `
            <tr id="trMedicamentosModal_${index}" class='text-center'>
                <td>${item.DescripcionMedicamento}</td>
                <td>${item.Dosis}</td>
                <td>${item.Frecuencia}</td>
                <td>${item.DescripcionViaAdministracion}</td>
                <td>${(item.Acciones.Quitar) ? aQuitarMedicamento : ""}</td>
            </tr>
        `);

}
function dibujarHtmlMedicamentos() {

    $("#tblMedicamentosBox tbody, #tblMedicamentos tbody").empty();
    let casosRealizados = 0;

    arrayMedicamentosSeleccionados.forEach((item, index) => {

        const aMedicamentoSuministrado = `<a id="aMedicamentoSuministrado_${index}" data-index='${index}' data-id="${item.IdMedicamento}" class='btn btn-success'
                                            onclick='showSuministracionMedicamento(this)' data-toggle='tooltip' data-accion='accion-realizada'
                                            data-placement='bottom' title='Medicamento suministrado'><i class="fas fa-check"></i></a>`;

        const aMedicamentoNoSuministrado = `<a id="aMedicamentoNoSuministrado_${index}" data-index='${index}' data-id="${item.IdMedicamento}" class='btn btn-warning'
                                            onclick='showSuministracionMedicamento(this)' data-toggle='tooltip' data-accion='accion-no-realizada'
                                            data-placement='bottom' title='Medicamento No suministrado'><i class="fas fa-times"></i></a>`;

        const aQuitarMedicamento = `<a data-index="${index}" data-id="${item.IdMedicamento}" class='btn btn-danger'
                                        onclick='QuitarMedicamento(this)'data-toggle='tooltip' data-accion="Quitar" data-index='${index}'
                                        data-placement='bottom' title='Quitar'><i class='fa fa-trash'></i></a>`;

        const aEditarMedicamento = `<a id="aEditarMedicamento_${index}" class='btn btn-info' data-id="${item.IdMedicamento}"
                                        onclick='EditarInsumoMedicamento(this)'data-toggle='tooltip' data-index='${index}' data-accion="Editar"
                                        data-placement='bottom' title='Editar Medicamento'><i class="fas fa-pencil-alt"></i></a>`;

        const aEditarCierre = `<a id="aEditarMedicamentoCierre_${index}" class='btn btn-info' data-id="${item.IdMedicamento}"
                                        onclick='showSuministracionMedicamento(this)'data-toggle='tooltip' data-index='${index}' data-accion="Editar"
                                        data-placement='bottom' title='Editar Cierre'><i class="fas fa-pencil-alt"></i></a>`;

        const aVerMedicamento = `<a id="aVerMedicamento_${index}" class='btn btn-info' data-id="${item.IdMedicamento}"
                                        onclick='VerMedicamento(this)'data-toggle='tooltip' data-index='${index}'
                                        data-placement='bottom' title='Ver Observaciones'><i class="fas fa-eye"></i></a>`;

        $("#tblMedicamentosBox tbody").append(`
            <tr id="trMedicamentos_${index}">
                <td>${(index + 1)}</td>
                <td>${item.DescripcionMedicamento}</td>
                <td>
                    <strong>Dosis:</strong> ${item.Dosis}<br />
                    <strong>Frecuencia:</strong> ${item.Frecuencia}<br />
                    <strong>Vía de administración:</strong> ${item.DescripcionViaAdministracion}
                </td>
                <td>${moment(item.FechaIngreso).format('DD-MM-YYYY HH:mm:ss')}</td>
                <td>${(item.FechaCierre != null) ? moment(item.FechaCierre).format('DD-MM-YYYY HH:mm:ss') : ""}</td>
                <td class='text-center'>
                    ${(item.Acciones.Cerrar) ? `${aMedicamentoSuministrado} ${aMedicamentoNoSuministrado}` : ""}
                    ${(item.Acciones.Editar) ? aEditarMedicamento : ""}
                    ${(item.Acciones.EditarCierre) ? aEditarCierre : ""}
                    ${(item.Acciones.Quitar) ? aQuitarMedicamento : ""}
                    ${(item.Acciones.Ver) ? aVerMedicamento : ""}
                </td>
            </tr>
        `);

        dibujarHtmlMedicamentosModal(item, index, aQuitarMedicamento);
        $(`#trMedicamentos_${index} a[data-toggle="tooltip"]`).tooltip();
        PintarFila(`trMedicamentos_${index}`, item.AccionRealizada);

        if (item.FechaCierre != null)
            casosRealizados++;

    });

    const casosActivos = $("#tblMedicamentosBox tbody tr").length;
    actualizarContador("#aMedicamentoBox", "#divMedicamentos", casosRealizados, casosActivos, "#spInsumosMedicamentos", "#tblMedicamentosBox");
    ($("#tblMedicamentosBox tbody").html() == "") ? $("#tblMedicamentos, #aAceptarMedicamento").hide() :
        $("#tblMedicamentos, #aAceptarMedicamento").show();

}
function QuitarMedicamento(sender) {
    ShowModalAlerta('CONFIRMACION',
        'Quitar Medicamento',
        '¿Está seguro que quiere quitar el Medicamento o insumo?',
        function () {
            modificarMedicamento(sender);
            $('#modalAlerta').modal('hide');
        });
}
function VerMedicamento(sender) {
    showSuministracionMedicamento(sender);
    $("#txtObservacionesCarteraArancel").attr("disabled", "disabled");
    $("#mdlCerrarCarteraServicio .modal-footer").hide();
}
function showSuministracionMedicamento(sender) {

    const index = parseInt($(sender).data("index"));
    const item = arrayMedicamentosSeleccionados[index];
    const accion = $(sender).data("accion");
    const profesionalRealiza = $("#txtProfesionalEvolucionAtencion").val() + " | " + $("#sltProfesionEvolucionAtencion :selected").text();

    $("#aCerrarCarteraArancel").data("index", index);
    $("#aCerrarCarteraArancel").data("accion", accion);

    $("#divDetalleCarteraArancel").html(
        `
            <h5><strong>Detalles Medicamento</strong></h5>
            <label>Solicitante:</label>
            <span class="form-control form-control-no-height">${(item.ProfesionalSolicitante == null) ? "" : item.ProfesionalSolicitante}</span>
            <label>Profesinal que cierra:</label>
            <span class="form-control">${(item.ProfesionalRealiza == null) ? profesionalRealiza : item.ProfesionalRealiza}</span>
            <label>Medicamento:</label> <span class="form-control form-control-no-height">${item.DescripcionMedicamento}</span>
            <label>Descripción:</label> <span class="form-control form-control-no-height">` +
                                                            `<strong>Dosis:</strong> ${item.Dosis}
                                                              <strong>Frecuencia:</strong> ${item.Frecuencia}
                                                              <strong>Vía de administración:</strong> ${item.DescripcionViaAdministracion}</span>`);

    $("#txtObservacionesCarteraArancel").val(item.Observaciones);
    $("#aCerrarCarteraArancel").html((item.FechaCierre != null) ? "<i class='fas fa-pencil-alt'></i> Editar" :
        `<i class="fas fa-save"></i> Guardar`);

    $("#txtObservacionesCarteraArancel").attr("data-required", (accion == "accion-no-realizada"));
    $("#txtObservacionesCarteraArancel").removeAttr("disabled");
    $("#mdlCerrarCarteraServicio .modal-header h4").html((accion == "accion-realizada") ? "Confirmar Acción" : "Editar");
    $("#mdlCerrarCarteraServicio .modal-footer").show();
    $("#mdlCerrarCarteraServicio").modal("show");
    $("#aCerrarCarteraArancel").unbind().on('click', function () { cerrarMedicamento(); });
    ReiniciarRequired();

}
function cerrarMedicamento() {

    const tipoAccion = $("#aCerrarCarteraArancel").data("accion");
    if (tipoAccion == "accion-no-realizada" && !validarCampos("#divObservacionesCierre", false))
        return;

    const index = parseInt($("#aCerrarCarteraArancel").data("index"));
    arrayMedicamentosSeleccionados[index].FechaCierre = (arrayMedicamentosSeleccionados[index].FechaCierre == null) ?
        GetFechaActual() :
        arrayMedicamentosSeleccionados[index].FechaCierre;
    arrayMedicamentosSeleccionados[index].IdProfesionRealiza = valCampo($("#sltProfesionEvolucionAtencion").val());
    arrayMedicamentosSeleccionados[index].ProfesionalRealiza = $("#txtProfesionalEvolucionAtencion").val() + " | " + $("#sltProfesionEvolucionAtencion :selected").text();
    arrayMedicamentosSeleccionados[index].Observaciones = valCampo($("#txtObservacionesCarteraArancel").val());
    arrayMedicamentosSeleccionados[index].IdTipoEstado = 113;
    arrayMedicamentosSeleccionados[index].Acciones.Cerrar = false;
    arrayMedicamentosSeleccionados[index].Acciones.Quitar = false;
    arrayMedicamentosSeleccionados[index].Acciones.Ver = false;
    arrayMedicamentosSeleccionados[index].Acciones.Editar = false;
    arrayMedicamentosSeleccionados[index].Acciones.EditarCierre = true;
    if (arrayMedicamentosSeleccionados[index].AccionRealizada === null)
        arrayMedicamentosSeleccionados[index].AccionRealizada = (tipoAccion == "accion-realizada") ? "SI" : "NO";

    dibujarHtmlMedicamentos();
    $("#mdlCerrarCarteraServicio").modal("hide");

}
function CargarArrayMedicamentos() {

    if (arrayMedicamentosSeleccionados.length > 0) {

        ingresoUrgencia.Medicamentos = [];
        arrayMedicamentosSeleccionados.forEach((item, index) => {

            let json = {
                IdMedicamento: item.IdMedicamento,
                IdTipoEstado: item.IdTipoEstado,
                FechaIngreso: moment(item.FechaIngreso).format("YYYY-MM-DD HH:mm:ss"),
                Dosis: item.Dosis,
                Frecuencia: item.Frecuencia,
                IdTipoViaAdministracion: item.IdTipoViaAdministracion,
                AccionRealizada: item.AccionRealizada,
                IdProfesionSolicitante: item.IdProfesionSolicitante
            }

            if (item.IdAtencionMedicamento != undefined) {
                json.IdAtencionMedicamento = item.IdAtencionMedicamento;
            }

            if (item.FechaCierre != null) {
                json.FechaCierre = moment(item.FechaCierre).format("YYYY-MM-DD HH:mm:ss");
                json.IdProfesionRealiza = item.IdProfesionRealiza;
            }

            if (item.Observaciones != null)
                json.Observaciones = item.Observaciones;

            ingresoUrgencia.Medicamentos.push(json);

        });

    } else ingresoUrgencia.Medicamentos = null;

}

// INTERCONSULTOR
function agregarSolicitudInterconsultor() {

    let id = $("#aGuardarInterconsultor").data("id");

    if (validarCampos("#mdlInterconsultista", false)) {

        if (id == 0) {

            let json = {
                "Id": null,
                "Fecha": GetFechaActual(),
                "IdUsuarioSolicita": parseInt(sSession.ID_USUARIO),
                "LoginUsuarioSolicita": sSession.LOGIN_USUARIO.toUpperCase(),
                "IdUsuarioRealizada": null,
                "LoginUsuarioRealiza": null,
                "AtencionConsultorCerrada": "NO",
                "IdTipoConsultor": $('#sltTipoInterconsultor').val(),
                "DescripcionTipoConsultor": $('#sltTipoInterconsultor option:selected').text(),
                "ActividadSolicitada": $('#txtDetalleSolicitudInterconsultor').val(),
                "ActividadRealizada": $('#txtDetalleAtencionInterconsultor').val() != "" ? $.trim($('#txtDetalleAtencionInterconsultor').val()) : null,
                "FechaCierre": null,
                Acciones: {
                    CerrarAtender: true,
                    VerDetalle: true,
                    Quitar: true
                }
            };

            arrayInterconsultoresSeleccionados.push(json);

        } else {

            for (var i = 0; i < arrayInterconsultoresSeleccionados.length; i++) {
                if (arrayInterconsultoresSeleccionados[i].Id == id) {
                    arrayInterconsultoresSeleccionados[i].IdUsuarioRealizada = parseInt(sSession.ID_USUARIO);
                    arrayInterconsultoresSeleccionados[i].LoginUsuarioRealiza = sSession.LOGIN_USUARIO.toUpperCase();
                    arrayInterconsultoresSeleccionados[i].ActividadRealizada = $.trim($('#txtDetalleAtencionInterconsultor').val());
                }
            }

        }

        dibujarHtmlInterconsultor();
        $("#aGuardarInterconsultor").data("id", "0");
        $('#mdlInterconsultista').modal('hide');

    }
}
function eliminarFilaInterconsultor(fill) {

    ShowModalAlerta('CONFIRMACION',
        'Eliminar Interconsultor',
        '¿Está seguro que desea eliminar esta solicitud?',
        () => {
            arrayInterconsultoresSeleccionados.splice(fill, 1);
            dibujarHtmlInterconsultor();
            $('#modalAlerta').modal("hide");
        }
    );
    

}
function cerrarInterconsultor(id) {
    ShowModalAlerta('CONFIRMACION',
        'Cerrar Interconsultor',
        '¿Está seguro que desea cerrar esta solicitud?',
        () => {
            for (var i = 0; i < arrayInterconsultoresSeleccionados.length; i++) {
                if (arrayInterconsultoresSeleccionados[i].Id == id) {
                    arrayInterconsultoresSeleccionados[i].AtencionConsultorCerrada = "SI";
                    arrayInterconsultoresSeleccionados[i].FechaCierre = GetFechaActual();
                    arrayInterconsultoresSeleccionados[i].Acciones =
                    {
                        CerrarAtender: false,
                        VerDetalle: true,
                        Quitar: false
                    }
                }
            }
            dibujarHtmlInterconsultor();
            $('#modalAlerta').modal("hide");
        }
    );
}
function atenderInterconsultor(id, fecha, accion) {

    CargarInterconsultistas();

    $("#sltTipoInterconsultor").attr("disabled", "disabled");
    $("#txtDetalleSolicitudInterconsultor").attr("disabled", "disabled");

    // let guardarInterconsultor = document.getElementById("aGuardarInterconsultor");
    if (accion == "ver") {

        $("#txtDetalleAtencionInterconsultor").attr("disabled", "disabled");
        $("#txtDetalleAtencionInterconsultor").val("");
        $("#aGuardarInterconsultor").hide();

    } else if (accion == "atender") {

        $("#txtDetalleAtencionInterconsultor").removeAttr("disabled").attr("data-required", "true");
        $("#aGuardarInterconsultor").data("id", id);
        $("#aGuardarInterconsultor").show();

    }

    $("#divDetalleAtencionInterconsultor").show();

    if (id != null) {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + 'URG_Atenciones_Urgencia/Interconsultor/' + id,
            async: true,
            success: function (data) {
                $('#sltTipoInterconsultor').val(data[0].IdTipoConsultor);
                $('#txtDetalleSolicitudInterconsultor').val(data[0].ActividadSolicitada);
                for (var i = 0; i < arrayInterconsultoresSeleccionados.length; i++) {
                    if (arrayInterconsultoresSeleccionados[i].Id == id) {
                        $('#txtDetalleAtencionInterconsultor').val(arrayInterconsultoresSeleccionados[i].ActividadRealizada);
                    }
                }
            }
        });

    } else {

        for (var i = 0; i < arrayInterconsultoresSeleccionados.length; i++) {
            if (arrayInterconsultoresSeleccionados[i].Id == id && fecha == moment(arrayInterconsultoresSeleccionados[i].Fecha).format('DD-MM-YYYY HH:mm:ss')) {
                $('#sltTipoInterconsultor').val(arrayInterconsultoresSeleccionados[i].IdTipoConsultor);
                $('#txtDetalleSolicitudInterconsultor').val(arrayInterconsultoresSeleccionados[i].ActividadSolicitada);
            }
        }

    }

}
function dibujarHtmlInterconsultor() {

    $('#tblInterconsultores tbody').empty();
    let casosActivos = arrayInterconsultoresSeleccionados.length, casosRealizados = 0;
    $.each(arrayInterconsultoresSeleccionados, function (i, r) {

        const btnAtender = `
                            <a id="aAtenderInterconsultor_${i}" onclick="atenderInterconsultor(${r.Id},${null},'atender')" class="btn btn-success">
                                Atender
                            </a>`;
        const btnBorrar = `
                            <a id="aQuitarInterconsultor_${i}" onclick="eliminarFilaInterconsultor(${i})" class="btn btn-danger" data-toggle='tooltip'
                                data-placement='bottom' title='Quitar'>
                                <i class="fa fa-trash"></i>
                            </a>`;
        const btnDetalle = `
                            <a id="aDetalleInterconsultor_${i}" onclick="atenderInterconsultor(${r.Id}, '${moment(r.Fecha).format('DD-MM-YYYY HH:mm:ss')}', 'ver')"
                                class="btn btn-info" data-toggle='tooltip' data-placement='bottom' title='Ver Detalles'>
                                <i class="far fa-eye"></i>
                            </a>`;
        const btnCerrar = `
                            <a id="aCerrarInterconsultor_${i}" onclick="cerrarInterconsultor(${r.Id})" class="btn btn-warning">
                                Cerrar
                            </a>`;

        const nuevaFila = `
                <tr id="trInterconsultor_${i}">
                    <td>${i + 1}</td>                                            
                    <td>${r.DescripcionTipoConsultor}</td>                                            
                    <td>${moment(r.Fecha).format('DD-MM-YYYY HH:mm:ss')}</td>
                    <td>${r.LoginUsuarioSolicita}</td>
                    <td>${(r.FechaCierre != null) ? moment(r.FechaCierre).format('DD-MM-YYYY HH:mm:ss') : ""}</td>
                    <td>${r.LoginUsuarioRealiza ?? ""}</td>
                    <td class='text-center'>
                        ${r.Acciones.CerrarAtender ? btnAtender + " " + btnCerrar : ""}
                        ${r.Acciones.VerDetalle ? btnDetalle : ""}
                        ${r.Acciones.Quitar ? btnBorrar + " " : ""}
                    </td>
                </tr>`;

        if (r.AtencionConsultorCerrada == "SI")
            casosRealizados++;

        $('#tblInterconsultores tbody').append(nuevaFila);
        PintarFila(`trInterconsultor_${i}`, (r.AtencionConsultorCerrada == "NO") ? null : "SI");
        $(`#trInterconsultor_${i} a[data-toggle="tooltip"]`).tooltip();

    });

    actualizarContador("#aInterconsultor", "#divSolicitudInterconsultor", casosRealizados, casosActivos, "#spInterconsultor", "#tblInterconsultores");

}
function actualizarContador(idBtn, idDiv, casosRealizados, casosActivos, spContador, tbl) {

    $(`${spContador}`).text(casosRealizados + "/" + casosActivos);
    $(`${idDiv}`).hide();

    if ($(`${idBtn} .badge`).html().indexOf("/") > -1) {

        let interconsultasRealizadas = $(`${idBtn} .badge`).html().split("/")[0];
        let interconsultasPorRealizar = $(`${idBtn} .badge`).html().split("/")[1];

        if (interconsultasPorRealizar == 0)
            $(`${idBtn}`).removeClass("success warning").addClass("default");
        else if (interconsultasPorRealizar == interconsultasRealizadas) {
            $(`${idDiv}`).show();
            $(`${idBtn}`).removeClass("warning default").addClass("success");
        } else {
            $(`${idDiv}`).show();
            $(`${idBtn}`).removeClass("success default").addClass("warning");
        }

    }

    ($(`${tbl} tbody`).html() == "") ? $(idDiv).hide() : $(idDiv).show();

}
function CargarInterconsultistas() {

    let url = `${GetWebApiUrl()}URG_Tipo_Consultor/combo`;
    setCargarDataEnCombo(url, false, $("#sltTipoInterconsultor"));

    $('#txtDetalleSolicitudInterconsultor').val('');
    $("#sltTipoInterconsultor").removeAttr("disabled");
    $("#txtDetalleSolicitudInterconsultor").removeAttr("disabled");
    $("#divDetalleAtencionInterconsultor").hide();
    $("#aGuardarInterconsultor").show();
    $('#mdlInterconsultista').modal('show');

}
function CargarArrayInterconsultistas() {

    if (arrayInterconsultoresSeleccionados.length > 0) {

        ingresoUrgencia.InterconsultorAtencion = [];
        arrayInterconsultoresSeleccionados.forEach((item, index) => {

            ingresoUrgencia.InterconsultorAtencion.push(
                {
                    FechaAtencion: moment(item.Fecha).format("YYYY-MM-DD HH:mm:ss"),
                    IdTipoConsultor: item.IdTipoConsultor,
                    ActividadSolicitada: item.ActividadSolicitada,
                    IdUsuarioRealizada: item.IdUsuarioRealizada,
                    ActividadRealizada: item.ActividadRealizada,
                    Cerrada: item.AtencionConsultorCerrada,
                    FechaCierre: (item.FechaCierre == null) ? null : moment(item.FechaCierre).format("YYYY-MM-DD HH:mm:ss")
                }
            );

        });

    } else ingresoUrgencia.InterconsultorAtencion = null;

}