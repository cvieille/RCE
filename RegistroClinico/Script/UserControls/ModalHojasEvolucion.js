﻿
function GetTablaHojaEvualuacion(idPaciente, idEvento) {
    console.log(idPaciente)
    console.log(idEvento)
    var adataset = [];
    var sBaseURL = GetWebApiUrl() + "RCE_Hoja_Evolucion/DetalledePaciente?GEN_idPaciente=" + idPaciente + "&RCE_idEventos=" + idEvento;

    $.ajax({
        type: "GET",
        url: sBaseURL,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            console.log(data)
            $.each(data, function (key, val) {
                adataset.push([
                    val.RCE_idHoja_Evolucion,
                    val.GEN_idTipo_Estados_Sistemas,
                    moment(moment(val.RCE_fechaHoja_Evolucion).toDate()).format('DD-MM-YYYY'),
                    val.RCE_diagnostico_principalHoja_Evolucion,
                    val.GEN_nombrePersonasProfesional
                ]);
            });

            LlenarGrillaHojasEvolucion(adataset, '#tblHojasEvolucion', sSession);
        }
    });

    $('body').on('click', '#btnVolverCarousel', function(e){
        e.preventDefault();
    });
}

function LlenarGrillaHojasEvolucion(datos, grilla, sSession) {
    
    $(grilla).DataTable({
        data: datos,
        columns: [
            { title: "id_HojaEvolucion" },
            { title: "GEN_idTipo_Estados_Sistemas" },
            { title: "Fecha H.E." },
            { title: "Diagnóstico" },
            { title: "Profesional" },
            { title: "" }
        ],
        lengthMenu: [[5, 10], [5, 10]],
        columnDefs: [
            {
                targets: 2,
                sType: "date-ukLong"
            },
            {
                targets: -1,
                data: null,
                orderable: false,
                searchable: false,
                className: 'w-1',
                render: function (data, type, row, meta)
                {
                    var fila = meta.row;
                    var botones;

                    botones = `<div class="btn-group" role="group" aria-label="Basic example">`

                    if (sSession.CODIGO_PERFIL != 5)
                        botones += `    <button data-id='${datos[fila][0]}' type='button' class='btn btn-success load-click' onclick='linkEditarHojaEvolucion(this);'
                                            data-toggle='tooltip' data-placement='top' title='Editar H.E.'>
                                            <i class='fa fa-edit'></i>
                                        </button>`;
                    else
                        botones += `    <button id='linkEditarHojaEvolucionD' data-id='${datos[fila][0]}' type='button' class='btn btn-blue-grey disabled'
                                            data-toggle='tooltip' data-placement='top' title='Editar H.E.'>
                                            <i class='fa fa-edit'></i>
                                        </button>`;

                    if (datos[fila][1] == 81)
                        botones += `    <button data-id='${datos[fila][0] }' type='button' class='btn btn-info load-click' data-print='true' data-frame='#frameHojaEvolucion'
                                            data-toggle='tooltip' data-placement='top' title='Imprimir H.E.' onclick='linkImprimirHojasEvolucion(this);'>
                                            <i class='fa fa-print'></i>
                                        </button>`;
                    else
                        botones += `    <button data-id='${datos[fila][0]}' type='button' class='btn btn-blue-grey disabled' data-toggle='tooltip' data-placement='top' 
                                            title='Imprimir H.E.'>
                                            <i class='fa fa-print'></i>
                                        </button>`;

                    if (datos[fila][1] == 82 && sSession.CODIGO_PERFIL == 1)
                        botones += `    <button id='linkValidarHojaEvolucion' data-id='${datos[fila][0]}' type='button' class='btn btn-success load-click' 
                                            onclick='linkValidarHojaEvolucion(this);' data-toggle='tooltip' data-placement='top' title='Validar H.E.'>
                                            <i class='fa fa-check'></i>
                                        </button>`;
                    else
                        botones += `    <button id='linkValidarHojaEvolucionD' data-id='${datos[fila][0]}' type='button' class='btn btn-blue-grey disabled'
                                            data-toggle='tooltip' data-placement='top' title='Validar H.E.'>
                                            <i class='fa fa-check'></i>
                                        </button>`;

                    botones += `     <button id='linkVerMovimientosHE' data-id='${datos[fila][0]}' type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' 
                                        title='Ver movimientos' onclick='linkVerMovimientosHojaEvolucion(this);'>
                                        <i class='fa fa-list'></i>
                                    </button>`;

                    botones += `</div>`;

                    return botones;
                }
            },
            {
                "targets": [0, 1],
                "visible": false
            }
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) { },
        "bDestroy": true
    });

    $('[data-toggle="tooltip"]').tooltip();
    $(grilla).css("width", "100%");

}

function linkVerHojasEvolucion(sender) {

    $('#txtTitle').html('Hoja de evolución');
    $('#divCarouselHos').carousel(0);
    
    var idPaciente = ($(sender).data("id") === undefined ? '' : $(sender).data("id"));
    var idEvento = ($(sender).attr("data-idEvento") === undefined ? '' : $(sender).attr("data-idEvento"));

    var idhos = ($(sender).data("idhos") === undefined ? '' : $(sender).data("idhos"));
    console.log(idhos)
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/HojaIngresoEgreso/${idhos}`,
        async: false,
        success: function (data, status, jqXHR) {
            //GEN_idUbicacion = data[0].GEN_idUbicacion_Ingreso;
            $('#txtPacienteHE').val(data[0].GEN_nombrePaciente + ' ' + data[0].GEN_ape_paternoPaciente + ' ' + data[0].GEN_ape_maternoPaciente);
            $('#txtCamaActualHE').val(data[0].HOS_nombreCamaActual);
        }
    });

    $("#linkNuevaHojaEvolucion").data("idPaciente", idPaciente);
    $("#linkNuevaHojaEvolucion").data("idEvento", idEvento);

    GetTablaHojaEvualuacion(idPaciente, idEvento);
    $("#mdlVerHojasEvolucion").modal('show');

}

function linkCrearNuevaHojaEvolucion() {

    var idPaciente = $("#linkNuevaHojaEvolucion").data("idPaciente");
    var idEvento = $("#linkNuevaHojaEvolucion").data("idEvento");
    setSession('ID_PACIENTE', idPaciente);
    
    if (idEvento != undefined)
        setSession('ID_EVENTO', idEvento);
    
    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevaHojaEvolucionHOS.aspx";

}

function linkEditarHojaEvolucion(sender) {
    var id = $(sender).data("id");
    setSession('ID_HOJAEVOLUCION', id);    
    window.location.replace(ObtenerHost() + "/Vista/ModuloHOS/NuevaHojaEvolucionHOS.aspx");
}

function linkValidarHojaEvolucion(sender) {

    var idHE = $(sender).data("id");
    var jsonHE = null;

    $.ajax({
        type: "GET",
        url: GetWebApiUrl() + "RCE_Hoja_Evolucion/" + idHE,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            // 81 = Hoja de Evolución Validada
            jsonHE = data[0];
            jsonHE.GEN_idTipo_Estados_Sistemas = 81;

        }
    });
    
    var json = {
        "RCE_idHoja_Evolucion": jsonHE.RCE_idHoja_Evolucion,
        "GEN_idUbicacion": jsonHE.GEN_idUbicacion,
        "RCE_fechaHoja_Evolucion": jsonHE.RCE_fechaHoja_Evolucion,
        "RCE_idEventos": jsonHE.RCE_idEventos,
        "GEN_idProfesional": jsonHE.GEN_idProfesional,
        "RCE_descripcionHoja_Evolucion": jsonHE.RCE_descripcionHoja_Evolucion,
        "RCE_estadoHoja_Evolucion": jsonHE.RCE_estadoHoja_Evolucion,
        "RCE_fecha_limite_edicionHoja_Evolucion": jsonHE.RCE_fecha_limite_edicionHoja_Evolucion,
        "GEN_idTipo_Estados_Sistemas" : jsonHE.GEN_idTipo_Estados_Sistemas,
        "RCE_idPlantillas": jsonHE.RCE_idPlantillas,
        "RCE_evolucionHoja_Evolucion" : jsonHE.RCE_evolucionHoja_Evolucion,
        "RCE_examen_FisicoHoja_Evolucion": jsonHE.RCE_examen_FisicoHoja_Evolucion
    }
    
    $.ajax({
        type: "PUT",
        url: GetWebApiUrl() + "RCE_Hoja_Evolucion/" + idHE,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(json),
        async: false,
        success: function (data) {
            toastr.success('La Hoja de Evolución se a validado exitosamente.');
        }
    });

    GetTablaHojaEvualuacion(jsonHE.GEN_idPaciente, '');

}

function linkImprimirHojasEvolucion(sender) {
    
    var id = $(sender).data("id");
    //visibleDesdeOyente($(sender));
    setSession('ID_HOJAEVOLUCION', id);
    $("#modalCargando").show();
    $('#frameHojaEvolucion').attr('src', ObtenerHost() + '/Vista/ModuloHOS/ImpresionHojaEvolucion.aspx?id=' + id);
    $('#mdlImprimirHE').modal('show');
    $('#mdlVerHojasEvolucion').modal('hide');

}

function linkVerMovimientosHojaEvolucion(sender) { 

    var id = $(sender).data("id");
    var adataset = [];

    $.ajax({
        type: "GET",
        url: GetWebApiUrl() + "RCE_Movimientos_Hoja_Evolucion/RCE_idHoja_Evolucion/" + id,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $.each(data, function (i, r) {
                adataset.push([
                    r.RCE_idMovimientos_Hoja_Evolucion,
                    moment(moment(r.RCE_fechaMovimientos_Hoja_Evolucion).toDate()).format('DD-MM-YYYY HH:mm:ss'),
                    r.GEN_loginUsuarios,
                    r.GEN_descripcionTipo_Movimientos_Sistemas
                ]);
            });
        }
    });

    $('#tblMovHE').addClass("nowrap").DataTable({
        data: adataset,
        lengthMenu: [[5, 10], [5, 10]],
        columns: [
            { title: "RCE_idMovimientos_Interconsulta" },
            { title: "Fecha Movimiento" },
            { title: "Usuario" },
            { title: "Movimiento" }
        ],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false
            },
            { "targets": 1, "sType": "date-ukLong" }
        ],
        "bDestroy": true
    });
    $('#txtTitle').html('Movimientos');
    $('#divCarouselHos').carousel(1);
    $("#tblMovHE").css("width", "100%");
    //$('#mdlMovimientoHE').modal('show');
}