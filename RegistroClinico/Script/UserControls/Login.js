﻿
$(document).ready(function () {
    $("#aLoginModal").click(function () {
        LoginModal();
    });
});

function LoginModal() {

    const user = $('#txtUsuarioLogin').val();
    const pass = CryptoJS.MD5($('#txtClaveLogin').val());
    const datos = {
        "grant_type": "password",
        "username": user,
        "password": '' + pass,
        "clientid": '14'
    }

    delete $.ajaxSettings.headers['Authorization'];

    $.ajax({
        type: "POST",
        url: `${GetWebApiUrl()}recuperarToken`,
        data: datos,
        async: false,
        success: function (response) {
            setSession("TOKEN", response.access_token);
            setSession("FECHA_FINAL_SESION", moment().add(parseInt(response.expires_in) - 5, 'seconds').format('YYYY-MM-DD HH:mm:SS'));
            setSession("LOGIN_USUARIO", user);
            $.ajaxSetup({ headers: { 'Authorization': GetToken() } });
            GetUsuarioModal();
        },
        error: function (err) {
            console.log("ERROR no se pudo obtener el token: " + JSON.stringify(err));
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: "Usuario o contraseña incorrecta",
                showConfirmButton: false,
                timer: 2000
            });
        }
    });

}

function GetUsuarioModal() {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/GEN_Usuarios/Acceso`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            if ($.isEmptyObject(data)) {

                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: "Usuario o contraseña incorrecta",
                    showConfirmButton: false,
                    timer: 2000
                });

            } else {

                console.log(JSON.stringify(data));

                SetSessionServer({
                    LOGIN_USUARIO: getSession().LOGIN_USUARIO,
                    NOMBRE_USUARIO: getSession().NOMBRE_USUARIO,
                    CODIGO_PERFIL: getSession().CODIGO_PERFIL,
                    PERFIL_USUARIO: getSession().PERFIL_USUARIO,
                    TOKEN: getSession().TOKEN
                });

                $('#mdlLogin').modal('hide');
                $('#txtUsuarioLogin').val('');
                $('#txtClaveLogin').val('');
            }

        }
    });

}
