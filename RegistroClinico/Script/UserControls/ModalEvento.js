﻿
var evento = {};

function GetEvento() {
    return evento;
}

$(document).ready(function () {
    
    $('#txtEventoInicioFecha').change(function () {
        if (fechaEvento()) 
            $('#spanFechaInicio').show();
        else
            $('#spanFechaInicio').hide();
    });

    $('#txtEventoFinFecha').change(function () {
        if (fechaEvento())
            $('#spanFechaFin').show();
        else
            $('#spanFechaFin').hide();
    });

    ComboTipoEvento();
    $("#txtEventoInicioFecha").val(moment().format("YYYY-MM-DD"));

    $('#btnNuevoEvento').on('click', function (e) {

        var bValidar = true;

        bValidar = validarCampos('#divEvento', false);

        if (fechaEvento())
            bValidar = false;

        if (bValidar) {
            CrearNuevoEvento();
            $('#mdlEventoNuevo').modal('hide');


            //nuevoIPD
    if ($('#txtDiagnostico'))
        $('#txtDiagnostico').val($('#txtEventoDiagnostico').val());
    if ($('#lbltxtDiagnostico'))
        $('#lbltxtDiagnostico').addClass('active');
        }        
    });
});

function iniciarCamposEvento() {
    $('button[data-id="sltEventoAmbito"]').html('Seleccione');
    $('#sltEventoAmbito').prop('selectedIndex', 0);
    $('#txtEventoCausa').val('');
    $('#txtEventoAnamnesis').val('');
    $('#txtEventoDiagnostico').val('');
    $('#txtEventoDiagnostico').val('');
    $('#txtEventoFechaAlta').val(moment().format("YYYY-MM-DD"));
    $('#txtEventoInicioFecha').val(moment().format("YYYY-MM-DD"));
    $('#txtEventoFinFecha').val(moment().format("YYYY-MM-DD"));
}

function fechaEvento() {
    var date1 = moment($('#txtEventoInicioFecha').val()).format("YYYY-MM-DD");
    var date2 = moment($('#txtEventoFinFecha').val()).format("YYYY-MM-DD");
    if (date1 > date2)
        return true;
    else
        return false;
}

function ComboTipoEvento() {
    $('#sltEventoAmbito').empty();
    $("#sltEventoAmbito").append("<option value='0'>Seleccione</option>");
    
    try {
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Tipo_Evento/Combo",
            async: false,
            success: function (data, status, jqXHR) {
                $.each(data, function (key, val) {
                    $("#sltEventoAmbito").append("<option value='" + val.RCE_idTipo_Evento + "'>" +
                        val.RCE_descripcionTipo_Evento + "</option>");
                });
                $("#sltEventoAmbito");
            },
            error: function (jqXHR, status) {
                console.log("Error al llenar Tipo Evento: " + jqXHR.responseText);
            }
        });
    } catch (err) {
        alert("Error al llenar Tipo Evento: " + err.message);
    }
}

function CrearNuevoEvento() {
    var json = {
        RCE_anamnesisEventos: valCampo($('#txtEventoAnamnesis').val()),
        RCE_diagnosticoEventos: valCampo($('#txtEventoDiagnostico').val()),
        RCE_alta_pacienteEventos: valCampo($('#txtEventoFechaAlta').val()),
        GEN_idPaciente: evento.GEN_idPaciente,
        GEN_idProfesional: evento.GEN_idProfesional,
        RCE_fecha_inicioEventos: valCampo($('#txtEventoInicioFecha').val()),
        RCE_fecha_terminoEventos: valCampo($('#txtEventoFinFecha').val()),
        RCE_causaEventos: valCampo($('#txtEventoCausa').val()),
        RCE_idTipo_Evento: valCampo($('#sltEventoAmbito').val()),
        RCE_estadoEventos: "Activo"
    }

    $.ajax({
        type: "POST",
        url: GetWebApiUrl() + "RCE_Eventos",
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {

            $("#mdlEventoNuevo").modal('hide');
            evento.RCE_idEventos = data.RCE_idEventos;
            toastr.success('Evento creado Exitosamente.');

        },
        error: function (jqXHR, status) {
            alert('Error al crear Evento ' + status.code);
        }
    });
}

function ShowModalEvento() {
    $("#mdlEventoNuevo").modal('show');
}
