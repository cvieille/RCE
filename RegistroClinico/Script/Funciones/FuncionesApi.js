﻿function getDataMovimientosporApi(url, tabla, modal) {
    var adataset = [];
    $("#lnkExportarMovimientos").click(function (event) {
        event.preventDefault();
    });

    $.ajax({
        type: "GET",
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            let response = data[0];
            $("#idHosModalMovimientos").html(response.Paciente.Hospitalizacion.id);
            $("#nomPacModalMovimientos").html("Paciente: <b>"+response.Paciente.nombre+"</b>");
            $.each(response.Movimiento, function (i, r) {
                adataset.push([
                    moment(r.Fecha).format('DD-MM-YYYY HH:mm:ss'),
                    r.Usuario,
                    r.Descripcion
                ]);
            });

            if (adataset.length == 0)
                $('#lnkExportarMovimientos').addClass('disabled');

            else
                $('#lnkExportarMovimientos').removeClass('disabled');

            $(tabla).addClass("nowrap").DataTable({
                data: adataset,
                order: [],
                columns: [
                    { title: "Fecha Movimiento" },
                    { title: "Usuario" },
                    { title: "Movimiento" }
                ],
                "columnDefs": [
                    {
                        "targets": 1,
                        "sType": "date-ukLong"
                    }
                ],
                "bDestroy": true
            });
            $(modal).modal('show');
        }
    });
}