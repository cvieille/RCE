﻿$(document).ready(function () {
    var sSession = getSession();
    RevisarAcceso(false, sSession);
    if (sSession.CODIGO_PERFIL != '7' != sSession.CODIGO_PERFIL != '14') {

    }
    //console.log();

    $('#txtFiltroDesde').val(moment().format('YYYY-MM-DD'));
    $('#txtFiltroHasta').val(moment().format('YYYY-MM-DD'));
    tablaReporte();

    $('#selFiltroReporte').change(function (e) {
        $('#btnExportar').addClass('disabled').attr('disabled', true);
    });

    $('#btnFiltro').click(function (e) {        
        tablaReporte();
        e.preventDefault();
    });
});

function tablaReporte()
{
    var json = {};
    var adataset = [];
    var f = $('#selFiltroReporte').val();
    json['dFechaDesde'] = $('#txtFiltroDesde').val();
    json['dFechaHasta'] = $('#txtFiltroHasta').val();

    var url = `${GetWebApiUrl()}Orden/Informes/FichasCreadas`;
    if (f == '2') 
        url = `${GetWebApiUrl()}Orden/Informes/fichasModificadasOdontologia`;

    $.ajax({
        type: 'POST',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(json),
        async: false,
        success: function (data) {
            $.each(data, function (key, val) {
                adataset.push([
                    f,
                    val.UbicacionInterna,
                    val.Documento,
                    val.NombrePaciente,
                    moment(val.FechaCreacion, 'DD/MM/YYYY').format('DD-MM-YYYY'),
                    //val.FechaCreacion,
                    val.CreadoPor,
                    val.ModificadoPor
                ]);
            });
        }
    });

    if (adataset.length == 0)
        $('#btnExportar').addClass('disabled').attr('disabled', true);
    else
        $('#btnExportar').removeClass('disabled').removeAttr('disabled');
    //console.log('a');
    $('#tblReporte').DataTable({
        data: adataset,
        columns: [
            { title: ''},
            { title: "N° ubicación interna" },
            { title: "N° documento" },
            { title: "Nombre paciente" },
            { title: "Fecha de creación" },
            { title: "Creado por" },
            { title: "Modificado por" },
        ],
        columnDefs: [
            { targets: 3, sType: 'date-ukShort' },
            { targets: 0, visible: false, searchable: false },
            //{
            //    targets: [4, 6, 8],
            //    visible: false
            //}
        ],
        bDestroy: true
    });
}