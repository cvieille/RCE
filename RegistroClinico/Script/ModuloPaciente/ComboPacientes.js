﻿function ComboPrevision() {
    let url = `${GetWebApiUrl()}GEN_Prevision/Combo`;
    setCargarDataEnCombo(url, false, "#sltPrevision");
}
function ComboTramo(slt, idPrevision) {
    if (idPrevision != '' && idPrevision != undefined && idPrevision != null) {
        try {
            $("#sltPrevisionTramo").empty();
            $("#sltPrevisionTramo").prop("disabled", false);
            $("#sltPrevisionTramo").append("<option value='0'>Seleccione</option>");


            let url = `${GetWebApiUrl()}GEN_Prevision_Tramo/GEN_idPrevision/${idPrevision}`;
            setCargarDataEnCombo(url, false, $("#sltPrevisionTramo"));
        } catch (err) {
            alert(err.message);
        }
    }
}