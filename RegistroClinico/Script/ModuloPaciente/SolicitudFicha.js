﻿var vID;
$(document).ready(function () {

    var sSession = getSession();
    RevisarAcceso(false, sSession);

    comboAmbito('#ddlAmbito');
    tablaSolicitud();

    $('#btnFiltro').click(function () {
        //ámbito = 0;
        //hacer funcionar esto cuando esté el controlador
        tablaSolicitud();
    });
    $('#btnLimpiarFiltro').click(function () {
        tablaSolicitud();
    });

    $('#btnOrigen').click(function () {
        var bGuardar = true;
        if ($('#ddlUbicacionDestino').prop('value') == 0 || $('#ddlUbicacionDestino').prop('value') == '' || $('#ddlUbicacionDestino').prop('value') == undefined) {
            $('#ddlUbicacionDestino').selectpicker('setStyle', 'btn-danger', 'add');
            bGuardar = false;
        }
        if (bGuardar) {
            $.ajax({
                type: 'POST',
                url: ObtenerHost() + '/Vista/ModuloPaciente/SolicitudFicha.aspx/cambiarUbicacion',
                contentType: 'application/json',
                data: JSON.stringify({ id: vID, nuevaub: $('#ddlUbicacionDestino').prop('value')}),
                dataType: 'json',
                async: false,
                success: function (data) {
                    vID = 0;
                    tablaSolicitud();
                    $('#mdlUbicacion').modal('hide');
                }
            });
        }
    });

    $('#btnQuitar').click(function () {
        $.ajax({
            type: 'POST',
            url: ObtenerHost() + '/Vista/ModuloPaciente/SolicitudFicha.aspx/quitarSolicitud',
            contentType: 'application/json',
            data: JSON.stringify({ id: vID }),
            dataType: 'json',
            async: false,
            success: function (data) {
                vID = 0;
                tablaSolicitud();
                $('#mdlQuitar').modal('hide');
            }
        });
    });
});

function comboAmbito(elem) {
    $(elem).empty();
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Ambito/Combo',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (i, r) {
                $(elem).append('<option value="' + r.GEN_idAmbito + '">' + r.GEN_nombreAmbito + '</option>');
            });
            $(elem).selectpicker('refresh');
        }
    });
}

function comboUbicacion(elem, id) {
    $(elem).empty();
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Ubicacion/Clinicos',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (i, r) {
                $(elem).append('<option value="' + r.GEN_idUbicacion + '">' + r.GEN_nombreUbicacion + '</option>');
            });
            $(elem).selectpicker('refresh');
            $(elem).prop('value', id).selectpicker('refresh');
        }
    });
}

function linkubicacion(e)
{
    var ubi = $(e).data("ubicacion");
    var id = $(e).data("id");
    vID = id;
    $('#ddlUbicacionOrigen').selectpicker('destroy').selectpicker();
    $('#ddlUbicacionDestino').selectpicker('destroy').selectpicker();
    comboUbicacion('#ddlUbicacionOrigen', ubi);
    comboUbicacion('#ddlUbicacionDestino', 0);

    $('#mdlUbicacion').modal('show');
}

function quitarsolicitud(e) {
    var id = $(e).data("id");
    vID = id;
    $('#mdlQuitar').modal('show');
}

function tablaSolicitud()
{
    var adataset = [];
    $.ajax({
        type: 'POST',
        url: ObtenerHost() + '/Vista/ModuloPaciente/SolicitudFicha.aspx/bandeja',
        contentType: 'application/json',
        //data: JSON.stringify({ s: $('#idPaciente').val() }),
        dataType: 'json',
        async: false,
        success: function (data) {
            var json = JSON.parse(data.d);
            $.each(json, function (i, r) {
                adataset.push([
                    r.gen_idsolicitud_nui,
                    r.gen_idpaciente,
                    r.gen_idtipo_estados_sistemas,
                    r.gen_idubicacion,
                    r.gen_idambito,
                    r.GEN_nombrePaciente,
                    r.GEN_nombreTipo_Estados_Sistemas,
                    r.GEN_nombreUbicacion,
                    r.GEN_nombreAmbito
                ]);
            });
        }
    });

    $('#tblSolicitud').addClass('nowrap').DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: 'ID solicitud' },
            { title: 'gen_idpaciente' },
            { title: 'gen_idtipo_estados_sistemas' },
            { title: 'gen_idubicacion' },
            { title: 'gen_idambito' },
            { title: 'Paciente' },
            { title: 'Estado sistema' },
            { title: 'Ubicación' },
            { title: 'Ámbito' },
            { title: '' }
        ],
        columnDefs: [
            {
                targets: -1,
                data: null,
                orderable: false,
                render: function (data, type, row, meta) {
                    var fila = meta.row;
                    var botones;
                    botones = '<button class="btn btn-primary btn-block" data-toggle="collapse" data-target="#div_accionesIPD' + fila + '" onclick="return false;">ACCIONES</button>';
                    botones += '<div id="div_accionesIPD' + fila + '" class="collapse">';
                    botones += '<div class="btn-group-vertical w-100">';
                    botones += '<a id="linkubicacion" data-id="' + adataset[fila][0] + '" data-ubicacion="' + adataset[fila][3] + '" class="btn btn-default btn-block" href="#/" onclick="linkubicacion(this);"><i class="fa fa-pencil"></i> Ubicación</a>';
                    botones += '<a id="quitarsolicitud" data-id="' + adataset[fila][0] + '" class="btn btn-danger btn-block" href="#/" onclick="quitarsolicitud(this);"><i class="fa fa-ban"></i> Quitar</a>';
                   
                    botones += "</div>";
                    botones += "</div>";
                    
                    return botones;
                }
            },
            {
                targets: [1, 2, 3, 4],
                searchable: false,
                visible: false
            }
        ],
        bDestroy: true
    });
    //tblSolicitud
}