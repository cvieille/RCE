﻿
var sSession;
var idPac;

//READY
$(document).ready(function () {

    let IniciarComponentes = async function () {

        ShowModalCargando(true);
        await sleep(0);

        sSession = getSession();
        RevisarAcceso(true, sSession);

        cargaCombos();
        $.fn.bootstrapSwitch.defaults.onColor = 'info';
        $.fn.bootstrapSwitch.defaults.offColor = 'danger';
        $.fn.bootstrapSwitch.defaults.onText = 'SI';
        $.fn.bootstrapSwitch.defaults.offText = 'NO';
        $.fn.bootstrapSwitch.defaults.labelWidth = '1';
        DarFuncionRadios();

        let html = $("#divDatosAdicionalesPaciente").html();
        $("#divDatosAdicionalesPaciente").remove();
        $("#divDatosPaciente .card-body").append(`<div id='divDatosAdicionalesPaciente'>${html}</div>`);

        $("#ddlTramo").append("<option value='0'> - Seleccione - </option>");
        $(".material-icons").tooltip();
        $(".material-icons").click(function () {
            $(".material-icons").tooltip("hide");
        });

        //Cuando cambia algun valor de algun input o combo asigna valor a hdf
        $(".editable").change(function () {
            if ($("#hdfEditable").val() != "SI") {
                $("#hdfEditable").val("SI");
            }
        });

        //CUando cambia algun valor de algun checkbox asigna valor a hdf
        $('.editable-switch').on('switchChange.bootstrapSwitch', function () {
            if ($("#hdfEditable").val() != "SI") {
                $("#hdfEditable").val("SI");
            }
        });

        //Cuando se hace click sobre algun item de la lista del sidebar paciente pregunta si se edito algun campo
        //si es así levanta modal de guardado
        $(".nav-link.panel-paciente").click(function () {

            if ($("#hdfEditable").val() == "SI") {

                $("#btnGuardarCambios").data("pacienteDclinico", ($("#hdfTransicion").val() === "btnDatosClinicos"));
                //Asigna a hdf la pestaña a la cual iba antes de lanzar el modal y terminar el proceso
                $("#hdfTransicion").val($(this).attr("id"));
                $("#mdlGuardado").modal("toggle");
                return false;
            }
        });

        $("#ddlTipoIdentificacionPaciente").change(function () {
            comboTipoIdentificacion($(this).val());
        });

        //propiedades toastr
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "300",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        //Prueba del boton de evento
        $('body').on('click', '.muestraEvento', function (e) {
            var id = $(this).data("id");
            e.preventDefault();
        });

        $('body').on('change', '#ddlAseguradora', function (e) {
            if ($(this).val() != '0') {
                if ($('#ddlTramo').hasClass('disabled'))
                    $('#ddlTramo').removeClass('disabled');
                APITramo($(this).val(), null);
            }
        });

        //CUando cambia algun valor de algun checkbox asigna valor a hdf
        $('#switchOtrasAlergias').on('switchChange.bootstrapSwitch', function () {
            if ($("#switchOtrasAlergias").attr("checked")) {
                $("#divTxtOtrasAlergias").removeClass("hide")
            } else {
                $("#divTxtOtrasAlergias").addClass("hide");
            }
        });

        //calcula edad segun fecha de nacimiento
        $('#txtFechaNacPaciente').blur(function () {
            $('#txtEdadPaciente').val(CalcularEdad($(this).val()).edad);
        });

        $('#btnNuevoPacienteModal').click(function () {
            LimpiarPaciente();
            DeshabilitarPaciente(true);
            $("#sltIdentificacion").val('1').change();
            $("#sltIdentificacion option[value='5']").remove();
            $("#divExistente, #btnNuevoPaciente").hide();
            CargarCombosUbicacionPaciente();
            ComboNacionalidad()
            $('#mdlNuevoPaciente').modal('show');
        });

        //CLICK GRILLA FORMULARIO IND QX
        $('#btnSolPabellon').click(function () {
            getFormulariosIndQx();
        });

        //CLICK GRILLA PROTOCOLO OPERATORIO
        $('#btnProtocolo').click(function () {
            getProtocolosOperatorios();
        });

        //CLICK GRILLA HOSPITALIZACION
        $('#btnHospitalizacion').click(function () {
            getHospitalizaciones();
        });

        //CLICK GRILLA RECETA
        $('#btnReceta').click(function () {
            getRecetas()
        });

        //CLICK GRILLA BIOPSIA
        $('#btnBiopsia').click(function () {
            getRegistrosBiopsias();
        });

        //CLICK GRILLA URGENCIA
        $('#btnUrgencia').click(function () {
            getAtencionesUrgencia();
        });

        $('#btnEpicrisis').click(function () {
            getEpicrisis();
        });

        $('#btnIPD').click(function () {
            getIPD();
        });

        $('#btnHojaEvolucion').click(function () {
            getHojasEvolucion();
        });

        $('#btnInterconsulta').click(function () {
            getInterconsultas();
        });

        $('#btnLaboratorio').click(function () {
            getLaboratorio();
        });

        $('#btnEventoAdverso').click(function () {
            getEventosAdversos();
        });

        //Boton de Registro Clínico
        $('#btnRegistroClinico').click(function () {

            let verRCE = async function () {

                $("#modalCargando").show();
                await sleep(0);

                //Obtiene una lista de los sistemas a los cuales tiene permiso
                getPermisosSistemas();

                $('.material-icons.nav-link.active.show').removeClass('active show');
                $('.tab-pane.fade.panel-rc.active.show').removeClass('active show in');
                $('#btnSolPabellon').addClass('active show');
                $('#panel10').addClass('active show');
                getFormulariosIndQx();
                //Primera vista la de FIQ
                getCountRegistroClinico();
                //getFormularioIndQx();
                getProtocolosOperatorios();
                getRecetas();
                getRegistrosBiopsias();
                getHospitalizaciones();
                getAtencionesUrgencia();
                getEpicrisis();
                getLaboratorio();
                inactivarBotonesRegistroClinico();
                $('#alertRegistroClinico').hide();
            }

            verRCE().then(() => ShowModalCargando(false));

        });

        $('#btnAtencionesPaciente').click(function () {
            getAtencionesPaciente();
        });

        //Boton Refresh de registro clinico
        $('#btnRefresh').click(function (e) {

            e.preventDefault();
            getCountRegistroClinico();
            getFormulariosIndQx();
            getProtocolosOperatorios();
            getRecetas();
            getRegistrosBiopsias();
            getHospitalizaciones();
            getAtencionesUrgencia();
            getEpicrisis();
            getLaboratorio();
            getEventosAdversos();
            inactivarBotonesRegistroClinico();

        });

        $("#ddlPaisPaciente").change(function () {
            if ($("#ddlPaisPaciente").val() != '0') {
                APIRegion($("#ddlPaisPaciente").val());
                $("#ddlProvinciaPaciente").val('0');
                $("#ddlCiudadPaciente").val('0');
                $("#ddlProvinciaPaciente").addClass('disabled');
                $("#ddlCiudadPaciente").addClass('disabled');
            }
        });

        $("#ddlRegionPaciente").change(function () {
            if ($("#ddlRegionPaciente").val() != '0') {
                APIProvincia($("#ddlRegionPaciente").val());
                $("#ddlCiudadPaciente").val('0');
                $("#ddlCiudadPaciente").addClass('disabled');
            }
        });

        $("#ddlProvinciaPaciente").change(function () {
            if ($("#ddlProvinciaPaciente").val() != '0') {
                APICiudad($("#ddlProvinciaPaciente").val());
            }
        });

        //Select tipo de identificación
        $('#sltTipoIdentificacion').change(function () {
            $('#txtFiltroRut').val("");
            $('#txtDv').val("");
            $('#txtFiltroRut').attr("placeholder", $(this).children("option:selected").html());
            if ($(this).val() == 1 || $(this).val() == 4) {
                $('.digito').show();
                $('#txtFiltroRut').attr("maxlength", 8);
            } else {
                $('.digito').hide();
                $('#txtFiltroRut').attr("maxlength", 10);
            }
        });

        //Select RUT
        $('#txtFiltroRut').on('blur', function () {
            ValidarRut();
        });

        $('#txtFiltroRut').on('input', function () {

            if ($.trim($(this).val()) !== "")
                $("#txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe, #txtFiltroNUI").val("").attr("disabled", "disabled");
            else
                $("#txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe, #txtFiltroNUI").removeAttr("disabled");

        });

        $('#txtFiltroNUI').on('input', function () {

            if ($.trim($(this).val()) !== "")
                $("#txtFiltroRut, #txtDv, #txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe").val("").attr("disabled", "disabled");
            else
                $("#txtFiltroRut, #txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe").removeAttr("disabled");

        });
        $('#txtRutPac').on('blur', function () {
            ('#txtRutDigitoPaciente').val(ObtenerVerificador($(this).val()));
        });

        $('#txtEmailPaciente').on('blur', function () {
            var valido = caracteresCorreoValido($(this));
        });

        $('.buscar').on('keypress', function (e) {
            if (e.keyCode == 13) {
                ValidarRut();
                seleccionaPaciente();
            }
        });

        $('#btnFiltroBandeja').click(function () {
            seleccionaPaciente();
        });

        $('#ddlAseguradora').change(function () {
            if ($(this).val() != '1' && $(this).val() != '2') {
                $('#divPrais').hide();
            } else {
                $('#divPrais').show();
            }
        });

        cargaPerfiles();
        CargarComboTipoRiesgo();
        deleteSession('ID_PACIENTE');

        $('#btnNuevoPaciente').click(function (e) {
            if (validarCampos("#divDatosPaciente", false) & validarCampos("#divDatosAdicionalesPaciente", false)) {
                GuardarPaciente();
                $("#sltTipoIdentificacion").val($("#sltIdentificacion").val());
                $("#txtFiltroRut").val($("#txtnumerotPac").val());
                ValidarRut();
                $('#mdlNuevoPaciente').modal('hide');
                seleccionaPaciente();
            }
        });

        $("#btnLimpiarFiltros").click(function () {
            $("#sltTipoIdentificacion").val(1);
            $("#txtFiltroRut, #txtDv, #txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe, #txtFiltroNUI").val("");
            $("#txtFiltroRut, #txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe, #txtFiltroNUI").removeAttr("disabled");
            limpiaCampos();
            $('#divPacientes').hide();
        });

        OyentesNuevoPaciente();
    }

    IniciarComponentes().then(() => ShowModalCargando(false));

});

function OyentesNuevoPaciente() {

    $("#sltIdentificacion").change(function () {
        $("#divInfoPaciente, #divUbicacionPaciente").show();
        $("#btnNuevoPaciente, #divExistente").hide();
    });

    $("#txtnumerotPac").on('input', function () {
        // 1 = RUT / 4 = RUT MATERNO
        if ($("#sltIdentificacion").val() === '1' || $("#sltIdentificacion").val() === '4') {
            $("#divInfoPaciente, #divUbicacionPaciente").show();
            $("#divExistente").hide();
        }
    });

    $("#txtnumerotPac").on("blur", function () {
        if ($("#sltIdentificacion").val() === '2' || $("#sltIdentificacion").val() === '3') {
            if ($.trim($("#txtnumerotPac").val()) != "") {
                if (GetPaciente().GEN_idPaciente != undefined) {
                    $("#divInfoPaciente, #divUbicacionPaciente, #btnNuevoPaciente").hide();
                    $("#divExistente").show();
                } else {
                    LimpiarPaciente();
                    $("#divInfoPaciente, #divUbicacionPaciente, #btnNuevoPaciente").show();
                    $("#divExistente").hide();
                }
            } else {
                $("#divInfoPaciente, #divUbicacionPaciente").show();
                $("#btnNuevoPaciente, #divExistente").hide();
            }
        }
    });

    $("#txtDigitoPac").on("input", function () {
        if ($("#sltIdentificacion").val() === '1' || $("#sltIdentificacion").val() === '4') {
            if (GetPaciente().GEN_idPaciente != undefined) {
                $("#divInfoPaciente, #divUbicacionPaciente, #btnNuevoPaciente").hide();
                $("#divExistente").show();
            } else {
                $("#divInfoPaciente, #divUbicacionPaciente").show();
                $("#divExistente").hide();
                if ($.trim($("#txtDigitoPac").val()) != "")
                    $("#btnNuevoPaciente").show();
            }
        }
    });
}

function ValidarRut() {

    if (($('#txtFiltroRut').val() != "" && ($('#sltTipoIdentificacion').val() == '1') || $('#sltTipoIdentificacion').val() == '4')) {
        let dig = ObtenerVerificador($('#txtFiltroRut').val());
        if (dig !== null)
            $('#txtDv').val(dig);
        else
            $('#txtDv, #txtFiltroRut').val("");
    } else
        $('#txtDv').val("");

}

//FUNCIONES
function inactivarBotonesRegistroClinico() {
    $('.material-icons.nav-link').removeClass('active').removeClass('show');
    $('.panel-rc').removeClass('in').removeClass('active').removeClass('show');
}

function getAtencionesPaciente() {

    var adataset = [];
    //console.log($('#idPaciente').val());

    $.ajax({
        type: "POST",
        url: ObtenerHost() + "/Vista/ModuloPaciente/BandejaPAC.aspx/bandeja",
        contentType: "application/json",
        data: JSON.stringify({ s: $('#idPaciente').val() }),
        dataType: "json",
        async: false,
        success: function (data) {
            var json = JSON.parse(data.d);
            $.each(json, function (i, r) {
                adataset.push([
                    r.GEN_idAtencion_Paciente_Establecimiento,
                    r.GEN_nombreEstablecimiento,
                    r.GEN_fechaAtencion_Paciente_Establecimiento
                ]);
            });

        }
    });

    $('#tblAtenciones').addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: "ID Atención" },
            { title: "Establecimiento" },
            { title: "Fecha atención" }
        ],
        columnDefs: [
            {
                targets: [0],
                searchable: false
            }
        ],
        bDestroy: true
    });
}

function getPermisosSistemas() {

    $.ajax({
        type: "GET",
        url: GetWebApiUrl() + "GEN_Usuarios/Sistemas",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $.each(data, function () {
                $("[data-id-sistema='" + this.GEN_idSistemas + "']").removeClass("disabled");
                $("[data-id-sistema='" + this.GEN_idSistemas + "'] > span").removeClass("hide");
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('error:' + xhr.responseText);
        }
    });
}


function CargarComboTipoRiesgo() {

    $("#sltTabaco, #sltOh").empty();

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "PAB_Tipo_Riesgo/Combo",
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (key, val) {
                $("#sltTabaco, #sltOh").append("<option value='" + val.PAB_idTipo_Riesgo + "'>" +
                    val.PAB_nombreTipo_Riesgo + "</option>");
            });

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar PAB_Tipo_Riesgo: " + jqXHR.responseText);
        }
    });

}

//GUARDAR
function guardarPaciente_click(e) {
    if (!validarCampos("#divDPaciente", true))
        return;
    var valid = validaCampos();
    if (!valid) {
        toastr.error("Falta completar algunos campos obligatorios");
        return;
    }
    guardaPaciente(false);

}

function guardarDatosClinicos_click(e) {
    //var valid = validaCampos();
    if (/*!valid | */!validarCampos("#divHabitos", true)) {
        toastr.error("Falta completar algunos campos obligatorios");
        //e.preventDefault();
    } else
        guardaPaciente(true);
}

function guardarDatosPrevisionales_click(e) {
    var valid = validaCampos();
    if (!valid) {
        toastr.error("Falta completar algunos campos obligatorios");
    }
    else
        guardaPaciente(false);
}

//VALIDA (Hans)
function validaCampos() {
    var valido = true;
    //Datos paciente
    valido = ValidarTxt($("#txtNombrePaciente"), valido);
    valido = ValidarTxt($("#txtApellidoPaciente"), valido);
    valido = ValidarTxt($("#txtFechaNacPaciente"), valido);
    valido = ValidarTxt($("#ddlSexoPaciente"), valido);
    valido = ValidarTxt($("#ddlGeneroPaciente"), valido);
    valido = ValidarTxt($("#ddlNacionalidadPaciente"), valido);
    if ($('#ddlTipoIdentificacionPaciente').val() == 1 || $('#ddlTipoIdentificacionPaciente').val() == 4) {
        //valido = ValidarTxt($("#txtTelefonoPaciente"), valido);
    }
    valido = caracteresCorreoValido($('#txtEmailPaciente'), valido);
    return valido;
}

//Cambia de tipo de identificación
function comboTipoIdentificacion(value) {
    if (value == 1 || value == 4) {
        $("#txtRutPaciente").show();
        $("#RutPaciente").show();
        $("#txtRutDigitoPaciente").show();
        $("#lblRutPaciente").show();
        //calcula rut
        if ($("#txtRutPaciente").val() != null) {
            $("#txtRutDigitoPaciente").val(calculaDigito($("#txtRutPaciente").val()));
        }
    } else {
        $("#txtRutDigitoPaciente").val(null);
        if (value == 5) {
            $("#lblRutPaciente").hide();
            $("#txtRutPaciente").hide();
            $("#RutPaciente").hide();
            $("#txtRutDigitoPaciente").hide();
        } else {
            $("#lblRutPaciente").show();
            $("#txtRutPaciente").show();
            $("#RutPaciente").show();
            $("#txtRutDigitoPaciente").hide();
        }
    }
}

function calculaDigito(run) {
    digito = ObtenerVerificador(run);
    return digito;
}

//GET API REST Para Traer objeto Paciente completo
function seleccionaPaciente() {

    $("#divPacientes").hide();

    if ($('#txtFiltroRut').val() != "") {

        let valido = validaFiltroRut();
        let iden = '';
        let rut = '';

        if (!valido) {
            toastr.error("Debe seleccionar un tipo de identificación para buscar al paciente por ficha clínica.");
            event.preventDefault();
        }
        //ddlFiltroIdentificacion

        iden = $('#sltTipoIdentificacion').val();
        rut = $('#txtFiltroRut').val();

        //Obtener el Paciente por RUT
        getPacienteRut(iden, rut);

    } else {

        let nombre = '', apep = '', apem = '', nui = '';

        if ($.trim($('#txtFiltroNombre').val()) != "")
            nombre = $.trim($('#txtFiltroNombre').val());
        if ($.trim($('#txtFiltroApe').val()) != "")
            apep = $.trim($('#txtFiltroApe').val());
        if ($.trim($('#txtFiltroSApe').val()) != "")
            apem = $.trim($('#txtFiltroSApe').val());
        if ($.trim($("#txtFiltroNUI").val()) != "")
            nui = $.trim($("#txtFiltroNUI").val());

        if (nombre != "" || apep != "" || apem != "" || nui != "") {
            $("#divPacientes").show();
            getPacienteFiltro(nombre, apep, apem, nui);
        } else {
            toastr.error("Debe ingresar algún dato de búsqueda (Documento o nombres del paciente).");
            limpiaCampos();
        }

    }

}

//Valida que al buscar por ficha clínica haya seleccionado algun identificador
function validaFiltroRut() {

    var idenvalido;

    if ($('#sltTipoIdentificacion').val() == 0) {
        if (!$('#sltTipoIdentificacion').hasClass('is-invalid')) {
            $('#sltTipoIdentificacion').addClass('is-invalid')
        }
        idenvalido = false;
    } else {
        if ($('#sltTipoIdentificacion').hasClass('is-invalid')) {
            $('#sltTipoIdentificacion').removeClass('is-invalid');
        }
        idenvalido = true;
    }
    return idenvalido;

}

//Descarta el guardado de datos
function descartaGuardar() {
    $("#hdfEditable").val("NO");
    $("#mdlGuardado").modal("toggle");
    descartarGuardar();
    $("#" + ($("#hdfTransicion").val())).trigger("click");
}

//Modal guardar
function btnGuardaDatosModal() {
    var valid = validaCampos();
    if (!valid) {
        toastr.error("Falta completar algunos campos obligatorios");
        $("#mdlGuardado").modal("toggle");
        event.preventDefault();
    } else {
        $("#mdlGuardado").modal("toggle");
        guardaPaciente(($("#btnGuardarCambios").data("pacienteDclinico") == true));
        $("#hdfEditable").val("NO");
        toastr.success('DATOS DE PACIENTE GUARDADOS CORRECTAMENTE');
        $("#" + ($("#hdfTransicion").val())).trigger("click");
    }
}

//funciones de guardado por sección
function guardaPaciente(esGuardadoDClinico) {

    if ($("#hdfEditable").val() == "SI") {
        $.ajax({
            type: 'PUT',
            url: GetWebApiUrl() + "GEN_Paciente/" + $('#idPaciente').val(),
            data: JSON.stringify(JSONPaciente()),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, status, jqXHR) {
                if (esGuardadoDClinico)
                    guardaPacienteDClinico();
                toastr.success('SE HAN GUARDADO LOS DATOS DEL PACIENTE');
            },
            error: function (jqXHR, status) {
                toastr.error('ERROR AL GUARDAR DATOS DE PACIENTE');
                console.log(jqXHR.responseText);
            }
        });
    }
    else {
        toastr.info('SE HAN GUARDADO LOS DATOS DEL PACIENTE');
    }
    $("#hdfEditable").val("NO");
}

function guardaPacienteDClinico() {

    var json = JSONPacienteDClinico();

    if ($('#idPacienteDClinico').val() != 0) {

        json.GEN_idPaciente_Dclinico = parseInt($('#idPacienteDClinico').val());

        $.ajax({
            type: 'PUT',
            url: `${GetWebApiUrl()}/GEN_Paciente_Dclinico/${$('#idPacienteDClinico').val()}`,
            data: JSON.stringify(json),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, status, jqXHR) {
                toastr.success('SE HAN GUARDADO LOS DATOS DEL PACIENTE DE CLÍNICO');
            },
            error: function (jqXHR, status) {
                toastr.error('ERROR AL ACTUALIZAR DATOS DE PACIENTE DE CLÍNICO');
                console.log(jqXHR.responseText);
            }
        });
    } else {
        //console.log('POST');

        $.ajax({
            type: 'POST',
            url: `${GetWebApiUrl()}GEN_Paciente_Dclinico`,
            data: JSON.stringify(json),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, status, jqXHR) {
                toastr.success('SE HAN GUARDADO LOS DATOS DEL PACIENTE DE CLÍNICO');
            },
            error: function (jqXHR, status) {
                toastr.error('ERROR AL GUARDAR DATOS DE PACIENTE DE CLÍNICO');
                console.log(jqXHR.responseText);
            }
        });
    }

}

//descartar guaradado (carga nuevamente al paciente)
function descartarGuardar() {
    var identificacion = $("#ddlTipoIdentificacionPaciente").val()
    var documento = $("#txtRutPaciente").val()
    getPacienteRut(identificacion, documento);
}

//Funciones validar(Hans)
function ValidarTxt(text, b) {
    var bValido = true;
    if (text.val().trim() == '') {
        bValido = false;
        if (!text.hasClass('is-invalid'))
            text.addClass('is-invalid');
    } else {
        if (text.hasClass('is-invalid'))
            text.removeClass('is-invalid');
    }
    if (!b) {
        return false;
    }
    return bValido;
}
function ValidarDropDown(drop, b) {
    var bValido = true;
    if (drop[0].selectedIndex == 0) {
        bValido = false;
        if (!drop.hasClass('is-invalid'))
            drop.addClass('is-invalid');
    } else {
        if (drop.hasClass('is-invalid'))
            drop.removeClass('is-invalid');
    }

    if (drop[0].length == 1) {
        if (drop.hasClass('is-invalid'))
            drop.removeClass('is-invalid');
        bValido = true;

    }

    if (!b)
        return false;

    return bValido;
}
//Carga combos por API
function cargaCombos() {
    let selectorIdentificacion = "#sltTipoIdentificacionBusqueda, #sltTipoIdentificacion"
    //let selectorNacionalidad = "#sltNacionalidad, #sltNacionalidadPaciente"
    //ComboNacionalidad()
    //APIIdentificador();

    ComboTipoEscolaridad()
    comboIdentificacion(selectorIdentificacion)
    
    APISexo();
    APIGenero();
    APINacionalidad();
    APIEstadoConyugal();
    APIPuebloOriginario();
    APIGrupoSanguineo();
    APIPais();
    APIPrevision();
    $("#ddlRegionPaciente, #ddlProvinciaPaciente, #ddlCiudadPaciente").addClass('disabled');
    $("#ddlRegionPaciente, #ddlProvinciaPaciente, #ddlCiudadPaciente").append("<option value='0'> - Seleccione - </option>");

}
//Funciones API RESTful
//function APIIdentificador() {
//    $.ajax({
//        type: 'GET',
//        url: `${GetWebApiUrl()}GEN_Identificacion/Combo`,
//        contentType: "application/json",
//        dataType: "json",
//        success: function (data) {
//            $.each(data, function (key, val) {
//                $("#ddlTipoIdentificacionPaciente").append("<option value='" + val.GEN_idIdentificacion + "'>" + val.GEN_nombreIdentificacion + "</option>");
//                if (val.GEN_idIdentificacion != 5) { $("#ddlFiltroIdentificacion").append("<option value='" + val.GEN_idIdentificacion + "'>" + val.GEN_nombreIdentificacion + "</option>") }
//            });
//        }
//    });
//}

function APISexo() {
    let url = `${GetWebApiUrl()}GEN_Sexo/Combo`;
    setCargarDataEnCombo(url, true, "#ddlSexoPaciente");
}

function APIGenero() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Genero/Combo`;
    setCargarDataEnCombo(url, true, "#ddlGeneroPaciente");
}

function APINacionalidad () {
    let url = `${GetWebApiUrl()}GEN_Nacionalidad/Combo`;
    setCargarDataEnCombo(url, true, "#ddlNacionalidadPaciente");
}

function APIEstadoConyugal() {
    let url = `${GetWebApiUrl()}GEN_Estado_Conyugal/Combo`
    setCargarDataEnCombo(url, false, "#ddlEstadoConyuPaciente");
}
function APIPuebloOriginario() {
    let url = `${GetWebApiUrl()}GEN_Pueblo_Originario/Combo`
    setCargarDataEnCombo(url, false, "#ddlPuebloOrigPaciente");
}

function APIGrupoSanguineo() {
    let url = `${GetWebApiUrl()}GEN_Grupo_Sanguineo/Combo`;
    setCargarDataEnCombo(url, false, "#ddlGrupoSanguineoPaciente");
}

function APIPais() {

    $("#ddlPaisPaciente").append("<option value='0'> - Seleccione - </option>");

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Pais/Combo",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (key, val) {
                $("#ddlPaisPaciente").append("<option value='" + val.GEN_idPais + "'>" + val.GEN_nombrePais + "</option>");
            });
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar País: ${JSON.stringify(jqXHR)}`);
        }
    });

}

//Region aun no tiene controlador
function APIRegion(idPais) {

    $("#ddlRegionPaciente").removeClass('disabled');
    $("#ddlRegionPaciente").empty();
    $("#ddlRegionPaciente").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "/GEN_Region/GEN_idPais/" + idPais,
        async: false,
        success: function (data, status, jqXHR) {
            $.each(data, function () {
                $("#ddlRegionPaciente").append("<option value='" + this.GEN_idRegion + "'>" + this.GEN_nombreRegion + "</option>");
            });
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar region ${JSON.stringify(jqXHR)}`);
        }
    });

    cargaPerfiles();

}
//Provincia aun no tiene controlador
function APIProvincia(idRegion) {

    $("#ddlProvinciaPaciente").removeClass('disabled');
    $("#ddlProvinciaPaciente").empty();
    $("#ddlProvinciaPaciente").append("<option value='0'> - Seleccione - </option>");

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}` + "/GEN_Provincia/GEN_idRegion/" + idRegion,
        async: false,
        success: function (data, status, jqXHR) {
            $.each(data, function () {
                $("#ddlProvinciaPaciente").append("<option value='" + this.GEN_idProvincia + "'>" + this.GEN_nombreProvincia + "</option>");
            });
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar provincia ${JSON.stringify(jqXHR)}`);
        }
    });

    cargaPerfiles();

}
function APICiudad(idProvincia) {

    $("#ddlCiudadPaciente").removeClass('disabled');
    $("#ddlCiudadPaciente").empty();
    $("#ddlCiudadPaciente").append("<option value='0'> - Seleccione - </option>");

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Ciudad/GEN_Provincia/${idProvincia}`,
        async: false,
        success: function (data, status, jqXHR) {
            $.each(data, function () {
                $("#ddlCiudadPaciente").append(`<option value='${this.GEN_idCiudad}'>${this.GEN_nombreCiudad}</option>`);
            });
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar ciudad ${JSON.stringify(jqXHR)}`);
        }
    });

    cargaPerfiles();

}


function APIPrevision() {

    $("#ddlAseguradora").append("<option value='0'> - Seleccione - </option>");

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Prevision/Combo`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (key, val) {
                $("#ddlAseguradora").append(`<option value='${val.Id}'>${val.Valor}</option>`);
            });
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Previsión ${JSON.stringify(jqXHR)}`);
        }
    });

}
function APITramo(idPrevision, select) {
    let url = `${GetWebApiUrl()}GEN_Prevision_Tramo/GEN_idPrevision/${idPrevision}`;
    setCargarDataEnCombo(url, false, $("#ddlTramo"));

    if (select != null)
        $("#ddlTramo").val(select);
    cargaPerfiles();
    /*
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Prevision_Tramo/GEN_idPrevision/${idPrevision}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            if ($("#ddlTramo").hasClass('disabled'))
                $("#ddlTramo").removeClass('disabled');

            $("#ddlTramo").empty();
            $("#ddlTramo").append("<option value='0'> - Seleccione - </option>");

            $.each(data, function (key, val) {
                $("#ddlTramo").append(`<option value='${val.GEN_idPrevision_Tramo}'>${val.GEN_descripcionPrevision_Tramo}</option>`);
            });

            if (select != null)
                $("#ddlTramo").val(select);

            cargaPerfiles();

        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Tramo ${JSON.stringify(jqXHR)}`);
        }
    });
    */
}

function imprimirHOS(e) {
    visibleDesdeOyente($(e));
    var id = $(e).data("id");
    //setSession('ID_HOS', id);
    //$('#frameHospitalizacion').attr('src', 'ImprimirFormularioAdmision.aspx?id=' + id);
    $('#frameImprimir').attr('src', ObtenerHost() + '/Vista/ModuloHOS/ImprimirFormularioAdmision.aspx?id=' + id);
    $('#mdlImprimir').modal('show');
}

function imprimirIPD(e) {
    visibleDesdeOyente($(e));
    var id = $(e).data("id");
    setSession('ID_IPD', id);
    $('#frameImprimir').attr('src', ObtenerHost() + '/Vista/ModuloIPD/ImprimirIPD.aspx?id=' + id);
    $('#mdlImprimir').modal('show');
}

function imprimirIC(e) {
    visibleDesdeOyente($(e));
    var id = $(e).data("id");
    setSession('ID_INTERCONSULTA', id);
    $('#frameImprimir').attr('src', ObtenerHost() + '/Vista/ModuloIC/ImprimirIC.aspx?id=' + id);
    $('#mdlImprimir').modal('show');
}

function imprimirHE(e) {
    visibleDesdeOyente($(e));
    var id = $(e).data("id");
    setSession('ID_HOJAEVOLUCION', id);
    $('#frameImprimir').attr('src', ObtenerHost() + '/Vista/ModuloHOS/ImpresionHojaEvolucion.aspx?id=' + id);
    $('#mdlImprimir').modal('show');
}

function imprimirURG(e) {
    visibleDesdeOyente($(e));
    var id = $(e).data("id");
    $('#frameImprimir').attr('src', `${ObtenerHost()}/Vista/ModuloUrgencia/ImprimirDAU.aspx?id=${id}`);
    $('#mdlImprimir').modal('show');

}

//Llena grilla de eventos del paciente
function LlenaGrilla(datos, grilla, columnas, tabla, hide) {

    try {

        if ($(grilla).hasClass('hide')) {
            $(grilla).removeClass('hide');
        }
        var table =
            $(grilla).DataTable({
                order: [],
                data: datos,
                columns: columnas,
                "columnDefs": [
                    {
                        "targets": -1,
                        "data": null,
                        orderable: false,
                        "render": function (data, type, row, meta) {
                            var fila = meta.row;
                            var botones = "";
                            switch (tabla) {
                                case 'Eventos':
                                    botones =
                                        `<button class='btn btn-default btn-block' data-toggle='collapse' data-target='#pnlBotones${fila}' 
                                        onclick='return false;'>
                                        <span class='fa fa-bars'></span>
                                    </button>
                                    <div id='pnlBotones${fila}' class='collapse'>
                                        <div class='row'>
                                            <div class='col-md-12'>
                                                <button class='btn btn-info btn-block muestraEvento' data-target='#panelBoton'
                                                    data-id='${datos[fila][0]}'>
                                                    <i class='fa fa-search'></i>
                                                </button>
                                                <button class='btn btn-default btn-block ImprimeEvento' style='margin-top:0px !important'
                                                    data-id='${datos[fila][0]}'>
                                                    <i class='fa fa-print'></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>`;
                                    break;
                                case 'ProtocoloOperatorio':
                                    botones =
                                        `<button class='btn btn-info btn-block muestraEvento' data-target='#panelBoton'
                                        data-id='${datos[fila][0]}' onclick="ImprimirDocumento(${datos[fila][0]}, 'ProtocoloOperatorio');" 
                                        data-print='true' data-frame='#frameImprimir'>
                                        <i class='fa fa-print'></i>
                                    </button>`;
                                    break;
                                case 'EPICRISIS':
                                    botones =
                                        `<a data-id='${datos[fila][0]}' class='btn btn-info btn-block' href='#/' 
                                        onclick="ImprimirDocumento(${datos[fila][0]}, 'Epicrisis');" data-print='true' 
                                        data-frame='#frameImprimir'>
                                        <i class='fa fa-print'></i>                                       
                                    </a>`;
                                    break;
                                case 'Pacientes':
                                    botones =
                                        `<a class='btn btn-info btnSeleccionPaciente' data-target='#panelBoton' 
                                        onclick='linkPacienteRut(${datos[fila][0]},"${datos[fila][2]}")'> 
                                        <i class="fas fa-user"></i> Ver
                                    </a>`;
                                    break;
                                case 'Recetas':
                                    botones =
                                        `<a data-id='${datos[fila][0]}' class='btn btn-info btn-block' href='#/' 
                                        onclick="ImprimirDocumento(${datos[fila][0]}, 'Receta');">
                                        <i class='fa fa-print'></i>
                                    </a>`;
                                    break;
                                case 'FormularioIndQx':
                                    botones =
                                        `<a data-id='${datos[fila][0]}' class='btn btn-info btn-block' href='#/' 
                                        onclick='imprimeFormularioIQX(${datos[fila][0]});'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                                    break;
                                case 'Hospitalizacion':
                                    botones =
                                        `<a id='imprimirHOS_${fila}' data-id='${datos[fila][0]}' class='btn btn-info btn-block load-click' href='#/' onclick='imprimirHOS(this);'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                                    break;
                                case 'Biopsia':
                                    botones =
                                        `<a id='imprimirBIO_${fila}' data-id='${datos[fila][0]}' class='btn btn-info btn-block' href='#/' onclick='imprimirBIO(this);'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                                    break;
                                case 'IPD':
                                    botones =
                                        `<a id='imprimirIPD_${fila}' data-id='${datos[fila][0]}' class='btn btn-info btn-block load-click' href='#/' onclick='imprimirIPD(this);'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                                    break;
                                case 'Inteconsulta':
                                    botones =
                                        `<a id='imprimirIC_${fila}' data-id='${datos[fila][0]}' class='btn btn-info btn-block load-click' href='#/' onclick='imprimirIC(this);'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                                    break;
                                case 'HojaEvolucion':
                                    botones =
                                        `<a id='imprimirHE_${fila}' data-id='${datos[fila][0]}' class='btn btn-info btn-block load-click' href='#/' onclick='imprimirHE(this);'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                                    break;
                                case 'URGENCIA':
                                    botones =
                                        `<a id='urgencia_${fila}' data-id='${datos[fila][0]}' class='btn btn-info btn-block load-click' href='#/' onclick='imprimirURG(this);'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                                    break;
                                case 'EventoAdverso':
                                    botones =
                                        `<a data-id='${datos[fila][0]}' class='btn btn-info btn-block' href='#/' onclick='ImprimirDocumento(${datos[fila][0]},'EventoAdverso');'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                                    break;
                            }
                            return botones;
                        }
                    }
                ],
                responsive: {
                    details: {
                        renderer: function (api, rowIdx, columns) {
                            var data = $.map(columns, function (col, i) {
                                var toReturn = '';
                                if (col.hidden) {
                                    toReturn = toReturn.concat('<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">');
                                    if (col.title == '') {
                                        toReturn = toReturn.concat('<td>' + col.data + '</td>');
                                        toReturn = toReturn.concat('<td></td>');
                                    }
                                    else {
                                        toReturn = toReturn.concat('<td>' + col.title + ':' + '</td> ');
                                        toReturn = toReturn.concat('<td>' + col.data + '</td>');
                                    }
                                    toReturn = toReturn.concat('</tr>');
                                }
                                return toReturn;
                            }).join('');

                            return data ?
                                $('<table/>').append(data) :
                                false;
                        }
                    }
                },
                buttons: [
                    'copy', 'excel', 'pdf'
                ],
                "bDestroy": true
            });
        if (hide != null) {
            table.column(hide).visible(false);
        }
        //  event.preventDefault();
        //var tabla =  $(grilla).DataTable();
        //var datosgrilla = tabla.buttons.exportData();
    } catch (error) { console.log('error: ' + error.message) }
}

function pdfGrilla(grilla) {

    var doc = new jsPDF('l', 'mm', [297, 210]);
    var elem = document.getElementById("tblEventos");
    var res = doc.autoTableHtmlToJson(elem);
    var acreditacion = null;
    var minsal = null;
    var logohosp = null;

    imgToBase64("", function (base64) {
        acreditacion = base64;
    });
    imgToBase64("", function (base64) {
        minsal = base64;
    });
    imgToBase64("", function (base64) {
        logohosp = base64
    });
    doc.text("Eventos Clínicos Historicos", 110, 30);

    doc.setFontSize(12);
    doc.text("Paciente: " + Mayus($('#txtNombrePaciente').val().toLowerCase()) + " " + Mayus($('#txtApellidoPaciente').val().toLowerCase()) + " " + Mayus($('#txtApellidoMPaciente').val().toLowerCase()), 14, 50);

    doc.autoTable(res.columns, res.data, {
        styles: { cellPadding: 1, fontSize: 8 },
        startY: 60,
        theme: 'grid'
    });
    doc.save('a.pdf');

}

function imgToBase64(src, callback) {
    var outputFormat = src.substr(-3) === 'png' ? 'image/png' : 'image/jpeg';
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function () {
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var dataURL;
        canvas.height = this.naturalHeight;
        canvas.width = this.naturalWidth;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        callback(dataURL);
    };
    img.src = src;
    if (img.complete || img.complete === undefined) {
        img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
        img.src = src;
    }
}
function Mayus(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}
//API GET EVENTOS
function getEventosPaciente() {
    var adataset = [];
    var columnas = [{ title: 'ID EVENTO' }, { title: 'FECHA' }, { title: 'DIAGNOSTICO' }, { title: 'ANAMNESIS' }, { title: 'CAUSA' }, { title: 'TIPO EVENTO' }, { title: '' }];
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "/RCE_Eventos/GEN_idPaciente/" + $("#idPaciente").val(),
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            if ($.isEmptyObject(data)) {
                grillaEventosVacia();
            } else {
                if ($("#btnExportarEvento").hasClass('hide')) {
                    $("#btnExportarEvento").removeClass('hide');
                }
                if (!$('#errGrillaEventos').hasClass('hide')) {
                    $('#errGrillaEventos').addClass('hide');
                }
                $.each(data, function () {
                    adataset.push([this.RCE_idEventos, moment(new Date(this.RCE_fecha_inicioEventos)).format('DD-MM-YYYY'), this.RCE_diagnosticoEventos, this.RCE_anamnesisEventos, this.RCE_causaEventos, this.RCE_descripcionTipo_Evento]);
                });
                LlenaGrilla(adataset, "#tblEventos", columnas, 'Eventos', null);
            }
        }
    });
    event.preventDefault();
}

function grillaEventosVacia() {
    if (!$("#btnExportarEvento").hasClass('hide')) {
        $("#btnExportarEvento").addClass('hide');
    }
    $('#errGrillaEventos').empty();
    $('#errGrillaEventos').append("No existen eventos para este paciente.");
    if ($('#errGrillaEventos').hasClass('hide')) {
        $('#errGrillaEventos').removeClass('hide');
    }
    $('#tblEventos').DataTable().clear().draw();
    $('#tblEventos_wrapper').addClass('hide');
    $('#tblEventos').addClass('hide');
}

//ABRE MODAL NUEVO EVENTO
function showNuevoEvento() {
    $('#txtEventoPacNom').val($('#txtNombrePaciente').val() + ' ' + $('#txtApellidoPaciente').val() + ' ' + $('#txtApellidoMPaciente').val());
    $('#txtEventoPacRut').val($('#txtRutPaciente').val() + '-' + $('#txtRutDigitoPaciente').val());
    $('#mdlEventoNuevo').modal('show');
}

function linkPacienteRut(tipo, rut) {
    getPacienteRut(tipo, rut);
}

//API GET PACIENTE POR RUT
function getPacienteRut(tipo, rut) {
    console.log(tipo)
    limpiaCampos();
    $('#divPacientes').hide();
    rut = (rut.indexOf("-") == -1) ? rut : rut.split("-")[0];

    var url = `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/${tipo}/${rut}`;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                var json = data[0];
                if ($.isEmptyObject(json)) {
                    errorPaciente();
                } else {
                    var valDClinico = null;
                    $.ajax({
                        type: 'GET',
                        url: `${GetWebApiUrl()}GEN_Paciente/${json.GEN_idPaciente}/DatosClinicos`,
                        contentType: "application/json",
                        dataType: "json",
                        success: function (data) {

                            valDClinico = data;

                            $("#idPacienteDClinico").val(valDClinico.GEN_idPaciente_Dclinico);
                            $("#ddlGrupoSanguineoPaciente").val(data.GEN_idGrupo_Sanguineo);

                            if (data.GEN_latexPaciente_Dclinico == "SI")
                                $("#rdoLatexSi").bootstrapSwitch('state', true, true);
                            else if (data.GEN_latexPaciente_Dclinico == "NO")
                                $("#rdoLatexNo").bootstrapSwitch('state', true, true);
                            else
                                $("#rdoLatexSi, #rdoLatexNo").bootstrapSwitch('state', false, true);

                            $("#switchOtrasAlergias").bootstrapSwitch('state', (data.GEN_alergias_detallePaciente_Dclinico != null), true);
                            $("#txtOtrasAlergias").val(data.GEN_alergias_detallePaciente_Dclinico);
                            $("#txtAntecedentesFamiliares").val(data.GEN_antecedentes_familiaresPaciente_Dclinico);
                            $("#txtEnfermedadesHereditarias").val(data.GEN_enfermedades_hereditariasPaciente_Dclinico);
                            $("#txtEnfermedadesPrevias").val(data.GEN_enfermedades_previasPaciente_Dclinico);
                            $("#txtAntecedentesNeonatales").val(data.GEN_antecedentes_neonatalesPaciente_Dclinico);
                            $("#txtMedicacionPrevia").val(data.GEN_medicacion_previaPaciente_Dclinico);
                            $("#txtTratFarmacologico").val(data.GEN_tratamiento_farmacologicoPaciente_Dclinico);
                            $("#txtAntecedentesObstetricos").val(data.GEN_antecedentes_obstetricosPaciente_Dclinico);
                            $("#txtAntecedentesQuirurgicos").val(data.GEN_antecedentes_quirurgicosPaciente_Dclinico);
                            $("#txtDetalleTabaco").val(data.GEN_tabacoPaciente_Dclinico);
                            $("#txtDetalleOH").val(data.GEN_OHPaciente_Dclinico);
                            $("#txtDrogas").val(data.GEN_drogasPaciente_Dclinico);
                            $("#txtHipertension").val(data.GEN_hipertensionPaciente_Dclinico);
                            $("#txtDiabetes").val(data.GEN_diabetes_detallePaciente_Dclinico);
                            $("#txtAsma").val(data.GEN_asmaPaciente_Dclinico);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            $('#idPacienteDClinico').val('0');
                            if (xhr.status == 404) {//no encontró paciente
                                //no se encontraron datos dclinico
                            }
                        }
                    });

                    $('#idPaciente').val(json.GEN_idPaciente);
                    idPac = json.GEN_idPaciente;
                    if (json.GEN_numero_documentoPaciente != null) {
                        $('#txtRutPaciente').val(json.GEN_numero_documentoPaciente);
                    }

                    if (json.GEN_digitoPaciente != null) {
                        $('#txtRutDigitoPaciente').val(json.GEN_digitoPaciente);
                    }

                    $('#ddlTipoIdentificacionPaciente').val(json.GEN_idIdentificacion);

                    ($('#ddlTipoIdentificacionPaciente').val() != '5') ? $("#ddlTipoIdentificacionPaciente option[value='5']").hide() :
                        $("#ddlTipoIdentificacionPaciente option[value='5']").show();

                    $('#txtNuiPaciente').val(json.GEN_nuiPaciente);
                    if (json.GEN_nuiPaciente != null && json.GEN_nuiPaciente != 0 && json.GEN_nuiPaciente.toString().length >= 6) {
                        $("#txtNuiPaciente").attr("disabled", "disabled");
                    } else {
                        $("#txtNuiPaciente").removeAttr("disabled");
                        $("#txtNuiPaciente").removeClass("disabled");
                    }
                       

                    if (json.GEN_nombrePaciente != null) {
                        $('#txtNombrePaciente').val(json.GEN_nombrePaciente);
                    }

                    if (json.GEN_ape_paternoPaciente != null) {
                        $('#txtApellidoPaciente').val(json.GEN_ape_paternoPaciente);
                    }

                    if (json.GEN_ape_maternoPaciente != null) {
                        $('#txtApellidoMPaciente').val(json.GEN_ape_maternoPaciente);
                    }

                    if (json.GEN_fec_nacimientoPaciente != null) {
                        $('#txtFechaNacPaciente').val(moment(new Date(json.GEN_fec_nacimientoPaciente)).format('YYYY-MM-DD'));
                    }

                    if (json.GEN_idNacionalidad != null) {
                        $('#ddlNacionalidadPaciente').val(json.GEN_idNacionalidad);
                    }

                    if (json.GEN_idSexo != null) {
                        $('#ddlSexoPaciente').val(json.GEN_idSexo);
                    }

                    if (json.GEN_idTipo_Genero != null) {
                        $('#ddlGeneroPaciente').val(json.GEN_idTipo_Genero);
                    }

                    if (json.GEN_idPais != null) {
                        $('#ddlPaisPaciente').val(json.GEN_idPais);
                        APIRegion(json.GEN_idPais);
                    }

                    if (json.GEN_idRegion != null) {
                        $('#ddlRegionPaciente').val(json.GEN_idRegion);
                        APIProvincia(json.GEN_idRegion);
                    }

                    if (json.GEN_idProvincia != null) {
                        $('#ddlProvinciaPaciente').val(json.GEN_idProvincia);
                        APICiudad(json.GEN_idProvincia);
                    }

                    if (json.GEN_idCiudad != null) {
                        $('#ddlCiudadPaciente').val(json.GEN_idCiudad);
                    }

                    //              $('#ddlProvinciaPaciente').val(val.GEN_idProvincia);
                    if (json.GEN_dir_callePaciente != null) {
                        $('#txtDireccionPaciente').val(json.GEN_dir_callePaciente);
                    }

                    if (json.GEN_dir_numeroPaciente != null) {
                        $('#txtNumeroDireccionPaciente').val(json.GEN_dir_numeroPaciente);
                    }

                    if (json.GEN_telefonoPaciente != null) {
                        $('#txtTelefonoPaciente').val(json.GEN_telefonoPaciente);
                    }

                    if (json.GEN_otros_fonosPaciente != null) {
                        $('#txtOtroTelefonoPaciente').val(json.GEN_otros_fonosPaciente);
                    }

                    if (json.GEN_emailPaciente != null) {
                        $('#txtEmailPaciente').val(json.GEN_emailPaciente);
                    }

                    if (json.GEN_idTipos_Domicilio == 2) {
                        $('#switchRural').bootstrapSwitch('state', true, true);
                    } else {
                        $('#switchRural').bootstrapSwitch('state', false, true);
                    }

                    if (json.GEN_idPueblo_Originario != null) {
                        $('#ddlPuebloOrigPaciente').val(json.GEN_idPueblo_Originario);
                    }

                    if (json.GEN_idTipo_Escolaridad != null) {
                        $('#ddlNivelEscolarPaciente').val(json.GEN_idTipo_Escolaridad);
                    }

                    if (json.GEN_idEstado_Conyugal != null) {
                        $('#ddlEstadoConyuPaciente').val(json.GEN_idEstado_Conyugal);
                    }

                    if (json.GEN_fec_actualizacionPaciente != null) {
                        $('#txtFechaActualizacionPaciente').val(moment(new Date(json.GEN_fec_actualizacionPaciente)).format('DD-MM-YYYY'));
                    }

                    if (json.GEN_fecha_fallecimientoPaciente != null) {
                        $('#txtFechaFallecimiento').val(moment(new Date(json.GEN_fecha_fallecimientoPaciente)).format('YYYY-MM-DD'));
                        $('#txtFechaFallecimiento').removeClass("hide");
                        $('#lblFechaFallecimientoPacientePill').text(moment(new Date(json.GEN_fecha_fallecimientoPaciente)).format('DD-MM-YYYY'));
                    } else {
                        $('#txtFechaFallecimiento').addClass("hide");
                    }

                    if (json.GEN_responde_nombrePaciente != null) {
                        //$('#lblRespondePaciente').addClass('active');
                        $('#txtRespondePaciente').val(json.GEN_responde_nombrePaciente);
                    }

                    //DATOS PREVISIONALES
                    if (json.GEN_idPrevision != null) {
                        $('#ddlAseguradora').val(json.GEN_idPrevision);
                        if (json.GEN_idPrevision_Tramo != null) {
                            APITramo(json.GEN_idPrevision, json.GEN_idPrevision_Tramo);
                        }
                    }

                    if (json.GEN_praisPaciente != null) {
                        if (json.GEN_praisPaciente == 'SI') {
                            $('#switchPrais').bootstrapSwitch('state', true, true);
                        } else {
                            $('#switchPrais').bootstrapSwitch('state', false, true);
                        }
                    }
                    if ($('#txtFechaNacPaciente').val() != "")
                        $('#txtEdadPaciente').val(CalcularEdad($('#txtFechaNacPaciente').val()).edad);

                    $(".nav-paciente .nav-link").removeClass("disabled");

                    var NombreCompleto = $('#txtNombrePaciente').val() + ' ' + $('#txtApellidoPaciente').val() + ' ' + $('#txtApellidoMPaciente').val();
                    var documento = $('#txtRutPaciente').val() + (($('#txtRutDigitoPaciente').val() != '') ? '-' + $('#txtRutDigitoPaciente').val() : '');

                    $('#lblNombrePacientePill').empty();
                    $('#lblRutPacientePill').empty();
                    $('#lblNUIPacientePill').empty();
                    $('#lblNombrePacientePill').append(firstUpper(NombreCompleto));
                    $('#lblRutPacientePill').append(documento);
                    $('#lblNUIPacientePill').append(`${(json.GEN_nuiPaciente !== undefined && json.GEN_nuiPaciente !== null) ? `Número ubicación interna: ${json.GEN_nuiPaciente}` : "No posee NUI"}`);
                    $('#lblPaciente').show();

                    if ($('#txtFechaFallecimiento').val() != "") { //fecha fallecimiento
                        fadeIn($('#lblPacienteFallecido'));
                    }

                    $('#hdfEditable').val('NO');

                    //'habilitar' botones
                    $('#btnPaciente').removeClass('bg-secondary disabled').addClass('bg-info');
                    $('#btnDatosClinicos').removeClass('bg-secondary disabled').addClass('bg-info');
                    $('#btnDatosPrev').removeClass('bg-secondary disabled').addClass('bg-info');
                    $('#btnRegistroClinico').removeClass('bg-secondary disabled').addClass('bg-info');

                    $('html, body').stop().animate({ scrollTop: 0 }, 500);

                }
            } catch (err) {
                console.log("Error: " + err.message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr.status == 404) {//no encontró paciente
                toastr.error("No se han encontrado pacientes con los campos seleccionados");
            }
        }
    });
}

//LIMPIA CAMPOS
function limpiaCampos() {

    $('#idPaciente').val('');
    $('#txtRutPaciente').val('');
    $('#txtRutDigitoPaciente').val('');
    $('#ddlTipoIdentificacionPaciente').val('');
    $('#txtNuiPaciente').val('');
    $('#txtNombrePaciente').val('');
    $('#txtApellidoPaciente').val('');
    $('#txtApellidoMPaciente').val('');
    $('#txtFechaNacPaciente').val('');
    $('#ddlSexoPaciente').val('0');
    $('#ddlGeneroPaciente').val('0');
    $('#ddlNacionalidadPaciente').val('0');
    $('#ddlRegionPaciente').val('0');
    $('#ddlProvinciaPaciente').val('0');
    $('#txtDireccionPaciente').val('');
    $('#txtNumeroDireccionPaciente').val('');
    $('#txtTelefonoPaciente').val('');
    $('#txtOtroTelefonoPaciente').val('');
    $('#txtEmailPaciente').val('');
    $('#txtRespondePaciente').val('');
    $('#ddlPuebloOrigPaciente').val('0');
    $('#ddlNivelEscolarPaciente').val('0');
    $('#ddlEstadoConyuPaciente').val('0');
    $('#txtFechaActualizacionPaciente').val('');
    $('#txtFechaFallecimiento').val('');
    $('#ddlGrupoSanguineoPaciente').val('0');
    $('#txtOtrasAlergias').val('');
    $('#txtAntecedentesFamiliares').val('');
    $('#txtEnfermedadesHereditarias').val('');
    $('#txtEnfermedadesPrevias').val('');
    $('#txtAntecedentesNeonatales').val('');
    $('#txtMedicacionPrevia').val('');
    $('#txtTratFarmacologico').val('');
    $('#txtAntecedentesObstetricos').val('');
    $('#txtAntecedentesQuirurgicos').val('');
    $('#ddlAseguradora').val('0');
    $('#ddlTramo').val('0');
    $('#switchPrais').bootstrapSwitch('state', false);
    $('#switchRural').bootstrapSwitch('state', false);
    $('#switchOtrasAlergias').bootstrapSwitch('state', false);
    $('#rdoLatexSi').bootstrapSwitch('state', false);
    $('#rdoLatexNo').bootstrapSwitch('state', false);
    $('#hdfEditable').val("NO");
    $('#lblPacienteFallecido').hide();
    $('#lblFechaFallecimientoPacientePill').text('');

    // HÁBITOS
    $("#txtDetalleTabaco").val("");
    $("#txtDetalleOH").val("");
    $("#txtDrogas").val('');
    $("#txtHipertension").val('');
    $("#txtDiabetes").val('');
    $("#txtAsma").val('');

    $('#lblPaciente').hide();
    BloquearMenu(true);
    limpiarGrillas();
}

//JSON PACIENTE
function JSONPaciente() {

    var GEN_Paciente = {};

    GEN_Paciente.GEN_idPaciente = $('#idPaciente').val();
    GEN_Paciente.GEN_numero_documentoPaciente = valCampo($('#txtRutPaciente').val());
    GEN_Paciente.GEN_digitoPaciente = valCampo($('#txtRutDigitoPaciente').val());
    GEN_Paciente.GEN_nombrePaciente = valCampo($('#txtNombrePaciente').val());
    GEN_Paciente.GEN_ape_paternoPaciente = valCampo($('#txtApellidoPaciente').val());
    GEN_Paciente.GEN_ape_maternoPaciente = valCampo($('#txtApellidoMPaciente').val());
    GEN_Paciente.GEN_dir_callePaciente = valCampo($('#txtDireccionPaciente').val());
    GEN_Paciente.GEN_dir_numeroPaciente = valCampo($('#txtNumeroDireccionPaciente').val());
    GEN_Paciente.GEN_idTipos_Domicilio = $('#switchRural').bootstrapSwitch('state') ? '2' : '1';
    GEN_Paciente.GEN_idPais = ($('#ddlPaisPaciente').val()) != '0' ? $('#ddlPaisPaciente').val() : null;
    GEN_Paciente.GEN_idRegion = ($('#ddlRegionPaciente').val()) != '0' ? $('#ddlRegionPaciente').val() : null;
    GEN_Paciente.GEN_idComuna = null;
    GEN_Paciente.GEN_idProvincia = ($('#ddlProvinciaPaciente').val()) != '0' ? $('#ddlProvinciaPaciente').val() : null;
    GEN_Paciente.GEN_idCiudad = ($('#ddlCiudadPaciente').val()) != '0' ? $('#ddlCiudadPaciente').val() : null;
    GEN_Paciente.GEN_telefonoPaciente = valCampo($('#txtTelefonoPaciente').val());
    GEN_Paciente.GEN_idTipo_Genero = valCampo($('#ddlGeneroPaciente').val());
    GEN_Paciente.GEN_idSexo = valCampo($('#ddlSexoPaciente').val());
    GEN_Paciente.GEN_fec_nacimientoPaciente = valCampo($('#txtFechaNacPaciente').val());
    GEN_Paciente.GEN_otros_fonosPaciente = valCampo($('#txtOtroTelefonoPaciente').val());
    GEN_Paciente.GEN_emailPaciente = valCampo($('#txtEmailPaciente').val());
    GEN_Paciente.GEN_idPrevision = ($('#ddlAseguradora').val()) != '0' ? $('#ddlAseguradora').val() : null;
    GEN_Paciente.GEN_idPrevision_Tramo = ($('#ddlTramo').val()) != '0' ? $('#ddlTramo').val() : null;
    GEN_Paciente.GEN_nuiPaciente = valCampo($('#txtNuiPaciente').val());
    GEN_Paciente.GEN_idIdentificacion = valCampo($('#ddlTipoIdentificacionPaciente').val());
    GEN_Paciente.GEN_PraisPaciente = $('#switchPrais').bootstrapSwitch('state') ? 'SI' : 'NO';
    GEN_Paciente.GEN_idEstado_Conyugal = ($('#ddlEstadoConyuPaciente').val()) != '0' ? $('#ddlEstadoConyuPaciente').val() : null;
    GEN_Paciente.GEN_idPueblo_Originario = ($('#ddlPuebloOrigPaciente').val()) != '0' ? $('#ddlPuebloOrigPaciente').val() : null;
    GEN_Paciente.GEN_idNacionalidad = ($('#ddlNacionalidadPaciente').val()) != '0' ? $('#ddlNacionalidadPaciente').val() : null;
    GEN_Paciente.GEN_idTipo_Escolaridad = ($('#ddlNivelEscolarPaciente').val()) != '0' ? $('#ddlNivelEscolarPaciente').val() : null;
    GEN_Paciente.GEN_estadoPaciente = 'Activo'
    GEN_Paciente.GEN_fecha_fallecimientoPaciente = valCampo($('#txtFechaFallecimiento').val());
    GEN_Paciente.GEN_responde_nombrePaciente = valCampo($('#txtRespondePaciente').val());

    //console.log(JSON.stringify(GEN_Paciente));
    return GEN_Paciente;

}
function JSONPacienteDClinico() {

    var GEN_PacienteDClinico = {};
    GEN_PacienteDClinico["GEN_idPaciente"] = parseInt($('#idPaciente').val());
    GEN_PacienteDClinico["GEN_idGrupo_Sanguineo"] = valCampo(parseInt($('#ddlGrupoSanguineoPaciente').val()));
    if ($('#switchOtrasAlergias').bootstrapSwitch('state')) {
        GEN_PacienteDClinico["GEN_alergiasPaciente_Dclinico"] = 'SI';
    } else {
        GEN_PacienteDClinico["GEN_alergiasPaciente_Dclinico"] = 'NO';
    }
    GEN_PacienteDClinico["GEN_alergias_detallePaciente_Dclinico"] = valCampo($('#txtOtrasAlergias').val());
    //CARDIOPATIA!<-
    GEN_PacienteDClinico["GEN_cardiopatiaPaciente_Dclinico"] = null
    GEN_PacienteDClinico["GEN_cardiopatia_detallePaciente_Dclinico"] = null
    //FIN
    //hta!<-
    GEN_PacienteDClinico["GEN_htaPaciente_Dclinico"] = null;
    GEN_PacienteDClinico["GEN_hta_detallePaciente_Dclinico"] = null;
    //FIN
    //DIABETES!<-
    GEN_PacienteDClinico["GEN_diabetesPaciente_Dclinico"] = null;
    GEN_PacienteDClinico["GEN_diabetes_detallePaciente_Dclinico"] = null;
    //FIN  
    if ($('#txtAntecedentesQuirurgicos').val() != '') {
        GEN_PacienteDClinico["GEN_antec_qxPaciente_Dclinico"] = 'SI';
    } else {
        GEN_PacienteDClinico["GEN_antec_qxPaciente_Dclinico"] = 'NO';
    }
    GEN_PacienteDClinico["GEN_antec_qx_detallePaciente_Dclinico"] = valCampo($('#txtAntecedentesQuirurgicos').val());
    //HOSPITALIZACIÓN PREVIA!<-
    GEN_PacienteDClinico["GEN_hosp_anterPaciente_Dclinico"] = null;
    GEN_PacienteDClinico["GEN_hosp_anter_detallePaciente_Dclinico"] = null;
    //FIN
    if ($('#rdoLatexSi').bootstrapSwitch('state')) {
        GEN_PacienteDClinico["GEN_latexPaciente_Dclinico"] = 'SI';
    } else if ($('#rdoLatexNo').bootstrapSwitch('state')) {
        GEN_PacienteDClinico["GEN_latexPaciente_Dclinico"] = 'NO';
    } else {
        GEN_PacienteDClinico["GEN_latexPaciente_Dclinico"] = null;
    }
    GEN_PacienteDClinico["GEN_tratamiento_farmacologicoPaciente_Dclinico"] = valCampo($('#txtTratFarmacologico').val());
    GEN_PacienteDClinico["GEN_antecedentes_familiaresPaciente_Dclinico"] = valCampo($('#txtAntecedentesFamiliares').val());
    GEN_PacienteDClinico["GEN_enfermedades_hereditariasPaciente_Dclinico"] = valCampo($('#txtEnfermedadesHereditarias').val());
    //ENFERMEDADES CRONICAS!<-
    GEN_PacienteDClinico["GEN_enfermedades_cronicasPaciente_Dclinico"] = null;
    //FIN
    GEN_PacienteDClinico["GEN_medicacion_previaPaciente_Dclinico"] = valCampo($('#txtMedicacionPrevia').val());
    GEN_PacienteDClinico["GEN_antecedentes_quirurgicosPaciente_Dclinico"] = valCampo($('#txtAntecedentesQuirurgicos').val());
    GEN_PacienteDClinico["GEN_antecedentes_obstetricosPaciente_Dclinico"] = valCampo($('#txtAntecedentesObstetricos').val());

    //ANTECEDENTES GINECOLOGICOS!<-
    GEN_PacienteDClinico["GEN_antecentes_ginecologicosPaciente_Dclinico"] = null;
    //FIN
    GEN_PacienteDClinico["GEN_antecedentes_neonatalesPaciente_Dclinico"] = valCampo($('#txtAntecedentesNeonatales').val());
    GEN_PacienteDClinico["GEN_enfermedades_previasPaciente_Dclinico"] = valCampo($('#txtEnfermedadesPrevias').val());

    // HÁBITOS
    GEN_PacienteDClinico["GEN_tabacoPaciente_Dclinico"] = valCampo($('#txtDetalleTabaco').val());
    GEN_PacienteDClinico["GEN_OHPaciente_Dclinico"] = valCampo($('#txtDetalleOH').val());
    GEN_PacienteDClinico["GEN_drogasPaciente_Dclinico"] = valCampo($("#txtDrogas").val());
    GEN_PacienteDClinico["GEN_hipertensionPaciente_Dclinico"] = valCampo($("#txtHipertension").val());
    GEN_PacienteDClinico["GEN_diabetes_detallePaciente_Dclinico"] = valCampo($("#txtDiabetes").val());
    GEN_PacienteDClinico["GEN_asmaPaciente_Dclinico"] = valCampo($("#txtAsma").val());

    return GEN_PacienteDClinico;

}
//API GET PACIENTE POR FILTRO
function getPacienteFiltro(nombre, apep, apem, nui) {

    var adataset = [];
    var columnas = [
        { title: 'ID_IDENTIFICACIÓN' },
        { title: 'IDENTIFICACIÓN' },
        { title: 'FICHA CLÍNICA' },
        { title: 'NOMBRE' },
        { title: 'APELLIDO' },
        { title: 'SEGUNDO APELLIDO' },
        { title: 'NUI' },
        { title: '' }];

    let filtros = "";
    if (nombre != "")
        filtros = `nombrePaciente=${nombre}`;

    if (apep != "")
        filtros += (filtros == "") ? `ape_paternoPaciente=${apep}` : `&ape_paternoPaciente=${apep}`;

    if (apem != "")
        filtros += (filtros == "") ? `ape_maternoPaciente=${apem}` : `&ape_maternoPaciente=${apem}`;

    if (nui != "")
        filtros += (filtros == "") ? `nuiPaciente=${nui}` : `&nuiPaciente=${nui}`;

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Paciente/Buscar?${filtros}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            if ($.isEmptyObject(data)) {
                errorPaciente();
            } else {

                $('#divPacientes').show();
                $.each(data, function () {
                    adataset.push([
                        this.IdIdentificacion,
                        this.NombreIdentificacion,
                        this.NumeroDocumento,
                        this.Nombre,
                        this.ApellidoPaterno,
                        this.ApellidoMaterno,
                        this.NUI]);
                });
                LlenaGrilla(adataset, "#tblPacientes", columnas, 'Pacientes', 0);
            }
        }
    });
}

function BloquearMenu(esBloqueo) {

    $("#btnPaciente, #btnDatosClinicos, #btnDatosPrev, #btnRegistroClinico")
        .removeAttr("disabled")
        .removeClass("bg-secondary bg-info disabled");
    $("#btnPaciente, #btnDatosClinicos, #btnDatosPrev, #btnRegistroClinico")
        .addClass((esBloqueo) ? "bg-secondary disabled" : "bg-info");

}

//NO SE ENCUENTRA PACIENTE
function errorPaciente() {
    //$(".nav-link").addClass("disabled");
    limpiaCampos();
    toastr.error('No existen pacientes con los datos ingresados');
    $('#divPacientes').hide();
}
//
function getFormulariosIndQx() {

    let idPaciente = $('#idPaciente').val();
    let adataset = [];
    let columnas = [{ title: 'FOLIO' }, { title: 'DIAGNOSTICO' }, { title: 'PRESTACIÓN' }, { title: 'GES' }, { title: 'UCA' }, { title: 'APTO' }, { title: 'ESTADO' }, { title: 'EVENTO' }, { title: '' }];
    let url = `${GetWebApiUrl()}PAB_Formulario_Ind_Qx/buscar?idPaciente=${idPaciente}`;
    //buscar?idpaciente
    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Formularios de Indicación Quirúrgica")
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([this.Id,
                            this.Diagnostico,
                            this.Prestacion,
                            this.Ges,
                            this.Uca,
                            this.UcaApto != null ? this.UcaApto : ' - ',
                            this.IdTipoEstado,
                            this.IdEvento]);
                    });
                    LlenaGrilla(adataset, '#tblFormularioIndQx', columnas, 'FormularioIndQx', 7);
                }
            } catch (err) {
                console.log("error:" + err.message);
            }
        }
    });
}
function getProtocolosOperatorios() {
    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var columnas = [{ title: 'ID' }, { title: 'ÁMBITO' }, { title: 'DIAGNÓSTICO' }, { title: 'CIRUGÍA' }, { title: 'FECHA' }, { title: 'EVENTO' }, { title: '' }];
    var url = `${GetWebApiUrl()}PAB_Protocolo_Operatorio/buscar?idPaciente=${idPaciente}`;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Protocolos Operatorios");
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([
                            this.Id,
                            this.Ambito,
                            this.Diagnostico,
                            this.OperacionRealizada,
                            (this.Fecha != null) ? moment(new Date(this.Fecha)).format('DD-MM-YYYY') : '',
                            this.IdEvento ?? "N/A"]);
                    });
                    LlenaGrilla(adataset, '#tblProtocoloOperatorio', columnas, 'ProtocoloOperatorio', 7);
                }
            } catch (err) {
                console.log("error:" + err.message);
            }
        }
    });
}

function imprimeFormularioIQX(id) {
    var login;
    var pass;

    var json = GetLoginPass();
    login = json.login;
    pass = json.pass;

    //concatena el parametro a enviar al sistema de protocolo
    txt = login + '|' + pass + '|' + id;

    var txtEncrypt = GetTextoEncriptado(txt);
    window.open('http://10.6.180.236/PABELLON/IMP_Formulario.aspx?c=' + txtEncrypt, '_blank');
    return false;
}

function ImprimirDocumento(id, tipoDocumento) {

    $.ajax({
        cache: false,
        type: 'GET',
        url: `${GetWebApiUrl()}hl7/Patient/${tipoDocumento}/${id}/Imprimir`,
        contentType: false,
        processData: false,
        xhrFields: {
            responseType: 'blob'
        },
        success: function (response, status, xhr) {

            try {

                var blob = new Blob([response], { type: 'application/pdf' });
                var URL = window.URL || window.webkitURL;
                var downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);

            } catch (ex) {
                console.log(ex);
            }

        }
    });
}

function getHospitalizaciones() {
    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var columnas = [{ title: 'ID' }, { title: 'FECHA INGRESO' }, { title: 'FECHA EGRESO' }, { title: 'EVENTO' }, { title: 'DIAGNÓSTICO' }, { title: 'PROCEDENCIA' }, { title: 'ÁMBITO' }, { title: '' }];
    var url = `${GetWebApiUrl()}HOS_Hospitalizacion/buscar?idPaciente=${idPaciente}`;
    
    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            console.log(data)
            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Hospitalización");
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([
                            this.Id,
                            (this.Fecha != null) ? moment(new Date(this.Fecha)).format('DD-MM-YYYY') : '',
                            (this.FechaEgreso != null) ? moment(new Date(this.FechaEgreso)).format('DD-MM-YYYY') : '<span style="color:gray">No se ha egresado</span>',
                            this.IdEvento,
                            (this.Epicrisis != null) ? this.Epicrisis.DiagnosticoPrincipal.split('\n').join('<br>') : '<span style="color:gray">No registra epicrisis</span>',
                            this.ProcedenciaIngreso,
                            this.Ambito.Valor]);
                    });
                    LlenaGrilla(adataset, '#tblHospitalizacion', columnas, 'Hospitalizacion', 3);
                }
            } catch (err) {
                console.log("error:" + err.message);
            }
        }
    });
}
function mostrarPillSinDatosRegistroClinico(modulo) {
    $('#alertRegistroClinico').empty();
    $('#alertRegistroClinico').append("No existe Información de " + modulo);
    $('#alertRegistroClinico').show();
}
function getRecetas() {

    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var columnas = [{ title: 'ID' }, { title: 'FECHA' }, { title: 'DIAGNOSTICO' }, { title: '' }];
    var url = `${GetWebApiUrl()}HOS_Receta_Indicaciones/Paciente/${idPaciente}`;
    //console.log(url);

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Recetas");
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([this.HOS_idReceta_Indicaciones, (this.HOS_fecha_recetaReceta_Indicaciones != null) ? moment(new Date(this.HOS_fecha_recetaReceta_Indicaciones)).format('DD-MM-YYYY') : '', SaltoDeLinea(this.HOS_diagnostico_principalReceta_Indicaciones, 6)]);
                    });

                    LlenaGrilla(adataset, '#tblReceta', columnas, 'Recetas', null);
                }
            } catch (err) {
                console.log("error:" + err.message);
            }
        }
    });
}
function getRegistrosBiopsias() {
    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var columnas = [{ title: 'ID' }, { title: 'N° REGISTRO' }, { title: 'FECHA RECEPCION' }, { title: 'ORGANO' }, { title: 'ESTADO' }, { title: '' }];
    var url = `${GetWebApiUrl()}ANA_Registro_Biopsias/Buscar?idPaciente=${idPaciente}`;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Biopsias");
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([this.Id, this.Numero, (this.FechaRecepcion != null) ? moment(new Date(this.FechaRecepcion)).format('DD-MM-YYYY') : '', this.OrganoBiopsia.toString(), this.NombreTipoEstadoSistemas]);
                    });
                    LlenaGrilla(adataset, '#tblBiopsia', columnas, 'Biopsia', null);
                }
            } catch (err) {
                console.log("error:" + err.message);
            }
        }
    });
}
function getAtencionesUrgencia() {

    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var columnas = [{ title: 'ID' }, { title: 'FECHA' }, { title: 'HORA' }, { title: 'MOTIVO' }, { title: '' }];
    var url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/Paciente/${idPaciente}`;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Urgencias")
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([
                            this.IdAtencion,
                            (this.Fecha != null) ? moment(new Date(this.Fecha)).format('DD-MM-YYYY') : '<span style="color:gray">No se registra fecha</span>',
                            (this.Fecha != null) ? moment(new Date(this.Fecha)).format('HH:mm') : '<span style="color:gray">No se registra hora</span>',
                            this.MotivoConsulta
                        ]);
                    });
                    LlenaGrilla(adataset, '#tblUrgencia', columnas, 'URGENCIA', 4);
                }
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });
}

function getEpicrisis() {
    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var columnas = [{ title: 'ID' }, { title: 'FECHA INGRESO' }, { title: 'FECHA EGRESO' }, { title: 'DIAGNOSTICO' }, { title: '' }];
    var url = `${GetWebApiUrl()}HOS_Epicrisis/Paciente/${idPaciente}`;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Epicrisis");
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([this.HOS_idEpicrisis, (this.HOS_fecha_ingresoEpicrisis != null) ? moment(new Date(this.HOS_fecha_ingresoEpicrisis)).format('DD-MM-YYYY') : '', (this.HOS_fecha_egresoEpicrisis != null) ? moment(new Date(this.HOS_fecha_egresoEpicrisis)).format('DD-MM-YYYY') : '', this.HOS_diagnostico_principalEpicrisis.split('\n').join('<br>')]);
                    });
                    LlenaGrilla(adataset, '#tblEpicrisis', columnas, 'EPICRISIS', null);
                }
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });

}

function getIPD() {

    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var columnas = [{ title: 'ID' }, { title: 'ÁMBITO' }, { title: 'PATOLOGÍA GES' }, { title: 'DIAGNÓSTICO' }, { title: 'FECHA' }, { title: '' }];
    var url = `${GetWebApiUrl()}RCE_IPD/Paciente/${idPaciente}`;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("IPD")
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([this.RCE_idIPD, this.GEN_nombreAmbito, (this.RCE_descripcion_Patologia_GES != null) ? this.RCE_descripcion_Patologia_GES : '<span style="color:gray">No registra descripción de Patología</span>', this.RCE_diagnosticoIPD, moment(new Date(this.RCE_fechaIPD)).format('DD-MM-YYYY')]);
                    });
                    LlenaGrilla(adataset, '#tblIPD', columnas, 'IPD', null);
                }
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });

}

function getHojasEvolucion() {
    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var columnas = [{ title: 'ID' }, { title: 'FECHA' }, { title: 'ESTADO' }, { title: '' }];
    var url = `${GetWebApiUrl()}RCE_Hoja_Evolucion/Paciente/${idPaciente}`;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Hoja de Evolución");
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([this.RCE_idHoja_Evolucion, this.RCE_fechaHoja_Evolucion, this.GEN_nombreTipo_Estados_Sistemas]);
                    });
                    LlenaGrilla(adataset, '#tblHojaEvolucion', columnas, 'HojaEvolucion', null);
                }
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });
}

function getInterconsultas() {

    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var columnas = [{ title: 'ID' }, { title: 'DIAGNOSTICO' }, { title: 'FECHA' }, { title: 'ESPECIALIDAD ORIGEN' }, { title: 'ESPECIALIDAD DESTINO' }, { title: '' }];
    var url = `${GetWebApiUrl()}RCE_Interconsulta/Paciente/${idPaciente}`;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Interconsulta");
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([this.RCE_idInterconsulta, this.RCE_diagnosticoInterconsulta, this.RCE_fechaInterconsulta, this.GEN_nombreEspecialidadOrigen, this.GEN_nombreEspecialidadDestino]);
                    });
                    LlenaGrilla(adataset, '#tblInterconsulta', columnas, 'Inteconsulta', null);
                }
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });

}

function abrirDoc(e) {

    var ruta = $(e).data('doc');
    var url = `http://10.6.180.186/laboratorio/infinity/${ruta}`;
    window.open(url, '_blank');

}

function verDocumentosLab(e) {
    var id = $(e).data('id');
    var rut = $('#lblRutPacientePill').text();

    var adataset = [];
    $.ajax({
        type: "GET",
        url: `http://10.6.180.186/laboratorio/getCasos.php?sRut=${rut}&sSampleID={id}`,
        contentType: "application/json",
        async: true,
        dataType: "json",        
        success: function (data) {
            var json = JSON.parse(data.d);
            $.each(json, function (i, r) {
                adataset.push([
                    r.id,
                    r.nombre,
                    r.fecha,
                    r.sistema
                ]);
            });
        }
    });

    $('#tblDocumentosLab').addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: "ID" },
            { title: "Nombre archivo" },
            { title: "Fecha archivo" },
            { title: "Sistema" },
            { title: "estado" }
        ],
        columnDefs: [
            { targets: [0, 4], searchable: false, visible: false },
            {
                targets: 1,
                data: null,
                orderable: false,
                render: function (data, type, row, meta) {
                    var fila = meta.row;
                    return '<a href="#/" data-doc="' + adataset[fila][1] + '" onclick="abrirDoc(this)" style="color:#0000ff;">' + adataset[fila][1] + '</a>';
                }
            },
        ],
        bDestroy: true
    });
    $('#mdlDocumentosLab').modal('show');
}

function getLaboratorio() {

    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var sUsuario;
    var sPass;
    var sFecha;

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Usuarios/Login`,
        async: false,
        success: function (data) {
            sUsuario = data[0].GEN_loginUsuarios;
            sPass = data[0].GEN_claveUsuarios;
        }
    });
    sFecha = GetFechaActual();
    var s = `${sUsuario}|${sPass}|${moment(sFecha).add(1, 'hours').format('YYYY-MM-DD HH:mm:ss')}|14`
    var sUrl = `http://10.6.180.236/LABWS/Lab.asmx/Orden?sRut=${$('#txtRutPaciente').val()}-${ObtenerVerificador($("#txtRutPaciente").val())}&sFecha1=2000-01-01&sFecha2=2030-01-01&sToken=${GetTextoEncriptado(s)}`

    $.ajax({
        type: 'GET',
        url: sUrl,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        dataType: 'xml',
        async: false,
        success: function (response) {
            var $xml = $(response);
            var $orderData = $xml.find('OrderData');
            $orderData.each(function () {
                var g = $(this).find('GroupList');
                if (g.length > 0) {
                    var sSampleID = $(this).find('SampleID').text();
                    var sOrderID = $(this).find('InternalOrderID').text();
                    var sSampleFecha = $(this).find('RegisterDate').text();
                    var dFecha = $(this).find('RegisterDate').text();
                    var dHora = $(this).find('RegisterHour').text();
                    var dFechaOrden = moment(dFecha + " " + dHora);
                    var sSolicitado = '';
                    var sProcedencia = '';
                    var sHorario = '';
                    var iGroupID = $(g).find('GroupID').text();

                    var $demographic = $(this).find('Demographics');
                    if ($demographic.length > 0) {
                        var $demographics = $demographic.find('Demographic');

                        $demographics.each(function () {
                            var internalID = parseInt($(this).find('InternalDemographicID').text());
                            var dCurrentValue = $(this).find('DemographicCurrentValue').text();
                            switch (internalID) {
                                case 26: sProcedencia = dCurrentValue; break;
                                case 48: sHorario = dCurrentValue; break;
                                case 8: sSolicitado = dCurrentValue; break;
                                default: break;
                            }
                        });
                    }
                    adataset.push([sSampleID, sOrderID, sSampleFecha, moment(dFechaOrden).format('DD-MM-YYYY HH:mm:ss'), sSolicitado, sProcedencia, sHorario, iGroupID, '']);
                }
            });

            if (adataset.length > 0)
                $('#divLeyendaMicro').show();
            else
                $('#divLeyendaMicro').hide();

            $('#alertRegistroClinico').hide();
            $('#tblLaboratorio').addClass('nowrap').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [2, 7], visible: false, searchable: false },
                        {
                            targets: 8,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var fila = meta.row;

                                return '<a class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="verDocumentosLab(this)" >Ver documentos</a>';
                            }
                        },
                    ],
                columns: [
                    { title: 'Id petición' },
                    { title: 'Id orden' },
                    { title: 'sSampleFecha' },
                    { title: 'Fecha orden' },
                    { title: 'Solicitado por' },
                    { title: 'Procedencia' },
                    { title: 'Rutina/Urgencia' },
                    { title: 'iGroupID' },
                    { title: '' }
                ],
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    if (aData[7].trim() == '-1')
                        $('td', nRow).css('background-color', '#eeffdd');
                },
                bDestroy: true
            });
        },
        error: function (error) {
            console.log(error);
        }
    });

    //console.log(sUrl);



    //$.ajax({
    //    type: 'GET',
    //    url: url,
    //    contentType: "application/json",
    //    dataType: "json",
    //    success: function (data) {
    //        try {
    //            if ($.isEmptyObject(data)) {
    //                if ($('#alertInterconsulta').hasClass('hide')) {
    //                    $('#alertInterconsulta').removeClass('hide');
    //                }
    //            } else {
    //                if (!($('#alertInterconsulta').hasClass('hide'))) {
    //                    $('#alertInterconsulta').addClass('hide');
    //                }
    //                $.each(data, function () {
    //                    adataset.push([this.RCE_idInterconsulta, this.RCE_diagnosticoInterconsulta, this.RCE_fechaInterconsulta, this.GEN_nombreEspecialidadOrigen, this.GEN_nombreEspecialidadDestino]);
    //                });
    //                LlenaGrilla(adataset, '#tblInterconsulta', columnas, 'Inteconsulta', null);
    //            }
    //        } catch (err) {
    //            console.log("error: " + err.message);
    //        }
    //    }
    //});
}

function getEventosAdversos() {

    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var columnas = [{ title: 'ID' }, { title: 'TIPO DE FORMULARIO' }, { title: 'TIPO EVENTO' }, { title: 'FECHA DE OCURRENCIA' }, { title: 'FECHA DE NOTIFICACION' }, { title: '' }];
    var url = `${GetWebApiUrl()}NEA_Evento_Adverso/Paciente/${idPaciente}`;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Evento Adverso");
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([this.NEA_idEvento_Adverso, this.NEA_nombreTipo_Formulario, this.NEA_nombreTipo_Evento, this.NEA_fecha_hora_ocurrenciaEvento_Adverso, this.NEA_fecha_notificacionEvento_Adverso]);
                    });
                    LlenaGrilla(adataset, '#tblEventoAdverso', columnas, 'EventoAdverso', null);
                }
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });

}

function limpiarGrillaPacientes() {
    $('#tblPacientes').DataTable().clear().draw();
    $('#divPacientes').hide();
}

function limpiarGrillas() {
    if (!($.trim($("#tblEventos").html()) == '')) {
        $('#tblEventos').DataTable().clear().draw();
        $('#tblEventos_wrapper').addClass('hide');
        $('#tblEventos').addClass('hide');
    }
    if (!($.trim($("#tblFormularioIndQx").html()) == '')) {
        $('#tblFormularioIndQx').DataTable().clear().draw();
        $('#tblFormularioIndQx_wrapper').addClass('hide');
        $('#tblFormularioIndQx').addClass('hide');
    }
    if (!($.trim($("#tblProtocoloOperatorio").html()) == '')) {
        $('#tblProtocoloOperatorio').DataTable().clear().draw();
        $('#tblProtocoloOperatorio_wrapper').addClass('hide');
        $('#tblProtocoloOperatorio').addClass('hide');
    }
    if (!($.trim($("#tblHospitalizacion").html()) == '')) {
        $('#tblHospitalizacion').DataTable().clear().draw();
        $('#tblHospitalizacion_wrapper').addClass('hide');
        $('#tblHospitalizacion').addClass('hide');
    }
    if (!($.trim($("#tblReceta").html()) == '')) {
        $('#tblReceta').DataTable().clear().draw();
        $('#tblReceta_wrapper').addClass('hide');
        $('#tblReceta').addClass('hide');
    }
    if (!($.trim($("#tblBiopsia").html()) == '')) {
        $('#tblBiopsia').DataTable().clear().draw();
        $('#tblBiopsia_wrapper').addClass('hide');
        $('#tblBiopsia').addClass('hide');
    }
    if (!($.trim($("#tblUrgencia").html()) == '')) {
        $('#tblUrgencia').DataTable().clear().draw();
        $('#tblUrgencia_wrapper').addClass('hide');
        $('#tblUrgencia').addClass('hide');
    }
    if (!($.trim($("#tblEpicrisis").html()) == '')) {
        $('#tblEpicrisis').DataTable().clear().draw();
        $('#tblEpicrisis_wrapper').addClass('hide');
        $('#tblEpicrisis').addClass('hide');
    }
    if (!($.trim($("#tblIPD").html()) == '')) {
        $('#tblIPD').DataTable().clear().draw();
        $('#tblIPD_wrapper').addClass('hide');
        $('#tblIPD').addClass('hide');
    }
}

function getCountRegistroClinico() {

    var idPaciente = $('#idPaciente').val();
    var url = GetWebApiUrl() + "/GEN_Paciente/CantidadHistoriaClinica/" + idPaciente;

    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                $('.badge-count').show();
                $('.badge-count').empty();
                $.each(data, function () {
                    $('#bdFormularioIndQx').append(this.FORMULARIO_IND_QX.Cantidad);
                    $('#bdProtocoloOperatorio').append(this.PROTOCOLO.Cantidad);
                    //$('#bdExamen').val(this.EXAMEN);
                    $('#bdReceta').append(this.RECETA);
                    $('#bdBiopsia').append(this.BIOPSIA.Cantidad);
                    $('#bdHospitalizacion').append(this.HOSPITALIZACION);
                    $('#bdUrgencia').append(this.URGENCIA);
                    $('#bdEpicrisis').append(this.EPICRISIS);
                    $('#bdIPD').append(this.IPD);
                    $('#bdHE').append(this.HOJAEVOLUCION);
                    $('#bdIC').append(this.INTERCONSULTA);
                    $('#bdNEA').append(this.EVENTOADVERSO);
                    $('.badge-count').each(function () {
                        if ($(this).text() == '0') { $(this).hide(); }
                    });
                });
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });

    //no creo que LAB se pueda contar con la api, el count:
    var sUsuario;
    var sPass;
    var sFecha;
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Usuarios/Login`,
        async: false,
        success: function (data) {
            sUsuario = data[0].GEN_loginUsuarios;
            sPass = data[0].GEN_claveUsuarios;
        }
    });
    sFecha = GetFechaActual();
    var s = `${sUsuario}|${sPass}|${moment(sFecha).add(1, 'hours').format('YYYY-MM-DD HH:mm:ss')}|14`
    var sUrl = `http://10.6.180.236/LABWS/Lab.asmx/Orden?sRut=${$('#txtRutPaciente').val()}-${ObtenerVerificador($("#txtRutPaciente").val())}&sFecha1=2000-01-01&sFecha2=2030-01-01&sToken=${GetTextoEncriptado(s)}`

    var iCount = 0;
    $.ajax({
        type: 'GET',
        url: sUrl,
        async: false,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        dataType: 'xml',
        success: function (response) {
            var $xml = $(response);
            var $orderData = $xml.find('OrderData');
            $orderData.each(function () {
                var g = $(this).find('GroupList');
                if (g.length > 0)
                    iCount++;
            });
        }
    });

    if (iCount > 0) {
        $('#bdLAB').append(iCount);
        $('#bdLAB').show();
    }
    else
        $('#bdLAB').hide();
}

//FUNCIONES PARA PERFILES Y PERMISOS
function datosPaciente(permiso) {
    (!permiso) ? $('#txtRutPaciente').addClass('disabled').attr('disabled', true) : $('#txtRutPaciente').removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#lblRutPaciente").addClass('disabled') : $("#lblRutPaciente").removeClass('disabled');
    (!permiso) ? $("#ddlTipoIdentificacionPaciente").addClass('disabled').attr('disabled', true) : $("#ddlTipoIdentificacionPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#txtNuiPaciente").addClass('disabled').attr('disabled', true) : $("#txtNuiPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? '' : $("#lblNuiPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtNombrePaciente").addClass('disabled').attr('disabled', true) : $("#txtNombrePaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? '' : $("#lblNombrePaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtApellidoPaciente").addClass('disabled').attr('disabled', true) : $("#txtApellidoPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblApellidoPaciente").addClass('disabled') : $("#lblApellidoPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtApellidoMPaciente").addClass('disabled').attr('disabled', true) : $("#txtApellidoMPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblApellidoMPaciente").addClass('disabled') : $("#lblApellidoMPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtFechaNacPaciente").addClass('disabled').attr('disabled', true) : $("#txtFechaNacPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblFechaNacPaciente").addClass('disabled') : $("#lblFechaNacPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#ddlSexoPaciente").addClass('disabled').attr('disabled', true) : $("#ddlSexoPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlGeneroPaciente").addClass('disabled').attr('disabled', true) : $("#ddlGeneroPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlNacionalidadPaciente").addClass('disabled').attr('disabled', true) : $("#ddlNacionalidadPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlPaisPaciente").addClass('disabled').attr('disabled', true) : $("#ddlPaisPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlRegionPaciente").addClass('disabled').attr('disabled', true) : $("#ddlRegionPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlProvinciaPaciente").addClass('disabled').attr('disabled', true) : $("#ddlProvinciaPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlCiudadPaciente").addClass('disabled').attr('disabled', true) : $("#ddlCiudadPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#txtDireccionPaciente").addClass('disabled').attr('disabled', true) : $("#txtDireccionPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblDireccionPaciente").addClass('disabled') : $("#lblDireccionPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtNumeroDireccionPaciente").addClass('disabled').attr('disabled', true) : $("#txtNumeroDireccionPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblNumeroDireccionPaciente").addClass('disabled') : $("#lblNumeroDireccionPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtTelefonoPaciente").addClass('disabled').attr('disabled', true) : $("#txtTelefonoPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblTelefonoPaciente").addClass('disabled') : $("#lblTelefonoPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtOtroTelefonoPaciente").addClass('disabled').attr('disabled', true) : $("#txtOtroTelefonoPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblOtroTelefonoPaciente").addClass('disabled') : $("#lblOtroTelefonoPaciente").removeClass('disabled');
    (!permiso) ? $("#switchRural").bootstrapSwitch('disabled', true) : $("#switchRural").bootstrapSwitch('disabled', false).removeClass('editable');
    (!permiso) ? $("#txtEmailPaciente").addClass('disabled').attr('disabled', true) : $("#txtEmailPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblEmailPaciente").addClass('disabled') : $("#lblEmailPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtRespondePaciente").addClass('disabled').attr('disabled', true) : $("#txtRespondePaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblRespondePaciente").addClass('disabled') : $("#lblRespondePaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#ddlPuebloOrigPaciente").addClass('disabled').attr('disabled', true) : $("#ddlPuebloOrigPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlNivelEscolarPaciente").addClass('disabled').attr('disabled', true) : $("#ddlNivelEscolarPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlEstadoConyuPaciente").addClass('disabled').attr('disabled', true) : $("#ddlEstadoConyuPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#txtFechaFallecimiento").addClass('disabled').attr('disabled', true) : $("#txtFechaFallecimiento").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblFechaFallecimientoPaciente").addClass('disabled') : $("#lblFechaFallecimientoPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#btnGuardaDatos").hide() : $("#btnGuardaDatos").show();

}

function datosClinicos(permiso) {
    (!permiso) ? $("#ddlGrupoSanguineoPaciente").addClass('disabled').attr('disabled', true) : $("#ddlGrupoSanguineoPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#rdoLatexSi").bootstrapSwitch('disabled', true) : $("#rdoLatexSi").bootstrapSwitch('disabled', false).removeClass('editable');
    (!permiso) ? $("#rdoLatexNo").bootstrapSwitch('disabled', true) : $("#rdoLatexNo").bootstrapSwitch('disabled', false).removeClass('editable');
    (!permiso) ? $("#switchOtrasAlergias").bootstrapSwitch('disabled', true) : $("#switchOtrasAlergias").bootstrapSwitch('disabled', false).removeClass('editable');
    (!permiso) ? $("#txtOtrasAlergias").addClass('disabled').attr('disabled', true) : $("#txtOtrasAlergias").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblOtrasAlergias").addClass('disabled') : $("#lblOtrasAlergias").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtAntecedentesFamiliares").addClass('disabled').attr('disabled', true) : $("#txtAntecedentesFamiliares").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtAntecedentesFamiliares").addClass('disabled') : $("#lbltxtAntecedentesFamiliares").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtEnfermedadesHereditarias").addClass('disabled').attr('disabled', true) : $("#txtEnfermedadesHereditarias").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtEnfermedadesHereditarias").addClass('disabled') : $("#lbltxtEnfermedadesHereditarias").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtEnfermedadesPrevias").addClass('disabled').attr('disabled', true) : $("#txtEnfermedadesPrevias").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtEnfermedadesPrevias").addClass('disabled') : $("#Content_txtEnfermedadesPrevias").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtAntecedentesNeonatales").addClass('disabled').attr('disabled', true) : $("#txtAntecedentesNeonatales").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtAntecedentesNeonatales").addClass('disabled') : $("#lbltxtAntecedentesNeonatales").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtMedicacionPrevia").addClass('disabled').attr('disabled', true) : $("#txtMedicacionPrevia").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtMedicacionPrevia").addClass('disabled') : $("#lbltxtMedicacionPrevia").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtTratFarmacologico").addClass('disabled').attr('disabled', true) : $("#txtTratFarmacologico").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtTratFarmacologico").addClass('disabled') : $("#lbltxtTratFarmacologico").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtAntecedentesObstetricos").addClass('disabled').attr('disabled', true) : $("#txtAntecedentesObstetricos").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtAntecedentesObstetricos").addClass('disabled') : $("#lbltxtAntecedentesObstetricos").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtAntecedentesQuirurgicos").addClass('disabled').attr('disabled', true) : $("#txtAntecedentesQuirurgicos").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtAntecedentesQuirurgicos").addClass('disabled') : $("#lbltxtAntecedentesQuirurgicos").removeClass('disabled').removeClass('editable');

    // HÁBITOS
    (!permiso) ? $("#txtDetalleTabaco").addClass('disabled').attr('disabled', true) : $("#txtDetalleTabaco").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#txtDetalleOH").addClass('disabled').attr('disabled', true) : $("#txtDetalleOH").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#txtDrogas").addClass('disabled') : $("#txtDrogas").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtHipertension").addClass('disabled') : $("#txtHipertension").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtDiabetes").addClass('disabled') : $("#txtDiabetes").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtAsma").addClass('disabled') : $("#txtAsma").removeClass('disabled').removeClass('editable');

    (!permiso) ? $("#btnGuardaClinicos").hide() : $("#btnGuardaClinicos").show();

}

function datosPrevisionales(permiso) {
    (!permiso) ? $("#ddlAseguradora").addClass('disabled').attr('disabled', true) : $("#ddlAseguradora").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlTramo").addClass('disabled').attr('disabled', true) : $("#ddlTramo").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#switchPrais").bootstrapSwitch('disabled', true) : $("#switchPrais").bootstrapSwitch('disabled', false).removeClass('editable');
    (!permiso) ? $("#btnGuardaPrevision").hide() : $("#btnGuardaPrevision").show();
}

function cargaPerfiles() {

    // 6 = Admisión, 5 = Enfermera de Servicio, 15 = Gestión Cama y 7 = Admin. de Sistema.
    $("#btnNuevoPacienteModal").removeClass("bg-info").addClass('bg-secondary disabled').attr('disabled', true);

    if (sSession.CODIGO_PERFIL == 13 || sSession.CODIGO_PERFIL == 6 || sSession.CODIGO_PERFIL == 16 || sSession.CODIGO_PERFIL == 20)
        $("#btnNuevoPacienteModal").removeClass('bg-secondary disabled').addClass("bg-info").attr('disabled', false);
    switch (sSession.CODIGO_PERFIL) {
        case 6:
            //Recaudación
            datosPaciente(true);
            datosClinicos(false);
            datosPrevisionales(true);
            break;
        case "4":
            //Usuario de Consulta
            datosPaciente(false);
            datosClinicos(false);
            datosPrevisionales(false);
            break;
        case "1": // Médico
        case "2": // PROFESIONAL GES
        case "3": // APOYO CLINICO
        case "8": // MONITOR(A) GES
        case "9": // DIGITADOR(A) GES
        case "10":// INTERCONSULTOR
        case "12":// INTERNO DE MEDICINA
        case 13:// ADMISIÓN
            datosPaciente(true);
            datosClinicos(false);
            datosPrevisionales(true);
            break;
        case 16:// ADMISIÓN URGENCIA
            datosPaciente(true);
            datosClinicos(false);
            datosPrevisionales(true);
            break;
        case 19:// GESTIÓN DE PACIENTES
            datosPaciente(true);
            $('#txtRutPaciente').addClass('disabled').attr('disabled', true);
            $("#ddlTipoIdentificacionPaciente").addClass('disabled').attr('disabled', true);
            datosClinicos(false);
            datosPrevisionales(true);
            //Debe poder modificar el NUI
            $("#txtNuiPaciente").removeClass('disabled').attr('disabled', false);
            break;
        //GRD
        case 20:
            datosPaciente(true);
            datosClinicos(false);
            datosPrevisionales(true);
            break
        case "5":
        case "7":
        case 15:
            $("#btnNuevoPacienteModal").removeClass('bg-secondary disabled').addClass("bg-info").attr('disabled', false);
            break;
        case 11: // Estudiante de medicina
        case 14: //FICHERO
            $("#dvCaratula").prop("style", "display: flex;");
            break;
        default:
            datosPaciente(false);
            datosClinicos(true);
            datosPrevisionales(false);
            break;
    }

    if (sSession.CODIGO_PERFIL == "19")
        $("#btnNuevoPacienteModal").removeClass('bg-secondary disabled').addClass("bg-info").attr('disabled', false);

}

function linkPacienteCaratula() {
    if (idPac != null) {
        var url = ObtenerHost() + "/Vista/ModuloPaciente/ImprimirCaratulaFicha.aspx?id=" + idPac;
        window.open(url);
    }
}