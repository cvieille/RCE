﻿
var ingresoMedicoUCI = {};

$(document).ready(function () {
    
    $("#txtApache2").blur(function () {
        GetMortalidad();
    });

});

function CargarCombosUCI() {
    CargarComboRankin();
}

function GetJsonIMUCI() {
    return ingresoMedicoUCI;
}

function GetNumeroMortalidad(apache2) {

    var mortalidad;

    if (apache2 >= 0 && apache2 <= 4)
        mortalidad = 4;
    else if (apache2 >= 5 && apache2 <= 9)
        mortalidad = 8;
    else if (apache2 >= 10 && apache2 <= 14)
        mortalidad = 15;
    else if (apache2 >= 15 && apache2 <= 19)
        mortalidad = 25;
    else if (apache2 >= 20 && apache2 <= 24)
        mortalidad = 40;
    else if (apache2 >= 25 && apache2 <= 29)
        mortalidad = 55;
    else if (apache2 >= 30 && apache2 <= 34)
        mortalidad = 75;
    else if (apache2 > 34)
        mortalidad = 85;
    else
        mortalidad = null;

    return mortalidad;
}

function CargarJsonIMUCI(RCE_idIngreso_Medico_UCI) {

    var json = {};

    if (RCE_idIngreso_Medico_UCI !== undefined) {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Ingreso_Medico_UCI/" + RCE_idIngreso_Medico_UCI,
            async: false,
            success: function (data, status, jqXHR) {
                json = data;
            },
            error: function (jqXHR, status) {
                console.log("Error al cargar JSON Ingreso Médico UCI: " + JSON.stringify(jqXHR));
            }
        });

        ingresoMedicoUCI =
            {
                RCE_idIngreso_Medico_UCI: RCE_idIngreso_Medico_UCI,
                RCE_via_aerea_dificilIngreso_Medico_UCI: json.RCE_via_aerea_dificilIngreso_Medico_UCI,
                RCE_apache2Ingreso_Medico_UCI: json.RCE_apache2Ingreso_Medico_UCI,
                GEN_idEscala_Rankin: json.GEN_idEscala_Rankin,
                RCE_estadoIngreso_Medico_UCI: json.RCE_estadoIngreso_Medico_UCI,
                RCE_idIngreso_Medico: json.RCE_idIngreso_Medico
            };
    }

}

function AsignarJsonIMUCI() {

    ingresoMedicoUCI.RCE_idIngreso_Medico = GetIngresoMedico().RCE_idIngreso_Medico;
    
    if ($("#rdoViaAeraDificilSi").bootstrapSwitch('state'))
        ingresoMedicoUCI.RCE_via_aerea_dificilIngreso_Medico_UCI = "SI";
    else if ($("#rdoViaAeraDificilNo").bootstrapSwitch('state'))
        ingresoMedicoUCI.RCE_via_aerea_dificilIngreso_Medico_UCI = "NO";
    else
        ingresoMedicoUCI.RCE_via_aerea_dificilIngreso_Medico_UCI = null;
    
    ingresoMedicoUCI.RCE_apache2Ingreso_Medico_UCI = parseInt($("#txtApache2").val());
    ingresoMedicoUCI.GEN_idEscala_Rankin = parseInt(valCampo($("#sltEscalaRankin").val()));
    ingresoMedicoUCI.RCE_estadoIngreso_Medico_UCI = "Activo";

}

function GuardarIMUCI() {
    
    CargarJsonIMUCI(ingresoMedicoUCI.RCE_idIngreso_Medico_UCI);
    AsignarJsonIMUCI();

    var method = (ingresoMedicoUCI.RCE_idIngreso_Medico_UCI != undefined) ? 'PUT' : 'POST';
    var url = GetWebApiUrl() + "RCE_Ingreso_Medico_UCI" + ((method === 'PUT') ? '/' + ingresoMedicoUCI.RCE_idIngreso_Medico_UCI : '');
    
    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(ingresoMedicoUCI),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) { },
        error: function (jqXHR, status) {
            console.log("Error al guardar Ingreso Médico UCI: " + JSON.stringify(jqXHR));
        }
    });

}

function CargarIMUCI(RCE_idIngreso_Medico_UCI) {

    CargarJsonIMUCI(RCE_idIngreso_Medico_UCI);
    if (ingresoMedicoUCI.RCE_idIngreso_Medico_UCI !== undefined) {

        CargarCombosUCI();
        console.log(JSON.stringify(ingresoMedicoUCI));
        $("#rdoViaAeraDificilSi").bootstrapSwitch('state', (ingresoMedicoUCI.RCE_via_aerea_dificilIngreso_Medico_UCI === "SI"));
        $("#rdoViaAeraDificilNo").bootstrapSwitch('state', (ingresoMedicoUCI.RCE_via_aerea_dificilIngreso_Medico_UCI === "NO"));
        $("#txtApache2").val(ingresoMedicoUCI.RCE_apache2Ingreso_Medico_UCI);
        $("#sltEscalaRankin").val(ingresoMedicoUCI.GEN_idEscala_Rankin);
        GetMortalidad();

    }

}

function GetMortalidad() {
    if ($("#txtApache2").val() !== '') {
        if (GetNumeroMortalidad($("#txtApache2").val()) != null)
            $("#txtMortalidad").val(GetNumeroMortalidad($("#txtApache2").val()) + " % Mortalidad.");
        else
            $("#txtMortalidad").val("Puntaje inválido.");
    }
}

function CargarComboRankin() {

    if ($('#sltEscalaRankin').children('option').length === 0) { 

        $("#sltEscalaRankin").empty();
        $("#sltEscalaRankin").append("<option value='0'>Seleccione</option>");

        try {

            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + "GEN_Escala_Rankin/Combo",
                async: false,
                success: function (data, status, jqXHR) {

                    $.each(sortByKeyAsc(data, "GEN_idEscala_Rankin"), function (key, val) {
                        $("#sltEscalaRankin").append("<option value='" + val.Id + "'>" +
                            val.Valor + "</option>");
                    });

                },
                error: function (jqXHR, status) {
                    console.log("Error al llenar Escala Rankin: " + jqXHR.responseText);
                }
            });

        } catch (err) {
            alert(err.message);
        }

    }

}
