﻿
var jsonIMGinecologia = {};

function CargarCombosIMGinecologia() {
    CargarMetodosAnticonceptivos();
}

function GetJsonIMGinecologia() {
    return jsonIMGinecologia;
}

function CargarJsonIMGinecologia() {

    var json = {};

    if (jsonIMGinecologia.RCE_idIngreso_Medico_Ginecologia !== undefined) { 

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Ingreso_Medico_Ginecologia/" + jsonIMGinecologia.RCE_idIngreso_Medico_Ginecologia,
            async: false,
            success: function (data, status, jqXHR) {
                json = data;
            },
            error: function (jqXHR, status) {
                console.log("Error al cargar JSON Ingreso Médico Ginecología: " + console.log(JSON.stringify(jqXHR)));
            }
        });

        console.log(JSON.stringify(json));

        jsonIMGinecologia =
            {
                RCE_idIngreso_Medico_Ginecologia: json.RCE_idIngreso_Medico_Ginecologia,
                RCE_idIngreso_Medico: json.RCE_idIngreso_Medico,
                RCE_fecha_ultima_reglaIngreso_Medico_Ginecologia: json.RCE_fecha_ultima_reglaIngreso_Medico_Ginecologia,
                RCE_idMetodo_Anticonceptivo: json.RCE_Metodo_Anticonceptivo.RCE_idMetodo_Anticonceptivo,
                RCE_gestacionesIngreso_Medico_Ginecologia: json.RCE_gestacionesIngreso_Medico_Ginecologia,
                RCE_partosIngreso_Medico_Ginecologia: json.RCE_partosIngreso_Medico_Ginecologia,
                RCE_abortosIngreso_Medico_Ginecologia: json.RCE_abortosIngreso_Medico_Ginecologia,
                RCE_ciclosIngreso_Medico_Ginecologia: json.RCE_ciclosIngreso_Medico_Ginecologia,
                RCE_papanicolaouIngreso_Medico_Ginecologia: json.RCE_papanicolaouIngreso_Medico_Ginecologia,
                RCE_mamografiaIngreso_Medico_Ginecologia: json.RCE_mamografiaIngreso_Medico_Ginecologia,
                RCE_ecogtafia_normalIngreso_Medico_Ginecologia: json.RCE_ecogtafia_normalIngreso_Medico_Ginecologia,
                RCE_ecogtafia_anormalIngreso_Medico_Ginecologia: json.RCE_ecogtafia_anormalIngreso_Medico_Ginecologia,
                RCE_pipelleIngreso_Medico_Ginecologia: json.RCE_pipelleIngreso_Medico_Ginecologia,
                RCE_polipoIngreso_Medico_Ginecologia: json.RCE_polipoIngreso_Medico_Ginecologia,
                RCE_estadoIngreso_Medico_Ginecologia: json.RCE_estadoIngreso_Medico_Ginecologia
            };
    }

}

function AsignarJsonIMGinecologia() {

    jsonIMGinecologia.RCE_idIngreso_Medico = GetIngresoMedico().RCE_idIngreso_Medico;
    jsonIMGinecologia.RCE_fecha_ultima_reglaIngreso_Medico_Ginecologia = moment($("#txtFechaUltimaRegla").val()).format("YYYY-MM-DD");
    jsonIMGinecologia.RCE_idMetodo_Anticonceptivo = valCampo($("#sltMetodoAnticonceptivo").val());
    jsonIMGinecologia.RCE_gestacionesIngreso_Medico_Ginecologia = valCampo($("#txtGestaciones").val());
    jsonIMGinecologia.RCE_partosIngreso_Medico_Ginecologia = valCampo($("#txtPartos").val());
    jsonIMGinecologia.RCE_abortosIngreso_Medico_Ginecologia = valCampo($("#txtAbortos").val());
    jsonIMGinecologia.RCE_ciclosIngreso_Medico_Ginecologia = valCampo($("#txtCiclos").val());
    jsonIMGinecologia.RCE_papanicolaouIngreso_Medico_Ginecologia = valCampo($("#txtPapanicolaou").val());
    jsonIMGinecologia.RCE_mamografiaIngreso_Medico_Ginecologia = valCampo($("#txtMamografia").val());
    jsonIMGinecologia.RCE_ecogtafia_normalIngreso_Medico_Ginecologia = ($("#rdoEcoNormal").is(":checked")) ? "SI" : null;
    
    if (jsonIMGinecologia.RCE_ecogtafia_normalIngreso_Medico_Ginecologia === "SI")
        jsonIMGinecologia.RCE_ecogtafia_anormalIngreso_Medico_Ginecologia = null;
    else
        jsonIMGinecologia.RCE_ecogtafia_anormalIngreso_Medico_Ginecologia = valCampo($("#txtDescripcionEcoAnormal").val());

    jsonIMGinecologia.RCE_pipelleIngreso_Medico_Ginecologia = ($("#chbPipelleBiopsia").is(":checked")) ? "SI" : "NO";
    jsonIMGinecologia.RCE_polipoIngreso_Medico_Ginecologia = ($("#chbPolipoBiopsia").is(":checked")) ? "SI" : "NO";
    jsonIMGinecologia.RCE_estadoIngreso_Medico_Ginecologia = "Activo";
    
}

function GuardarIMGinecologia() {

    CargarJsonIMGinecologia();
    AsignarJsonIMGinecologia();
    var method = (jsonIMGinecologia.RCE_idIngreso_Medico_Ginecologia != undefined) ? 'PUT' : 'POST';
    var url = GetWebApiUrl() + "RCE_Ingreso_Medico_Ginecologia" + ((method === 'PUT') ? '/' + jsonIMGinecologia.RCE_idIngreso_Medico_Ginecologia : '');

    console.log(JSON.stringify(jsonIMGinecologia));

    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(jsonIMGinecologia),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {},
        error: function (jqXHR, status) {
            console.log("Error al guardar Ingreso Médico Ginecologia: " + JSON.stringify(jqXHR));
        }
    });

}

function CargarIMGinecologia(RCE_idIngreso_Medico_Ginecologia) {
    
    if (RCE_idIngreso_Medico_Ginecologia !== null) {

        CargarCombosIMGinecologia();
        jsonIMGinecologia.RCE_idIngreso_Medico_Ginecologia = RCE_idIngreso_Medico_Ginecologia;
        CargarJsonIMGinecologia();
        console.log(JSON.stringify(jsonIMGinecologia));
        
        $("#txtFechaUltimaRegla").val(moment(jsonIMGinecologia.RCE_fecha_ultima_reglaIngreso_Medico_Ginecologia).format("YYYY-MM-DD"));
        $("#sltMetodoAnticonceptivo").val(jsonIMGinecologia.RCE_idMetodo_Anticonceptivo);
        $("#txtGestaciones").val(jsonIMGinecologia.RCE_gestacionesIngreso_Medico_Ginecologia);
        $("#txtPartos").val(jsonIMGinecologia.RCE_partosIngreso_Medico_Ginecologia);
        $("#txtAbortos").val(jsonIMGinecologia.RCE_abortosIngreso_Medico_Ginecologia);
        $("#txtCiclos").val(jsonIMGinecologia.RCE_ciclosIngreso_Medico_Ginecologia);
        $("#txtPapanicolaou").val(jsonIMGinecologia.RCE_papanicolaouIngreso_Medico_Ginecologia);
        $("#txtMamografia").val(jsonIMGinecologia.RCE_mamografiaIngreso_Medico_Ginecologia);
        
        (jsonIMGinecologia.RCE_ecogtafia_normalIngreso_Medico_Ginecologia === "SI") ? $("#rdoEcoNormal").attr("checked", "checked") : $("#rdoEcoNormal").removeAttr("checked");
        (jsonIMGinecologia.RCE_ecogtafia_anormalIngreso_Medico_Ginecologia !== null) ? $("#rdoEcoAnormal").attr("checked", "checked") : $("#rdoEcoAnormal").removeAttr("checked");

        if (jsonIMGinecologia.RCE_ecogtafia_anormalIngreso_Medico_Ginecologia !== null) {
            $("#divDescripcionEcoAnormal").show();
            $("#txtDescripcionEcoAnormal").attr("data-required", "true");
            $("#txtDescripcionEcoAnormal").val(jsonIMGinecologia.RCE_ecogtafia_anormalIngreso_Medico_Ginecologia);
        }

        (jsonIMGinecologia.RCE_pipelleIngreso_Medico_Ginecologia === "SI") ? $("#chbPipelleBiopsia").attr("checked", "checked") : $("#chbPipelleBiopsia").removeAttr("checked");
        (jsonIMGinecologia.RCE_polipoIngreso_Medico_Ginecologia === "SI") ? $("#chbPolipoBiopsia").attr("checked", "checked") : $("#chbPolipoBiopsia").removeAttr("checked");
        
    }

}

function CargarMetodosAnticonceptivos() {

    if ($('#sltMetodoAnticonceptivo').children('option').length === 0) {

        $("#sltMetodoAnticonceptivo").empty();
        $("#sltMetodoAnticonceptivo").append("<option value='0'>Seleccione</option>");

        try {

            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + "RCE_Metodo_Anticonceptivo/Combo",
                async: false,
                success: function (data, status, jqXHR) {


                    $.each(data, function (key, val) {
                        $("#sltMetodoAnticonceptivo").append("<option value='" + val.RCE_idMetodo_Anticonceptivo + "'>" +
                            val.RCE_descripcionMetodo_Anticonceptivo + "</option>");
                    });

                },
                error: function (jqXHR, status) {
                    console.log("Error al llenar Métodos Anticonceptivos: " + jqXHR.responseText);
                }
            });

        } catch (err) {
            alert(err.message);
        }

    }

}