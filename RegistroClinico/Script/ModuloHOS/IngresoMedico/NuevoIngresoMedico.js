﻿
var sSession = null;
var idPro;
var ingresoMedico = {};
var HOS_idHospitalizacion = 0;
var HOS_Hospitalizacion = {};
var arrayDiagnosticos = [];
var alertFlag = 0;
let formatFlag = 0;

var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
};

$(document).ready(function () {

    $("textarea").val("");
    //let fechaActual = moment(GetFechaActual()).format("LL");
    let fechaActual = moment(GetFechaActual()).format("l");
    console.log(fechaActual);
    document.getElementById("lbl_fechaActual").innerHTML = fechaActual;
    sSession = getSession();
    //ReiniciarRequired();

    //Al scrollear hacia abajo se muestran el alertas de Campos Obligatorios y/o formato
    $(window).scroll(function () {
        if (alertFlag > 0) {
            ($($(this)).scrollTop() > 150) ? $('#alertObligatorios').stop().show('fast') : $('#alertObligatorios').stop().hide('fast');
        } else {
            $('#alertObligatorios').stop().hide('fast')
        }
        if (formatFlag > 0) {
            ($($(this)).scrollTop() > 150) ? $('#alertFormatos').stop().show('fast') : $('#alertFormatos').stop().hide('fast');
        } else {
            $('#alertFormatos').stop().hide('fast')
        }
    });

    if (sSession.ID_HOSPITALIZACION != undefined)
        HOS_idHospitalizacion = parseInt(sSession.ID_HOSPITALIZACION);

    if (HOS_idHospitalizacion != 0) {
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "HOS_Hospitalizacion/" + HOS_idHospitalizacion,
            async: false,
            success: function (data, status, jqXHR) {
                HOS_Hospitalizacion = data[0];
            },
            error: function (jqXHR, status) {
                console.log("Error al consultar hospitalización: " + jqXHR.responseText);
            }
        });
    }
    
    textboxio.replaceAll('#txtExamenFisicoSegmentado',
        {
            paste: { style: 'clean' },
            images: { allowLocal: false }
        });

    $(".divTipoIngresoMedico, #tblDiagnosticos, #liTablist").hide();

    $.typeahead({
        input: '#txtDiagnosticoCIE10',
        minLength: 1,
        maxItem: 12,
        maxItemPerGroup: 10,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el diagnóstico especificado ({{query}})",
        group: {
            key: "GEN_descripcion_padreDiagnostico_CIE10",
            template: function (item) {
                return item.GEN_descripcion_padreDiagnostico_CIE10;
            }
        },
        display: ["GEN_descripcionDiagnostico_CIE10"],
        template:
            '<span>' +
                '<span class="GEN_descripcionDiagnostico_CIE10">{{GEN_descripcionDiagnostico_CIE10}}</span>' +
            '</span>',
        correlativeTemplate: true,
        source: {
            GEN_descripcionDiagnostico_CIE10: {
                ajax: {
                    type: "GET",
                    url: GetWebApiUrl() + "GEN_diagnostico_CIE10/Combo",
                    beforeSend: function (jqXHR, options) {
                        jqXHR.setRequestHeader('Authorization', GetToken());
                    }
                }
            }
        },
        callback: {
            onClick: function (node, a, item, event) {

                if (!ExisteDiagnosticoSeleccionado(item.GEN_idDiagnostico_CIE10)) {
                    item.RCE_idIngreso_Medico_Diagnostico_CIE10 = 0;
                    AgregarFila(item);
                }

            }
        }
    });

    $("#divDescripcionEcoAnormal").hide();
    $("#rdoEcoNormal").change(function () {
        $("#divDescripcionEcoAnormal").hide();
        $("#txtDescripcionEcoAnormal").attr("data-required", "false");
    });
    $("#rdoEcoAnormal").change(function () {
        $("#divDescripcionEcoAnormal").show();
        $("#txtDescripcionEcoAnormal").attr("data-required", "true");
    });

    $('#txtDiagnosticoCIE10').focus(function () { $(this).val(''); });

    RevisarAcceso('');
    window.addEventListener('beforeunload', bunload, false);


    $("input.bootstrapSwitch").bootstrapSwitch();
    $("#divMedicos, #divCirugias, #divAlergias, #divOh, #divTabaco, #divActividadFisica, #divDrogas, " +
        "#divHipertension, #divDiabetes, #divAsma").hide();

    $("input.bootstrapSwitch").on('switchChange.bootstrapSwitch', function (event, state) {
        if ($(this).attr("data-descripcion") !== undefined) {
            (state) ? $("#" + $(this).attr("data-descripcion")).show() : $("#" + $(this).attr("data-descripcion")).hide();
            $("#" + $(this).attr("data-descripcion") + " input[type='text']").attr("data-required", state);
        }
    });

    $('body').on('click', '#btnNuevoIngresoExistente', function (e) {
        CargarUsuario();
        $('label[for]').addClass('active');
        $('#mdlIngresoExistente').modal('hide');
        e.preventDefault();
    });

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
        async: false,
        success: function (data) {
            idPro = data[0].GEN_idProfesional;
        }
    });

    CargarComboUbicacion();
    CargarComboTipoIngresoMedico();

    $('#sltTipoIngresoMedico').change(function () {
        CambiarTipoIngresoMedico($(this));
    });

    $('#mdlIngresoExistente').on('shown.bs.modal', function () {
        $('.hhcc').attr('style', 'width:1px !important;');
    });

    if (!obtenerIngreso())
        CargarUsuario();

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 3500,
        "extendedTimeOut": 2500,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }

    ShowModalCargando(false);

});

//Alterna botones y campos dependiendo del tipo del ingreso medico
function CambiarTipoIngresoMedico(sender) {

    if ($(sender).val() != 0) {

        $("#liTablist").show();
        $("#divExamenFisicoPediatria, #divPersonalesPediatria").hide();
        $("#liTipoIngresoMedico").remove();
        $("#liAntecedentesClinicos a").trigger("click");
        $("textarea").removeAttr("style");
        textboxio.replace("#txtExamenFisicoSegmentado").content.set(
            `<b>Cabeza: </b><br />
            <b>Cuello: </b><br />
            <b>Tórax: </b><br />
            <b>Abdomen: </b><br />
            <b>Extremidades: </b>`);

        if ($(`#div${$("#sltTipoIngresoMedico option:selected").text()}`).length > 0) {
            
            $('#ulTablist').append($(
                `<li id='liTipoIngresoMedico' class='nav-item'>
                    <a id="profile-tab-just" class='nav-link' data-toggle="pill" href= "#div${ $("#sltTipoIngresoMedico option:selected").text() }">
                        ${ $("#sltTipoIngresoMedico option:selected").text() }
                    </a>
                 </li>`));
        }

        switch (parseInt($(sender).val())) {
            
            case 1: //GENERAL
                $('#btn-siguiente-IM').hide();
                break;
            case 2: // GINECOLOGÍA
                CargarCombosIMGinecologia();
                $('#btn-siguiente-IM').show();
                break;
            case 3: // OBSTETRICIA
                CargarCombosIMObstetricia();
                $('#btn-siguiente-IM').show();
                break;
            case 4: // PEDIATRÍA
                $("#divExamenFisicoPediatria, #divPersonalesPediatria").show();
                textboxio.replace("#txtExamenFisicoSegmentado").content.set(
                    `<b>General: </b><br />
                    <b>Piel: </b><br />
                    <b>Cabeza/boca/cuello: </b><br />
                    <b>Faringe: </b><br />
                    <b>Otoscopia: </b><br />
                    <b>Cardiopulmonar: </b><br />
                    <b>Abdomen: </b><br />
                    <b>Genitales: </b><br />
                    <b>Extremidades: </b><br />
                    <b>Neurológico: </b>`);
                CargarPediatria();
                $('#btn-siguiente-IM').show();
                break;
            case 6: // UCI
                CargarCombosUCI();
                $('#btn-siguiente-IM').show();
                //$("a[href='#divUCI']").trigger("click");
                //$("#aSiguienteUCI").show();
                /*$("#aSiguienteUCI").click(function () {
                    $('a[href="#divAntecedentesClinicos"]').trigger('click');
                    $(this).hide();
                });*/
                
                break;
            case 5: //Neonatal
                $('#btn-siguiente-IM').hide();
                break;
        }

        $("#txtExamenFisicoSegmentado, #txtExamenFisicoObstetricia, #txtEcografiaObstetrica").hide();

    } else
        $("#liTablist").hide();

}

function resetTab() {

    var tabs = $("#ulTablist li:not(:first)");
    var len = 1;

    $(tabs).each(function (k, v) {
        len++;
        $(this).find('a').html($(this).find('a').text());
    });

    tabID--;
}

function IrSiguiente() {
    $(".nav-link.active[data-toggle='pill']").parent().next().children("a").trigger('click');
}

function IrAnterior() {
    $(".nav-link.active[data-toggle='pill']").parent().prev().children("a").trigger('click');
}

function AgregarFila(json) {

    var idTr = "trDiagnosticos_" + $("#txtDiagnosticoCIE10 tbody").length;

    var tr =
        `
            <tr id='${ idTr }' data-idDiagnostico = '${json.GEN_idDiagnostico_CIE10}' data-idIngresoDiagnostico = '${json.RCE_idIngreso_Medico_Diagnostico_CIE10}'>
                <td class='pt-1 pb-1' style='font-size:20px !important;'>${ json.GEN_descripcionDiagnostico_CIE10}</td>
                <td class='text-center pt-1 pb-1'>
                    <a class='btn btn-danger' data-idIngresoDiagnostico = '${json.RCE_idIngreso_Medico_Diagnostico_CIE10}'>
                        <i class='fa fa-minus-circle'></i>
                    </a>
                </td>
            </tr>
        `;

    $("#tblDiagnosticos tbody").append(tr);
    $("#tblDiagnosticos").show();

    arrayDiagnosticos.push({
        RCE_idIngreso_Medico_Diagnostico_CIE10: json.RCE_idIngreso_Medico_Diagnostico_CIE10,
        GEN_idDiagnostico_CIE10: json.GEN_idDiagnostico_CIE10,
        RCE_estadoIngreso_Medico_Diagnostico_CIE10: "Activo"
    });

    $("#" + idTr + " td a").click(function () {

        $(this).parent().parent().remove();

        if ($(this).attr("data-idIngresoDiagnostico") !== 0){
            for (var i = 0; i < arrayDiagnosticos.length; i++) {
                
                if (arrayDiagnosticos[i].RCE_idIngreso_Medico_Diagnostico_CIE10 == $(this).attr("data-idIngresoDiagnostico")) { 
                    arrayDiagnosticos[i].RCE_estadoIngreso_Medico_Diagnostico_CIE10 = "Inactivo";
                    break;
                }

            }
        } else {
            for (var i = 0; i < arrayDiagnosticos.length; i++) {
                if (arrayDiagnosticos[i].GEN_idDiagnostico_CIE10 == $(this).parent().parent().attr("data-idDiagnostico")) {
                    arrayDiagnosticos.splice(i, 1);
                    break;
                }
            }
        }

        if ($.trim($("#tblDiagnosticos tbody").html()) === "")
            $("#tblDiagnosticos").hide();

        $(this).unbind();

    });

}

function ExisteDiagnosticoSeleccionado(GEN_idDiagnostico) {

    var existeDiagnostico = false;

    $("#tblDiagnosticos tbody tr").each(function (index) {
        if ($(this).attr("data-idDiagnostico") == GEN_idDiagnostico)
            existeDiagnostico = true;
    });

    return existeDiagnostico;

}

function obtenerIngreso() {

    var bExistente = false;
    if (sSession.ID_PACIENTE != null && sSession.ID_PACIENTE != undefined) {

        var adataset = [];

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Ingreso_Medico/GEN_idPaciente/" + sSession.ID_PACIENTE,
            contentType: "application/json",
            dataType: "json",
            async: false,
            success: function (data) {
                if ($.isEmptyObject(data)) {
                    //
                } else {
                    bExistente = true;
                    $.each(data, function () {
                        adataset.push([
                            this.RCE_idIngreso_Medico,
                            moment(this.RCE_fechaIngreso_Medico).format("YYYY-MM-DD"),
                            (this.RCE_diagnostico_principalIngreso_Medico == null) ? '' : this.RCE_diagnostico_principalIngreso_Medico,
                            this.GEN_nombrePersonasProfesional,
                            this.GEN_nombreUbicacion,
                            ""
                        ]);
                    });
                }
            }
        });
    }

    if (bExistente) {
        $('#tblIngresoExistente').addClass("nowrap").DataTable({
            dom: 'ti',
            data: adataset,
            order: [],
            columns: [
                { title: 'ID' },
                { title: 'Fecha Ingreso' },
                { title: 'Diagnóstico Principal' },
                { title: 'Profesional' },
                { title: 'Ubicación' },
                { title: '' }
            ],
            columnDefs: [
                {
                    targets: -1,
                    orderable: false,
                    searchable: false,
                    className: 'hhcc',
                    render: function (data, type, row, meta) {
                        var fila = meta.row;
                        var botones;
                        botones = "<a id='linkAsignarIngreso' data-id='" + adataset[fila][0] + "' class='btn btn-info' href='#/' onclick='linkAsignarIngreso(this);' style='margin:-3px; padding:5px; font-size:0.9rem;'><i class='fa fa-list'></i> Editar Ingreso Médico</a>";
                        return botones;
                    }
                },
            ],
            bDestroy: true
        });

        $('#mdlIngresoExistente').modal('show');
    }

    return bExistente;

}

function linkAsignarIngreso(sender) {

    var idIngreso = $(sender).data("id");
    sSession.ID_INGRESO_MEDICO = idIngreso;
    setSession(idIngreso);
    CargarUsuario();
    $('label[for]').addClass('active');
    $('#mdlIngresoExistente').modal('hide');

}

function CargarUsuario() {

    if (sSession.ID_INGRESO_MEDICO == undefined) {

        $("#txtFechaIngreso").val(moment(sSession.FECHA_ACTUAL).format("YYYY-MM-DD"));
        $("#txtHoraIngreso").val(moment(sSession.FECHA_ACTUAL).format("HH:mm"))
        $("#hFechaCreacion").hide();

        if (sSession.ID_PACIENTE != undefined) {
            CargarPaciente(sSession.ID_PACIENTE);
            BloquearPaciente();
        }

        if (sSession.ID_EVENTO != undefined)
            ingresoMedico.RCE_idEventos = parseInt(sSession.ID_EVENTO);
        
        if (HOS_Hospitalizacion.Ubicacion.GEN_idUbicacion != null) {
            $("#sltUbicacion").val(HOS_Hospitalizacion.Ubicacion.GEN_idUbicacion); //JAVIER
            
        }

        if (idPro != undefined) {
            CargarProfesional();

            //no ingreso médico? tratar de cargar el servicio desde GEN_Profesional_Ubicacion
            //$.ajax({
            //    type: 'POST',
            //    url: ObtenerHost() + '/Vista/ModuloHOS/NuevoIngresoMedico.aspx/Servicio',
            //    contentType: 'application/json',
            //    data: JSON.stringify({
            //        idp: idPro
            //    }),
            //    dataType: 'json',
            //    success: function (data) {                    
            //        $("#sltUbicacion").val(data.d);
            //    }
            //});
        }
        
        
    } else 
        CargarIngresoMedico();

    if (sSession.ID_EVENTO != undefined)
        deleteSession('ID_EVENTO');

    if (sSession.ID_PACIENTE != undefined)
        deleteSession('ID_PACIENTE');

    if (sSession.ID_HOSPITALIZACION != undefined)
        deleteSession('ID_HOSPITALIZACION');
    
}

function CargarProfesional() {

    try {
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "GEN_Profesional/" + idPro,
            async: false,
            success: function (data, status, jqXHR) {
                ingresoMedico.GEN_idProfesional = data.GEN_idProfesional;
                $("#txtNumeroDocumentoProfesional").val(data.GEN_rutProfesional +
                    (data.GEN_digitoProfesional !== null ? '-' + data.GEN_digitoProfesional : ''));
                $("#txtNombreProfesional").val(data.GEN_nombreProfesional);
                $("#txtApePatProfesional").val(data.GEN_apellidoProfesional);
                $("#txtApeMatProfesional").val(data.GEN_sapellidoProfesional);

            },
            error: function (jqXHR, status) {
                alert("Error al cargar Profesional: " + JSON.stringify(jqXHR));
            }
        });

    } catch (err) {
        alert(err.message);
    }

}

function CargarComboTipoIngresoMedico() {
    //PAB_Tipo_Riesgo             
    $("#sltTipoIngresoMedico").empty();
    $("#sltTipoIngresoMedico").append("<option value='0'>Seleccione</option>");

    try {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Tipo_Ingreso_Medico/Combo",
            async: false,
            success: function (data, status, jqXHR) {

                $.each(data, function (key, val) {
                    $("#sltTipoIngresoMedico").append("<option value='" + val.RCE_idTipo_Ingreso_Medico + "'>" +
                        val.RCE_descripcionTipo_Ingreso_Medico + "</option>");
                });
                
            },
            error: function (jqXHR, status) {
                console.log("Error al llenar PAB_Tipo_Riesgo: " + jqXHR.responseText);
            }
        });

    } catch (err) {
        alert(err.message);
    }

}

function CargarComboUbicacion() {

    $("#sltUbicacion").empty();
    $("#sltUbicacion").append("<option value='0'>Seleccione</option>");

    try {
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "GEN_Ubicacion/Hospitalizacion/1",
            async: false,
            success: function (data, status, jqXHR) {

                $.each(data, function (key, val) {
                    $("#sltUbicacion").append("<option value='" + val.Id + "'>" +
                        val.Valor + "</option>");
                });
                
            },
            error: function (jqXHR, status) {
                console.log("Error al llenar Ubicación: " + jqXHR.responseText);
            }
        });

    } catch (err) {
        alert(err.message);
    }
        
}

function GetIngresoMedico() {
    return ingresoMedico;
}

function CargarIngresoMedico() {
    
    ingresoMedico.RCE_idIngreso_Medico = parseInt(sSession.ID_INGRESO_MEDICO);
    var json = GetJsonIngresoMedico();

    //ingresoMedico.GEN_idProfesional = sSession.GEN_idProfesional;
    ingresoMedico.GEN_idProfesional = json.Profesional.GEN_idProfesional;

    // Datos Paciente
    buscarPacientePorDocumento(json.Paciente.Identificacion.GEN_idIdentificacion, json.Paciente.Identificacion.GEN_numero_documentoPaciente);
    BloquearPaciente();

    //RCE_idTipo_Ingreso_Medico
    //$("#sltTipoIngresoMedico").val(json.RCE_Tipo_Ingreso_Medico.RCE_idTipo_Ingreso_Medico);
    $("#sltTipoIngresoMedico").val(json.Identificacion.RCE_idTipo_Ingreso_Medico);
    CambiarTipoIngresoMedico($("#sltTipoIngresoMedico"));
    $("#sltTipoIngresoMedico").attr("disabled", "disabled");

    // UCI
    if (json.RCE_Tipo_Ingreso_Medico == 6) {
        
        if (json.RCE_Ingreso_Medico_UCI.length > 0) 
            CargarIMUCI(json.RCE_Ingreso_Medico_UCI[0].RCE_idIngreso_Medico_UCI);

    //Obstetricia
    } else if (json.Identificacion.RCE_idTipo_Ingreso_Medico == 3) {
        
        if (json.RCE_Ingreso_Medico_Obstetricia.length > 0)
            CargarIMObstetricia(json.RCE_Ingreso_Medico_Obstetricia[0].RCE_idIngreso_Medico_Obstetricia);

    // Ginecología
    } else if (json.Identificacion.RCE_idTipo_Ingreso_Medico == 2) {

        if (json.RCE_Ingreso_Medico_Ginecologia.length > 0)
            CargarIMGinecologia(json.RCE_Ingreso_Medico_Ginecologia[0].RCE_idIngreso_Medico_Ginecologia);

    }// Pediatrico
    else if (json.Identificacion.RCE_idTipo_Ingreso_Medico == 4) {

        if (json.RCE_Ingreso_Medico_Pediatrico.length > 0)
            CargariMPediatrico(json.RCE_Ingreso_Medico_Pediatrico[0].RCE_idIngreso_Medico_Pediatrico);

    }

    console.log(JSON.stringify(json));
    // Antecedentes Clínicos 
    // 1. MORBIDOS
    $("#txtMedicos").val(json.RCE_medicosIngreso_Medico);
    $("#txtCirugias").val(json.RCE_cirugiasIngreso_Medico);
    $("#txtAlergias").val(json.RCE_alergiasIngreso_Medico);
    $("#txtHipertension").val(json.RCE_hipertensionIngreso_Medico);
    $("#txtDiabetes").val(json.RCE_diabetesIngreso_Medico);
    $("#txtAsma").val(json.RCE_asmaIngreso_Medico);

    // 2. HÁBITOS
    $("#chbTabaco").bootstrapSwitch('state', (json.RCE_tabacoIngreso_Medico !== null));
    $("#txtTabaco").val(json.RCE_tabacoIngreso_Medico);

    $("#chbOh").bootstrapSwitch('state', (json.RCE_OHIngreso_Medico !== null));
    $("#txtOh").val(json.RCE_OHIngreso_Medico);

    $("#chbDrogas").bootstrapSwitch('state', json.RCE_drogasIngreso_Medico);
    $("#txtDrogas").val(json.RCE_drogasIngreso_Medico);

    // 3. EXAMEN FÍSICO GENERAL
    $("#txtPesoExamenFisico").val(json.RCE_pesoIngreso_Medico);
    $("#txtTallaExamenFisico").val(json.RCE_tallaIngreso_Medico);
    $("#txtPresionArterialExamenFisico").val(json.RCE_presion_arterialIngreso_Medico);
    $("#txtFrecuenciaCardiacaExamenFisico").val(json.RCE_frecuencia_arterialIngreso_Medico);
    $("#txtTemperaturaExamenFisico").val(json.RCE_temperaturaIngreso_Medico);
    $("#txtFrecuenciaRespiratoriaCardiacaExamenFisico").val(json.RCE_frecuencia_respiratoriaIngreso_Medico);
    $("#txtExamenFisico").val(json.RCE_examen_fisicoIngreso_Medico);
    textboxio.replace("#txtExamenFisicoSegmentado").content.set((json.RCE_examen_segmentadoIngreso_medico === null) ? '' :
                                                                    json.RCE_examen_segmentadoIngreso_medico);

    // 4. MEDICAMENTOS
    $("#txtMedicamentos").val(json.RCE_medicamentosIngreso_Medico);

    // 5. OTROS
    $("#txtDescripcionOtros").val(json.RCE_otrosIngreso_Medico);

    // Datos de Ingreso Médico

    $("#txtFechaIngreso").val(moment(json.RCE_fecha_horaIngreso_Medico).format("YYYY-MM-DD"));
    $("#txtHoraIngreso").val(moment(json.RCE_fecha_horaIngreso_Medico).format("HH:mm"));
    $("#sltUbicacion").val(json.Ubicacion.GEN_idUbicacion);
    $("#txtMotivoConsulta").val(json.RCE_motivo_consultaIngreso_Medico);
    $("#txtAnamnesis").val(json.RCE_anamnesisIngreso_Medico);
    $("#txtExamenFisico").val(json.RCE_examen_fisicoIngreso_Medico);

    // Hipotesis diagnóstica o Diagnóstico de Ingreso
    $("#txtDiagnostico").val(json.RCE_diagnosticoIngreso_Medico);
    $("#txtPlanManejo").val(json.RCE_plan_manejoIngreso_Medico);
    
    // Datos del Profesional 
    //$("#txtNumeroDocumentoProfesional").val(json.Profesional.GEN_numero_documentoPersonasProfesional);
    $("#txtNumeroDocumentoProfesional").val(json.Profesional.GEN_numero_documentoPersonas);
    $("#txtNombreProfesional").val(json.Profesional.GEN_nombrePersonas);
    $("#txtApePatProfesional").val(json.Profesional.GEN_apellido_paternoPersonas);
    $("#txtApeMatProfesional").val(json.Profesional.GEN_apellido_maternoPersonas);

    $("#hFechaCreacion").show();
    $("#strFechaCreacion").text(moment(json.RCE_fecha_hora_creacionIngreso_Medico).format("DD-MM-YYYY HH:mm:SS"));

    /*Datos UCI*/
    console.warn(json.RCE_Ingreso_Medico_UCI.length)
    if (json.RCE_Ingreso_Medico_UCI.length > 0) {
        uci = json.RCE_Ingreso_Medico_UCI[0];
        
        $("#sltEscalaRankin").val(uci.GEN_Escala_Rankin.GEN_idEscala_Rankin);
        $("#txtApache2").val(uci.RCE_apache2Ingreso_Medico_UCI);
        $("#txtMortalidad").val(GetNumeroMortalidad($("#txtApache2").val()) + " % Mortalidad.");

        if (uci.RCE_via_aerea_dificilIngreso_Medico_UCI == "NO") {
            $("#rdoViaAeraDificilNo").bootstrapSwitch('state', true);
        } else {
            $("#rdoViaAeraDificilSi").bootstrapSwitch('state', true);

        }
        
    }

    CargarDiagnosticosCIE10(sSession.ID_INGRESO_MEDICO);

    deleteSession('ID_INGRESO_MEDICO');

    //Esto esta a medio filo
    var vEditable = json.GEN_idProfesional == idPro;

    if (!vEditable) {

        //pasar todos los que tengan X clase a inactivo
        $('.m_editable').find('input').addClass('disabled').attr('disabled', true);
        $('.m_editable').find('textarea').addClass('disabled').attr('disabled', true);
        $('.m_editable').find('textarea').css('background', '#eeeeee');

        $("#tblDiagnosticos tbody tr, #tblDiagnosticos thead tr").each(function () {
            $(this).find("td:eq(1)").remove();
            $(this).find("th:eq(1)").remove();
        });

        //textboxio.replace("#txtExamenFisicoSegmentado").content.documentElement().body.setAttribute("contenteditable", "false");
        textboxio.replace("#txtExamenFisicoSegmentado").events.loaded.addListener(function () {
            var edDocument = textboxio.replace("#txtExamenFisicoSegmentado").content.documentElement();
            edDocument.body.setAttribute("contenteditable", "false");
        });

        var v = $('.m_editable').find('select');
        for (var i = 0; i < v.length; i++) {
            $("button[data-id='" + v[i].getAttribute('id') + "']").addClass('disabled').attr('disabled', true);
        }
        $('.m_editable').find('.btn-info').addClass('disabled').attr('disabled', true);
        $('.m_editable').find('.bootstrap-switch').addClass('disabled').attr('disabled', true);
        $('#aGuardarIngresoMedico').hide();
    }
}

function CargarDiagnosticosCIE10(RCE_idIngreso_Medico) {

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Ingreso_Medico_Diagnostico_CIE10/RCE_idIngreso_Medico/" + RCE_idIngreso_Medico,
        async: false,
        headers: { 'Authorization': GetToken() },
        success: function (data) {

            for (var i = 0; i < data.length; i++) 
                AgregarFila(data[i]);
            
        }
    });

}

function GetJsonIngresoMedico() {
    
    var json = {};

    if (ingresoMedico.RCE_idIngreso_Medico !== undefined) {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Ingreso_Medico/" + ingresoMedico.RCE_idIngreso_Medico,
            async: false,
            success: function (data, status, jqXHR) {
                json = data[0];
            },
            error: function (jqXHR, status) {
                console.log("Error al cargar JSON Ingreso Médico: " + console.log(JSON.stringify(jqXHR)));
            }
        });

    } else {
        json = {
            RCE_Ingreso_Medico: {
                RCE_Tipo_Ingreso_Medico: {},
                GEN_Tipo_Estados_Sistemas: {}
            }
        }
    }


    return json;

}

function CargarJsonIngresoMedico() {

    var json = GetJsonIngresoMedico();
    //ingresoMedico.RCE_idTipo_Ingreso_Medico = valCampo(json.RCE_Tipo_Ingreso_Medico.RCE_idTipo_Ingreso_Medico);
    ingresoMedico.RCE_idTipo_Ingreso_Medico = valCampo(json.RCE_Ingreso_Medico.RCE_idTipo_Ingreso_Medico);

    // Antecedentes Clínicos
    // 1. Morbidos
    ingresoMedico.RCE_medicosIngreso_Medico = valCampo(json.RCE_medicosIngreso_Medico);
    ingresoMedico.RCE_cirugiasIngreso_Medico = valCampo(json.RCE_cirugiasIngreso_Medico);
    ingresoMedico.RCE_alergiasIngreso_Medico = valCampo(json.RCE_alergiasIngreso_Medico);
    ingresoMedico.RCE_hipertensionIngreso_Medico = valCampo(json.RCE_hipertensionIngreso_Medico);
    ingresoMedico.RCE_diabetesIngreso_Medico = valCampo(json.RCE_diabetesIngreso_Medico);
    ingresoMedico.RCE_asmaIngreso_Medico = valCampo(json.RCE_asmaIngreso_Medico);

    // 2. Hábitos
    ingresoMedico.RCE_tabacoIngreso_Medico = valCampo(json.RCE_tabacoIngreso_Medico);
    ingresoMedico.RCE_OHIngreso_Medico = valCampo(json.RCE_OHIngreso_Medico);
    ingresoMedico.RCE_drogasIngreso_Medico = valCampo(json.RCE_drogasIngreso_Medico);

    // 3. Examen físico general
    ingresoMedico.RCE_pesoIngreso_Medico = valCampo(json.RCE_pesoIngreso_Medico);
    ingresoMedico.RCE_tallaIngreso_Medico = valCampo(json.RCE_tallaIngreso_Medico);
    ingresoMedico.RCE_frecuencia_respiratoriaIngreso_Medico = valCampo(json.RCE_frecuencia_respiratoriaIngreso_Medico);
    ingresoMedico.RCE_presion_arterialIngreso_Medico = valCampo(json.RCE_presion_arterialIngreso_Medico);
    ingresoMedico.RCE_frecuencia_arterialIngreso_Medico = valCampo(json.RCE_frecuencia_arterialIngreso_Medico);
    ingresoMedico.RCE_temperaturaIngreso_Medico = valCampo(json.RCE_temperaturaIngreso_Medico);
    ingresoMedico.RCE_examen_fisicoIngreso_Medico = valCampo(json.RCE_examen_fisicoIngreso_Medico);
    ingresoMedico.RCE_examen_segmentadoIngreso_medico = valCampo(json.RCE_examen_segmentadoIngreso_medico);

    // 4. Medicamentos
    ingresoMedico.RCE_medicamentosIngreso_Medico = valCampo(json.RCE_medicamentosIngreso_Medico);
    
    // 5. Otros
    ingresoMedico.RCE_otrosIngreso_Medico = valCampo(json.RCE_otrosIngreso_Medico);

    // INFORMACIÓN DE REGISTRO
    ingresoMedico.RCE_fecha_hora_creacionIngreso_Medico = valCampo(json.RCE_fecha_hora_creacionIngreso_Medico);
    ingresoMedico.RCE_estadoIngreso_Medico = valCampo(json.RCE_estadoIngreso_Medico);
    ingresoMedico.GEN_idTipo_Estados_Sistemas = valCampo(json.GEN_Tipo_Estados_Sistemas.GEN_idTipo_Estados_Sistemas);

    // Datos de Ingreso Médico
    ingresoMedico.RCE_fecha_horaIngreso_Medico = json.RCE_fecha_horaIngreso_Medico;
    ingresoMedico.GEN_idUbicacion = valCampo(json.GEN_idUbicacion);
    ingresoMedico.RCE_motivo_consultaIngreso_Medico = valCampo(json.RCE_motivo_consultaIngreso_Medico);
    ingresoMedico.RCE_anamnesisIngreso_Medico = valCampo(json.RCE_anamnesisIngreso_Medico);
    ingresoMedico.RCE_diagnosticoIngreso_Medico = valCampo(json.RCE_diagnosticoIngreso_Medico);
    ingresoMedico.RCE_plan_manejoIngreso_Medico = valCampo(json.RCE_plan_manejoIngreso_Medico);
    
    if (ingresoMedico.RCE_idEventos === undefined)
        ingresoMedico.RCE_idEventos = valCampo(json.RCE_idEventos);

    if (ingresoMedico.GEN_idProfesional === undefined)
        ingresoMedico.GEN_idProfesional = json.GEN_idProfesional;
    
}

function ValidarDiagnosticosCIE10() {

    if ($("#tblDiagnosticos tbody tr").length === 0) {
        $("#txtDiagnosticoCIE10").addClass('invalid-input');
        $('html, body').animate({ scrollTop: $("#txtDiagnosticoCIE10").offset().top - 40 }, 500);
        return false;
    } else { 
        $("#txtDiagnosticoCIE10").removeClass('invalid-input');
    }

    return true;

}

async function GuardarIngresoMedico() {

    var valido = true;
    //Bandera en caso de que se deba mostrar el Alert
    alertFlag = 0;
    formatFlag = 0;

    //Aca podría llamarse al modalCargando pero habría que ver la manera para que los Clicks funcionen

    if (!validarCampos("#divTipoIngresoMedico", true) | !validarCampos("#divDatosPaciente", true)) {
        //Mostrar mensaje de campos obligatorios
        alertFlag = 1;

    } else {
        //Validación de formatos en campos de telefono y correo
        let flag = 0;
        if (!$("#txtTelefono").val().match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im) && $("#txtTelefono").val() != "") {
            $("#txtTelefono").addClass('invalid-format');
            flag = 1;
        } else {
            $("#txtTelefono").removeClass('invalid-format');
        }

        if (!$("#txtOtroTelefono").val().match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im) && $("#txtOtroTelefono").val() != "") {
            $("#txtOtroTelefono").addClass('invalid-format');
            flag = 1;
        } else {
            $("#txtOtroTelefono").removeClass('invalid-format');
        }

        if (!$("#txtCorreoElectronico").val().match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/) && $("#txtCorreoElectronico").val() != "") {
            $("#txtCorreoElectronico").addClass('invalid-format');
            flag = 1;
        } else {
            $("#txtCorreoElectronico").removeClass('invalid-format');
        }
        if (flag == 1) {
            formatFlag = 1;
        } else {
            console.log("No hay errores de formato");
            formatFlag = 0;
        }
    }

    $('a[href="#divAntecedentesClinicos"]').trigger("click");
    await sleep(250);

    if (!validarCampos("#divAntecedentesClinicos", true)) {
        alertFlag = 1;
    }

    $('a[href="#divDatosIngreso"]').trigger("click");
    await sleep(250);

    if (!validarCampos("#divDatosIngreso", true)) {
        alertFlag = 1;
    }
        
    
    // GINECOLOGÍA
    if ($('#sltTipoIngresoMedico').val() == 2) {

        $('a[href="#divGinecología"]').trigger("click");
        await sleep(250);

        if (!validarCampos("#divGinecología", true)) {
            alertFlag = 1;
        }

     // OBSTETRICIA
    } else if ($('#sltTipoIngresoMedico').val() == 3) {

        $('a[href="#divObstetricia"]').trigger("click");
        await sleep(250);

        if (!validarCampos("#divObstetricia", true)) {
            alertFlag = 1;
        }
            

     // UCI
    } else if ($('#sltTipoIngresoMedico').val() == 6) {

        $('a[href="#divUCI"]').trigger("click");
        await sleep(250);

        if (!validarCampos("#divUCI", true)) {
            alertFlag = 1;
        }

      // Pediatría
    } else if ($('#sltTipoIngresoMedico').val() == 4) {

        $('a[href="#divPediatría"]').trigger("click");
        await sleep(250);

        if (!validarCampos("#divPediatría", true)) {
            alertFlag = 1;
        }

    }

    if (alertFlag > 0 || formatFlag > 0) {
        return;
    }

    GuardarPaciente();
    debugger;
    if (ingresoMedico.RCE_idIngreso_Medico != undefined) {
        CargarJsonIngresoMedico();
    }

    var method = (ingresoMedico.RCE_idIngreso_Medico != undefined) ? 'PUT' : 'POST';
    var url = GetWebApiUrl() + "RCE_Ingreso_Medico" + ((method === 'PUT') ?
        '/' + ingresoMedico.RCE_idIngreso_Medico :
        '?GEN_idPaciente=' + GetPaciente().GEN_idPaciente);
        
    ingresoMedico.RCE_idTipo_Ingreso_Medico = $("#sltTipoIngresoMedico").val();

    // Antecedentes Clínicos 
    // 1. Morbidos
    ingresoMedico.RCE_medicosIngreso_Medico = valCampo($("#txtMedicos").val())
    ingresoMedico.RCE_cirugiasIngreso_Medico = valCampo($("#txtCirugias").val());
    ingresoMedico.RCE_alergiasIngreso_Medico = valCampo($("#txtAlergias").val());
    ingresoMedico.RCE_hipertensionIngreso_Medico = valCampo($("#txtHipertension").val());
    ingresoMedico.RCE_diabetesIngreso_Medico = valCampo($("#txtDiabetes").val());
    ingresoMedico.RCE_asmaIngreso_Medico = valCampo($("#txtAsma").val());

    // 2. Hábitos
    ingresoMedico.RCE_tabacoIngreso_Medico = $("#chbTabaco").bootstrapSwitch('state') ? valCampo($("#txtTabaco").val()) : null;
    ingresoMedico.RCE_OHIngreso_Medico = $("#chbOh").bootstrapSwitch('state') ? valCampo($("#txtOh").val()) : null;
    ingresoMedico.RCE_drogasIngreso_Medico = $("#chbDrogas").bootstrapSwitch('state') ? valCampo($("#txtDrogas").val()) : null;
        
    // 3. Examen físico general
    ingresoMedico.RCE_pesoIngreso_Medico = valCampo($("#txtPesoExamenFisico").val());
    ingresoMedico.RCE_tallaIngreso_Medico = valCampo($("#txtTallaExamenFisico").val());
    ingresoMedico.RCE_presion_arterialIngreso_Medico = valCampo($("#txtPresionArterialExamenFisico").val());
    ingresoMedico.RCE_frecuencia_arterialIngreso_Medico = valCampo($("#txtFrecuenciaCardiacaExamenFisico").val());
    ingresoMedico.RCE_temperaturaIngreso_Medico = valCampo($("#txtTemperaturaExamenFisico").val());
    ingresoMedico.RCE_examen_fisicoIngreso_Medico = valCampo($("#txtExamenFisico").val());
    ingresoMedico.RCE_examen_segmentadoIngreso_medico = $.trim(textboxio.replace('#txtExamenFisicoSegmentado').content.get());
    ingresoMedico.RCE_frecuencia_respiratoriaIngreso_Medico = valCampo($("#txtFrecuenciaRespiratoriaCardiacaExamenFisico").val());

    // 4. Medicamentos
    ingresoMedico.RCE_medicamentosIngreso_Medico = valCampo($("#txtMedicamentos").val());

    // 5. Otros
    ingresoMedico.RCE_otrosIngreso_Medico = valCampo($("#txtDescripcionOtros").val());

    // Datos de Ingreso Médico
    ingresoMedico.RCE_fecha_horaIngreso_Medico = `${$("#txtFechaIngreso").val()} ${$("#txtHoraIngreso").val()}:00`;
    ingresoMedico.GEN_idUbicacion = parseInt(valCampo($("#sltUbicacion").val()));
    ingresoMedico.RCE_motivo_consultaIngreso_Medico = valCampo($("#txtMotivoConsulta").val());
    ingresoMedico.RCE_anamnesisIngreso_Medico = valCampo($("#txtAnamnesis").val());

    // Hipotesis diagnóstica o Diagnóstico de Ingreso
    ingresoMedico.RCE_diagnosticoIngreso_Medico = valCampo($("#txtDiagnostico").val());
    ingresoMedico.RCE_plan_manejoIngreso_Medico = valCampo($("#txtPlanManejo").val());
        
    // 94 = Ingreso Médico No Validado
    // 95 = Ingreso Médico Validado
    // VALIDA SI ES MÉDICO
    ingresoMedico.GEN_idTipo_Estados_Sistemas = (sSession.CODIGO_PERFIL == 1) ? 95 : 94;

    // Datos de Registro
    if (ingresoMedico.RCE_fecha_hora_creacionIngreso_Medico === null)
        ingresoMedico.RCE_fecha_hora_creacionIngreso_Medico = moment(GetFechaActual()).format("YYYY-MM-DD HH:mm:SS");

    ingresoMedico.RCE_estadoIngreso_Medico = "Activo";
    if (sSession.ID_EVENTO != null)
        ingresoMedico.RCE_idEventos = sSession.ID_EVENTO;
    
    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(ingresoMedico),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            debugger;
            toastr.success('Se registra Ingreso Médico.');
            var RCE_idIngreso_Medico = (data == undefined) ? sSession.ID_INGRESO_MEDICO : data.RCE_idIngreso_Medico;
            GuardarDiagnosticos(RCE_idIngreso_Medico);
            ingresoMedico.RCE_idIngreso_Medico = RCE_idIngreso_Medico;

            // UCI
            if (ingresoMedico.RCE_idTipo_Ingreso_Medico == 6)
                GuardarIMUCI();
            // Obstetricia
            else if (ingresoMedico.RCE_idTipo_Ingreso_Medico == 3)
                GuardarIMObstetricia();
            // Ginecología
            else if (ingresoMedico.RCE_idTipo_Ingreso_Medico == 2)
                GuardarIMGinecologia();
            // Pediatría
            else if (ingresoMedico.RCE_idTipo_Ingreso_Medico == 4)
                GuardarIngresoPediatrico();

            SalirIngresoMedico();

        },
        error: function (jqXHR, status) {
            debugger;
            console.log("Error al guardar Ingreso Médico: " + JSON.stringify(jqXHR));
            SalirIngresoMedico();
        }
    });
    
    
}

function GuardarDiagnosticos(RCE_idIngreso_Medico) {

    if (RCE_idIngreso_Medico !== undefined) {

        for (var i = 0; i < arrayDiagnosticos.length; i++) {

            arrayDiagnosticos[i].RCE_idIngreso_Medico = RCE_idIngreso_Medico;

            var url = GetWebApiUrl() + "RCE_Ingreso_Medico_Diagnostico_CIE10";
            var method = "POST";

            if (arrayDiagnosticos[i].RCE_estadoIngreso_Medico_Diagnostico_CIE10 === "Inactivo") {
                url += "/" + arrayDiagnosticos[i].RCE_idIngreso_Medico_Diagnostico_CIE10;
                method = "PUT";
            } else if (arrayDiagnosticos[i].RCE_idIngreso_Medico_Diagnostico_CIE10 !== 0) {
                continue;
            } else 
                delete arrayDiagnosticos[i].RCE_idIngreso_Medico_Diagnostico_CIE10;
            
            $.ajax({
                type: method,
                url: url,
                data: JSON.stringify(arrayDiagnosticos[i]),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data, status, jqXHR) {
                    toastr.success('Se registra Diagnóstico CIE-10.');
                },
                error: function (jqXHR, status) {
                    alert("Error al guardar Diagnostico CIE10: " + jqXHR.responseText);
                }
            });

        }

    }
    
}

function CancelarIngresoMedico() {
    ShowModalAlerta('CONFIRMACION',
        'Salir del Ingreso Médico',
        '¿Está seguro que desea cancelar el registro?',
        SalirIngresoMedico);
}

function SalirIngresoMedico() {
    window.removeEventListener('beforeunload', bunload, false);
    location.href = ObtenerHost() + "/Vista/ModuloHOS/BandejaHOS.aspx";
}
