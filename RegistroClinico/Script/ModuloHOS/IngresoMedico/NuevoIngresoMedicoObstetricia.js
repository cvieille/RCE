﻿
var jsonIMObstetricia = {};

$(document).ready(function () {
    
    textboxio.replaceAll('#txtExamenFisicoObstetricia',
    {
        paste: { style: 'clean' },
        images: { allowLocal: false }
    });

    textboxio.replace('#txtExamenFisicoObstetricia').content.set(
        `
            <b>VULVA / PERIANAL: </b><br />
            <b>ESPECULOSCOPÍA: </b><br />
            <b>TACTO VAGINAL: </b>
        `);

    textboxio.replaceAll('#txtEcografiaObstetrica',
    {
        paste: { style: 'clean' },
        images: { allowLocal: false }
    });
    
    textboxio.replace('#txtEcografiaObstetrica').content.set(
        `
            <b>CERVICOMETRÍA: </b><br />
            <b>LCN / BIOMETRÍA / EPF: </b><br />
            <b>PRESENTACIÓN: </b><br />
            <b>PLACENTA: </b><br />
            <b>LÍQUIDO AMNIÓTICO: </b><br />
            <b>DOPPLER MATERNO / FETAL: </b>
        `);
    
});

function CargarCombosIMObstetricia() {
    CargarTipoEmbarazo();
    CargarTipoReposo();
    CargarTipoRegimen();
}

function GetJsonIMObstetricia() {
    return jsonIMObstetricia;
}

function CargarJsonIMObstetricia() {

    var json = {};

    if (jsonIMObstetricia.RCE_idIngreso_Medico_Obstetricia !== undefined) {
        
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Ingreso_Medico_Obstetricia/" + jsonIMObstetricia.RCE_idIngreso_Medico_Obstetricia,
            async: false,
            success: function (data, status, jqXHR) {
                json = data;
            },
            error: function (jqXHR, status) {
                console.log("Error al cargar JSON Ingreso Médico Obstetricia: " + console.log(JSON.stringify(jqXHR)));
            }
        });

        console.log(JSON.stringify(json));

        jsonIMObstetricia =
            {
                "RCE_gestacionesIngreso_Medico_Obstetricia": json.RCE_gestacionesIngreso_Medico_Obstetricia,
                "RCE_partosIngreso_Medico_Obstetricia": json.RCE_partosIngreso_Medico_Obstetricia,
                "RCE_abortosIngreso_Medico_Obstetricia": json.RCE_abortosIngreso_Medico_Obstetricia,
                "RCE_fecha_ultima_reglaIngreso_Medico_Obstetricia": json.RCE_fecha_ultima_reglaIngreso_Medico_Obstetricia,
                "RCE_idTipo_Embarazo": json.RCE_idTipo_Embarazo,
                "RCE_descripcion_embarazoIngreso_Medico_Obstetricia": json.RCE_descripcion_embarazoIngreso_Medico_Obstetricia,
                "RCE_otros_antecedentesIngreso_Medico_Obstetricia": json.RCE_otros_antecedentesIngreso_Medico_Obstetricia,
                "RCE_examen_fisico_obstetricoIngreso_Medico_Obstetricia": json.RCE_examen_fisico_obstetricoIngreso_Medico_Obstetricia,
                "RCE_idIngreso_Medico": json.RCE_idIngreso_Medico,
                "RCE_ecografia_obstetricaIngreso_Medico_Obstetricia": json.RCE_ecografia_obstetricaIngreso_Medico_Obstetricia,
                "RCE_rbneIngreso_Medico_Obstetricia": json.RCE_rbneIngreso_Medico_Obstetricia,
                "RCE_duIngreso_Medico_Obstetricia": json.RCE_duIngreso_Medico_Obstetricia,
                "RCE_perfil_biofisicoIngreso_Medico_Obstetricia": json.RCE_perfil_biofisicoIngreso_Medico_Obstetricia,
                "RCE_laboratorioIngreso_Medico_Obstetricia": json.RCE_laboratorioIngreso_Medico_Obstetricia,
                "RCE_reposoIngreso_Medico_Obstetricia": json.RCE_reposoIngreso_Medico_Obstetricia,
                "RCE_regimenIngreso_Medico_Obstetricia": json.RCE_regimenIngreso_Medico_Obstetricia,
                "RCE_prescripcionIngreso_Medico_Obstetricia": json.RCE_prescripcionIngreso_Medico_Obstetricia,
                "RCE_csvIngreso_Medico_Obstetricia": json.RCE_csvIngreso_Medico_Obstetricia,
                "RCE_control_obstetricoIngreso_Medico_Obstetricia": json.RCE_control_obstetricoIngreso_Medico_Obstetricia,
                "RCE_hgtIngreso_Medico_Obstetricia": json.RCE_hgtIngreso_Medico_Obstetricia,
                "RCE_examenesIngreso_Medico_Obstetricia": json.RCE_examenesIngreso_Medico_Obstetricia,
                "RCE_interconsultasIngreso_Medico_Obstetricia": json.RCE_interconsultasIngreso_Medico_Obstetricia,
                "RCE_procedimientosIngreso_Medico_Obstetricia": json.RCE_procedimientosIngreso_Medico_Obstetricia,
                "RCE_estadoIngreso_Medico_Obstetricia": json.RCE_estadoIngreso_Medico_Obstetricia,
                "RCE_operacionalIngreso_Medico_Obstetricia": json.RCE_operacionalIngreso_Medico_Obstetricia,
                "GEN_idTipo_Regimen": json.GEN_idTipo_Regimen,
                "GEN_idTipo_Reposo": json.GEN_idTipo_Reposo
            };
    }

}

function AsignarJsonIMObstetricia() {

    jsonIMObstetricia.RCE_idIngreso_Medico = GetIngresoMedico().RCE_idIngreso_Medico;

    // 1. Antecedentes Gineco-Obstétrico

    jsonIMObstetricia.RCE_fecha_ultima_reglaIngreso_Medico_Obstetricia = valCampo($("#txtFechaUltimaReglaObstetricia").val());
    jsonIMObstetricia.RCE_gestacionesIngreso_Medico_Obstetricia = valCampo(parseInt($("#txtGestacionesObstetricia").val()));
    jsonIMObstetricia.RCE_partosIngreso_Medico_Obstetricia = valCampo(parseInt($("#txtPartosObstetricia").val()));
    jsonIMObstetricia.RCE_abortosIngreso_Medico_Obstetricia = valCampo(parseInt($("#txtAbortosObstetricia").val()));

    if ($("#rdoOperacionalSi").bootstrapSwitch("state"))
        jsonIMObstetricia.RCE_operacionalIngreso_Medico_Obstetricia = "SI";
    else if ($("#rdoOperacionalNo").bootstrapSwitch("state"))
        jsonIMObstetricia.RCE_operacionalIngreso_Medico_Obstetricia = "NO";

    jsonIMObstetricia.RCE_idTipo_Embarazo = valCampo(parseInt($("#sltTipoEmbarazo").val()));
    jsonIMObstetricia.RCE_descripcion_embarazoIngreso_Medico_Obstetricia = valCampo($("#txtDescripcionEmbarazo").val());
    jsonIMObstetricia.RCE_otros_antecedentesIngreso_Medico_Obstetricia = valCampo($("#txtOtrosAntecedentes").val());
    jsonIMObstetricia.RCE_examen_fisico_obstetricoIngreso_Medico_Obstetricia = $.trim(textboxio.replace('#txtExamenFisicoObstetricia').content.get());

    // 2. Exámenes Complementarios

    jsonIMObstetricia.RCE_ecografia_obstetricaIngreso_Medico_Obstetricia = $.trim(textboxio.replace('#txtEcografiaObstetrica').content.get());
    jsonIMObstetricia.RCE_rbneIngreso_Medico_Obstetricia = valCampo($("#txtRbns").val());
    jsonIMObstetricia.RCE_duIngreso_Medico_Obstetricia = valCampo($("#txtDu").val());
    jsonIMObstetricia.RCE_perfil_biofisicoIngreso_Medico_Obstetricia = valCampo($("#txtPerfilBiofisico").val());
    jsonIMObstetricia.RCE_laboratorioIngreso_Medico_Obstetricia = valCampo($("#txtLaboratorio").val());

    // 3. Indicaciones

    jsonIMObstetricia.RCE_reposoIngreso_Medico_Obstetricia = valCampo($("#txtReposo").val());
    jsonIMObstetricia.GEN_idTipo_Reposo = valCampo($("#sltTipoReposo").val());
    jsonIMObstetricia.RCE_regimenIngreso_Medico_Obstetricia = valCampo($("#txtRegimen").val());
    jsonIMObstetricia.GEN_idITpo_Regimen = valCampo($("#sltTipoRegimen").val());
    jsonIMObstetricia.RCE_prescripcionIngreso_Medico_Obstetricia = valCampo($("#txtPretaciones").val());
    jsonIMObstetricia.RCE_csvIngreso_Medico_Obstetricia = valCampo($("#txtCsv").val());
    jsonIMObstetricia.RCE_control_obstetricoIngreso_Medico_Obstetricia = valCampo($("#txtControlObstetrico").val());
    jsonIMObstetricia.RCE_hgtIngreso_Medico_Obstetricia = valCampo($("#txtHgt").val());
    jsonIMObstetricia.RCE_examenesIngreso_Medico_Obstetricia = valCampo($("#txtExamenes").val());
    jsonIMObstetricia.RCE_interconsultasIngreso_Medico_Obstetricia = valCampo($("#txtInterconsultas").val());
    jsonIMObstetricia.RCE_procedimientosIngreso_Medico_Obstetricia = valCampo($("#txtProcedimientos").val());
    jsonIMObstetricia.RCE_estadoIngreso_Medico_Obstetricia = "Activo";

}

function CargarTipoEmbarazo() {

    if ($('#sltTipoEmbarazo').children('option').length === 0) { 

        $("#sltTipoEmbarazo").empty();
        $("#sltTipoEmbarazo").append("<option value='0'>Seleccione</option>");

        try {

            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + "RCE_Tipo_Embarazo/Combo",
                async: false,
                success: function (data, status, jqXHR) {


                    $.each(data, function (key, val) {
                        $("#sltTipoEmbarazo").append("<option value='" + val.RCE_idTipo_Embarazo + "'>" +
                            val.RCE_nombreTipo_Embarazo + "</option>");
                    });

                },
                error: function (jqXHR, status) {
                    console.log("Error al llenar Tipo embarazo: " + jqXHR.responseText);
                }
            });

        } catch (err) {
            alert(err.message);
        }

    }
}

function CargarTipoReposo() {

    if ($('#sltTipoReposo').children('option').length === 0) {

        $("#sltTipoReposo").empty();
        $("#sltTipoReposo").append("<option value='0'>Seleccione</option>");

        try {

            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + "GEN_Tipo_Reposo/Combo",
                async: false,
                success: function (data, status, jqXHR) {


                    $.each(data, function (key, val) {
                        $("#sltTipoReposo").append("<option value='" + val.GEN_idTipo_Reposo + "'>" + val.GEN_nombreTipo_Reposo + "</option>");
                    });

                },
                error: function (jqXHR, status) {
                    console.log("Error al llenar tipo reposo: " + jqXHR.responseText);
                }
            });

        } catch (err) {
            console.log("Catch Error al llenar tipo reposo: " + err.message);
        }

    }

}

function CargarTipoRegimen() {

    if ($('#sltTipoRegimen').children('option').length === 0) {

        $("#sltTipoRegimen").empty();
        $("#sltTipoRegimen").append("<option value='0'>Seleccione</option>");

        try {

            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + "GEN_Tipo_Regimen/Combo",
                async: false,
                success: function (data, status, jqXHR) {
                    
                    $.each(data, function (key, val) {
                        $("#sltTipoRegimen").append("<option value='" + val.GEN_idTipo_Regimen + "'>" + val.GEN_nombreTipo_Regimen + "</option>");
                    });

                },
                error: function (jqXHR, status) {
                    console.log("Error al llenar tipo régimen: " + JSON.stringify(jqXHR));
                }
            });

        } catch (err) {
            console.log("Catch Error al llenar tipo régimen: " + JSON.stringify(err));
        }

    }

}

function GuardarIMObstetricia() {

    CargarJsonIMObstetricia();
    AsignarJsonIMObstetricia();
    var method = (jsonIMObstetricia.RCE_idIngreso_Medico_Obstetricia != undefined) ? 'PUT' : 'POST';
    var url = GetWebApiUrl() + "RCE_Ingreso_Medico_Obstetricia" + ((method === 'PUT') ? '/' + jsonIMObstetricia.RCE_idIngreso_Medico_Obstetricia : '');

    console.log(JSON.stringify(jsonIMObstetricia));

    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(jsonIMObstetricia),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) { },
        error: function (jqXHR, status) {
            console.log("Error al guardar Ingreso Médico Obstetricia: " + JSON.stringify(jqXHR));
        }
    });

}
function CargarIMObstetricia(RCE_idIngreso_Medico_Obstetricia) {

    if (RCE_idIngreso_Medico_Obstetricia !== null) {

        CargarCombosIMObstetricia();
        jsonIMObstetricia.RCE_idIngreso_Medico_Obstetricia = RCE_idIngreso_Medico_Obstetricia;
        CargarJsonIMObstetricia();

        // 1. Antecedentes Gineco-Obstétrico

        $("#txtFechaUltimaReglaObstetricia").val(jsonIMObstetricia.RCE_fecha_ultima_reglaIngreso_Medico_Obstetricia);
        $("#txtGestacionesObstetricia").val(jsonIMObstetricia.RCE_gestacionesIngreso_Medico_Obstetricia);
        $("#txtPartosObstetricia").val(jsonIMObstetricia.RCE_partosIngreso_Medico_Obstetricia);
        $("#txtAbortosObstetricia").val(jsonIMObstetricia.RCE_abortosIngreso_Medico_Obstetricia);

        if (jsonIMObstetricia.RCE_operacionalIngreso_Medico_Obstetricia == "SI")
            $("#rdoOperacionalSi").bootstrapSwitch("state", true);
        else if (jsonIMObstetricia.RCE_operacionalIngreso_Medico_Obstetricia = "NO")
            $("#rdoOperacionalNo").bootstrapSwitch("state", true);

        $("#sltTipoEmbarazo").val(jsonIMObstetricia.RCE_idTipo_Embarazo);
        $("#txtDescripcionEmbarazo").val(jsonIMObstetricia.RCE_descripcion_embarazoIngreso_Medico_Obstetricia);
        $("#txtOtrosAntecedentes").val(jsonIMObstetricia.RCE_otros_antecedentesIngreso_Medico_Obstetricia);
        textboxio.replace('#txtExamenFisicoObstetricia').content.set(jsonIMObstetricia.RCE_examen_fisico_obstetricoIngreso_Medico_Obstetricia);
        

        // 2. Exámenes Complementarios

        textboxio.replace('#txtEcografiaObstetrica').content.set(jsonIMObstetricia.RCE_ecografia_obstetricaIngreso_Medico_Obstetricia);
        $("#txtRbns").val(jsonIMObstetricia.RCE_rbneIngreso_Medico_Obstetricia);
        $("#txtDu").val(jsonIMObstetricia.RCE_duIngreso_Medico_Obstetricia);
        $("#txtPerfilBiofisico").val(jsonIMObstetricia.RCE_perfil_biofisicoIngreso_Medico_Obstetricia);
        $("#txtLaboratorio").val(jsonIMObstetricia.RCE_laboratorioIngreso_Medico_Obstetricia);

        // 3. Indicaciones

        $("#txtReposo").val(jsonIMObstetricia.RCE_reposoIngreso_Medico_Obstetricia);
        $("#sltTipoReposo").val(jsonIMObstetricia.GEN_idTipo_Reposo);
        $("#txtRegimen").val(jsonIMObstetricia.RCE_regimenIngreso_Medico_Obstetricia);
        $("#sltTipoRegimen").val(jsonIMObstetricia.GEN_idTipo_Regimen);
        $("#txtPretaciones").val(jsonIMObstetricia.RCE_prescripcionIngreso_Medico_Obstetricia);
        $("#txtCsv").val(jsonIMObstetricia.RCE_csvIngreso_Medico_Obstetricia);
        $("#txtControlObstetrico").val(jsonIMObstetricia.RCE_control_obstetricoIngreso_Medico_Obstetricia);
        $("#txtHgt").val(jsonIMObstetricia.RCE_hgtIngreso_Medico_Obstetricia);
        $("#txtExamenes").val(jsonIMObstetricia.RCE_examenesIngreso_Medico_Obstetricia);
        $("#txtInterconsultas").val(jsonIMObstetricia.RCE_interconsultasIngreso_Medico_Obstetricia);
        $("#txtProcedimientos").val(jsonIMObstetricia.RCE_procedimientosIngreso_Medico_Obstetricia);

    }

}