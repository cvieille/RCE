﻿
var ingresoMedicoPediatria = {};
var arrayVacunasPaciente = [];

$(document).ready(function () {

    ingresoMedicoPediatria.GEN_idPersona_Cuidador = null;
    ingresoMedicoPediatria.GEN_idPersona_parental_1 = null;
    ingresoMedicoPediatria.GEN_idPersona_parental_2 = null;

    $("#aAgregarPaciente, #sltVacunasPaciente").attr("disabled", "disabled");
    $("#aAgregarPaciente").addClass("disabled");

    $("#tblVacunasPaciente, #tblHospitalizaciones").hide();

    $("#sltIdentificacionCuidador, #sltIdentificacionParental1, #sltIdentificacionParental2").change(function () {

        $(`label[for='${$(this).attr("id")}']`).text($(this).children("option:selected").text());
        var parent = $(this).parent().parent().parent();
        ($(this).val() == '1' || $(this).val() == '4') ? $(`#${parent.attr("id")} .digito`).show() : $(`#${parent.attr("id")} .digito`).hide();
        DeshabilitarPersona(parent.attr("id"), true);
        ReiniciarPersona(parent.attr("id"));

        if ($(this).attr("id") === "sltIdentificacionCuidador")
            $("#txtnumeroDocCuidador, #txtDigCuidador").val("");
        else if ($(this).attr("id") === "sltIdentificacionParental1")
            $("#txtnumeroDocParental1, #txtDigParental1").val("");
        else if ($(this).attr("id") === "sltIdentificacionParental2")
            $("#txtnumeroDocParental2, #txtDigParental2").val("");

    });

    $("#txtnumeroDocCuidador, #txtnumeroDocParental1, #txtnumeroDocParental2").on('input', function () {

        var parent = $(this).parent().parent().parent().parent();
        DeshabilitarPersona(parent.attr("id"), true);
        ReiniciarPersona(parent.attr("id"));

        if ($(this).attr("id") === "txtnumeroDocCuidador") {
            ingresoMedicoPediatria.GEN_idPersona_Cuidador = null;
            $("#txtDigCuidador").val("");
        } else if ($(this).attr("id") === "txtnumeroDocParental1") {
            ingresoMedicoPediatria.GEN_idPersona_parental_1 = null;
            $("#txtDigParental1").val("");
        } else if ($(this).attr("id") === "txtnumeroDocParental2") {
            ingresoMedicoPediatria.GEN_idPersona_parental_2 = null;
            $("#txtDigParental2").val("");
        }
    });

    $("#txtnumeroDocCuidador, #txtnumeroDocParental1, #txtnumeroDocParental2").blur(function () {

        var parent = $(this).parent().parent().parent().parent();
        
        // SI ES RUT O RUT MATERNO
        if ($(`#${parent.attr("id")} select.identificacion`).val() == '1' || $(`#${parent.attr("id")} select.identificacion`).val() == '4') {
            var dig = ObtenerVerificador($(this).val());
            if ($(this).attr("id") === "txtnumeroDocCuidador") {
                ingresoMedicoPediatria.GEN_idPersona_Cuidador = null;
                (dig !== null) ? $(`#txtDigCuidador`).val(dig) : $(`#txtDigCuidador, #txtnumeroDocCuidador`).val("");
            } else if ($(this).attr("id") === "txtnumeroDocParental1") {
                ingresoMedicoPediatria.GEN_idPersona_parental_1 = null;
                (dig !== null) ? $(`#txtDigParental1`).val(dig) : $(`#txtDigParental1, #txtnumeroDocParental1`).val("");
            } else if ($(this).attr("id") === "txtnumeroDocParental2") {
                ingresoMedicoPediatria.GEN_idPersona_parental_2 = null;
                (dig !== null) ? $(`#txtDigParental2`).val(dig) : $(`#txtDigParental2, #txtnumeroDocParental2`).val("");
            }
        }
        
    });

    $("#aBuscarPacienteCuidador, #aBuscarPacienteParental1, #aBuscarPacienteParental2").click(function () {

        var parent = $(this).parent().parent().parent().parent();
        $(`#${parent.attr("id")} [data-required='true']`).attr("data-required", "false");

        if ((($(`#${parent.attr("id")} select.identificacion`).val() == '1' || $(`#${parent.attr("id")} select.identificacion`).val() == '4')
            && $(`#${parent.attr("id")} input.digito`).val() !== "")
            || (($(`#${parent.attr("id")} select.identificacion`).val() == '2' || $(`#${parent.attr("id")} select.identificacion`).val() == '3')
                && $.trim($(`#${parent.attr("id")} input.numero-documento`).val()) !== "")) {

            DeshabilitarPersona(parent.attr("id"), false);

            if ($(this).attr("id") === "aBuscarPacienteCuidador")
                CargarPersona(null, $(`#${parent.attr("id")} select.identificacion`).val(), $(`#${parent.attr("id")} input.numero-documento`).val(), "Cuidador");
            else if ($(this).attr("id") === "aBuscarPacienteParental1")
                CargarPersona(null, $(`#${parent.attr("id")} select.identificacion`).val(), $(`#${parent.attr("id")} input.numero-documento`).val(), "Parental1");
            else if ($(this).attr("id") === "aBuscarPacienteParental2")
                CargarPersona(null, $(`#${parent.attr("id")} select.identificacion`).val(), $(`#${parent.attr("id")} input.numero-documento`).val(), "Parental2");

        }

    });

    $("#txtFechaNacimientoParental1, #txtFechaNacimientoParental2").blur(function () {

        var fechaActual = moment(moment()).format("YYYY-MM-DD");
        var fechaNacimiento = moment(moment($(this).val()).toDate()).format("YYYY-MM-DD");
        var parent = $(this).parent().parent();

        if (moment(fechaActual).isSameOrAfter(fechaNacimiento)) {
            parent.children(".edad").val(CalcularEdad(moment($(this).val()).format("YYYY-MM-DD")).edad);
        } else {
            parent.children(".edad").val('');
            $(this).val('');
        }

    });

    $("#aAgregarPaciente").click(function () {
        if($("#sltVacunasPaciente").val() !== '0')
            AgregarFilaVacunaPaciente("sltVacunasPaciente");
    });
    
    $("#txtDigitoPac").on('input', function () {
        if ($("#sltTipoIngresoMedico").val() == '4') {
            CargarTablaHospitalizaciones();
            CargarVacunasPacientes();
        }
    });

    $("#txtnumerotPac").on('blur', function () {
        if ($("#sltTipoIngresoMedico").val() == '4') {
            CargarTablaHospitalizaciones();
            CargarVacunasPacientes();
        }
    });

});


function CargarPersona(GEN_idPersona, GEN_idIdentificacion, GEN_numero_documentoPersonas, tipoPersona) {

    var json = GetJsonCargarPersona(GEN_idPersona, GEN_idIdentificacion, GEN_numero_documentoPersonas);
    
    if (json !== null) {

        if (tipoPersona === "Cuidador") {
            $("#divResponsable [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_Cuidador = json.GEN_idPersonas;
            $("#txtnombreCuidador").val(json.GEN_nombrePersonas);
            $("#txtApePatCuidador").val(json.GEN_apellido_paternoPersonas);
            $("#txtApeMatCuidador").val(json.GEN_apellido_maternoPersonas);
            $("#txtTelefonoCuidador").val(json.GEN_telefonoPersonas);
            $("#txtDireccionCuidador").val(json.GEN_dir_callePersonas);
            if (GEN_idPersona !== null) {
                DeshabilitarPersona("divResponsable", false);
                $("#sltIdentificacionCuidador").val(json.GEN_idIdentificacion);
                $("#txtnumeroDocCuidador").val(json.GEN_numero_documentoPersonas);
                if (json.GEN_idIdentificacion === 1 || json.GEN_idIdentificacion === 4)
                    $("#txtDigCuidador").val(json.GEN_digitoPersonas);
            }
        } else if (tipoPersona === "Parental1") {
            $("#divParental1 [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_parental_1 = json.GEN_idPersonas;
            $("#txtnombreParental1").val(json.GEN_nombrePersonas);
            $("#txtApePatParental1").val(json.GEN_apellido_paternoPersonas);
            $("#txtApeMatParental1").val(json.GEN_apellido_maternoPersonas);
            $("#txtFechaNacimientoParental1").val(moment(json.GEN_fecha_nacimientoPersonas).format('YYYY-MM-DD'));
            $("#txtEdadParental1").val(CalcularEdad(moment($("#txtFechaNacimientoParental1").val()).format("YYYY-MM-DD")).edad);
            ValTipoCampo($("#sltActividadParental1"), json.GEN_idCategorias_Ocupacion);
            ValTipoCampo($("#sltEducaciónParental1"), json.GEN_idTipo_Escolaridad);
            if (GEN_idPersona !== null) {
                DeshabilitarPersona("divParental1", false);
                $("#sltIdentificacionParental1").val(json.GEN_idIdentificacion);
                $("#txtnumeroDocParental1").val(json.GEN_numero_documentoPersonas);
                if (json.GEN_idIdentificacion === 1 || json.GEN_idIdentificacion === 4)
                    $("#txtDigParental1").val(json.GEN_digitoPersonas);
            }
        } else if (tipoPersona === "Parental2") {
            $("#divParental2 [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_parental_2 = json.GEN_idPersonas;
            $("#txtnombreParental2").val(json.GEN_nombrePersonas);
            $("#txtApePatParental2").val(json.GEN_apellido_paternoPersonas);
            $("#txtApeMatParental2").val(json.GEN_apellido_maternoPersonas);
            $("#txtFechaNacimientoParental2").val(moment(json.GEN_fecha_nacimientoPersonas).format('YYYY-MM-DD'));
            $("#txtEdadParental2").val(CalcularEdad(moment($("#txtFechaNacimientoParental2").val()).format("YYYY-MM-DD")).edad);
            ValTipoCampo($("#sltActividadParental2"), json.GEN_idCategorias_Ocupacion);
            ValTipoCampo($("#sltEducaciónParental2"), json.GEN_idTipo_Escolaridad);
            if (GEN_idPersona !== null) {
                DeshabilitarPersona("divParental2", false);
                $("#sltIdentificacionParental2").val(json.GEN_idIdentificacion);
                $("#txtnumeroDocParental2").val(json.GEN_numero_documentoPersonas);
                if (json.GEN_idIdentificacion === 1 || json.GEN_idIdentificacion === 4)
                    $("#txtDigParental2").val(json.GEN_digitoPersonas);
            }
        }

    } else {

        if (tipoPersona === "Cuidador") {
            $("#divResponsable [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_Cuidador = 0;
        } else if (tipoPersona === "Parental1") {
            $("#divParental1 [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_parental_1 = 0;
        } else if (tipoPersona === "Parental2") {
            $("#divParental2 [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_parental_2 = 0;
        }

    }
}

function CargarPediatria() {

    CargarIdentificacionPersonas();
    CargarComboEstablecimientos();
    CargarComboTipoRecienNacido();
    CargarComboTipoParto();
    CargarComboTipoVacuna();
    CargarComboTipoEdad();
    CargarrComboCategoriaOcupacion();
    CargarComboTipoEscolaridad();
    CargarComboEstadoConyugal();
    CargarComboTipoDesarrolloPsicoMotor();
    CargarComboTipoDatosNutricionales();
    CargarTablaHospitalizaciones();
    CargarVacunasPacientes();
    
}

function CargarIdentificacionPersonas() {

    $("#sltIdentificacionCuidador, #sltIdentificacionParental1, #sltIdentificacionParental2").empty();
    $("#sltIdentificacionCuidador, #sltIdentificacionParental1, #sltIdentificacionParental2").append($("#sltIdentificacion").html());
    $("#sltIdentificacionCuidador, #sltIdentificacionParental1, #sltIdentificacionParental2").val("1");
    $(`label[for='sltIdentificacionCuidador']`).text($("#sltIdentificacionCuidador").children("option:selected").text());
    $(`label[for='sltIdentificacionParental1']`).text($("#sltIdentificacionParental1").children("option:selected").text());
    $(`label[for='sltIdentificacionParental2']`).text($("#sltIdentificacionParental2").children("option:selected").text());
    $("#sltIdentificacionCuidador option[value='5'], #sltIdentificacionParental1 option[value='5'], #sltIdentificacionParental2 option[value='5']").remove();

}

function DeshabilitarPersona(divPadre, esDisabled) {
    (!esDisabled) ? $(`#${divPadre} .datos-persona`).removeAttr("disabled") : $(`#${divPadre} .datos-persona`).attr("disabled", "disabled");
}

function ReiniciarPersona(divPadre) {
    $(`#${divPadre} input.datos-persona`).val("");
    $(`#${divPadre} select.datos-persona`).val("0");
    $(`#${divPadre} [data-required='true']`).attr("data-required", "false");
}

function GetJsonCargarPersona(GEN_idPersona, GEN_idIdentificacion, GEN_numero_documento) {

    var url = (GEN_idPersona === null) ? `${GetWebApiUrl()}GEN_Personas/Buscar?idIdentificacion=${GEN_idIdentificacion}&numeroDocumento=${GEN_numero_documento}`:
        `${GetWebApiUrl()}GEN_Personas/${GEN_idPersona}`;
    var d = null;

    // SI ES 0 ES PORQUE SE CREARÁ UNA PERSONA NUEVA
    if (GEN_idPersona !== 0) {
        $.ajax({
            type: "GET",
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                if ($.isArray(data)) {
                    if (data.length > 0)
                        d = data[0];
                } else {
                    d = data;
                }
            },
            error: function (request, status) {
                console.log("Error: " + JSON.stringify(request));
            }
        });

    } else {
        d = {
            GEN_numero_documentoPersonas: null,
            GEN_digitoPersonas: null,
            GEN_nombrePersonas: null,
            GEN_apellido_paternoPersonas: null,
            GEN_apellido_maternoPersonas: null,
            GEN_telefonoPersonas: null,
            GEN_emailPersonas: null,
            GEN_idIdentificacion: null,
            GEN_idSexo: null,
            GEN_estadoPersonas: "Activo",
            GEN_dir_callePersonas: null,
            GEN_idCategorias_Ocupacion: null,
            GEN_idTipo_Escolaridad: null,
            GEN_fecha_nacimientoPersonas: null
        }
    }

    if(d !== null)
        d.GEN_fecha_actualizacionPersonas = GetFechaActual();
    
    return d;

}

function CargarComboEstablecimientos() {
    
    $("#sltConsultorioOrigen").empty();
    $("#sltConsultorioOrigen").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Establecimiento/Combo",
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (key, val) {
                $("#sltConsultorioOrigen").append("<option value='" + val.Id + "'>" +
                    val.Valor + "</option>");
            });

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar Establecimiento: " + jqXHR.responseText);
        }
    });
}

function CargarComboTipoRecienNacido() {

    $("#sltRecienNacido").empty();
    $("#sltRecienNacido").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Tipo_Recien_Nacido/Combo",
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (key, val) {
                $("#sltRecienNacido").append(
                    `<option value='${val.Id}'>
                        ${val.Valor} 
                    </option>`);
            });

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar Tipo recien nacido: " + jqXHR.responseText);
        }
    });

}

function CargarComboTipoParto() {

    $("#sltParto").empty();
    $("#sltParto").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Tipo_Parto/Combo",
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (key, val) {
                $("#sltParto").append(
                    `<option value='${val.GEN_idTipo_Parto}'>
                        ${val.GEN_nombreTipo_Parto} 
                    </option>`);
            });

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar Tipo Parto: " + jqXHR.responseText);
        }
    });

}

function CargarComboTipoVacuna() {

    $("#sltVacunasPaciente").empty();
    $("#sltVacunasPaciente").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Tipo_Vacuna/Combo",
        async: false,
        success: function (data, status, jqXHR) {
            
            $.each(data, function (key, val) {
                $("#sltVacunasPaciente").append(
                    `<option value='${val.GEN_idTipo_Vacuna}'>
                        ${val.GEN_nombreTipo_Vacuna} 
                    </option>`);
            });

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar Tipo Vacuna: " + jqXHR.responseText);
        }
    });

}

function CargarComboTipoEdad() {

    $("#sltTipoEdadDuracionLMaterna, #sltInicioLArtificial").empty();

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Tipo_Edad/Combo",
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (key, val) {
                $("#sltTipoEdadDuracionLMaterna, #sltInicioLArtificial").append(
                    `<option value='${val.GEN_idTipo_Edad}'>
                        ${val.GEN_nombreTipo_Edad} 
                    </option>`);
            });

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar Tipo Edad: " + jqXHR.responseText);
        }
    });

}

function CargarrComboCategoriaOcupacion() {

    $("#sltActividadParental1, #sltActividadParental2").empty();
    $("#sltActividadParental1, #sltActividadParental2").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Tipo_Categoria_Ocupacion/Combo",
        async: false,
        success: function (data, status, jqXHR) {
           
            $.each(data, function (key, val) {
                $("#sltActividadParental1, #sltActividadParental2").append(
                    `<option value='${val.Id}'>
                        ${val.Valor} 
                    </option>`);
            });

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar Categoria Ocupación: " + jqXHR.responseText);
        }
    });
}

function CargarComboTipoEscolaridad() {

    $("#sltEducaciónParental1, #sltEducaciónParental2").empty();
    $("#sltEducaciónParental1, #sltEducaciónParental2").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Tipo_Escolaridad/Combo",
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (key, val) {
                $("#sltEducaciónParental1, #sltEducaciónParental2").append(
                    `<option value='${val.Id}'>
                        ${val.Valor} 
                    </option>`);
            });

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar Tipo Escolaridad: " + jqXHR.responseText);
        }
    });
}

function CargarComboEstadoConyugal() {

    $("#sltSituacionMarital").empty();
    $("#sltSituacionMarital").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Estado_Conyugal/Combo",
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (key, val) {
                $("#sltSituacionMarital").append(
                    `<option value='${val.Id}'>
                        ${val.Valor} 
                    </option>`);
            });

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar Estado conyugal: " + jqXHR.responseText);
        }
    });

}

function CargarComboTipoDesarrolloPsicoMotor() {

    $("#sltDSM").empty();
    $("#sltDSM").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Tipo_Desarrollo_Psico_Motor/Combo",
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (key, val) {
                $("#sltDSM").append(
                    `<option value='${val.RCE_idTipo_Desarrollo_Psico_Motor}'>
                        ${val.RCE_nombreTipo_Desarrollo_Psico_Motor} 
                    </option>`);
            });

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar Tipo Desarrollo Psico-Motor: " + jqXHR.responseText);
        }
    });
}

function CargarComboTipoDatosNutricionales() {

    $("#sltNutricional").empty();
    $("#sltNutricional").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Tipo_Datos_Nutricionales/Combo",
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (key, val) {
                $("#sltNutricional").append(
                    `<option value='${val.GEN_idTipo_Datos_Nutricionales}'>
                        ${val.GEN_nombreTipo_Datos_Nutricionales} 
                    </option>`);
            });

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar Tipo Datos Nutricionales: " + jqXHR.responseText);
        }
    });
}

function CargariMPediatrico(RCE_idIngreso_Medico_Pediatrico) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_Ingreso_Medico_Pediatrico/${RCE_idIngreso_Medico_Pediatrico}`,
        async: false,
        success: function (data, status, jqXHR) {

            var json = data;

            ingresoMedicoPediatria.RCE_idIngreso_Medico_Pediatrico = RCE_idIngreso_Medico_Pediatrico;

            // INFORMACIÓN CUIDADOR
            CargarPersona(json.GEN_idPersona_Cuidador, null, null, "Cuidador");
            $("#txtparentescoCuidador").val(json.RCE_parentesco_cuidadorIngreso_Medico_Pediatrico);

            // ANTECEDENTES PERSONALES
            $("#sltConsultorioOrigen").val(json.GEN_idEstablecimiento_Origen);
            $("#sltRecienNacido").val(json.RCE_idTipo_Recien_Nacido);
            $("#sltParto").val(json.GEN_idTipo_Parto);
            $("#txtPatologiaPerinatal").val(json.RCE_patologia_perinatalIngreso_Medico_Pediatrico);
            $("#txtOtrosPediatria").val(json.RCE_otros_antecedentes_personalesIngreso_Medico_Pediatrico);
            $("#txtDuracionLMaterna").val(json.RCE_duracion_lactancia_añosIngreso_Medico_Pediatrico);
            $("#sltTipoEdadDuracionLMaterna").val(json.GEN_idTipo_Edad_duracion_lactancia_añosIngreso_Medico_Pediatrico);
            $("#txtInicioLArtificial").val(json.RCE_inicio_lactancia_añosIngreso_Medico_Pediatrico);
            $("#sltInicioLArtificial").val(json.GEN_idTipo_Edad_inicio_lactancia_añosIngreso_Medico_Pediatrico);
            $("#txt1Comida").val(moment(json.RCE_fecha_primera_comidaIngreso_Medico_Pediatrico).format('YYYY-MM-DD'));
            $("#txt2Comida").val(moment(json.RCE_fecha_segunda_comidaIngreso_Medico_Pediatrico).format('YYYY-MM-DD'));

            $("#rdoConvulsionesSi").bootstrapSwitch('state', (json.RCE_convulsiones_personalIngreso_Medico_Pediatrico === "SI"));
            $("#rdoConvulsionesNo").bootstrapSwitch('state', (json.RCE_convulsiones_personalIngreso_Medico_Pediatrico === "NO"));

            $("#rdoSBORSi").bootstrapSwitch('state', (json.RCE_SBOR_personalIngreso_Medico_Pediatrico === "SI"));
            $("#rdoSBORNo").bootstrapSwitch('state', (json.RCE_SBOR_personalIngreso_Medico_Pediatrico === "NO"));

            $("#rdoNeumoniasSi").bootstrapSwitch('state', (json.RCE_neumonias_personalIngreso_Medico_Pediatrico === "SI"));
            $("#rdoNeumoniasNo").bootstrapSwitch('state', (json.RCE_neumonias_personalIngreso_Medico_Pediatrico === "NO"));

            $("#rdoGastrointestSi").bootstrapSwitch('state', (json.RCE_gastrointest_personaltIngreso_Medico_Pediatrico === "SI"));
            $("#rdoGastrointestNo").bootstrapSwitch('state', (json.RCE_gastrointest_personaltIngreso_Medico_Pediatrico === "NO"));

            $("#rdoUrinarioSi").bootstrapSwitch('state', (json.RCE_urinario_personalIngreso_Medico_Pediatrico === "SI"));
            $("#rdoUrinarioNo").bootstrapSwitch('state', (json.RCE_urinario_personalIngreso_Medico_Pediatrico === "NO"));

            // EXAMEN FÍSICO
            $("#txtTannerExamenFisico").val(json.RCE_tannerIngreso_Medico_Pediatrico);
            $("#txtDenticionExamenFisico").val(json.RCE_denticionIngreso_Medico_Pediatrico);
            $("#txtPArtExamenFisico").val(json.RCE_partIngreso_Medico_Pediatrico);
            $("#txtSaturometriaExamenFisico").val(json.RCE_saturometriaIngreso_Medico_Pediatrico);
            $("#txtScoreExamenFisico").val(json.RCE_scoreIngreso_Medico_Pediatrico);

            // ANTECEDENTES FAMILIARES
            CargarVacunasPacientes();

            $("#rdoAlergiasSi").bootstrapSwitch('state', (json.RCE_alergias_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoAlergiasNo").bootstrapSwitch('state', (json.RCE_alergias_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#rdoAsmaSi").bootstrapSwitch('state', (json.RCE_asma_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoAsmaNo").bootstrapSwitch('state', (json.RCE_asma_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#rdoEpilepsiaSi").bootstrapSwitch('state', (json.RCE_epilepsia_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoEpilepsiaNo").bootstrapSwitch('state', (json.RCE_epilepsia_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#rdoDiabetesSi").bootstrapSwitch('state', (json.RCE_diabetes_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoDiabetesNo").bootstrapSwitch('state', (json.RCE_diabetes_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#rdoCardiovascSi").bootstrapSwitch('state', (json.RCE_cardiovasc_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoCardiovascNo").bootstrapSwitch('state', (json.RCE_cardiovasc_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#rdoHTASi").bootstrapSwitch('state', (json.RCE_hta_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoHTANo").bootstrapSwitch('state', (json.RCE_hta_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#txtMuertesFamiliares").val(json.RCE_muertes_familiaresIngreso_Medico_Pediatrico);
            $("#txtAlergiaFaramco").val(json.RCE_alergias_farmacos_familiaresIngreso_Medico_Pediatrico);
            $("#txtOtrosAntecendentesPersonales").val(json.RCE_otros_familiaresIngreso_Medico_Pediatrico);

            CargarTablaHospitalizaciones();

            CargarPersona(json.GEN_idPersona_parental_1, null, null, "Parental1");
            CargarPersona(json.GEN_idPersona_parental_2, null, null, "Parental2");

            $("#sltSituacionMarital").val(json.GEN_idEstado_Conyugal);
            $("#sltDSM").val(json.RCE_idTipo_Desarrollo_Psico_Motor);
            $("#sltNutricional").val(json.GEN_idTipo_Datos_Nutricionales);

        }
    });
}

function CargarTablaHospitalizaciones() {
    
    if (GetPaciente().GEN_idPaciente !== undefined) {

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/DetallePaciente/${GetPaciente().GEN_idPaciente}`,
            async: false,
            success: function (data, status, jqXHR) {

                $("#tblHospitalizaciones tbody").empty();

                if (data.length > 0) {

                    var html = "";

                    $.each(data, function () {
                        $("#tblHospitalizaciones tbody").append(
                        `<tr class='text-center'>
                            <td>${ (this.RCE_diagnosticoIngreso_Medico !== undefined) ? this.RCE_diagnosticoIngreso_Medico : this.HOS_diagnostico_principalEpicrisis}</td>
                            <td>${ CalcularEdad(this.GEN_fec_nacimientoPaciente).edad }</td>
                            <td>${ moment(this.HOS_fecha_ingreso_realHospitalizacion).format('DD-MM-YYYY')}</td>
                            <td>${ (this.HOS_fecha_egreso_realHospitalizacion === null) ? "" : moment(this.HOS_fecha_egreso_realHospitalizacion).format('DD-MM-YYYY')}</td>
                            <td>${ this.HOS_diasHospitalizacion}</td>
                        </tr>`);
                    });
                    
                    $("#tblHospitalizaciones").show();
                    $("#divHospitalizaciones").hide();

                } else {

                    $("#tblHospitalizaciones").hide();
                    $("#divHospitalizaciones").show();

                }

            }
        });

    } else {

        $("#tblHospitalizaciones").hide();
        $("#divHospitalizaciones").show();

    }

}

function CargarVacunasPacientes() {

    if (GetPaciente().GEN_idPaciente !== undefined) {

        $("#aAgregarPaciente, #sltVacunasPaciente").removeAttr("disabled");
        $("#aAgregarPaciente").removeClass("disabled");

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente_Tipo_Vacuna/GEN_PACIENTE/${GetPaciente().GEN_idPaciente}`,
            async: false,
            success: function (data, status, jqXHR) {

                for (var i = 0; i < data.length; i++)
                    AgregarFila(data[i]);

            }
        });

    }

}

function AgregarFilaVacunaPaciente(slt) {

    if (!ExisteVacunaPaciente($(`#${slt}`).val())) {
        var json = {
            GEN_idPaciente_Tipo_Vacuna : 0,
            GEN_idTipo_Vacuna: $(`#${slt}`).val(),
            GEN_nombreTipo_Vacuna: $(`#${slt} option:selected`).text(),
            GEN_estadoPaciente_Tipo_Vacuna : "Activo"
        };
        AgregarFilaVP(json);
    }

}

function ExisteVacunaPaciente(GEN_idTipo_Vacuna) {

    var existeVacuna = false;

    $("#tblVacunasPaciente tbody tr").each(function (index) {
        if ($(this).attr("data-idTipoVacuna") == GEN_idTipo_Vacuna)
            existeVacuna = true;
    });

    return existeVacuna;

}

function AgregarFilaVP(json) {

    try {
        
        var idTr = "trVacunaPaciente_" + $("#tblVacunasPaciente tbody").length;

        var tr =
        `<tr id='${idTr}' class='text-center' data-idTipoVacuna='${json.GEN_idTipo_Vacuna}' data-idPacienteTipoVacuna = '${json.GEN_idPaciente_Tipo_Vacuna}'>
            <td></td>
            <td>${json.GEN_nombreTipo_Vacuna}</td>  
            <td></td>
            <td class='text-center'>
                <a class='btn btn-danger' data-idPacienteTipoVacuna = '${json.GEN_idPaciente_Tipo_Vacuna}'>
                    <i class='fa fa-minus-circle'></i>
                </a>
            </td>
        </tr>`;

        $("#tblVacunasPaciente tbody").append(tr);
        $("#tblVacunasPaciente").show();
        $("#divVacunasPaciente").hide();

        arrayVacunasPaciente.push({
            GEN_idPaciente_Tipo_Vacuna: json.GEN_idPaciente_Tipo_Vacuna,
            GEN_idTipo_Vacuna: json.GEN_idTipo_Vacuna,
            GEN_estadoPaciente_Tipo_Vacuna: "Activo"
        });

        $(`#${idTr} td a`).click(function () {

            $(this).parent().parent().remove();

            if ($(this).attr("data-idPacienteTipoVacuna") !== 0) {
                for (var i = 0; i < arrayVacunasPaciente.length; i++) {

                    if (arrayVacunasPaciente[i].GEN_idPaciente_Tipo_Vacuna == $(this).attr("data-idPacienteTipoVacuna")) {
                        arrayVacunasPaciente[i].GEN_estadoPaciente_Tipo_Vacuna = "Inactivo";
                        break;
                    }

                }
            }

            if ($.trim($("#tblVacunasPaciente tbody").html()) === "") {
                $("#tblVacunasPaciente").hide();
                $("#divVacunasPaciente").show();
            }

            $(this).unbind();

        });

    } catch (ex) {
        console.log(JSON.stringify(ex));
    }

}

function GuardarPersona(GEN_idPersonas, tipoPersona) {

    var url = (GEN_idPersonas === 0) ? `${GetWebApiUrl()}GEN_Personas` : `${GetWebApiUrl()}GEN_Personas/${GEN_idPersonas}`;
    var method = (GEN_idPersonas === 0) ? "POST" : "PUT";
    var json = GetJsonCargarPersona(GEN_idPersonas, null, null);

    var persona = {
        GEN_telefonoPersonas: json.GEN_telefonoPersonas,
        GEN_emailPersonas: json.GEN_emailPersonas,
        GEN_idSexo: json.GEN_idSexo,
        GEN_estadoPersonas: json.GEN_estadoPersonas,
        GEN_dir_callePersonas: json.GEN_dir_callePersonas,
        GEN_idCategorias_Ocupacion: json.GEN_idCategorias_Ocupacion,
        GEN_idTipo_Escolaridad: json.GEN_idTipo_Escolaridad,
        GEN_fecha_nacimientoPersonas: json.GEN_fecha_nacimientoPersonas,
        GEN_fecha_actualizacionPersonas: json.GEN_fecha_actualizacionPersonas 
    };

    if (tipoPersona === "Cuidador") {
        persona.GEN_idIdentificacion = valCampo($("#sltIdentificacionCuidador").val());
        persona.GEN_numero_documentoPersonas = valCampo($("#txtnumeroDocCuidador").val());
        persona.GEN_digitoPersonas = valCampo($("#txtDigCuidador").val());
        persona.GEN_nombrePersonas = valCampo($("#txtnombreCuidador").val());
        persona.GEN_apellido_paternoPersonas = valCampo($("#txtApePatCuidador").val());
        persona.GEN_apellido_maternoPersonas = valCampo($("#txtApeMatCuidador").val());
        persona.GEN_telefonoPersonas = valCampo($("#txtTelefonoCuidador").val());
        persona.GEN_dir_callePersonas = valCampo($("#txtDireccionCuidador").val());

    } else if (tipoPersona === "Parental1") {

        persona.GEN_idIdentificacion = valCampo($("#sltIdentificacionParental1").val());
        persona.GEN_numero_documentoPersonas = valCampo($("#txtnumeroDocParental1").val());
        persona.GEN_digitoPersonas = valCampo($("#txtDigParental1").val());
        persona.GEN_nombrePersonas = valCampo($("#txtnombreParental1").val());
        persona.GEN_apellido_paternoPersonas = valCampo($("#txtApePatParental1").val());
        persona.GEN_apellido_maternoPersonas = valCampo($("#txtApeMatParental1").val());
        persona.GEN_fecha_nacimientoPersonas = ($("#txtFechaNacimientoParental1").val() === "") ? null : moment($("#txtFechaNacimientoParental1").val()).format("YYYY-MM-DD");
        persona.GEN_idCategorias_Ocupacion = valCampo($("#sltActividadParental1").val());
        persona.GEN_idTipo_Escolaridad = valCampo($("#sltEducaciónParental1").val());
        
    } else if (tipoPersona === "Parental2") {

        persona.GEN_idIdentificacion = valCampo($("#sltIdentificacionParental2").val());
        persona.GEN_numero_documentoPersonas = valCampo($("#txtnumeroDocParental2").val());
        persona.GEN_digitoPersonas = valCampo($("#txtDigParental2").val());
        persona.GEN_nombrePersonas = valCampo($("#txtnombreParental2").val());
        persona.GEN_apellido_paternoPersonas = valCampo($("#txtApePatParental2").val());
        persona.GEN_apellido_maternoPersonas = valCampo($("#txtApeMatParental2").val());
        persona.GEN_fecha_nacimientoPersonas = ($("#txtFechaNacimientoParental2").val() === "") ? null : moment($("#txtFechaNacimientoParental1").val()).format("YYYY-MM-DD");
        persona.GEN_idCategorias_Ocupacion = valCampo($("#sltActividadParental2").val());
        persona.GEN_idTipo_Escolaridad = valCampo($("#sltEducaciónParental2").val());

    }

    if (GEN_idPersonas !== 0)
        persona.GEN_idPersonas = GEN_idPersonas;
    
    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(persona),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {

            if (data != undefined) {
                if (tipoPersona === "Cuidador")
                    ingresoMedicoPediatria.GEN_idPersona_Cuidador = data.GEN_idPersonas;
                else if (tipoPersona === "Parental1")
                    ingresoMedicoPediatria.GEN_idPersona_parental_1 = data.GEN_idPersonas;
                else if (tipoPersona === "Parental2")
                    ingresoMedicoPediatria.GEN_idPersona_parental_2 = data.GEN_idPersonas;
            }
            
        },
        error: function (jqXHR, status) {
            console.log("Error al guardar Persona: " + jqXHR.responseText);
        }
    });

}

function GuardarVacunasPaciente() {

    if (GetPaciente().GEN_idPaciente !== undefined) {

        for (var i = 0; i < arrayVacunasPaciente.length; i++) {

            arrayVacunasPaciente[i].GEN_idPaciente = GetPaciente().GEN_idPaciente;

            var url = GetWebApiUrl() + "GEN_Paciente_Tipo_Vacuna";
            var method = "POST";

            if (arrayVacunasPaciente[i].GEN_estadoPaciente_Tipo_Vacuna === "Inactivo") {
                url += "/" + arrayVacunasPaciente[i].GEN_idPaciente_Tipo_Vacuna;
                method = "PUT";
            } else if (arrayVacunasPaciente[i].GEN_idPaciente_Tipo_Vacuna !== 0) {
                continue;
            } else
                delete arrayVacunasPaciente[i].GEN_idPaciente_Tipo_Vacuna;

            $.ajax({
                type: method,
                url: url,
                data: JSON.stringify(arrayVacunasPaciente[i]),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data, status, jqXHR) {
                    toastr.success('Se registra Vacuna de Paciente.');
                },
                error: function (jqXHR, status) {
                    console.log("Error al guardar Vacuna de Paciente: " + jqXHR.responseText);
                }
            });

        }

    }

}

//function GetJsonIngresoMedicoPeditrico() {

//    var json = {};

//    if (ingresoMedicoPediatria.RCE_idIngreso_Medico_Pediatrico !== undefined) {

//        $.ajax({
//            type: 'GET',
//            url: `${GetWebApiUrl()}RCE_Ingreso_Medico_Pediatrico/${ingresoMedicoPediatria.RCE_idIngreso_Medico_Pediatrico}`,
//            async: false,
//            success: function (data, status, jqXHR) {
//                json = data
//            }
//        });

//    }

//    return json;
    
//}

function CargarJsonIngresoMedicoPediatria() {

    //var json = GetJsonIngresoMedicoPeditrico();
    //console.log(JSON.stringify(json));

    //// INFORMACIÓN CUIDADOR
    ingresoMedicoPediatria.RCE_parentesco_cuidadorIngreso_Medico_Pediatrico = valCampo($("#txtparentescoCuidador").val());

    // ANTECEDENTES PERSONALES
    ingresoMedicoPediatria.GEN_idEstablecimiento_Origen = valCampo($("#sltConsultorioOrigen").val());
    ingresoMedicoPediatria.RCE_idTipo_Recien_Nacido = valCampo($("#sltRecienNacido").val());
    ingresoMedicoPediatria.GEN_idTipo_Parto = valCampo($("#sltParto").val());
    ingresoMedicoPediatria.RCE_patologia_perinatalIngreso_Medico_Pediatrico = valCampo($("#txtPatologiaPerinatal").val());
    ingresoMedicoPediatria.RCE_otros_antecedentes_personalesIngreso_Medico_Pediatrico = valCampo($("#txtOtrosPediatria").val());
    ingresoMedicoPediatria.RCE_duracion_lactancia_añosIngreso_Medico_Pediatrico = valCampo($("#txtDuracionLMaterna").val());
    ingresoMedicoPediatria.GEN_idTipo_Edad_duracion_lactancia_añosIngreso_Medico_Pediatrico = valCampo($("#sltTipoEdadDuracionLMaterna").val());
    ingresoMedicoPediatria.RCE_inicio_lactancia_añosIngreso_Medico_Pediatrico = valCampo($("#txtInicioLArtificial").val());
    ingresoMedicoPediatria.GEN_idTipo_Edad_inicio_lactancia_añosIngreso_Medico_Pediatrico = valCampo($("#sltInicioLArtificial").val());
    ingresoMedicoPediatria.RCE_fecha_primera_comidaIngreso_Medico_Pediatrico = ($("#txt1Comida").val() === "") ? null : moment($("#txt1Comida").val()).format('YYYY-MM-DD');
    ingresoMedicoPediatria.RCE_fecha_segunda_comidaIngreso_Medico_Pediatrico = ($("#txt2Comida").val() === "") ? null : moment($("#txt2Comida").val()).format('YYYY-MM-DD');

    ingresoMedicoPediatria.RCE_convulsiones_personalIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoConvulsionesSi"), $("#rdoConvulsionesNo"));
    ingresoMedicoPediatria.RCE_SBOR_personalIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoSBORSi"), $("#rdoSBORNo"));
    ingresoMedicoPediatria.RCE_neumonias_personalIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoNeumoniasSi"), $("#rdoNeumoniasNo"));
    ingresoMedicoPediatria.RCE_gastrointest_personaltIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoGastrointestSi"), $("#rdoGastrointestNo"));
    ingresoMedicoPediatria.RCE_urinario_personalIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoUrinarioSi"), $("#rdoUrinarioNo"));

    // EXAMEN FÍSICO
    ingresoMedicoPediatria.RCE_tannerIngreso_Medico_Pediatrico = valCampo($("#txtTannerExamenFisico").val());
    ingresoMedicoPediatria.RCE_denticionIngreso_Medico_Pediatrico = valCampo($("#txtDenticionExamenFisico").val());
    ingresoMedicoPediatria.RCE_partIngreso_Medico_Pediatrico = valCampo($("#txtPArtExamenFisico").val());
    ingresoMedicoPediatria.RCE_saturometriaIngreso_Medico_Pediatrico = valCampo($("#txtSaturometriaExamenFisico").val());
    ingresoMedicoPediatria.RCE_scoreIngreso_Medico_Pediatrico = valCampo($("#txtScoreExamenFisico").val());

    // ANTECEDENTES FAMILIARES

    ingresoMedicoPediatria.RCE_alergias_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoAlergiasSi"), $("#rdoAlergiasNo"));
    ingresoMedicoPediatria.RCE_asma_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoAsmaSi"), $("#rdoAsmaNo"));
    ingresoMedicoPediatria.RCE_epilepsia_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoEpilepsiaSi"), $("#rdoEpilepsiaNo"));
    ingresoMedicoPediatria.RCE_diabetes_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoDiabetesSi"), $("#rdoDiabetesNo"));
    ingresoMedicoPediatria.RCE_cardiovasc_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoCardiovascSi"), $("#rdoCardiovascNo"));
    ingresoMedicoPediatria.RCE_hta_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoHTASi"), $("#rdoHTANo"));

    ingresoMedicoPediatria.RCE_muertes_familiaresIngreso_Medico_Pediatrico = valCampo($("#txtMuertesFamiliares").val());
    ingresoMedicoPediatria.RCE_alergias_farmacos_familiaresIngreso_Medico_Pediatrico = valCampo($("#txtAlergiaFaramco").val());
    ingresoMedicoPediatria.RCE_otros_familiaresIngreso_Medico_Pediatrico = valCampo($("#txtOtrosAntecendentesPersonales").val());

    ingresoMedicoPediatria.GEN_idEstado_Conyugal = valCampo($("#sltSituacionMarital").val());
    ingresoMedicoPediatria.RCE_idTipo_Desarrollo_Psico_Motor = valCampo($("#sltDSM").val());
    ingresoMedicoPediatria.GEN_idTipo_Datos_Nutricionales = valCampo($("#sltNutricional").val());
    ingresoMedicoPediatria.RCE_idIngreso_Medico = GetIngresoMedico().RCE_idIngreso_Medico;

}

function ObtenerValRadio(radioSi, radioNo) {

    if (radioSi.bootstrapSwitch('state'))
        return "SI";
    else if (radioNo.bootstrapSwitch('state'))
        return "NO";

    return null;
}

function GuardarIngresoPediatrico() {

    if (ingresoMedicoPediatria.GEN_idPersona_Cuidador !== null)
        GuardarPersona(ingresoMedicoPediatria.GEN_idPersona_Cuidador, "Cuidador");
    if (ingresoMedicoPediatria.GEN_idPersona_parental_1 !== null)
        GuardarPersona(ingresoMedicoPediatria.GEN_idPersona_parental_1, "Parental1");
    if (ingresoMedicoPediatria.GEN_idPersona_parental_2 !== null)
        GuardarPersona(ingresoMedicoPediatria.GEN_idPersona_parental_2, "Parental2");

    GuardarVacunasPaciente();

    CargarJsonIngresoMedicoPediatria();

    var url = (ingresoMedicoPediatria.RCE_idIngreso_Medico_Pediatrico === undefined) ? `${GetWebApiUrl()}RCE_Ingreso_Medico_Pediatrico` :
        `${GetWebApiUrl()}RCE_Ingreso_Medico_Pediatrico/${ingresoMedicoPediatria.RCE_idIngreso_Medico_Pediatrico}`;
    var method = (ingresoMedicoPediatria.RCE_idIngreso_Medico_Pediatrico === undefined) ? "POST" : "PUT";
    
    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(ingresoMedicoPediatria),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            toastr.success('Se registra Ingreso Médico Pediatrico.');
        },
        error: function (jqXHR, status) {
            console.log("Error al guardar Ingreso Médico pediatrico: " + JSON.stringify(jqXHR));
        }
    });
    
}