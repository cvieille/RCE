﻿
var mouseOut = true, sSession;
var jsonCamaOrigen = null, jsonCamaDestino = null;

$(document).ready(function () {
    var popoverinfo = `
                    <div class="card">
                        <div class="card-header">
                            <h5>Cuadro de ayuda</h5>
                        </div>
                        <div class="card-body" style="text-align:justify;">
                            <p><b>Obtener información del paciente:</b></br>
                            Haga click sobre una cama que esté ocupada y desplegará
                            la información de la atención y del paciente, allí mismo puede ver los traslados o movimientos del paciente. </p>
                            <p><b>Traslado de paciente:</b></br>
                            Haga click y mantenga presionado sobre una cama ocupada y arrastrela hacia la cama destino, posteriormente se  desplegará una ventana para confirmar el traslado del paciente. En caso de que la cama destino esté ocupada, se realizará un intercambio de pacientes. </br><b>Nota:</b>Es importante que la cama de origen, al arrastrarla quede bien posicionada sobre su destino.</p>
                        </div>
                    </div>
                        `;
    $(function () {
        $('#icon-info').popover({
            html: true,
            placement: 'left',
            content: popoverinfo
        })
    })
    

    sSession = getSession();
    RevisarAcceso(false, sSession);
    if (sSession.CODIGO_PERFIL != 5 && sSession.CODIGO_PERFIL != 15)
        window.location.replace(ObtenerHost() + "/Vista/Default.aspx");
    
    $("#txtFechaHospitalizacion").val(moment(GetFechaActual()).format("YYYY-MM-DD"));
    CargarComboUbicacion();
    //CargarComboAla();

    $("#sltUbicacion").change(function () {
        CargarComboAla();
        $("#sltAla").removeAttr("disabled");
    });

    $("#btnHosFiltro").click(function () {
        CargarMapaCamas(true);
    });

    CargarMapaCamas(true);

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": true,
        "showDuration": 500,
        "hideDuration": 500,
        "timeOut": 5000,
        "extendedTimeOut": 2500,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }

    ShowModalCargando(false);
});

function CargarComboUbicacion() {
    let url = GetWebApiUrl() + "GEN_Ubicacion/Hospitalizacion/1";
    setCargarDataEnCombo(url, false, $("#sltUbicacion"));
}

function CargarComboAla() {
    let url = GetWebApiUrl() + "GEN_Ala/GEN_Ubicacion/" + $("#sltUbicacion").val();
    setCargarDataEnCombo(url, false, $("#sltAla"));
}
//Funcion ejecuta query que trae un paciente y atencion en especifico
function getAtencionPaciente(idAtencion) {
    let Pacientes = [];
    if (idAtencion) {
        const url = GetWebApiUrl() + "/HOS_Hospitalizacion/Buscar?idHospitalizacion=" + idAtencion;
        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (data, status, jqXHR) {
                Pacientes = data;
            },
            error: function (jqXHR, status) {
                console.log("Error al Cargar Mapa de Camas: " + JSON.stringify(jqXHR));
            }
        });

    }
    return Pacientes;
}
//ESTA ES LA FUNCIÓN PRINCIPAL QUE CARGA Y ESTRUCTURA LAS CAMAS SEGÚN LOS DATOS RECIBIDOS
function CargarMapaCamas(esCargaInicial) {

    try {

        var url = GetWebApiUrl() + "HOS_Hospitalizacion/Mapa";

        if ($("#sltUbicacion").val() != "0") {
            url += "/" + $("#sltUbicacion").val();
            if ($("#sltAla").val() != "0")
                url += "/" + $("#sltAla").val();
        }
        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (data, status, jqXHR) {
                $("[data-toggle='popover']").popover('dispose');
                var array = data, html = "", arrayCamas = [];
                var htmlSinInfo = "<h1 class='text-center w-100'><b><i class='fa fa-info-circle'></i> Sin Información</b></h1>";
                
                // LOS FOR ANIDADOS FORMA EL HTML DEL MAPA DE CAMA
                // ORDEN: UBICACION - ALA - HABITACIÓN Y CAMAS
                for (var iUbi = 0; iUbi < array.length; iUbi++) {
                    var alas = "", jsonEstadosUbi = { disponibles: 0, bloqueadas: 0, ocupadas: 0, deshabilitadas: 0 };
                    for (var iAla = 0; iAla < array[iUbi].Ala.length; iAla++) {
                        var habitaciones = "", jsonEstadosAla = { disponibles: 0, bloqueadas: 0, ocupadas: 0, deshabilitadas: 0 };
                        for (var iHab = 0; iHab < array[iUbi].Ala[iAla].Habitacion.length; iHab++) {

                            var camas = "";
                            for (var iCam = 0; iCam < array[iUbi].Ala[iAla].Habitacion[iHab].Cama.length; iCam++) {

                                // HTML DE CAMAS
                                array[iUbi].Ala[iAla].Habitacion[iHab].Cama[iCam].NombreUbicacion = array[iUbi].NombreUbicacion;
                                camas += GetHtmlCama(array[iUbi].Ala[iAla].Habitacion[iHab].Cama[iCam]);
                                arrayCamas.push(array[iUbi].Ala[iAla].Habitacion[iHab].Cama[iCam]);
                                switch (array[iUbi].Ala[iAla].Habitacion[iHab].Cama[iCam].IdTipoEstado) {
                                    // Cama Disponible
                                    case 84:
                                        ++jsonEstadosAla.disponibles;
                                        ++jsonEstadosUbi.disponibles;
                                        break;
                                    // Cama Ocupada
                                    case 85:
                                        ++jsonEstadosAla.ocupadas;
                                        ++jsonEstadosUbi.ocupadas;
                                        break;
                                    // Cama Bloqueada
                                    case 86:
                                        ++jsonEstadosAla.bloqueadas;
                                        ++jsonEstadosUbi.bloqueadas;
                                        break;
                                    // Cama Deshabilitada
                                    case 99:
                                        ++jsonEstadosAla.deshabilitadas;
                                        ++jsonEstadosUbi.deshabilitadas;
                                        break;
                                }

                            }

                            // HTML DE HABITACIONES
                            habitaciones += GetHtmlHabitacion(array[iUbi].Ala[iAla].Habitacion[iHab], (camas === "") ? htmlSinInfo : camas);

                        }

                        // HTML DE ALAS
                        array[iUbi].Ala[iAla].idUbicacion = array[iUbi].IdUbicacion;

                        // esCargaInicial = ES PARA DESPLEGAR ACORDIÓN O NO
                        if (esCargaInicial)
                            alas += GetHtmlAla(array[iUbi].Ala[iAla], (habitaciones === "") ? htmlSinInfo : habitaciones, jsonEstadosAla, array[iUbi].Ala.length);
                        else
                            alas += GetHtmlAla(array[iUbi].Ala[iAla], (habitaciones === "") ? htmlSinInfo : habitaciones, jsonEstadosAla, 1);
                        
                    }
                    
                    // HTML DE UBICACIÓN
                    html += GetHtmlUbicacion(array[iUbi], (alas === "") ? htmlSinInfo : alas, jsonEstadosUbi);
                }
                $("#divGeneralMapaCama").html(html);
                
                // PINTA POPUP DE CADA CAMA
                // SE DEBE HACER APARTE PORQUE PRIMERO SE DEBE PINTAR EL HTML COMPLETO EN LA PÁGINA 
                for (var i = 0; i < arrayCamas.length; i++)
                    SetPopup(arrayCamas[i]);

            },
            error: function (jqXHR, status) {
                console.log("Error al Cargar Mapa de Camas: " + JSON.stringify(jqXHR));
            }
        });

    } catch (err) {
        alert(err.message);
    }

}

function GetHtmlUbicacion(json, htmlAla, jsonEstados) {

    var html =
        `   <div class="card card-primary">
                <div class="card-header" style="cursor:pointer !important;">
                    <a data-toggle="collapse" data-target="#divUbi_${ json.IdUbicacion}" aria-expanded="true" 
                        aria-controls="collapseOne">
                        <div class="row">
                            <div class="col-md-7">
                                <h4 class="mb-0">${ json.NombreUbicacion }</h4>
                            </div>
                            <div class="col-md-5 d-flex flex-row-reverse">
                                <div class="d-inline">
                                    Disponibles: <span class="badge badge-success">${ jsonEstados.disponibles}</span> | 
                                    Ocupadas: <span class="badge badge-ocupada">${ jsonEstados.ocupadas}</span> | 
                                    Bloqueadas: <span class="badge badge-bloqueada">${ jsonEstados.bloqueadas}</span> | 
                                    Deshabilitadas: <span class="badge badge-deshabilitada">${ jsonEstados.deshabilitadas }</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div id="divUbi_${ json.IdUbicacion}" class="collapse show" aria-labelledby="headingOne">
                    <div class="card-body p-0">
                        ${ htmlAla }
                    </div>
                </div>
            </div>`;

    return html;

}

function GetHtmlAla(json, htmlHabitacion, jsonEstados, lengthAla) {
    var html =
        `   <div class="card card-info" style="margin-bottom: 0px !important;">
                <div class="card-header" style="cursor:pointer !important;">
                    <a data-toggle="collapse" data-target="#divAla_${ json.idUbicacion }_${ json.IdAla }" aria-expanded="true" 
                        aria-controls="collapseOne">
                        <div class="row">
                            <div class="col-md-7">
                                <h5 class="mb-0">${ json.NombreAla }</h5>
                            </div>
                            <div class="col-md-5 d-flex flex-row-reverse">
                                <div class="d-inline">
                                    Disponibles: <span class="badge badge-success">${ jsonEstados.disponibles }</span> | 
                                    Ocupadas: <span class="badge badge-ocupada">${ jsonEstados.ocupadas }</span> | 
                                    Bloqueadas: <span class="badge badge-bloqueada">${ jsonEstados.bloqueadas}</span> | 
                                    Deshabilitadas: <span class="badge badge-deshabilitada">${ jsonEstados.deshabilitadas }</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div id="divAla_${ json.idUbicacion}_${json.IdAla}" class="collapse ${(lengthAla === 1) ? "show" : "" }" aria-labelledby="headingOne">
                    <div class="card-body">
                        <div class="row">
                            ${ htmlHabitacion}
                        </div>
                    </div>
                </div>
            </div>`;

    return html;

}

function GetHtmlHabitacion(json, htmlCamas) {

    var html =
        `   <div class="col-lg-2 col-md-3 col-sm-4">
                <div class="card card-primary">
                    <div class="card-header text-center">
                        <strong>${ json.NombreHabitacion }</strong>
                    </div>
                    <div class="card-body center-horizontal-vertical pl-0 pr-0">
                        ${ htmlCamas }      
                    </div>
                </div>
            </div>`;

    return html;

}

function buscarSinglePaciente(e) {
    var PacienteElegido = [];

    let idAtencion = $(e).data('idatencion');
    let ubicacion = $(e).data('ubicacion');
    let nombreCama = $(e).data('nombrecama');
    let estadoCama = $(e).data('estadocama');
    //Obtener del paciente elegido
    PacienteElegido = getAtencionPaciente(idAtencion);
    //Funcion que dubija el modal
    ModalInformacionPaciente(PacienteElegido, ubicacion, nombreCama, estadoCama);
}

function ModalInformacionPaciente(Paciente, ubicacion, nombreCama, estadoCama) {
    $("#headerModalInfoPaciente").empty();
    $("#bodyModalInfoPaciente").empty();
    $("#footerModalInfoPaciente").empty();
    let modalBody = "";
    //nombre de cama y ubicacion para el modal.
    let modalHeader = `
             <h5 class="modal-title" id="exampleModalLongTitle"> <strong>${ubicacion} | ${nombreCama} </strong></h5>
    `;
    let modalFooter;
    switch (estadoCama) {
        //Cama disponible
        case 84:
            //no se encontro información del paciente = cama disponible
            modalBody = `<h5>Cama disponible, no registra información de paciente</h5>`;
            modalFooter = `
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="LimpiarComponentes()">Cerrar</button>
                 `;
            break
        //Cama ocupada
        case 85:
            //comprobación existencia paciente en cama seleccionada.
            let claseEstilo;
            let riesgo;
            let dependencia;
            let codigo
            if (Paciente[0]) {
                //Codigo categorizacion
                if (Paciente[0].Categorizacion)
                    codigo = Paciente[0].Categorizacion.Codigo;
                //Obtener nivel de riesgo
                if (codigo) {
                    riesgo = Paciente[0].Categorizacion.Riesgo.Valor;
                    dependencia = Paciente[0].Categorizacion.Dependencia.Valor;
                    //Obtener letra de categoría
                    let code = codigo.substr(0, 1);
                    switch (code) {
                        case "A":
                            claseEstilo = "badge badge-danger";
                            break
                        case "B":
                            claseEstilo = "badge badge-warning";
                            break
                        case "C":
                            claseEstilo = "badge badge-info";
                            break
                        case "D":
                            claseEstilo = "badge badge-success";
                            break
                        default:
                            claseEstilo = "badge badge-secondary";
                            break
                    }
                
                modalHeader += `<p><b> </b><span class="${claseEstilo}" style="font-size:1.3em;">${codigo}</span></p>`;
                } else {
                    //sin cateogirzación
                    riesgo = "N/A";
                    dependencia = "N/A";
                    codigo = "S/C";
                }
                //obtener fecha actual
                var fechaActual = GetFechaActual();
                
                let idPaciente = Paciente[0].Paciente.Id;
                let nombrePaciente = Paciente[0].Paciente.Nombre + " " + Paciente[0].Paciente.ApellidoPaterno + " " + Paciente[0].Paciente.ApellidoMaterno;
                let NUI = Paciente[0].Paciente.Nui;
                let numeroDocumento = Paciente[0].Paciente.NumeroDocumento + "-" + Paciente[0].Paciente.Digito;
                let fechaNacimiento = Paciente[0].Paciente.FechaNacimiento;
                let genero = Paciente[0].Paciente.NombreGenero;
                let edadPaciente;
                //Fecha de atención hospitalaria
                let fechaAtencion = Paciente[0].Fecha;
                //Id de la atención hospitalaria
                let idAtencion = Paciente[0].Id;
                //Estado de la atención
                let estadoAtencion = Paciente[0].Estado.Valor;
                //Calculo edad
                if (fechaNacimiento) {
                    edadPaciente = CalcularEdad(fechaNacimiento).edad;
                } else {
                    edadPaciente = "No se registro fecha de nacimiento o edad del paciente";
                }
                //Calculo del tiempo de atención transcurrido
                if (fechaAtencion) {
                    fechaIngreso = fechaAtencion;
                    fechaIngreso = moment(fechaIngreso);
                    tiempoTranscurridoHoras = Math.abs(fechaIngreso.diff(fechaActual, 'hours'));
                    tiempoTranscurridoDias = Math.abs(fechaIngreso.diff(fechaActual, 'days'));
                    fechaIngreso = fechaIngreso.format("DD-MM-YY HH:mm");
                } else {
                    tiempoTranscurridoDias = 0
                    tiempoTranscurridoHoras = 0
                    fechaIngreso = " No se registro fecha de ingreso";
                }
                //información de paciente a inyectar en el modal

                modalBody = `
                <strong><p>Nombre paciente: ${nombrePaciente} </p></strong>
                <p><b>NUI:</b> ${NUI}</p>
                <p><b>Numero de documento:</b> ${numeroDocumento}</p>
                <p><b>Edad: </b> ${edadPaciente} </p>
                <p><b>Genero :</b> ${genero}</p>
                <p><b>Estado del paciente:</b> ${estadoAtencion} </p>
                <p><b>Fecha de ingreso: </b>${fechaIngreso} </p>
                <p><b>Tiempo atención transcurrido:</b> En días: ${tiempoTranscurridoDias} días, En horas: ${tiempoTranscurridoHoras} horas</p>
                <p><b>Riesgo:  </b>${riesgo} &nbsp; <b>Dependencia: </b> ${dependencia}</p>
                
                `;
                modalFooter = `
                    <a  class="btn btn-info"
                        data-idPaciente="${idPaciente}"
                        onClick='CargarMovimientosHospitalizacion(${idAtencion});'
                    >
                    <i class="fa fa-eye"></i>
                    Movimiento Hospitalización
                    </a>
                    <a  class="btn btn-info"
                         data-idPaciente="${idPaciente}"
                         onClick='CargarTrasladosHospitalizacion(${idAtencion});'
                    >
                    <i class="fa fa-eye"></i>
                    Ver traslados
                    </a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="LimpiarComponentes()">Cerrar</button>`;
            }
            break
    }
    $("#headerModalInfoPaciente").html(modalHeader);
    $("#bodyModalInfoPaciente").html(modalBody);
    $("#footerModalInfoPaciente").html(modalFooter);
    
}

function GetHtmlCama(json) {
    
    var estado = "", icon = "", jsonClassEstado = GetClassEstado(json.IdTipoEstado);
    var html =
        `   <div>
                <div class='text-center'>
                    <strong>${ json.Nombre}</strong> 
                </div>`

    //case 84: estado: "disponible",
    //case 85: estado: "ocupada",
    //case 86: estado: "bloqueada",
    //case 99: estado: "deshabilitada",
    //default: estado: "sin-estado",
    


    switch (jsonClassEstado.estado) {
        case "disponible":

            html +=
                `   <div id='divCama_${json.Id}' class='center-horizontal'>
                                <a
                                data-toggle="modal"
                                data-target="#mdlInfoPaciente"
                                id='aCama_${json.Id}'
                                data-ubicacion="${json.NombreUbicacion}"
                                data-nombrecama="${json.Nombre}"
                                data-estadocama="${json.IdTipoEstado}"
                                draggable='true'
                                data-info="disponible"
                                ondragstart="Drag(event)"
                                ondrop="Drop(event)"
                                ondragover="OverDrag(event)"
                                ondragleave="OutDrag(event)"
                                ondragend="EndDrag(event)"
                                onClick=buscarSinglePaciente(this)>

                            <div class='cama ${ jsonClassEstado.estado}' >
                                <img src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                <i id='iCama_${ json.Id}' class='fa ${jsonClassEstado.icon}' style='z-index:1;'></i>
                            </div>
                       
                            </a>
                    </div>`;
            break;

        case "ocupada":
            let color;

            if (json.Paciente.length > 0 && json.Paciente[0].Atencion.Categorizacion != null) {
                
                let code = json.Paciente[0].Atencion.Categorizacion.Codigo.substr(0, 1);
                switch (code) {
                    case "A":
                        //Red
                        color = "#dc3545";
                        break
                    case "B":
                        //Yellow
                        color = "#DBA607";
                        break
                    case "C":
                        //Green 
                        color = "#17a2b8";
                        break
                    case "D":
                        //Blue
                        color = "#28a745";
                        break
                    default:
                        color = "black";
                        break
                }
            } else {
                color = "black"
            }
            html +=
                `   <div id='divCama_${json.Id}' class='center-horizontal'>
                        <a  data-toggle="modal"
                                id='aCama_${json.Id}'
                                data-idPersona="${json.Paciente.length > 0 ? json.Paciente[0].Paciente.IdPaciente : ""}"
                                data-idatencion="${json.Paciente.length > 0 ? json.Paciente[0].Atencion.Id : ""}"
                                data-target="#mdlInfoPaciente"
                                data-ubicacion="${json.NombreUbicacion}"
                                data-nombrecama="${json.Nombre}"
                                data-estadocama="${json.IdTipoEstado}"
                                draggable='true'
                                role='button' class='cama-a'
                                ondragstart="Drag(event)"
                                ondrop="Drop(event)"
                                ondragover="OverDrag(event)"
                                ondragleave="OutDrag(event)"
                                ondragend="EndDrag(event)"
                                onClick=buscarSinglePaciente(this)>
                        
                            <div class='cama ${ jsonClassEstado.estado}'>
                                <img id='imgCama_${ json.Id}' src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                <i id='iCama_${ json.Id}' class='fa ${jsonClassEstado.icon}'" style="color:${color};"></i>
                            </div>
                        </a>
                    </div>`;
            break;

        case "deshabilitada":
            html +=
                `   <div id='divCama_${json.Id}' class='center-horizontal'>
                        <a id='aCama_${json.Id}' tabindex='0' data-toggle='popover' data-container='body' data-placement='auto' data-html='true' 
                            role='button' class='cama-a'
                                ondrop="Drop(event)"
                                ondragover="OverDrag(event)"
                                ondragleave="OutDrag(event)"
                                ondragend="EndDrag(event)>
                            <div class='cama ${jsonClassEstado.estado}' >
                                <img src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                <i id='iCama_${json.Id}' class='fa ${jsonClassEstado.icon}' style='z-index:1;'></i>
                            </div>
                        </a>
                    </div>`
        case "bloqueada":
            html +=
                `   <div id='divCama_${json.Id}' class='center-horizontal'>
                        <a id='aCama_${json.Id}' tabindex='0' data-toggle='popover' data-container='body' data-placement='auto' data-html='true' 
                            role='button' class='cama-a'
                                ondrop="Drop(event)"
                                ondragover="OverDrag(event)"
                                ondragleave="OutDrag(event)"
                                ondragend="EndDrag(event)>
                            <div class='cama ${jsonClassEstado.estado}' >
                                <img src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                <i id='iCama_${json.Id}' class='fa ${jsonClassEstado.icon}' style='z-index:1;'></i>
                            </div>
                        </a>
                    </div>`
        case "sin-estado":

            html +=
                `   <div id='divCama_${json.Id}' class='center-horizontal'>
                        <a id='aCama_${json.Id}' tabindex='0' data-toggle='popover' data-container='body' data-placement='auto' data-html='true' 
                            role='button' class='cama-a'
                                ondrop="Drop(event)"
                                ondragover="OverDrag(event)"
                                ondragleave="OutDrag(event)"
                                ondragend="EndDrag(event)>
                            <div class='cama ${ jsonClassEstado.estado}' >
                                <img src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                <i id='iCama_${ json.Id}' class='fa ${jsonClassEstado.icon}' style='z-index:1;'></i>
                            </div>
                        </a>
                    </div>`;

    }

    return html + "</div>";

    
}

//Acá se definen los Select de cada cama...
function SetPopup(json) {
    var idaCama = `#aCama_${ json.Id }`;
    $(idaCama).data("sltCama", `#sltCama_${ json.Id }`);
    $(idaCama).data("idCama", json.Id);
    $(idaCama).data("jsonCama", JSON.stringify(json));
    
    $(idaCama).popover({
        html: true,
        placement: 'top',
        trigger: 'manual',
        content: function () {
            var json = JSON.parse($(`#aCama_${$(this).data("idCama")}`).data('jsonCama'));
            return GetHtmlPopup(json);
        }
    }).on('inserted.bs.popover', function () {

        $("#divPopover").html("");
        $("#aCamaSeleccionar").data("jsonCama", $(idaCama).data("jsonCama"));

        //$("#aVerMas").click(function () {
        //    var idHosp = $(this).attr("data-idHosp");
        //    alert(idHosp);
        //    VerMasInformacionHospitalizacion(idHosp);
        //});

        $($(idaCama).data("sltCama")).on('focus', function () {
            $(this).data("SeleccionTemporal", $(this).val());
        });

        $($(idaCama).data("sltCama")).on('change', function () {

            var json = JSON.parse($(`#aCama_${$(this).attr("data-idCama")}`).data("jsonCama"));

            // 85 = Cama Ocupada
            if ($(this).data("SeleccionTemporal") == 85 && json.Paciente.length > 0) {

                $(this).val($(this).data("SeleccionTemporal"));
                toastr.warning("No se puede cambiar de estado una cama ocupada.");

            } else if ($(this).val() == 85 && json.paciente.length === 0) {

                $(this).val($(this).data("SeleccionTemporal"));
                toastr.warning("No se puede cambiar a estado ocupado porque la cama no tiene ninguna hospitalización relacionada.");

            } else {

                var jsonClassEstado = GetClassEstado(parseInt($(this).val()));
                CambiarEstadoCama($(this).attr("data-idCama"), $(this).val());
    
                $(`#aCama_${$(this).attr("data-idCama")} div`).removeAttr('class').attr('class', 'cama ' + jsonClassEstado.estado);
                $(`#aCama_${$(this).attr("data-idCama")} div i`).removeAttr('class').attr('class', 'fa ' + jsonClassEstado.icon);

                // SE ACTUALIZA JSON PARA ACTUALIZAR ESTADO EN INFO DEL JSON
                var json = JSON.parse($(`#aCama_${$(this).attr("data-idCama")}`).data('jsonCama'));
                json.IdTipo_Estados_Sistemas = parseInt($(this).val());
                $(`#aCama_${$(this).attr("data-idCama")}`).data('jsonCama', JSON.stringify(json));

                // CAMBIO DE CLASS SEGUN EL ESTADO EN EL POPOVER 
                $("#" + $(`#aCama_${$(this).attr("data-idCama")}`).attr("aria-describedby") + " .card")
                                                                .removeAttr('class')
                                                                .attr('class', 'card card-' + jsonClassEstado.estado);

                $("#" + $(this).attr("id") + " option[value='0']").remove();
                

            }
            
        });
    });
    
    $(idaCama).click(function () {

        mouseOut = false;
        $(".camaSeleccionada").removeClass("camaSeleccionada").addClass("cama-no-Seleccionada");
        $("[data-toggle=popover]").popover('hide');
        //$(this).popover('show').css({ "z-index": "1061" });

        //1061 !important
        $(".background-popover").remove();
        $(".popover.show").before("<div class='background-popover'></div>");
        $(".background-popover").unbind().click(function () {
            if (mouseOut)
                LimpiarComponentes();
                //Vuelve a cargar el mapa de camas y los contadores
                CargarMapaCamas(false);
        });

        $("#" + $(this).attr("aria-describedby")).on("mouseout", function () { mouseOut = true; });
        $("#" + $(this).attr("aria-describedby")).on("mouseover", function () { mouseOut = false; });

        $(this).removeClass("cama-no-Seleccionada").addClass("camaSeleccionada");
        
        

    });

    $(idaCama).on("mouseout", function () { mouseOut = true; });
    
}

function LimpiarComponentes() {
    $(".popover").unbind();
    $("[data-toggle=popover]").popover('hide');
    $(".camaSeleccionada").css({ "z-index": "" }).removeClass("camaSeleccionada").addClass("cama-no-Seleccionada");
    $(".background-popover").remove();
    jsonCamaOrigen = jsonCamaDestino = null;

}

function CambiarEstadoCama(HOS_idCama, GEN_idTipo_Estados_Sistemas) {

    $.ajax({
        type: "PATCH",
        url: `${GetWebApiUrl()}HOS_Cama/CambiarEstado/${HOS_idCama}/${GEN_idTipo_Estados_Sistemas}`,
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            toastr.success('Se Cambia Estado de Cama.');
        },
        error: function (jqXHR, status) {
            alert("Error al cambiar estado de cama: " + JSON.stringify(jqXHR));
        }
    });
}

function GetHtmlPopup(json) {

    var card = "", icon = "";
    switch (json.GEN_idTipo_Estados_Sistemas) {
        
        // Cama Disponible
        case 84:
            icon = "fa fa-check-circle";
            card = "card-disponible";
            break;
        // Cama Ocupada
        case 85:
            icon = "fa fa-user-circle";
            card = "card-ocupada";
            break;
        // Cama Bloqueada
        case 86:
            icon = "fa fa-lock";
            card = "card-bloqueada";
            break;
        // Cama Deshabilitada
        case 99:
            icon = "fa fa-ban";
            card = "card-deshabilitada";
            break;
        // Sin Estado
        default:
            icon = "fa fa-question-circle";
            card = "card-sin-estado";
            break;

    }
    
    var paciente = (json.Paciente.length === 0) ? "" :
        `   <div class='mt-1'>
                <strong>${ json.Paciente[0].nombreIdentificacion }</strong><br />
                <span>${ json.Paciente[0].numero_documentoPaciente }</span>
            </div>
            <div class='mt-1'>
                <strong>Paciente</strong><br />
                <span>${ json.Paciente[0].PersonasnombrePaciente}</span>
            </div>
            <div class='mb-0'>
                <strong>Fecha y Hora de Ingreso</strong><br />
                <span>${ moment(json.Paciente[0].fecha_ingreso_realHospitalizacion).format("DD-MM-YYYY HH:mm:SS") }</span>
            </div>
            <div class='text-center mt-2'>
                <a class='btn btn-light btn-block' data-l='21' onClick="VerMasInformacionHospitalizacion(${json.Paciente[0].idHospitalizacion});">
                    <i class="fa fa-info-circle"></i> Ver más 
                </a>
            </div>`;
    
    var html =
        `  <div class='card ${ card }' style='min-width: 250px; margin-bottom: 0px !important;'>
                <div class='card-header text-center'>
                    <strong>${ json.nombreCama }</strong>
                </div>
                <div class='card-body p-2'>
                    ${ paciente }
                </div>
                <div class='card-footer'>
                    <div class='mb-0'>
                        <strong>Estado de Cama</strong><br />
                        <select id="sltCama_${ json.Id }" class='form-control' data-idCama='${ json.Id }'>
                            ${ (card.includes("sin-estado")) ? "<option value='0' selected >Sin Estado</option>" : "" }
                            <option value='84' ${ (card.includes("disponible")) ? "selected" : "" }>Cama Disponible</option>
                            <option value='85' ${ (card.includes("ocupada")) ? "selected" : "" }>Cama Ocupada</option>
                            <option value='86' ${ (card.includes("bloqueada")) ? "selected" : "" }>Cama Bloqueada</option>
                            <option value='99' ${ (card.includes("deshabilitada")) ? "selected" : "" }>Cama Deshabilitada</option>
                        </select>
                    </div>
                    ${ (!card.includes("disponible")) ? "" : 
                    `<div class="mt-3 mb-0 ml-0 mr-0 btn-group-vertical w-100">
                        <a id="aCamaSeleccionar" class='btn btn-success btn-block' onClick='SeleccionarPaciente(this,null);'>
                            <i class="fa fa-sync"></i> Seleccionar
                        </a>
                    </div>`}
                    ${(card.includes("disponible") || json.Paciente.length === 0) ? "" :
                    `<div class="mt-3 mb-0 ml-0 mr-0 btn-group-vertical w-100">
                        <a id="aCamaSeleccionar" class='btn btn-success btn-block' onClick='SeleccionarPaciente(this,${json.Paciente[0].idHospitalizacion});'>
                            <i class="fa fa-sync"></i> Seleccionar
                        </a>
                        <a class='btn btn-info btn-block' onClick='CargarMovimientosHospitalizacion(${json.Paciente[0].idHospitalizacion});'>
                            <i class='fa fa-eye'></i> Mov. Hospitalización
                        </a>
                        <a class='btn btn-info btn-block' onClick='CargarTrasladosHospitalizacion(${json.Paciente[0].idHospitalizacion});'>
                            <i class='fa fa-eye'></i> Ver Traslados
                        </a>
                        <a class='btn btn-warning btn-block' onClick='VerEgresarHospitalizacion(${json.Paciente[0].idHospitalizacion});'>
                            <i class='fa fa-check'></i> Egresar
                        </a>
                    </div>`}
                </div>
            </div>`;
    
    return html;
}

function Drag(event) {
    //Lleva el id de la cama de origen por el datatransfer ...
    event.dataTransfer.setData("id", event.target.id);
    $("#" + event.target.id).removeClass("cama-no-Seleccionada").addClass("camaSeleccionada");
}

function Drop(event) {
    event.preventDefault();
    var a = $(`#${event.target.id}`).parent().parent();

    if (a != null) {

        var aDrag = $("#" + event.dataTransfer.getData("id"));
        var aDrop = $("#" + event.target.id).parent().parent();
        
        if (aDrag.attr("id") !== aDrop.attr("id")) {
            var dataOrigen = JSON.parse(aDrag.data("jsonCama"));
            var dataDestino = JSON.parse(aDrop.data("jsonCama"));
            // 84 = DISPONIBLE 
            // 85 = OCUPADA
            if (((dataOrigen.IdTipoEstado == 85 && dataOrigen.Paciente.length > 0) &&
                (dataDestino.IdTipoEstado == 85 && dataDestino.Paciente.length > 0)) ||
                (dataOrigen.Paciente.length > 0 && dataDestino.IdTipoEstado == 84))
                ShowTrasladoCama(dataOrigen, dataDestino);
            else
                toastr.warning('Debe seleccionar al menos una cama con una hospitalización.');

        } else
            toastr.warning('No se puede realizar el traslado.');
        
        $(".camaSeleccionada, .camaHoverSeleccionada").removeClass("camaSeleccionada")
                                                        .removeClass("camaHoverSeleccionada")
                                                        .addClass("cama-no-Seleccionada");

    }
}

function ShowTrasladoCama(dataOrigen, dataDestino) {
    $("#divBodyMdlTraspaso").html(GetHtmlModalConfirmacionTraslado(dataOrigen, dataDestino));
    $("#aAceptarConfirmarTraspaso").data("dataOrigen", JSON.stringify(dataOrigen));
    $("#aAceptarConfirmarTraspaso").data("dataDestino", JSON.stringify(dataDestino));
    $("#mdlConfirmarTraspaso").modal('show');
}

function EjecutarGuardarTraslado() {
    let pacienteOrigen = [];
    let PacienteDestino = [];
    let AtencionOrigen = [];
    let AtencionDestino = [];
   
    var dataOrigen = JSON.parse($("#aAceptarConfirmarTraspaso").data("dataOrigen"));
    var dataDestino = JSON.parse($("#aAceptarConfirmarTraspaso").data("dataDestino"));
    
    //Extracción de datos de origen para un mejor manejo
    PacienteOrigen = dataOrigen.Paciente.map((e) => e.Paciente)
    AtencionOrigen = dataOrigen.Paciente.map((e) => e.Atencion)
    //Extracción de datos de destino
    PacienteDestino = dataDestino.Paciente.map((e) => e.Paciente)
    AtencionDestino = dataDestino.Paciente.map((e) => e.Atencion)
    
    if (dataDestino.IdTipoEstado === 85 && dataOrigen.IdTipoEstado === 85) {
        EjecutarPostTrasladosCama({
            //Id de atención de hospitalización
            "IdHospitalizacion": AtencionDestino.Id,
            //id cama Origen
            "IdCamaOrigen": dataDestino.Id,
            //id cama destino
            "IdCamaDestino": dataOrigen.Id
        }, `${GetWebApiUrl()}HOS_Traslados_Cama/INTERCAMBIOCAMA/${AtencionOrigen[0].Id}/${AtencionDestino[0].Id}`);
    } else {
        if (dataOrigen.Paciente.length !== 0 || dataDestino.Paciente.length !== 0)
            EjecutarPostTrasladosCama({
                "IdHospitalizacion": (dataOrigen.Paciente.length !== 0) ? AtencionOrigen[0].Id : AtencionDestino[0].Id,
                "IdCamaOrigen": (dataOrigen.Paciente.length !== 0) ? dataOrigen.Id : dataDestino.Id,
                "IdCamaDestino": (dataOrigen.Paciente.length !== 0) ? dataDestino.Id : dataOrigen.Id
            }, GetWebApiUrl() + "HOS_Traslados_Cama");
        else
            toastr.warning('Acaba de ocurrir un error, intentelo nuevamente.');
    }
     
    $("#mdlConfirmarTraspaso").modal('hide');
    CargarMapaCamas(false);

}

function GetHtmlModalConfirmacionTraslado(jsonOrigen, jsonDestino) {
    let pacienteOrigen = [];
    let PacienteDestino = [];
    let AtencionOrigen = [];
    let AtencionDestino = [];
    PacienteOrigen = jsonOrigen.Paciente.map((e) => e.Paciente)
    AtencionOrigen = jsonOrigen.Paciente.map((e) => e.Atencion)

    PacienteDestino = jsonDestino.Paciente.map((e) => e.Paciente)
    AtencionDestino = jsonDestino.Paciente.map((e) => e.Atencion)
    var html = 
        `<div class="mr-5">
            <div class="card">
                <div class="card-header ${ GetClassEstado(jsonOrigen.IdTipoEstado).estado }">
                    <h6 class='mb-0'><strong>Información Paciente</strong></h6>
                </div>
                <div class="card-body">                  
                    <div class="row">
                        ${ (jsonOrigen.IdTipoEstado === 85) ?
                            `<div class="col-md-12" style="margin:0 auto;">
                                <div>
                                    <p><b>Rut: </b>${ PacienteOrigen[0].NumeroDocumento + "-" + PacienteOrigen[0].Digito}</p>
                                    <p><b>Nombre: </b>${PacienteOrigen[0].Nombre +" "+ PacienteOrigen[0].ApellidoPaterno + " " +PacienteOrigen[0].ApellidoMaterno }</p>
                                    <p><b>Fecha de atención: </b>${ moment(AtencionOrigen[0].fecha).format("DD-MM-YYYY HH:mm:SS") }</p>
                                </div>
                            </div>`: ""}
                          </br>
                    </div>
                    <div class="row">
                        <div class="${(jsonOrigen.IdTipoEstado === 85) ? "col-md-4" : "col-md-12" }" style="margin:0 auto;">
                            <div class="d-inline-block">
                                <div class='text-center'>
                                    <strong>${ jsonOrigen.Nombre }</strong> 
                                </div>
                                <div class='center-horizontal'>
                                    <div class='cama ${ GetClassEstado(jsonOrigen.IdTipoEstado).estado }'>
                                        <img src='../../Style/img/bed.PNG' width='40' />
                                        <i class='fa ${ GetClassEstado(jsonOrigen.IdTipoEstado).icon }'></i>
                                    </div> 
                                </div>
                                <div class="text-nowrap text-center">
                                    ${ jsonOrigen.NombreUbicacion }
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-footer text-center">
                    <strong>${ GetClassEstado(jsonOrigen.IdTipoEstado).estado.toUpperCase() }</strong >
                </div>
            </div>
        </div>

        ${ (jsonDestino.IdTipoEstado === 85) ? "<i class='fa fa-arrows-alt-h black-text fa-5x'></i>" : "<i class='fa fa-arrow-right black-text fa-5x' ></i >"}
                            
        <div class="ml-5">

            <div class="card">
                <div class="card-header ${ GetClassEstado(jsonDestino.IdTipoEstado).estado }">
                    <h6 class='mb-0'><strong>Información Paciente</strong></h6>
                </div>
                <div class="card-body">           
                    <div class="row">
                        ${ (jsonDestino.IdTipoEstado === 85) ?
                        `<div class="col-md-12" style="margin:0 auto;">
                                <div>
                                    <p><b>Rut: </b>${PacienteDestino[0].NumeroDocumento + "-" + PacienteDestino[0].Digito}</p>
                                    <p><b>Nombre: </b>${PacienteDestino[0].Nombre + " " + PacienteDestino[0].ApellidoPaterno + " " + PacienteDestino[0].ApellidoMaterno}</p>
                                    <p><b>Fecha de atención: </b>${moment(AtencionDestino[0].fecha).format("DD-MM-YYYY HH:mm:SS")}</p>
                                </div>
                        </div>`: ""}
                        </br>
                    </div>
                    <div class="row">
                        <div class="${ (jsonDestino.IdTipoEstado === 85) ? "col-md-4" : "col-md-12"}" style="margin:0 auto;">
                            <div class="d-inline-block">
                                <div class='text-center'>
                                    <strong>${jsonDestino.Nombre}</strong>
                                </div>
                                <div class='center-horizontal'>
                                    <div class='cama ${ GetClassEstado(jsonDestino.IdTipoEstado).estado }'>
                                        <img src='../../Style/img/bed.PNG' width='40' />
                                        <i class='fa ${ GetClassEstado(jsonDestino.IdTipoEstado).icon }'></i>
                                    </div>
                                </div>
                                <div class="text-center text-nowrap">
                                    ${jsonDestino.NombreUbicacion}
                                </div>
                            </div>
                        </div>
                       
                    </div>

                </div>
                <div class="card-footer text-center">
                    <strong><strong>${ GetClassEstado(jsonDestino.IdTipoEstado).estado.toUpperCase() }</strong ></strong>
                </div>
            </div>
        </div>`

    return html;
}

function EjecutarPostTrasladosCama(json, url) {
    
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            toastr.success('Se Realizó el traslado Correctamente');
        },
        error: function (jqXHR, status) {
            alert("Error al guardar Traslado: " + JSON.stringify(jqXHR));
        }
    });

}

function OverDrag(event) {
    event.preventDefault();

    if (document.getElementById(event.target.id) != null) {
        
        var aDrag = $(".camaSeleccionada");
        var aDrop = $("#" + event.target.id).parent().parent();

        if (aDrag.attr("id") !== aDrop.attr("id")) {
            aDrop.removeClass("cama-no-Seleccionada").addClass("camaHoverSeleccionada");
        }

    }

}

function OutDrag(event) {
    event.preventDefault();
    $(".camaHoverSeleccionada").removeClass("camaHoverSeleccionada").addClass("cama-no-Seleccionada");

}

function EndDrag(event) {

    event.preventDefault();
    $(".camaSeleccionada, .camaHoverSeleccionada").removeClass("camaSeleccionada")
        .removeClass("camaHoverSeleccionada")
        .addClass("cama-no-Seleccionada");

}

function GetClassEstado(IdTipo_Estados_Sistemas) {
    switch (IdTipo_Estados_Sistemas) {
        case 84:
            return {
                estado : "disponible",
                icon : "fa-check-circle"
            }
        case 85:
            return {
                estado: "ocupada",
                icon: "fa-user-circle"
            }
        case 86:
            return {
                estado: "bloqueada",
                icon: "fa-lock"
            }
        case 99:
            return {
                estado: "deshabilitada",
                icon: "fa-ban"
            }
        default:
            return {
                estado: "sin-estado",
                icon: "fa-question-circle"
            }

    }

}

function CargarTrasladosHospitalizacion(idHospitalizacion) {

    var adataset = [];
    $("#mdlMovimientoHospitalizacion .heading").text("Traslados de la Hospitalización");
    $("#movDisp").hide();
    $("#traDisp").show();
    
    $.ajax({
        type: "GET",
        url: GetWebApiUrl() + "HOS_Hospitalizacion/" + idHospitalizacion + '/Traslados',
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            data.map((e) => {
                adataset = e.Traslados.map((a) => a)
            })
            if (data.Traslados) {
                var traslados = data.Traslados;
                //Extrae los traslados.
                
            }
        }
    });

    if (adataset.length == 0)
        $('#lnkExportarMovHospitalizacion').addClass('disabled');
    else
        $('#lnkExportarMovHospitalizacion').removeClass('disabled');

    $('#lnkExportarMovHospitalizacion').data("btnExportarH", "ctl00$Content$btnExportarMovTrasladosH");

    //$('#tblMovHospitalizacion').DataTable().destroy();
    //$('#tblMovHospitalizacion').empty();
    
    $('#tblTraHospitalizacion').addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        destroy: true,
        columns: [
            { title: "Fecha traslado", data: "fecha" },
            { title: "Cama origen", data: "origen" },
            { title: "cama destino", data: "destino" }
        ]
    });
    
    $('#mdlMovimientoHospitalizacion').modal('show');

}

function CargarMovimientosHospitalizacion(idHospitalizacion) {

    var adataset = [];
    $("#mdlMovimientoHospitalizacion .heading").text("Movimientos de Hospitalización");
    var url = GetWebApiUrl() + "HOS_Hospitalizacion/" + idHospitalizacion + '/Movimientos';
    $("#traDisp").hide();
    $("#movDisp").show();


    $.ajax({
        type: "GET",
        url: url,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            //Extrae los movimientos.
            data.map((e) => {
                adataset = e.Movimiento.map((a) => a)
            })
        }
    });
    if (adataset.length == 0)
        $('#lnkExportarMovHospitalizacion').addClass('disabled');
    else
        $('#lnkExportarMovHospitalizacion').removeClass('disabled');

    $('#lnkExportarMovHospitalizacion').data("btnExportarH", "ctl00$Content$btnExportarMovHospitalizacionH");
    //Cuando la Tabla tiene atributo bDestroy el width debería ir de preferencia en la etiqueta
    $('#tblMovHospitalizacion').addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        destroy: true,
        columns: [
            { title: "Fecha Movimiento", data: "Fecha" },
            { title: "Usuario", data: "Usuario" },
            { title: "Movimiento", data: "Descripcion" }
        ]
    });

    $('#mdlMovimientoHospitalizacion').modal('show');

}


function SeleccionarPaciente(sender) {

    $(".popover").unbind();
    $("[data-toggle=popover]").popover('hide');
    $(".background-popover").remove();
    var jsonCama = JSON.parse($(sender).data("jsonCama"));

    if (jsonCamaOrigen === null || jsonCamaOrigen === jsonCama) {
        jsonCamaOrigen = jsonCama;
    } else if (jsonCamaDestino === null || jsonCamaDestino === jsonCama)
        jsonCamaDestino = jsonCama;             

    if (jsonCamaOrigen !== null && jsonCamaDestino !== null) { 

        if (jsonCamaOrigen.GEN_idTipo_Estados_Sistemas === 85 || jsonCamaDestino.GEN_idTipo_Estados_Sistemas === 85)
            ShowTrasladoCama(jsonCamaOrigen, jsonCamaDestino);
        else
            toastr.warning('Debe seleccionar al menos una cama con una hospitalización.');

        LimpiarComponentes();

    }
    
}
//Esta funcion al usar el modal probablemente no se este usando o esté obsoleta, al igual que el popover
function VerMasInformacionHospitalizacion(HOS_idHospitalizacion) {

    var html = "";
    var json = GetJsonHospitalizacion(HOS_idHospitalizacion);

    if (json != null) {

        html =
            `
                <div class='mt-2 mb-2 text-left' style='font-size: medium;'>
                    <strong>${ json.GEN_nombreIdentificacion}:</strong><br /> 
                    ${ json.GEN_numero_documentoPaciente} <br />
                    <strong>Nombre:</strong><br />
                    ${ json.GEN_PersonasnombrePaciente} <br />
                    <strong>Fecha ingreso:</strong><br />
                    ${ moment(json.HOS_fecha_ingreso_realHospitalizacion).format("DD-MM-YYYY HH:mm:SS")}<br />
                    <div class='mt-3 mb-3'>
                        <strong>Motivo de ingreso:</strong><br />
                        ${ (json.HOS_motivoHospitalizacion != null) ? json.HOS_motivoHospitalizacion : "Sin Información"} <br />
                    </div>
                    <div class='mt-3 mb-3'>
                        <strong>Último diagnóstico ingreso médico:</strong><br />
                        <p class='text-justify'>
                            ${ (json.RCE_diagnosticoIngreso_Medico.length > 0)
                                ? json.RCE_diagnosticoIngreso_Medico[json.RCE_diagnosticoIngreso_Medico.length - 1].RCE_diagnosticoIngreso_Medico
                                : "Sin Información"}
                        </p>
                    </div>
                    <div class='mt-3 mb-3'>
                        <strong>Último diagnóstico epicrisis:</strong><br />
                        <p class='text-justify'>
                            ${ (json.HOS_diagnostico_principalEpicrisis.length > 0)
                                ? json.HOS_diagnostico_principalEpicrisis[json.HOS_diagnostico_principalEpicrisis.length - 1].HOS_diagnostico_principalEpicrisis
                                : "Sin Información"}
                        </p>
                    </div>
                </div>
            `;

    } else
        html = "No se ha encontrado información de la hospitalización";

    $("#mdlVerMasInformacion .modal-body").html(html);
    $("#mdlVerMasInformacion").modal('show');
    
}

function GetJsonHospitalizacion(idHospitalizacion ) {

    var json = null;
    
    $.ajax({
        type: "GET",
        url: GetWebApiUrl() + "HOS_Hospitalizacion/Mapa/DetalleHospitalizacion/" + idHospitalizacion,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            if (data.length > 0)
                json = data[0];

        }
    });

    return json;

}

function VerEgresarHospitalizacion(HOS_idHospitalizacion) {
    
    var json = GetJsonHospitalizacion(HOS_idHospitalizacion);

    ShowModalAlerta("CONFIRMACION", "Confirmación",
        `   
            <div class='text-left' style='font-size:medium;'>
                
                <h5 class='text-center mb-4'><strong>Información de la hospitalización</strong></h5>
                
                <div class='row'>
                    <div class='col-md-6'>
                        <strong>${ json.GEN_nombreIdentificacion}:</strong><br /> 
                        ${ json.GEN_numero_documentoPaciente}
                    </div>
                    <div class='col-md-6'>
                        <strong>Fecha ingreso:</strong><br />
                        ${ moment(json.HOS_fecha_ingreso_realHospitalizacion).format("DD-MM-YYYY HH:mm:SS")}
                    </div>
                </div>
            
                <div class='row'>
                    <div class='col-md-12'>
                        <strong>Nombre:</strong><br />
                        ${ json.GEN_PersonasnombrePaciente }
                    </div>
                </div>
                <label for='txtFechaEgreso'>Fecha y hora de egreso</label>
                <div class="input-group mb-3">
                    <input id='txtFechaEgreso' type='date' class='form-control' aria-label="Recipient's username" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <input id='txtHoraEgreso' type='time' class='form-control clockpicker' /> 
                    </div>
                </div>
            </div>

            ¿Estás seguro qué quiere egresar a este paciente?
        `,
        function () { // Botón que egresa paciente

            var fechaActual = moment(GetFechaActual());
            var fechaEgreso = moment($("#txtFechaEgreso").val());
            var esValido = true;

            if ($("#txtFechaEgreso").val() === "" || !fechaEgreso.isBefore(fechaActual)) {
                $("#txtFechaEgreso").addClass('invalid-input');
                esValido = false;
            } else $("#txtFechaEgreso").removeClass('invalid-input');

            if ($("#txtHoraEgreso").val() === "") {
                $("#txtHoraEgreso").addClass('invalid-input');
                esValido = false;
            } else $("#txtHoraEgreso").removeClass('invalid-input');
            
            if (esValido) {
                EgresarHospitalizacion(HOS_idHospitalizacion);
                $('#modalAlerta').modal('hide');
            }

        },
        function () { // Botón cancelar
            $('#modalAlerta').modal('hide');
        }, "Egresar", "Cancelar");

    $(".clockpicker").clockpicker({ autoclose: true });
}

function EgresarHospitalizacion(HOS_idHospitalizacion) {

    var json = {
        "HOS_fecha_egreso_realHospitalizacion": $("#txtFechaEgreso").val(),
        "HOS_hora_egreso_realHospitalizacion": $("#txtHoraEgreso").val()
    };

    $.ajax({
        type: "POST",
        url: GetWebApiUrl() + "HOS_Hospitalizacion/Egreso/Enfermeria/" + HOS_idHospitalizacion,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            toastr.success('El paciente se ha egresado correctamente.');
            // Una vez que se actualiza el mapa de camas, queda con el fondo del popover.
            $(".background-popover").remove();
            CargarMapaCamas(false);
        },
        error: function (jqXHR, status) {
            console.log("Error al egresar paciente: " + JSON.stringify(jqXHR));
        }
    });
}