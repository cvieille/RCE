﻿
var sSession;
var idHos;
var idCama;
var idAislamiento;
let idUbicacionActualCamaTraslado;
let datosExportarHOS = [];
let datosExportarIM = [];


$(document).ready(function () {
         

    let IniciarComponentes = async () => {

        ShowModalCargando(true);
        await sleep(0);

        idAislamiento = 0;
        idCama = 0;
        idHos = 0;
        idDiv = "";
        sSession = getSession();

        if (sSession.CODIGO_PERFIL != 13) {
            //admision
            $('#mdlVerHojasEvolucion').on('shown.bs.modal', function () {
                $('.hhcc').attr('style', 'width:1px !important;');
            });
        }

        if (sSession.CODIGO_PERFIL != 15)
            $('#ulTablist').show();

        //Si es MEDICO o ADMISIÓN se muestra IM
        if (sSession.CODIGO_PERFIL == 1 || sSession.CODIGO_PERFIL == 13) {
            $("#liDatosIngreso").prop("style", "display: list-item;");
            $("#divBandejaIngresoMedico").prop("style", "display: flex;");
            $('#aBandejaIngresoMedico').click(function () {
                getTablaIngreso();
                if (session.CODIGO_PERFIL == 13) {
                    $('#divProfesional').show();
                    comboProfesional();
                }
                if (session.CODIGO_PERFIL == 1)
                    $('#btnNuevoIng').show();
            });
        }

        comboIdentificacion("#sltTipoIdentificacion");
        comboUbicacion();
        comboEstado();   

        cargarPerfil(sSession.CODIGO_PERFIL);
        getTablaHospitalizacion();

        $('#aBandejaHospitalizacion').click(function () {
            getTablaHospitalizacion();
        });

        $('#txtHosFiltroRut').on('keypress', function (e) {
            if ($('#sltTipoIdentificacion').val() == 1 || $('#sltTipoIdentificacion').val() == 4) {
                var charCode = (e.which) ? e.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
            }
            return true;
        });

        $('#txtHosFiltroRut').on('blur', function () {
            if ($('#sltTipoIdentificacion').val() == 1 || $('#sltTipoIdentificacion').val() == 4)
                $('#txtHosFiltroDV').val(ObtenerVerificador($(this).val()));
        });

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 150,
            "hideDuration": 150,
            "timeOut": 2000,
            "extendedTimeOut": 1000,
            "showEasing": "swing",
            "hideEasing": "swing",
            "showMethod": "slideDown",
            "hideMethod": "slideUp"
        }

        $("#btnHosFiltro").on('click', function () {
            getTablaHospitalizacion();
            return false;
        });

        $("#btnIngFiltro").on('click', function () {
            getTablaIngreso();
            return false;
        });

        $('#btnHosLimpiarFiltro').on('click', function () {
            $('#txtHosFiltroRut').val('');
            $('#txtHosFiltroNombre').val('');
            $('#txtHosFiltroApe').val('');
            $('#txtHosFiltroSApe').val('');
            $('#txtHosFiltroDV').val('');
            //if (sSession.CODIGO_PERFIL != 13)
            //    $('#ddlHosEstado').val('0');
            getTablaHospitalizacion();
            return false;
        });

        $("#btnIngLimpiarFiltro").on('click', function () {

            $("#txtIngFiltroRut").val('');
            $("#txtIngFiltroNombre").val('');
            $("#txtIngFiltroApe").val('');
            $("#txtIngFiltroSApe").val('');

            $("#ddlMedicoIngresoMedico").empty().append("<option value='0'>-Seleccione-</option>").val('0');

            comboUbicacion();
            comboProfesional();
            getTablaIngreso();

            return false;
        });

        $('#ddlServicioOrigen').change(function () {
            CargarCama($(this).val(), "#ddlCamaOrigenNuevo");
        });

        $('#ddlServicioDestino').change(function () {
            CargarCama($(this).val(), '#ddlCamaDestino');
        });

        $("#btnOrigen").click(function () { // Asignacion cama de origen
            let id = parseInt($("#numHospitalizacionCamaOrigen").text())
            let textoAlert = `Se ha establecido exitósamente la cama de origen para la hospitalización N° ${id}`
            if (validarCampos("#mdlCamaOrigen", false)) {
                json = {
                    "IdHospitalizacion": id,
                    "AntecedentesMorbidos": valCampo($("#txtAntecedentesMorbidos").val()),
                    "Diagnostico": valCampo($("#txtDiagnosticoHospitalizacion").val()),
                    "CondicionActual": valCampo($("#txtCondicionActualPaciente").val()),
                    "MotivoHospitalizacion": valCampo($("#txtMotivoConsultaPaciente").val()),
                    "IdCama": valCampo($("#ddlCamaOrigenNuevo").val())
                }
            }

            ShowModalCargando(true);
            let bValidar = validarCampos('#divCamaOrigen', false);
            if (bValidar) {
                $.ajax({
                    type: 'PUT',
                    url: `${GetWebApiUrl()}HOS_Hospitalizacion/${id}/AsignarCama`,
                    data: JSON.stringify(json),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        Swal.fire(
                            'Datos actualizados!',
                            textoAlert,
                            'success'
                        )
                        $("#mdlCamaOrigen").modal('hide');
                        getTablaHospitalizacion();
                    },
                    error: function (err) {
                        console.log(JSON.stringify(err))
                    }
                });
            }
            ShowModalCargando(false);
        });

        $('#aTraslado').click(function () {
            if (idCama == null) {
                toastr.warning("No se puede trasladar al paciente porque éste no tiene cama actual");
            }
            else if (validarCampos('#divNuevoTraslado', false)) {
                ShowModalCargando(true);
                var id = $('#idHos').val();
                var idTraslado = $('#idTraslado').val();

                var json;
                var sMetodo;
                let tipoTraslado = $("#swTrasladoExterno").is(":checked") ? 296 : 295;

                if (idTraslado == '0') {
                    sMetodo = 'POST';
                    json = {
                        HOS_idHospitalizacion: id,
                        HOS_idCamaDestino: $('#ddlCamaDestino').val(),
                        GEN_idTipo_Movimientos_Sistemas: tipoTraslado
                    };
                    idTraslado = '';
                    idCama = $('#ddlCamaDestino').val();

                } else {
                    sMetodo = 'PUT';
                    $.ajax({
                        type: 'GET',
                        url: GetWebApiUrl() + "HOS_Traslados_Cama/" + idTraslado,
                        contentType: "application/json",
                        dataType: "json",
                        async: false,
                        success: function (data) {
                            json = data;
                        }
                    });

                    json.HOS_idCamaDestino = $('#ddlCamaDestino').val();
                    //delete json.GEN_Cama;
                    //delete json.GEN_Cama1;
                    delete json.HOS_Cama;
                    delete json.HOS_Cama1;
                    idCama = $('#ddlCamaDestino').val();

                }

                $.ajax({
                    type: sMetodo,
                    url: GetWebApiUrl() + 'HOS_Traslados_Cama' + ((sMetodo === 'PUT') ? '/' + idTraslado : ''),
                    data: JSON.stringify(json),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: false,
                    success: function (data, status, jqXHR) {
                        llenarGrillaTraslado(false);
                        $('#aTraslado').text('Agregar Traslado');
                        $('#idTraslado').val('0');
                        CargarCama($('#ddlServicioDestino').val(), '#ddlServicioDestino');
                        idUbicacionActualCamaTraslado = $('#ddlServicioDestino').val();
                        getTablaHospitalizacion();
                    }, error: function (jqXHR, status) {
                        console.log(jqXHR);
                    }
                });
                ShowModalCargando(false);
            } else {
                toastr.error("Debe ingresar los campos requeridos!");
            }
        });

        $('#btnNuevoAislamiento').click(function (e) {

            var bValidar = validarCampos('#divNuevoAislamiento', false);
            var fechaFinAislamiento = $('#txtFechaFinAisl').val();
            if ($('#txtFechaFinAisl').val() == null || $('#txtFechaFinAisl').val() == undefined || $('#txtFechaFinAisl').val() == '')
                fechaFinAislamiento = null;

            if (bValidar) {
                var json = {
                    HOS_idHospitalizacion: idHos,
                    GEN_idTipo_Aislamiento: $('#ddlTipoAislamiento').val(),
                    HOS_fechaInicioHospitalizacion_Aislamiento: $('#txtFechaInicioAisl').val(),
                    HOS_fechaFinHospitalizacion_Aislamiento: fechaFinAislamiento,
                    HOS_estadoHospitalizacion_Aislamiento: 'Activo'
                };
                if (idAislamiento == 0) {
                    //console.log("Nuevo aislamiento");
                    $.ajax({
                        type: 'POST',
                        url: GetWebApiUrl() + "HOS_Hospitalizacion_Aislamiento",
                        data: JSON.stringify(json),
                        contentType: "application/json",
                        dataType: "json",
                        success: function (data) {
                            getTablaHospitalizacion();
                            getTablaAislamiento();
                        },
                        error: function (err) {
                            console.log("ERROR no se ingresar aislamiento: " + JSON.stringify(err));
                        }
                    });

                } else {
                    //console.log("edición aislamiento");
                    json.HOS_idHospitalizacion_Aislamiento = idAislamiento;
                    $.ajax({
                        type: 'PUT',
                        url: GetWebApiUrl() + "HOS_Hospitalizacion_Aislamiento/" + idAislamiento,
                        data: JSON.stringify(json),
                        contentType: "application/json",
                        dataType: "json",
                        success: function (data) {
                            idAislamiento = 0;
                            $('#btnNuevoAislamiento').text('Agregar Aislamiento');
                            getTablaHospitalizacion();
                            getTablaAislamiento();
                        },
                        error: function (err) {
                            console.log("ERROR no se pudo editar aislamiento: " + JSON.stringify(err));
                        }
                    });
                }
            }
            e.preventDefault();
        });

        deleteSession('ID_HOSPITALIZACION');
        deleteSession('TIPO_EDICION');
        deleteSession('ID_HOJAEVOLUCION');
        deleteSession('ID_INGRESO_MEDICO');
        deleteSession('ID_HOJA_ENFERMERIA');
        //var arr = $('#tblHospitalizacion').find('.asdf').parent();
        //$.each(arr, function (e,a) {
        //    $(a).children("div.collapse").attr("id", $(a).children("div.collapse").attr("id") + "asdf");
        //});

    }
    IniciarComponentes().then(() => ShowModalCargando(false));
});

function abrirEpicrisis() {
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Sistemas/17",
        async: false,
        success: function (data, status, jqXHR) {
            window.open(data.GEN_hostSistemas, '_blank');

            //$.each(data, function (key, val) {
            //    $("#sltUbicacion").append("<option value='" + val.GEN_idUbicacion + "'>" +
            //        val.GEN_nombreUbicacion + "</option>");
            //});

        },
        error: function (jqXHR, status) {
            console.log("Error al llenar nombre de host: " + jqXHR.responseText);
        }
    });

}
//carga funcionalidades y estados según perfil
function cargarPerfil(codPerfil) {
    switch (codPerfil) {
        case 1://medico
            $('#tabBandejaHOS').show();
            //$('#ddlHosEstado').val('88');
            break;
        case 5://enfermera clinica
            $('#tabBandejaHOS').show();
            $('#btnNuevoHos').show();
            //$('#ddlHosEstado').val('88');
            break;
        case 13: // admision
        case 24: // Secretaria
            $('#tabBandejaHOS').show();
            //$('#ddlHosEstado').val('87');
            //$('#ddlHosEstado').attr('disabled', true);
            $('#btnNuevoHos').show();
            break;
        case 15://Gestión Cama
            $('#tabBandejaHOS').show();
            //$('#ddlHosEstado').val('88');
            break;
        default:
            console.log('NO tiene perfil valido para este módulo');
            break;
    }
}

function CargarCama(GEN_idUbicacion, sltCama) {

    $(sltCama).empty();
    $(sltCama).append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Cama/Hospitalizacion/GEN_Ubicacion/${GEN_idUbicacion}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $.each(data, function (i, r) {
                $(sltCama).append(`<option value='${r.HOS_idCama}'>${r.HOS_nombreCama}</option>`);
            });
        }
    });

}

function LinkEditarHospitalizacion(sender) {
    setSession('ID_HOSPITALIZACION', $(sender).data("id"));
    setSession('TIPO_EDICION', 'EDICION');
    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevoFormularioAdmision.aspx";
}


// Mostrar modal Hoja Enfermeria
const LinkVerHojaEnfermeria = (e) => {
    const idHospitalizacion = $(e).data("id");
    const pac = $(e).data("pac");
    const cama = $(e).data("cama");

    $('#mdlVerHojaEnfermeria').modal('show');
    $('#nomPacModalHojaEnfermeria').html(pac);
    $('#numCamaModalHojaEnfermeria').html(cama);
    $('#numHospitalizacion').html(idHospitalizacion);

    //Tabla Hoja Enfermería por hospitalización
    getHojaEnfermeriaPorNHospitalizacion(idHospitalizacion, pac, cama);

    $("#btnNuevaHojaEnfermeria").unbind().click(function (e) {
        irNuevaHojaEnfermeria(idHospitalizacion);
        e.preventDefault();
    });
}

// Abrir formulario nueva hoja Enfermería
const irNuevaHojaEnfermeria = (idHospitalizacion) => {

    const accion = "INGRESO"
    setSession('ID_HOSPITALIZACION', idHospitalizacion);
    setSession('ACCION', accion)
    console.log(`${ObtenerHost()}/Vista/ModuloHOS/NuevaHojaEnfermeria.aspx`);
    window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/NuevaHojaEnfermeria.aspx`;
}

// Editar Hoja de Enfermería
const LinkEditarHojaEnfermeria = (e) => {
    let id = $(e).data("id")
    let idhosp = $(e).data("idhosp")
    setSession('ID_HOJA_ENFERMERIA', id)
    setSession('ID_HOSPITALIZACION', idhosp)
    setSession('ACCION', "EDITAR")
    window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/NuevaHojaEnfermeria.aspx`
}

// Imprimir de Enfermería
const LinkImprimirHojaEnfermeria = (e) => {
    $('#modalCargando').show();
    let id = $(e).data("id");
    $('#frameImpresion').attr('src', `ImprimirHojaEnfermeria.aspx?id=` + id);
    $('#mdlImprimir').modal('show');
}

// Anular Hoja de Enfermería
const LinkAnularHojaEnfermeria = (e) => {

    let id = $(e).data("id")
    let idhosp = $(e).data("idhosp")

    Swal.fire({
        title: '¿Seguro quieres anular la hoja?',
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.value) {

            anularHojaEnfermeria(id, idhosp)
        }
    })

}


//Tabla lista hojas de enfermeria segun parametro

const getHojaEnfermeriaPorNHospitalizacion = (idHospitalizacion, nomPaciente = "", nomCama = "") => {

    let url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/buscar?idHospitalizacion=${idHospitalizacion}`
    let adataset = []

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            data.forEach((item, index) => {
                adataset.push([
                    item.Id,
                    moment(item.Fecha).format("DD-MM-YYYY HH:mm:ss"),
                    item.Turno.Valor,
                    item.Diagnostico,
                    `${item.Profesional.Nombre} ${item.Profesional.ApellidoPaterno}`,
                    item.IdHospitalizacion
                ]);
            });

            $("#tblHojaEnfermeriaPorHospitalizacion").addClass("nowrap").DataTable({
                "order": [[0, "desc"]],
                data: adataset,
                columns: [
                    { title: "ID" },
                    { title: "Fecha" },
                    { title: "Turno" },
                    { title: "Diagnostico" },
                    { title: "Profesional" },
                    { title: "IdHopitalizacion" },
                    { title: "", class: "text-center" }

                ],

                columnDefs: [
                    {
                        targets: -1,
                        data: null,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            let botones =
                                `
                                <button class='btn btn-primary btn-circle' 
                                    onclick='toggle("#div_accionesIM${fila}"); return false;'>
                                    <i class="fa fa-list" style="font-size:15px;"></i>
                                </button>
                                    <div id='div_accionesIM${fila}' class='btn-container' style="display: none;">
                                        <center>
                                            <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                            <div class="rounded-actions">
                                                <center>
                            `;
                            //EDITAR
                            botones +=
                                `
                                <a
                                    data-id='${adataset[fila][0]}'
                                    data-idhosp='${adataset[fila][5]}'
                                    class='btn btn-info btn-circle btn-lg load-click'
                                    onclick='LinkEditarHojaEnfermeria(this);'
                                    data-toggle="tooltip"
                                    data-placement="left"
                                    title="Editar Hoja Enfermería/Matronería">
                                    <i class="fas fa-edit"></i>   
                                </a>
                                <br>
                            `;
                            //IMPRIMIR
                            botones +=
                                `
                                <a
                                    data-id='${adataset[fila][0]}'
                                    data-idhosp='${adataset[fila][5]}'
                                    class='btn btn-info btn-circle btn-lg load-click'
                                    onclick='LinkImprimirHojaEnfermeria(this);'
                                    data-toggle="tooltip"
                                    data-placement="left"
                                    title="Imprimir Hoja Enfermería/Matronería">
                                    <i class="fas fa-print"></i>
                                </a>
                                <br>
                            `;
                            //ANULAR
                            botones +=
                                `
                                <a  
                                    data-id='${adataset[fila][0]}'
                                    data-idhosp='${adataset[fila][5]}'
                                    class='btn btn-danger btn-circle btn-lg load-click'
                                    onclick='LinkAnularHojaEnfermeria(this);'
                                    data-toggle="tooltip"
                                    data-placement="left"
                                    title="Anular Hoja Enfermería/Matronería">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                                <br>
                            `;
                            //BOTON VIGILANCIA IAAS (Cambiar fila y columna en tabla)
                            //botones +=
                            //    `   
                            //    <a  data-id='${adataset[fila][0]}'
                            //        data-idhosp='${adataset[fila][5]}'
                            //        data-pac='${nomPaciente}'
                            //        data-cama='${nomCama}'
                            //        class='btn btn-success btn-circle btn-lg'
                            //        onclick='vigilancia(this);'
                            //        data-toggle="tooltip" data-placement="left"
                            //        title="Vigilancia IAAS">
                            //        <i class="fas fa-diagnoses"></i>
                            //    </a>
                            //    <br>
                            //`;
                            //MOVIMIENTOS
                            botones +=
                                `
                                <a  id='linkNuevaHospitalizacion_${fila}' data-idPaciente='${adataset[fila][1]}' 
                                    data-idMed='${adataset[fila][0]}' class='btn btn-primary btn-circle btn-lg load-click' 
                                    onclick='LinkCrearNuevaHospitalizacion(this);' data-toggle="tooltip" data-placement="left" 
                                    title="Ver Movimietos Hoja Enfermería/Matronería">
                                    <i class="fas fa-list"></i>
                                </a><br>
                            `;

                            botones += `
                                    </center >
                                    </div >
                                </center >
                            </div >
                            `;

                            return botones;
                        }
                    },

                    {
                        "targets": 5,
                        "visible": false
                    }
                ],
                "bDestroy": true
            });

            if (Array.isArray(adataset) && adataset.length === 0) {

                $("#tblHojaEnfermeriaPorHospitalizacion").empty()
                $("#ocultarTablaHojaEnfermeriaPorHospitalizacion").hide()
                $("#alertHojaEnfermeriaPorHospitalizacion").html("No existe información para mostrar")
            }

            $("#ocultarTablaHojaEnfermeriaPorHospitalizacion").show()
        },
        error: function (err) {
            console.log(JSON.stringify(err))
            $("#modalCargando").hide()

            if (err.status == 404) {

            }
        }
    });
}

const anularHojaEnfermeria = (id, idhosp) => {

    let url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}`

    $.ajax({
        type: 'DELETE',
        url: url,
        success: function (status, jqXHR) {
            getHojaEnfermeriaPorNHospitalizacion(idhosp)
        },
        error: function (jqXHR, status) {
            toastr.error('Error al anular Hoja de Enfermería / Matronería')
            console.log(jqXHR.responseText)
        }
    });

    return `id=${id} id=${idhosp}`
}

function getTablaAislamiento() {
    var adataset = [];

    //console.log(GetWebApiUrl() + "HOS_Hospitalizacion_Aislamiento/HOS_Hospitalizacion/" + idHos);
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "HOS_Hospitalizacion_Aislamiento/HOS_Hospitalizacion/" + idHos,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            $.each(data, function (i, r) {
                adataset.push([
                    r.HOS_idHospitalizacion_Aislamiento,
                    moment(r.HOS_fechaInicioHospitalizacion_Aislamiento).format('DD-MM-YYYY'),
                    r.HOS_fechaFinHospitalizacion_Aislamiento == null ? '' : moment(r.HOS_fechaFinHospitalizacion_Aislamiento).format('DD-MM-YYYY'),
                    r.GEN_nombreTipo_Aislamiento
                ]);
            });

            $('#tblAislamientoPaciente').addClass("nowrap").DataTable({
                data: adataset,
                order: [],
                columns: [
                    { title: "ID aislamiento" },
                    { title: "Fecha inicio aislamiento" },
                    { title: "Fecha fin aislamiento" },
                    { title: "Tipo aislamiento" },
                    { title: "" }
                ],
                columnDefs: [
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        visible: sSession.CODIGO_PERFIL == 5,
                        render: function (data, type, row, meta) {
                            var fila = meta.row;
                            var botones;
                            botones = "<a id='linkEditarAislamiento' data-id='" + adataset[fila][0] + "' data-aisl='" + adataset[fila][3] + "' class='btn btn-info btn-block' href='#/' onclick='linkEditarAislamiento(this)'><i class='fa fa-edit'></i> Editar</a>";
                            return botones;
                        }
                    },
                    {
                        targets: [1, 2], sType: "date-ukShort"
                    }
                ],
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    //if (aData[10] != '') {
                    //    $('td', nRow).css('background-color', '#DFF0D8');
                    //}
                },
                bDestroy: true
            });
        }
    });
}

function linkImprimir(e) {
    $('#modalCargando').show();
    var id = $(e).data("id");
    $('#frameImpresion').attr('src', 'ImprimirFormularioAdmision.aspx?id=' + id);
    $('#mdlImprimir').modal('show');
}

/*
function linkImprimir(e) {
    let link = e.target.id;

    let elemento = $("#" + link).data("id");
    let vista = $("#" + link).data("vista");
    let datos = {
        "idElemento": elemento,
        "nombreVista": vista + "?id=" + elemento
    };
    __doPostBack("<%= btnImprimirPdfAdmision.UniqueID %>", JSON.stringify(datos));
}
*/

function comboAislamiento() {
    $('#ddlTipoAislamiento').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Tipo_Aislamiento/combo`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, r) {
                $('#ddlTipoAislamiento').append('<option value="' + r.GEN_idTipo_Aislamiento + '">' + r.GEN_nombreTipo_Aislamiento + '</option>');
            });
        }
    });
}

function comboEstado() {
    $('#ddlHosEstado').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Tipo_Estados_Sistemas/HOSPITALIZACION`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $('#ddlHosEstado').append('<option value="0">-Todos-</option>');
            $.each(data, function (i, r) {
                $('#ddlHosEstado').append(`<option value='${r.GEN_idTipo_Estados_Sistemas}'>${r.GEN_nombreTipo_Estados_Sistemas}</option>`);
            });
        }
    });
    $('#ddlHosEstado').val('0');
}

function comboServicios(elem) {

    $(elem).empty();

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $(elem).append("<option value='0'>Seleccione</option>");
            $.each(data, function (i, r) {
                $(elem).append(`<option value='${r.Id}'>${r.Valor}</option>`);
            });
        }
    });

}

function linkImprimirIngreso(sender) {
    var id = $(sender).data("id");
    $("#modalCargando").show();
    $('#frameImpresion').attr('src', ObtenerHost() + '/Vista/ModuloHOS/ImprimirIngresoMedico.aspx?id=' + id);
    $('#mdlImprimir').modal('show');
}

function comboUbicacion() {
    $('#ddlUbicacion').empty();
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Ubicacion/Hospitalizacion/1",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $("#ddlUbicacion").append("<option value='0'>Seleccione</option>");
            $.each(data, function (i, r) {
                $('#ddlUbicacion').append("<option value='" + r.Id + "'>" + r.Valor + "</option>");
            });
        }
    });
}

function comboProfesional() {
    $('#ddlMedicoIngresoMedico').empty();
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Ingreso_Medico/Medicos",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $("#ddlMedicoIngresoMedico").append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (i, r) {
                $('#ddlMedicoIngresoMedico').append("<option value='" + r.GEN_idProfesional + "'>" + r.GEN_nombrePersonas + "</option>");
            });

        }
    });
}




function linkMovimientosHosp(e) {
    let id = $(e).data('idhos');
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/${id}/Movimientos`;
    getDataMovimientosporApi(url, $('#tblMovHospitalizacion'), $('#mdlMovimientosHospitalizacion'));
}

function linkMovimientosIngreso(e, grillaPadre) {

    $('#divCarouselIngresoMedico').carousel(1);
    var adataset = [];
    var id = $(e).data("id");

    (grillaPadre === "tblIngresoMedico") ? $("#aVolverCarouselIngresoMedico").show() : $("#aVolverCarouselIngresoMedico").hide();

    $.ajax({
        type: "GET",
        url: GetWebApiUrl() + "RCE_Movimientos_Ingreso_Medico/RCE_Ingreso_Medico/" + id,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $.each(data, function (i, r) {
                adataset.push([
                    r.RCE_idMovimientos_Ingreso_Medico,
                    moment(moment(r.RCE_fechaMovimientos_Ingreso_Medico).toDate()).format('DD-MM-YYYY hh:mm:ss'),
                    r.GEN_loginUsuarios,
                    r.GEN_descripcionTipo_Movimientos_Sistemas
                ]);
            });
        }
    });

    if (adataset.length == 0)
        $('#lnkExportarMovIngreso').addClass('disabled');
    else
        $('#lnkExportarMovIngreso').removeClass('disabled');

    $('#tblMovimientosIng').addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: "RCE_idMov_IPD" },
            { title: "Fecha Movimiento" },
            { title: "Usuario" },
            { title: "Movimiento" }
        ],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false
            },
            {
                "targets": 1,
                "sType": "date-ukLong"
            }
        ],
        "bDestroy": true
    });

    $('#tblMovimientosIng').css("width", "100%");
    $('#mdlMovimientosIngresoMedico').modal('show');

}

function anularIngresoConfirma(id) {
    $('#mdlAlertaCuerpo').empty();
    $('#mdlAlertaCuerpo').append('¿Esta seguro de anular el Ingreso Médico?');
    $('#linkConfirmar').attr('onclick', 'anularIngreso(' + id + ')');
    $('#linkConfirmar').addClass('btn-danger');
    $('#mdlAlerta').modal('show');
}

function anularHospitalizacionConfirma(e) {
    var id = $(e).data("id");

    $('#mdlAlertaCuerpo').empty();
    $('#mdlAlertaCuerpo').append('¿Esta seguro de anular la hospitalización?');
    $('#linkConfirmar').attr('onclick', 'anularHospitalizacion(' + id + ')');
    $('#linkConfirmar').addClass('btn-danger');
    $('#mdlAlerta').modal('show');
}

function anularIngreso(id) {

    $.ajax({
        type: 'PUT',
        url: GetWebApiUrl() + "RCE_Ingreso_Medico/" + id + "/Anular",
        async: false,
        success: function (status, jqXHR) {
            getTablaIngreso()
            $('#mdlAlerta').modal('hide');
            toastr.success('INGRESO MÉDICO ANULADO');
        },
        error: function (jqXHR, status) {
            toastr.error('ERROR AL ANULAR INGRESO MÉDICO');
            console.log(json.stringify(jqXHR));
        }
    });

}

function anularHospitalizacion(id) {

    $.ajax({
        type: 'DELETE',
        url: GetWebApiUrl() + "HOS_Hospitalizacion/" + id,
        success: function (status, jqXHR) {
            getTablaHospitalizacion();
            $('#mdlAlerta').modal('hide');
            toastr.success('HOSPITALIZACIÓN ANULADA');
        },
        error: function (jqXHR, status) {
            toastr.error('ERROR AL ANULAR HOSPITALIZACIÓN');
            console.log(jqXHR.responseText);
        }
    });
}

function linkVerAislamiento(e) {
    comboAislamiento();
    idAislamiento = 0;

    idHos = $(e).data("id");

    var idPaciente = $(e).data('idpaciente');
    var idNuevo = $(e).data('nuevo');

    $('#ddlTipoAislamiento').val('0');
    //$('button[data-id="ddlTipoAislamiento"]').html('Seleccione');    

    if (sSession.CODIGO_PERFIL == 5) {
        $('#divNuevoAislamiento').show();
        $('#txtFechaInicioAisl').val(moment().format('YYYY-MM-DD'));
        $('#txtFechaFinAisl').val(moment().add(5, 'days').format('YYYY-MM-DD'));
    }

    getTablaAislamiento();

    $("#mdlAislamiento").modal('show');
}

function linkVerEditarIngreso(e) {
    var id = $(e).data("id");
    setSession('ID_INGRESO_MEDICO', id);
    window.location.replace(ObtenerHost() + "/Vista/ModuloHOS/NuevoIngresoMedico.aspx");
}

//Nota esta es la que se llama actualmente
function getTablaIngreso() {

    var adataset = [];
    var filtros = {};

    if ($('#ddlUbicacion').val() != null && $('#ddlUbicacion').val() != '')
        filtros.GEN_idUbicacion = $('#ddlUbicacion').val();
    if ($('#ddlMedicoIngresoMedico').val() != '0')
        filtros.GEN_idProfesional = $('#ddlMedicoIngresoMedico').val();
    if ($('#txtIngFiltroRut').val() != '')
        filtros.GEN_numero_documentoPaciente = $('#txtIngFiltroRut').val();
    if ($('#txtIngFiltroNombre').val() != '')
        filtros.GEN_nombrePaciente = $('#txtIngFiltroNombre').val();
    if ($('#txtIngFiltroApe').val() != '')
        filtros.GEN_ape_paternoPaciente = $('#txtIngFiltroApe').val();
    if ($('#txtIngFiltroSApe').val() != '')
        filtros.GEN_ape_maternoPaciente = $('#txtIngFiltroSApe').val();

    //console.log(sBaseURL);

    $.ajax({
        type: 'POST',
        url: `${GetWebApiUrl()}RCE_Ingreso_Medico/Bandeja`,
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(filtros),
        responsive: true,
        async: false,
        success: function (data) {
            $.each(data, function (key, val) {
                //console.log(JSON.stringify(val));
                adataset.push([
                    val.RCE_idIngreso_Medico,
                    val.GEN_idPaciente,
                    val.GEN_idProfesional,
                    val.GEN_idUbicacion,
                    val.GEN_numero_documentoPersonasPaciente,
                    val.GEN_nombrePersonasPaciente,
                    val.GEN_nombrePersonasProfesional,
                    val.GEN_nombreUbicacion,
                    moment(moment(val.RCE_fecha_horaIngreso_Medico).toDate()).format('DD-MM-YYYY')
                ]);
            });
            setJsonExportarIM(data);
            $('#tblIngreso').addClass('nowrap').addClass("tblBandeja").addClass("dataTable").DataTable({
                data: adataset,
                responsive: {
                    details: false,
                    type: 'column'
                },
                columns: [
                /*0*/ { title: 'ID', className: "text-center font-weight-bold rce-tray-id" },
                /*1*/ { title: 'ID Paciente' },
                /*2*/ { title: 'idProfesional' },
                /*3*/ { title: 'idUbicacion' },
                /*4*/ { title: 'Ficha Paciente' },
                /*5*/ { title: 'Paciente' },
                /*6*/ { title: 'Profesional' },
                /*7*/ { title: 'Ubicación' },
                /*8*/ { title: 'Fecha de ingreso' },
                /*9*/ { title: '', className: 'text-center' }
                ],

                columnDefs: [
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var fila = meta.row;
                            var botones =
                                `
                                    <button class='btn btn-primary btn-circle' 
                                        onclick='toggle("#div_accionesIM${fila}"); return false;'>
                                        <i class="fa fa-list" style="font-size:15px;"></i>
                                    </button>
                                        <div id='div_accionesIM${fila}' class='btn-container' style="display: none;">
                                           <center>
                                                <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                                <div class="rounded-actions">
                                                    <center>
                                `;

                            if (sSession.CODIGO_PERFIL == '1')
                                botones +=
                                    `
                                        <a id='linkVerEditarIngreso_${fila}' data-id='${adataset[fila][0]}' 
                                            class='btn btn-default btn-circle btn-lg' href='#/' onclick='linkVerEditarIngreso(this)' 
                                            data-toggle='tooltip' data-placement='left' title='Editar Ingreso'>
                                            <i class='fa fa-edit'></i>
                                        </a><br>
                                    `;

                            if (sSession.CODIGO_PERFIL == '13')
                                botones +=
                                    `
                                        <a id='linkNuevaHospitalizacion_${fila}' data-idPaciente='${adataset[fila][1]}' 
                                            data-idMed='${adataset[fila][0]}' class='btn btn-default btn-circle btn-lg load-click' 
                                            onclick='LinkCrearNuevaHospitalizacion(this);' data-toggle="tooltip" data-placement="left" 
                                            title="Nueva Hospitalización">
                                            <i class='fa fa-plus'></i>
                                        </a><br>
                                    `;

                            botones +=
                                `
                                    <a id='linkImprimirIngreso_${fila}' data-id='${adataset[fila][0]}' class='btn btn-info btn-circle btn-lg' 
                                        href='#/' onclick='linkImprimirIngreso(this);' data-print='true' data-frame='#frameImpresion' 
                                        data-toggle="tooltip" data-placement="left" title="Imprimir Ingreso">
                                        <i class='fa fa-print'></i>
                                    </a><br>
                                    <a id='linkMovimientosIngreso_${fila}' data-id='${adataset[fila][0]}' 
                                        class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkMovimientosIngreso(this, \"tblIngreso\")' 
                                        data-toggle="tooltip" data-placement="left" title="Movimientos de Ingreso">
                                        <i class='fa fa-eye'></i>
                                    </a><br>
                                `;

                            if (sSession.CODIGO_PERFIL == '1') {
                                botones +=
                                    `
                                        <a id='linkAnularIngreso_${fila}' class='btn btn-danger btn-circle btn-lg' href='#/' 
                                            onclick='anularIngresoConfirma(${adataset[fila][0]});return false;' data-toggle="tooltip" 
                                            data-placement="left" title="Anular Ingreso">
                                            <i class='fa fa-ban'></i>
                                        </a><br>
                                    `;
                            }

                            botones += `
                                    </center >
                                    </div >
                                </center >
                            </div >`
                                ;

                            return botones;

                        }
                    },
                    {
                        'targets': [1, 2, 3],
                        'visible': false,
                        'searchable': false
                    },
                    //ocultar columnas en responsivo.
                    { responsivePriority: 1, targets: 0 }, // Id
                    { responsivePriority: 2, targets: 9 }, // botonera
                    { responsivePriority: 3, targets: 5 }, // nombre
                    { responsivePriority: 4, targets: 7 }, // ubicacion
                    { responsivePriority: 5, targets: 6 }, // profesional
                    { responsivePriority: 6, targets: 8 }, // Fecha de ingreso
                ],
                
                bDestroy: true
            });

            $('[data-toggle="tooltip"]').tooltip();
        }
    });
}

function toggle(id) {

    if (idDiv == "") {

        $(id).fadeIn();
        idDiv = id;

    } else if (idDiv == id) {

        $(id).fadeOut();
        idDiv = "";

    } else {

        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }

}

//Completa la tabla hospitalización...
function getTablaHospitalizacion() {

    var adataset = [];
    var url = `${GetWebApiUrl()}HOS_Hospitalizacion/Bandeja/` +
        `${$("#ddlHosEstado").val()}?` +
        `numerodocumento=${($.trim($("#txtHosFiltroRut").val()) !== "") ? $.trim($("#txtHosFiltroRut").val()) : ""}` +
        `&idIdentificacion=${$("#sltTipoIdentificacion").val()}` +
        `&nombre=${($.trim($("#txtHosFiltroNombre").val()) !== "") ? $.trim($("#txtHosFiltroNombre").val()) : ""}` +
        `&apellidopaterno=${($.trim($("#txtHosFiltroApe").val()) !== "") ? $.trim($("#txtHosFiltroApe").val()) : ""}` +
        `&apellidomaterno=${($.trim($("#txtHosFiltroSApe").val()) !== "") ? $.trim($("#txtHosFiltroSApe").val()) : ""}`;


    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (key, val) {
                adataset.push([
                    /*0*/ val.GEN_idPaciente,
                    /*1*/ val.HOS_idCama_Actual,
                    /*2*/ val.RCE_idEventos,
                    /*3*/ val.HOS_idHospitalizacion,
                    /*4*/ val.GEN_idTipo_Estados_Sistemas,
                    /*5*/ val.GEN_numero_documentoPaciente,
                    /*6*/ DevolverString(val.GEN_nombrePaciente),
                    /*7*/ (val.HOS_Paciente_Cama_Hospitalizacion == null || val.HOS_Paciente_Cama_Hospitalizacion.length === 0 ? '' : val.HOS_Paciente_Cama_Hospitalizacion),
                    /*8*/ moment(val.HOS_fecha_ingreso_realHospitalizacion).toDate().format('dd-MM-yyyy'),
                    /*9*/ val.HOS_hora_ingreso_realHospitalizacion,
                    /*10*/ val.HOS_diasHospitalizacion,
                    /*11*/ (val.HOS_fecha_egresoEpicrisis == null) ? '' : moment(moment(val.HOS_fecha_egresoEpicrisis).toDate()).format('DD-MM-YYYY'),
                    /*12*/ val.HOS_Aislamiento,
                    /*13*/ '', //estados
                    /*14*/ '', //botones
                    /*15*/  val.Acciones.EditarHospitalizacion,
                    /*16*/  val.Acciones.AnularHospitalizacion,
                    /*17*/  val.Acciones.ImprimirHospitalizacion,
                    /*18*/  val.Acciones.VerMovimientosHospitalizacion,
                    /*19*/  val.Acciones.VerIngresoMedico,
                    /*20*/  val.Acciones.VerFormulariosExternos,
                    /*21*/  val.Acciones.AsignarCama,
                    /*22*/  val.Acciones.VerHojaEnfermeria,
                    /*23*/  val.Acciones.VerVigilancia,
                    /*24*/  val.Acciones.VerHojadeEvolucion
                ]);
            });
            setJsonExportarHOS(data);
            //LlenarGrillaHospitalizacion(adataset, '#tblHospitalizacion', sSession);
            if (adataset.length == 0)
                $('#btnHosExportar').addClass('disabled');
            else
                $('#btnHosExportar').removeClass('disabled');

            $('#tblHospitalizacion').addClass('nowrap').addClass('#tblbandeja').addClass('dataTable').DataTable({
                data: adataset,
                responsive: {
                    details: false,
                    type: 'column'
                },
                sDom: 'ltip',
                columns: [
                    /*0*/  { title: "id_Paciente" },
                    /*1*/  { title: "id_Cama" },
                    /*2*/  { title: "id_Evento" },
                    /*3*/  { title: "ID", className: "text-center font-weight-bold rce-tray-id" },
                    /*4*/  { title: "id_estado" },
                    /*5*/  { title: "Ficha" },
                    /*6*/  { title: "Paciente" },
                    /*7*/  { title: "Cama" },
                    /*8*/  { title: "Fecha Hosp" },
                    /*9*/  { title: "Hora Hosp" },
                    /*10*/ { title: "Días" },
                    /*11*/ { title: "Fecha epicrisis" },
                    /*12*/ { title: "Aislamiento" },
                    /*13*/ { title: "Estado", className: "text-center" },
                    /*14*/ { title: "", className: "text-center" },
                    /*15*/ { title: "Editable" },
                    /*16*/ { title: "Anulable" },
                    /*17*/ { title: "Imprimible" },
                    /*18*/ { title: "Movimientos" },
                    /*19*/ { title: "Ver Ing. Med." },
                    /*20*/ { title: "Formularios Externos" },
                    /*21*/ { title: "Asignar Cama" },
                    /*22*/ { title: "Ver Hoja de Enfermería" },
                    /*23*/ { title: "Ver Vigilancia" },
                    /*24*/ { title: "Ver Hoja de Evolucion" },
                ],

                columnDefs: [
                    { "targets": [8], "type": "date-eu" },
                    {
                        targets: 14,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {

                            var fila = meta.row;
                            var botones =
                                `
                                    <button class='btn btn-primary btn-circle'
                                        onclick='toggle("#div_accionesHOS${fila}"); return false;'>
                                        <i class="fa fa-list" style="font-size:15px;"></i>
                                    </button>
                                        <div id='div_accionesHOS${fila}' class='btn-container' style="display: none;">
                                           <center>
                                                <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                                <div class="rounded-actions">
                                                    <center>
                                `;

                            //BOTON EDITAR INGRESO ADMINISTRATIVO
                            if (adataset[fila][15]) {
                                botones +=
                                    `   
                                    <a data-id='${adataset[fila][3]}' data-pac='${adataset[6]}' class='btn btn-success btn-circle btn-lg'
                                        onclick='LinkEditarHospitalizacion(this);' data-toggle="tooltip" data-placement="left" 
                                        title="Editar Ingreso Administrativo"> <i class="fas fa-pencil-alt"></i>
                                    </a><br>
                                `;
                            }

                            //BOTON VER INGRESO MEDICO
                            if (adataset[fila][19]) {
                                botones +=
                                    `
                                        <a data-idPaciente='${adataset[fila][0]}' data-idEvento='${adataset[fila][2]}' 
                                            data-idHos='${adataset[fila][3]}' class='btn btn-info btn-circle btn-lg' 
                                            onclick='LinkIngresoMedico(this);' data-toggle='tooltip' data-placement='left' 
                                            title='Ingreso médico'> IM
                                        </a><br>
                                    `;
                            }

                            //BOTON FORMULARIOS EXTERNOS
                            //if (adataset[fila][20]) {
                            //    //PERFIL MEDICO 
                            //    //87 = Hospitalización Pre-ingresada || 88 = Hospitalización Ingresada
                            //    botones +=
                            //        `
                            //            <a data-idPaciente='${adataset[fila][0]}' data-idEvento='${adataset[fila][2]}' 
                            //                class='btn btn-primary btn-circle btn-lg' onclick='LinkShowModalFormularios(this);' 
                            //                data-toggle="tooltip" data-placement="left" title="Formularios Externos">
                            //                <i class='fa fa-edit'></i>
                            //            </a><br>
                            //        `;
                            //}

                            //BOTON HOJA DE EVOLUCION
                            if (adataset[fila][24]) {
                                // 1 = MÉDICO || 15 = GESTIÓN CAMA || 5 = ENFERMERA(O) CLÍNICA(O)
                                botones +=
                                    `
                                        <a id='linkVerHojasEvolucion' data-idhos='${adataset[fila][3]}' data-id='${adataset[fila][0]}' 
                                            data-idEvento='${adataset[fila][2]}' class='btn btn-info btn-circle btn-lg' href='#/' 
                                            onclick='linkVerHojasEvolucion(this);' data-toggle="tooltip" data-placement="left" 
                                            title="Hoja De Evolución"> HDE
                                        </a><br>
                                    `;
                            }

                            //VER MOVIMIENTOS DE LA HOSPITALIZACION
                            //if ((sSession.CODIGO_PERFIL == 1 || sSession.CODIGO_PERFIL == 15 || sSession.CODIGO_PERFIL == 5) && (adataset[fila][4] == 88)) {
                            // 1 = MÉDICO || 15 = GESTIÓN CAMA || 5 = ENFERMERA(O) CLÍNICA(O)
                            botones +=
                                `
                                <a id='linkMovimientosHosp' data-idhos='${adataset[fila][3]}' data-id='${adataset[fila][0]}' 
                                    class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkMovimientosHosp(this);' 
                                    data-toggle="tooltip" data-placement="left" title="Movimientos Hospitalización">
                                    <i class="fas fa-list"></i>
                                </a><br>
                                `
                                ;
                            //}

                            //VER HISTORIAL HOSPITALIZACIONES DEL PACIENTE
                            botones +=
                                `
                                <a id='linkHistorialHosp' data-idpac='${adataset[fila][0]}' data-nompac='${adataset[fila][6]}'
                                    class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkHistorialHosp(this);' 
                                    data-toggle="tooltip" data-placement="left" title="Historial de Hospitalizaciones">
                                    <i class="fas fa-history"></i>
                                </a><br>
                                `
                                ;

                            //BOTON ASIGNAR CAMA
                            if (adataset[fila][21]) {
                                // 5 = ENFERMERA(O) CLÍNICA(O)
                                // 87 = Hospitalización Pre-ingresada
                                botones +=
                                    `
                                <a id='linkCamaOrigen' data-id='${adataset[fila][3]}' data-pac='${adataset[fila][6]}' class='btn btn-warning btn-circle btn-lg' 
                                    href='#/' onclick='linkCamaOrigen(this);' data-toggle="tooltip" data-placement="left" 
                                    title="Asignar Cama">
                                    <i class='fa fa-bed nav-icon'></i>
                                </a><br>
                                `;
                            }

                            //BOTON HOJA ENFERMERIA
                            if (adataset[fila][22]) { // devuelve true or false
                                botones +=
                                    ` 
                                    <a data-id='${adataset[fila][3]}' 
                                        data-pac='${adataset[fila][6]}' 
                                        data-cama='${adataset[fila][7]}' 
                                        data-fechaHosp='${adataset[fila][8]}'
                                        data-horaHosp='${adataset[fila][9]}'
                                        class='btn btn-success btn-circle btn-lg'
                                        onclick='LinkVerHojaEnfermeria(this);'
                                        data-toggle="tooltip" data-placement="left"
                                        title="Ver Hoja Enfermería/Matronería">
                                        <i class="fas fa-user-nurse"></i>
                                    </a>
                                    <br>
                                `;
                            }
                            //BOTON TRASLADOS PACIENTE
                            //if (sSession.CODIGO_PERFIL != 13 && adataset[fila][4] == 88) {//Hospitalización Ingresada

                            //    botones +=
                            //        `
                            //            <a id='linkTraslados${fila}' data-id='${adataset[fila][3]}' data-idcama='${adataset[fila][1]}' 
                            //                data-egreso='${adataset[fila][11]}' class='btn btn-info btn-circle btn-lg' href='#/' 
                            //                onclick='linkVerTraslados(this);' data-toggle="tooltip" data-placement="left" 
                            //                title="Traslados Paciente">
                            //                <i class='fa fa-bed nav-icon'></i>
                            //            </a><br>
                            //        `;
                            //}

                            //BOTON EGRESAR PACIENTE
                            //if (adataset[fila][4] == 88 && sSession.CODIGO_PERFIL == 1) {
                            //    botones +=
                            //        `
                            //            <a id='linkEgresarHostpializacion' data-id='${adataset[fila][3]}' 
                            //                class='btn btn-warning btn-circle btn-lg' href='#/' onclick='abrirEpicrisis();' 
                            //                data-toggle="tooltip" data-placement="left" title="Egresar">
                            //                <i class='fa fa-check'></i>
                            //            </a><br>
                            //        `;
                            //} else if (adataset[fila][4] == 88 && sSession.CODIGO_PERFIL == 5) {
                            //    botones +=
                            //        `
                            //            <a id='linkVerEgreso' class='btn btn-warning btn-circle btn-lg' href='#/'
                            //                onclick='linkVerEgresarHospitalizacion(${adataset[fila][3]});' data-toggle="tooltip" 
                            //                data-placement="left" title="Egresar">
                            //                <i class='fa fa-check'></i>
                            //            </a><br>
                            //        `;

                            //}

                            //BOTON AISLAMIENTO
                            if (sSession.CODIGO_PERFIL == 5 && adataset[fila][4] == 88) {
                                if (adataset[fila][12] == 'Sin Aislamiento') {
                                    botones +=
                                        `<a id='linkNuevoAislamiento' data-id='${adataset[fila][3]}' data-idpaciente='${adataset[fila][0]}' 
                                            data-nuevo='1' class='btn btn-warning btn-circle btn-lg' href='#/' 
                                            onclick='linkVerAislamiento(this);' data-toggle="tooltip" data-placement="left" 
                                            title="Ver Aislamiento">
                                            <i class='fa fa fa-plus nav-icon'></i>
                                        </a><br>`;
                                } else {
                                    botones +=
                                        `<a id='linkAislamiento' data-id='${adataset[fila][3]}' data-idpaciente='${adataset[fila][0]}' 
                                            data-nuevo='0' class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkVerAislamiento(this);'
                                            data-toggle="tooltip" data-placement="left" title="Ver Aislamiento">
                                            <i class='fa fa-eye nav-icon'></i>
                                        </a><br>`;
                                }
                            }

                            //IMPRIMIR
                            if (adataset[fila][17]) {
                                botones +=
                                    `<a id='linkImprimir' data-id='${adataset[fila][3]}' class='btn btn-info btn-circle btn-lg' href='#/' 
                                        onclick='linkImprimir(this);' data-toggle="tooltip" data-placement="left" title="Imprimir">
                                        <i class='fa fa-print'></i>
                                    </a><br>`;
                            }

                            //ANULAR
                            if (adataset[fila][16]) {
                                botones +=
                                    `<a id='linkAnular' data-id='${adataset[fila][3]}' class='btn btn-danger btn-circle btn-lg' href='#/' 
                                        onclick='anularHospitalizacionConfirma(this); return false;' data-toggle="tooltip" 
                                        data-placement="left" title="Anular">
                                        <i class="fas fa-trash-alt"></i>
                                    </a><br>`;
                            }

                            botones += `
                                    </center >
                                    </div >
                                </center >
                            </div >`
                                ;

                            return botones;
                        }
                    },

                    {
                        targets: 13,
                        data: null,
                        //orderable: false,
                        render: function (data, type, row, meta) {
                            var fila = meta.row;
                            var botones;
                            var s;
                            var b;
                            switch (adataset[fila][4]) {
                                case 87: s = '&nbsp;P&nbsp;'; b = 'secondary'; break;
                                case 88: s = '&nbsp;I&nbsp;'; b = 'success'; break;
                                case 89: s = '&nbsp;E&nbsp;'; b = 'warning'; break;
                                default:
                            }

                            botones = `<h5><span id="bgEstado" class="badge badge-pill btn-${b} badge-count">${s}</span>`;
                            if (adataset[fila][12] > 0) //si tiene aislamiento pinta en rojo
                                botones += `&nbsp;<span id="bgAilamiento" class="badge badge-pill btn-danger badge-count">&nbsp;A&nbsp;</span>`;
                            botones += "</h5>"
                            return botones;
                        }
                    },
                    { //control de botones
                        targets: [15, 16, 17, 18, 19, 20, 21, 22, 23],
                        visible: false,
                        searchable: false
                    },
                    {
                        targets: [0, 1, 2, 4, 12],
                        visible: false,
                        searchable: false
                    },

                    {
                        targets: [7, 11, 12],
                        visible: sSession.CODIGO_PERFIL != 13,
                        searchable: sSession.CODIGO_PERFIL != 13
                    },
                    //ocultar columnas en responsivo.
                    { responsivePriority: 1, targets: 3 }, // Id
                    { responsivePriority: 2, targets: 14 }, // botonera
                    { responsivePriority: 3, targets: 6 }, // nombre
                    { responsivePriority: 4, targets: 13 }, // estado
                    { responsivePriority: 5, targets: 7 }, // cama
                    { responsivePriority: 6, targets: 10 }, //Dias
                ],

                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    if (aData[11] != '') {
                        $('td', nRow).css('background-color', '#DFF0D8');
                    }
                    //if (aData[12] != 'Sin Aislamiento') {
                    //    $('td', nRow).css('background-color', '#EFD7E5');
                    //}
                },

                ////Crea la nueva tabla al redimensionar
                //responsive: {
                //    details: {
                //        renderer: function (api, rowIdx, columns) {
                //            var data = $.map(columns, function (col, i) {
                //                var toReturn = '';
                //                if (col.hidden) {
                //                    toReturn = toReturn.concat('<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">');

                //                    if (col.title == '') {

                //                        //Obtener objeto JQuery desde una cadena HTML
                //                        var button = $(col.data, "button").get();

                //                        var ID;
                //                        var newID;

                //                        if (button) {
                //                            ID = $(button).attr("id");
                //                            var r = ID.replace('#', '');
                //                            newID = r + "-resp";
                //                        }

                //                        var first = col.data.replace(new RegExp(r, 'g'), newID);
                //                        var second = first.replace(new RegExp('<br>', 'g'), '');
                //                        var result = second.replace(new RegExp('rounded-actions', 'g'), 'rounded-actions-resp');
                //                        //console.log(result);

                //                        toReturn = toReturn.concat('<td colspan=2 align=center>' + result + '</td>');
                //                    }
                //                    else {
                //                        toReturn = toReturn.concat('<td align=right style="width: 50%;">' + col.title + ':' + '</td> ');
                //                        toReturn = toReturn.concat('<td style="width: 50%;">' + col.data + '</td>');
                //                    }

                //                    toReturn = toReturn.concat('</tr>');
                //                }


                //                return toReturn;

                //            }).join('');

                //            return data ?
                //                $('<table style = "width: 100%"/>').append(data) :
                //                false;
                //        }
                //    }
                //},

                bDestroy: true
            });

            $('#tblHospitalizacion').css("width", "100%");
            $('[data-toggle="tooltip"]').tooltip();

        }
    });

    // SI NO ES PERFIL MEDICO NO SE PUEDEN CREAR NUEVAS HOJAS DE EVOLUCIÓN
    if (sSession.CODIGO_PERFIL != 1 && sSession.CODIGO_PERFIL != 12)
        $("#linkNuevaHojaEvolucion").hide();

}

function setJsonExportarHOS(json) {
    json.forEach((row) => {
        let Object = new Proxy(row, handler);
        datosExportarHOS.push([
            Object.HOS_idHospitalizacion,
            Object.GEN_numero_documentoPaciente,
            DevolverString(Object.GEN_nombrePaciente),
            Object.HOS_Paciente_Cama_Hospitalizacion,
            formatDate(Object.HOS_fecha_ingreso_realHospitalizacion),
            Object.HOS_hora_ingreso_realHospitalizacion,
            Object.HOS_diasHospitalizacion,
            formatDate(Object.HOS_fecha_egresoEpicrisis),
            Object.HOS_Aislamiento
        ]);
        //Indices de 0 a 8 fijos...
    });
}

function setJsonExportarIM(json) {
    json.forEach((row) => {
        let Object = new Proxy(row, handler);
        datosExportarIM.push([
            Object.RCE_idIngreso_Medico,
            Object.GEN_numero_documentoPersonasPaciente,
            Object.GEN_nombrePersonasPaciente,
            Object.GEN_nombrePersonasProfesional,
            Object.GEN_nombreUbicacion,
            formatDate(Object.RCE_fecha_horaIngreso_Medico)
        ]);
        //Indices de 0 a 8 fijos...
    });
}


function LinkShowModalFormularios(sender) {
    $("#btnNuevoIPD, #btnNuevoIC, #btnNuevoEXA, #btnNuevoFQX").data("idEvento", $(sender).attr("data-idEvento"));
    $("#btnNuevoIPD, #btnNuevoIC, #btnNuevoEXA, #btnNuevoFQX").data("idPaciente", $(sender).attr("data-idPaciente"));
    $("#mdlFormularios").modal("show");
}

function LinkIngresoMedico(sender) {

    if (sSession.CODIGO_PERFIL == 1) {
        $('#aIngresoMedico').hide();
    } else {
        $("#aIngresoMedico").data("idEvento", $(sender).attr("data-idEvento"));
        $("#aIngresoMedico").data("idPaciente", $(sender).attr("data-idPaciente"));
        $("#aIngresoMedico").data("idHospitalizacion", $(sender).attr("data-idHos"));
    }

    LlenarGrillaIngresoMedico($(sender).attr("data-idHos"));
    $('#divCarouselIngresoMedico').carousel(0);
    $("#mdlMovimientosIngresoMedico").modal('show');

}

function LinkCrearNuevoIngresoMedico() {
    setSession('ID_EVENTO', $("#aIngresoMedico").data("idEvento"));
    setSession('ID_PACIENTE', $("#aIngresoMedico").data("idPaciente"));
    setSession('ID_HOSPITALIZACION', $("#aIngresoMedico").data("idHospitalizacion"));
    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevoIngresoMedico.aspx";
}

function LlenarGrillaIngresoMedico(HOS_idHospitalizacion) {

    var datos = [];
    $.ajax({
        type: "GET",
        url: GetWebApiUrl() + "RCE_Ingreso_Medico/HOS_idHospitalizacion/" + HOS_idHospitalizacion,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $.each(data, function (key, val) {
                datos.push([
                    val.RCE_idIngreso_Medico,
                    moment(moment(val.RCE_fecha_horaIngreso_Medico).toDate()).format('DD-MM-YYYY HH:mm:SS'),
                    val.RCE_diagnosticoIngreso_Medico,
                    val.GEN_nombrePersonasProfesional,
                    val.GEN_nombreUbicacion
                ]);
            });
        }
    });

    $("#tblIngresoMedico").addClass("nowrap").DataTable({
        data: datos,
        columns: [
            { title: "ID" },
            { title: "Fecha/Hora Ingreso" },
            { title: "Diagnóstico" },
            { title: "Profesional" },
            { title: "Ubicación" },
            { title: "" }
        ],
        lengthMenu: [[5, 10], [5, 10]],
        columnDefs: [
            {
                targets: 2,
                sType: "date-ukLong"
            },
            {
                targets: -1,
                data: null,
                orderable: false,
                searchable: false,
                className: 'text-right w-1',
                render: function (data, type, row, meta) {

                    var fila = meta.row;
                    var botones =
                        `
                            <a id='linkEditarIngresoMedico' data-id='${datos[fila][0]}' class='btn btn-info load-click mr-2' href='#/' onclick='linkVerEditarIngreso(this);' style='margin:-3px; padding:5px; font-size:0.9rem;'>
                                <i class='fa fa-edit'></i> Editar
                            </a>
                            <a id='linkImprimirIngresoMedico' data-id='${datos[fila][0]}' class='btn btn-info load-click mr-2' href='#/' onclick='linkImprimirIngreso(this);' data-print='true' data-frame='#frameImpresion' style='margin:-3px; padding:5px; font-size:0.9rem;'>
                                <i class='fa fa-print'></i> Imprimir
                            </a>
                            <a id='linkVerMovIngresoMedico' data-id='${datos[fila][0]}' class='btn btn-info load-click' href='#/' onclick='linkMovimientosIngreso(this);' style='margin:-3px; font-size:0.9rem;'>
                                <i class='fa fa-list'></i> Movimientos
                            </a>
                        `;

                    return botones;

                }
            }
        ],

        responsive: true,

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) { },
        "bDestroy": true
    });

    $("#tblIngresoMedico").css("width", "100%");

}

function LinkCrearNuevaHospitalizacion(sender) {
    setSession('ID_PACIENTE', $(sender).attr("data-idPaciente"));
    setSession('ID_INGRESO_MEDICO', $(sender).attr("data-idMed"));
    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevoFormularioAdmision.aspx";
}

function linkEgresarHojaEvolucion(sender) {
    setSession('ID_HOSPITALIZACION', $(sender).data("id"));
    setSession('TIPO_EDICION', 'EGRESO');
    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevoFormularioAdmision.aspx";
}

function linkCamaOrigen(e) {

    let id = $(e).data("id");
    let pac = $(e).data("pac");

    $("#btnOrigen").attr("data-idhosp", id)

    $('#numHospitalizacionCamaOrigen').html(id);
    $('#nomPacienteCamaOrigen').html(pac);

    comboServicios('#ddlServicioOrigen');

    $('#ddlServicioOrigen').val('0');
    $('#ddlCamaOrigenNuevo').empty().append("<option value='0'>-Seleccione-</option>");

    $("#txtAntecedentesMorbidos").val('')
    $("#txtMotivoConsultaPaciente").val('')
    getMotivoConsultaHospitalizacion(id)
    $("#txtDiagnosticoHospitalizacion").val('')
    $("#txtCondicionActualPaciente").val('')
    $("#ddlCamaOrigenNuevo").val('')

    $("#mdlCamaOrigen").modal('show');
}

const getMotivoConsultaHospitalizacion = (id) => {

    let json = {}

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/${id}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            const { MotivoHospitalizacion } = data[0]

            $("#txtMotivoConsultaPaciente").val(MotivoHospitalizacion)

        },
        error: function (err) {
            console.log(JSON.stringify(err))
        }
    })
}

function linkEditarAislamiento(sender) {
    var id = $(sender).data('id');
    var aisl = $(sender).data('aisl');
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'HOS_Hospitalizacion_Aislamiento/' + id,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            idAislamiento = data.HOS_idHospitalizacion_Aislamiento;
            $('#ddlTipoAislamiento').val(idAislamiento);
            //$('button[data-id="ddlTipoAislamiento"]').html(aisl);
            $('#txtFechaInicioAisl').val(moment(data.HOS_fechaInicioHospitalizacion_Aislamiento).format('YYYY-MM-DD'));
            $('#txtFechaFinAisl').val(moment(data.HOS_fechaFinHospitalizacion_Aislamiento).format('YYYY-MM-DD'));
            $('#btnNuevoAislamiento').text('Editar Aislamiento');
        }
    });
}

function linkVerTraslados(sender) {
    'use strict';
    // GEN_idUbicacion_Ingreso
    var id = $(sender).data("id");
    var egreso = $(sender).data("egreso");
    var bMostrarEditar;
    idUbicacionActualCamaTraslado = 0;

    idCama = $(sender).data('idcama');
    $('#idHos').val(id);
    let data = llenarGrillaTraslado(bMostrarEditar);
    console.log("data " + JSON.stringify(data));

    if (!($.isEmptyObject(data))) {
        idUbicacionActualCamaTraslado = data.Hospitalizacion.idUbicacion;
    }
    console.log("Ubicacion > " + idUbicacionActualCamaTraslado);

    if (sSession.CODIGO_PERFIL == 5 && egreso == '') {
        bMostrarEditar = true;
        comboServicios('#ddlServicioDestino');
        mostrarServicioTrasladoExterno(idUbicacionActualCamaTraslado);
        $('#divNuevoTraslado').show();
        $('#dvSwitch, #ddlServicioDestino, #lblServicioDestinoTraslado, #ddlCamaDestino, #lblCamaDestinoTraslado, #aTraslado').show();
        $("#swTrasladoExterno").on("change", (e) => {
            mostrarServicioTrasladoExterno(idUbicacionActualCamaTraslado);
        });
    } else {
        bMostrarEditar = false;
        $('#dvSwitch, #ddlServicioDestino, #lblServicioDestinoTraslado, #ddlCamaDestino, #lblCamaDestinoTraslado, #aTraslado').hide();
    }

    $('#mdlTraslados').on('hidden.bs.modal', function () {
        $("#swTrasladosExterno").off();
    });

    console.log(idUbicacionActualCamaTraslado);
    $('#aTraslado').text('Agregar Traslado');
    $("#mdlTraslados").modal('show');
}

function mostrarServicioTrasladoExterno(idUbicacion = 0) {
    let checked = $("#swTrasladoExterno").is(":checked");
    if (checked) {
        $('.external-input').show();
        $("#ddlServicioDestino").val('0');
        $("#ddlServicioDestino option[value='" + idUbicacion.toString() + "']").prop('disabled', true);
        $('#ddlCamaDestino').empty().append("<option value='0'>-Seleccione-</option>");
    } else {
        console.log("Unchecked Location: " + idUbicacion);
        $("#ddlServicioDestino option[value='" + idUbicacion.toString() + "']").prop('disabled', false);
        $("#ddlServicioDestino").val(idUbicacion);
        CargarCama(idUbicacion, '#ddlServicioDestino');
        $('.external-input').hide();
    }
}

function linkEditarTraslado(sender) {

    var idTraslado = $(sender).data("id");
    var idOrigen = $(sender).data("idorigen");

    $('#idTraslado').val(idTraslado);
    $('#aTraslado').text('Editar Traslado');

    $('#ddlServicioDestino').val('0');
    $('#ddlCamaDestino').val('0');
    $('button[data-id="ddlServicioDestino"]').html('Seleccione');
    $('button[data-id="ddlCamaDestino"]').html('Seleccione');

}

function BtnNuevoFormRCE(sender) {

    setSession('ID_PACIENTE', $(sender).data("idPaciente"));
    setSession('ID_EVENTO', $(sender).data("idEvento"));
    window.location.href = ObtenerHost() + "/" + $(sender).attr("data-href");

}

function BtnCargarNuevoForm(sender) {

    var vURL = 'http://10.6.180.235/PABELLON/ING_Form_IQX_SP.aspx?id=0&c=';
    var json = GetLoginPass();
    var txtEncrypt = GetTextoEncriptado(json.ID_USUARIO + '|' + json.login + '|' + json.pass + '|' + $(sender).data("idPaciente"));
    //console.log(vURL + txtEncrypt);
    window.open(vURL + txtEncrypt, '_blank');

}

function llenarGrillaTraslado(bMostrarEditar) {

    var adataset = [];
    let json = {};
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "HOS_hospitalizacion/" + $('#idHos').val() + "/Traslados",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            console.log(GetWebApiUrl() + "HOS_hospitalizacion/" + $('#idHos').val() + "/Traslados");
            //GEN_idUbicacion = data[0].Hospitalizacion.GEN_idUbicacion;
            $('#txtPacienteTraslado').val(data[0].Paciente.nombre);
            $('#txtCamaActualTraslado').val(data[0].Hospitalizacion.cama);
            if (data.length > 0)
                json = data[0];

            $.each(data[0].Traslados, function (key, val) {
                adataset.push([
                    val.id,
                    data[0].Hospitalizacion.id,
                    val.tipo,
                    moment(moment(val.fecha).toDate()).format('DD-MM-YYYY HH:mm:ss'),
                    val.origen,
                    val.destino
                ]);
            });
            //LlenarGrillaTraslados(adataset, '#tblTrasladosPaciente', sSession);
            $('#tblTrasladosPaciente').addClass("nowrap").DataTable({
                data: adataset,
                columns: [
                    { title: "ID Traslados" },
                    { title: "IDHos" },
                    { title: "Tipo de Traslado" },
                    { title: "Fecha Traslado" },
                    { title: "Cama Origen" },
                    { title: "Cama Destino" },
                    { title: "" }
                ],
                "columnDefs": [
                    { "targets": 3, "sType": "date-ukLong" },
                    {
                        "targets": -1,
                        "visible": false,
                        "data": null,
                        orderable: false,
                        "render": function (data, type, row, meta) {
                            var fila = meta.row;
                            var botones = '';
                            if (fila == adataset.length - 1)
                                //botones = "<a id='linkEditarTraslado' data-id='" + adataset[fila][0] + "' data-idorigen='" + adataset[fila][2] + "' data-iddestino='" + adataset[fila][3] + "' class='btn btn-info btn-block' href='#/' onclick='linkEditarTraslado(this)'>Editar</a>";
                                botones = "";
                            return botones;
                        }
                    },
                    {
                        "targets": [1],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [-1],
                        "visible": (sSession.CODIGO_PERFIL == 5 && bMostrarEditar) ? true : false,
                        "searchable": false
                    }
                ],

                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) { },
                "bDestroy": true
            });
        }
    });
    return json;
}

function GetJsonHospitalizacion(HOS_idHospitalizacion) {
    var json = null;

    $.ajax({
        type: "GET",
        url: GetWebApiUrl() + "HOS_Hospitalizacion/Mapa/DetalleHospitalizacion/" + HOS_idHospitalizacion,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            if (data.length > 0)
                json = data[0];

        }
    });

    return json;
}

function linkVerEgresarHospitalizacion(HOS_idHospitalizacion) {

    var json = GetJsonHospitalizacion(HOS_idHospitalizacion);

    ShowModalAlerta("CONFIRMACION", "Confirmación",
        `   
            <div class='text-left' style='font-size:medium;'>
                
                <h5 class='text-center mb-4'><strong>Información de la hospitalización</strong></h5>
                
                <div class='row'>
                    <div class='col-md-6'>
                        <strong>${json.GEN_nombreIdentificacion}:</strong><br /> 
                        ${json.GEN_numero_documentoPaciente}
                    </div>
                    <div class='col-md-6'>
                        <strong>Fecha ingreso:</strong><br />
                        ${moment(json.HOS_fecha_ingreso_realHospitalizacion).format("DD-MM-YYYY HH:mm:SS")}
                    </div>
                </div>
            
                <div class='row'>
                    <div class='col-md-12'>
                        <strong>Nombre:</strong><br />
                        ${json.GEN_PersonasnombrePaciente}
                    </div>
                </div>
                <label for='txtFechaEgreso'>Fecha y hora de egreso</label>
                <div class="input-group mb-3">
                    <input id='txtFechaEgreso' type='date' class='form-control' aria-label="Recipient's username" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <input id='txtHoraEgreso' type='time' class='form-control clockpicker' /> 
                    </div>
                </div>
            </div>

            ¿Estás seguro qué quiere egresar a este paciente?
        `,
        function () { // Botón que egresa paciente

            var fechaActual = moment(GetFechaActual());
            var fechaEgreso = moment($("#txtFechaEgreso").val());
            var GEN_idPaciente = $(this).data("idPaciente");
            var esValido = true;

            if ($("#txtFechaEgreso").val() === "" || !fechaEgreso.isBefore(fechaActual)) {
                $("#txtFechaEgreso").addClass('invalid-input');
                esValido = false;
            } else $("#txtFechaEgreso").removeClass('invalid-input');

            if ($("#txtHoraEgreso").val() === "") {
                $("#txtHoraEgreso").addClass('invalid-input');
                esValido = false;
            } else $("#txtHoraEgreso").removeClass('invalid-input');

            // HAY QUE HACER CONTROLADOR PARA FILTRAR POR HOSPITALIZACIÓN
            //if (HayEpicrisis(GEN_idPaciente)) {
            //    esValido = false;
            //    $("#txtHoraEgreso").append("No se puede egresar el paciente porque éste no tiene epicrisis creada.");
            //}

            if (esValido) {
                EgresarHospitalizacion(HOS_idHospitalizacion);
                $('#modalAlerta').modal('hide');
            }

        },
        function () { // Botón cancelar
            $('#modalAlerta').modal('hide');
        }, "Egresar", "Cancelar");

    $("#btnAlertaAceptar").data("idPaciente", json.GEN_idPaciente);

    $(".clockpicker").clockpicker({ autoclose: true });

}

function HayEpicrisis(GEN_idPaciente) {

    var hayEpicrisis = false;

    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}HOS_Epicrisis/DetallePaciente/${GEN_idPaciente}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            hayEpicrisis = (data.length > 0);
        }
    });

    return hayEpicrisis;

}

function EgresarHospitalizacion(HOS_idHospitalizacion) {

    var json = {
        "HOS_fecha_egreso_realHospitalizacion": $("#txtFechaEgreso").val(),
        "HOS_hora_egreso_realHospitalizacion": $("#txtHoraEgreso").val()
    };

    $.ajax({
        type: "POST",
        url: GetWebApiUrl() + "HOS_Hospitalizacion/Egreso/Enfermeria/" + HOS_idHospitalizacion,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            toastr.success('El paciente se ha egresado correctamente.');
            getTablaHospitalizacion();
        },
        error: function (jqXHR, status) {
            console.log("Error al egresar paciente: " + JSON.stringify(jqXHR));
        }
    });
}

function linkHistorialHosp(event) {
    let id = $(event).data("idpac");
    let nombre = $(event).data("nompac");
    ShowModalHospitalizaciones(id, nombre);
}
