﻿
var sSession = {};
var estado = null;
var HOS_Hospitalizacion = {};
var alertFlag = 0;
var hosFlag = 0;

var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
}

function mostrarModalHospitalizacionPrevia(id) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idPaciente=${id}&idTipoEstado=87`,
        success: function (data, status, jqXHR) {
            if (data.length > 0) {
                let fechaIngreso = (data[0].Fecha != null) ?
                    moment(data[0].Fecha).format("DD-MM-YYYY") : "";
                let horaIngreso = (data[0].Hora != null) ?
                    data[0].Hora : "";
                let ubicacion = (data[0].Cama != null) ?
                    data[0].Cama : "";
                $("#stgHospitalizacion").html(`<i class="fas fa-bed"></i> ${fechaIngreso} ${horaIngreso} ${ubicacion}`);
                $("#aEditarHospitalizacion").data("idHos", data[0].Id);
                $("#mdlHosActiva").modal("show");
            }
        },
        error: function (jqXHR, status) {
            console.log(`No se ha podido verificar si el paciente posee hospitalizacion previa. Error: ${JSON.stringify(jqXHR)}`);
        }
    });
}

// DOCUMENT
$(document).ready(function () {

    let IniciarComponentes = async () => {

        ShowModalCargando(true);
        await sleep(0);

        let html = $("#divDatosAdicionalesPaciente").html();
        $("#divEgreso").hide();
        $("#divDatosAdicionalesPaciente").remove();
        $("#divDatosPaciente .card-body").append(`<div id='divDatosAdicionalesPaciente'>${html}</div>`);

        let fechaHoy = moment(GetFechaActual()).format("YYYY-MM-DD");
        $("#txtFechaIngreso").attr("min", AgregarDias(fechaHoy, -7));
        $("#txtFechaIngreso").attr("max", AgregarDias(fechaHoy, 7));
        $("#txtFechaIngreso").val(GetFechaActual().format("yyyy-MM-dd"));

        $("textarea").val("");

        $(window).scroll(function () {
            if (alertFlag) {
                ($($(this)).scrollTop() > 150) ? $('#alertObligatorios').stop().show('fast') : $('#alertObligatorios').stop().hide('fast');
            }
        });

        
        $(window).scroll(function () {
            if (hosFlag) {
                ($($(this)).scrollTop() > 150) ? $('#alertHospitalizacionActiva').stop().show('fast') : $('#alertHospitalizacionActiva').stop().hide('fast');
            }
        });
        
        window.addEventListener('beforeunload', bunload, false);

        // Llena combos
        EstablecerSesion();
        RevisarAcceso(true, sSession);

        LlenarCombos();

        $.fn.bootstrapSwitch.defaults.onColor = 'info';
        $.fn.bootstrapSwitch.defaults.offColor = 'danger';
        $.fn.bootstrapSwitch.defaults.onText = 'SI';
        $.fn.bootstrapSwitch.defaults.offText = 'NO';
        $.fn.bootstrapSwitch.defaults.labelWidth = '1';
        DarFuncionRadios();
        InicializarComponentes();
        //
        //RUT PACIENTE VERIFICAR HOSPITALIZACIONES PREVIAS
        $("#txtnumerotPac").on("blur", function () {
            if ($("#sltIdentificacion").val() === '2' || $("#sltIdentificacion").val() === '3') {
                if (GetPaciente().GEN_idPaciente != undefined) {
                    mostrarModalHospitalizacionPrevia(GetPaciente().GEN_idPaciente);
                }
            }
        });

        $("#txtDigitoPac").on("blur", function () {
            if ($("#sltIdentificacion").val() === '1' || $("#sltIdentificacion").val() === '4') {
                if (GetPaciente().GEN_idPaciente != undefined) {
                    //mostrarModalHospitalizacionPrevia(GetPaciente().GEN_idPaciente);
                }
            }
        });

        // PREVISION
        $('#sltPrevision').change(function () {
            CargarComboHijo('#sltPrevision', '#sltPrevisionTramo', ComboTramo);
        });

        // SERVICIO SALUD
        $("#sltServicioSaludIngreso").change(function () {
            CargarComboHijo("#sltServicioSaludIngreso", "#sltEstablecimientoIngreso", ComboEstablecimiento);
        });

        $('#sltEstablecimientoIngreso').change(function () {
            CargarComboHijo('#sltEstablecimientoIngreso', "#sltUbicacionIngreso", ComboUbicacion);
        });

        // PROCEDENCIA
        $("#sltProcedenciaIngreso").change(function () {
            if ($("#sltProcedenciaIngreso").val() == '3') {
                $('#sltEstablecimientoProcedenciaIngreso').prop('disabled', false);
                $('#sltEstablecimientoProcedenciaIngreso').attr('data-required', 'true');
                ComboEstablecimiento($('#sltEstablecimientoProcedenciaIngreso'), "1");
            } else {
                $('#sltEstablecimientoProcedenciaIngreso').prop('disabled', true);
                $('#sltEstablecimientoProcedenciaIngreso').attr('data-required', 'false');
                $('#sltEstablecimientoProcedenciaIngreso').val('0');
            }
            ReiniciarRequired();
        });

        // CheckBox LEY PREVISIONAL INGRESO
        $('#chbLeyPrevisionalIngreso').on('switchChange.bootstrapSwitch', function (event, state) {
            $('#sltLeyPrevisionalIngreso').prop('disabled', !state).attr("data-required", state);
            $('#sltLeyPrevisionalIngreso').val('0');
            ReiniciarRequired();
        });

        // CheckBox LEY PREVISIONAL EGRESO
        $('#chbLeyPrevisionalEgreso').on('switchChange.bootstrapSwitch', function (event, state) {
            $('#sltLeyPrevisionalEgreso').prop('disabled', !state);
            $('#sltLeyPrevisionalEgreso').attr("data-required", state);
            ReiniciarRequired();
        });

        $("#txtFechaEgreso").on('blur', function () {
            $("#txtDiasEstada").val(" " + GetDiferenciaFechas($("#txtFechaIngreso").val(),
                $("#txtFechaEgreso").val()));
        });

        $('#btnGuardar').on('click', function () {

            if (parseInt($("#txtFechaIngreso").val()) > parseInt($("#txtFechaIngreso").attr("max")) || parseInt($("#txtFechaIngreso").val()) < parseInt($("#txtFechaIngreso").attr("min")))
            {
                $("#txtFechaIngreso").addClass('is-invalid');
                return false;
            }

            ShowModalCargando(true);
            $("#btnGuardar").attr("disabled", "disabled").addClass("disabled");
            GuardarHospitalizacion();
        });

        $("#txtDigitoPac").on('blur', function () {
            avisoHospitalizaciones();
        });

        $("#sltTipoDocumento").on('change', function () {
            if ($(this).val() != 0 && $("#txtNumeroDocumento").val()) {
                CargarPaciente($("#sltTipoDocumento").val(), $("#txtNumeroDocumento").val());
            }
        });

        if (sSession.ID_PACIENTE != null) {
            CargarPaciente(sSession.ID_PACIENTE);
            avisoHospitalizaciones();
            if (sSession.ID_INGRESO_MEDICO !== undefined) {
                DeshabilitarPaciente(false);
                $("#sltIdentificacion, #txtnumerotPac, #txtDigitoPac").attr("disabled", "disabled");
            }
        }

        //Pregunta si es un formulario nuevo o esta editando
        if (sSession.ID_HOSPITALIZACION != null) {
            CargarHospitalizacion(sSession.ID_HOSPITALIZACION);
            $('#sltIdentificacion').attr('disabled', 'disabled');
            $('#txtnumerotPac').attr('disabled', 'disabled');
            $('#txtDigitoPac').attr('disabled', 'disabled');
        } else {
            $("#btnCancelar").on('click', function () { CancelarFormularioAdmision(); });
            $('.panel-egreso').hide();
        }

        if (sSession.TIPO_EDICION == 'EGRESO') {

        }

        if (sSession.TIPO_EDICION == 'EGRESO') {
            $('#sltIdentificacion').attr('disabled', 'disabled');
            $('#txtnumerotPac').attr('disabled', 'disabled');
            $('#txtDigitoPac').attr('disabled', 'disabled');
            $('#txtnombrePac').attr('disabled', 'disabled');
            $('#txtApePat').attr('disabled', 'disabled');
            $('#txtApeMat').attr('disabled', 'disabled');
            $('#txtFecNacPac').attr('disabled', 'disabled');
            $('#txtDireccionPaciente').attr('disabled', 'disabled');
            $('#txtTelefono').attr('disabled', 'disabled');
            $('#txtOtroTelefono').attr('disabled', 'disabled');
            $('#txtCorreoElectronico').attr('disabled', 'disabled');
        }

        $('#mdlImprimir').on('hidden.bs.modal', function () {
            quitarListener();
            location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`;
        });

        $("#aNuevaHospitalizacion").click(function () {
            ReiniciarComponentesClinicos();
            delete sSession.ID_HOSPITALIZACION;
            delete sSession.TIPO_EDICION;
            $("#mdlHosActiva").modal("hide");
        });

        $('#aEditarHospitalizacion').click(function () {
            let idHos = $(this).data("idHos");
            sSession.ID_HOSPITALIZACION = idHos;
            sSession.TIPO_EDICION = 'EDICION';
            ReiniciarComponentesClinicos();
            CargarHospitalizacion(idHos);
            $("#mdlHosActiva").modal("hide");
        });

        $("#aCancelarIngresoHospitalizacion").click(function () {
            ReiniciarComponentesPaciente();
            delete sSession.ID_HOSPITALIZACION;
            delete sSession.TIPO_EDICION;
            $("#mdlHosActiva").modal("hide");
        });
    }

    IniciarComponentes().then(() => ShowModalCargando(false));

});

function InicializarComponentes() {
    $('#sltServicioSaludIngreso').val('1');
    ComboEstablecimiento('#sltEstablecimientoIngreso', '1');
    ComboUbicacion("#sltUbicacion", '1');
    $('#sltEstablecimientoIngreso').val('1');
    CargarCombosUbicacionPaciente();
}
function ReiniciarComponentesPaciente() {
    LimpiarPaciente();
    DeshabilitarPaciente(true);
    $("#txtnumerotPac, #txtDigitoPac").val('');
}
function ReiniciarComponentesClinicos() {
    $("#txtFechaIngreso, #txtHoraIngreso, #txtMotivoHospitalizacion").val('');
    $("#sltEstablecimientoProcedenciaIngreso, sltLeyPrevisionalIngreso").val('0');
    $("#sltProcedenciaIngreso").val('0').change();
    $("#sltServicioSaludIngreso").change().val('1');
    $("#sltEstablecimientoIngreso").change().val('1');
    $("#sltUbicacionIngreso, #sltTipoHospitalizacion, #sltModalidadFonasaIngreso").val('0');
    $("#sltUbicacionIngreso, #sltEstablecimientoIngreso").removeAttr("disabled");
    $("#chbLeyPrevisionalIngreso").bootstrapSwitch('state', false);
}
function EstablecerSesion() {
    sSession = getSession();
    deleteSession("ID_HOSPITALIZACION");
    deleteSession("ID_PACIENTE");
    deleteSession("TIPO_EDICION");
}

//FIN DOCUMENT
function imprimirFormulario() {
    var id = sSession.ID_HOSPITALIZACION;
    $("#modalCargando").show();
    $('#frameHospitalizacion').attr('src', 'ImprimirFormularioAdmision.aspx?id=' + id);
    $('#mdlImprimir').modal('show');
}
function OmitirImpresion() {
    quitarListener();
    location.href = ObtenerHost() + "/Vista/ModuloHOS/BandejaHOS.aspx";
}
function CargarHospitalizacion(idHos) {

    if (idHos != undefined) {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "/HOS_Hospitalizacion/" + idHos,
            async: false,
            success: function (data, status, jqXHR) {

                var json = data[0];

                // CARGA PACIENTE
                console.log("/HOS_Hospitalizacion/" + idHos);
                buscarPacientePorId(json.Paciente.IdPaciente).then(() => {

                    if (sSession.TIPO_EDICION == "EDICION") {
                        DeshabilitarPaciente(false);
                        //$("#txtnumerotPac").attr("disabled", "disabled");
                    }

                    // PROCEDENCIA
                    $("#sltProcedenciaIngreso").val(json.Ingreso.IdProcedencia);
                    //$("#sltProcedenciaIngreso").val(json.HOS_idProcedencias_Ingreso); ...
                    if (json.Ingreso.IdProcedencia == 3)
                        $("#sltProcedenciaIngreso").change();

                    // OTRO ESTABLECIMIENTO
                    if (json.Ingreso.IdEstablecimiento != null)
                        $("#sltEstablecimientoProcedenciaIngreso").val(json.Ingreso.IdEstablecimiento);
                        //$("#sltEstablecimientoProcedenciaIngreso").val(json.GEN_idEstablecimiento);
                        // ...Etc

                    // FECHA INGRESO
                    let fechaIngreso = moment(json.Ingreso.Fecha, "YYYY.MM.DD HH.mm.ss").toDate();
                    $("#txtFechaIngreso").val(moment(fechaIngreso).format('YYYY-MM-DD'));

                    // HORA INGRESO
                    $("#txtHoraIngreso").val(json.Ingreso.Hora);

                    // SERVICIO DE SALUD
                    $("#sltServicioSaludIngreso").val('1');
                    //$("#sltServicioSaludIngreso").prop('value', 1).selectpicker('refresh');
                    // ESTABLECIMIENTO
                    CargarComboHijo("#sltServicioSaludIngreso", "#sltEstablecimientoIngreso", ComboEstablecimiento);
                    //$("#sltEstablecimientoIngreso").selectpicker('val', 1);
                    $("#sltEstablecimientoIngreso").val(1);

                    if (json.Ingreso.IdLeyPrevisional != null) {
                        $("#chbLeyPrevisionalIngreso").bootstrapSwitch('state', true);
                        $("#sltLeyPrevisionalIngreso").val(json.Ingreso.IdLeyPrevisional);
                    } else {
                        $("#chbLeyPrevisionalIngreso").bootstrapSwitch('state', false);
                    }

                    // UBICACION INGRESO
                    $("#sltUbicacionIngreso").val(json.Ingreso.IdUbicacion ? json.Ingreso.IdUbicacion: "0");

                    // PREVISION Y TRAMO DE INGRESO
                    $("#sltPrevision").val(json.Ingreso.IdPrevision).change();
                    $("#sltPrevisionTramo").val(json.Ingreso.IdPrevisionTramo);
                    $("#sltModalidadFonasaIngreso").val(json.Ingreso.IdModalidad);
                    $("#sltTipoHospitalizacion").val(json.TipoHospitalizacion.Id);
                    $("#txtMotivoHospitalizacion").val(json.MotivoHospitalizacion);
                    //PACIENTE AMBULATORIO
                    $("#chbPacienteAmbulatorio").bootstrapSwitch('state', (json.IdAmbito == 1) ? false : true);
                    //Ley previsional egreso
                    $("#chbLeyPrevisionalEgreso").bootstrapSwitch('state', (json.Egreso.IdLeyPrevisionalEgreso != null), true);

                    //DATOS DE EGRESO
                    var fechaEgreso;
                    if (json.Egreso.Fecha != null) {
                        fechaEgreso = moment(json.Egreso.Fecha, 'YYYY-MM-DD HH:mm:ss').toDate();
                        $("#txtFechaEgreso").val(moment(fechaEgreso).format('YYYY-MM-DD'));
                        $("#txtHoraEgreso").val(moment(fechaEgreso).format('HH:mm'));
                        $("#txtDiasEstada").val(" " + GetDiferenciaFechas($("#txtFechaIngreso").val(),
                            $("#txtFechaEgreso").val()));
                        if (sSession.TIPO_EDICION != "EGRESO") {
                            $(".panel-egreso").hide();
                            $("select.datos-egreso").prop('disabled', true);
                            $(".datos-egreso").attr("disabled", "disabled").addClass("disabled");
                        }
                        window.removeEventListener('beforeunload', bunload, false);
                        $("#btnCancelar").on('click', function () { location.href = ObtenerHost() + "/Vista/ModuloHOS/BandejaHOS.aspx" });
                    } else if (sSession.TIPO_EDICION == "EGRESO") {
                        $(".datos-egreso").attr("data-required", "true");
                        $("#sltLeyPrevisionalEgreso").attr("data-required", "false");
                        $("#btnCancelar").on('click', function () { CancelarFormularioAdmision(); });
                        fechaEgreso = GetFechaActual();
                        $("#txtFechaEgreso").val(moment(fechaEgreso).format('YYYY-MM-DD'));
                        $("#txtHoraEgreso").val(moment(fechaEgreso).format('HH:mm'));
                        $("#txtDiasEstada").val(" " + GetDiferenciaFechas($("#txtFechaIngreso").val(),
                            $("#txtFechaEgreso").val()));
                    } else {
                        $(".panel-egreso").hide();
                        $("select.datos-egreso").prop('disabled', true);
                        $(".datos-egreso").attr("disabled", "disabled").addClass("disabled");
                        window.removeEventListener('beforeunload', bunload, false);
                        $("#btnCancelar").on('click', function () { location.href = ObtenerHost() + "/Vista/ModuloHOS/BandejaHOS.aspx" });
                    }

                    //$("#sltModalidadFonasaEgreso").val(json.GEN_idModalidad_Fonasa_Egreso);
                    $("#sltModalidadFonasaEgreso").val(json.Egreso.IdModalidadEgreso);
                    $("#chbLeyPrevisionalEgreso").bootstrapSwitch('state', (json.Egreso.IdLeyPrevisionalEgreso != null), true);
                    $("#sltLeyPrevisionalEgreso").val(json.Egreso.IdLeyPrevisionalEgreso);

                    $("#sltTipoEgreso").val(json.Egreso.IdTipoEgreso);
                    $("#sltDestinoAlta").val(json.IdDestinoAlta);
                    //$("#txtDiagnostico").text(json.HOS_diagnostico_principalEpicrisis);
                    estado = json.EstadoSistema.Id;

                    $("label").removeClass("active");
                    $("label").addClass("active");

                    if (sSession.TIPO_EDICION == "EGRESO") {
                        $("#divEgreso").show();
                        //Deshabilita los datos del paciente y los datos de ingreso
                        $("select.datos-pac, select.datos-ingreso").prop('disabled', true);
                        //$(".selectpicker.datos-pac, .selectpicker.datos-ingreso").prop('disabled', true).selectpicker('refresh');
                        $(".datos-pac, .datos-ingreso").addClass("disabled");
                        //hace un scroll hacia el apartado del egreso
                        $('html, body').animate({ scrollTop: $("#divDatosEgreso").offset().top - 40 }, 1000);
                    }

                });
            },
            error: function (jqXHR, status) {
                console.log("Error al cargar hospitalización " + jqXHR.responseText);
            }
        });

    } else {
        $("select.datos-egreso").prop('disabled', true);
        $(".datos-egreso").attr("disabled", "disabled").addClass("disabled");
    }

}

//Llaves del POST
function GetJSONHospitalizacionIngreso() {
    return {
        "IdPaciente": GetPaciente().GEN_idPaciente,
        "FechaIngreso": $("#txtFechaIngreso").val(),
        "HoraIngreso": $("#txtHoraIngreso").val(),
        "IdPrevisionIngreso": valCampo(parseInt($("#sltPrevision").val())),
        "IdPrevisionTramoIngreso": valCampo(parseInt($("#sltPrevisionTramo").val())),
        "IdModalidadFonasaIngreso": valCampo(parseInt($("#sltModalidadFonasaIngreso").val())),
        "IdLeyPrevisionalIngreso": valCampo(parseInt($("#sltLeyPrevisionalIngreso").val())),
        "IdEstablecimiento": valCampo(parseInt($("#sltEstablecimientoProcedenciaIngreso").val())),
        "IdProcedenciasIngreso": valCampo(parseInt($("#sltProcedenciaIngreso").val())),
        "IdUbicacionIngreso": valCampo(parseInt($("#sltUbicacionIngreso").val())),
        "IdOcupaciones": GetPaciente().GEN_idOcupaciones,
        "IdCategoriasOcupacion": GetPaciente().GEN_idTipo_Categoria_Ocupacion,
        "IdTipoHospitalizacion": valCampo(parseInt($("#sltTipoHospitalizacion").val())),
        "MotivoHospitalizacion": valCampo($("#txtMotivoHospitalizacion").val())
    };
}

//JSON INGRESO
function GetJSONHospitalizacionIngresoViejo() {

    //GEN_idPaciente
    HOS_Hospitalizacion.GEN_idPaciente = GetPaciente().GEN_idPaciente;
    //Prevision Ingreso
    HOS_Hospitalizacion.GEN_idPrevision_Ingreso = valCampo(parseInt($("#sltPrevision").val()));
    //Prevision Tramo Ingreso
    HOS_Hospitalizacion.GEN_idPrevision_Tramo_Ingreso = valCampo(parseInt($("#sltPrevisionTramo").val()));

    //HOS_fecha_ingreso_realHospitalizacion
    HOS_Hospitalizacion.HOS_fecha_ingreso_realHospitalizacion = $("#txtFechaIngreso").val();
    //HOS_hora_ingreso_realHospitalizacion
    HOS_Hospitalizacion.HOS_hora_ingreso_realHospitalizacion = $("#txtHoraIngreso").val();
    //HOS_idOrigen_Ingreso
    HOS_Hospitalizacion.HOS_idOrigen_Ingreso = 1;

    //HOS_idProcedencias_Ingresoc
    HOS_Hospitalizacion.HOS_idProcedencias_Ingreso = valCampo(parseInt($("#sltProcedenciaIngreso").val()));

    //sltEstablecimientoProcedenciaIngreso
    HOS_Hospitalizacion.GEN_idEstablecimiento = valCampo(parseInt($("#sltEstablecimientoProcedenciaIngreso").val()));

    //GEN_idUbicacion_Ingreso
    HOS_Hospitalizacion.GEN_idUbicacion_Ingreso = valCampo(parseInt($("#sltUbicacionIngreso").val()));

    //GEN_idLey_Previsional_Ingreso
    HOS_Hospitalizacion.GEN_idLey_Previsional_Ingreso = valCampo(parseInt($("#sltLeyPrevisionalIngreso").val()));

    HOS_Hospitalizacion.GEN_idAmbito = 1;
    HOS_Hospitalizacion.GEN_idModalidad_Fonasa_Ingreso = valCampo(parseInt($("#sltModalidadFonasaIngreso").val()));
    HOS_Hospitalizacion.HOS_idTipo_Hospitalizacion = valCampo(parseInt($("#sltTipoHospitalizacion").val()));
    HOS_Hospitalizacion.HOS_motivoHospitalizacion = valCampo($("#txtMotivoHospitalizacion").val());

    return HOS_Hospitalizacion;
}

//COMBOS
function LlenarCombos() {

    ComboServicioDeSalud();
    ComboPuebloOriginario();
    ComboTipoEscolaridad();
    ComboNacionalidad();
    ComboPrevision();
    ComboLeyPrevisional();
    ComboTipoEgreso();
    ComboDestinoAlta();
    ComboCategoriaOcupacion();
    ComboOcupacion();
    ComboProcedencia();
    ComboModalidadFonasa();
    ComboTipoHospitalizacion();

    $("#sltPrevisionTramo").prop('disabled', true).append("<option value='0'>Seleccione</option>");

    $('#sltLeyPrevisionalIngreso').prop('disabled', true);
    $('#sltLeyPrevisionalEgreso').prop('disabled', true);
    $('#sltEstablecimientoProcedenciaIngreso').prop('disabled', true).append("<option value='0'>Seleccione</option>");

}
function ComboTipoHospitalizacion() {
    let url = `${GetWebApiUrl()}HOS_Tipo_Hospitalizacion/Combo`;
    setCargarDataEnCombo(url, false, $("#sltTipoHospitalizacion"));
}
function ComboServicioDeSalud() {
    let url = `${GetWebApiUrl()}GEN_Servicio_Salud/Combo`;
    setCargarDataEnCombo(url, false, $("#sltServicioSaludIngreso"));
}
function ComboEstablecimiento(slt, idServicioSalud) {
    let url = `${GetWebApiUrl()}GEN_Establecimiento/ServicioSalud/${idServicioSalud}`;
    setCargarDataEnCombo(url, false, $("#sltEstablecimientoIngreso"));
}
function ComboModalidadFonasa() {
    let url = `${GetWebApiUrl()}GEN_Modalidad_Fonasa/Combo`;
    setCargarDataEnCombo(url, false, $("#sltModalidadFonasaIngreso, #sltModalidadFonasaEgreso"));
}
function ComboPuebloOriginario() {
    let url = `${GetWebApiUrl()}GEN_Pueblo_originario/combo`;
    setCargarDataEnCombo(url, false, $("#sltPuebloOriginario"));
}

function ComboLeyPrevisional() {
    let url = `${GetWebApiUrl()}GEN_Ley_Previsional/Combo`;
    setCargarDataEnCombo(url, false, $("#sltLeyPrevisionalIngreso, #sltLeyPrevisionalEgreso"));
}

function ComboTipoEgreso() {
    let url = `${GetWebApiUrl()}HOS_Tipo_Egreso/Combo`;
    setCargarDataEnCombo(url, false, $("#sltTipoEgreso"));
}

function ComboUbicacion(slt, idEstablecimiento) {
    let url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/${idEstablecimiento}`;
    setCargarDataEnCombo(url, false, $("#sltUbicacionIngreso"));
}

function ComboDestinoAlta() {
    let url = `${GetWebApiUrl()}HOS_Destino_Alta/Combo`;
    setCargarDataEnCombo(url, false, $("#sltDestinoAlta"));
}



function ComboProcedencia() {
    let url = `${GetWebApiUrl()}HOS_Procedencias_Ingreso/Combo`;
    setCargarDataEnCombo(url, false, "#sltProcedenciaIngreso");
}

async function GuardarHospitalizacion() {

    alertFlag = 0;

    if (sSession.TIPO_EDICION == "EGRESO" && validarCampos("#divDatosEgreso", true)) {
        //
        let idHos = sSession.ID_HOSPITALIZACION;
        let json = GetJSONHospitalizacion();
        //Asigna al JSON el estado de Hospitalizacion egresada
        json.GEN_idTipo_Estados_Sistemas = 89;

        //Valida formato de correo y telefono
        //if (!validarFormatos()) return;

        await $.ajax({
            type: 'PUT',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/${idHos}`,
            data: JSON.stringify(json),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data, status, jqXHR) {
                toastr.success('Paciente se ha egresedo exitósamente.');
                $('#mdlAlerta').modal("show");
            },
            error: function (jqXHR, status) {
                console.log(`Error al Actualizar Hospitalizacion ${JSON.stringify(jqXHR)}`);
            }
        });

    } else if (sSession.TIPO_EDICION == "EDICION" && (
        ((estado == 87 || estado == 88) && (
            validarCampos("#divIngreso", true) &
            validarCampos("#divDatosAdicionalesPaciente", true) &
            validarCampos("#divDatosPaciente", true)
        ))
        || (estado == 89 && (
            validarCampos("#divDatosEgreso", true) &
            validarCampos("#divIngreso", true) &
            validarCampos("#divDatosAdicionalesPaciente", true) &
            validarCampos("#divDatosPaciente", true)
        )))) {

        //SE EDITA
        //Valida formato de correo y telefono
        if (!validarFormatos()) return;

        await GuardarPaciente();
        var idHos = sSession.ID_HOSPITALIZACION;
        //Se Obtiene el JSON para guardar la Hospitalizacion normal
        var json = await GetJSONHospitalizacion();

        await $.ajax({
            type: 'PUT',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/${idHos}`,
            data: JSON.stringify(json),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data, status, jqXHR) {
                toastr.success('La Hospitalizacion se ha editado exitósamente.');
                quitarListener();
                window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`;
            },
            error: function (jqXHR, status) {
                console.log(`Error al Actualizar Hospitalizacion ${JSON.stringify(jqXHR)}`);
            }
        });

    } else if ((sSession.TIPO_EDICION != "EGRESO" && sSession.TIPO_EDICION != "EDICION") && (
        validarCampos("#divIngreso", true) &
        validarCampos("#divDatosAdicionalesPaciente", true) &
        validarCampos("#divDatosPaciente", true))) {

        //Valida formato de correo y telefono
        if (!validarFormatos()) return;

        await GuardarPaciente();
        let json = await GetJSONHospitalizacionIngreso();
        let paramIngreso_Medico = (sSession.ID_INGRESO_MEDICO != null) ? sSession.ID_INGRESO_MEDICO : "0";
        let urlPost = GetWebApiUrl() + "HOS_Hospitalizacion";
        console.log("Hos POST");
        //let urlPost = GetWebApiUrl() + "HOS_Hospitalizacion/" + paramIngreso_Medico + "/" + GetPaciente().GEN_idPaciente;
        await $.ajax({
            type: 'POST',
            url: urlPost,
            data: JSON.stringify(json),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data, status, jqXHR) {
                toastr.success('Formulario de Ingreso se ha creado exitosamente.');
                quitarListener();
                window.location.href = ObtenerHost() + "/Vista/ModuloHOS/BandejaHOS.aspx";
            },
            error: function (jqXHR, status) {
                console.log("Error al ingresar hospitalización:" + JSON.stringify(jqXHR));
            }
        });

    } else {
        //Marca de RCE
        alertFlag = 1;
    }

    ShowModalCargando(false);
    $("#btnGuardar").removeAttr("disabled").removeClass("disabled");

}
function validarFormatos() {

    let flag = 0;

    //if (!$("#txtTelefono").val().match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im)) {
    //    $("#txtTelefono").addClass('invalid-input');
    //    flag = 1;
    //} else {
    //    $("#txtTelefono").removeClass('invalid-input');
    //}

    //if (!$("#txtOtroTelefono").val().match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im)) {
    //    $("#txtOtroTelefono").addClass('invalid-input');
    //    flag = 1;
    //} else {
    //    $("#txtOtroTelefono").removeClass('invalid-input');
    //}

    //if (!$("#txtCorreoElectronico").val().match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)) {
    //    $("#txtCorreoElectronico").addClass('invalid-input');
    //    flag = 1;
    //} else {
    //    $("#txtCorreoElectronico").removeClass('invalid-input');
    //}

    if (flag == 1)
        return 0;
    else
        return 1;
}

function GetJSONHospitalizacion() {

    var jsonDatHosp = {};
    var idHos = sSession.ID_HOSPITALIZACION;
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "HOS_Hospitalizacion/" + idHos,
        async: false,
        success: function (data, status, jqXHR) {
            jsonDatHosp = data[0];
        },
        error: function (jqXHR, status) {
            console.log("Error al actualizar hospitalización " + jqXHR.responseText);
        }
    });

    let respuesta = mapHospitalizacionActual(jsonDatHosp);
    console.log(JSON.stringify(respuesta));
    return respuesta;
}

function mapHospitalizacionActual(HOSJ) {
    return {
        "IdHospitalizacion": HOSJ.HOS_idHospitalizacion,
        "IdPaciente": HOSJ.Paciente.IdPaciente,
        "FechaIngreso": moment($("#txtFechaIngreso").val()).format("YYYY-MM-DD"),
        "HoraIngreso": valCampo($("#txtHoraIngreso").val()),
        "IdPrevisionIngreso": HOSJ.Ingreso.IdPrevision,
        "IdPrevisionTramoIngreso": HOSJ.Ingreso.IdPrevisionTramo,
        "IdModalidadFonasaIngreso": $("#sltModalidadFonasaIngreso").val() === "0" ? null : $("#sltModalidadFonasaIngreso").val(),
        "IdLeyPrevisionalIngreso": $("#sltLeyPrevisionalIngreso").val() === "0" ? null : $("#sltLeyPrevisionalIngreso").val(),
        "IdEstablecimiento": $("#sltEstablecimientoProcedenciaIngreso").val() === "0" ? null : $("#sltEstablecimientoProcedenciaIngreso").val(),
        "IdProcedenciasIngreso": valCampo($("#sltProcedenciaIngreso").val()),
        "IdUbicacionIngreso": $("#sltUbicacionIngreso").val() === "0" ? null : $("#sltUbicacionIngreso").val(),
        "IdOcupaciones": HOSJ.Ocupaciones.Id,
        //Permite null (Null por mientras)
        "IdCategoriasOcupacion": null,
        "IdTipoHospitalizacion": valCampo($("#sltTipoHospitalizacion").val()),
        "MotivoHospitalizacion": valCampo($("#txtMotivoHospitalizacion").val()),
    };
}
//Lo dejo por cualquier cotsa
function mapHospitalizacionAntiguo(jsonHos) {
    return {
        "HOS_idHospitalizacion": jsonHos.HOS_idHospitalizacion,
        "GEN_idPaciente": jsonHos.GEN_idPaciente,
        "HOS_fecha_ingreso_realHospitalizacion": moment($("#txtFechaIngreso").val()).format("YYYY-MM-DD"),
        "HOS_hora_ingreso_realHospitalizacion": valCampo($("#txtHoraIngreso").val()),
        "HOS_fecha_registro_ingresoHospitalizacion": jsonHos.HOS_fecha_registro_ingresoHospitalizacion,
        "HOS_idProcedencias_Ingreso": jsonHos.HOS_idProcedencias_Ingreso,
        "GEN_idUbicacion_Ingreso": jsonHos.GEN_idUbicacion_Ingreso,
        "HOS_idCama_Ingreso": jsonHos.HOS_idCama_Ingreso,
        "GEN_idPrevision_Ingreso": jsonHos.GEN_idPrevision_Ingreso,
        "GEN_idPrevision_Tramo_Ingreso": jsonHos.GEN_idPrevision_Tramo_Ingreso,
        "GEN_idLey_Previsional_Ingreso": valCampo($("#sltLeyPrevisionalIngreso").val()),
        "HOS_ingreso_confirmadoHospitalizacion": jsonHos.HOS_ingreso_confirmadoHospitalizacion,
        "HOS_fecha_egreso_realHospitalizacion": jsonHos.HOS_fecha_egreso_realHospitalizacion,
        "HOS_hora_egreso_realHospitalizacion": jsonHos.HOS_hora_egreso_realHospitalizacion,
        "HOS_fecha_registro_egresoHospitalizacion": jsonHos.HOS_fecha_registro_egresoHospitalizacion,
        "HOS_idTipo_Egreso": (valCampo($("#sltTipoEgreso").val()) === null ? null : parseInt(valCampo($("#sltTipoEgreso").val()))),
        "GEN_idAmbito_Jerarquia_Egreso": jsonHos.GEN_idAmbito_Jerarquia_Egreso,
        "GEN_idCama_Egreso": jsonHos.GEN_idCama_Egreso,
        "GEN_idPrevision_Egreso": jsonHos.GEN_idPrevision_Egreso,
        "GEN_idPrevision_Tramo_Egreso": jsonHos.GEN_idPrevision_Tramo_Egreso,
        "GEN_idLey_Previsional_Egreso": (valCampo($("#sltLeyPrevisionalEgreso").val()) === null ? null : parseInt(valCampo($("#sltLeyPrevisionalEgreso").val()))),
        "HOS_idDestino_Alta": (valCampo($("#sltDestinoAlta").val()) === null ? null : parseInt(valCampo($("#sltDestinoAlta").val()))),
        "HOS_egreso_confirmadoHospitalizacion": jsonHos.HOS_egreso_confirmadoHospitalizacion,
        "HOS_estadoHospitalizacion": jsonHos.HOS_estadoHospitalizacion,
        "RCE_idEventos": jsonHos.RCE_idEventos,
        "HOS_fecha_egreso_clinicoHospitalizacion": moment($("#txtFechaEgreso").val()).format("YYYY-MM-DD") + "T" + $("#txtHoraEgreso").val() + ":00",
        "GEN_idAmbito": jsonHos.GEN_idAmbito,
        "GEN_idModalidad_Fonasa_Ingreso": valCampo($("#sltModalidadFonasaIngreso").val()),
        "GEN_idModalidad_Fonasa_Egreso": valCampo($("#sltModalidadFonasaEgreso").val()),
        "HOS_idTipo_Hospitalizacion": valCampo($("#sltTipoHospitalizacion").val()),
        "HOS_motivoHospitalizacion": valCampo($("#txtMotivoHospitalizacion").val())
    }
}

function GetJSONHospitalizacionEdicion() {

    var HOS_Hospitalizacion = {};
    var idHos = sSession.ID_HOSPITALIZACION;

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "HOS_Hospitalizacion/" + idHos,
        async: false,
        success: function (data, status, jqXHR) {
            HOS_Hospitalizacion = data[0];
        },
        error: function (jqXHR, status) {
            console.log("Error al actualizar hospitalización " + jqXHR.responseText);
        }
    });

    //DATOS INGRESO
    //Prevision Ingreso
    HOS_Hospitalizacion["GEN_idPrevision_Ingreso"] = valCampo(parseInt($("#sltPrevision").val()));
    //Prevision Tramo Ingreso
    HOS_Hospitalizacion["GEN_idPrevision_Tramo_Ingreso"] = valCampo(parseInt($("#sltPrevisionTramo").val()));
    //HOS_fecha_ingreso_realHospitalizacion
    HOS_Hospitalizacion["HOS_fecha_ingreso_realHospitalizacion"] = valCampo($("#txtFechaIngreso").val());
    //HOS_hora_ingreso_realHospitalizacion
    HOS_Hospitalizacion["HOS_hora_ingreso_realHospitalizacion"] = $("#txtHoraIngreso").val();

    //HOS_idOrigen_Ingreso
    //HOS_Hospitalizacion["HOS_idOrigen_Ingreso"] = 1;

    //HOS_Hospitalizacion["HOS_ingreso_confirmadoHospitalizacion"] = 'NO';
    //HOS_Hospitalizacion["HOS_egreso_confirmadoHospitalizacion"] = 'NO';
    //HOS_Hospitalizacion["HOS_estadoHospitalizacion"] = 'Activo';
    //HOS_Hospitalizacion["GEN_idAmbito"] = 1;

    //HOS_idProcedencias_Ingresoc
    HOS_Hospitalizacion["HOS_idProcedencias_Ingreso"] = valCampo(parseInt($("#sltProcedenciaIngreso").val()));
    HOS_Hospitalizacion["GEN_idEstablecimiento"] = valCampo(parseInt($("#sltEstablecimientoProcedenciaIngreso").val()));
    //GEN_idUbicacion_Ingreso
    HOS_Hospitalizacion["GEN_idUbicacion_Ingreso"] = valCampo(parseInt($("#sltUbicacionIngreso").val()));

    //GEN_idLey_Previsional_Ingreso
    HOS_Hospitalizacion["GEN_idLey_Previsional_Ingreso"] = valCampo(parseInt($("#sltLeyPrevisionalIngreso").val()));
    HOS_Hospitalizacion["GEN_idModalidad_Fonasa_Ingreso"] = valCampo(parseInt($("#sltModalidadFonasaIngreso").val()));

    //DATOS EGRESO
    if (valCampo($("#sltTipoEgreso").val()) != null)
        HOS_Hospitalizacion["HOS_idTipo_Egreso"] = parseInt(valCampo($("#sltTipoEgreso").val()));
    if (valCampo($("#sltDestinoAlta").val()) != null)
        HOS_Hospitalizacion["HOS_idDestino_Alta"] = parseInt(valCampo($("#sltDestinoAlta").val()));

    if (valCampo($("#sltLeyPrevisionalEgreso").val()) != null)
        HOS_Hospitalizacion["GEN_idLey_Previsional_Egreso"] = parseInt(valCampo($("#sltLeyPrevisionalEgreso").val()));
    else
        HOS_Hospitalizacion["GEN_idLey_Previsional_Egreso"] = null;

    if ($("#txtFechaEgreso").val() != "")
        HOS_Hospitalizacion["HOS_fecha_egreso_clinicoHospitalizacion"] = moment($("#txtFechaEgreso").val()).format("YYYY-MM-DD") + "T" + $("#txtHoraEgreso").val() + ":00";

    HOS_Hospitalizacion["GEN_idModalidad_Fonasa_Egreso"] = valCampo($("#sltModalidadFonasaEgreso").val());
    HOS_Hospitalizacion["HOS_idTipo_Hospitalizacion"] = valCampo($("#sltTipoHospitalizacion").val());
    if ($("#txtMotivoHospitalizacion").val() != "")
        HOS_Hospitalizacion["HOS_motivoHospitalizacion"] = $("#txtMotivoHospitalizacion").val();

    return HOS_Hospitalizacion;
}

function CancelarFormularioAdmision() {
    ShowModalAlerta('CONFIRMACION',
        'Salir del Formulario',
        '¿Está seguro que desea cancelar el registro?',
        quitarListener);
}

function avisoHospitalizaciones() {
    if (Array.isArray(pacienteTieneHospitalizaciones(null))) {
        hosFlag = 1;
    } else {
        hosFlag = 0;
    }
}