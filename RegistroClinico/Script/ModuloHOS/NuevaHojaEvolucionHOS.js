﻿
var sSession = null;
var idPro;
var hojaEvolucion = {};
var arrayFlujoExamen = [];
var plantilla = null;

$(document).ready(function () {
   IniciarComponentes();
});

var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
}

function IniciarComponentes() {

    sSession = getSession();
    $("textarea").val("");
    //RevisarAcceso(true, sSession);
    //ReiniciarRequired();

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
        async: false,
        success: function (data) {
            idPro = data[0].GEN_idProfesional;
        }
    });

    window.addEventListener('beforeunload', bunload, false);
    CargarComboUbicacion();
    //Solucionar no tiene idMenu PROBLEMA, ERROR
    var idMenu = sessionStorage.idMenu;
    CargarComboPlantilla(sessionStorage.idMenu);
    
    BloquearPaciente();
    CargarExamenes();
    CargarUsuario();
    
    $("#aIrFlujoExamen").click(function () {
        $('a[href="#nav-flujoExamenes"]').trigger("click");
    });

    $("#aIrDatosEvolucion").click(function () {
        $('a[href="#divEvolucion"]').trigger("click");
    });

    textboxio.replaceAll('#txtDetEvolucion',
        {
            paste: { style: 'clean' },
            images: { allowLocal: false }
        });

    $('#sltPlantilla').click(function () {
        $(this).attr("data-previous", $(this).val());
    });

    $('#sltPlantilla').change(function () {

        if ($.trim(textboxio.replace('#txtDetEvolucion').content.get()) != '') 
            ShowModalAlerta('CONFIRMACION', 'Cargar Plantilla',
                '¿Está seguro que desea cargar esta plantilla, se borrarán todos los cambios?',
                CargarPlantilla,
                CargarSeleccionPrevia);
        else
            CargarPlantilla();

    });

    $('#btnLimpiar').click(function () {
        $('#sltPlantilla').val('0');
        textboxio.replace("#txtDetEvolucion").content.set('');
    });

    document.getElementById('txtEvolucion').onpaste = function (e) {
        e.preventDefault();
    }

    ValidarNumeros();

    $("#btnEditarExamen").hide();

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "preventDuplicates": false,
        "onclick": null,
        "positionClass": "toast-bottom-full-width",
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 6000,
        "extendedTimeOut": 1000,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }

}

function CargarPlantilla() {

    if ($("#sltPlantilla").val() != '0') {
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Plantillas/" + $("#sltPlantilla").val(),
            async: false,
            success: function (data, status, jqXHR) {
                textboxio.replace("#txtDetEvolucion").content.set(data.RCE_detallePlantillas);
                plantilla = data.RCE_detallePlantillas;
            }
        });
    } else {
        textboxio.replace("#txtDetEvolucion").content.set('');
    }

    $('#modalAlerta').modal('hide');

}

function CargarSeleccionPrevia() {

    $("#sltPlantilla").val($("#sltPlantilla").attr('data-previous'));

}

function CargarComboUbicacion() {

    $("#sltUbicacion").empty();
    $("#sltUbicacion").append("<option value='0'>Seleccione</option>");

    try {
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "GEN_Ubicacion/Hospitalizacion/1",
            async: false,
            success: function (data, status, jqXHR) {

                $.each(data, function (key, val) {
                    $("#sltUbicacion").append("<option value='" + val.GEN_idUbicacion + "'>" +
                        val.GEN_nombreUbicacion + "</option>");
                });

            },
            error: function (jqXHR, status) {
                console.log("Error al llenar Ubicación: " + jqXHR.responseText);
            }
        });

    } catch (err) {
        alert(err.message);
    }
    
}

function CargarComboPlantilla(id) {
    console.log(id)
    $("#sltPlantilla").empty();
    $("#sltPlantilla").append("<option value='0'>Seleccione</option>");
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Plantillas/Combo/" + id,
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (key, val) {
                $("#sltPlantilla").append("<option value='" + val.RCE_idPlantillas + "'>" +
                    val.RCE_nombrePlantillas + "</option>");
            });

        }
    });

}

function CargarUsuario() {
    
    $("#txtFechaEvolucion").val(moment(sSession.FECHA_ACTUAL).format('YYYY-MM-DD'));
    
    if (sSession.ID_HOJAEVOLUCION != undefined) {
        CargarHojaEvolucion();
    } else {
        
        if (sSession.ID_EVENTO != undefined)
            CargarDiagnosticosAnteriores();

        if (sSession.ID_PACIENTE != undefined) {

            CargarPaciente(sSession.ID_PACIENTE);

            if (sSession.ID_EVENTO != undefined) {
                hojaEvolucion.RCE_idEventos = parseInt(sSession.ID_EVENTO);
                CargarFlujoExamenes();
            }

            //if (sSession.ID_PROFESIONAL != undefined)
            CargarProfesional(idPro);

        } else
            quitarListener();
        
    }

    if (sSession.ID_HOJAEVOLUCION != undefined)
        deleteSession('ID_HOJAEVOLUCION');

    if (sSession.ID_PACIENTE != undefined)
        deleteSession('ID_PACIENTE');
    
}

function CargarExamenes() {

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Tipo_Examen/combo",
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (i, item) {
                arrayFlujoExamen.push({
                    RCE_idHoja_Evolucion_Tipo_Examen: null,
                    RCE_idTipo_Examen: item.RCE_idTipo_Examen,
                    RCE_descripcionTipo_Examen: item.RCE_descripcionTipo_Examen,
                    RCE_valorHoja_Evolucion_Tipo_Examen: null
                });
            });
            
        }
    });

    ActualizarTablaExamenes();

}

function CargarDiagnosticos() {

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Tipo_Examen/combo",
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (i, item) {
                arrayFlujoExamen.push({
                    RCE_idHoja_Evolucion_Tipo_Examen: null,
                    RCE_idTipo_Examen: item.RCE_idTipo_Examen,
                    RCE_descripcionTipo_Examen: item.RCE_descripcionTipo_Examen,
                    RCE_valorHoja_Evolucion_Tipo_Examen: null
                });
            });

        }
    });

}

function CargarDiagnosticosAnteriores() {

    //Faltaba castear el "null"
    if (sSession.ID_EVENTO != undefined && JSON.parse(sSession.ID_EVENTO) != null) {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Hoja_Evolucion/Diagnostico/RCE_idEvento/" + sSession.ID_EVENTO,
            async: false,
            success: function (data, status, jqXHR) {

                var json = data;

                if (json.RCE_diagnostico_principalHoja_Evolucion !== null)
                    $("#txtDiagnosticoPrincipal").val(json.RCE_diagnostico_principalHoja_Evolucion);
                else if (json.RCE_diagnosticoIngreso_Medico !== null) 
                    $("#txtDiagnosticoPrincipal").val(json.RCE_diagnosticoIngreso_Medico);
                
            }, error: function (jqXHR, status) {
                console.log("ERROR Diagnósticos Anteriores: " + JSON.stringify(jqXHR));
            }
        });
    }
    
}

function CargarFlujoExamenes() {

    //Faltaba Checkear el NaN
    if (hojaEvolucion.RCE_idEventos != null && !(Number.isNaN(hojaEvolucion.RCE_idEventos))) {
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Hoja_Evolucion_Tipo_Examen/Evento/" + hojaEvolucion.RCE_idEventos,
            async: false,
            success: function (data, status, jqXHR) {

                var temp = null;

                $.each(data, function (i, item) {

                    if (temp !== item.RCE_fechaHoja_Evolucion)
                        $("#tblFlujoExamen thead tr").append("<th>" + moment(item.RCE_fechaHoja_Evolucion).format("DD-MM-YY") + "</th>");

                    if ($("#tblFlujoExamen tbody tr[data-id='" + item.RCE_descripcionTipo_Examen + "']").length == 0) {

                        var countHead = $("#tblFlujoExamen thead tr th").length;
                        var html = "";
                        html += "<tr data-id='" + item.RCE_descripcionTipo_Examen + "'>" +
                            "<td class='text-center'>" + item.RCE_descripcionTipo_Examen + "</td>";

                        // ES 2 PORQUE SALTA EL ENCABEZADO DE EXAMEN Y LA PRIMERA
                        for (var j = 2; j < countHead; j++)
                            html += "<td></td>";

                        html += "<td class='text-center'>" + item.RCE_valorHoja_Evolucion_Tipo_Examen + "</td>" +
                            "</tr>";

                        $("#tblFlujoExamen tbody").append(html);

                    } else {
                        $("#tblFlujoExamen tbody tr[data-id='" + item.RCE_descripcionTipo_Examen + "']").append(
                            "<td class='text-center'>" + item.RCE_valorHoja_Evolucion_Tipo_Examen + "</td>");
                    }


                    if ((i + 1) === data.length || data[i].RCE_fechaHoja_Evolucion !== data[i + 1].RCE_fechaHoja_Evolucion) {

                        $.each($("#tblFlujoExamen tbody tr"), function (i, item) {

                            if ($(this).children().length !== $("#tblFlujoExamen thead tr th").length)
                                $(this).append("<td></td>");

                        });

                    }

                    temp = item.RCE_fechaHoja_Evolucion;

                });

            }
        });
    }
}

function CargarProfesional(idProfesional) {
    
    try {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "GEN_Profesional/" + idProfesional,
            async: false,
            success: function (data, status, jqXHR) {
                
                hojaEvolucion.GEN_idProfesional = data.GEN_idProfesional;
                $("#txtNumeroDocumentoProfesional").val(data.GEN_rutProfesional +
                    (data.GEN_digitoProfesional !== null ? '-' + data.GEN_digitoProfesional : ''));
                $("#txtNombreProfesional").val(data.GEN_nombreProfesional);
                $("#txtApePatProfesional").val(data.GEN_apellidoProfesional);
                $("#txtApeMatProfesional").val(data.GEN_sapellidoProfesional);
                    
            },
            error: function (jqXHR, status) {
                console.log("Error al cargar Profesional: " + jqXHR.responseText);
            }
        });

    } catch (err) {
        alert(err.message);
    }

}

function BloquearHojaEvolucion(enabled) {

    if (enabled)
        $("#divHojaEvolucion select," +
            "#divHojaEvolucion input," +
            "#divHojaEvolucion textarea," +
            "#tblExamenes tbody tr td input").attr("disabled", "disabled");
    else
        $("#divHojaEvolucion select," +
            "#divHojaEvolucion input," +
            "#divHojaEvolucion textarea," +
            "#tblExamenes tbody tr td input").removeAttr("disabled");

}

function CargarHojaEvolucion() {
    
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Hoja_Evolucion/" + sSession.ID_HOJAEVOLUCION,
        async: false,
        success: function (data, status, jqXHR) {

            CargarJsonHojaEvolucionNuevo(data[0]);
            
            var fechaLimite =
                moment(hojaEvolucion.RCE_fecha_limite_edicionHoja_Evolucion, "YYYY-MM-DD HH:mm:ss").toDate();

            CargarTablaExamenes();

            if (fechaLimite < GetFechaActual()) {

                textboxio.replace("#txtDetEvolucion").events.loaded.addListener(function () {
                    var edDocument = textboxio.replace("#txtDetEvolucion").content.documentElement();
                    edDocument.body.setAttribute("contenteditable", "false");
                });

                BloquearHojaEvolucion(true);
                ShowModalAlerta("ADVERTENCIA", "Fecha límite sobrepasada", "La Hoja de Evolución ya no puede ser editada " +
                    "porque se sobrepasó el timpo límite de modificación.", null, null, '', 'Cerrar');
                $('#aGuardarHojaEvolucion').hide();

                
            } else
                BloquearHojaEvolucion(false);

            plantilla = data[0].RCE_detallePlantillas;

            $("#txtEvolucion").val($.trim(hojaEvolucion.RCE_evolucionHoja_Evolucion));
            $("#txtExamenFisico").val($.trim(hojaEvolucion.RCE_examen_FisicoHoja_Evolucion));
            $("#txtFechaEvolucion").val(moment(hojaEvolucion.RCE_fechaHoja_Evolucion).format('YYYY-MM-DD'));
            $("#sltPlantilla").val(hojaEvolucion.RCE_idPlantillas);
            $("#sltUbicacion").val(hojaEvolucion.GEN_idUbicacion);
            textboxio.replace("#txtDetEvolucion").content.set(hojaEvolucion.RCE_descripcionHoja_Evolucion);
            $("#strFechaCreacion").text(moment(hojaEvolucion.RCE_fecha_limite_edicionHoja_Evolucion).format('DD-MM-YYYY HH:mm:ss'));
            $("#txtDiagnosticoPrincipal").val(hojaEvolucion.RCE_diagnostico_principalHoja_Evolucion);
            $("#txtOtrosDiagnosticos").val(hojaEvolucion.RCE_otros_diagnosticosHoja_Evolucion);

            //if (sSession.ID_PACIENTE == undefined)
            CargarPaciente(hojaEvolucion.GEN_idPaciente);
            CargarProfesional(hojaEvolucion.GEN_idProfesional);
            cargarArchivosExistentes(hojaEvolucion.RCE_idHoja_Evolucion, 'HOS');
            CargarFlujoExamenes();

        },
        error: function (jqXHR, status) {
            console.log("Error al cargar Profesional: " + jqXHR.responseText);
        }
    });
    
}

function CargarJsonHojaEvolucionNuevo(json) {

    hojaEvolucion.GEN_idPaciente = json.GEN_idPaciente;
    hojaEvolucion.RCE_idHoja_Evolucion = json.RCE_idHoja_Evolucion;
    hojaEvolucion.GEN_idProfesional = json.GEN_idProfesional;
    hojaEvolucion.GEN_idUbicacion = json.GEN_idUbicacion;
    hojaEvolucion.RCE_fechaHoja_Evolucion = json.RCE_fechaHoja_Evolucion;
    hojaEvolucion.RCE_descripcionHoja_Evolucion = json.RCE_descripcionHoja_Evolucion;
    hojaEvolucion.RCE_evolucionHoja_Evolucion = json.RCE_evolucionHoja_Evolucion;
    hojaEvolucion.RCE_examen_FisicoHoja_Evolucion = json.RCE_examen_FisicoHoja_Evolucion;
    hojaEvolucion.RCE_estadoHoja_Evolucion = json.RCE_estadoHoja_Evolucion;
    hojaEvolucion.RCE_idPlantillas = json.RCE_idPlantillas;
    hojaEvolucion.RCE_idEventos = json.RCE_idEventos;
    hojaEvolucion.GEN_idTipo_Estados_Sistemas = json.GEN_idTipo_Estados_Sistemas;
    hojaEvolucion.RCE_fecha_limite_edicionHoja_Evolucion = json.RCE_fecha_limite_edicionHoja_Evolucion;
    hojaEvolucion.RCE_diagnostico_principalHoja_Evolucion = json.RCE_diagnostico_principalHoja_Evolucion;
    hojaEvolucion.RCE_otros_diagnosticosHoja_Evolucion = json.RCE_otros_diagnosticosHoja_Evolucion;
    
}

function CargarJsonHojaEvolucion() {
    
    hojaEvolucion.GEN_idUbicacion = parseInt($("#sltUbicacion").val());
    hojaEvolucion.RCE_fechaHoja_Evolucion = $("#txtFechaEvolucion").val();
    hojaEvolucion.RCE_descripcionHoja_Evolucion = $.trim(textboxio.replace('#txtDetEvolucion').content.get());
    hojaEvolucion.RCE_evolucionHoja_Evolucion = $.trim($("#txtEvolucion").val());
    hojaEvolucion.RCE_examen_FisicoHoja_Evolucion = $.trim($("#txtExamenFisico").val());
    hojaEvolucion.RCE_estadoHoja_Evolucion = "Activo";
    hojaEvolucion.RCE_idPlantillas = parseInt($("#sltPlantilla").val());
    hojaEvolucion.RCE_diagnostico_principalHoja_Evolucion = $.trim($("#txtDiagnosticoPrincipal").val());
    hojaEvolucion.RCE_otros_diagnosticosHoja_Evolucion = valCampo($("#txtOtrosDiagnosticos").val());

    if  (hojaEvolucion.RCE_idHoja_Evolucion == undefined) {

        // 81 = Hoja de Evolución Validada
        // 82 = Hoja de Evolución no Validada
        //hojaEvolucion.RCE_idEventos = GetEvento().RCE_idEventos;
        //hojaEvolucion.RCE_idEventos = 10730; //1;
        hojaEvolucion.GEN_idTipo_Estados_Sistemas = (sSession.CODIGO_PERFIL == 1) ? 81 : 82;
        hojaEvolucion.RCE_fecha_limite_edicionHoja_Evolucion = AgregarHoras(24);

    }
    
}

function CargarTablaExamenes() {
    
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Hoja_Evolucion_tipo_Examen/RCE_Hoja_Evolucion/" + hojaEvolucion.RCE_idHoja_Evolucion,
        async: false,
        success: function (data, status, jqXHR) {

            var array = data;

            $.each(array, function (iArray, vArray) {

                var bHayFlujo = false;

                $.each(arrayFlujoExamen, function (iArrayFlujo, vArrayFlujo) {
                    
                    if (vArray.RCE_idTipo_Examen == vArrayFlujo.RCE_idTipo_Examen) {
                        arrayFlujoExamen[iArrayFlujo].RCE_idHoja_Evolucion_Tipo_Examen = vArray.RCE_idHoja_Evolucion_Tipo_Examen;
                        arrayFlujoExamen[iArrayFlujo].RCE_valorHoja_Evolucion_Tipo_Examen = vArray.RCE_valorHoja_Evolucion_Tipo_Examen;
                        bHayFlujo = true;
                        return false;
                    }

                });

                if (!bHayFlujo)
                    
                    arrayFlujoExamen.push({
                        RCE_idHoja_Evolucion_Tipo_Examen: vArray.RCE_idHoja_Evolucion_Tipo_Examen,
                        RCE_idTipo_Examen: vArray.RCE_idTipo_Examen,
                        RCE_descripcionTipo_Examen: vArray.RCE_descripcionTipo_Examen,
                        RCE_valorHoja_Evolucion_Tipo_Examen: vArray.RCE_valorHoja_Evolucion_Tipo_Examen
                    });

            });
            
        },
        error: function (jqXHR, status) {
            console.log("Error al cargar Flujo Exámenes: " + jqXHR.responseText);
        }
    });
    
    ActualizarTablaExamenes();
        
}

function ActualizarTablaExamenes() {

    $("#tblExamenes tbody").empty('');

    $.each(arrayFlujoExamen, function (index, item) {

        var $tr = $('<tr>').append(
            $('<td class="center-horizontal-vertical">').html(item.RCE_descripcionTipo_Examen),
            $('<td style="padding: 0px;">').html("<input id='txtValorExamen_" + index + "' type='text' "
                + "class='form-control text-center number' data-index='" + index + "' maxlength='10' />")
        );

        $("#tblExamenes tbody").append($tr);
        $("#txtValorExamen_" + index).val(item.RCE_valorHoja_Evolucion_Tipo_Examen);
        
    });
}

function ReiniciarExamen() {
    $("#sltExamenes").val(0).selectpicker("refresh");
    $("button[data-id='sltExamenes']").html("Seleccione");
    $("#txtValorExamen").val('');
    $("#btnEditarExamen").hide();
    $("#btnAgregarExamen").show();
}

function GuardarFlujoExamen() {

    var method = "";
    var url = "";

    if (arrayFlujoExamen.length > 0) {

        $.each(arrayFlujoExamen, function (index, item) {
            var valorHE = ($("#txtValorExamen_" + index).val() != '') ? parseFloat($("#txtValorExamen_" + index).val()) : null;

            var json = {
                RCE_idHoja_Evolucion: hojaEvolucion.RCE_idHoja_Evolucion,
                RCE_idTipo_Examen: parseInt(item.RCE_idTipo_Examen),
                RCE_valorHoja_Evolucion_Tipo_Examen: item.RCE_valorHoja_Evolucion_Tipo_Examen,
                RCE_estadoHoja_Evolucion_tipo_Examen: 'Activo'
            }

            if (item.RCE_idHoja_Evolucion_Tipo_Examen != undefined) {
                json.RCE_idHoja_Evolucion_Tipo_Examen = item.RCE_idHoja_Evolucion_Tipo_Examen;
                json.RCE_estadoHoja_Evolucion_tipo_Examen = (valorHE === null) ? 'Inactivo' : 'Activo';
                method = 'PUT';
                url = GetWebApiUrl() + "RCE_Hoja_Evolucion_tipo_Examen/" + item.RCE_idHoja_Evolucion_Tipo_Examen;

                if (valorHE !== null)
                    json.RCE_valorHoja_Evolucion_Tipo_Examen = valorHE;

                GuardarJsonFlujoExamenes(method, url, json);
            } else {
                url = GetWebApiUrl() + "RCE_Hoja_Evolucion_tipo_Examen";
                method = 'POST';
                json.RCE_valorHoja_Evolucion_tipo_Examen = valorHE;

                if (valorHE !== null)
                    GuardarJsonFlujoExamenes(method, url, json);
            }
        });

    } else
        toastr.success('Hoja de Evolución creada Exitosamente.');
    
    SubirTodosArchivos(hojaEvolucion.RCE_idHoja_Evolucion, 'HOS', SalirHojaEvolucion);
    
}

function GuardarJsonFlujoExamenes(method, url, json) {
    
    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            
            $('#aGuardarHojaEvolucion').addClass('disabled');
            $('#aCancelarHojaEvolucion').addClass('disabled');

            SubirTodosArchivos(hojaEvolucion.RCE_idHoja_Evolucion, 'HOS', SalirHojaEvolucion);
            
        },
        error: function (jqXHR, status) {
            console.log("Error al guardar Flujo de Examenes: " + jqXHR.message);
            alert("Error al guardar Flujo de Examenes: " + status.code);
        }
    });

}

function GuardarHojaEvolucion() {

    CargarJsonHojaEvolucion();
    var method = (hojaEvolucion.RCE_idHoja_Evolucion != undefined) ? 'PUT' : 'POST';
    var url = GetWebApiUrl() + "RCE_Hoja_Evolucion" + ((method === 'PUT') ? '/' + hojaEvolucion.RCE_idHoja_Evolucion : '');

    console.log(JSON.stringify(hojaEvolucion));

    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(hojaEvolucion),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {

            hojaEvolucion.RCE_idHoja_Evolucion = (hojaEvolucion.RCE_idHoja_Evolucion == undefined) ?
                data.RCE_idHoja_Evolucion : hojaEvolucion.RCE_idHoja_Evolucion;
            
            GuardarFlujoExamen();

        },
        error: function (jqXHR, status) {
            console.log("Error al guardar hoja de evolución: " + jqXHR.message);
            alert("Error al guardar hoja de evolución: " + status.code);
        }
    });
    
}


async function GuardarGeneral() {
    
    $("a[href='#nav-evolucion']").tab('show');
    await sleep(250);

    if (validarCampos("#divHojaEvolucion", true)) {
        if (textboxio.replace("#txtDetEvolucion").content.get().length > 2147483647) {
            $("#ephox_" + $("#txtDetEvolucion").attr("id")).addClass("textboxio-invalid");
            toastr.error('El Detalle de Evolución sobrepasó la cantidad máxima de caracteres permitidos.');
        } else if (hojaEvolucion.RCE_fecha_limite_edicionHoja_Evolucion != undefined &&
            hojaEvolucion.RCE_fecha_limite_edicionHoja_Evolucion < GetFechaActual()) {
            ShowModalAlerta("ADVERTENCIA",
                "Tiempo límite sobrepasado",
                "La Hoja de Evolución ya no puede ser editada " +
                "porque se sobrepaso el tiempo límite de modificación.", null);
        } else if (!ValidarPlantilla()) {
            $("#ephox_" + $("#txtDetEvolucion").attr("id")).addClass("textboxio-invalid");
            toastr.error("La plantilla seleccionada está incompleta, " +
                "presione en \"Actualizar Plantilla\" para quitar el error");
            $('html, body').animate({ scrollTop: $("#ephox_" + $("#txtDetEvolucion").attr("id")).offset().top - 40 }, 500);
        } else if (!ValidarTextoItemPlantilla()) {
            $("#ephox_" + $("#txtDetEvolucion").attr("id")).addClass("textboxio-invalid");
            toastr.error("Se ha dejado vacío uno del los puntos en la plantilla.");
            $('html, body').animate({ scrollTop: $("#ephox_" + $("#txtDetEvolucion").attr("id")).offset().top - 40 }, 500);
        } else {

            //if (hojaEvolucion.RCE_idEventos != undefined /*|| GetEvento().RCE_idEventos != undefined*/) {
            GuardarHojaEvolucion();
            //} else {
            //    ShowModalAlerta("ADVERTENCIA",
            //        "No existe evento asociado",
            //        "La hoja de Evolución debe tener un evento asociado. ", null);
            //    ShowPacienteEventos(GetPaciente().GEN_idPaciente);
            //}

        }
    }

}

function ValidarPlantilla() {
    
    var array = plantilla.split("<br />");
    array.pop();

    var detalleEvolucion = HtmlAString(textboxio.replace('#txtDetEvolucion').content.get());
    var bValido = true;

    $.each(array, function (index, item) {
        if (!detalleEvolucion.includes($.trim(HtmlAString(item)))) {
            bValido = false;
            return false;
        }

    });

    return bValido;

}

function ValidarTextoItemPlantilla() {

    var array = plantilla.split("<br />");
    array.pop();

    var detalleEvolucion = $.trim(HtmlAString(textboxio.replace('#txtDetEvolucion').content.get())).replace(/\r?\n|\r/g,"");
    var bValido = false;
    
    $.each(array, function (index, item) {

        $.each(array, function (jndex, jtem) {

            var indexOf = detalleEvolucion.indexOf($.trim(HtmlAString(item))) + $.trim(HtmlAString(item)).length;
            var cont = 0;
            var str = $.trim(detalleEvolucion.substring(indexOf, detalleEvolucion.length))
            bValido = false;

            //alert("Array Charts: " + $.trim(HtmlAString(jtem)).split('').join(","));
            //alert("SubString: " + str);

            $.each($.trim(HtmlAString(jtem)).split(''), function (kndex, ktem) {
                
                if (ktem != str.charAt(cont)) {
                    bValido = true;
                    return false;
                } else {
                    cont++;
                    if (str.length == cont)
                        return false;
                }
                    
            });

            if (!bValido)
                return false;
        });

        if (!bValido)
            return false;
    });

    return bValido;
}

function CancelarHojaEvolucion() {
    ShowModalAlerta('CONFIRMACION',
        'Salir de la hoja de evolución',
        '¿Está seguro que desea cancelar el registro?',
        quitarListener);
}

function SalirHojaEvolucion() {
    window.removeEventListener('beforeunload', bunload, false);
    location.href = document.referrer;
}
