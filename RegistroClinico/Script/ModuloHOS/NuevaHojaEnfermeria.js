﻿//const sign = require("core-js/fn/math/sign");

//const { forEach } = require("core-js/core/array");

//const { forEach } = require("core-js/es6/array");

//const { bottom } = require("@popperjs/core");

//const { error } = require("jquery");
let signosVitalesIngresados = [], arrayCarteraArancel = [], arrayExamenesLaboratorio = [], EgresosHidricosIngresados = []
const actividades = {}
let pesosIngresados = [], tallasIngresadas = [], arregloVigilancias = [], vigilanciasAntiguas = [], IngresosHidricosIngresados=[]
//Arrays para API
let tallasApi = [], pesosApi = [], vigilancias = [], SignosVitalesApi = [], valoracionesApi = [], hidricosApi =[]
let ExamenesLabApi = [], planCuidadosApi = [], vigilanciasApi = [], riesgosApi = [],  RespuestasApi = [], planCuidados = []
const sesion = getSession()
let fechaActual = GetFechaActual()

let cantidadHorarios = 0

$(document).ready(function () {

    //no se declara en la funcion con los demas combos porque se llena 3 veces
    comboHidricos()
    
    let fechaMaxima = moment(fechaActual).format("YYYY-MM-DD");
    $("#tomaFechaPeso").prop('max', fechaMaxima)
    $("#tomaFechaAltura").prop('max', fechaMaxima)

    if (sesion.ID_HOSPITALIZACION != undefined) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/${sesion.ID_HOSPITALIZACION}`,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                
                const {
                    NombreCamaActual,
                    Enfermeria: { AntecedentesMorbidos, Diagnostico, },
                    Ingreso: { Fecha },
                    TipoInvasivo: TipoInvasivo,
                    Examenes: Examenes
                } = data[0]

                let fechaAux = moment(GetFechaActual()).format("YYYY-MM-DD")
                let fechaActual = moment(fechaAux);
                let diasEstadiaHospitalizacion = fechaActual.diff(moment(Fecha).format("YYYY-MM-DD"), 'days')

                $("#fechaHosp").html(imprimeFechaHora(data))
                $("#diasEstadia").html(diasEstadiaHospitalizacion)
                $("#camaHosp").html(NombreCamaActual)
                $("#txtDiagnosticoHojaEnfermeria").val(Diagnostico)
                $("#txtMorbidosHojaEnfermeria").val(AntecedentesMorbidos)

                buscarPacientePorId(data[0].Paciente.IdPaciente)
                inicializarCombo()
                BloquearPaciente()
                ocultaCombosExtremidadPlano()
                agregarActividadesPlanCuidado()
                llenarTablaPlanDeCuidados()
                //Examenes ingresados anteriormente
                dibujarHistorialExamenesLaboratorio(Examenes)
                vigilanciasAntiguas = TipoInvasivo
                dibujarHistorialInvasivos(TipoInvasivo)
                //Llenar la hoja aca

                $("#divPlanCuidadoEnfermeria").hide()
                $("#tblPlanCuidadoEnfermeria").hide()
                $("#alertPlanCuidadoEnfermeria").show()


                $("#modalCargando").hide()
            },
            error: function (err) {
                console.log(JSON.stringify(err))
                $("#modalCargando").hide()
            }
        });
        //printTableExamen();

        getIntervencionesPaciente(sesion.ID_HOSPITALIZACION)
        getProcedimientosProgramados(sesion.ID_HOSPITALIZACION)

        $("#navtipoHoja1").hide()
        $("#mdlHorariosPlanCuidadoEnfermeria").modal("hide")

        $("#mostrarModalHorarios").click(function () {

            $("#mdlHorariosPlanCuidadoEnfermeria").modal("show")
        })


        $('#sltTipoHojaEnfermeria').change(function () {

            let value = $(this).val()
            let text = $(this).find('option:selected').text()

            $("#divPlanCuidadoEnfermeria").show()
            $("#alertPlanCuidadoEnfermeria").hide()

            let HojaObj = [
                {
                    id: 1,
                    nombre: 'Registro Clínico Enfermería',
                    ocultar: false
                },
                {
                    id: 2,
                    nombre: 'Registro Clínico Enfermería UPC Pediatría',
                    ocultar: false
                },
                {
                    id: 3,
                    nombre: 'Registro Clínico Enfermería Cuidados Básicos Pediatría',
                    ocultar: false
                },
                {
                    id: 4,
                    nombre: 'Hoja de Registros Clínicos de Matronería',
                    ocultar: false
                },
                {
                    id: 5,
                    nombre: 'Hoja de Registros Clínicos de Matronería UPC Neonatología',
                    ocultar: false
                },
                {
                    id: 6,
                    nombre: 'Hoja Diaria de Matronería Neonatal',
                    ocultar: false
                },
                {
                    id: 7,
                    nombre: 'Registro Clínico Enfermería UPC Adulto',
                    ocultar: false
                }
            ]

            const extraido = extraerParaOcultar(HojaObj, value)
            ocultar(extraido, value, text)
        })

    }

    const extraerParaOcultar = (obj, num) => {
        n = parseInt(num)
        const aux = obj.filter(x => x.id !== n)
        result = aux.map(x => {
            x.ocultar = true
            return x
        })
        return result
    }

    const ocultar = (obj, num, string) => {

        n = parseInt(num)
        tag = '#navtipoHoja'
        tag2 = '#titleTipoHojaEnfermeria'

        if (n === 0) {

            $("#navtipoHoja1").hide()
            $("#navtipoHoja2").hide()
            $("#navtipoHoja3").hide()
            $("#navtipoHoja4").hide()
            $("#navtipoHoja5").hide()
            $("#navtipoHoja6").hide()
            $("#navtipoHoja7").hide()

        }

        obj.forEach((x, i) => {
            if (x.ocultar == true)
                id = `${tag}${x.id}`
            $(id).hide()
        })

        id = `${tag}${n}`
        id2 = `${tag2}${n}`

        $(id).show()
        $(id).text(string)
        $(id2).text(string)
    }

    $("#aIngresosVitales").on("click", function () {
        mostrarSignosVitales(this)
    })
    $("#aGuardarSignoVital").on("click", function () {
        let input = $("#divSignosVitales").find("input");
        //No han sido ingresado datos 

        for (let i = 0; i < input.length; i++) {
            if (input[i].value != "") {
                signosVitalesIngresados.push({ Id: input[i].name, Nombre: input[i].placeholder, Valor: input[i].value })
            }
        }

        //Dibujar signos vitales
        dibujarSignosVitales();
        $("#mdlSignosVitales").modal("hide");
    });

    arrayCarteraArancel = GetJsonCartera();
    if (sesion.ACCION != undefined) {

        if (sesion.ACCION === "EDITAR") {

            mostrarFechaHojaEnfermeria(sesion.ID_HOJA_ENFERMERIA)
            mostrarBrazalete(sesion.ID_HOJA_ENFERMERIA)
            inicializarCombo()

            llenarHojaEnfermeria()

            $("#txtDiagnosticoHojaEnfermeria").removeAttr("disabled")
            $("#txtMorbidosHojaEnfermeria").removeAttr("disabled")
            mostrarIdHOspitalizacion(sesion.ID_HOJA_ENFERMERIA, sesion.ID_HOSPITALIZACION)
        }
        if (sesion.ACCION === "INGRESO") {
            mostrarFechaHojaEnfermeria()
            //attr ? 
            if ($("#txtDiagnosticoHojaEnfermeria").val() != "" || $("#txtDiagnosticoHojaEnfermeria").val() != "<empty string>") {
                $("#txtDiagnosticoHojaEnfermeria").removeAttr("disabled")

            }

            if ($("#txtMorbidosHojaEnfermeria").val() != "" || $("#txtDiagnosticoHojaEnfermeria").val() != "<empty string>") {
                $("#txtMorbidosHojaEnfermeria").removeAttr("disabled")
            }

            mostrarIdHOspitalizacion(undefined, sesion.ID_HOSPITALIZACION)
        }

        $("#btnIngresoNuevaHojaEnfermeria").click(function (e) {

            IngresoEdicionHojaEnfermeria(sesion.ACCION)
            e.preventDefault()
        })
    }
    else {
        window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`
    }
    //Inicializar typeahead examenes laboratorio
    let element = "#thExamenesLaboratorio";
    let emptytempl = "No existe el examen especificado ({{query}})";
    let templ = `<span>
                    <span class="{{IdCarteraServicio}}">{{DescripcionCarteraServicio}}</span>
                </span>`;
    let source = "GEN_cartera";

    $("#thExamenesLaboratorio").typeahead(setTypeAhead(arrayCarteraArancel, element, emptytempl, templ, source));
    $("#thExamenesLaboratorio").change(() => $("#thExamenesLaboratorio").removeData("id"));
    $("#thExamenesLaboratorio").blur(() => {
        if ($("#thExamenesLaboratorio").data("id") == undefined)
            $("#thExamenesLaboratorio").val("");
    });
    $("#addElementTableExam").on('click', function () {
        if (validarCampos("#divExamLab", false)) {
            $("#thExamenesLaboratorio").data("text-servicio", $("#thExamenesLaboratorio").val())
            let idcartera = $("#thExamenesLaboratorio").data("id");
            $("#thExamenesLaboratorio").data("id-tipo-cartera", 61)
            $("#thExamenesLaboratorio").data("id-cartera", idcartera);
            $("#thExamenesLaboratorio").data("id-tipo-cartera", 61)
            llenarTableExamen($("#thExamenesLaboratorio"));
        }
    })
    //Inicializar vigilancia
    vigilancia()
    tableHidricos()
})


//---esta funcion dibuja los pill y las preguntas para hacer una carga de evaluacion de riesgos--//
function printRiesgosSeguridad(datos) {
    $("#riesgos-pills-tab").empty()
    $("#riesgos-pills-tabContent").empty()

    let titulo = ""
    let content = "";
    let contenido = "";
    let cont = "";
    datos.map(a => {
        titulo += `<div class="col-lg-12"> <h3> ${a.Item.DescripcionItem} </h3> </div>`;
        a.Item.SubItem.map((e, index) => {
            contenido += `<a class="nav-link ${index == 0 ? "active" : ""}" 
                            id="v-pills-${e.IdItem}-tab" data-toggle="pill"
                            href="#riesgos-pills-${e.IdItem}" role="tab"
                            aria-controls="v-pills-${e.IdItem}"
                            aria-selected="${index == 0 ? "true" : "false"}">${e.DescripcionItem}</a>`

            cont += ` <div class="tab-pane fade ${index == 0 ? "show active" : ""}"
                        id="riesgos-pills-${e.IdItem}" role="tabpanel"
                        aria-labelledby="v-pills-${e.IdItem}-tab">
                        <div class="row" id="container-${e.IdItem}" >

                        </div>
                    </div>`;
        })
    })
    $("#riesgos-pills-tab").append(contenido)
    $("#riesgos-pills-tabContent").append(cont)
    datos.map(a => {
        for (let item of a.Item.SubItem) {
            let tipoInput;
            content=""
            item.Preguntas.map((e, indicex) => {
                
                //Preguntas con multiples respuestas

                e.MultipleSeleccion ? tipoInput = "checkbox" : tipoInput = "radio"

                content += `<div class="col-md-2 col-lg-3">
                                <div class="card">
                                    <div class="card-header bg-info">
                                        <h5 class="card-title">${e.DescripcionPregunta}</h5>
                                    </div>
                                    <div class="card-body">
                                        <span>Seleccione:</span> <br>
                                        <div class="form-check" id="resp-${item.idItem}-${indicex}">`
                e.Alternativas.map((x) => {
                    content += `
                                            <input id="riesgo-${x.IdRespuesta}" class="form-check-input" value="${x.IdRespuesta}"
                                            type="${tipoInput}" name="${item.IdItem}-${e.DescripcionPregunta}" data-idgrupo="${item.IdItem}" data-required="true"
                                            >
                                              <label class="form-check-label" for="${x.DescripcionAlternativa}">
                                                ${x.DescripcionAlternativa}
                                              </label>
                                            <br>`
                })
                content += `            </div>
                                    </div>
                                </div>
                            </div>`
            })
            $("#container-" + item.IdItem).append(content)
        }
    })
}
//funcion que guarda los riesgos o las valoraciones en arrays


function guardarRiesgosValoracion(id) {
    let comprobar = true
    if (id == 1) {
        if (validarCampos("#divRiesgos")) {
            //Obtener todos los radio y los checkbos checked
            let inputRadio = $("#divRiesgos").find("input[type='radio']:checked");
            let inputCheck = $("#divRiesgos").find("input[type='checkbox']:checked");
            //Union de todos los inputs checked
            let Respuestas = []
            for (let item of inputRadio) {
                Respuestas.push(parseInt(item.value))
            }
            for (let item of inputCheck) {
                Respuestas.push(parseInt(item.value))
            }
            //Evaluación de riesgos = IdItem = 1
            riesgosApi.push({ "IdItem": 1, "Respuestas": Respuestas })
        } else {
            comprobar = false
        }
    } else if (id == 10){
        if (validarCampos("#divValoracion")) {
            //Obtener todos los radio checked
            let inputRadio = $("#divValoracion").find("input[type='radio']:checked");
            let inputCheck = $("#divValoracion").find("input[type='checkbox']:checked");
            //let input = inputRadio.concat(inputCheck)
            let Respuestas = []
            for (let item of inputRadio) {
                Respuestas.push(parseInt(item.value))
            }
            for (let item of inputCheck) {
                Respuestas.push(parseInt(item.value))
            }
            //Valoraciones  = IdItem = 10
            valoracionesApi.push({ "IdItem": 10, "Respuestas": Respuestas })
        } else {
            comprobar= false
        }
    }
    return comprobar
}
//Dibuja los inputs necesarios para hacer un ingreeso de valoracion del paciente
function printValoracion(data) {
    let contenido="", alternativas ="", tipoInput
    data.map((e) => {
        e.Item.Preguntas.map(a => {
            a.MultipleSeleccion ? tipoInput = "checkbox" : tipoInput = "radio"
            alternativas = "";
            if (a.Alternativas.length > 0) {
                a.Alternativas.map(x => {
                    alternativas += `
                            <input class="form-check-input" value="${x.IdRespuesta}"
                            type="${tipoInput}" name="${a.IdPregunta}" data-idgrupo="${a.IdPregunta}" data-required="true">
                            <label class="form-check-label" for="${x.DescripcionAlternativa}">
                            ${x.DescripcionAlternativa}
                            </label></br>`
                })
            }
            contenido += `
             <div class="col-md-3">
                <div class="card ">
                    <div class="card-header bg-info">
                        <h5>${a.DescripcionPregunta}</h5>
                    </div>
                    <div class="card-body" id="">
                        <div class="form-check" id="body-${a.IdPregunta}" style="min-height:130px;">
                               ${alternativas} 
                        </div>
                    </div>
                </div>
            </div>
             `

        })
        $("#divValoracion").append(contenido)
    })
}
//Busca las preguntas/respuestas de riesgos o valoraciones para dibujar el cuestionario (IdHojaEnfermeriaRespuesta)
function getRiesgosSeguridad(id) {
    //id = 1 es riesgo ... id = 10 valoracion
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria_Item/1/${id}/Item`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            if (id == 1) {
                if ($("#aRiesgos").hasClass("d-none")) {
                    printRiesgosSeguridad(data)
                    $("#addRiesgos").data("clicked", true)
                    $("#addRiesgos").addClass("d-none")
                    $("#aRiesgos").removeClass("d-none")
                } else {
                    $("#addRiesgos").data("clicked", false)
                    $("#riesgos-pills-tab").empty()
                    $("#riesgos-pills-tabContent").empty()
                    $("#addRiesgos").removeClass("d-none")
                    $("#aRiesgos").addClass("d-none")
                }
            } else{
                if ($("#aValoracion").hasClass("d-none")) {
                    printValoracion(data)
                    $("#addValoracion").data("clicked", true)
                    $("#addValoracion").addClass("d-none")
                    $("#aValoracion").removeClass("d-none")
                } else {
                    $("#addValoracion").data("clicked", false)
                    //Vaciar pill tab content
                    $("#divValoracion").empty()
                    $("#addValoracion").removeClass("d-none")
                    $("#aValoracion").addClass("d-none")
                }
            }

        }, error: function (err) {
            console.log("Error al buscar los Riesgos y medidas de seguridad" + err)
        }
    });
}
//llenar la tabla de los examenes seleccionados
function llenarTableExamen(element) {
    if (element) {
        let idCarteraServicio = $(element).data("idCartera");
        let nombreExamen = $(element).data("textServicio");
        //118 examen solictado por se acaba de ingresar... es su estado inicial
        let estado = 118;
        //comprobar que no este agregado el examen
        let coincide = false;

        arrayExamenesLaboratorio.find((e) => { if (e.DescripcionExamen == nombreExamen) coincide = true });
        if (!coincide) {
            let FechaActual = GetFechaActual()
            arrayExamenesLaboratorio.push({
                IdHojaEnfermeriaExamen: null,
                IdServicioCartera: idCarteraServicio,
                FechaSolicitud: FechaActual,
                DescripcionExamen: nombreExamen,
                IdEstado: estado
            });
            toastr.success("Elemento agregado al listado")
            printTableExamen();
        } else
            toastr.warning("Este elemento ya ha sido agregado")
    }
}
//Agregando tallas o pesos a la hoja de enfermeria (Evoluciones)
function AgregarPesoTalla(elemento) {
    let fechaActual = moment(GetFechaActual()).format('YYYY-MM-DD')
    let Valor, Medida, Hora, tipoMedida, MedidaTexto, Fecha;
    tipoMedida = $(elemento).data("tipo");
    //Ingresando altura
    if (tipoMedida == "altura") {
        Valor = $("#txtTallaPacienteHojaEnfermeria").val();
        MedidaTexto = $("#sltTipoAltura option:selected").text();
        Fecha = moment($("#tomaFechaAltura").val());
        Medida = $("#sltTipoAltura").val();
        Hora = $("#tomaHoraAltura").val();
    } else {
        //Ingresando peso
        Valor = $("#txtPesoPaciente").val();
        MedidaTexto = $("#sltTipoPeso  option:selected").text();
        Fecha = moment($("#tomaFechaPeso").val());
        Medida = $("#sltTipoPeso").val();
        Hora = $("#tomaHoraPeso").val();
    }
    //Validar campos
    if (Valor == "" || Medida == 0 || Medida == "0" || Hora == "" || !Fecha.isValid()) {
        toastr.error("Información incompleta")
    } else {
        if (Fecha.diff(fechaActual, 'days') > 0) {
            toastr.error("La fecha no puede superar la fecha actual")
        } else {
            Fecha = Fecha.format("YYYY-MM-DD") + " " + Hora;
            toastr.success("Ingresado")
            tipoMedida == "altura" ? tallasIngresadas.push({ Valor: Valor, Medida: Medida, MedidaTexto: MedidaTexto, Fecha: Fecha }) : pesosIngresados.push({ Valor: Valor, Medida: Medida, MedidaTexto: MedidaTexto, Fecha: Fecha })
        }
    }
    printListPesoTalla(tipoMedida)
}
//Dibuja un listado de las tallas o los pesos
function printListPesoTalla(tipoMedida) {
    let lista, content = "";
    if (tipoMedida == "altura") {
        lista = "#listAltura";
        $(lista).empty();
        tallasIngresadas.map((e, index) => {
            content += `<tr>
                           <td> ${e.Valor} </td>
                           <td> ${e.MedidaTexto} </td>
                           <td> ${e.Fecha}</td>
                           <td><a class="btn btn-danger" onclick="eliminarPesoTalla('altura',${index})"><i class="fa fa-trash"></i></a></td>
                        </tr>`;
        })
    } else {
        lista = "#listPeso";
        $(lista).empty();
        pesosIngresados.map((e, index) => {
            content += `<tr>
                           <td> ${e.Valor} </td>
                           <td> ${e.MedidaTexto} </td>
                           <td> ${e.Fecha}</td>
                           <td><a class="btn btn-danger" onclick="eliminarPesoTalla('peso',${index})"><i  class="fa fa-trash"></i></a></td>
                        </tr>`;
        })
    }
    $(lista).append(content);
}
function eliminarPesoTalla(tipoMedida, indice) {
    if (tipoMedida == "altura") {
        tallasIngresadas.splice(indice, 1);
    } else {
        pesosIngresados.splice(indice, 1);
    }
    printListPesoTalla(tipoMedida);
}

//Esta funcion deberia optimizarse tambien-->Obtiene los examenes de laboratorio
function GetJsonCartera() {

    let array = [];
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Cartera_Servicio/${61}/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            array = data;
        }
    });

    return array;
}
function quitarExamLab(element) {
    let estado = $(element).data("estado");
    //Examen ya realizado
    if (estado != 118) {
        toastr.warning("No se puede eliminar un examen ya realizado")
    } else {
        ShowModalAlerta('CONFIRMACION',
            'Quitar Caso',
            '¿Está seguro que quiere quitar el caso?',
            function () {
                deleteTableExamLab(element);
                $('#modalAlerta').modal('hide');
            });
    }
}
//EliminarElemetos de la tabla examenes
function deleteTableExamLab(element) {
    let idExamen = $(element).data("id-cartera");
    arrayExamenesLaboratorio.find((a, index) => {
        if (a.IdServicioCartera == idExamen)
            indice = index;
    })
    //Eliminar elementos
    arrayExamenesLaboratorio.splice(indice, 1);
    toastr.warning("Elemento eliminado")
    //Dibuja tabla
    printTableExamen();
}
function printModalExamen(element) {

    $("#TitleNombreExamen").text("");
    let idCartera;
    if (element) {
        idCartera = $(element).data("id-cartera");
        let seleccionado = arrayExamenesLaboratorio.find(e => e.IdServicioCartera == idCartera)
        if (seleccionado) {
            $("#TitleNombreExamen").text("Modificando: " + seleccionado.DescripcionExamen)
            $("#btnCambioEstado").data("id-cartera", seleccionado.IdServicioCartera)
            $("#mdlCambioEstado").modal("show");
        }
    }

}
function modificarEstadoExamen(element) {
    let idCartera = $(element).data("id-cartera");
    let nuevoEstado = $("#sltEstadosExamen").val();
    arrayExamenesLaboratorio.find((e) => {
        if (e.IdServicioCartera == idCartera) {
            if (e.IdEstado != 118 && parseInt(nuevoEstado) == 118) {
                toastr.warning("No se puede revertir el estado del examen")
            } else {
                if (e.IdEstado == parseInt(nuevoEstado)) {
                    toastr.warning("El elemento ya tiene este estado")
                } else {
                    e.IdEstado = parseInt(nuevoEstado)
                    toastr.success("Estado cambiado")
                }
                e.IdEstado = parseInt(nuevoEstado)
            }
        }
    })
    $("#mdlCambioEstado").modal("hide");
    printTableExamen()
}
//Imprime la tabla exámenes agregados recientemente
function printTableExamen() {
    $("#tbodyExamLab").empty();
    let content, clase, title, filaEstado;
    if (arrayExamenesLaboratorio.length > 0) {
        arrayExamenesLaboratorio.map((e) => {
            switch (e.IdEstado) {
                case 118:
                    //Examen solicitado 
                    title = "Examen solicitado";
                    clase = "fa fa-clock fa-lg btn btn-outline-secondary"
                    break
                case 115:
                    //Examen tomado
                    title = "Examen tomado";
                    clase = "far fa-thumbs-up fa-lg btn btn-outline-info"
                    break
                case 116:
                    //resultado pendiente  
                    title = "Resultado pendiente";
                    clase = "btn btn-outline-dark fa fa-hourglass fa-lg"
                    break
                case 117:
                    //Informado a medico
                    title = "Informado a medico";
                    clase = "fa fa-check-square fa-lg btn btn-outline-success"
                    break
            }
            content += `<tr id="trMedicamentosModal_" class='text-center'>
                        <td>${e.DescripcionExamen}</td>
                        <td>
                            ${e.FechaSolicitud ? moment(e.FechaSolicitud).format("DD-MM-YYYY hh:ss") : "N/A"} </br>
                            ${e.ProfesionalSolicitud ? e.ProfesionalSolicitud : ""} 
                        </td>
                        <td><i class="${clase}"
                        data-toggle="tooltip"
                        data-placement="bottom"
                        title="${title}"
                        </td>
                        <td>
                            ${e.FechaTermino ? moment(e.FechaTermino).format("DD-MM-YYYY hh:ss") : "N/A"} </br>
                            ${e.ProfesionalTermina ? e.ProfesionalTermina : ""}
                        </td>
                        <td>
                            <a  class="btn btn-danger"
                                onclick='quitarExamLab(this)'
                                data-estado="${e.IdEstado}" 
                                data-id-cartera="${e.IdServicioCartera}" 
                                data-id-tipo-cartera="61"
                                data-name="${e.DescripcionExamen}"
                                data-toggle="tooltip"
                                data-placement="bottom"
                                title="Eliminar examen"
                                cursor:pointer;">
                                <i class="fa fa-trash fa-lg"></i>
                            </a>
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            <a  class="btn btn-info"
                                onclick='printModalExamen(this)'
                                data-estado="${e.IdEstado}" 
                                data-id-cartera="${e.IdServicioCartera}" 
                                data-id-tipo-cartera="61"
                                data-name="${e.DescripcionExamen}"
                                data-toggle="tooltip"
                                data-placement="bottom"
                                title="Cambiar estado"
                                style="cursor:pointer;">
                                <i class="fas fa-exchange-alt fa-lg"></i>
                            </a>
                        </td>
                        </tr>`;
        })
    } else {
        content += `<tr><h5>No se han agregado elementos todavia</h5></tr>`;
    }
    $("#tbodyExamLab").append(content);
    $('[data-toggle="tooltip"]').tooltip()

}

const imprimeFechaHora = (data) => {

    const { Ingreso: { Fecha, Hora } } = data[0]

    let fechaHosp = (moment(Fecha).format('DD-MM-YYYY') != null) ? moment(Fecha).format('DD-MM-YYYY') : ""
    let horaHosp = (Hora != null) ? Hora : ""

    return `${fechaHosp} ${horaHosp}`
}

// COMBO TIPO HOJA ENFERMERIA
const comboTipoHojaEnfermeria = () => {

    let url = `${GetWebApiUrl()}/HOS_Tipo_Hoja_Enfermeria/combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoHojaEnfermeria'))

    $('#slttipohojaenfermeria').change(function () {

        let value = $(this).val()
        let text = $(this).find('option:selected').text()
    })
}
//Combo talla del paciente para la evolucion
const comboTalla = () => {
    let url = `${GetWebApiUrl()}/GEN_Tipo_Talla/Combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoAltura'))
}
//Combo peso del paciente para la evolucion
const comboPeso = () => {
    let url = `${GetWebApiUrl()}/GEN_Tipo_Peso/Combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoPeso'))
}
// COMBO TURNO
const comboTurno = () => {
    let url = `${GetWebApiUrl()}/GEN_Turno/Combo`;
    setCargarDataEnCombo(url, false, $('#sltIdTurnoHojaEnfermeria'))
}



// COMBO TIPO EXTREMIDAD
const comboTipoExtremidad = () => {

    let url = `${GetWebApiUrl()}/GEN_Tipo_Extremidad/Combo`;
    setCargarDataEnCombo(url, false, $('#sltIdTipoExtremidad'))
}

// COMBO TIPO PLANO
const comboTipoPlano = () => {

    let url = `${GetWebApiUrl()}/GEN_Tipo_Plano/Combo`;
    setCargarDataEnCombo(url, false, $('#sltIdTipoPlano'))
}
function edicionSignosVitales() {
    //Existen datos ingresados?
    if (signosVitalesIngresados.length > 0) {
        //Buscar inputs
        let input = $("#divSignosVitales").find("input");
        //Recorre los arreglos para cargar los datos que anteriormente han sido ingresados
        signosVitalesIngresados.map((e, a) => {
            input.map((_, x) => {
                if (e.Nombre == x.placeholder) {
                    $("#" + x.id).val()
                }
            })
        })
    }
}
//Carga los signos vitales de la bdd
function mostrarSignosVitales(element) {
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}GEN_Tipo_Medida/Combo`,
        contentType: "application/json",
        dataType: "json",
        async: true,
        success: function (data) {
            //$('#lblPacienteSignosVitales').html($(sender).data('pac'));

            $("#divSignosVitales").empty();
            $("#divSignosVitales").append(
                `
                    <h5 class="mb-3"><strong><i class='fa fa-heartbeat'></i> Signos vitales</strong></h5>
                    <div class='row'></div>
                `);

            $.each(data, function (i, r) {

                let type = "number";
                if (r.Formato === "str") {
                    type = "text";
                }

                $("#divSignosVitales .row").append(
                    `<div class='col-md-2 mt-2'>
                        <label>${r.Valor}</label>
                        <input id='sltSignoVital_${r.Id}' name='${r.Id}' type='${type}' maxlength='${r.Maximo}' onkeypress="validarLongitud(this, ${r.Maximo})" class='form-control ${type}' placeholder='${r.Valor}'/>
                    </div>`);

                $(`#sltSignoVital_${r.Id}`).data("id", r.Id);

            });

            $(".number").unbind();
            validarDecimales();
            //$("#aGuardarSignoVital").data("id", $(sender).data('id'));
            //CargarSignosVitalesId($(sender).data('id'))
            $("#mdlSignosVitales").modal("show");

        },
        error: function (err) {
            console.log("No se pudo cargar el modal con los signos vitales")
        }
    });
    edicionSignosVitales()
}
function dibujarSignosVitales() {
    //infoSignosVitales
    $("#infoSignosVitales").empty();
    $("#titleIngresoActual").empty();
    let html = "";
    if (signosVitalesIngresados.length > 0) {
        title = "<h4><b>Ingreso actual:</b></h4>";
        $("#titleIngresoActual").html(title);
        signosVitalesIngresados.map((e, index) => {
            html += `<div class="col-md-2" ><span class="btn btn-outline-dark mb-2"><b>${e.Nombre}:</b> ${e.Valor} &nbsp;<a class="fa fa-times-circle" onclick="eliminarSigno(${index})" style="cursor:pointer; color:red;"></a> </span></h5></div>`;
        })
        $("#infoSignosVitales").css("border-bottom", "1px solid black")
        $("#infoSignosVitales").append(html)
    } else {
        toastr.error("No se ingresaron signos")
    }
}
function eliminarSigno(id) {
    signosVitalesIngresados.splice(id, 1)
    dibujarSignosVitales();
}
//TABLA INTERVENCIONES PACIENTE
const getIntervencionesPaciente = (idHospitalizacion) => {

    let adataset = []
    let cantidadFilas = 0

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/PAB_Protocolo_Operatorio/buscar?idHospitalizacion=${idHospitalizacion}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            if (data.length !== 0) {

                let fechaAux = moment(GetFechaActual()).format("YYYY-MM-DD")
                let fechaActual = moment(fechaAux);

                data.forEach((item, index) => {
                    adataset.push([
                        item.Id,
                        moment(item.Fecha).format("DD-MM-YYYY"),
                        (item.Diagnostico != null) ? item.Diagnostico : "",
                        (item.OperacionRealizada != null) ? item.OperacionRealizada : "",
                        item.DiasOperados = fechaActual.diff(moment(item.Fecha).format("YYYY-MM-DD"), 'days'),
                        (item.HoraInicio != null) ? item.HoraInicio : "",
                        (item.HoraFin != null) ? item.HoraFin : ""
                    ]);

                });

                if (!$.isEmptyObject(adataset)) {

                    $("#tblIntervenciones").DataTable({
                        responsive: {
                            details: false,
                            type: 'column'
                        },
                        data: adataset,
                        columns: [
                            { title: "ID" },
                            { title: "Fecha" },
                            { title: "Diagnostico" },
                            { title: "Operacion Realizada" },
                            { title: "Días Operados" },
                            { title: "Hora Inicio" },
                            { title: "Hora Final" },
                            { title: "" }
                        ],

                        columnDefs: [
                            { responsivePriority: 10001, targets: 6 },
                            { responsivePriority: 10001, targets: 5 },
                            { responsivePriority: 10001, targets: 4 },
                            { responsivePriority: 10001, targets: 3 },
                            { responsivePriority: 10001, targets: 2 },
                            { responsivePriority: 10001, targets: 1 },
                            {
                                targets: -1,
                                defaultContent: "<button type='button' class='btn btn-outline-primary'>Imprimir</button>",
                            }
                        ]
                    })
                }

                cantidadFilas = $("#tblIntervenciones").DataTable().rows().count()
                $("#cantidadIntervenciones").text(cantidadFilas)

            }
            else {
                $('#alertIntervenciones').append("No existe información para mostrar")
                $("#cantidadIntervenciones").removeClass("badge-success")
                $("#cantidadIntervenciones").addClass("badge-secondary")
                $("#cantidadIntervenciones").text(cantidadFilas)
            }
        },
        error: function (err) {
            console.log(JSON.stringify(err))
            $("#modalCargando").hide()
        }
    });
}

//TABLA PROCEDIMIENTOS PROGRAMADOS
const getProcedimientosProgramados = (idHospitalizacion) => {

    let adataset = []
    cantidadFilas = 0

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/PAB_Formulario_Ind_Qx/Buscar?idHospitalizacion=${idHospitalizacion}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            if (data.length != 0) {

                const { IdFormulario, Diagnostico, Cirugia, FechaPropuesta, Especialidad, Estado, Modalidad } = data[0]

                data.forEach((item, index) => {
                    adataset.push([
                        IdFormulario,
                        Diagnostico,
                        Cirugia,
                        moment(FechaPropuesta).format("DD-MM-YYYY"),
                        Especialidad,
                        Estado,
                        Modalidad
                    ]);

                });

                if (!$.isEmptyObject(adataset)) {

                    $("#tblProcedimientosProgramados").DataTable({
                        responsive: {
                            details: false,
                            type: 'column'
                        },
                        data: adataset,
                        columns: [
                            { title: "Diagnostico" },
                            { title: "Cirugia" },
                            { title: "Fecha Propuesta" },
                            { title: "Especialidad" },
                            { title: "Estado" },
                            { title: "Modalidad" },
                            { title: "" }
                        ],

                        columnDefs: [
                            { responsivePriority: 10001, targets: 6 },
                            { responsivePriority: 10001, targets: 5 },
                            { responsivePriority: 10001, targets: 4 },
                            { responsivePriority: 10001, targets: 3 },
                            { responsivePriority: 10001, targets: 2 },
                            { responsivePriority: 10001, targets: 1 },
                            {
                                targets: -1,
                                defaultContent: "<button type='button' class='btn btn-outline-primary'>Imprimir</button>",
                            }
                        ]
                    })
                }

                cantidadFilas = $("#tblProcedimientosProgramados").DataTable().rows().count()
                $("#cantidadProcedimientosProgramados").text(cantidadFilas)

            }
            else {
                $('#alertProcedimientos').text("No existe información para mostrar")
                $('#cantidadProcedimientosProgramados').append("No existe información para mostrar")
                $("#cantidadProcedimientosProgramados").removeClass("badge-success")
                $("#cantidadProcedimientosProgramados").addClass("badge-secondary")
                $("#cantidadProcedimientosProgramados").text(cantidadFilas)
            }
        },
        error: function (err) {
            console.log(JSON.stringify(err))
            $("#modalCargando").hide()
        }
    });
}

const ocultaCombosExtremidadPlano = () => {

    $("#sltIdTipoExtremidad").hide()
    $("#lblIdTipoExtremidad").hide()
    $("#sltIdTipoPlano").hide()
    $("#lblIdTipoPlano").hide()
}

const muestraCombosExtremidadPlano = () => {

    $("#sltIdTipoExtremidad").show()
    $("#lblIdTipoExtremidad").show()
    $("#sltIdTipoPlano").show()
    $("#lblIdTipoPlano").show()
}


//VALIDA SI EXISTE BRAZALETE

const mostrarBrazalete = (id) => {

    if (id > 0 && id !== undefined) {

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}`,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                const { Extremidad: { Id: IdExtremidad }, Plano: { Id: IdPlano } } = data

                if (IdExtremidad > 0 || IdPlano > 0) {
                    $("#switchBrazalete").attr("checked", true)
                    muestraCombosExtremidadPlano()
                }
            },
            error: function (err) {
                console.log(JSON.stringify(err))
            }
        })
    }

    return false
}

//SWITCH USO BRAZALETE
$("#switchBrazalete").change(function () {

    let opcion = $(this).is(':checked');

    if (opcion === true) {
        muestraCombosExtremidadPlano()
        $("#sltIdTipoExtremidad").append("<option value='0' selected>-Seleccione-</option>").val('0');
        $("#sltIdTipoExtremidad").attr("data-required", "true")
        $("#sltIdTipoPlano").append("<option value='0' selected>-Seleccione-</option>").val('0');
        $("#sltIdTipoPlano").attr("data-required", "true")
    }
    else {
        $("#sltIdTipoExtremidad").removeAttr("data-required")
        $("#sltIdTipoExtremidad").val(0)
        $("#sltIdTipoPlano").removeAttr("data-required")
        $("#sltIdTipoPlano").val(0)

        ocultaCombosExtremidadPlano()
    }
})

// PLAN DE CUIDADOS ENFERMERIA
const comboActividadTipoCuidado = () => {

    let url = `${GetWebApiUrl()}/HOS_Tipo_Cuidado/Combo`;
    setCargarDataEnCombo(url, false, $('#sltActividadesEnfermeria'))
    $('#sltActividadesEnfermeria option:first').text("Seleccione Actividad")
}
//Combo invasivo
function cargarComboTipoInvasivo() {
    let url = "/GEN_Tipo_Invasivo/Combo";
    let id = "#sltTipoInvasivoVigilancia";

    if (!setCargarDataEnCombo(GetWebApiUrl() + url, false, id)) {
        $(id).append(`<option value='0'>Seleccione</option>`);
    }
}
//combo extremidad invasivo
function cargarComboTipoExtremidad() {
    let url = "/GEN_Tipo_Extremidad/Combo";
    let id = "#sltTipoExtremidadVigilancia";

    if (!setCargarDataEnCombo(GetWebApiUrl() + url, false, id)) {
        $(id).append(`<option value='0'>Seleccione</option>`);
    }
}
//combo tipo plano invasivo
function cargarComboTipoPlano() {
    setCargarDataEnCombo(GetWebApiUrl() + "/GEN_Tipo_Plano/Combo", false, "#sltTipoPlanoVigilancia");
}

function comboHidricos() {
    let elemento = "#selectHidrico"
    let url = `${GetWebApiUrl()}/HOS_Tipo_Balance/1/Combo`;
    setCargarDataEnComboGroup(url,false,elemento)
}

const comboCantidadTipoCuidado = () => {

    let obj = []

    for (let i = 1; i <= 12; i++) {
        let objAux = new Object()
        obj[i] = objAux.i = i
    }
    obj.shift()

    const el = $("#sltCantidadActividades")
    el.html('<option value="0" selected>Seleccione Cantidad</option>')

    obj.forEach((value, key) => {
        el.append($('<option></option>').attr('value', value).text(value))
    })
}

function validarPlanCuidado(valueActividadesEnfermeria, valueCantidadActividades) {
    if (valueActividadesEnfermeria === 0 || valueCantidadActividades === 0) {
        Swal.fire(
            'Por favor',
            'Seleccione una actividad y la cantidad de horas',
            'info'
        )
        return false
    }


    let filtroPlanCuidados = planCuidados.filter(e => parseInt(e.Id) === valueActividadesEnfermeria)

    if (filtroPlanCuidados.length > 0) {

        Swal.fire(
            'Información',
            'Ya existe esta actividad en el plan de cuidados',
            'info'
        )

        return false
    }

    return true
}

function dibujarPlanCuidados(data) {
    let html = ""
    let clase = ""
    let textToolTip = ""
    let textTooltipObs = ""
    let id = ""

    $("#tblPlanCuidadoEnfermeria").empty()

    html += `<thead>
                <tr>
                    <th scope="col" class="col-md-2">Actividades</th>
                    <th scope="col">Horarios</th>
                    <th scope="col">Observaciones</th>
                    <th scope="col" class="col-md-2" >Acciones</th>
                </tr>
            </thead>
            <tbody>
            `
    data.forEach(e => {
        html += `<tr><td>${e.Descripcion}</td><td style="max-width: 100px; overflow: hidden;">`
        e.Horarios.forEach((x, index) => {
            switch (x.IdEstado) {
                case 119:
                    clase = "btn btn-outline-secondary" //Pendiente    
                    textToolTip = "Pendiente"
                    textTooltipObs = (x.ObservacionCierre !== null) ? x.ObservacionCierre : ""
                    break
                case 120:
                    clase = "btn btn-outline-success" //Realizado
                    textToolTip = "Realizado"
                    textTooltipObs = (x.ObservacionCierre !== null) ? x.ObservacionCierre : ""
                    break
                case 121:
                    clase = "btn btn-outline-danger" //No Realizado
                    textToolTip = "No realizado"
                    textTooltipObs = (x.ObservacionCierre !== null) ? x.ObservacionCierre : ""
                    break
                default:
                    console.log("No existe el estado")
            }

            html += `<button type="button" 
                    data-id-estado-horario='${e.Id}${index}${x.Horario}' 
                    data-descripcion-horario='${e.Descripcion}' 
                    onclick='cambiarEstadoHorarioPlanCuidado(this)'
                    class="${clase} mr-1" data-toggle="tooltip" 
                    data-placement="bottom" title="${textTooltipObs}" 
                    style='font-size: 16px; cursor: pointer;'>
                    ${moment(x.Horario, "HH:mm:ss").format("HH:mm")}
                    </button>`
                
        })

        html += `</td>
                    <td style="max-width: 10px; overflow: hidden;">
                    ${e.Observacion}
                    </td>
                    <td style="max-width: 5px; overflow: hidden;">
                    <button id='btnEliminarActividad${e.Id}' 
                            onclick='eliminarPlanCuidado(${e.Id},"${e.Descripcion}")' 
                            class='btn btn-outline-danger mr-2'
                            data-toggle="tooltip" data-placement="bottom"
                            title="Eliminar" type='button'>
                    <i class='fas fa-trash-alt'></i>
                    </button>
                    <button id='btnEditarActividad${e.Id}'
                            onclick='editarPlanCuidado(${e.Id})' 
                            class='btn btn-outline-primary'
                            data-toggle="tooltip" data-placement="bottom"
                            title="Editar" type='button'>
                    <i class='fas fa-pencil-alt'></i>
                    </button>
                    </td>
                    </tr>`

    })

    html += `</tbody>`

    $("#tblPlanCuidadoEnfermeria").append(html)
    $('[data-toggle="tooltip"]').tooltip()
}

const cambiarEstadoHorarioPlanCuidado = (e) => {

    
    let id = $(e).data("id-estado-horario")
    let descripcion = $(e).data("descripcion-horario")
    let obs = null

    planCuidados.filter(x => {
        x.Horarios.find((n, index) => {
            let idx = `${x.Id}${index}${n.Horario}`

            if (idx === id) {
                if (n.IdEstado === 119) {

                    $("#radioPendiente").prop("checked", true)
                    obs = (n.ObservacionCierre !== null) ? n.ObservacionCierre : null
                    $("#txtObsHorario").val(obs)
                }                    
                if (n.IdEstado === 120) {

                    $("#radioRealizado").prop("checked", true)
                    obs = (n.ObservacionCierre !== null) ? n.ObservacionCierre : null
                    $("#txtObsHorario").val(obs)
                }

                if (n.IdEstado === 121) {

                    $("#radioNoRealizado").prop("checked", true)
                    obs = (n.ObservacionCierre !== null) ? n.ObservacionCierre : null
                    $("#txtObsHorario").val(obs)
                }

                else {
                    return false
                }
            }
            
        })
    })

    $("#btnCambiarEstadoModalCuidadoEnfermeria").click(function () {
        if (validarCampos("#mdlEstadoHorariosPlanCuidadoEnfermeria", false)) {
            $(this).unbind()

            planCuidados.filter(x => {
                x.Horarios.find((n, index) => {

                    let idx = `${x.Id}${index}${n.Horario}`

                    if (idx == id) {

                        //console.log("idx: " + idx + " id: " + id)

                        if ($("#radioPendiente").prop("checked")) {

                            n.IdEstado = parseInt($("#radioPendiente").val())
                            n.ObservacionCierre = $("#txtObsHorario").val()
                            return false

                        }
                        else if ($("#radioRealizado").prop("checked")) {

                            n.IdEstado = parseInt($("#radioRealizado").val())
                            n.ObservacionCierre = $("#txtObsHorario").val()
                            return false
                        }
                        else if ($("#radioNoRealizado").prop("checked")) {

                            n.IdEstado = parseInt($("#radioNoRealizado").val())
                            n.ObservacionCierre = valCampo($("#txtObsHorario").val())
                            return false
                        }
                        else {

                            return false                        
                        }
                    }
                })
            })
            dibujarPlanCuidados(planCuidados)

            $("#mdlEstadoHorariosPlanCuidadoEnfermeria").modal("hide")
        }
    })

    $("#radioPendiente").change(() => $("#txtObsHorario").removeAttr("data-required"))
    $("#radioRealizado").change(() => $("#txtObsHorario").removeAttr("data-required"))
    $("#radioNoRealizado").change(() => $("#txtObsHorario").attr("data-required", "true"))


    $("#tituloEstadoHorariosTipoCuidados").html("<span>Cambiando estado del Horario: <b>" + descripcion + "</b></span>")
    $("#mdlEstadoHorariosPlanCuidadoEnfermeria").modal("show")

    return false

}

const eliminarPlanCuidado = (id, actividad) => {

    let idactividad = id.toString()
    let indice = 0

    Swal.fire({
        title: `¿Seguro quieres eliminar los horarios de ${actividad}?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.value) {

            planCuidados.find((x, index) => {
                if (x.Id == idactividad)
                    indice = index
            })

            planCuidados.splice(indice, 1)
            dibujarPlanCuidados(planCuidados)
        }
    })
}

const editarPlanCuidado = (id) => {
    //Obtener el plan de cuidado a editar
    const planEncontrado = planCuidados.filter(x => x.Id == id)
    
    let [{ Id, Horarios, Descripcion, Observacion }] = planEncontrado
    
    //Cantidad de horarios encontrados
    cantidadHorarios = Horarios.length
    //setear id al select tipo cuidado enfermeria
    $("#sltActividadesEnfermeria").val(Id)

    html = ""
    //Titulo del modal
    $("#tituloHorariosTipoCuidados").html("HORARIOS DE <b>" + Descripcion + "</b> CANTIDAD: " + "<b>" + cantidadHorarios + "</b>")

    $("#formHorarios").empty()

    html += `</div class='col-md'>
            `

    $("#formHorarios").append(html)

    Horarios.forEach((x, index) => {
        $("#formHorarios").append(`<div class="col-md-2 mb-4">
        <label for="formHorariosId${index}">Horario: ${(index + 1)} 
        </label>
        <input type="time"
                class="form-control"
                id="formHorariosId${index}" 
                data-observacion="${x.ObservacionCierre}" 
                data-idestado="${x.IdEstado}" 
                value="${x.Horario}"
                placeholder="horario"
                data-required="true">
        </div>`)
    })
    $("#formHorarios").append(`</div></div><div class="row"></div class="col-md">`)

    $("#formHorarios").append(`<textarea id="observacionesHorarios" 
                                class="form-control invalid-input"
                                maxlength="200" rows="5"
                                style="resize: none;"
                                data-required="true">${Observacion.trim()}</textarea>`)

    $("#btnAgregarModalCuidadoEnfermeria").text("Editar")

    $("#mdlHorariosPlanCuidadoEnfermeria").modal("show")


    const arr = planEncontrado.filter(x => x.Id == id)

    return arr
}

//****Desplega el modal para agregar actividades de cuidado****//
const agregarActividadesPlanCuidado = () => {

    $("#sltActividadesEnfermeria").change(function () {
        $('#sltCantidadActividades option:eq(1)').prop('selected', true)
    })

    $("#btnAgregarActividades").click(function () {

        $("#btnAgregarModalCuidadoEnfermeria").text("Aceptar")
        $("#tblPlanCuidadoEnfermeria").show()

        let valueActividadesEnfermeria = parseInt($("#sltActividadesEnfermeria").val())
        let valueCantidadActividades = parseInt($("#sltCantidadActividades").val())
        let actividad = $("#sltActividadesEnfermeria").find('option:selected').text()

        html = ""

        if (validarPlanCuidado(valueActividadesEnfermeria, valueCantidadActividades)) {

            $("#tituloHorariosTipoCuidados").html("HORARIOS DE <b>" + actividad + "</b> CANTIDAD: " + "<b>" + valueCantidadActividades + "</b>")

            $("#formHorarios").empty()

            html += `</div class='col-md'>
            `

            $("#formHorarios").append(html)

            for (let i = 0; i < valueCantidadActividades; i++) {

                $("#formHorarios").append(`<div class="col-md-2 mb-4">
                                                <label for="formHorariosId${i}">Horario: ${(i + 1)} 
                                                </label>
                                                <input type="time"
                                                class="form-control"
                                                id="formHorariosId${i}"
                                                placeholder="horario"
                                                data-required="true">
                                            </div>`)
            }

            $("#formHorarios").append(`</div></div><div class="row"></div class="col-md">`)

            $("#formHorarios").append(`<textarea id="observacionesHorarios" class="form-control invalid-input" maxlength="200" rows="5" style="resize: none;" data-required="true"></textarea>`)

            $("#mdlHorariosPlanCuidadoEnfermeria").modal("show")

        }
    })
}


const llenarTablaPlanDeCuidados = () => {

    let tipoActividad = ""
    let cantidad = 0
    let items = []
    let tipoActividades = {}

    $("#btnAgregarModalCuidadoEnfermeria").click(function () {

        tipoActividad = $("#sltActividadesEnfermeria").find('option:selected').text()
        idTipoActividad = $("#sltActividadesEnfermeria").find('option:selected').val()
        cantidad = parseInt($("#sltCantidadActividades").val())
        obs = $("#observacionesHorarios").val()


        if ($("#btnAgregarModalCuidadoEnfermeria").text() == "Editar") {

            let obs = document.getElementById("observacionesHorarios").value

            let horarios = []
            
            //Busca los inputs para obtener la hora (por si es editada)
            let inputs = $("#formHorarios").find('input')
            horarios = [...inputs.map((index, item) => {
                //Obtiene valores enviados por data para conservar el estado y al observacion
                let estado = $(item).data("idestado")
                let obs = $(item).data("observacion")
                return {
                    IdEstado: estado,
                    Horario: item.value,
                    ObservacionCierre: obs
                }
            })]
            //Retorna el elemento a editar
            const obj = editarPlanCuidado(idTipoActividad)
            //extraer id
            const [{ Id }] = obj
            //Hacer las modificaciones ingresadas
            if (obs !== "") {
                const arr = planCuidados.filter(x => {
                    if (Id == x.Id) {
                        x.Observacion = obs
                        x.Horarios = horarios
                    }
                    return x
                })
                planCuidados = arr

                $("#btnAgregarModalCuidadoEnfermeria").text("Aceptar")

                dibujarPlanCuidados(planCuidados)
                $("#mdlHorariosPlanCuidadoEnfermeria").modal("hide")
            }

            return false

        }
        if ($("#btnAgregarModalCuidadoEnfermeria").text() == "Aceptar") {
            if (validarCampos("#formHorarios", false)) {

                for (let i = 0; i < cantidad; i++) {

                    let value = $('#formHorariosId' + i).val()

                    items.push({ IdEstado: 119, Horario: value, ObservacionCierre: null })
                }

                tipoActividades = {
                    Id: parseInt(idTipoActividad),
                    Horarios: items, // {Hora: '12:00', IdEstado: 1}   
                    Descripcion: tipoActividad,
                    Observacion: obs
                }

                planCuidados.push(tipoActividades)

                dibujarPlanCuidados(planCuidados)

                items = []

                $("#mdlHorariosPlanCuidadoEnfermeria").modal("hide")
            }
        }
    })
}
function validarRiesgosValoraciones() {
    let bolRiesgo = true, bolValoracion = true
    //Gestion  de arreglos para enviar a la api
    //True = Valida campos riesgos
    if ($("#addRiesgos").data("clicked") == true) {
        bolRiesgo = guardarRiesgosValoracion(1)
    }
    //True == para validar los campos de las valoraciones
    if ($("#addValoracion").data("clicked") == true) {
        bolValoracion = guardarRiesgosValoracion(10)
    }
    //concatenar riesgos y valoraciones en un solo arreglo para la API
    if (bolRiesgo == true && bolValoracion == true) {
        let riesgosLength = riesgosApi.length
        let valoracionesLength = valoracionesApi.length
        if (riesgosLength > 0 && valoracionesLength > 0) {
            
            //Hay riesgos y valoraciones
            RespuestasApi.push(riesgosApi)
            RespuestasApi.push(valoracionesApi)
            riesgosApi = [], valoracionesApi = []
            //Deshabilita los campos para no tener que validar de nuevo
            let inputRiesgos = $('#divRiesgos').find("input[type='checkbox']")
            for (let item of inputRiesgos) {
                item.disabled = true
            }
            inputRiesgos = $('#divRiesgos').find("input[type='radio']");
            for (let item of inputRiesgos) {
                item.disabled = true
            }
            let inputValoraciones = $('#divValoracion').find("input[type='checkbox']")
            for (let item of inputValoraciones) {
                item.disabled = true
            }
            inputValoraciones = $('#divValoracion').find("input[type='radio']");
            for (let item of inputValoraciones) {
                item.disabled = true
            }
            //Cambia el atributo para notener que validar de nuevo
            $("#addRiesgos").data("clicked", false)
            $("#addValoracion").data("clicked", false)

        } else if (riesgosLength > 0 && valoracionesLength == 0) {
            //Solamente hay riesgos
            
            RespuestasApi.push(riesgosApi)
            riesgosApi = []
            //Deshabilita los campos para no tener que validar de nuevo
            let inputRiesgos = $('#divRiesgos').find("input[type='checkbox']")
            for (let item of inputRiesgos) {
                item.disabled = true
            }
            inputRiesgos = $('#divRiesgos').find("input[type='radio']");
            for (let item of inputRiesgos) {
                item.disabled = true
            }
            $("#addRiesgos").data("clicked", false)

        } else if (valoracionesLength > 0 && riesgosLength == 0) {
            
            //Solamente hay valoraciones
            RespuestasApi.push(valoracionesApi)
            valoracionesApi = []
            //Deshabilita los campos para no tener que validar de nuevo
            let inputValoraciones = $('#divValoracion').find("input[type='checkbox']:checked")
            for (let item of inputValoraciones) {
                item.disabled = true
            }
            inputValoraciones = $('#divValoracion').find("input[type='radio']");
            for (let item of inputValoraciones) {
                item.disabled = true
            }
            $("#addValoracion").data("clicked", false)
        }
    }
    return [
        { Nombre: "Evaluación de riesgo", Validacion: bolRiesgo },
        { Nombre: "Valoracion", Validacion: bolValoracion }
    ]
}
const generateId = () => Math.random().toString(36).substr(2, 18)

function ExtraerDatosApi() {
    let validacionRV = validarRiesgosValoraciones()

    if (vigilanciasAntiguas.length > 0) {
        vigilancias = vigilanciasAntiguas.map((e) => {
            return {
                IdHojaEnfermeriaInvasivo: e.IdHojaEnfermeriaInvasivo,
                FechaCreacion: e.FechaCreacion,
                IdTipoInvasivo: e.IdTipoInvasivo,
                IdTipoPlano: e.IdTipoPlano,
                IdTipoExtremidad: e.IdTipoExtremidad,
                FechaCierre: e.FechaCierre,
                Observacion: e.Observacion,
                IdProfesionalCierra: e.IdProfesionalCierra,
                IdHojaEnfermeriaCierra: e.IdHojaEnfermeriaCierra
            }
        })
        vigilanciasApi = arregloVigilancias.concat(vigilancias);
    } else {
        vigilanciasApi = arregloVigilancias
    }

    SignosVitalesApi = signosVitalesIngresados.map((e) => {
        return {
            "Id": e.Id,
            "Valor": e.Valor
        }
    })
    hidricosApi = IngresosHidricosIngresados.concat(EgresosHidricosIngresados)
    //console.info(planCuidados)
    planCuidadosApi = planCuidados.map(x => {
        return {
            Id: x.Id,
            Observacion: x.Observacion,
            Horarios: x.Horarios
        }
    })

    //console.error(planCuidadosApi)
    tallasIngresadas.map((e) => { tallasApi.push({ IdTipoTalla: parseInt(e.Medida), Talla: parseFloat(e.Valor), FechaHora: e.Fecha }) })
    pesosIngresados.map((e) => { pesosApi.push({ IdTipoPeso: parseInt(e.Medida), Peso: parseFloat(e.Valor), FechaHora: e.Fecha }) })
    ExamenesLabApi = arrayExamenesLaboratorio.map(e => {
        return {
            IdHojaEnfermeriaExamen: e.IdHojaEnfermeriaExamen,
            IdServicioCartera: e.IdServicioCartera,
            IdEstado: e.IdEstado
        }
    })
    
    //Las variables inicialmente son true.. si hay algo para validar incompleto sera false
    return validacionRV
}
function errorDeValidacion(validacion) {
    let camposSinCompletar = validacion.filter(e => e.Validacion == false)
    let stringSinCompletar = ""
    let separador = ""
    camposSinCompletar.map((e, index) => {
        if (index == camposSinCompletar.length - 1 || camposSinCompletar.length == 1) {
            separador = "."
        } else {
            separador = ", "
        }
        stringSinCompletar += e.Nombre + separador
    })
    toastr.error("Error faltan campos por completar:" + stringSinCompletar)
}
// INGRESO NUEVA HOJA ENFERMERIA
function IngresoNuevaHojaEnfermeria(HOS_idHospitalizacion) {
    //Validacion de los riesgos o las valoraciones... false = faltan campos por completar.
    let validacion = ExtraerDatosApi()
    let coincidencia = validacion.find(e => e.Validacion == false)
    let idTipoExtremidad = ($("#sltIdTipoExtremidad").val() != 0) ? $("#sltIdTipoExtremidad").val() : null
    let idTipoPlano = ($("#sltIdTipoPlano").val() != 0) ? $("#sltIdTipoPlano").val() : null

    if (validarCampos("#hojaEnfermeriaGeneral", true) && !coincidencia) {
        let json = {}
        json = {
            "Fecha": moment(GetFechaActual()).format("YYYY-MM-DD HH:mm:ss"),
            "IdTurno": valCampo($("#sltIdTurnoHojaEnfermeria").val()),
            "IdPersonaResponsable": 1, // depende de control de usuario DatosAcompañante.ascx
            "IdTipoRepresentante": 2, // depende de control de usuario DatosAcompañante.ascx
            "IdHospitalizacion": valCampo(HOS_idHospitalizacion),
            "Diagnostico": valCampo($("#txtDiagnosticoHojaEnfermeria").val()),
            "MedicamentosHabituales": valCampo($("#txtMedicamentosHabituales").val()),
            "OtrosProcedimientos": valCampo($("#txtOtrosProcedimientos").val()),
            "Morbidos": valCampo($("#txtMorbidosHojaEnfermeria").val()),
            "Alergias": $("#txtAlergiaHojaEnfermeria").val(),
            "IdTipoExtremidad": valCampo(idTipoExtremidad),
            "IdTipoPlano": valCampo(idTipoPlano),
            "IdTipoHojaEnfermeria": valCampo($("#sltTipoHojaEnfermeria").val()),
            "Evolucion": valCampo($("#txtEvolucionAtencion").val()),
            "SignosVitales": SignosVitalesApi,
            "TipoCuidado": planCuidadosApi,
            "Examenes": ExamenesLabApi,
            "TipoInvasivo": vigilanciasApi,
            "TipoTalla": tallasApi,
            "TipoPeso": pesosApi,
            "TipoBalance": hidricosApi,
            "RespuestasItem": RespuestasApi.flat()
        };

        $.ajax({
            type: "POST",
            url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria`,
            data: JSON.stringify(json),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, status, jqXHR) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Ingreso Correcto',
                    showConfirmButton: false,
                    timer: 2000,
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.timer) {
                        window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`
                    }
                });
            },
            error: function (jqXHR, status) {
                console.log("Error al ingresar hoja de enfermeria: " + JSON.stringify(jqXHR));
            }
        });
    } else {
        errorDeValidacion(validacion)
    }
}

const editarHojaEnfermeria = (id) => {
    //Validacion de los riesgos o valoraciones... false == faltan campos por completar (en caso de ser necesario).
    let validacion = ExtraerDatosApi()
    let coincidencia = validacion.find(e => e.Validacion == false)
    
    let json = {}
    let idTipoExtremidad = ($("#sltIdTipoExtremidad").val() != 0) ? $("#sltIdTipoExtremidad").val() : null
    let idTipoPlano = ($("#sltIdTipoPlano").val() != 0) ? $("#sltIdTipoPlano").val() : null

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            const {
                Fecha,
                IdHospitalizacion
            } = data

            if (validarCampos("#hojaEnfermeriaGeneral", true) && !coincidencia) {

                json = {
                    "Id": id,
                    "Fecha": moment(Fecha).format("YYYY-MM-DD HH:mm:ss"),
                    "IdTurno": valCampo($("#sltIdTurnoHojaEnfermeria").val()),
                    "IdPersonaResponsable": 1,
                    "IdTipoRepresentante": 2,
                    "IdHospitalizacion": IdHospitalizacion,
                    "Diagnostico": valCampo($("#txtDiagnosticoHojaEnfermeria").val()),
                    "Morbidos": valCampo($("#txtMorbidosHojaEnfermeria").val()),
                    "Alergias": $("#txtAlergiaHojaEnfermeria").val(),
                    "MedicamentosHabituales": valCampo($("#txtMedicamentosHabituales").val()),
                    "OtrosProcedimientos": valCampo($("#txtOtrosProcedimientos").val()),
                    "IdTipoExtremidad": valCampo(idTipoExtremidad),
                    "IdTipoPlano": valCampo(idTipoPlano),
                    "IdTipoHojaEnfermeria": valCampo($("#sltTipoHojaEnfermeria").val()),
                    "Evolucion": valCampo($("#txtEvolucionAtencion").val()),
                    "TipoInvasivo": vigilanciasApi,
                    "Examenes": ExamenesLabApi,
                    "SignosVitales": SignosVitalesApi,
                    "TipoPeso": pesosApi,
                    "TipoTalla": tallasApi,
                    "DescripcionEvolucion": valCampo($("#txtEvolucionAtencion").val()),
                    "TipoCuidado": planCuidadosApi,
                    "TipoBalance": hidricosApi,
                    "RespuestasItem": RespuestasApi.flat()
                }

                actualizaHojaEnfermeria(json, id)
            } else {
                errorDeValidacion(validacion)
            }
        },
        error: function (err) {
            console.log(JSON.stringify(err))
        }
    })
}

const actualizaHojaEnfermeria = (json, id) => {

    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}`,
        data: json,
        success: function (data) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Datos Actualizados correctamente',
                showConfirmButton: false,
                timer: 2000,
                allowOutsideClick: false
            }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`
                }
            });
        },
        error: function (err) {
            console.log(JSON.stringify(err))
        }
    });
}

const llenarHojaEnfermeria = () => {

    inicializarCombo()
    let arrayHorarios = []

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}`,
        contentType: "application/json",
        dataType: "json",
        success:
            function (data) {
                const {
                    Profesional: { Nombre: NombreProfesional, ApellidoPaterno: ApellidoPaternoProfesional },
                    Fecha,
                    Turno: { Id: IdTurno, Valor: Turno },
                    Responsable: {
                        Nombre: NombreResponsable,
                        ApellidoPaterno: ApellidoPaternoResponsable,
                        ApellidoMaterno: ApellidoMaternoResponsable,
                        NombreSexo: SexoResponsable,
                        Telefono: TelefonoResponsable
                    },
                    TipoResponsable: { Valor: TipoResponsable },
                    IdHospitalizacion,
                    Diagnostico,
                    Morbidos,
                    Alergias,
                    MedicamentosHabituales,
                    OtrosProcedimientos,
                    Extremidad: { Id: IdExtremidad, Valor: Extremidad },
                    Plano: { Id: IdPlano, Valor: Plano },
                    TipoHoja: { Id: IdTipoHoja, Valor: TipoHoja },
                    Examenes,
                    SignosVitales,
                    TipoCuidado,
                    TipoTalla,
                    TipoPeso,
                    TipoInvasivo,
                    Evolucion,
                    RespuestasItem,
                    TipoBalance
                } = data;

                let fechaBD = moment(Fecha).format("YYYY-MM-DD");               

                dibujarHistorialBalanceHidrico(TipoBalance)
                dibujarHistorialSignosVitales(SignosVitales)
                dibujarHistorialExamenesLaboratorio(Examenes);
                //Arreglo de las vigilancias ya agregadas 
                vigilanciasAntiguas = TipoInvasivo;
                //0 = peso, 1= Talla
                dibujarHistorialPesosTallas(TipoPeso, 0)
                dibujarHistorialPesosTallas(TipoTalla, 1)
                dibujarHistorialInvasivos(TipoInvasivo)
                dibujarHistorialEvolucion(Evolucion)

                let idTipoExtremidad = (IdExtremidad > 0) ? IdExtremidad : 0
                let idTipoPLano = (IdPlano > 0) ? IdPlano : 0                

                dibujarHistorialRiesgos(RespuestasItem)
                $("#txtFechaHojaEnfermeria").val(fechaBD)
                $("#sltTipoHojaEnfermeria").val(IdTipoHoja)
                $("#sltIdTurnoHojaEnfermeria").val(IdTurno)
                $("#txtDiagnosticoHojaEnfermeria").val(Diagnostico)
                $("#txtMorbidosHojaEnfermeria").val(Morbidos)
                $("#txtAlergiaHojaEnfermeria").val(Alergias)
                $("#txtMedicamentosHabituales").val(MedicamentosHabituales)
                $("#txtOtrosProcedimientos").val(OtrosProcedimientos)
                $("#sltIdTipoExtremidad").val(idTipoExtremidad)
                $("#sltIdTipoPlano").val(idTipoPLano)


                arrayHorarios = TipoCuidado.map(x => {
                    return {
                        Id: x.Id,
                        Horarios: {
                            IdEstado: x.IdEstado,
                            Horario: x.Horario,
                            ObservacionCierre: x.ObservacionCierre
                        }
                    }
                })
                let arrayTipoCuidados = [...new Set(TipoCuidado.map(x => x.Id))]
                    .map(id => {
                        return {
                            Id: id,
                            Horarios: arrayHorarios.filter(x => x.Id == id).map(x => x.Horarios),
                            Descripcion: TipoCuidado.find(x => x.Id == id).Descripcion,
                            Observacion: TipoCuidado.find(x => x.Id == id).Observacion
                        }
                    })

                planCuidados = arrayTipoCuidados

                dibujarPlanCuidados(arrayTipoCuidados)
                $("#divPlanCuidadoEnfermeria").show()
                $("#tblPlanCuidadoEnfermeria").show()
            },
        error: function (err) {
            console.log(JSON.stringify(err))
        }
    })
}
function dibujarHistorialBalanceHidrico(data) {

    if (data.length > 0) {
        //Ordenar arreglo por profesional
        let contenido = ""
        let content = ""
        let contenidoTotales = "", contenidoTablaTotales = ""
        let ordenado = [...new Set(data.map(i => i.Profesional))].map(a => {
            return {
                Profesional: a,
                Balances: [...new Set(data.map(a => a.Tipo))].map(i => {
                    return {
                        Balance: i,
                        Items: data.filter(x => x.Tipo == i && x.Profesional == a).map(a => {
                            return {
                                DescripcionBalance: a.DescripcionBalance,
                                Balance: a.Balance,
                                Fecha: traducirMoment(moment(a.Fecha).format('LLL'))
                            }
                        })
                    }
                })
            }
        })
        //Dibujar el las tablas de registros hidricos por profesional
        ordenado.map(a => {
            content = ""
            a.Balances.map(e => {
                content += `<tr class="${e.Balance == "INGRESO" ? "table-success" : "table-danger"} text-center">
                            <th colspan="3">${e.Balance}</th>
                        </tr>
                        <tr><th>Nombre</th><th>Valor</th><th>Fecha</th></tr>`
                e.Items.map(i => {
                    content += `<tr><td>${i.DescripcionBalance}</td><td>${i.Balance}</td><td>${i.Fecha}</td><tr>`
                })
            })
            let idElemento = a.Profesional.replace(/ /g, "")
            contenido += `<div class="col-md-6">
                      <h5>Profesional: ${a.Profesional}</h5>
                      <table id="table-${idElemento}"
                            class="table table-striped table-bordered table-hover col-xs-12 col-md-12">
                      ${content}
                      </table></div>`
        })
        $("#divHistorialHidricos").append(contenido)
        let totales = [... new Set(data.map(a => a.Tipo))].map(i => {
            return {
                Tipo: i,
                Balances: [...new Set(data.filter(e => e.Tipo == i).map(x => x.DescripcionBalance))].map(f => {
                    return {
                        DescripcionBalance: f,
                        Total: data.filter(j => j.DescripcionBalance == f).map(q => q.Balance).reduce((acc, item) => {
                            return acc + item
                        })
                    }
                })
            }
        })
        totales.map(e => {
            contenidoTablaTotales += `<tr class="${e.Tipo == "INGRESO" ? "table-success" : "table-danger"}" text-center">
                            <th colspan="3">${e.Tipo}</th>
                        </tr>
                        <tr><th>Nombre</th><th>Total</th></tr>`
            e.Balances.map(i => {
                contenidoTablaTotales += `<tr><td>${i.DescripcionBalance}</td><td>${i.Total}</td><tr>`
            })
        })
        contenidoTotales = ` 
                    <div class="col-md-6">
                    <h5>Totales por muestra</h5>
                    <table id="table-TotalesHidricos"
                            class="table table-striped table-bordered table-hover col-xs-12 col-md-12">
                            ${contenidoTablaTotales}
                    </table></div>`
        $("#totalHistorialHidrico").append(contenidoTotales)
        let totalesIngresoEgreso = totales.map(e => {
            return {
                Tipo: e.Tipo,
                Total: e.Balances.map(i => i.Total).reduce((acc, item) => acc + item)
            }
        })
        //Dibujar tabla tota ingresos y egresos
        content = ""
        totalesIngresoEgreso.map(a => {
            content += `<tr><td class="${a.Tipo == "INGRESO" ? "table-success" : "table-danger"}" >${a.Tipo}</td><td>${a.Total}</td></tr>`
        })
        contenido = ""
        contenido += ` 
                    <div class="col-md-6">
                    <h5>Totales por ingreso/egreso</h5>
                    <table id="table-TotalesIngresos-Egresos"
                            class="table table-striped table-bordered table-hover col-xs-12 col-md-12">
                            <tr><td>Tipo</td><td>Total</td></tr>
                            ${content}
                    </table></div>`
        $("#totalHistorialHidrico").append(contenido)
    } else {
        contenido = `<div class="alert alert-secondary m-2" role="alert">
                    <i class="fa fa-info-circle fa-lg"></i>
                    No se ha ingresado información previamente
                   </div>`
        $("#divHistorialHidricos").append(contenido)
        $("#totalHistorialHidrico").append(contenido)
    }
}
   
//Funcion que dibuja riesgos o valoraciones ingresadas anteriormente (Cuando se edita una hoja de enfeermeria)
function dibujarHistorialRiesgos(data) {
    let getRiesgos = [], getValoraciones = []
    getRiesgos = data.filter(i => i.IdItem==1)
    getValoraciones = data.filter(i => i.IdItem == 10)
    let titulo,titulo2
    let contenido = ""
    let contenido2 = ""
    let fechaTraducida
    if (getRiesgos.length > 0) {
        titulo = `<div class="alert alert-info m-2" role="alert">
                    <i class="fa fa-info-circle fa-lg"></i>
                    Hay ${getRiesgos.length} ${getRiesgos.length == 1 ? "evaluacion de riesgo registrada." : "evaluaciones de riesgo registradas."}
                   </div>`
        getRiesgos.map(e => {
            fechaTraducida = traducirMoment(moment(e.Fecha).format("LLL"))
            //data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
            contenido += `
                    <button class="btn btn-outline-primary m-2"
                    type="button"
                    data-toggle="collapse"
                    href="#collapse${e.IdHojaEnfermeriaRespuesta}"
                    role="button"
                    data-focus="true"
                    aria-expanded="false"
                    data-tiporespuesta="${e.IdItem}"
                    data-fecha="${fechaTraducida}"
                    id="pop${e.IdHojaEnfermeriaRespuesta}"
                    onclick='buscarHistorialRiesgo(${e.IdHojaEnfermeriaRespuesta},this)'
                    >
                    ${fechaTraducida}
                    </button>
                    `;
        })
        getRiesgos.map(e => {
            contenido += `
                    <div class="collapse" id="collapse${e.IdHojaEnfermeriaRespuesta}">
                        <div class="card card-body">
                            <div class="row" id="cardbodyRiesgo${e.IdHojaEnfermeriaRespuesta}"></div>
                            
                            </div>
                    </div>`
        })
        $("#historialRiesgos").append(contenido)
    } else {
        titulo = `<div class="alert alert-dark m-2" role="alert">
                    <i class="fa fa-info-circle fa-lg"></i> 
                    El paciente no registra aun evaluaciones de riesgo.
                   </div>`
    }

    //Hay valoraciones
    if (getValoraciones.length > 0) {
        titulo2 = `<div class="alert alert-info m-2" role="alert">
                    <i class="fa fa-info-circle fa-lg"></i>
                    Hay ${getValoraciones.length} ${getValoraciones.length == 1 ? "valoracion registrada." : "valoraciones registradas."}
                   </div>`
        getValoraciones.map(e => {
            fechaTraducida = traducirMoment(moment(e.Fecha).format("LLL"))
            //data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
            contenido2 += `
                    <button class="btn btn-outline-primary m-2"
                    type="button"
                    data-toggle="collapse"
                    href="#collapse${e.IdHojaEnfermeriaRespuesta}"
                    role="button"
                    data-focus="true"
                    aria-expanded="false"
                    data-tiporespuesta="${e.IdItem}"
                    data-fecha="${fechaTraducida}"
                    id="pop${e.IdHojaEnfermeriaRespuesta}"
                    onclick='buscarHistorialRiesgo(${e.IdHojaEnfermeriaRespuesta},this)'
                    >
                    ${fechaTraducida}
                    </button>
                    `;
        })
        getValoraciones.map(e => {
            contenido2 += `
                    <div class="collapse" id="collapse${e.IdHojaEnfermeriaRespuesta}">
                        <div class="card card-body">
                            <div class="row" id="cardbodyVal${e.IdHojaEnfermeriaRespuesta}"></div>
                            
                            </div>
                    </div>`
        })
        //historialValoracion
        $("#historialValoracion").append(contenido2)
    } else {
        titulo2 = `<div class="alert alert-dark m-2" role="alert">
                    <i class="fa fa-info-circle fa-lg"></i> 
                    El paciente no registra aun valoraciones.
                   </div>`
    }
    $("#titleHistorialRiesgo").append(titulo)
    $("#titleHistorialValoracion").append(titulo2)
    
}
//Busca la informacion necesarioa para dibujar los ingresos anteriores (riesgo o valoracion)
function buscarHistorialRiesgo(id, element) {
    //focus = true, click para mostrar informacion, y hacer la peticion
    //focus = false, oculta collapse.. no hace peticion, no hace uso de recursos
    let focus = $(element).data("focus")
    if (focus == true) {
        let fechaTitulo = $(element).data("fecha")
        let tipoRespuesta = $(element).data("tiporespuesta")
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria_Item/${id}/RespuestaDetalle`,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                //Data = informaciona dibujar, id= para indentifcar contenedor donde dibujar
                //fecha = fecha del registro de la evaluacion-valoracion
                //Tipo respuesta == id para identificar si es evaluacion o es valoracion
                printHistorialRiesgo(data, id, fechaTitulo, tipoRespuesta)
            },
            error: function (err) {
                console.log(JSON.stringify(err))
            }
        })
        $(element).data("focus", false)
    } else {
        //estaba en false.. cambia a true para mostrar informacion en el proximo click 
        $(element).data("focus", true)
    }
    
}
//Dibuja la informacion para las enfermeras (riesgo o valoracion)
function printHistorialRiesgo(data, id, fechaTitulo, tipoRespuesta) {
    let content = "", contenido = "", categoria = "", stringRespuestas = [], separador="", cantidadRespuestas 
    let Respuestas = [], element;
    
    //Son riegos o valoracioness
    tipoRespuesta == 1 ? element = "#cardbodyRiesgo" : element =  "#cardbodyVal"
    $(element+id).empty()
        content += `<div class="col-md-12"><h5><b>${fechaTitulo}</b></h5></div>`
    data.map(e => {
            contenido = ""
            e.Preguntas.map(i => {
                stringRespuestas = ""
                cantidadRespuestas = i.Alternativas.length
                //Mas de una respuesta
                    if (cantidadRespuestas == 1) {
                        //Solo una respuesta
                        separador = "."
                        stringRespuestas += `${i.Alternativas[0].DescripcionAlternativa + separador}`
                    } else {
                        i.Alternativas.map((x,index) => {
                            index == parseInt(cantidadRespuestas) - 1 ? separador = "." : separador = ", "
                            stringRespuestas += `${x.DescripcionAlternativa + separador}`
                        })
                }
                if (tipoRespuesta == 1) {
                    contenido += ` <span> <b> ${i.DescripcionPregunta}: </b>${stringRespuestas}</span></br>`;
                } else {
                    contenido += `<div class="col-lg-4"><span> <b> ${i.DescripcionPregunta}: </b>${stringRespuestas}</span></div>`
                }
                
            })
            content += `<div class="col-md-4 col-lg-${tipoRespuesta == 1 ? "3": "12"}" >
                        <div class="card">
                            <div class="card-header bg-gradient-warning">
                                <h6>${e.DescripcionItem}</h6>
                            </div>
                            <div class="card-body ${tipoRespuesta==10 ? "row" : ""}" style="min-height:190px;">
                                ${contenido}
                            </div>
                        </div>
                   </div>`
        })
    $(element+id).append(content)
    
}
function dibujarHistorialInvasivos(data) {
    $("#tbodyInvasivos").empty()
    let content;
    let estado;
    if (data.length > 0) {
        data.map((e, index) => {
            content += `
                    <tr class="table-${e.IdProfesionalCierra ? "danger" : "success"}">
                    <td>${e.DescripcionTipoInvasivo}</td>
                    <td>${e.DescripcionExtremidad}</td>
                    <td>${e.DescripcionPlano}</td>
                    <td>${moment(e.FechaCreacion).format("YYYY-MM-DD hh:ss")} &nbsp;  ${e.NombreProfesionalCrea}</td>
                    <td>${e.IdProfesionalCierra ? e.FechaCierre + " " + e.NombreProfesionalCierre : "N/A"}</td>
                    <td>${e.Observacion}</td>
                    <td>
                        <button
                        type="button"
                        data-toggle=${!e.IdProfesionalCierra ? "tooltip" : ""}
                        data-placement="bottom"
                        data-original-title="Retirar invasivo"
                        class="btn btn-outline-danger ${e.IdProfesionalCierra ? "disabled" : ""}"
                        style="cursor:${e.IdProfesionalCierra ? "forbidden" : "pointer"}"
                        ${e.IdProfesionalCierra ? "" : `onclick="retirarInvasivo(${index})"`}>
                            <i class="fas fa-sign-out-alt"></i>
                        </button>
                    </td>
                    </tr >`;
        })
    } else {
        content = "<tr><td></td> <td> <i class='fa fa-info'></i> &nbsp; No se han encontrado registros </td> </tr>";
    }
    $("#tbodyInvasivos").append(content)
    $('#tbodyInvasivos [data-toggle="tooltip"]').tooltip()
}
function retirarInvasivo(indice) {
    Swal.fire({
        title: `¿Retirar invasivo?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.value) {
            vigilanciasAntiguas.map((e, i) => {
                if (i == indice) {
                    e.IdProfesionalCierra = sesion.ID_USUARIO;
                    e.NombreProfesionalCierre = sesion.NOMBRE_USUARIO;
                    e.IdHojaEnfermeriaCierra = sesion.ID_HOJA_ENFERMERIA;
                    e.FechaCierre = moment().format('YYYY-MM-DD, hh:mm');
                }
            })
            dibujarHistorialInvasivos(vigilanciasAntiguas)
        }
    })
}
function dibujarHistorialPesosTallas(data, Tipo) {
    let content = "";
    let tabla;
    if (data.length > 0) {
        data.map((e) => {
            content += `<tr>
                        <td>${moment(e.Fecha).format("YYYY-DD-MM hh:mm")}</td>
                        <td>${Tipo == 0 ? e.Peso : e.Talla} &nbsp; ${Tipo == 0 ? e.DescripcionPeso : e.DescripcionTalla} </td>
                        </tr>`;
        })
    } else {
        content += "<tr><td></td> <td> <i class='fa fa-info'></i> &nbsp; No se han encontrado registros </td> </tr>"
    }
    if (Tipo == 0)
        tabla = "#tbodyPesos";
    else
        tabla = "#tbodyTallas";

    $(tabla).append(content)
}
function dibujarHistorialEvolucion(data) {
    let content = "";
    if (data.length > 0) {
        data.map((e) => {
            content += `
                    <div class="card m-2">
                      <div class="card-header">
                        <b>${moment(e.Fecha).format("YYYY-MM-DD hh:mm")} &nbsp;|&nbsp; ${e.NombreProfesional}</b>
                      </div>
                      <div class="card-body">
                          <p>${e.Descripcion}</p>
                        
                      </div>
                    </div>
                    `;
        })
    } else {
        content += "<tr><td></td> <td> <i class='fa fa-info'></i> &nbsp; No se han encontrado registros </td> </tr>"
    }
    $("#tbodyDescripcion").append(content)
}
function dibujarHistorialExamenesLaboratorio(Examenes) {
        arrayExamenesLaboratorio = Examenes;
        printTableExamen();
}
//Shortcut
function dibujarHistorialSignosVitales(SignosVitales) {
    let fecha2;
    let id = 0;
    if (SignosVitales.length > 0) {
        title = "<h5>Historial de signos vitales:</h5>"
        SignosVitales.map((e) => {
            let Padre = "", Hijo = "";
            let fechaTraducida = traducirMoment(moment(e.Fecha).format("LLL"))
            if (e.Fecha != fecha2) {
                id++
                Padre += ` <div class="col-sm-2 col-md-4 col-lg-3">
                                           <div class="card">
                                                <div class="card-header" id="heading-${id}">
                                                  <h5 class="mb-0">
                                                    <a class="btn btn-outline-info"
                                                        data-toggle="collapse"
                                                        href="#collapse-${id}"
                                                        aria-expanded="false"
                                                        aria-controls="collapseOne">
                                                      ${fechaTraducida}
                                                    </a>
                                                  </h5>
                                                </div>
                                                <div id="collapse-${id}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                                    <div class="card-body" id="${id}">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>`
                $("#historialSignosVitales").append(Padre);
            }
            Hijo += `<div class="col" ><h5><span class="badge badge-pill badge-dark p-2"><b>${e.Descripcion}:</b> ${e.Valor}</span></h5></div>`;

            $("#" + id).append(Hijo)
            fecha2 = e.Fecha;
        })
        $("#titleConSignos").append(title);
    } else {
        $("#titleConSignos").empty();
        Padre = "<h5><i class='fa fa-info'></i> No se ha ingresado signos vitales hasta el momento</h5>"

        $("#titleConSignos").append(Padre);

    }
}

// MOSTRAR N° HOSPITALIZACION
const mostrarIdHOspitalizacion = (idHojaEnfermeria = undefined, idHospitalizacion) => {

    const tituloHojaEnfermeria = (idHojaEnfermeria !== undefined || idHojaEnfermeria > 0)
        ?
        ` N° de Hospitalización: ${sesion.ID_HOSPITALIZACION} / Hoja N° ${sesion.ID_HOJA_ENFERMERIA}`
        :
        ` N° de Hospitalización: ${sesion.ID_HOSPITALIZACION}`

    $("#cardIdHospitalizacion").text(tituloHojaEnfermeria)
}

// MOSTRAR FECHA HOJA ENFERMERIA
const mostrarFechaHojaEnfermeria = (id) => {

    if (id > 0 || id !== undefined) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}`,
            contentType: "application/json",
            dataType: "json",
            success: function ({ Fecha }) {
                $("#txtFechaHojaEnfermeria").attr('value', moment(Fecha).format("YYYY-MM-DD"))
            },
            error: function (err) {
                console.log(JSON.stringify(err))
            }
        })
    }
    else
        $("#txtFechaHojaEnfermeria").attr('value', moment(GetFechaActual()).format("YYYY-MM-DD"))
}

const IngresoEdicionHojaEnfermeria = (accion) => {

    if (accion != undefined && accion === "INGRESO") {

        IngresoNuevaHojaEnfermeria(sesion.ID_HOSPITALIZACION)
        deleteSession('ID_HOSPITALIZACION')
        deleteSession('ACCION')

    } else if (accion !== undefined && accion === "EDITAR") {

        editarHojaEnfermeria(sesion.ID_HOJA_ENFERMERIA)
        deleteSession('ID_HOJA_ENFERMERIA')
        deleteSession('ACCION')

    }
    else {
        return false
    }
}

$("#btnCancelarHojaEnfermeria").click(function (e) {
    e.preventDefault()
    window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`
})

$("#btnCancelarModalCuidadoEnfermeria").click(function (e) {
    e.preventDefault()
    $("#mdlHorariosPlanCuidadoEnfermeria").modal("hide")
})

const inicializarCombo = () => {
    cargarComboTipoPlano()
    cargarComboTipoExtremidad();
    cargarComboTipoInvasivo()
    comboPeso()
    comboTalla()
    comboTipoHojaEnfermeria()
    comboTurno()
    comboTipoExtremidad()
    comboTipoPlano()
    comboActividadTipoCuidado()
    comboCantidadTipoCuidado()
    
}

function vigilancia() {
    let Paciente = $("#txtnombrePac").val() + " " + $("#txtApePat").val() + " " + $("#txtApeMat").val();
    let Cama = $("#camaHosp").text();
    let idHoja;
    sesion.ACCION == undefined || sesion.ACCION == null ? idHoja = 0 : idHoja = sesion.ID_HOJA_ENFERMERIA;
    let data = {
        id: sesion.ID_HOJA_ENFERMERIA,
        id_hos: session.ID_HOSPITALIZACION,
        cama: Cama,
        paciente: Paciente,
    }

    cargaInicialVigilanciaIAAS(data);
    //Tabla Vigilancias IAAS por hospitalización
    crearTablaVigilanciaIAAS(data.id);
    let hoy = GetFechaActual();
    let ayer = moment(hoy).subtract(1, 'days').format("YYYY-MM-DD");
    let mañana = moment(hoy).add(1, 'days').format("YYYY-MM-DD");
    $("#dtFechaVigilancia").prop("min", ayer);
    $("#dtFechaVigilancia").prop("max", mañana);
    $('#btnGuardarVigilancias').data("id-hoja-enfermeria", data.id);
    $('#btnGuardarVigilancias').data("id-hospitalizacion", data.id_hos);
}

function cargaInicialVigilanciaIAAS(json) {
    $('#numHojaEnfermeriaVigilanciaIAAS').html(json.id);
    $('#numHospitalizacionVigilanciaIAAS').html(json.id_hos);
    $('#numCamaModalVigilanciaIAAS').html(json.cama);
    $('#nomPacModalVigilanciaIAAS').html(json.paciente);
}

function crearTablaVigilanciaIAAS(id) {
    let array = [];
    $("#tblVigilanciaIAAS").addClass("nowrap").DataTable({
        "pageLength": 5,
        "order": [[0, "desc"]],
        data: array,
        columns: [
            { title: "id" },
            { title: "Invasivo" },
            { title: "Extremidad" },
            { title: "Plano" },
            { title: "Fecha" },
            { title: "Observaciones" },
            { title: "Acciones", class: "text-center" },
        ],
        columnDefs: [
            { targets: 0, visible: false },
            {
                targets: -1,
                orderable: false,
                data: null,
                render: function (data, type, row, meta) {
                    //La variable data es la fila actual
                    var fila = meta.row;
                    var botones = `
                        <a id='linkEliminarVigilancia' data-id='${data[fila]}' class='btn btn-danger btn-circle btn-lg' href='#/'
                            onclick='eliminarVigilancia(this); return false;' data-toggle="tooltip"
                            data-placement="left" title="Anular">
                            <i class="fas fa-trash-alt"></i>
                        </a><br>
                    `;
                    return botones;
                }
            }
        ],
        "bDestroy": true
    });
}

function cargarTablaVigilanciaIAAS(id) {
    let array = [];
    //Hacer llamada AJAX llenar Array
    let output = [];
    array.forEach((row) => {
        //Relleno por si faltan llaves o vienen null
        let Object = new Proxy(row, handler);
        output.push([
            Object.IdHospitalizacion,
            Object.NombreInvasivo,
            Object.NombreExtremidad,
            Object.NombrePlano,
            formatDate(Object.Fecha),
            Object.Hora
        ]);
    });
}

function agregarVigilancia() {
    if (validarCampos("#cuerpoModalVigilancia")) {
        let id = $("#sltTipoInvasivoVigilancia").val();
        if (!(arregloVigilancias.find(x => x.IdTipoInvasivo == id))) {
            let table = $('#tblVigilanciaIAAS').DataTable();
            let inputs = obtenerInputsVigilancia();
            let rowArray = [inputs.arreglo];
            toastr.success("Elemento agregado")
            arregloVigilancias.push(inputs.objeto);
            table.rows.add(rowArray).draw();
        } else {
            toastr.error("Elemento ya ha sido agregado a la lista.")
        }
    }

}

function eliminarVigilancia(element) {
    let table = $('#tblVigilanciaIAAS').DataTable();
    let rIndex = $(element).parents('tr').prevAll().length;
    arregloVigilancias.splice(rIndex, 1);
    table.row(rIndex).remove().draw();
    toastr.warning("Elemento eliminado de la lista")
}

function obtenerInputsVigilancia() {

    let idHoja;
    sesion.ID_HOJA_ENFERMERIA == undefined || sesion.ID_HOJA_ENFERMERIA == null ? idHoja = 0 : idHoja = sesion.ID_HOJA_ENFERMERIA;
    let fechaHora = moment($("#dtFechaVigilancia").val() + " " + $("#tmHoraVigilancia").val()).format("YYYY-MM-DD HH:mm");
    return {
        arreglo: [
            idHoja,
            $("#sltTipoInvasivoVigilancia :selected").text(),
            $("#sltTipoExtremidadVigilancia :selected").text(),
            $("#sltTipoPlanoVigilancia :selected").text(),
            fechaHora,
            $("#txtObservacionesVigilancia").val()

        ],
        //Cambiar llaves y ver Fecha
        objeto: {
            FechaCreacion: fechaHora,
            IdHojaEnfermeriaCrea: idHoja,
            IdHojaEnfermeriaInvasivo: null,
            IdTipoExtremidad: $("#sltTipoExtremidadVigilancia").val(),
            IdTipoInvasivo: $("#sltTipoInvasivoVigilancia").val(),
            IdTipoPlano: $("#sltTipoPlanoVigilancia").val() == "0" ? null : $("#sltTipoPlanoVigilancia").val(),
            Observacion: $("#txtObservacionesVigilancia").val(),
        }
    };
}

function remapearVigilancia(pObject) {
    return {
        IdHojaEnfermeria: pObject.IdHojaEnfermeria,
        IdTipoInvasivo: pObject.IdHojaEnfermeria,
        IdTipoExtremidad: pObject.IdHojaEnfermeria,
        IdTipoPlano: pObject.IdHojaEnfermeria,
        Fecha: pObject.IdHojaEnfermeria,
        Hora: pObject.IdHojaEnfermeria,
        Observaciones: pObject.IdHojaEnfermeria,
    }
}
function agregarHidricos() {
    //divInputHidricos
    if (validarCampos("#divInputHidricos", false)) {
        let Tipo = $("#selectHidrico").find('option:selected').data("tipo")
        let tabla
        if (Tipo == "INGRESO") {
            tabla = "#dataTableIngresoHidrico"
        } else {
            tabla = "#dataTableEgresoHidrico"
        }
        let idHidrico = $("#selectHidrico").val()
        let nombreHidrico = $("#selectHidrico").find('option:selected').text()
        let valorHidrico = $("#valorHidrico").val()
        if (Tipo == "INGRESO") {
            IngresosHidricosIngresados.push({
                IdTipoBalance: parseInt(idHidrico),
                Balance: parseFloat(valorHidrico)
            })
        } else {
            EgresosHidricosIngresados.push({
                IdTipoBalance: parseInt(idHidrico),
                Balance: parseFloat(valorHidrico)
            })
        }
        
        let table = $(tabla).DataTable();
        let rowArray = [[idHidrico,nombreHidrico, valorHidrico]];
        table.rows.add(rowArray).draw();
        toastr.success("Agregado al listado")
    }
}
function tableHidricos() {
    let array = [];
    let dataTables = ["#dataTableIngresoHidrico", "#dataTableEgresoHidrico"]
    dataTables.map(i => {
        $(`${i}`).addClass("nowrap").DataTable({
            "pageLength": 5,
            "order": [[0, "desc"]],
            data: array,
            columns: [
                { title: "Id" },
                { title: "Nombre" },
                { title: "Valor" },
                { title: "Acciones", class: "text-center" },
            ],
            columnDefs: [
                { targets: 0, visible: false },
                {
                    targets: -1,
                    orderable: false,
                    data: null,
                    render: function (data, type, row, meta) {
                        //La variable data es la fila actual
                        var fila = meta.row;
                        var botones = `
                        <a id='linkEliminarHidrico' data-id='${data[fila]}' class='btn btn-danger btn-circle btn-lg' href='#/'
                            onclick='eliminarHidrico(this, ${data[0]}); return false;' data-table=${i} data-tipo="${i=="#dataTableIngresoHidrico"?"INGRESO":"EGRESO"}" data-toggle="tooltip"
                            data-placement="left" title="Anular">
                            <i class="fas fa-trash-alt"></i>
                        </a><br>
                    `;
                        return botones;
                    }
                }
            ],
            "bDestroy": true
        });
    })

}
function eliminarHidrico(element, id) {
    let table = $(element).data("table")
    let tipo = $(element).data("tipo")
    table = $(table).DataTable()
    let rIndex = $(element).parents('tr').prevAll().length;
    
    if (tipo == "INGRESO") {
        IngresosHidricosIngresados.splice(rIndex, 1)
    } else {
        EgresosHidricosIngresados.splice(rIndex, 1)
    }
    table.row($(element).parents('tr')).remove().draw();
    //table.row(rIndex).remove().draw();
    toastr.warning("Elemento eliminado de la lista")
}



