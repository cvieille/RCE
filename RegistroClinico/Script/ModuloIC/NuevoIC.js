﻿
var sSession = null;
var ic = {}, icGes = {};
var alertFlag = 0;

var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
}

//Javier
function cargarCombos() {
    CargarComboEstablecimiento();
    CargarComboEspecialidad();
    CargarComboTipointerconsulta();
    CargarComboServicioSalud();
    CargarComboGrupoPatologiaGES();
}

function CargarComboServicioSalud() {
    let url = `${GetWebApiUrl()}GEN_Servicio_Salud/Combo`;
    setCargarDataEnCombo(url, false, $("#sltServicioSaludOrigen"));
    setCargarDataEnCombo(url, false, $("#sltServicioSaludDestino"));
}
function CargarComboEstablecimiento() {
    let url = `${GetWebApiUrl()}GEN_Establecimiento/Combo`;
    setCargarDataEnCombo(url, false, $("#sltEstablecimientoOrigen"));
    setCargarDataEnCombo(url, false, $("#sltEstablecimientoDestino"));
}
function CargarComboEspecialidad() {
    let url = `${GetWebApiUrl()}GEN_Especialidad/Combo`;
    setCargarDataEnCombo(url, false, $("#sltEspecialidadOrigen"));
    setCargarDataEnCombo(url, false, $("#sltEspecialidadDestino"));

}
function CargarComboTipointerconsulta() { //REVISAR No ID o Valor
    let url = `${GetWebApiUrl()}RCE_Tipo_Interconsulta/Combo`;
    setCargarDataEnCombo(url, false, $("#sltTipoInterconsulta"));
}
function CargarComboGrupoPatologiaGES() { //REVISAR No ID o Valor
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/Combo`;
    setCargarDataEnCombo(url, false, $("#sltGrupoGes"));
}
function CargarComboAugeHijo(idPadre) {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/${idPadre}/Combo`;
    setCargarDataEnCombo(url, false, $("#sltSubGrupoGes"));
}

$('#sltGrupoGes').change(function () {
    idPadre = $('#sltGrupoGes').val()
    CargarComboAugeHijo(idPadre)

    select = document.querySelector("#sltSubGrupoGes");
    //console.log(select.options.length)
    if (select.options.length == 1) { //No tiene valores 
        $('#divSubGruposGes').hide()
    } else {
        $('#divSubGruposGes').show()
    }
});


$(document).ready(function () {

    cargarCombos();

    sSession = getSession();
    RevisarAcceso(true, sSession);
    window.addEventListener('beforeunload', bunload, false);


            //Al scrollear hacia abajo se muestra el alert de Campos Obligatorios
            $(window).scroll(function () {
                if (alertFlag) {
                    ($($(this)).scrollTop() > 150) ? $('#alertObligatorios').stop().show('fast') : $('#alertObligatorios').stop().hide('fast');
                }
            });

            $("#divOtrosGes, #divSubGruposGes").hide(); //Exc Tipo interconsulta == "Otro"
            $("#chbAuge").bootstrapSwitch();
            $("#divAuge").hide();
            $('#chbAuge').on('switchChange.bootstrapSwitch', function (event, state) {
                (state) ? $('#divAuge').stop().fadeIn(500) : $('#divAuge').stop().fadeOut(500);
                DarRequiredAuge();
            });

            $("#sltTipoInterconsulta").on('change', function () {
                ($(this).val() == 4) ? $("#divOtrosGes").fadeIn(500) : $("#divOtrosGes").fadeOut(500);
                $("#txtOtrosGes").attr("data-required", ($(this).val() == 4).toString());
                $("#txtOtrosGes").val('');
            });

            var idPro;
            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
                async: false,
                success: function (data) {
                    idPro = data[0].GEN_idProfesional;
                    //console.log(data);
                }
            });

            CargarUsuario(idPro);
   
            if (sSession.CODIGO_PERFIL == '10') {
                $('#divEnviado select').attr('disabled', true).selectpicker('refresh');
                $('#divDatosPaciente select').attr('disabled', true).selectpicker('refresh');
                $('#divDatosPaciente input').attr('disabled', true);
                $('#divDerivado select').attr('disabled', true).selectpicker('refresh');
                $('#divDatosClinicos textarea').attr('disabled', true);
                $('#divDatosClinicos input').bootstrapSwitch('disabled', true);
                $('#divCargarArchivos').hide();
                $('#aGuardarIC').html('<i class="fa fa-reply"></i> Responder');
                $('#divRespuesta').show();
                textboxio.replaceAll('#txtRespuesta',
                    {
                        paste: { style: 'clean' },
                        images: { allowLocal: false }
                    });
                $("html, body").animate({ scrollTop: document.body.scrollHeight }, 'slow');
            }

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": function () {
                    SalirIC();
                },
                "showDuration": 150,
                "hideDuration": 150,
                "timeOut": 3500,
                "extendedTimeOut": 2500,
                "showEasing": "swing",
                "hideEasing": "swing",
                "showMethod": "slideDown",
                "hideMethod": "slideUp"
            }

            deleteSession('ID_INTERCONSULTA');
            ShowModalCargando(false);

});

function CargarUsuario(idPro) {

    if (sSession.CODIGO_PERFIL == '10' && sSession.ID_INTERCONSULTA == undefined) {
        quitarListener();
        window.location.replace(ObtenerHost() + "/Vista/ModuloIC/BandejaIC.aspx");
    }

    if (sSession.ID_EVENTO != undefined) {
        ic.RCE_idEventos = parseInt(sSession.ID_EVENTO);
        deleteSession("ID_EVENTO");
    }

    if (sSession.ID_PACIENTE != undefined) {
        console.log("Cargando Paciente")
        CargarPaciente(sSession.ID_PACIENTE);
        BloquearPaciente();
        CargarProfesional(idPro);
        deleteSession("ID_PACIENTE");
    } else if (sSession.ID_INTERCONSULTA != undefined) {  //Editando
        console.log("Cargando IC")
        CargarIC();
    }else {
        console.log("Cargando Profesional")
        CargarProfesional(idPro);
    }
        

    

}

function DarRequiredAuge() {

    if ($("#chbAuge").bootstrapSwitch('state')) {
        $("#sltSubGrupoGes").attr("data-required", ($("#sltSubGrupoGes").children("option").length < 0).toString())
        $("#txtOtrosGes").attr("data-required", ($("#txtOtrosGes").val() == 4).toString());
        $("#txtFundamentosDiagnosticoGes, #txtTratamientosGes, #sltTipoInterconsulta, " +
            "#sltGrupoGes").attr("data-required", "true");
        $("#txtSintomalogia, #txtSolicitante").attr("data-required", "false");
    } else {
        $("#txtFundamentosDiagnosticoGes, #txtTratamientosGes, #sltTipoInterconsulta, " +
            "#sltGrupoGes, #txtOtrosGes, #sltSubGrupoGes").attr("data-required", "false");
        $("#txtSintomalogia, #txtSolicitante").attr("data-required", "true");
    }

    ReiniciarRequired();

}

function CargarProfesional(idProfesional) {
    
    try {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "GEN_Profesional/" + idProfesional,
            async: false,
            success: function (data, status, jqXHR) {
                //console.log(data);
                
                //GetEvento().GEN_idProfesional = parseInt(idProfesional);
                ic.GEN_idProfesional = data.GEN_idProfesional;
                $("#txtNumeroDocumentoProfesional").val(data.GEN_rutProfesional +
                    (data.GEN_digitoProfesional !== null ? '-' + data.GEN_digitoProfesional : ''));
                $("#txtNombreProfesional").val(data.GEN_nombreProfesional);
                $("#txtApePatProfesional").val(data.GEN_apellidoProfesional);
                $("#txtApeMatProfesional").val(data.GEN_sapellidoProfesional);

            },
            error: function (jqXHR, status) {
                console.log("Error al cargar Profesional: " + jqXHR.responseText);
            }
        });

    } catch (err) {
        alert(err.message);
    }

}

function CargarIC() {

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Interconsulta/" + sSession.ID_INTERCONSULTA,
        async: false,
        success: function (data, status, jqXHR) {

            CargarJsonICNuevo(data[0]);
            

            //console.log(data[0])
            // SERVICIO Y ESPECIALIDAD ORIGEN
            $("#sltServicioSaludOrigen").val(1);
            $('#sltServicioSaludOrigen').prop('disabled', 'disabled');
            $("#sltEstablecimientoOrigen").val(1); 
            $('#sltEstablecimientoOrigen').prop('disabled', 'disabled');

            $("#sltEspecialidadOrigen").val(ic.GEN_idEspecialidad_origenInterconsulta); 

            // SERVICIO Y ESPECIALIDAD DESTINO
            $("#sltServicioSaludDestino").val(1);
            $('#sltServicioSaludDestino').prop('disabled', 'disabled');
            $("#sltEstablecimientoDestino").val(1);
            $('#sltEstablecimientoDestino').prop('disabled', 'disabled');

            $("#sltEspecialidadDestino").val(ic.GEN_idEspecialidad_destinoInterconsulta);
            
            // DATOS CLINICOS
            $("#txtDiagnostico").val($.trim(ic.RCE_diagnosticoInterconsulta));
            $("#txtSintomalogia").val($.trim(ic.RCE_sintomatologiaInterconsulta));
            $("#txtSolicitante").val($.trim(ic.RCE_solicitudInterconsulta));
            $("#strFechaCreacion").text(moment(ic.RCE_fechaInterconsulta).format("DD-MM-YYYY"));
            //GetEvento().RCE_idEventos = ic.RCE_idEventos;
            //GetEvento().GEN_idProfesional = ic.GEN_idProfesional;
            //$scope.pacienteControl.idPaciente = ic.GEN_idPaciente;
            CargarPacienteActualizado(data[0].Paciente.IdPaciente);
            //CargarProfesional(ic.GEN_idProfesional);
            cargarArchivosExistentes(ic.RCE_idInterconsulta, 'IC');

            // PROFESIONAL
            $("#txtNumeroDocumentoProfesional").val(data[0].Profesional.NumeroDocumento);
            $("#txtNombreProfesional").val(data[0].Profesional.Nombre);
            $("#txtApePatProfesional").val(data[0].Profesional.ApellidoPaterno);
            $("#txtApeMatProfesional").val(data[0].Profesional.ApellidoMaterno);

            let arregloInterconsultaGES = Object.values(data[0].InterconsultaGES);
            if (arregloInterconsultaGES.length > 0)
               $("#chbAuge").bootstrapSwitch('state', true);
               $("#divAuge").show();
               CargarICGes(data[0]); 
                //console.log(data[0])
            // RESPUESTA
            if (sSession.CODIGO_PERFIL == '10') {//interconsultor
            
                var jRespuesta = data[0].RESPUESTAS;
                if (!$.isEmptyObject(jRespuesta)) {
                    ic.RESPUESTAS = jRespuesta;
                    textboxio.replace("#txtRespuesta").content.set(ic.RESPUESTAS[0].RCE_respuestaInterconsulta_Respuesta);
                }

            }

        },
        error: function (jqXHR, status){
            console.log("Error al cargar IC: " + jqXHR.responseText);
        }
    });

    //Desactivar campos no editables
    $('#sltIdentificacion').prop('disabled', 'disabled');
    $('#txtnumerotPac').prop('disabled', 'disabled');
    $('#txtDigitoPac').prop('disabled', 'disabled');
}

function CargarICGes(json) {
    
    //ESTE JSON SE UTILIZA EN CASO DE TENER QUE INACTIVAR LA INTERCONSULTA GES 

    CargarJsonICGesNuevo(json);
    


    var patologiaPadre = json.InterconsultaGES;

    if (patologiaPadre.PatologiaPadre != null) {
        var idGrupoGes = patologiaPadre.PatologiaPadre.IdPatologiaPadre
        var idSubGrupoGes = patologiaPadre.IdPatologia;
    }else {
        var idGrupoGes = patologiaPadre.IdPatologia;
    }
    
    $("#sltTipoInterconsulta").val(json.InterconsultaGES.IdTipoInterconsulta);
    $("#sltGrupoGes").val(idGrupoGes);
    if (idSubGrupoGes) {
        $("#divSubGruposGes").show()
        idPadre = $('#sltGrupoGes').val()
        CargarComboAugeHijo(idPadre)
        $("#sltSubGrupoGes").val(idSubGrupoGes) //Javier Revisando
    }
    $("#txtFundamentosDiagnosticoGes").val($.trim(json.InterconsultaGES.FundamentosDiagnostico));
    $("#txtTratamientosGes").val($.trim(json.InterconsultaGES.Tratamiento));

    if (json.InterconsultaGES.Otros != null) {
        $("#txtOtrosGes").val($.trim(json.InterconsultaGES.Otros));
        $("#divOtrosGes").show()
    }

    DarRequiredAuge();

}

function GuardarIC() {

    CargarJsonIC();
    
    var method = (ic.RCE_idInterconsulta != undefined) ? 'PUT' : 'POST';
    var url = GetWebApiUrl() + "RCE_Interconsulta" + ((method === 'PUT') ? '/' + ic.RCE_idInterconsulta : '/GEN_Paciente/' + GetPaciente().GEN_idPaciente);
    
    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(ic),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {

            

            ic.RCE_idInterconsulta = (ic.RCE_idInterconsulta == undefined) ?
                data.RCE_idInterconsulta : ic.RCE_idInterconsulta;
            if ($("#chbAuge").bootstrapSwitch('state'))
                GuardarICGes(); 
            else if (!$("#chbAuge").bootstrapSwitch('state') && icGes.RCE_idInterconsulta_GES != undefined) {
                icGes.RCE_estadoInterconsulta_GES = "Inactivo"
                GuardarICGes();   
            }else 
                SubirTodosArchivos(ic.RCE_idInterconsulta, 'IC', null);

            toastr.success('Interconsulta creada Exitosamente.', '', {
                onHidden: function () {
                    SalirIC();
                }
            });
            //toastr.success('Interconsulta creada Exitosamente.');
        },
        error: function (jqXHR, status) {
            alert("Error al guardar Interconsulta: " + jqXHR.responseText);
        }
    });
}

function GuardarICGes() { 
    
    if (icGes.RCE_estadoInterconsulta_GES !== "Inactivo") 
        CargarJsonICGes(); 
    var method = (icGes.RCE_idInterconsulta_GES != undefined) ? 'PUT' : 'POST';
    var url = GetWebApiUrl() + "RCE_Interconsulta_Ges" + ((method === 'PUT') ? '/' + icGes.RCE_idInterconsulta_GES : '');
    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(icGes),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            SubirTodosArchivos(ic.RCE_idInterconsulta, 'IC', null);
            toastr.success('Interconsulta GES creada Exitosamente.', '', {
                onHidden: function () {
                    SalirIC();
                }
            });
            //toastr.success('Interconsulta GES creada Exitosamente.');
        },
        error: function (jqXHR, status) {
            console.log("Error al guardar Interconsulta GES: " + jqXHR.message);
        }
    });
    
}

function CerrarIC()
{
    CargarJsonIC();
    var url = GetWebApiUrl() + 'RCE_Interconsulta/' + ic.RCE_idInterconsulta;
    ic.GEN_idTipo_Estados_Sistemas = 76;
    $.ajax({
        type: 'PUT',
        url: url,
        data: JSON.stringify(ic),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            quitarListener();
            window.location.replace(ObtenerHost() + "/Vista/ModuloIC/BandejaIC.aspx");
        },
        error: function (jqXHR, status) {
            console.log("Error al cerrar IC: " + jqXHR.message);
            $('#modalAlerta').modal('hide');
        }
    });
}

function BorradorRespuesta()
{
    quitarListener();
    window.location.replace(ObtenerHost() + "/Vista/ModuloIC/BandejaIC.aspx");
}

function CrearInterconsulta() {
    if (sSession.CODIGO_PERFIL == '10') //interconsultor
    {
        var json = {
            RCE_idInterconsulta: sSession.ID_INTERCONSULTA,
            RCE_respuestaInterconsulta_Respuesta: textboxio.replace('#txtRespuesta').content.get(),
            RCE_estadoInterconsulta_Respuesta: 'Activo'
        };

        var sMetodo = 'PUT';
        if ($.isEmptyObject(ic.RESPUESTAS))
            sMetodo = 'POST';
        else
            json.RCE_idInterconsulta_Respuesta = ic.RESPUESTAS[0].RCE_idInterconsulta_Respuesta;

        var url = GetWebApiUrl() + "RCE_Interconsulta_Respuesta" + ((sMetodo === 'PUT') ? '/' + ic.RESPUESTAS[0].RCE_idInterconsulta_Respuesta : '');
        
        $.ajax({
            type: sMetodo,
            url: url,
            data: JSON.stringify(json),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, status, jqXHR) {
                ShowModalAlerta('CONFIRMACION',
                    'Respuesta guardada correctamente',
                    '¿Quiere cerrar esta Interconsulta?<br/>La respuesta podrá ser modificada mas adelante si no la cierra.',
                    CerrarIC, BorradorRespuesta, 'CERRAR IC', 'BORRADOR');
            },
            error: function (jqXHR, status) {
                console.log("Error al guardar Respuesta: " + jqXHR.message);
            }
        });
    }
    else {

        var auge = $("#chbAuge").bootstrapSwitch('state');
        //Validación de campos vacios y otras
        if ((!auge || (auge && validarCampos("#divICGes", true)))
            & (validarCampos("#divDatosClinicos", true)
                & validarCampos("#divDerivado", true)
                & validarCampos("#divDatosPaciente", true)
                & validarCampos("#divEnviado", true)))
        {
            //Validación de formatos en campos de telefono y correo
            var flag = 0;

            if (!$("#txtTelefono").val().match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im)) {
                $("#txtTelefono").addClass('invalid-input');
                flag = 1;
            } else {
                $("#txtTelefono").removeClass('invalid-input');
            }

            if (!$("#txtOtroTelefono").val().match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im)) {
                $("#txtOtroTelefono").addClass('invalid-input');
                flag = 1;
            } else {
                $("#txtOtroTelefono").removeClass('invalid-input');
            }

            if (!$("#txtCorreoElectronico").val().match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)) {
                $("#txtCorreoElectronico").addClass('invalid-input');
                flag = 1;
            } else {
                $("#txtCorreoElectronico").removeClass('invalid-input');
            }

            if (flag == 1)
                return;

            //Se continua con el guardado de la IC
            $('#aGuardarIC').addClass('disabled');
            $('#aGuardarIC').attr('disabled', true);
            $('#aCancelarInterconsulta').addClass('disabled');
            $('#aCancelarInterconsulta').attr('disabled', true);
            GuardarPaciente();
            //if (ic.RCE_idEventos != undefined || GetEvento().RCE_idEventos != undefined) {
            GuardarIC();
            //} else {
            //    ShowModalAlerta("ADVERTENCIA",
            //        "No existe evento asociado",
            //        "La hoja de Evolución debe tener un evento asociado. ", null);
            //    ShowPacienteEventos(GetEvento().GEN_idPaciente);
            //}
        } else {
            alertFlag = 1;
            return;
            
        }
    }
}

function CancelarIC() {
    ShowModalAlerta('CONFIRMACION',
        'Salir de la Interconsulta',
        '¿Está seguro que desea cancelar el registro?',
        SalirIC);
}

function SalirIC() {
    quitarListener();
    location.href = ObtenerHost() + "/Vista/ModuloIC/BandejaIC.aspx";
}


//Formatear Json
function CargarJsonICNuevo(json) { //Pasar a Variables JSON
    ic.RCE_idInterconsulta = json.IdInterconsulta;
    ic.GEN_idEspecialidad_origenInterconsulta = json.IdEspecialidadOrigen;
    ic.GEN_idEspecialidad_destinoInterconsulta = json.IdEspecialidadDestino;
    ic.RCE_diagnosticoInterconsulta = json.Diagnostico;//Rev
    ic.RCE_sintomatologiaInterconsulta = json.Sintomatologia;
    ic.RCE_solicitudInterconsulta = json.Solicitud;
    ic.GEN_idProfesional = json.Profesional.Id;
    ic.RCE_idEventos = json.IdEvento;
    ic.RCE_estadoInterconsulta = json.DescripcionEstado;
    ic.RCE_fechaInterconsulta = json.Fecha;
    ic.GEN_idTipo_Estados_Sistemas = json.IdEstado;
    ic.RCE_fechaInterconsulta = json.Fecha
    if (json.InterconsultaGES)
        ic.RCE_idInterconsultaGES = json.InterconsultaGES.IdInterconsultaGES;
}

function CargarJsonIC() {
    
    ic.GEN_idEstablecimientoorigenInterconsulta = parseInt(valCampo($("#sltEstablecimientoOrigen").val()));
    ic.GEN_idEstablecimientodestinoInterconsulta = parseInt(valCampo($("#sltEstablecimientoDestino").val()));
    ic.GEN_idEspecialidad_origenInterconsulta = parseInt(valCampo($("#sltEspecialidadOrigen").val()));
    ic.GEN_idEspecialidad_destinoInterconsulta = parseInt(valCampo($("#sltEspecialidadDestino").val()));
    ic.RCE_diagnosticoInterconsulta = valCampo($("#txtDiagnostico").val());
    ic.RCE_sintomatologiaInterconsulta = valCampo($("#txtSintomalogia").val());
    ic.RCE_solicitudInterconsulta = valCampo($("#txtSolicitante").val());
    ic.RCE_estadoInterconsulta = "Activo";
    // 75 = IC Ingresada
    ic.GEN_idTipo_Estados_Sistemas = 75;

    if (sSession.ID_INTERCONSULTA == undefined) {
        ic.RCE_idEventos = null;
        //ic.RCE_idEventos = 1;
        ic.RCE_fechaInterconsulta = moment(GetFechaActual()).format("YYYY-MM-DD HH:mm:ss");
    }

}

function CargarJsonICGesNuevo(json) {
    icGes.RCE_idTipo_Interconsulta = json.RCE_idTipo_Interconsulta;
    icGes.RCE_fundamentos_diagnosticoInterconsulta_GES = json.RCE_fundamentos_diagnosticoInterconsulta_GES;
    icGes.RCE_tratamientoInterconsulta_GES = json.RCE_tratamientoInterconsulta_GES;
    icGes.RCE_estadoInterconsulta_GES = json.RCE_estadoInterconsulta_GES;
    icGes.RCE_idInterconsulta = json.RCE_idInterconsulta;
    icGes.RCE_idPatologia_GES = json.InterconsultaGES.IdInterconsultaGES;
    icGes.RCE_otros_tipoInterconsulta_GES = json.RCE_otros_tipoInterconsulta_GES;
    icGes.RCE_idInterconsulta_GES = json.RCE_idInterconsulta_GES;

}

function CargarJsonICGes() {
    icGes.RCE_idTipo_Interconsulta = valCampo($("#sltTipoInterconsulta").val());
    icGes.RCE_fundamentos_diagnosticoInterconsulta_GES = valCampo($.trim($("#txtFundamentosDiagnosticoGes").val()));
    icGes.RCE_tratamientoInterconsulta_GES = valCampo($.trim($("#txtTratamientosGes").val()));
    icGes.RCE_estadoInterconsulta_GES = "Activo";
    icGes.RCE_idPatologia_GES = ($("#sltSubGrupoGes").children("option").length == 1) ? $("#sltGrupoGes").val() : $("#sltSubGrupoGes").val(); //Revisar
    icGes.RCE_otros_tipoInterconsulta_GES = valCampo($.trim($("#txtOtrosGes").val()));
    if (icGes.RCE_idInterconsulta == undefined)
        icGes.RCE_idInterconsulta = ic.RCE_idInterconsulta;
    if (icGes.RCE_idInterconsulta_GES == undefined)
        icGes.RCE_idInterconsulta_GES = ic.RCE_idInterconsultaGES;
}
/*
function CargarCombosServiciosSalud() {

    $("#sltServicioSaludOrigen, #sltServicioSaludDestino").empty();

    try {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "GEN_Servicio_Salud/Combo",
            async: false,
            success: function (data, status, jqXHR) {

                $.each(data, function (key, val) {
                    $("#sltServicioSaludOrigen, #sltServicioSaludDestino").append(
                        "<option value='" + val.Id + "'>" +
                            val.Valor +
                        "</option>");
                });

                $("#sltServicioSaludOrigen, #sltServicioSaludDestino").val('1');

            },
            error: function (jqXHR, status) {
                console.log("Error al llenar Servicio de Salud: " + jqXHR.responseText);
            }
        });

    } catch (err) {
        alert(err.message);
    }

}




function CargarCombosEstablecimientos(de) {

    let url = `${GetWebApiUrl()}GEN_Establecimiento/GEN_idServicio_Salud/`;
    let idTag = "#sltEstablecimientoOrigen";

    if (de == "ORIGEN")
        url = `${url}${$("#sltServicioSaludOrigen").val()}`;
    else if (de == "DESTINO") {
        url = `${url}${$("#sltServicioSaludDestino").val()}`;
        idTag = "#sltEstablecimientoDestino";
    }

    $(idTag).empty();

    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data, status, jqXHR) {
            $.each(data, function (key, val) {
                $(idTag).append(`<option value='${val.GEN_idEstablecimiento}'>${val.GEN_nombreEstablecimiento}</option>`);
            });
            $(idTag).val('1');
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Establecimiento: ${JSON.stringify(jqXHR)}`);
        }
    });

}

function CargarCombosEspecialidades(idPro) {

    $("#sltEspecialidadOrigen, #sltEspecialidadDestino").empty();
    $("#sltEspecialidadOrigen, #sltEspecialidadDestino").append("<option value='0'> - Seleccione - </option>");

    var sUrl = `${GetWebApiUrl()}GEN_Especialidad/Combo`;

    if (sSession.CODIGO_PERFIL == '1')
        sUrl = `${GetWebApiUrl()}GEN_Especialidad/Combo/GEN_idProfesional/${idPro}`;

    //
    var dataOrigen = null;
    $.ajax({
        type: 'GET',
        url: sUrl,
        async: false,
        success: function (data, status, jqXHR) {
            dataOrigen = data;
            $.each(data, function (key, val) {
                $("#sltEspecialidadOrigen").append(
                    "<option value='" + val.GEN_idEspecialidad + "'>" + val.GEN_nombreEspecialidad + "</option>");
            });
        },
        error: function (jqXHR, status) {
            console.log("Error al llenar Especialidad: " + jqXHR.responseText);
        }
    });
    //si el dataOrigen sigue siendo null, o no tiene nada que mostrar es por que el profesional no tiene especialidad ¿?
    //cargo todos...
    if ($.isEmptyObject(dataOrigen)) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Especialidad/Combo`,
            async: false,
            success: function (data) {
                $.each(data, function (i, r) {
                    $('#sltEspecialidadOrigen').append(`<option value='${r.GEN_idEspecialidad}'>${r.GEN_nombreEspecialidad}</option>`);
                });
                $("#sltEspecialidadOrigen").val('0');
            },
            error: function (jqXHR, status) {
                console.log(`Error al llenar Especialidad origen: ${JSON.stringify(jqXHR)}`);
            }
        });
    }

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Especialidad/Combo",
        async: false,
        success: function (data, status, jqXHR) {
            $.each(data, function (key, val) {
                $("#sltEspecialidadDestino").append(`<option value='${val.GEN_idEspecialidad}'>${val.GEN_nombreEspecialidad}</option>`);
            });
            $("#sltEspecialidadDestino").val('0');
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Especialidad destino: ${JSON.stringify(jqXHR)}`);
        }
    });
}

function CargarComboTipoIC() {

    $("#sltTipoInterconsulta").empty();
    $("#sltTipoInterconsulta").append("<option value='0'>Seleccione</option>");
   
    try {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Tipo_Interconsulta/Combo",
            async: false,
            success: function (data, status, jqXHR) {

                $.each(data, function (key, val) {
                    $("#sltTipoInterconsulta").append(
                        "<option value='" + val.RCE_idTipo_Interconsulta + "'>" +
                            val.RCE_DescripcionTipo_Interconsulta +
                        "</option>");
                });
               

            },
            error: function (jqXHR, status) {
                console.log("Error al llenar Tipo IC: " + jqXHR.responseText);
            }
        });

    } catch (err) {
        alert('Error al cargar Tipo IC: ' + response.data + ' ' + response.status + '!');
    }
   
}



function CargarComboGrupoGes() {
   
    $("#sltGrupoGes").empty();
    $("#sltGrupoGes").append("<option value='0'>Seleccione</option>");

    try {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Patologia_GES/Combo?RCE_idPatologia_GESPadre=null",
            async: false,
            success: function (data, status, jqXHR) {

                $.each(data, function (key, val) {
                    $("#sltGrupoGes").append(
                        "<option value='" + val.RCE_idPatologia_GES + "'>" +
                            val.RCE_descripcion_Patologia_GES +
                        "</option>");
                });
               
            },
            error: function (jqXHR, status) {
                console.log('Error al cargar Grupo GES: ' + jqXHR.responseText);
            }
        });

    } catch (err) {
        alert('Error al cargar Grupo GES: ' + response.data + ' ' + response.status + '!');
    }
   
}
*/
/*
function CargarSubGrupoGes(idGrupoGes, idSubGrupoGes) {

    $("#sltSubGrupoGes").empty();
    $("#sltSubGrupoGes").append("<option value='0'>Seleccione</option>");

    try {

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Patologia_GES/Combo?RCE_idPatologia_GESPadre=" + idGrupoGes,
            async: false,
            success: function (data, status, jqXHR) {

                if (!$.isEmptyObject(data) && idGrupoGes != 0) {
                    $.each(data, function (key, val) {
                        $("#sltSubGrupoGes").append(
                            "<option value='" + val.RCE_idPatologia_GES + "'>" +
                            val.RCE_descripcion_Patologia_GES +
                            "</option>");
                    });
                    $("#divSubGruposGes").fadeIn(500);
                    $("#sltSubGrupoGes").attr("data-required", "true");
                    $("#sltSubGrupoGes").val(idSubGrupoGes);
                } else {
                    $("#divSubGruposGes").fadeOut(500);
                    $("#sltSubGrupoGes").attr("data-required", "false");
                    $("#sltSubGrupoGes").empty();
                }

                ReiniciarRequired();
               
            },
            error: function (jqXHR, status) {
                console.log('Error al cargar Sub Grupo GES: ' + jqXHR.responseText);
            }
        });

    } catch (err) {
        alert('Error al cargar Sub Grupo GES: ' + response.data + ' ' + response.status + '!');
    }
   
}
*/