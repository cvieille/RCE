﻿var sSession;
var idPro;
var idDiv;
//Javier
function cargarCombos() {
    CargarComboEspecialidad();
    CargarComboGrupoPatologiaGES();
}

function CargarComboEspecialidad() {
    let url = `${GetWebApiUrl()}GEN_Especialidad/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlEspecialidadOrigen"));
    setCargarDataEnCombo(url, false, $("#ddlEspecialidadDestino"));
}
function CargarComboGrupoPatologiaGES() { 
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlGrupo"));
}
function CargarComboAugeHijo(idPadre) {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/${idPadre}/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlSubGrupo"));
}
$('#ddlGrupo').change(function () {
    idPadre = $('#ddlGrupo').val()
    CargarComboAugeHijo(idPadre)

    select = document.querySelector("#ddlSubGrupo");
    if (select.options.length == 1) { //No tiene valores 
        $('#divSubAuge').hide()
    } else {
        $('#divSubAuge').show()
    }
});

$(document).ready(function () {
    cargarCombos();
    
    sSession = getSession();
    RevisarAcceso(false, sSession);

    idDiv = "";
    
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
        async: false,
        success: function (data) {
            idPro = data[0].GEN_idProfesional;
        }
    });

    $('.select2').select2({ theme: 'bootstrap4' });
    
    cargarComboProfesional();
    //cargarComboGrupo();
    //cargarComboEspecialidadCambiar();

    DarFuncionRadios();
    getTablaIC(sSession);

    if (sSession.CODIGO_PERFIL != "1") {
        $('#lnbNuevoIC').hide();
    }

    $('#btnFiltro').on('click', function (e) {
        getTablaIC(sSession)
        e.preventDefault();
    });
    $('#btnLimpiarFiltro').on('click', function (e) {
        limpiarFiltros();
        e.preventDefault();
    });
    /*
    $("#ddlGrupo").change(function () {
        //$('button[data-id="ddlSubGrupo"]').html('Seleccione');
        cargarComboSubGrupo($(this).prop('value'));
    });
    */
    // MÉTODOS DROPDOWN//////////////////////////////////

    $('body').on('click', '.dropdown-menu a', function (e) {
        $(this).parents('.dropdown').find('.btn').text($(this).text());
        $(this).parents('.dropdown').find('.btn').attr('val', $(this).attr('val'));
        e.preventDefault();
    });

    $('body').on('click', '.dropdown', function (e) {
        if ($('.dropdown-menu', this).children().length == 0)
            e.stopPropagation();
    });

    $('#switchGES').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $('#divAuge').fadeIn(100);
           //$('#divSubAuge').fadeIn(100);
        }
        else {
            $('#divAuge').fadeOut(100);
            //$('#divSubAuge').fadeOut(100);
        }
    });

    $('#btnAnular').click(function () {
        alert($('#idIC').val());
        $.ajax({
            type: 'PUT',
            url: GetWebApiUrl() + "RCE_Interconsulta/Anular/" + $('#idIC').val(),          
            // async: false,
            success: function (data, status, jqXHR) {
                $('#idIC').val('0');
                $('#mdlRechazar').modal('hide');
                getTablaIC(sSession);
                toastr.success('InterConsulta ANULADA');
            },
            error: function (jqXHR, status) {
                console.log(jqXHR);
                alert('fail' + status.code);
            }
        });
    });

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 2000,
        "extendedTimeOut": 1000,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }

    $('#btnAceptarEsp').click(function () {
        var bValidar = validarCampos('#divEspecialidades', false);

        if (bValidar) {
            var iID = $('#iID').val();
            $.ajax({
                type: 'PUT',
                url: GetWebApiUrl() + 'RCE_Interconsulta/CambiarEspecialidad/' + iID + '/' + $('#ddlEspecialidad2').prop('value'),
                //data: JSON.stringify(json),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data, status, jqXHR) {
                    $('#iID').val('0');
                    $('#mdlEspecialidad').modal('hide');
                    getTablaIC(sSession);
                    toastr.success('Especialidad Cambiada');
                },
                error: function (jqXHR, status) {
                    console.log(jqXHR);
                    alert('fail' + status.code);
                }
            });

            //$.getJSON(GetWebApiUrl() + "RCE_Interconsulta/" + iID, function (data) {
            //    var json = data[0];
            //    console.log(json);
            //    var idEnviado = json.GEN_UbicacionSolicitante[0].GEN_idUbicacion_enviadoInterconsulta;

            //    delete json.GEN_UbicacionDestino;
            //    delete json.GEN_UbicacionSolicitante;
            //    delete json.RCE_descripcion_Patologia_GES;

            //    json.GEN_idUbicacion_destinoInterconsulta = parseInt($('#ddlEspecialidad2').prop('value'));
            //    json.GEN_idUbicacion_enviadoInterconsulta = idEnviado;

            //    $.ajax({
            //        type: 'PUT',
            //        url: GetWebApiUrl() + "RCE_Interconsulta/" + iID,
            //        data: JSON.stringify(json),
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //        async: false,
            //        success: function (data, status, jqXHR) {
            //            //alert('tamo');
            //            $('#iID').val('0');
            //            $('#mdlEspecialidad').modal('hide');
            //            getTablaIC(sSession);
            //            toastr.success('Especialidad Cambiada');
            //        },
            //        error: function (jqXHR, status) {
            //            console.log(jqXHR);
            //            alert('fail' + status.code);
            //        }
            //    });
            //});
        }
    });
    if (sSession.CODIGO_PERFIL == "10") {
        $('#divEspecialidadDestino').hide();
    }

    //sólo el médico debería crear IC?
    if (sSession.CODIGO_PERFIL == "1") {
        $('#btnNuevaIC').show();
    }

    deleteSession('ID_INTERCONSULTA');

});

function toggle(id) {

    if (idDiv == "") {

        $(id).fadeIn();
        idDiv = id;

    } else if (idDiv == id) {

        $(id).fadeOut();
        idDiv = "";

    } else {

        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }

}

/* ERROR
function cargarComboEspecialidadCambiar()
{
    $('#ddlEspecialidad1 ,#ddlEspecialidad2').empty().append("<option value='0'>-Seleccione-</option>");
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Especialidad/Combo",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, r) {
                $('#ddlEspecialidad1 ,#ddlEspecialidad2').append("<option value='" + r.GEN_idEspecialidad + "'>" + r.GEN_nombreEspecialidad + "</option>");
            });
            $('#ddlEspecialidad1 ,#ddlEspecialidad2');
        }
    });

    //$.getJSON(GetWebApiUrl() + "GEN_Especialidad/Combo", function (data) {
    //    $.each(data, function (i, r) {
    //        $('#ddlEspecialidad1 ,#ddlEspecialidad2').append("<option value='" + r.GEN_idEspecialidad + "'>" + r.GEN_nombreEspecialidad + "</option>");
    //    });
    //    $('#ddlEspecialidad1 ,#ddlEspecialidad2').selectpicker('refresh');
    //});
}
*/
function cargarComboProfesional()
{
    $('#ddlMedicoIC').empty().append("<option value='0'>-Seleccione-</option>");
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Interconsulta/Medicos",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, r) {
                $('#ddlMedicoIC').append("<option value='" + r.GEN_idProfesional + "'>" + r.GEN_nombrePersonas + "</option>");
            });

            if (idPro != null && idPro != undefined) {
                $('#ddlMedicoIC').prop('value', idPro);
            }
        }
    });
}
/*
function cargarComboGrupo() {

    $('#ddlGrupo').empty().append("<option value='0'>-Seleccione-</option>");
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Patologia_GES/Combo?RCE_idPatologia_GESPadre=null",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, r) {
                $('#ddlGrupo').append("<option value='" + r.RCE_idPatologia_GES + "'>" + r.RCE_descripcion_Patologia_GES + "</option>");
            });
            //$('#ddlGrupo').selectpicker('refresh');
        }
    });
    //$.getJSON(GetWebApiUrl() + "RCE_Patologia_GES/Combo?RCE_idPatologia_GESPadre=null", function (data) {
    //    $.each(data, function (i, r) {
    //        $('#ddlGrupo').append("<option value='" + r.RCE_idPatologia_GES + "'>" + r.RCE_descripcion_Patologia_GES + "</option>");
    //    });
    //    $('#ddlGrupo').selectpicker('refresh');
    //});    
}

function cargarComboSubGrupo(id) {

    $('#ddlSubGrupo').empty().append("<option value='0'>-Seleccione-</option>");
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Patologia_GES/Combo?RCE_idPatologia_GESPadre=" + id,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, r) {
                $('#ddlSubGrupo').append("<option value='" + r.RCE_idPatologia_GES + "'>" + r.RCE_descripcion_Patologia_GES + "</option>");
            });
            //$('#ddlSubGrupo').selectpicker('refresh');
        }
    });
    //$.getJSON(GetWebApiUrl() + "RCE_Patologia_GES/Combo?RCE_idPatologia_GESPadre=" + id, function (data) {
    //    $.each(data, function (i, r) {
    //        $('#ddlSubGrupo').append("<option value='" + r.RCE_idPatologia_GES + "'>" + r.RCE_descripcion_Patologia_GES + "</option>");
    //    });
    //    $('#ddlSubGrupo').selectpicker('refresh');
    //});
}
*/
function cargarComboEspecialidad(ddlOrigen, ddlDestino) {

    $('#' + ddlOrigen).empty().append("<option value='0'>-Seleccione-</option>");
    $('#' + ddlDestino).empty().append("<option value='0'>-Seleccione-</option>");
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Especialidad/Combo",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, r) {
                $('#' + ddlOrigen).append("<option value='" + r.GEN_idEspecialidad + "'>" + r.GEN_nombreEspecialidad + "</option>");
                $('#' + ddlDestino).append("<option value='" + r.GEN_idEspecialidad + "'>" + r.GEN_nombreEspecialidad + "</option>");
            });
            //$('#' + ddlOrigen).selectpicker('refresh');
            //$('#' + ddlDestino).selectpicker('refresh');
        }
    });
    //$.getJSON(GetWebApiUrl() + "GEN_Especialidad/Combo", function (data) {
    //    $.each(data, function (i, r) {
    //        $('#' + ddlOrigen).append("<option value='" + r.GEN_idEspecialidad + "'>" + r.GEN_nombreEspecialidad + "</option>");
    //        $('#' + ddlDestino).append("<option value='" + r.GEN_idEspecialidad + "'>" + r.GEN_nombreEspecialidad + "</option>");
    //    });
    //    $('#' + ddlOrigen).selectpicker('refresh');
    //    $('#' + ddlDestino).selectpicker('refresh');
    //});    
}

function limpiarFiltros() {

    $('#txtFiltroRut').val('');
    $('#txtFiltroNombre').val('');
    $('#txtFiltroApe').val('');
    $('#txtFiltroSApe').val('');
    //resetDropdown('#ddlMedicoIC');
    $('#ddlGrupo').val(0);
    $('#ddlSubGrupo').empty().append("<option value='0'>-Seleccione-</option>");
    $('#ddlSubGrupo').val(0);
    $('#ddlOrigen').val(0);
    $('#ddlEspecialidadOrigen').val(0).trigger('change');
    $('#ddlDestino').val(0);
    $('#ddlEspecialidadDestino').val(0).trigger('change');

    $('#switchGES').bootstrapSwitch('state', false);

    getTablaIC(sSession);

}

function getTablaIC(sSession) {

    var adataset = [];

    var filtros = {};
    
    if ($('#ddlGrupo').val() != null && $('#ddlGrupo').val() != '') {
        if ($("#ddlSubGrupo").val() != null && $('#ddlSubGrupo').val() != '')
            filtros["RCE_idPatologia_GES"] = $('#ddlSubGrupo').val();
        else 
            filtros["RCE_idPatologia_GES"] = $('#ddlGrupo').val();
    }

    if ($('#ddlEspecialidadOrigen').val() != '0')
        filtros["GEN_idEspecialidad_origenInterconsulta"] = $('#ddlEspecialidadOrigen').val();
    
    if (sSession.CODIGO_PERFIL == "10") {
        filtros["GEN_idEspecialidad_destinoInterconsulta"] = sSession.ID_ESPECIALIDAD;
    }
    else {
        if ($('#ddlEspecialidadDestino').val() != '0') 
            filtros["GEN_idEspecialidad_destinoInterconsulta"] = $('#ddlEspecialidadDestino').val();
    }
    
    if ($('#switchGES').bootstrapSwitch('state'))
        filtros["Interconsulta_GES"] = 1;
    else
        filtros["Interconsulta_GES"] = 0;

    if ($('#txtFiltroRut').val() != '0')
        filtros["GEN_numero_documentoPaciente"] = $('#txtFiltroRut').val();
    if ($('#txtFiltroNombre').val() != '0')
        filtros["GEN_nombrePaciente"] = $('#txtFiltroNombre').val();
    if ($('#txtFiltroApe').val() != '0')
        filtros["GEN_ape_paternoPaciente"] = $('#txtFiltroApe').val();
    if ($('#txtFiltroSApe').val() != '0')
        filtros["GEN_ape_maternoPaciente"] = $('#txtFiltroSApe').val();

    //console.log("Qué se pide:" + JSON.stringify(filtros));
    //console.log("Donde:" + GetWebApiUrl() + "RCE_Interconsulta/Bandeja");
   

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_Interconsulta/Bandeja?`+
            `idPatologiaGES=${filtros["RCE_idPatologia_GES"]}`+
            `&idEstado=Activo`+
            `&idEspecialidadDestino=${filtros["GEN_idEspecialidad_destinoInterconsulta"]}`+
            `&idEspecialidadOrigen=${filtros["GEN_idEspecialidad_origenInterconsulta"]}`+
            `&interconsultaGES=${filtros["Interconsulta_GES"]}`+
            `&numeroDocumento=${filtros["GEN_numero_documentoPaciente"]}`+
            `&nombre=${filtros["GEN_nombrePaciente"]}`+
            `&apellidoPaterno=${filtros["GEN_ape_paternoPaciente"]}`+
            `&apellidoMaterno=${filtros["GEN_ape_maternoPaciente"]}`,
        contentType: "application/json",
        dataType: "json",
        //data: JSON.stringify(filtros),
        async: false,
        success: function (data) {
            console.log(data)
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    moment(moment(val.Fecha).toDate()).format('DD-MM-YYYY'),
                    val.NombrePaciente + ' ' + val.PrimerApellidoPaciente + ' ' + val.SegundoApellidoPaciente,
                    val.DescripcionEstado,
                    val.IdEstado,
                    val.IdPaciente,
                    val.IdEvento,
                    val.IdProfesional,
                    val.IdEspecialidadDestino,
                    val.RespuestasInterconsultasCount
                ]);
            });
            //Aca deberia ir el id de la hospitalizacion
            LlenaGrillaIC(adataset, '#tblIC', sSession);
        }
    });

    //console.log("Pasó la solicitud");
}

function linkMovimientos(e) {
    var id = $(e).data("id");
    setSession('ID_INTERCONSULTA', id);
    var adataset = [];
    var id = $(e).data("id");
    $.ajax({
        type: "GET",
        url: GetWebApiUrl() + "RCE_Movimientos_Interconsulta/RCE_idInterconsulta/" + id,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $.each(data, function (i, r) {
                adataset.push([
                    r.RCE_idMovimientos_Interconsulta,
                    moment(moment(r.RCE_fechaMovimientos_Interconsulta).toDate()).format('DD-MM-YYYY hh:mm:ss'),
                    r.GEN_loginUsuarios,
                    r.GEN_descripcionTipo_Movimientos_Sistemas
                ]);
            });
        }
    });

    if (adataset.length == 0)
        $('#lnkExportarMov').addClass('disabled');
    else
        $('#lnkExportarMov').removeClass('disabled');

    $('#tblMovimientos').addClass("nowrap").DataTable({
        data: adataset,            
        order: [],
        columns: [
            { title: "RCE_idMovimientos_Interconsulta" },
            { title: "Fecha Movimiento" },
            //{ title: "GEN_idUsuarios" },
            { title: "Usuario" },
            { title: "Movimiento" }
        ],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false
            },
            { "targets": 1, "sType": "date-ukLong" }
        ],
        "bDestroy": true
    });
    $('#mdlMovimiento').modal('show');
}

function linkVerEditar(e) {
    var id = $(e).data("id");
    setSession('ID_INTERCONSULTA', id);
    //quitarListener();
    window.location.replace(ObtenerHost() + "/Vista/ModuloIC/NuevoIC.aspx");
}

function linkImprimir(e) {
    var id = $(e).data("id");
    //setSession('ID_INTERCONSULTA', id);
    $('#modalCargando').show();
    $('#frameIC').attr('src', 'ImprimirIC.aspx?id=' + id);
    $('#mdlImprimir').modal('show');
}

function linkCerrar(e) {
    var id = $(e).data('id');
    $('#idIC').val(id);
    ShowModalAlerta('CONFIRMACION',
        'Cerrar IC',
        'Una vez cerrada esta interconsulta será quitada de la lista.',
        CerrarIC, null, 'CERRAR IC', 'CANCELAR');
    //$('#mdlRechazar').modal('show');
}

function linkRechazar(e) {
    var id = $(e).data('id');
    $('#idIC').val(id);
    ShowModalAlerta('CONFIRMACION',
        'Anular IC',
        'Una vez anulada esta interconsulta será quitada de la lista.',
        AnularIC, null, 'ANULAR IC', 'CANCELAR');
    //$('#mdlRechazar').modal('show');
}

function CerrarIC() {

    $.ajax({
        type: 'PUT',
        url: GetWebApiUrl() + "RCE_Interconsulta/Cerrar/" + $('#idIC').val(),
        // async: false,
        success: function (data, status, jqXHR) {
            $('#idIC').val('0');
            getTablaIC(sSession);
            toastr.success('InterConsulta CERRADA');
        },
        error: function (jqXHR, status) {
            console.log(jqXHR);
        }
    });

    $('#modalAlerta').modal('hide');
}

function AnularIC() {

    $.ajax({
        type: 'PUT',
        url: GetWebApiUrl() + "RCE_Interconsulta/Anular/" + $('#idIC').val(),
        // async: false,
        success: function (data, status, jqXHR) {
            $('#idIC').val('0');
            getTablaIC(sSession);
            toastr.success('InterConsulta ANULADA');
        },
        error: function (jqXHR, status) {
            console.log(jqXHR);
        }
    });

    $('#modalAlerta').modal('hide');

}

function linkEspecialidad(e)
{
    var iID = $(e).data('id');
    $('#iID').val(iID);

    var iEsp = $(e).data('esp');

    $('#ddlEspecialidad1').prop('value', iEsp);
    resetDropdown('#ddlEspecialidad2');

    $('#mdlEspecialidad').modal('show');
    //alert(id);
}

function linkVerArchivosAdjuntos(sender) {
    cargarArchivosExistentes1($(sender).data('id'), 'IC', false);
    $("#mdlArchivosAdjuntos").modal('show');
}

function linkResponder(e)
{
    var id = $(e).data("id");
    setSession('ID_INTERCONSULTA', id);
    //quitarListener();
    window.location.replace(ObtenerHost() + "/Vista/ModuloIC/NuevoIC.aspx");
}

function LlenaGrillaIC(datos, grilla, sSession) {
    console.log(datos)
    if (datos.length == 0)
        $('#lnbExportar').addClass('disabled');
    else
        $('#lnbExportar').removeClass('disabled');

    $(grilla).addClass("nowrap").addClass("tblBandeja").addClass("dataTable").DataTable({
        data: datos,
        order: [],
        className: 'hhcc',
        columns: [
            { title: "IDIC", className: "text-center" },
            { title: "Fecha de ingreso", className: "text-center" },
            { title: "Paciente", className: "text-center" },
            { title: "Estado", className: "text-center" },
            { title: "GEN_idTipo_Estados_Sistemas", className: "text-center" },
            { title: "GEN_idPaciente", className: "text-center" },
            { title: "RCE_idEventos", className: "text-center" },
            { title: "GEN_idProfesional", className: "text-center"},
            { title: "idEspecialidad", className: "text-center" },
            { title: "countRespuesta", className: "text-center" },
            { title: "", className: "text-center" },
            { title: "", className: "text-center" }
        ],
        "columnDefs": [
            { "targets": 2, "sType": "date-ukLong" },
            {
                "targets": -2,
                "data": null,
                orderable: false,
                "render": function (data, type, row, meta) {
                    var fila = meta.row;
                    var botones;
                    var botones =
                        `
                                    <button class='btn btn-primary btn-circle' 
                                onclick='toggle("#div_accionesIC${fila}"); return false;'>
                                <i class="fa fa-list" style="font-size:15px;"></i>
                            </button>
                                <div id='div_accionesIC${fila}' class='btn-container' style="display: none;">
                                   <center>
                                        <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                        <div class="rounded-actions">
                                            <center>
                                `;
                    if (sSession.CODIGO_PERFIL == 1) 
                        botones += `<a id='linkVerEditar' data-id=${datos[fila][0]} class='btn btn-default btn-circle btn-lg' href='#/' onclick='linkVerEditar(this)' data-toggle="tooltip" data-placement="left" title="Ver/Editar IC"><i class='fa fa-edit'></i></a><br>`;
                    botones += `<a id='linkImprimir' data-id=${datos[fila][0]} class='btn btn-default btn-circle btn-lg' href='#/' onclick='linkImprimir(this);' data-print='true' data-frame='#frameIC' data-toggle="tooltip" data-placement="left" title="Imprimir"><i class='fa fa-print'></i></a><br>`;
                    botones += `<a id='linkMovimientos' data-id=${datos[fila][0]} class='btn btn-info btn-circle btn-lg ' href='#/' onclick='linkMovimientos(this)' data-toggle="tooltip" data-placement="left" title="Movimientos"><i class='fa fa-list'></i></a><br>`;
                    botones += `<a id='linkArchivosAdjuntos' data-id=${datos[fila][0]} class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkVerArchivosAdjuntos(this);'><i class='fa fa-clipboard' data-toggle="tooltip" data-placement="left" title="Adjuntos"></i></a><br>`;
                    
                    if (idPro != datos[fila][7] && sSession.CODIGO_PERFIL == 10 && datos[fila][4] == '75') {
                        var sRes = 'Responder';
                        if (datos[fila][9] > 0)
                            sRes = 'Editar Respuesta';
                        botones += `<a id='linkResponder' data-id=${datos[fila][0]} class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkResponder(this)'><i class='fa fa-reply' data-toggle="tooltip" data-placement="left" title="+{sRes}+"></i></a><br>`;
                    }

                    if (sSession.CODIGO_PERFIL == 10) //Interconsultor
                        botones += `<a id='linkEspecialidad' data-id=${datos[fila][0]} data-esp=${datos[fila][8]} class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkEspecialidad(this)' data-toggle="tooltip" data-placement="left" title="Cambiar Especialidad"><i class='fa fa-exchange' ></i></a><br>`;

                    if (sSession.CODIGO_PERFIL == 1) {
                        if (datos[fila][4] == '75' || datos[fila][4] == '78' || datos[fila][4] == '83')
                            botones += `<a id='linkCerrar' data-id=${datos[fila][0]} class='btn btn-warning btn-circle btn-lg' href='#/' onclick='linkCerrar(this)'><i class='fa fa-times-circle' data-toggle="tooltip" data-placement="left" title="Cerrar IC"></i></a><br>`;
                        if (datos[fila][4] == '75' || datos[fila][4] == '78')
                            botones += `<a id='linkRechazar' data-id=${datos[fila][0]} class='btn btn-danger btn-circle btn-lg' href='#/' onclick='linkRechazar(this)'><i class='fa fa-times' data-toggle="tooltip" data-placement="left" title="Anular IC"></i></a><br>`;
                    }

                    botones += `
                                </center >
                                </div >
                            </center >
                        </div >`
                        ;

                    return botones;
                }
            },
            {
                "targets": -1,
                "data": null,
                "orderable": false,
                "render": function (data, type, row, meta) {
                    //Orden de la informacion
                    //    val.Id,
                    //    moment(moment(val.Fecha).toDate()).format('DD-MM-YYYY'),
                    //    val.NombrePaciente + ' ' + val.PrimerApellidoPaciente + ' ' + val.SegundoApellidoPaciente,
                    //    val.DescripcionEstado,
                    //    val.IdEstado,
                    //    val.IdPaciente,
                    //    val.IdEvento,
                    //    val.IdProfesional,
                    //    val.IdEspecialidadDestino,
                    //    val.RespuestasInterconsultasCount
                    var fila = meta.row;
                    console.log(datos[fila])
                    var botones;
                    botones =  "<a id='linkVerHojasEvolucion' data-id='" + datos[fila][5] + "' data-idEvento='" + datos[fila][6] + "'";
                    botones += `    class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkVerHojasEvolucion(this);' data-toggle="tooltip" data-placement="left" title="Hoja De Evolución">`
                    botones += "    <i class='fa fa-eye'></i>";
                    botones += "</a>";

                    return botones;
                }
            },
            {
                "targets": [4, 5, 6, 7, 8, 9],
                "visible": false
            }
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData[7] == '78') {
                $('td', nRow).css('background-color', '#ffcccc');
            }
        },

        //Crea la nueva tabla al redimensionar
        responsive: {
            details: {
                renderer: function (api, rowIdx, columns) {
                    var data = $.map(columns, function (col, i) {
                        var toReturn = '';
                        if (col.hidden) {
                            toReturn = toReturn.concat('<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">');

                            if (col.title == '') {

                                //Obtener objeto JQuery desde una cadena HTML
                                var button = $(col.data, "button").get();

                                var ID;
                                var newID;

                                if (button) {
                                    ID = $(button).attr("id");
                                    var r = ID.replace('#', '');
                                    newID = r + "-resp";
                                }

                                var first = col.data.replace(new RegExp(r, 'g'), newID);
                                var second = first.replace(new RegExp('<br>', 'g'), '');
                                var result = second.replace(new RegExp('rounded-actions', 'g'), 'rounded-actions-resp');
                                //console.log(result);

                                toReturn = toReturn.concat('<td colspan=2 align=center>' + result + '</td>');
                            }
                            else {
                                toReturn = toReturn.concat('<td align=right style="width: 50%;">' + col.title + ':' + '</td> ');
                                toReturn = toReturn.concat('<td style="width: 50%;">' + col.data + '</td>');
                            }

                            toReturn = toReturn.concat('</tr>');
                        }

                        //Prueba
                        /*
                        $('body').on('.asdf', 'click', function () {
                            alert('aa');
                        });
                        */
                        //console.log("aquí");

                        return toReturn;

                    }).join('');

                    return data ?
                        $('<table style = "width: 100%"/>').append(data) :
                        false;
                }
            }
        },
        "bDestroy": true
    });

    $('[data-toggle="tooltip"]').tooltip();
}
