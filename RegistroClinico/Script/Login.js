﻿
$(document).ready(function () {

    $('#Content_btnLogin').click(async function () {
        ShowModalCargando(true);
        $("#Content_btnLogin").attr("disabled", "disabled");
        await Login(false);
        $("#Content_btnLogin").removeAttr("disabled");
        ShowModalCargando(false);
    });

});

async function Login(esLoginModal) {

    var success = false;

    try {

        var resp;
        let user = $('#txtUsuario').val();
        var pass = CryptoJS.MD5($('#txtClave').val());
        var webapi = `${GetWebApiUrl()}recuperarToken`;

        var datos = {
            "grant_type": "password",
            "username": user,
            "password": '' + pass,
            "clientid": '14'
        }
        
        delete $.ajaxSettings.headers['Authorization'];

        $.ajax({
            type: "POST",
            url: webapi,
            data: datos,
            async: false,
            success: function (response) {
                resp = response;
                success = true;
                
            },
            error: function (err) {
                console.log("ERROR no se pudo obtener el token: " + JSON.stringify(err));
            }
        });

    } catch (err) {
        console.log(JSON.stringify(err));
    }

    if (success) {

        setSession("TOKEN", resp.access_token);
        setSession("LOGIN_USUARIO", datos.username);
        setSession("FECHA_FINAL_SESION", moment().add(parseInt(resp.expires_in), 'seconds').format('YYYY-MM-DD HH:mm:SS'));

        $('#btnLogin').addClass('disabled');

        if ($('#divLogin').hasClass('shake'))
            $('#divLogin').removeClass('shake');

        $.ajaxSetup({
            headers: { 'Authorization': GetToken() }
        });

        getUsuario(esLoginModal);

    } else {
        LoginIncorrecto();
    }

    return success;
}

function getUsuario(esLoginModal) {

    var bool = true;

    try {

        const url = `${GetWebApiUrl()}/GEN_Usuarios/Acceso`;

        $.ajax({
            type: 'GET',
            url: url,
            contentType: "application/json",
            dataType: "json",
            async: false,
            success: function (data) {

                try {

                    if ($.isEmptyObject(data)) {

                        LoginIncorrecto();
                        bool = false;

                    } else {

                        const nombreUsuario = data[0].GEN_nombrePersonas + " " +
                            data[0].GEN_apellido_paternoPersonas +
                            ((data[0].GEN_apellido_maternoPersonas == null) ? "" : " " + data[0].GEN_apellido_maternoPersonas);

                        setSession("ID_USUARIO", data[0].GEN_idUsuarios);
                        setSession("ID_ESPECIALIDAD", data[0].GEN_idEspecialidad[0]);
                        setSession("NOMBRE_USUARIO", nombreUsuario);
                        
                        if (!($.isEmptyObject(data[0].GEN_idProfesional))) {
                            setSession("ID_PROFESIONAL", data[0].GEN_idProfesional);
                        }

                        $.each(data, function (key, val) {
                            
                            var perfiles = [];
                            var count = 0;

                            $.each(val.GEN_Perfiles, function () {
                                perfiles.push({
                                    "GEN_idPerfil": this.GEN_idPerfil,
                                    "GEN_codigoPerfil": this.GEN_codigoPerfil,
                                    "GEN_descripcionPerfil": this.GEN_descripcionPerfil
                                });
                                count++;
                            });

                            $("#ucMdlPerfiles_ddlPerfiles").empty();

                            // Valida si la contraseña es igual al RUT del usuario, esta sesión me sirve para saber si debo redireccionar 
                            // hacia el aspx de cambio de contraseña aparezca o no el modal de perfiles.
                            if (!esLoginModal)
                                setSession("ES_CAMBIO_CONTRASEÑA", ($('#txtClave').val() === val.GEN_numero_documentoPersonas));
                            
                            if ((!esLoginModal) && count > 1) {

                                setSession('PERFILES', JSON.stringify(perfiles));
                                llenaComboPerfiles(perfiles);
                                $("#mdlPerfiles").modal("toggle");
                                $("#modalCargando").hide();

                            } else {
                                console.log(getSession().LOGIN_USUARIO);
                                SetSessionServer({
                                    LOGIN_USUARIO: getSession().LOGIN_USUARIO,
                                    NOMBRE_USUARIO: nombreUsuario,
                                    CODIGO_PERFIL: val.GEN_Perfiles[0].GEN_codigoPerfil,
                                    PERFIL_USUARIO: val.GEN_Perfiles[0].GEN_descripcionPerfil,
                                    TOKEN: getSession().TOKEN
                                });

                                if (esLoginModal) {

                                    $('#mdlLogin').modal('hide');
                                    $('#txtUsuario').val('');
                                    $('#txtClave').val('');

                                } else {

                                    setSession("PERFIL_USUARIO", val.GEN_Perfiles[0].GEN_descripcionPerfil);
                                    setSession("CODIGO_PERFIL", val.GEN_Perfiles[0].GEN_codigoPerfil);
                                    setSession("ID_PERFIL", val.GEN_Perfiles[0].GEN_idPerfil);
                                    // Valida si la contraseña es igual al RUT del usuario
                                    if ($('#txtClave').val() === val.GEN_numero_documentoPersonasUsuario) {
                                        window.location.replace(`${ObtenerHost()}/Vista/CambioContraseña.aspx`);
                                    } else {
                                        window.location.replace(`${ObtenerHost()}/Vista/Inicio.aspx`);
                                    }

                                }
                            }
                        });
                    }
                } catch (err) {
                    console.log("error: " + err.message);
                    bool = false;
                }
            }
        });
    }catch(err){
        console.log("Error:" + err.message);
        bool = false;
    }
    return bool;
}
function getProfesional(id) {    
    var url = GetWebApiUrl() + "/GEN_Profesional/" + id;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                var idEspecialidad = null;
                if (!($.isEmptyObject(data.GEN_Pro_Especialidad))) {
                    $.each(data.GEN_Pro_Especialidad, function () {
                        $.each(this.GEN_Especialidad, function (esp) {
                            if (esp.GEN_estadoPro_Especialidad != "Inactivo")
                                idEspecialidad = esp.GEN_idEspecialidad;
                        });
                    });
                    if (idEspecialidad != null)
                        setSession("ID_ESPECIALIDAD", idEspecialidad);
                }
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });
    //$.getJSON(url,function(data){
    //    try{
    //        var idEspecialidad = null;
    //        if(!($.isEmptyObject(data.GEN_Pro_Especialidad))){
    //            $.each(data.GEN_Pro_Especialidad, function(){
    //                 $.each(this.GEN_Especialidad, function(esp){
    //                    if(esp.GEN_estadoPro_Especialidad != "Inactivo")
    //                        idEspecialidad = esp.GEN_idEspecialidad;
    //                 });
    //            });
    //            if(idEspecialidad != null)
    //                setSession("ID_ESPECIALIDAD", idEspecialidad);    
    //        }
    //    }catch(err){
    //        console.log("error: " + err.message);
    //    }
    //});
}
function llenaComboPerfiles(arr) {
    $('#ucMdlPerfiles_ddlPerfiles').append($('<option></option>').val('0').html('- SELECCIONE -'));
    $.each(arr, function () {
        //hans: debo encontrar una mejor forma de hacer esto
        var v = this.GEN_codigoPerfil + '|' + this.GEN_idPerfil;
        $('#ucMdlPerfiles_ddlPerfiles').append($('<option></option>').val(v).html(this.GEN_descripcionPerfil));
    });
}
function LoginIncorrecto() {

    Swal.fire({
        position: 'center',
        icon: 'error',
        title: "Usuario o contraseña incorrecta",
        showConfirmButton: false,
        timer: 2000
    });

    $('#divLogin').addClass('shake');
    if ($('#btnLogin').hasClass('disabled'))
        $('#btnLogin').removeClass('disabled');
    $("#modalCargando").hide();

}
