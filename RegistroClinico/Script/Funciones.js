﻿$(document).ready(function () {

    // MÉTODOS DROPDOWN
    $('body').on('click', '.dropdown-menu a', function (e) {
        $(this).parents('.dropdown').find('.btn').text($(this).text());
        e.preventDefault();
    });

    $('body').on('click', '.dropdown', function (e) {
        if ($('.dropdown-menu', this).children().length == 0)
            e.stopPropagation();
    });

    // MÉTODOS DROPDOWN 
    $('body').on('click', '.menuModulos', function (e) {
        // Guardar el id del menú clickeado o algo
        sessionStorage.idMenu = $(this).prop('id');
        console.log("asdasd")
    });

    $('.ui.tabular > .item').click((e) => {
        $('.arrowright.active').removeClass('active');
        $(this).children('.arrowright').addClass('active');
    });

});
function InicializarTypeAhead(element, divValidar, button, arrayCarteraArancel, emptytempl, templ, source, TipoCartera) {
    /* Element = input que realiza busqueda, DivValidar = div que contiene el input(se valida)
     * Button = Boton para agregar el elemento, arrayCartelArancel = datos de la bdd.
     * emptytempl= Mensaje por defecto cuando no hay coincidencias, templ = Mensaje cuando hay coincidencias.
     * TipoCartera = Ubicacion (Examenes, procedimientos, imageonologia)
     */
    $(element).typeahead(setTypeAhead(arrayCarteraArancel, element, emptytempl, templ, source));
    $(element).change(() => $(element).removeData("id"));
    $(element).blur(() => {
        if ($(element).data("id") == undefined)
            $(element).val("");
    });
    $(button).unbind().on('click', function (e) {
        e.preventDefault()
        if (validarCampos(divValidar, false)) {
            $(element).data("text-servicio", $(element).val())
            let idCartera = $(element).data("id");
            $(element).data("id-tipo-cartera", 61)
            $(element).data("id-cartera", idCartera);
            $(element).data("id-tipo-cartera", TipoCartera)
            modificarCarteraArancel($(element));
        }
    })
}
//TypeAhead para select2, filtro que busca coincidencias en el texto ingresado
function setTypeAhead(data, input, emptytempl, templ, source) {
    var typeAhObject = {
        input: input,
        minLength: 3,
        maxItem: 12,
        maxItemPerGroup: 10,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: emptytempl,
        template: templ,
        correlativeTemplate: true,
        source: {
            source: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                $(input).data('id', item.IdCarteraServicio);
            }
        }
    };

    return typeAhObject;
}
function ShowModalCargando(esVisible) {
    if (esVisible) {
        $("#modalCargando").show();
    } else
        $("#modalCargando").hide();
}

function firstUpper(str) {
    str = str.toLowerCase().replace(/\b[a-z]/g, function (letter) { return letter.toUpperCase(); });
    return str;
}

function fadeInLeft(e) {
    e.css('visibility', 'visible');
    e.addClass('animated fadeInLeft');
    setTimeout(function () { e.removeClass('animated fadeInLeft') }, 2000);
}

function fadeOutLeft(e) {
    e.addClass('animated fadeOutLeft');
    setTimeout(function () { e.removeClass('animated fadeOutLeft').css('visibility', 'hidden'); }, 2000);
}

function fadeIn(e) {
    e.css('visibility', 'visible');
    e.addClass('animated fadeIn');
    setTimeout(function () { e.removeClass('animated fadeIn') }, 2000);
}

function fadeOut(e) {
    e.addClass('animated fadeOut');
    setTimeout(function () { e.removeClass('animated fadeOut').css('visibility', 'hidden'); }, 2000);
}

function DarFuncionRadios() {
    $("input[type='checkbox'], input[type='radio']").bootstrapSwitch();

    $("input[type='checkbox'], input[type='radio']").each(function () {
        $(this).on('switchChange.bootstrapSwitch', function (event, state) {
            if (state) {
                var radioChecked = $(this);
                $(this).attr('checked', 'checked');

                $("input[name='" + radioChecked.attr("name") + "']").each(function () {
                    if (radioChecked.attr("id") !== $(this).attr("id"))
                        $(this).removeAttr("checked");
                });
            } else
                $(this).removeAttr("checked");
        });
    });
}

// funcion para validar el correo
function caracteresCorreoValido(email, b) {

    //var email = $(email).val();
    var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
    var correo = email.val();
    var valido = true;

    if (!b)
        return false;

    if (correo === "" || correo === null) {
        if (email.hasClass('is-invalid'))
            email.removeClass('is-invalid');
    } else {
        if (!caract.test(correo)) {
            valido = false;
            if (!email.hasClass('is-invalid'))
                email.addClass('is-invalid');
        } else {
            if (email.hasClass('is-invalid'))
                email.removeClass('is-invalid');
        }
    }

    return valido;

}

function ObtenerVerificador(sRut) {

    if (sRut.length >= 1 && /^[0-9]*$/.test(sRut)) { 

        var iN = [2, 3, 4, 5, 6, 7, 2, 3, 4, 5, 6, 7];

        var iTotal = 0;
        sRut = sRut.split("").reverse().join("");
        var sArray = [];
        for (var i = 0; i < sRut.length; i++) {
            sArray.push(sRut[i]);
            iTotal += sRut[i] * iN[i];
        }

        var iMod = iTotal % 11;
        var iV = 11 - iMod;
        if (iV === 11)
            iV = '0';
        if (iV === 10)
            iV = 'K';

        return iV;
    }

    return null;
}
//Traducir el mes de una fecha en-es
function traducirMoment(fecha) {
    let mes = moment(fecha).format("MMMM")
    switch (mes) {
        case "January":
            mest = "Enero"
            break
        case "February":
            mest = "Febrero"
            break
        case "March":
            mest = "Marzo"
            break
        case "April":
            mest = "Abril"
            break
        case "May":
            mest = "Mayo"
            break
        case "June":
            mest = "Junio"
            break
        case "July":
            mest = "Julio"
            break
        case "August":
            mest = "Agosto"
            break
        case "September":
            mest = "Septiembre"
            break
        case "October":
            mest = "Octubre"
            break
        case "November":
            mest = "Noviembre"
            break
        case "December":
            mest = "Diciembre"
            break
        default:
            mest = mes;
        break
    }
    return fecha.replace(mes, mest) 
}

function CalcularEdad(fechaNac) {

    var edad;
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}GEN_Paciente/CalcularEdad?GEN_fec_nacimientoPaciente=${fechaNac}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            edad = data;
        },
        error: function (err) {
            console.log(`Error al calcular edad paciente: ${ JSON.stringify(err) }`)
        }
    });

    return edad;
}

function GuardarError(sistema, modulo, formulario, detalle, stackTrace, codigo) {

    var edad;

    $.ajax({
        type: "POST",
        url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=GuardarLog`,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            sistema: sistema,
            modulo: modulo,
            formulario: formulario,
            log: JSON.stringify({
                detalle: detalle,
                stackTrace: stackTrace,
                codigo: codigo
            })
        }),
        async: false,
        success: function (data) { }
    });

    return edad;

}

function AgregarHoras(cantHoras) {

    var fecha = null;

    $.ajax({
        type: "GET",
        url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=AddHorasFechaActual&cantHoras=${cantHoras}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            fecha = data.fecha;
        }
    });

    return fecha;

}

function AgregarDias(fechaSeleccionada, cantDias) {

    var fecha = null;

    $.ajax({
        type: "GET",
        url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=AddDiasFechaActual&fecha=${fechaSeleccionada}&cant=${cantDias}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            fecha = data.fecha;
        }
    });

    return fecha;

}

function ConvertirBytes(tipo, bytes) {
    if (tipo === 'kb')
        return parseFloat(Math.round((bytes / 1024) * 100) / 100).toFixed(2);
    else if (tipo === 'mb')
        return parseFloat(Math.round((bytes / Math.pow(1024, 2)) * 100) / 100).toFixed(3);
    else if (tipo === 'gb')
        return parseFloat(Math.round((bytes / Math.pow(1024, 3)) * 100) / 100).toFixed(3);
}

function ObtenerHost() {
    var host = "";
    if (window.location.origin.indexOf("localhost") > -1)
        return window.location.origin;
    return window.location.origin + "/RCE";
}

function EsValidoDigitoVerificador(rut, digito) {

    if (rut.length >= 6 && /^[0-9]*$/.test(rut)) {
        var suma = 0;
        for (var i = rut.length - 1, j = 2; i >= 0; i--) {
            suma += (parseInt(rut.charAt(i)) * j);
            j = (j === 7) ? 2 : ++j;
        }

        return (digito.toLowerCase() === ObtenerDigitoOficial(11 - (suma % 11)));
    }

    return false;
}

function ObtenerDigitoOficial(digitoOficial) {
    if (digitoOficial === 10)
        return 'k';
    else if (digitoOficial === 11)
        return '0';
    else
        return digitoOficial.toString();
}

function visibleDesdeOyente(component) {

    if (component.data("print") === true) {
        $(component.data("frame")).on('load', function () { $("#modalCargando").hide(); });
        $("#modalCargando").show();
    }

}

function estiloTabla(tabla) {

    tabla.prepend($("<thead></thead>").append(tabla.find("tr:first")))
        .addClass('nowrap')
        .dataTable({
            'stateSave': true,
            'iStateDuration': 120,
            'lengthMenu': [[10, 20], [10, 20]],
            responsive: true,
            columnDefs: [
                { targets: [-1, -3], className: 'dt-body-right' }
            ]
        });

}

function obtenerNombreUsuario() {
    return $(".nombreUsuario").html();
}

function AgregarSeleccioneSelect(array, id, value) {
    array.unshift({ id: 0, value: "Seleccione" });
    return array;
}

//Validación para campos en general
function validarCampos(idDivPadre, irAPrimerComponente) {

    var esValido = true;
    var primerComponente = null;

    //Selecciona los Input/Campos dentro del Padre con idDivPadre que tienen data-required = true

    $(`${idDivPadre}  input[type='text'][data-required='true'],
        ${idDivPadre} input[type='password'][data-required='true'],
        ${idDivPadre} select[data-required='true'],
        ${idDivPadre} input[type='time'][data-required='true'],
        ${idDivPadre} input[type='date'][data-required='true'],
        ${idDivPadre} input[type='number'][data-required='true'],
        ${idDivPadre} input[type='search'][data-required='true'],
        ${idDivPadre} input[type='radio'][data-required='true'],
        ${idDivPadre} textarea[data-required='true'],
        ${idDivPadre} div[data-required='true'],
        ${idDivPadre} input[type='checkbox'][data-required='true'],
        ${idDivPadre} table[data-required='true']`).each(function () {
            
            //excepción cuando el select tiene solo 1 opción (-Seleccione-)
            //excepción cuando el elemento es visible
            //excepción cuando el elemento está deshabilitado
            switch ($(this).prop('nodeName')) {
                case 'INPUT':
                // Validación de Textarea
                case 'TEXTAREA':

                    if ($(this).hasClass('textboxio')) {

                        var txt = textboxio.replace('#' + $(this).attr("id")).content.get().replace(/<(?:.|\n)*?>/gm, '').replace(/\&nbsp;/g, '');

                        if ($.trim(txt) === '') {
                            esValido = false;
                            $("#ephox_" + $(this).attr("id")).addClass("textboxio-invalid");
                        } else $("#ephox_" + $(this).attr("id")).removeClass("textboxio-invalid");

                        break;
                    }

                    //Validación de tipo Checkbox
                    if ($(this).attr("type") === "checkbox") {

                        if ($(this).hasClass('custom-control-input') && !$(this).is(':checked')) {
                            $(this).parent().css({ "border": "2px solid red" });
                            esValido = false;
                        } else
                            $(this).parent().css({ "border": "" });

                        break;
                    }

                    if ($(this).attr("type") === "radio") {

                        esValido = false;

                        if ($(this).hasClass("bootstrapSwitch")) {

                            $(this).parent().parent().parent().addClass("invalid-input");

                            for (const rdo of $(`[name='${$(this).attr("name")}']`)) {
                                if ($(rdo).bootstrapSwitch('state'))
                                    esValido = true;
                            }

                            (!esValido) ?
                                $(this).parent().parent().parent().addClass("invalid-input") :
                                $(this).parent().parent().parent().removeClass('invalid-input');

                        } else {

                            for (const rdo of $(`[name='${$(this).attr("name")}']`)) {
                                if ($(rdo).is(':checked'))
                                    esValido = true;
                            }

                            (!esValido) ?
                                $(this).parent().parent().addClass("invalid-input") :
                                $(this).parent().parent().removeClass('invalid-input');

                        }


                    } else if (!AgregarEstiloErrorTxt(this)) esValido = false;
                    
                    break;
                //Validación de Select
                case 'SELECT':
                    if (!AgregarEstiloErrorSlt(this)) esValido = false;
                    break;
                case 'DIV':
                    //Validación de Switches de Bootstrap
                    if ($(this).hasClass("bootstrapSwitch")) {

                        var array = $("div.bootstrapSwitch input[type='radio'][name='" + $(this).attr("data-name") + "']");
                        var esValidoRadio = false;

                        for (var i = 0; i < array.length; i++) {
                            if ($(array[i]).bootstrapSwitch('state')) {
                                esValidoRadio = true;
                                break;
                            }
                        }

                        if (!esValidoRadio) {
                            esValido = false;
                            $(this).addClass('bootstrapSwitch-invalid');
                        } else $(this).removeClass('bootstrapSwitch-invalid');
                    }

                    if (!$(this).hasClass("bootstrapSwitch") && ($('button', this).attr('val') === '0' || $('button', this).attr('val') === 'string:0' || $('button', this).attr('val') === 'number:0') &&
                        $('.dropdown-menu', this).children().length > 0 && $(this).is(":visible") && !$(this).hasClass('disabled')) {
                        esValido = false;
                        $('button', this).addClass('invalid');
                    } else $('button', this).removeClass('invalid');
                    break;
                case 'TABLE':
                    if ($(this).children("tbody").length == 0) {
                        $(this).children("tbody").addClass('invalid');
                    } else $(this).children("tbody").removeClass('invalid');
                    break;
            }

            if (!esValido && primerComponente === null)
                primerComponente = $(this);

        });

    if (primerComponente !== null && irAPrimerComponente)
        $('html, body').animate({ scrollTop: primerComponente.offset().top - 40 }, 500);

    return esValido;

}

function AgregarEstiloErrorTxt(comp) {

    let esValido = true;

    if (($.trim($(comp).val()) === '' || $(comp).val() === null) && $(comp).is(":visible") && !$(comp).hasClass('disabled')) {
        esValido = false;
        $(comp).addClass('invalid-input');
        if ($(comp).next().prop('nodeName') == undefined)
            $(comp).after("<span class='invalid-input'>Este campo es obligatorio</span>");
    } else {
        $(comp).next().remove();
        $(comp).removeClass('invalid-input');
    }

    return esValido;

}

function AgregarEstiloErrorSlt(comp) {

    let esValido = true;

    if ($(comp).val() == null || $(comp).val() == '' || ($(comp).val() === '0' || $(comp).val().includes(':0')) &&
        ($(comp)[0].length > 1 && $(comp).is(":visible") && !$(comp).hasClass('disabled'))) {
        esValido = false;
        $(comp).addClass('is-invalid');
        if ($(comp).next().prop('nodeName') == undefined)
            $(comp).after("<span class='invalid-input'>Este campo es obligatorio</span>");
    } else {
        $(comp).next().remove();
        $(comp).removeClass('is-invalid');
    }

    return esValido;

}

function SaltoDeLinea(t, c) {

    var r = '';
    var res = t.split(' ');

    for (var i = c; i < res.length; i = i + (c + 1)) {
        res.splice(i, 0, '<br />');
    }

    return res.join(' ');

}

// Esta ruta se obtiene desde AppSetting.json y se establece en Default.aspx
function GetWebApiUrl() {

    if (localStorage.getItem("urlWebApi") == null || localStorage.getItem("urlWebApi") == undefined) {

        $.ajax({
            type: "GET",
            url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=GetUrlWebApi`,
            async: false,
            success: function (data) {
                localStorage.setItem("urlWebApi", data);
            },
            error: function (err) {
                console.log("Error al obtener la URL de la WebApi: " + JSON.stringify(err));
            }
        });

    }

    return localStorage.getItem("urlWebApi");

}

function getCookie(cname, key) {

    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            var cookie = c.substring(name.length, c.length);
            return cookie.split("=")[1];
        }
    }
    return null;
}

function EliminarCookie(cookiename) {
    var d = new Date();
    d.setDate(d.getDate() - 1);
    var expires = ";expires=" + d;
    var name = cookiename;
    var value = "";
    document.cookie = name + "=" + value + expires + "; path=/acc/html";
}

function ValTipoCampo(component, value) {
    switch (component.prop('nodeName')) {
        case 'SELECT':
            (value === null) ? component.val('0') : component.val(value)
            break;
    }
}

function valCampo(obj) {
    switch (typeof obj) {
        case 'string':
            return ($.trim(obj) !== '') ? $.trim(obj) : null;
        case 'number':
            return (obj !== 0) ? obj : null;
        case 'undefined':
            return null;
    }
    return null;
}

function DevolverString(obj) {
    switch (typeof obj) {
        case 'undefined':
        case 'null':
            return '';
            break;
    }

    return obj.toString();
}

function SetSessionServer(session) {
    $.ajax({
        type: "POST",
        url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=SetSession`,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: false,
        cache: false,
        data: JSON.stringify(session),
        async: false,
        success: function (data) { }
    });
}

// MÉTODOS VARIABLES DE SESIÓN
function deleteSession(key) {
    let json = JSON.parse(localStorage.getItem("sSession"));
    delete json[key];
    localStorage.setItem("sSession", JSON.stringify(json));
}

function setSession(key, value) {
    let json = (localStorage.getItem("sSession") != null) ? JSON.parse(localStorage.getItem("sSession")) : {};
    json[key] = value;
    localStorage.setItem("sSession", JSON.stringify(json));
}

function RevisarAcceso(quitarAcceso, session) {
    //permite reciclar la sesión si ya la he cargado
    var vSession;
    if (session == undefined || session == null)
        vSession = getSession();
    else
        vSession = session;

    if (vSession.ID_USUARIO != undefined) {

        //excepción por si no esta seteado nMODULO (Modal de perfiles se caía)
        if (vSession.nMODULO != undefined) {
            var vRedireccionar = true;
            //var vModulo = vSession.nMODULO.split(',');
            var cModulo = vSession.cMODULO.split(',');

            for (var i = 0; i < cModulo.length; i++) {
                //for (var i = 0; i < vModulo.length; i++) {
                var url = window.location.pathname;
                url = url.replace('/RCE', '');
                //console.log('modulo: ' + cModulo[i]);
                //console.log('url: ' + url);
                if (cModulo[i] == url)
                    vRedireccionar = false;
            }
            if (vRedireccionar) {
                if (quitarAcceso)
                    quitarListener();
            }
        }
    } else {
        if (quitarAcceso) 
            quitarListener();
        
        window.location.replace(ObtenerHost() + "/Vista/Default.aspx");
    }
}

function quitarListener() {
    window.removeEventListener('beforeunload', bunload, false);
    location.href = document.referrer;
}


function getSession() {
    if (localStorage.getItem("sSession") != null)
        return JSON.parse(localStorage.getItem("sSession"));
    return {};
}

// MÉTODOS VARIABLES DE SESIÓN//////////////////////////////////

function GetFechaActual() {

    var fecha = null;

    $.ajax({
        type: "GET",
        url: ObtenerHost() + '/Vista/Handlers/MetodosGenerales.ashx?method=GetFechaActual',
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            fecha = moment(data.fecha,"YYYY.MM.DD HH.mm.ss").toDate();
        }
    });

    return fecha;
}

function GetDiferenciaFechas(fechaInicio, fechaFinal) {

    var iDif = null;
    $.ajax({
        type: "GET",
        url: ObtenerHost() + '/Vista/Handlers/MetodosGenerales.ashx' +
        '?method=GetDiferenciaEnDias' +
        '&fechaInicio=' + fechaInicio +
        '&fechaFinal=' + fechaFinal,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            iDif = data.diferenciaFechas;
        }
    });

    return iDif;

}

function GetLoginPass() {
    try {
        var sSession = getSession();
        //if (sSession.ID_PROFESIONAL == undefined)
        RevisarAcceso(true, sSession);
        //window.location.replace(ObtenerHost() + "/Vista/Default.aspx");
        var json = {};
        //OBTIENE LOGIN Y CONTRASEÑA ENCRIPTADA
        $.ajax({
            type: "GET",
            url: GetWebApiUrl() + 'GEN_Usuarios/Login',
            contentType: "application/json",
            dataType: "json",
            async: false,
            success: function (data) {
                $.each(data, function () {
                    json.login = this.GEN_loginUsuarios;
                    json.pass = this.GEN_claveUsuarios;
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log('error:' + xhr.responseText);
            }
        });
        json.ID_USUARIO = sSession.ID_USUARIO;
        return json;
    } catch (err) {
        console.log("Error:" + err.message);
    }
}


// METODOS DE ENCRIPTACIÓN AES /////////////////////////////////

function GetTextoEncriptado(txt) {
    var txtEncriptado;
    $.ajax({
        type: "GET",
        url: ObtenerHost() + '/Vista/Handlers/MetodosGenerales.ashx?method=Encriptar&texto=' + txt,
        async: false,
        success: function (data) {
            txtEncriptado = data;
        },
        error: function (err) {
            console.log("error");
        }
    })
    return txtEncriptado;
}

// METODOS DE ENCRIPTACIÓN AES /////////////////////////////////

function GetToken() {
    if (getSession().TOKEN != undefined)
        return 'Bearer ' + getSession().TOKEN;
    return null;
}

function HtmlAString(html) {
    var div = document.createElement("div");
    div.innerHTML = html;
    return div.textContent || div.innerText || "";
}

function sortByKeyDesc(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}

function sortByKeyAsc(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function filterItems(query,array) {
    return array.filter(function (el) {
        return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
    });
}

function ReiniciarRequired() {

    $("[data-required]").each(function (index, item) {

        if ($(this).attr("data-required") == "true") {

            let label = "";

            if ($(this).attr("type") === "radio") 
                label = $(this).parent().parent().parent().children("label");
            else {

                label = $(this).parent().children("label");

                if (!label.length > 0) 
                    label = $(this).parent().parent().children("label");
                
                if (!label.length > 0) {
                    //console.log(`Este elemento no se pudo marcar ${$(this).attr("id")} (*)`);
                    //console.log($(this).parent());//este elemento no se pudo marcar(*)
                }

            }

            label.children("b").remove();
            label.html($.trim(label.text()) + " <b class='color-error'>(*)</b>");

        } else {
            $(this).parent().children("label").children("b").remove();
        }
    });

}
function validarLongitud(input, longitud) {
    if (input.value.length > input.maxLength) input.value = input.value.slice(0, longitud);
}
function ValidarNumeros() {

    $(".number").keypress(function (event) {

        // 103 = e // 44 = ,
        if (event.which == 101 || event.which == 44)
            event.preventDefault();

        // 43 = + // 45 = - // 46 = .
        switch (event.which) {
            case 43:
            case 45:

                if (($(this).val().includes('+') || $(this).val().includes('-')) || $(this).val().length > 0)
                    event.preventDefault();
                break;

            case 46:

                if ($(this).val().includes('.') || $(this).val() == '')
                    event.preventDefault();
                break;

            default:

                if (!/^[0-9]*$/.test(String.fromCharCode(event.which)))
                    event.preventDefault();
                else if ($(this).val().includes('.') && $(this).val().substring($(this).val().indexOf("."), $(this).val().length).length == 3)
                    event.preventDefault();
                break;

        }

    }).on('input', function () {
        if ($(this).val().includes('.') && $(this).val().substring($(this).val().indexOf("."), $(this).val().length).length > 3)
            $(this).val($(this).val().replace(".", ""));
    }).blur(function () {
        if ($(this).val() != '') {
            if ($(this).val().charAt($(this).val().length - 1) == '.')
                $(this).val($(this).val() + '0');
            else if ($(this).val().startsWith("."))
                $(this).val('0' + $(this).val());
            else if (($(this).val().includes('+') || $(this).val().includes('-')) && $(this).val().length < 2)
                $(this).val('');
        }
    });

}
function validarDecimales() {
    $(".decimal").keydown(function (event) {
        if ((event.which >= 48 && event.which <= 57) || (event.which >= 96 && event.which <= 105) || event.which == 8 || event.which == 190 || event.which == 110) {
            return true
        }
        return false;
    })
}
function setCargarDataEnCombo(url, asincrono, selector) {
    $(selector).empty();
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: asincrono,
        success: function (data) {
            $(selector).append("<option value='0'>Seleccione</option>");
            $.each(data, function (key, val) {
                $(selector).append(`<option value='${(val.id == undefined) ? val.Id : val.id}'>${(val.valor == undefined) ? val.Valor : val.valor}</option>`);
            });
            return true;
        },
        error: function (jqXHR, status) {
            console.log(`Error URL: ${url} ${JSON.stringify(jqXHR)}`);
            return false;
        }
    });

}
//funciona como el setCargarDataEnCombo:{id, valor}, se diferencia en que este tiene grupo =Tipo(grupo)...Balances=subitems
function setCargarDataEnComboGroup(url, asincrono, selector) {
    $(selector).empty();
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: asincrono,
        success: function (data) {
            let contenido=""
            data.map(a => {
                contenido += `<optgroup label="${a.Tipo}" >`
                a.Balances.map(i => {
                    contenido += `<option value="${i.Id}" data-Tipo="${a.Tipo}">${i.Valor}</option>`
                })
                contenido += `</optgroup>`
            })
            $(selector).append(contenido)
            return true;
        },
        error: function (jqXHR, status) {
            console.log(`Error URL: ${url} ${JSON.stringify(jqXHR)}`);
            return false;
        }
    });

}
function CargarComboHijo(padre, hijo, funcionCargadoHijo) {

    if ($(padre).val() != '0') {
        $(hijo).removeAttr('disabled');
        //descomentar
        //$(hijo).attr('data-required', 'false');
        funcionCargadoHijo($(hijo), $(padre).val());
    } else {
        $(hijo).attr('disabled', 'disabled');
        $(hijo).val('0');
    }

}

function showModalCargandoComponente(componente) {
    componente.append(
        `<div class="componente-cargando center-horizontal-vertical">
            <i class="fa fa-cog fa-spin fa-3x"></i>
        </div>`);
}

function hideModalCargandoComponente(componente) {
    componente.find('.componente-cargando').remove();
}

function formatDate(dateString) {
    let date = moment(dateString);
    if (date.isValid()) {
        date = date.toDate().format('dd-MM-yyyy');
    } else {
        date = dateString;
    }
    return date;
}

// Validación de tiempo de sesión del Token
function EsValidaSesionToken(element = null) {

    const fechaActual = moment();
    const fechaExpiracionToken = moment(getSession().FECHA_FINAL_SESION);
    if (fechaActual >= fechaExpiracionToken) {

        $("#txtUsuarioLogin").val(getSession().LOGIN_USUARIO);
        $("#mdlLogin").modal('show');

        if (element != null) {

            switch ($(element).prop('nodeName')) {
                case 'INPUT':
                case 'TEXTAREA':
                    $(element).val('');
                    break;
                case 'SELECT':
                    $(element).val('0');
                    break;
            }

        }

        return false;

    }

    return true;
}