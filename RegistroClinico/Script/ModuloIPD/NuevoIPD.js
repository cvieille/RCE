﻿
var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
}

var alertFlag = 0;
var RCE_idEventos = null;

//Javier
function cargarCombos() {
    CargarComboServicioSalud();
    CargarComboEstablecimiento();
    CargarComboEspecialidad();
    CargarComboTipoIPD();
    CargarComboPrevision();
    CargarComboConfirmarDiagnostico();
    comboTipoRepresentante();
    CargarComboAugePadre();
}

function CargarComboServicioSalud() {
    let url = `${GetWebApiUrl()}GEN_Servicio_Salud/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlServicio"));
}
function CargarComboEstablecimiento() {
    let url = `${GetWebApiUrl()}GEN_Establecimiento/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlEstablecimiento"));
}
function CargarComboEspecialidad() {
    let url = `${GetWebApiUrl()}GEN_Especialidad/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlEspecialidad"));
}
function CargarComboTipoIPD() {
    let url = `${GetWebApiUrl()}GEN_Ambito/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlAmbito"))
}
//Datos Clinicos
function CargarComboAugePadre() { 
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlAuge"));
}
function CargarComboAugeHijo(idPadre) {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/${idPadre}/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlSubAuge"));
}

function CargarComboPrevision() { //NO ID NO VALOR
    let url = `${GetWebApiUrl()}GEN_Prevision/Combo`; 
    setCargarDataEnCombo(url, false, $("#ddlPrevision"));
}
function CargarComboConfirmarDiagnostico() { //NO ID NO VALOR
    let url = `${GetWebApiUrl()}RCE_Tipo_IPD/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlTipoIPD"));

}
//Constancia
function comboTipoRepresentante() {
    let url = `${GetWebApiUrl()}/GEN_Tipo_Representante/Combo`;
    setCargarDataEnCombo(url, false, $('#ddlConstanciaRepresentante'));
}

function comboPrevisionTramo(id) {
    let url = `${GetWebApiUrl()}GEN_Prevision_Tramo/GEN_idPrevision/${id}`;
    setCargarDataEnCombo(url, false, $("#ddlPrevisionTramo"));
}
//T

$('#ddlPrevision').change(function () {
    id = $('#ddlPrevision').val();
    comboPrevisionTramo(id);
    $('divPrevisionTramo').show()
});

$(document).ready(function () {
    
    window.addEventListener('beforeunload', bunload, false);
    cargarCombos();

    $(window).scroll(function () {
        if (alertFlag) {
            ($($(this)).scrollTop() > 150) ? $('#alertObligatorios').stop().show('fast') : $('#alertObligatorios').stop().hide('fast');
        }
    });
    
    var sSession = getSession();
    RevisarAcceso(true, sSession);

    $("#switchAuge").bootstrapSwitch();
    $("#switchTitular").bootstrapSwitch();


    if (sSession.CODIGO_PERFIL == 1) {
        $('#ddlPrevisionTramo').removeAttr('data-required');
        $('#divIPDFecha').removeClass('offset-1');
        $('#divIPDFecha').addClass('offset-4');
    }
    $('#divSubAuge').hide();

    $('#txtFechaTrat').val(moment().format("YYYY-MM-DD"));
    $('#txtFechaNac').val(moment().format("YYYY-MM-DD"));

    var iIdIPD = sSession.ID_IPD;
    if (iIdIPD == undefined) {
        iIdIPD = 0;
    }

    $('#ddlPrevision').change(function () {
        //comboPrevisionTramo($(this).val(), 0);
        //$('button[data-id="ddlPrevisionTramo"]').html('Seleccione');
    });

    $('#idIPD').val(iIdIPD);
    
    //Hay ID
    //console.log("id "+iIdIPD);
    if (iIdIPD != 0 && iIdIPD != undefined) {
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_IPD/" + iIdIPD,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                //Servicio de Salud
                $('#ddlServicio').val(data.IdServicioSalud)
                $('#ddlEstablecimiento').val(data.IdEstablecimiento) 
                $('#ddlEspecialidad').val(data.IdEspecialidad) 
                $('#ddlAmbito').val(data.IdAmbito) 
                //Paciente
                CargarPacienteActualizado(data.Paciente.IdPaciente);
                 BloquearPaciente();
                 deleteSession("ID_PACIENTE");
                //Datos Clinicos
                $('#txtDiagnostico').val(data.Diagnostico) 
                $('#txtFundamentoDiag').val(data.Fundamento) 
                $('#txtTratamiento').val(data.Tratamiento) 
                $('#ddlTipoIPD').val(data.IdTipoIPD) 
                $('#txtFechaTrat').val(moment(new Date(data.FechaInicioTratamiento)).format("YYYY-MM-DD"))
                $('#ddlPrevision').val(data.IdPrevision)
                $('#ddlPrevisionTramo').val(data.IdPrevisionTramo)
                $('#txtIPDFecha').val(moment(new Date(data.Fecha)).format("YYYY-MM-DD"))
                $('#txtIPDHora').val(moment(new Date(data.Fecha)).format("HH:mm"))
                //Datos del profesional
                datosProfesional(data.GEN_idProfesional);

                //Datos GES
                if (data.PatologiaGES != null) {
                    $('#switchAuge').bootstrapSwitch('state', true);
                    if (data.PatologiaGES.PatologiaPadre != null) {
                        $('#ddlAuge').val(data.PatologiaGES.PatologiaPadre.IdPatologiaPadre)
                        $('#ddlSubAuge').show();
                        $('#divSubAuge').stop().fadeIn(100);
                        idPadre = $('#ddlAuge').val();
                        CargarComboAugeHijo(idPadre);
                        $('#ddlSubAuge').val(data.PatologiaGES.IdPatologiaGES)
                    } else {
                        $('#ddlAuge').val(data.PatologiaGES.IdPatologiaGES)
                    }
                }
                //IDEvento
                $('#idEvento').val(data.IdEvento);
                setSession("ID_EVENTO", data.IdEvento);

                if (sSession.ID_EVENTO != undefined) {
                    RCE_idEventos = parseInt(sSession.ID_EVENTO);
                    deleteSession("ID_EVENTO");
                }
                //Datos Constancia
                $('#ddlConstanciaRepresentante').val(data.ConstanciaGES[0].TipoRepresentante.IdTipoRepresentante);
                $('#txtRutRep').val(data.ConstanciaGES[0].Representante.NumeroDocumento);
                $('#txtRutDigitoRep').val(data.ConstanciaGES[0].Representante.Digito);
                $('#txtRutRep').removeClass('is-invalid');

                $('#txtConstanciaFecha').val((moment(new Date(data.ConstanciaGES[0].Fecha)).format("YYYY-MM-DD")));
                $('#txtConstanciaHora').val((moment(new Date(data.ConstanciaGES[0].Fecha)).format("HH:mm")));
                $('#txtNombreRep').val(data.ConstanciaGES[0].Representante.Nombre + " "+data.ConstanciaGES[0].Representante.ApellidoPaterno + " "+ data.ConstanciaGES[0].Representante.ApellidoMaterno);
                $('#txtConstanciaProRut').val(data.ConstanciaGES[0].Profesional.NumeroDocumento + "-" + data.ConstanciaGES[0].Profesional.Digito );
                $('#txtConstanciaPro').val(data.ConstanciaGES[0].Profesional.Nombre + " " + data.ConstanciaGES[0].Profesional.ApellidoPaterno + " " + data.ConstanciaGES[0].Profesional.ApellidoMaterno);
                $('#idRepresentante').val(data.ConstanciaGES[0].Representante.IdRepresentante);
                $('#idConstancia').val(data.ConstanciaGES[0].Id);
            }
        });

        

        if (sSession.ID_IPD != undefined)
            deleteSession('ID_IPD');

    } else {
        $('#txtIPDFecha').val(moment().format("YYYY-MM-DD"));
        $('#txtIPDHora').val(moment().format("HH:mm"));        
        //comboEstablecimiento(1, 1);

        var idPro = datosProfesional();
        //comboEspecialidad(idPro, 0);

        if (sSession.ID_EVENTO != undefined) {
            RCE_idEventos = parseInt(sSession.ID_EVENTO);
            deleteSession("ID_EVENTO");
        }

        if (sSession.ID_PACIENTE != undefined) {
            CargarPaciente(sSession.ID_PACIENTE);
            BloquearPaciente();
            deleteSession("ID_PACIENTE");
        }

        if (sSession.ID_IPD != undefined)
            deleteSession('ID_IPD');

        
    }


    $('#txtRutPac').on('change', function (e) {
        if (EsValidoDigitoVerificador($('#txtRutPac').val(), $('#txtRutDigitoPac').val())) {
            $('#spanRutCorrecto').show();
            $('#spanRutIncorrecto').hide();
            //$('#btnEventos').removeClass('disabled');
            //modalEventoAsignar();
        }
        else {
            $('#spanRutCorrecto').hide();
            $('#spanRutIncorrecto').show();
            //$('#btnEventos').addClass('disabled');
        }
    });

    $('#txtRutDigitoPac').on('input', function (e) {
        if (EsValidoDigitoVerificador($('#txtRutPac').val(), $('#txtRutDigitoPac').val())) {
            $('#spanRutCorrecto').show();
            $('#spanRutIncorrecto').hide();
            //$('#btnEventos').removeClass('disabled');
            //modalEventoAsignar();
        } else {
            $('#spanRutCorrecto').hide();
            $('#spanRutIncorrecto').show();
            //$('#btnEventos').addClass('disabled');
        }
    });
    
    $('#txtRutPro').on('change', function (e) {
        if (EsValidoDigitoVerificador($('#txtRutPro').val(), $('#txtRutDigitoPro').val())) {
            $('#spanRutCorrectoPro').show();
            $('#spanRutIncorrectoPro').hide();
            cargarProfesional();
        }
        else {
            $('#spanRutCorrectoPro').hide();
            $('#spanRutIncorrectoPro').show();
        }
    });

    $('#txtRutDigitoPro').on('input', function (e) {
        if (EsValidoDigitoVerificador($('#txtRutPro').val(), $('#txtRutDigitoPro').val())) {
            $('#spanRutCorrectoPro').show();
            $('#spanRutIncorrectoPro').hide();
            cargarProfesional();
        }
        else {
            $('#spanRutCorrectoPro').hide();
            $('#spanRutIncorrectoPro').show();
        }
    });
    
    $('#switchAuge').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $('#divAuge').stop().fadeIn(100);
            //$('#divSubAuge').stop().fadeIn(100);
        }else {
            $('#divAuge').stop().fadeOut(100);
            $('#divSubAuge').stop().fadeOut(100);
            $('#ddlSubAuge').attr("data-required", 'false');
            ReiniciarRequired();
            //$('#ddlAuge').selectpicker('destroy').selectpicker();
            //$('#ddlSubAuge').selectpicker('destroy').selectpicker();
        }
    });
   
    $('#switchTitular').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + "GEN_Paciente/" + paciente.GEN_idPaciente,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (!$.isEmptyObject(data)) {
                        
                        $('#idRepresentante').val(paciente.GEN_idPaciente);
                        $('#txtRutRep').val(data.GEN_numero_documentoPaciente);
                        if (data.GEN_digitoPaciente != null)
                            $('#txtRutDigitoRep').val(data.GEN_digitoPaciente.toUpperCase());

                        var nombre = data.GEN_nombrePaciente + ' ' + data.GEN_ape_paternoPaciente;
                        if (data.GEN_ape_maternoPaciente != null)
                            nombre += ' ' + data.GEN_ape_maternoPaciente;
                        $('#txtNombreRep').val(nombre);
                    }
                }
            });
            $('#lblValidarRutRep').addClass('active');
            $('#spanRutCorrectoRep').show();
            $('#spanRutIncorrectoRep').hide();
            //$('#ddlConstanciaRepresentante').selectpicker('setStyle', 'btn-danger', 'remove').selectpicker('setStyle', 'btn-primary-dark', 'add');

            //borrarDropDown('#ddlConstanciaRepresentante', 0);
            $('#ddlConstanciaRepresentante').empty().append('<option value="0">-Seleccione-</option>').val('0');
            $('#ddlConstanciaRepresentante').attr('disabled', true);
            //$('#ddlConstanciaRepresentante').selectpicker('refresh');

            //$('#txtConstanciaRepresentante').removeClass("invalid");
            $('#txtRutRep').removeClass('is-invalid');
            //$('#txtRutDigitoRep').removeClass('is-invalid');
        }
        else {
            comboTipoRepresentante();
            $('#idRepresentante').val('0');
            $('#txtRutRep').val('');
            $('#txtRutDigitoRep').val('');
            $('#txtNombreRep').val('');
            $('#lblValidarRutRep').removeClass('active');
            $('#spanRutCorrectoRep').hide();
            $('#spanRutIncorrectoRep').show();

            $('#ddlConstanciaRepresentante').removeAttr('disabled');
            //$('#ddlConstanciaRepresentante').selectpicker('refresh');
        }
    });

    $('#txtRutRep').on('click', function () {
        $(this).select();
    });

    $('#txtRutRep').on('input', function () {
        $('#spanRutCorrectoRep').hide();
        $('#spanRutIncorrectoRep').show();
        $('#txtRutDigitoRep').val('');
        $('#txtNombreRep').val('');
        $('#idRepresentante').val('0');
    });

    $('#txtRutRep').on('change', function () {
        if (EsValidoDigitoVerificador($('#txtRutRep').val(), $('#txtRutDigitoRep').val())) {
            $('#spanRutCorrectoRep').show();
            $('#spanRutIncorrectoRep').hide();
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/1/${$('#txtRutRep').val()}`,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (!$.isEmptyObject(data)) {
                        $('#txtNombreRep').val(data[0].GEN_nombrePaciente + ' ' + data[0].GEN_ape_paternoPaciente + ' ' + data[0].GEN_ape_maternoPaciente);
                        $('#idRepresentante').val(data[0].GEN_idPaciente);
                    }
                }
            });
        }
        else {
            $('#spanRutCorrectoRep').hide();
            $('#spanRutIncorrectoRep').show();
        }
    });

    $('#txtRutDigitoRep').on('input', function () {
        if (EsValidoDigitoVerificador($('#txtRutRep').val(), $('#txtRutDigitoRep').val())) {
            $('#spanRutCorrectoRep').show();
            $('#spanRutIncorrectoRep').hide();
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/1/${$('#txtRutRep').val()}`,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (!$.isEmptyObject(data)) {
                        $('#txtNombreRep').val(data[0].GEN_nombrePaciente + ' ' + data[0].GEN_ape_paternoPaciente + ' ' + data[0].GEN_ape_maternoPaciente);
                        $('#idRepresentante').val(data[0].GEN_idPaciente);
                    }
                }
            });
        }
        else {
            $('#spanRutCorrectoRep').hide();
            $('#spanRutIncorrectoRep').show();
        }
    });

    $('#btnContinuar').on('click', function (e) {
        //GetEvento().GEN_idPaciente

        //Validar campos del DIV IPD
        var bValidar = true;
        bValidar = validarCampos('#divIPD', true);

        //console.log(paciente);
        if (paciente.GEN_idPaciente == undefined || paciente.GEN_idPaciente == null || paciente.GEN_idPaciente == 0) {
            $('#txtnumerotPac').addClass('is-invalid');
            bValidar = false;
        }

        //if (fechaDocumento())
        //    bValidar = false;
        //    $('#spanFecha').show();
        
        //if (GetEvento().RCE_idEventos == '0') 
        //    bValidar = false;
        
        if (!$('#switchAuge').bootstrapSwitch('state')){
            //$('#idGrupo').val('0');
            $('#ddlAuge').val(0);
            $('#ddlSubAuge').val(0);
        }

        if (bValidar) {
            //Validación de formatos en campos de telefono y correo
            var flag = 0;

            if (!$("#txtTelefono").val().match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im)) {
                $("#txtTelefono").addClass('invalid-input');
                flag = 1;
            } else {
                $("#txtTelefono").removeClass('invalid-input');
            }
            

            if (!$("#txtOtroTelefono").val().match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im)) {
                $("#txtOtroTelefono").addClass('invalid-input');
                flag = 1;
            } else {
                $("#txtOtroTelefono").removeClass('invalid-input');
            }

            if (!$("#txtCorreoElectronico").val().match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)) {
                $("#txtCorreoElectronico").addClass('invalid-input');
                flag = 1;
            } else {
                $("#txtCorreoElectronico").removeClass('invalid-input');
            }

            if (flag == 1)
                return;

            var idIPD = nuevoIPD();
            //$('#idIPD').val(idIPD);
            //datosPaciente();
            $('#alertObligatorios').hide();

            $('#mdlConstancia').modal('show');
        } else {
            alertFlag = 1;
        }
        e.preventDefault();
    });

    $('#btnNuevaConstancia').on('click', function (e) {
        var bValidar = true;
        alertFlag = 0;

        //Validar campos constancia
        bValidar = validarCampos('#divConstancia', false);
        if ($('#idRepresentante').val() == 0) {
            bValidar = false;
            $('#txtRutRep').addClass('is-invalid');
            //$('#txtRutDigitoRep').addClass('is-invalid');
            //$('#txtConstanciaRepresentante').addClass('invalid');
        } else {
            alertFlag = 1;
        }
        
        //validar 
        if (!$('#switchTitular').bootstrapSwitch('state') && $('#ddlConstanciaRepresentante').val() == '0')
            bValidar = false;
        
        if (bValidar) {
            //$('#btnNuevaConstancia').addClass('disabled');
            $('#btnOmitirConstancia').addClass('disabled');
            $('#btnContinuar').addClass('disabled');

            if ($('#ddlConstanciaRepresentante').val() == '0')
                $('#idRepresentante').val(paciente.GEN_idPaciente);

            var dFecha = moment($('#txtConstanciaFecha').val() + ' ' + $('#txtConstanciaHora').val());
            //alert(dFecha);
            var json = {
                RCE_idConstancia_GES: $('#idConstancia').val(),
                RCE_idIPD: $('#idIPD').val(), //$('#idIPD').val()
                GEN_idProfesional: $('#idProfesional').val(),
                RCE_fechaConstancia_GES: moment(dFecha).format('YYYY-MM-DD HH:mm:ss'),
                GEN_id_representantePaciente: $('#idRepresentante').val(),
                GEN_idTipo_Representante: $('#ddlConstanciaRepresentante').val(),
                GEN_idTipo_Estados_Sistemas: 65,
                RCE_estadoConstancia_GES: 'Activo'
            };
            
            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + "RCE_IPD/" + iIdIPD,
                contentType: "application/json",
                dataType: "json",
                success: function (dataConst) {


                    if (iIdIPD == 0) { //nuevo IPD, crea constancia
                        nuevaConstancia('POST', json, '', iIdIPD); 
                    } else {
                        if (!$.isEmptyObject(dataConst.ConstanciaGES)) { //Modifica 
                            json.GEN_idTipo_Estados_Sistemas = 66;
                           nuevaConstancia('PUT', json, dataConst.ConstanciaGES[0].Id , iIdIPD);

                        } else { //Existe IPD pero no constancia
                           nuevaConstancia('POST', json, '', iIdIPD); 
                        }
                    }
                }
            });
            
        } else {
            alertFlag = 1;
        }
        e.preventDefault();
    });

    $('#btnOmitirConstancia').on('click', function (e) {
        var bValidar = true;
        
        $('#btnNuevaConstancia').addClass('disabled');
        $('#btnOmitirConstancia').addClass('disabled');
        $('#btnContinuar').addClass('disabled');
        
        if (iIdIPD == 0)
            //toastr.success('IPD INGRESADO CORRECTAMENTE');
            toastr.success('IPD INGRESADO CORRECTAMENTE', '', {
                onHidden: function () {
                    quitarListener();
                    window.location.replace(ObtenerHost() + "/Vista/ModuloIPD/BandejaIPD.aspx");
                }
            });
        else
            toastr.success('IPD ACTUALIZADO CORRECTAMENTE', '', {
                onHidden: function () {
                    quitarListener();
                    window.location.replace(ObtenerHost() + "/Vista/ModuloIPD/BandejaIPD.aspx");
                }
            })
        e.preventDefault();
    });
    /*
    $('#mdlConstancia').on("shown.bs.modal", function () {
        $('#txtConstanciaPro').val($('#txtNombrePro').val() + ' ' + $('#txtApellidoPPro').val() + ' ' + $('#txtApellidoMPro').val());
        $('#txtConstanciaProRut').val($('#txtRutPro').val() + '-' + $('#txtRutDigitoPro').val());

        var id = $('#idIPD').val();

        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "RCE_Constancia_GES/RCE_IPD/" + id,
            contentType: "application/json",
            dataType: "json",
            success: function (dataConst) {
                if ($.isEmptyObject(dataConst[0])) {
                    $('#idConstancia').val(0);

                    $('#txtConstanciaFecha').val(moment().format("YYYY-MM-DD"));
                    $('#txtConstanciaHora').val(moment().format("HH:mm"));
                } else {
                    $('#idConstancia').val(dataConst[0].RCE_idConstancia_GES);

                    $.ajax({
                        type: 'GET',
                        url: GetWebApiUrl() + "GEN_Profesional/" + dataConst[0].GEN_idProfesional,
                        contentType: "application/json",
                        dataType: "json",
                        success: function (dataPro) {
                            $('#txtConstanciaPro').val(dataPro.GEN_nombreProfesional + ' ' + dataPro.GEN_apellidoProfesional + ' ' + dataPro.GEN_sapellidoProfesional);
                            $('#txtConstanciaProRut').val(dataPro.GEN_rutProfesional + '-' + dataPro.GEN_digitoProfesional);
                        }
                    });                    
                    if (dataConst[0].GEN_idTipo_Representante == undefined) {
                        $('#switchTitular').bootstrapSwitch('state', true);
                        $('#ddlConstanciaRepresentante').val(0);
                        $('#idRepresentante').val(0);
                    }
                    else {
                        $('#switchTitular').bootstrapSwitch('state', false);
                        $('#ddlConstanciaRepresentante').val(dataConst[0].GEN_idTipo_Representante);
                        $('#idRepresentante').val(dataConst[0].GEN_id_representantePaciente);
                    }
                    //console.log(dataConst);
                    $.ajax({
                        type: 'GET',
                        url: GetWebApiUrl() + "GEN_Paciente/" + dataConst[0].GEN_id_representantePaciente,
                        contentType: "application/json",
                        dataType: "json",
                        success: function (dataPac) {
                            $('#txtRutRep').val(dataPac.GEN_numero_documentoPaciente);
                            $('#txtRutDigitoRep').val(dataPac.GEN_digitoPaciente);
                            $('#txtNombreRep').val(dataPac.GEN_nombrePaciente + ' ' + dataPac.GEN_ape_paternoPaciente + ' ' + dataPac.GEN_ape_maternoPaciente);

                            $('#spanRutCorrectoRep').show();
                            $('#spanRutIncorrectoRep').hide();
                            $('#lblValidarRutRep').addClass('active');
                            //$('#txtConstanciaRepresentante').val(dataPac.GEN_numero_documentoPaciente + '-' + dataPac.GEN_digitoPaciente + ' ' + dataPac.GEN_nombrePaciente + ' ' + dataPac.GEN_ape_paternoPaciente + ' ' + dataPac.GEN_ape_maternoPaciente);
                            //$('#lblConstanciaRepresentante').addClass('active');
                            $('#idRepresentante').val(dataConst[0].GEN_id_representantePaciente);
                        }
                    });
                    $('#txtConstanciaFecha').val(moment(new Date(dataConst[0].RCE_fechaConstancia_GES)).format("YYYY-MM-DD"));
                    $('#txtConstanciaHora').val(moment(new Date(dataConst[0].RCE_fechaConstancia_GES)).format("HH:mm"));
                }
            }
        });        
    });
   */
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 2500,
        "extendedTimeOut": 1500,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }

    ShowModalCargando(false);

});

function fechaDocumento()
{
    var date1 = moment($('#txtFechaTrat').val()).format("YYYY-MM-DD");
    var date2 = moment($('#fechaEvento').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
    
    if (date1 < date2)
        return true;
    else
        return false;
}

function nuevaConstancia(sMethod, json, idGES, idIPD)
{
    if (json.GEN_idTipo_Representante == 0)
        json.GEN_idTipo_Representante = null;


    $.ajax({
        type: sMethod,
        url: GetWebApiUrl() + "RCE_Constancia_GES/" + idGES,
        data: JSON.stringify(json),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (data, status, jqXHR) {
            if (idIPD == 0)
                toastr.success('IPD INGRESADO CORRECTAMENTE', '', {
                    onHidden: function () {
                        quitarListener();
                        window.location.replace(ObtenerHost() + "/Vista/ModuloIPD/BandejaIPD.aspx");
                    }
                });
            else 
                toastr.success('IPD ACTUALIZADO CORRECTAMENTE', '', {
                    onHidden: function () {
                        quitarListener();
                        window.location.replace(ObtenerHost() + "/Vista/ModuloIPD/BandejaIPD.aspx");
                    }
                });
        },
        error: function (jqXHR, status) {
            console.log(jqXHR);
            alert('fail2' + status.code);
        }
    });
}

function cargarProfesional()
{
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Profesional/BuscarporDocumento?GEN_rutProfesional=" + $('#txtRutPro').val(),
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            if ($.isEmptyObject(data[0])) {
                alert('El profesional no existe en la base de datos');
                $('#idProfesional').val(0);
                $('#txtNombrePro').val('');
                $('#txtApellidoPPro').val('');
                $('#txtApellidoMPro').val('');
                $('#txtRutPro').val('');
                $('#txtRutDigitoPro').val('')
            }
            else {
                $('#idProfesional').val(data[0].GEN_idProfesional);
                $('#txtNombrePro').val(data[0].GEN_nombreProfesional);
                $('#txtApellidoPPro').val(data[0].GEN_apellidoProfesional);
                $('#txtApellidoMPro').val(data[0].GEN_sapellidoProfesional);
            };
        }
    });
}

function nuevoIPD() {
    var dFecha = moment($('#txtIPDFecha').val() + ' ' + $('#txtIPDHora').val());

    var idIPD = $('#idIPD').val();
    var idGrupo = $('#ddlAuge').val();
    var idSubGrupo = $('#ddlSubAuge').val();
    //if (idGrupo == 0) 
    //    idGrupo = null;
    if (idSubGrupo == 0)
        idSubGrupo = null;
    var json = {
        IdIPD: parseInt(idIPD),
        Fecha: moment(dFecha).format('YYYY-MM-DD HH:mm:ss'),
        IdEstablecimiento: parseInt($('#ddlEstablecimiento').val()),
        IdEspecialidad: parseInt($('#ddlEspecialidad').val()),
        Diagnostico: $('#txtDiagnostico').val(),
        Fundamento: $('#txtFundamentoDiag').val(),
        Tratamiento: $('#txtTratamiento').val(),
        InicioTratamiento: '2022-02-01 22:00:30', //moment(new Date($('#txtFechaTrat').val())).add(1, 'days').format('YYYY-MM-DD'),
        IdPaciente: GetPaciente().GEN_idPaciente,
        IdAmbito: parseInt($('#ddlAmbito').val()),
        IdTipoIPD: parseInt($('#ddlTipoIPD').val()), 
        IdProfesional: parseInt($('#idProfesional').val()),
        IdPrevision: parseInt($('#ddlPrevision').val()) ?? null, 
        IdPrevisionTramo: parseInt($('#ddlPrevisionTramo').val()) ?? null, 
        IdPatologiaGES: idGrupo == '0' ? null : parseInt(idGrupo)
    };


        //RCE_idGrupo_Patologia_GES: idGrupo,
        //RCE_idSubgrupo_Patologia_GES: idSubGrupo,
        //GEN_idTipo_Estados_Sistemas: 63,           ACTIVO
       // RCE_estadoIPD: 'Activo',                   ACTIVO
        // GEN_idPrevision: $('#ddlPrevision').val(),
        //GEN_idPrevision_Tramo: $('#ddlPrevisionTramo').val() == '0' ? null : $('#ddlPrevisionTramo').val(),
    
    
    if (RCE_idEventos != null) {
        json.IdEventos = RCE_idEventos;
    }
    if (idSubGrupo != '' && idSubGrupo != null) {
        json.IdPatologiaGES = idSubGrupo;
    }


    

    if (idIPD == 0) {
        $.ajax({
            type: 'POST',
            url: `${GetWebApiUrl()}RCE_IPD`,
            data: JSON.stringify(json),
            contentType: 'application/json; charset=utf-8',
            crossDomain: true,
            dataType: 'json',
            async: false,
            success: function (data, status, jqXHR) {
                var iIdIPD = data.IdIPD;
                $('#idIPD').val(data.IdIPD); 
            },
            error: function (jqXHR, status) {
                console.log(jqXHR);
                alert(`4Error: ${jqXHR.message}`);
            }
        });

        

    }else {
        json.IdEventos = RCE_idEventos;
        $.ajax({
            type: 'PUT',
            url: GetWebApiUrl() + "RCE_IPD/" + idIPD,
            data: JSON.stringify(json),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, status, jqXHR) {
                //alert(data);
            },
            error: function (jqXHR, status) {
                console.log(jqXHR);
                alert('fail5' + status.code);
            }
        });
    }
    return idIPD;
}

function datosProfesional()
{
    var iIdPro;
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
        async: false,
        success: function (data) {
            iIdPro = data[0].GEN_idProfesional;
        }
    });

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Profesional/" + iIdPro,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $('#idProfesional').val(data.GEN_idProfesional)
            $('#txtRutPro').val(data.GEN_rutProfesional);
            $('#txtRutDigitoPro').val(data.GEN_digitoProfesional);
            $('#txtNombrePro').val(data.GEN_nombreProfesional);
            $('#txtApellidoPPro').val(data.GEN_apellidoProfesional);
            $('#txtApellidoMPro').val(data.GEN_sapellidoProfesional);


            $('#txtConstanciaPro').val(sSession.NOMBRE_USUARIO);
            $('#txtConstanciaProRut').val(data.GEN_rutProfesional + "-" + data.GEN_digitoProfesional);

            $('#spanRutIncorrectoPro').hide();
            $('#spanRutCorrectoPro').show();
        }
    });
    return iIdPro;
}

function fechaHoy()
{
    //https://stackoverflow.com/questions/1531093/how-do-i-get-the-current-date-in-javascript
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) 
        dd = '0' + dd
    if (mm < 10) 
        mm = '0' + mm
    return (yyyy + '-' + mm + '-' + dd);
}

$('#ddlAuge').change(function () {
    idPadre = $('#ddlAuge').val()
    CargarComboAugeHijo(idPadre)

    select = document.querySelector("#ddlSubAuge");
    //console.log(select.options.length)
    if (select.options.length == 1) { //No tiene valores 
        $('#divSubAuge').hide()
        $('#ddlSubAuge').attr("data-required", 'false');
        ReiniciarRequired();
    } else {
        $('#divSubAuge').show()
        $('#ddlSubAuge').attr("data-required", 'true');
        ReiniciarRequired();
    }
});
