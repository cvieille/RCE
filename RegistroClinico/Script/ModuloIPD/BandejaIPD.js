﻿var idDiv;
var sSession = null;


function CargarComboGrupoPatologiaGES() {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlAuge"));
}
function CargarComboAugeHijo(idPadre) {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/${idPadre}/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlSubGrupo"));
}

$('#ddlAuge').change(function () {
    idPadre = $('#ddlAuge').val()
    CargarComboAugeHijo(idPadre)

    select = document.querySelector("#ddlSubGrupo");
    //console.log(select.options.length)
    if (select.options.length == 1) { //No tiene valores 
        $('#divSubGrupo').hide()
    } else {
        $('#divSubGrupo').show()
    }
});
$(document).ready(function ()
{
    sSession = getSession();
    RevisarAcceso(false, sSession);
    DarFuncionRadios();

    comboEstadoIPD();
    CargarComboGrupoPatologiaGES();
    $('#divSubGrupo').hide()
    //comboGrupo('#ddlAuge', 0);
    //comboGarantia();

    idDiv = "";

    comboTipoRepresentante();
    getTablaIPD(sSession);

    if (sSession.CODIGO_PERFIL == "2" || sSession.CODIGO_PERFIL == "8" || sSession.CODIGO_PERFIL == "9") {
        $('#lnbNuevoIPD').hide();
        $('#divProfesional').show();
        comboProfesional();
    }
    else {
        $('#divProblemaSalud').addClass('offset-1');
    }

    $('#btnExcepcion').on('click', function (e) {
        var bValidar = true;
        
        if ($('#txtSeguimientoFecha').val() == null || $('#txtSeguimientoFecha').val() == undefined || $('#txtSeguimientoFecha').val() == '') {
            $('#txtSeguimientoFecha').addClass('invalid');
        }

        bValidar = validarCampos('#divExcepcion', false);
        //revalidar? luego del destroy no pasa por el método de exequiel
        if (bValidar) {
            $('#txtSeguimientoFecha').removeClass('invalid');

            var id = $('#idIPD').val();
            var idExc = $('#idExcepcion').val();
            var fecha = moment($('#fechaExcepcion').val(), 'DD/MM/YYYY').format('YYYY-MM-DD HH:mm:ss');

            var json = {
                RCE_idIPD_Excepcion: idExc,
                RCE_fechaIPD_Excepcion: fecha,
                RCE_fechaSeguimientoIPD_Excepcion: moment($('#txtSeguimientoFecha').val()).format('YYYY-MM-DD'),
                RCE_observacionIPD_Excepcion: $('#txtObservacion').val(),
                RCE_idGarantias_GES: $('#ddlGarantia').prop('value'),
                RCE_idIPD: id,
                RCE_estadoIPD_Excepcion: 'Activo'
            };

            if (!$('#switchSeguimiento').bootstrapSwitch('state')) {
                json.RCE_fechaSeguimientoIPD_Excepcion = null;
            }
            //console.log(json);

            var sMetodo;
            if (idExc == '0') {
                sMetodo = 'POST';
                idExc = '';
                json.RCE_fechaIPD_Excepcion = moment().format('YYYY-MM-DD HH:mm:ss');
            }
            else
                sMetodo = 'PUT';

            $.ajax({
                type: sMetodo,
                url: GetWebApiUrl() + 'RCE_IPD_Excepcion/' + idExc,
                data: JSON.stringify(json),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: false,
                success: function (data, status, jqXHR) {
                }
                //,error: function (jqXHR, status) {
                //    console.log(jqXHR);
                //    alert('fail' + status.code);
                //}
            });
            limpiarExcepcion();
            grillaExcepcion(id);
            idExc = '0';
            $('#idExcepcion').val('0')
            $('#fechaExcepcion').val('0')
        }

        e.preventDefault();
    });
    $('#switchSeguimiento').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $('#divSeguimiento').stop().fadeIn(100);
            $('#txtSeguimientoFecha').val(moment().add(1, 'M').format("YYYY-MM-DD"));
        } else {
            $('#divSeguimiento').stop().fadeOut(100);
        }
    });

    $('#btnFiltro').on('click', function (e) {
        getTablaIPD(sSession)
        e.preventDefault();
    });
    $('#btnLimpiarFiltro').on('click', function (e) {
        limpiarFiltros();
        $('#btnFiltro').click();
        e.preventDefault();
    });

    //$('#txtObservacion').keypress(function (e) {
    //    if (String.fromCharCode(e.which).match(/[^A-Za-z0-9 ]/))
    //        e.preventDefault();
    //});
    
    /*
    $('#ddlAuge').change(function () {
        //$('button[data-id="ddlSubGrupo"]').html('Seleccione');
        if ($(this).val() == '0')
            $('#ddlSubGrupo').empty().append("<option value='0'>-Seleccione-</option>").val('0');
        else
            comboGrupo('#ddlSubGrupo', $(this).val());
    });
    */
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 3000,
        "extendedTimeOut": 1500,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }

    $('.enterForm').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#btnFiltro').click();
            e.preventDefault();
        }
    });

    $('.enterExcepcion').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#btnExcepcion').click();
            e.preventDefault();
        }
    });

    //if (!$.isEmptyObject(sSession.curIPD))
    //{
    //    if (sSession.curIPD == 0)
    //        toastr.success('IPD INGRESADO CORRECTAMENTE');
    //    else
    //        toastr.success('IPD ACTUALIZADO CORRECTAMENTE');
    //}

    //deleteSession('curIPD');
    deleteSession('ID_IPD');
});

function limpiarFiltros() {
    $('#txtFiltroRut').val('');
    $('#txtFiltroNombre').val('');
    $('#txtFiltroApe').val('');
    $('#txtFiltroSApe').val('');

    $('#ddlMedicoIPD').empty().append("<option value='0'>-Seleccione-</option>").val('0');
    $('#ddlAuge').val('0');
    $("#ddlSubGrupo").empty().append("<option value='0'>-Seleccione-</option>").val('0');
    $("#ddlEstadoIPD").val('0');

    comboProfesional();

    //$('button[data-id="ddlMedicoIPD"]').html('Seleccione');
    //$('button[data-id="ddlAuge"]').html('Seleccione');
    //$('button[data-id="ddlSubGrupo"]').html('Seleccione');
    //$('button[data-id="ddlEstadoIPD"]').html('Seleccione');
}

function comboProfesional()
{
    $('#ddlMedicoIPD').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_Ingreso_Medico/Medicos`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (i, r) {
                $('#ddlMedicoIPD').append("<option value='" + r.GEN_idProfesional + "'>" + r.GEN_nombrePersonas + "</option>");
            });
            //$('#ddlMedicoIPD').selectpicker('refresh');
        }
    });
}

function comboEstadoIPD() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Estados_Sistemas/IPD/combo`;
    setCargarDataEnCombo(url, false, $("#ddlEstadoIPD"))
}

function comboGarantia()
{
    $('#ddlGarantia').empty();
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'RCE_Garantias_GES/Combo',
        //data: JSON.stringify({ sid: id }),
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            //console.log(data);
            $('#ddlGarantia').append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (i, r) {
                $('#ddlGarantia').append("<option value='" + r.RCE_idGarantias_GES + "'>" + r.RCE_descripcionGarantias_GES + "</option>");
            });
        }
    });
}

function comboGrupo(elem, id) {    
    $(elem).empty();
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_Patologia_GES/Combo?RCE_idPatologia_GESPadre=" + id,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $(elem).append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (i, r) {                
                $(elem).append("<option value=" + r.RCE_idPatologia_GES + ">" + r.RCE_descripcion_Patologia_GES + "</option>");
            });
        }
    });
}
function comboTipoRepresentante()
{
    ddlConstanciaRepresentante
    let url = `${GetWebApiUrl()}GEN_Tipo_Representante/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlConstanciaRepresentante"))
}

function linkVerEditar(e)
{
    var id = $(e).data("id");

    setSession('ID_IPD', id);
    //quitarListener();
    window.location.replace(ObtenerHost() + "/Vista/ModuloIPD/NuevoIPD.aspx");    
}

function linkVerConstancia(e)
{
    $('#switchTitular').bootstrapSwitch('disabled', false);
    var id = $(e).data("id");
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_IPD/" + id,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (dataConst) {
            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + "GEN_Profesional/" + dataConst.ConstanciaGES[0].Profesional.Id,
                contentType: "application/json",
                dataType: "json",
                success: function (dataPro) {
                    $('#txtConstanciaPro').val(dataPro.GEN_nombreProfesional + ' ' + dataPro.GEN_apellidoProfesional + ' ' + dataPro.GEN_sapellidoProfesional);
                    $('#txtConstanciaProRut').val(dataPro.GEN_rutProfesional + '-' + dataPro.GEN_digitoProfesional);
                }
            });
            if (dataConst.ConstanciaGES == undefined || dataConst.ConstanciaGES == null) {
                $('#switchTitular').bootstrapSwitch('state', true);
                $('#divRepresentante').hide();
                //$('#ddlConstanciaRepresentante').prop('value', 0).selectpicker('refresh');
                //asignarDropDown('#ddlConstanciaRepresentante', 0);
            }
            else {
                $('#divRepresentante').show();
                $('#switchTitular').bootstrapSwitch('state', false);
                //asignarDropDown('#ddlConstanciaRepresentante', dataConst[0].GEN_idTipo_Representante);
                $('#ddlConstanciaRepresentante').val(dataConst.ConstanciaGES[0].TipoRepresentante.IdTipoRepresentante);
            }
            var representante = dataConst.ConstanciaGES[0].Representante
            $('#txtConstanciaRepresentante').val(representante.Nombre + ' ' + representante.ApellidoPaterno + ' ' + representante.ApellidoMaterno);
            $('#txtConstanciaFecha').val(moment(moment(dataConst.ConstanciaGES[0].Fecha).toDate()).format("YYYY-MM-DD"));
            $('#txtConstanciaHora').val(moment(moment(dataConst.ConstanciaGES[0].Fecha).toDate()).format("HH:mm:ss"));
        }
    });
    $('#switchTitular').bootstrapSwitch('disabled', true);

    $('#mdlConstancia').modal('show');
}

function linkImprimir(e) {
    
    var id = $(e).data("id");
    //setSession('ID_IPD', id);
    $('#modalCargando').show();
    $('#frameIPD').attr('src', 'ImprimirIPD.aspx?id=' + id);
    $('#mdlImprimir').modal('show');
}

function grillaExcepcion(id)
{
    var adataset = [];
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'RCE_IPD_Excepcion/RCE_idIPD/' + id,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $.each(data, function (i, r) {

                var sDate = '';

                if (r.RCE_fechaSeguimientoIPD_Excepcion != null) {
                    sDate = moment(moment(r.RCE_fechaSeguimientoIPD_Excepcion).toDate()).format('DD-MM-YYYY');
                }

                adataset.push([
                    r.RCE_idIPD_Excepcion,
                    moment(moment(r.RCE_fechaIPD_Excepcion).toDate()).format('DD-MM-YYYY hh:mm:ss'),                   
                    sDate,
                    r.HOS_dias_vencimientoIPD_Excepcion,
                    r.RCE_descripcionGarantias_GES,
                    SaltoDeLinea(r.RCE_observacionIPD_Excepcion, 5)                    
                ]);

            });
        }
    });
    
    $('#tblExcepcion').DataTable({
        dom: 'ti',
        data: adataset,
        order: [],
        columnDefs: [
            {
                "targets": 0,
                "visible": false
            },
            {
                "targets": [1],
                "sType": "date-ukLong"
            },
            {
                "targets": [2],
                "sType": "date-ukShort"
            },
            {
                "targets": -1,
                "data": null,
                orderable: false,
                "render": function (data, type, row, meta) {
                    var fila = meta.row;
                    var botones;
                    botones = "<a id='linkVerEditar' data-id='" + adataset[fila][0] + "' data-fecha='" + adataset[fila][1] + "' class='btn btn-info btn-block' href='#/' onclick='linkEditarExc(this)'><i class='fa fa-edit'></i> Editar</a>";
                    return botones;
                }
            },
        ],
        columns: [
            { title: "idExcp" },
            { title: "Fecha Excepción" },
            { title: "Fecha de Seguimiento" },
            { title: "Días de vencimiento" },
            { title: "Garantía" },
            { title: "Observación" },
            { title: "" }
        ],

        "bDestroy": true
    });
}

function linkEditarExc(e)
{
    var id = $(e).data("id");
    var fecha = $(e).data("fecha");
    limpiarExcepcion();
    
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "RCE_IPD_Excepcion/" + id,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $('#idExcepcion').val(id);
            $('#fechaExcepcion').val(fecha);
            //console.log(fecha);            

            $('#ddlGarantia').val(data.RCE_idGarantias_GES);
            //$('button[data-id="ddlGarantia"]').html(data.RCE_Garantias_GES.RCE_descripcionGarantias_GES);
            //$('button[data-id="ddlGarantia"]').removeClass('btn-danger').selectpicker('refresh');


            $('#txtObservacion').val(data.RCE_observacionIPD_Excepcion);
            $('#txtObservacion').removeClass('invalid');
            $('#lbltxtObervacion').addClass('active');
            if (data.RCE_fechaSeguimientoIPD_Excepcion == null) {
                $('#switchSeguimiento').bootstrapSwitch('state', false);
                $('#divSeguimiento').hide();
            }
            else {
                $('#switchSeguimiento').bootstrapSwitch('state', true);
                $('#divSeguimiento').show();
                $('#txtSeguimientoFecha').val(moment(new Date(data.RCE_fechaSeguimientoIPD_Excepcion)).format('YYYY-MM-DD'));
            }
            $('#btnExcepcion').text('Editar Excepción');
        }
    });
}

function linkExcepcion(e) {
    var id = $(e).data("id");

    $('#idIPD').val(id);

    limpiarExcepcion();
    grillaExcepcion(id);

    $('#mdlExcepcion').modal('show');
}

function limpiarExcepcion()
{
    //$('button[data-id="ddlGarantia"]').removeClass('btn-danger').selectpicker('refresh');
    //$('button[data-id="ddlGarantia"]').html('Seleccione');
    //$('#ddlGarantia').selectpicker('setStyle', 'btn-danger', 'remove').selectpicker('setStyle', 'btn-primary-dark', 'add');
    //$('#ddlGarantia').removeClass('is-invalid').selectpicker('refresh');
    //$('#ddlGarantia').prop('value', 0).selectpicker('destroy');
    comboGarantia();
    $('#divSeguimiento').hide();
    $('#switchSeguimiento').bootstrapSwitch('state', false);
    $('#txtSeguimientoFecha').val(moment().add(1, 'M').format("YYYY-MM-DD"));
    $('#txtObservacion').val('');
    $('#txtObservacion').removeClass('invalid');
    $('#btnExcepcion').text('Agregar Excepción');
}

function linkMovimientos(e) {
    var id = $(e).data("id");
    setSession('ID_IPD', id);
    var adataset = [];
    var id = $(e).data("id");
    $.ajax({
        type: "GET",
        url: GetWebApiUrl() + "RCE_IPD/"+id+"/Movimientos",
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $.each(data, function (i, r) {
                adataset.push([
                    r.id,
                    moment(new Date(r.fecha)).format('DD-MM-YYYY hh:mm:ss'),
                    r.usuario,
                    r.descripcion
                ]);
            });
        }
    });

    if (adataset.length == 0)
        $('#lnkExportarMov').addClass('disabled');
    else
        $('#lnkExportarMov').removeClass('disabled');

    $('#tblMovimientos').DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: "RCE_idMov_IPD" },
            { title: "Fecha Movimiento" },
            //{ title: "GEN_idUsuarios" },
            { title: "Usuario" },
            { title: "Movimiento" }
        ],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false
            },
            { "targets": 1, "sType": "date-ukLong" }
        ],
        "bDestroy": true
    });
    $('#mdlMovimiento').modal('show');
}

function validarIPDConfirma(id) {
    $('#mdlAlertaCuerpo').empty();
    $('#mdlAlertaCuerpo').append('¿Esta seguro de validar el IPD?');
    $('#linkConfirmar').attr('onclick', 'validarIPD(' + id + ')');
    $('#linkConfirmar').addClass('btn-success');
    $('#mdlAlerta').modal('show');
}
function validarIPD(id) {
    $.ajax({
        type: 'PUT',
        url: GetWebApiUrl() + "RCE_IPD/Validar/" + id,
        async: false,
        success: function (status, jqXHR) {
            //var sSession = getSession();
            //if (sSession.ID_PROFESIONAL == undefined)
            //    RevisarAcceso(true, null);
                //window.location.replace(ObtenerHost() + "/Vista/Default.aspx");


            getTablaIPD(sSession);
            toastr.success('IPD VALIDADO CORRECTAMENTE');
            $('#mdlAlerta').modal('hide');
        },
        error: function (jqXHR, status) {
            var mensaje = jqXHR.responseJSON.Message;
            toastr.error('ERROR AL VALIDAR IPD <br/>' +  mensaje);
        }
    });
}
function subirIPD(id){
    $.ajax({
        type: 'PUT',
        url: GetWebApiUrl() + "RCE_IPD/Subir/" + id,
        async: false,
        success: function (status, jqXHR) {
            //var sSession = getSession();
            getTablaIPD(sSession);
            toastr.success('IPD SUBIDO CORRECTAMENTE');
        },
        error: function (jqXHR, status) {
            var mensaje = jqXHR.responseJSON.Message;
            toastr.error('ERROR AL SUBIR IPD <br/>' + mensaje);
        }
    });
}
function bajarIPD(id){
    $.ajax({
        type: 'PUT',
        url: GetWebApiUrl() + "RCE_IPD/Bajar/" + id,
        async: false,
        success: function (status, jqXHR) {
            //var sSession = getSession();
            getTablaIPD(sSession);
            toastr.success('IPD BAJADO CORRECTAMENTE');
        },
        error: function (jqXHR, status) {
            var mensaje = jqXHR.responseJSON.Message;
            toastr.error('ERROR AL BAJAR IPD <br/>' + mensaje);
        }
    });
}
function cerrarIPDConfirma(id) {
    $('#mdlAlertaCuerpo').empty();
    $('#mdlAlertaCuerpo').append('¿Esta seguro de cerrar el IPD?');
    $('#linkConfirmar').attr('onclick', 'cerrarIPD(' + id + ')');
    $('#linkConfirmar').addClass('btn-danger');
    $('#mdlAlerta').modal('show');
}
function cerrarIPD(id){
    $.ajax({
        type: 'PUT',
        url: GetWebApiUrl() + "RCE_IPD/Cerrar/" + id,
        async: false,
        success: function (status, jqXHR) {
            //var sSession = getSession();
            getTablaIPD(sSession);
            toastr.success('IPD CERRADO CORRECTAMENTE');
            $('#mdlAlerta').modal('hide');
        },
        error: function (jqXHR, status) {
            var mensaje = jqXHR.responseJSON.Message;
            toastr.error('ERROR AL CERRAR IPD <br/>' + mensaje);
        }
    });
}
function desvalidarIPD(id){
    $.ajax({
        type: 'PUT',
        url: GetWebApiUrl() + "RCE_IPD/Desvalidar/" + id,
        async: false,
        success: function (status, jqXHR) {
            //var sSession = getSession();
            getTablaIPD(sSession);
            toastr.success('IPD DESVALIDADO CORRECTAMENTE');
        },
        error: function (jqXHR, status) {
            var mensaje = jqXHR.responseJSON.Message;
            toastr.error('ERROR AL DESVALIDAR IPD <br/>' + mensaje);
        }
    });
}
function abrirIPD(id){
    $.ajax({
        type: 'PUT',
        url: GetWebApiUrl() + "RCE_IPD/Reabrir/" + id,
        async: false,
        success: function (status, jqXHR) {
            //var sSession = getSession();
            getTablaIPD(sSession);
            toastr.success('IPD ABIERTO CORRECTAMENTE');
        }, 
        error: function (jqXHR, status) {
             var mensaje = jqXHR.responseJSON.Message;
            toastr.error('ERROR AL ABRIR IPD <br/>' + mensaje);
        }
    });
}
function anularIPDConfirma(id) {
    $('#mdlAlertaCuerpo').empty();
    $('#mdlAlertaCuerpo').append('¿Esta seguro de anular el IPD?');
    $('#linkConfirmar').attr('onclick', 'anularIPD(' + id + ')');
    $('#linkConfirmar').addClass('btn-danger');
    $('#mdlAlerta').modal('show');
}
function anularIPD(id) {
    $.ajax({
        type: 'DELETE', //PUT
        url: GetWebApiUrl() + "RCE_IPD/Anular/" + id,
        async: false,
        success: function (status, jqXHR) {
            
            getTablaIPD(sSession);
            $('#mdlAlerta').modal('hide');
            toastr.success('IPD ANULADO');
        },
        error: function (jqXHR, status) {
            toastr.error('ERROR AL ANULAR IPD');
            console.log(jqXHR.responseText);
        }
    });
}

function toggle(id) {

    if (idDiv == "") {

        $(id).fadeIn();
        idDiv = id;

    } else if (idDiv == id) {

        $(id).fadeOut();
        idDiv = "";

    } else {

        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }

}

function LlenaGrillaIPD(datos, grilla, sSession) {



    if (datos.length == 0) 
        $('#lnbExportar').addClass('disabled');
    else
        $('#lnbExportar').removeClass('disabled');

    $(grilla).addClass("nowrap").addClass("tblBandeja").addClass("dataTable").DataTable({
        data: datos,
        columns: [
            { title: "ID IPD", className: "text-center" },
            { title: "Fecha IPD", className: "text-center" },
            { title: "Paciente", className: "text-center" },
            { title: "Patología GES", className: "text-center" },
            { title: "RCE_idTipo_IPD", className: "text-center" },
            { title: "Tipo IPD", className: "text-center" },
            { title: "GEN_idTipo_Estados_Sistemas", className: "text-center" },
            { title: "Estado IPD", className: "text-center" },
            { title: "RCE_idConstancia_GES", className: "text-center" },
            { title: "", className: "text-center" },

         /*10*/   { title: 'editable' },
         /*11*/   { title: 'anulable' },
         /*12*/   { title: 'ver-constancia' },
         /*13*/   { title: 'ver-excepcion' },
         /*14*/   { title: 'imprimir' },
         /*15*/   { title: 'ver-movimientos' },
         /*16*/   { title: 'validar-caso' },
         /*17*/   { title: 'subir-caso' },
         /*18*/   { title: 'bajar-caso' },
         /*19*/   { title: 'cerrar-caso' },
         /*20*/   { title: 'desvalidar-caso' },
         /*21*/   { title: 'abrir-caso' }
        ],
        "columnDefs": [
            { "targets": 1, "sType": "date-ukShort" },
            { targets: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21], visible: false, searchable: false },
            {
                "targets": 9,
                "data": null,
                orderable: false,
                "render": function (data, type, row, meta) {
                    var fila = meta.row;
                    var botones;
                    if (datos[fila][6] == 79) { //ANULADO MUESTRA BOTON BLOQUEADO Y NO CARGA BOTONES
                        botones = `<button class='btn btn-primary disabled btn-circle'><i class="fa fa - list" style="font - size: 18px;"></i></button>"`;
                    } else {
                        var botones =
                            `
                                    <button class='btn btn-primary btn-circle' 
                                onclick='toggle("#div_accionesIPD${fila}"); return false;'>
                                <i class="fa fa-list" style="font-size:15px;"></i>
                            </button>
                                <div id='div_accionesIPD${fila}' class='btn-container' style="display: none;">
                                   <center>
                                        <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                        <div class="rounded-actions">
                                            <center>
                                `;
                        switch (sSession.CODIGO_PERFIL) {

                            case 1: //Código de Perfil
                                //Si es Editable
                                if (datos[fila][10]) {
                                    botones += `<a id='linkVerEditar' data-id='${datos[fila][0]}' class='btn btn-success btn-circle btn-lg' href='#/' onclick='linkVerEditar(this)' data-toggle="tooltip" data-placement="left" title="Ver/Editar IPD"><i class='fa fa-edit'></i></a><br>`;
                                }

                                if (datos[fila][13] && datos[fila][6] != 72) { // LinkExcepcion -> true & IPD CERRADO
                                        botones += `<a id='linkExcepcion' data-id='${datos[fila][0]}' class='btn btn-success btn-circle btn-lg' href='#/' onclick='linkExcepcion(this)' data-toggle="tooltip" data-placement="left" title="Excepciones"><i class='fa fa-list-ol'></i></a><br>`;
                                }

                                if (datos[fila][12] && datos[fila][8] != null) {
                                    botones += `<a id='linkVerConstancia' data-id='${datos[fila][0]}' class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkVerConstancia(this)' data-toggle="tooltip" data-placement="left" title="Ver Constancia"><i class='fa fa-eye'></i></a><br>`;
                                }

                            break;

                            case 2:

                                if (datos[fila][13]){ //LinkExepcion...
                                    botones += `<a id='linkExcepcion' data-id='${datos[fila][0]}' class='btn btn-success btn-circle btn-lg ' href='#/' onclick='linkExcepcion(this)' data-toggle="tooltip" data-placement="left" title="Excepciones"><i class='fa fa-list-ol'></i></a><br>`;
                                }

                                if (datos[fila][6] == 63 && datos[fila][16]) { //Validable
                                        botones += `<a id='linkValidar' class='btn btn-success btn-circle btn-lg' href='#/' onclick='validarIPDConfirma(${datos[fila][0]});return false;' data-toggle="tooltip" data-placement="left" title="Validar"><i class='fa fa-edit'></i><br>`;
                                }

                                else if (datos[fila][6] == 73) {
                                    if (datos[fila][18] ) { //Bajable
                                        botones += `<a id='linkBajar' class='btn btn-info btn-circle btn-lg' href='#/' onclick='bajarIPD(${datos[fila][0]});return false;' data-toggle="tooltip" data-placement="left" title="Bajar"><i class='fa fa-arrow-circle-down'></i></a><br>`;
                                    }
                                    if (datos[fila][19] ) { //Cerrable
                                        botones += `<a id='linkCerrar' class='btn btn-danger btn-circle btn-lg' href='#/'  onclick='cerrarIPDConfirma(${datos[fila][0]});return false;' data-toggle="tooltip" data-placement="left" title="Cerrar"><i class='fa fa-check-double'></i></a><br>`;
                                    }
                                                      
                                }

                                else if (datos[fila][6] == 64 && datos[fila][20]) { //Desvalidable
                                     botones += `<a id='linkDesvalidar' class='btn btn-danger btn-circle btn-lg' href='#/'  onclick='desvalidarIPD(${datos[fila][0]});return false;' data-toggle="tooltip" data-placement="left" title="Desvalidar"><i class='fa fa-reply'></i></a><br>`;
                                }

                            break;

                            case 9:

                                if ((datos[fila][6] == 64 || datos[fila][6] == 74) && datos[fila][17]) { //Subible
                                    botones += `<a id='linkSubir' class='btn btn-default btn-circle btn-lg' href='#/' onclick='subirIPD(${datos[fila][0]});return false;' data-toggle="tooltip" data-placement="left" title="Subir"><i class='fa fa-arrow-circle-up'></i></a><br>`;
                                }

                            break;

                            case 8:

                                if (datos[fila][6] != 72 && datos[fila][13])  //IPD CERRADO & linkExcepcion -> true
                                    botones += `<a id='linkExcepcion' data-id='${datos[fila][0]}' class='btn btn-success btn-circle btn-lg' href='#/' onclick='linkExcepcion(this)' data-toggle="tooltip" data-placement="left" title="Exepciones"><i class='fa fa-list-ol'></i></a><br>`;

                                if (datos[fila][6] == 63 && datos[fila][16]) {
                                    botones += `<a id='linkValidar' class='btn btn-success btn-circle btn-lg' href='#/' onclick='validarIPDConfirma(${datos[fila][0]});return false;' data-toggle="tooltip" data-placement="left" title="Validar"><i class='fa fa-edit'></i></a><br>`;
                                }

                                else if (datos[fila][6] == 73) {
                                    if (datos[fila][18]) { //Bajable
                                        botones += `<a id='linkBajar' class='btn btn-info btn-circle btn-lg' href='#/' onclick='bajarIPD(${datos[fila][0]});return false;' data-toggle="tooltip" data-placement="left" title="Bajar"><i class='fa fa-arrow-circle-down'></i></a><br>`;
                                    }
                                    if (datos[fila][19]) { //Cerrable
                                        botones += `<a id='linkCerrar' class='btn btn-danger btn-circle btn-lg' href='#/'  onclick='cerrarIPDConfirma(${datos[fila][0]});return false;' data-toggle="tooltip" data-placement="left" title="Cerrar"><i class='fa fa-check-double'></i></a><br>`;
                                    }

                                }

                                else if (datos[fila][6] == 64 || datos[fila][6] == 74) {
                                    if (datos[fila][17]) { //Subible
                                        botones += `<a id='linkSubir' class='btn btn-default btn-circle btn-lg' href='#/' onclick='subirIPD(${datos[fila][0]});return false;' data-toggle="tooltip" data-placement="left" title="Subir"><i class='fa fa-arrow-circle-up'></i></a><br>`;
                                    }
                                    if (datos[fila][20]) { //Desvalidable
                                        botones += `<a id='linkDesvalidar' class='btn btn-danger btn-circle btn-lg' href='#/'  onclick='desvalidarIPD(${datos[fila][0]});return false;' data-toggle="tooltip" data-placement="left" title="Desvalidar"><i class='fa fa-reply'></i></a><br>`;
                                    }
                                    
                                }

                                else if (datos[fila][6] == 72 && datos[fila][21]) { //Abrible
                                    botones += `<a id='linkAbrir' class='btn btn-danger btn-circle btn-lg' href='#/' onclick='abrirIPD(${datos[fila][0]});return false;' data-toggle="tooltip" data-placement="left" title="Abrir"><i class='fa fa-reply'></i></a><br>`;
                                }

                            break;

                        }

                        if (datos[fila][14]) { //15 Imprimible
                            botones += `<a id='linkImprimir' data-id='${datos[fila][0]}' class='btn btn-secondary btn-circle btn-lg' href='#/' onclick='linkImprimir(this);' data-print='true' data-frame='#frameIPD' data-toggle="tooltip" data-placement="left" title="Imprimir"><i class='fa fa-print'></i></a><br>`;
                        }
                        
                        if (datos[fila][15]) { //16 VerMovimientable
                            botones += `<a id='linkMovimientos' data-id='${datos[fila][0]}' class='btn btn-info btn-circle btn-lg ' href='#/' onclick='linkMovimientos(this)' data-toggle="tooltip" data-placement="left" title="Movimientos"><i class='fa fa-list'></i></a><br>`;
                        }
                        
                        if (sSession.CODIGO_PERFIL == "1" && datos[fila][11]) { //Anulable 12
                            botones += `<a id='linkAnular' class='btn btn-danger btn-circle btn-lg' href='#/' onclick='anularIPDConfirma(${datos[fila][0]}); return false;' data-toggle="tooltip" data-placement="left" title="Anular"><i class='fa fa-ban'></i></a><br>`;
                        }

                        botones += `
                                </center >
                                </div >
                            </center >
                        </div >`
                        ;
                    }

                    return botones;
                }
            },
            {
                "targets": [4,6,8],
                "visible": false
            }
        ],

        responsive: {
            details: {
                renderer: function (api, rowIdx, columns) {
                    var data = $.map(columns, function (col, i) {
                        var toReturn = '';
                        if (col.hidden) {
                            toReturn = toReturn.concat('<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">');

                            if (col.title == '') {

                                //Obtener objeto JQuery desde una cadena HTML
                                var button = $(col.data, "button").get();

                                var ID;
                                var newID;

                                if (button) {
                                    ID = $(button).attr("id");
                                    var r = ID.replace('#', '');
                                    newID = r + "-resp";
                                }

                                var first = col.data.replace(new RegExp(r, 'g'), newID);
                                var second = first.replace(new RegExp('<br>', 'g'), '');
                                var result = second.replace(new RegExp('rounded-actions', 'g'), 'rounded-actions-resp');
                                //console.log(result);

                                toReturn = toReturn.concat('<td colspan=2 align=center>' + result + '</td>');
                            }
                            else {
                                toReturn = toReturn.concat('<td align=right style="width: 50%;">' + col.title + ':' + '</td> ');
                                toReturn = toReturn.concat('<td style="width: 50%;">' + col.data + '</td>');
                            }

                            toReturn = toReturn.concat('</tr>');
                        }

                        //Prueba
                        /*
                        $('body').on('.asdf', 'click', function () {
                            alert('aa');
                        });
                        */
                        //console.log("aquí");

                        return toReturn;

                    }).join('');

                    return data ?
                        $('<table style = "width: 100%"/>').append(data) :
                        false;
                }
            }
        },
        "bDestroy": true
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function getTablaIPD(sSession)
{
    var adataset = [];
    var filtros = {};

    //console.log($('#ddlAuge').val());
    if ($('#ddlAuge').val() != '0')
        filtros["RCE_idPatologia_GES"] = $('#ddlAuge').val();

    if ($('#ddlSubGrupo').val() != '0')
        filtros["RCE_idPatologia_GES"] = $('#ddlSubGrupo').val();

    if ($('#ddlEstadoIPD').val() != '0')
        filtros["GEN_idTipo_Estados_Sistemas"] = $('#ddlEstadoIPD').val();

    if ($('#txtFiltroRut').val() != '') 
        filtros["GEN_numero_documentoPaciente"] = $('#txtFiltroRut').val();

    if ($('#txtFiltroNombre').val() != '')
        filtros["GEN_nombrePaciente"] = $('#txtFiltroNombre').val();

    if ($('#txtFiltroApe').val() != '')
        filtros["GEN_ape_paternoPaciente"] = $('#txtFiltroApe').val();

    if ($('#txtFiltroSApe').val() != '')
        filtros["GEN_ape_maternoPaciente"] = $('#txtFiltroSApe').val();

    if (sSession.CODIGO_PERFIL == '10')
        filtros["GEN_idEspecialidad"] = sSession.ID_ESPECIALIDAD;

    //console.log(JSON.stringify(filtros));
    $.ajax({
        type: 'POST',
        url: `${GetWebApiUrl()}RCE_IPD/Bandeja`,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(filtros),
        async: false,

        headers: { 'Authorization': GetToken() },
        success: function (data) {

            $.each(data, function (key, val) {
                var sRes = '';

                if (!$.isEmptyObject(val.RCE_descripcion_Patologia_GES)) {
                    var pat = val.RCE_descripcion_Patologia_GES[0].RCE_descripcion_Patologia_GESFinal;
                    var patp = val.RCE_descripcion_Patologia_GES[0].RCE_descripcion_Patologia_GESPadre[0];
                    if (patp == undefined || patp == null)
                        sRes = pat;
                    else
                        sRes = patp + ' <br/> ' + pat;
                }

                adataset.push([
                    val.RCE_idIPD,
                    moment(new Date(val.RCE_fechaIPD)).format('DD-MM-YYYY'),
                    val.GEN_nombrePaciente + ' ' + val.GEN_ape_paternoPaciente + ' ' + val.GEN_ape_maternoPaciente,
                    sRes,
                    val.RCE_idTipo_IPD,
                    val.RCE_descripcionTipo_IPD,
                    val.GEN_idTipo_Estados_Sistemas,
                    val.GEN_nombreTipo_Estados_Sistemas,
                    val.RCE_idConstancia_GES,
                    '',
                    val.Acciones.Editar,
                    val.Acciones.Anular,
                    val.Acciones.VerConstancia,
                    val.Acciones.VerExcepcion,
                    val.Acciones.Imprimir,
                    val.Acciones.VerMovimientos,
                    val.Acciones.ValidarCaso,
                    val.Acciones.SubirCaso,
                    val.Acciones.BajarCaso,
                    val.Acciones.CerrarCaso,
                    val.Acciones.DesvalidarCaso,
                    val.Acciones.AbrirCaso
                ]);
            });
            LlenaGrillaIPD(adataset, '#tblIPD', sSession);
        }
    });
}