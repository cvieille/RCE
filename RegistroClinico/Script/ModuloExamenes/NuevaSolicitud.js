﻿var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
}
$(document).ready(function () {
    var sSession = getSession();
    RevisarAcceso(true, sSession);
    window.addEventListener('beforeunload', bunload, false);

    $('#txtFecha').val(moment().format('YYYY-MM-DD'));
    $('#txtFechaNacPac').val(moment().format('YYYY-MM-DD'));
    $('#txtHora').val(moment().format('HH:mm'));

    comboServicio();

    var idPro;
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
        async: false,
        success: function (data) {
            idPro = data[0].GEN_idProfesional;
        }
    });

    {
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + "GEN_Profesional/" + idPro,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                $('#idProSol').val(idPro);
                $('#txtRutPro').val(data.GEN_rutProfesional);
                $('#txtRutDigitoPro').val(data.GEN_digitoProfesional);
                $('#txtNombrePro').val(data.GEN_nombreProfesional + ' ' + data.GEN_apellidoProfesional + ' ' + data.GEN_sapellidoProfesional);
            }
        });
    }

    $('#txtRutPac').on('change', function (e) {
        if (EsValidoDigitoVerificador($('#txtRutPac').val(), $('#txtRutDigitoPac').val())) {
            $('#spanRutCorrecto').show();
            $('#spanRutIncorrecto').hide();
            cargarPaciente($('#txtRutPac').val());
            $('#txtRutPac').removeClass('invalid');
        }
        else {
            $('#spanRutCorrecto').hide();
            $('#spanRutIncorrecto').show();
        }
    });
    $('#txtRutDigitoPac').on('input', function (e) {
        if (EsValidoDigitoVerificador($('#txtRutPac').val(), $('#txtRutDigitoPac').val())) {
            $('#spanRutCorrecto').show();
            $('#spanRutIncorrecto').hide();
            cargarPaciente($('#txtRutPac').val());
            $('#txtRutDigitoPac').removeClass('invalid');
        }
        else {
            $('#spanRutCorrecto').hide();
            $('#spanRutIncorrecto').show();
        }
    });

    $('#txtRutPro').on('change', function (e) {
        if (EsValidoDigitoVerificador($('#txtRutPro').val(), $('#txtRutDigitoPro').val())) {
            $('#spanRutCorrectoPro').show();
            $('#spanRutIncorrectoPro').hide();
            cargarProfesional($('#txtRutPro').val(), false);
            $('#txtRutPro').removeClass('invalid');
        }
        else {
            $('#spanRutCorrectoPro').hide();
            $('#spanRutIncorrectoPro').show();
        }
    });
    $('#txtRutDigitoPro').on('input', function (e) {
        if (EsValidoDigitoVerificador($('#txtRutPro').val(), $('#txtRutDigitoPro').val())) {
            $('#spanRutCorrectoPro').show();
            $('#spanRutIncorrectoPro').hide();
            cargarProfesional($('#txtRutPro').val(), false);
            $('#txtRutDigitoPro').removeClass('invalid');
        }
        else {
            $('#spanRutCorrectoPro').hide();
            $('#spanRutIncorrectoPro').show();
        }
    });

    //$('#txtRutProS').on('change', function (e) {
    //    if (EsValidoDigitoVerificador($('#txtRutProS').val(), $('#txtRutDigitoProS').val())) {
    //        $('#spanRutCorrectoProS').show();
    //        $('#spanRutIncorrectoProS').hide();
    //        cargarProfesional($('#txtRutProS').val(), true);
    //        $('#txtRutProS').removeClass('invalid');
    //    }
    //    else {
    //        $('#spanRutCorrectoProS').hide();
    //        $('#spanRutIncorrectoProS').show();
    //    }
    //});
    //$('#txtRutDigitoProS').on('input', function (e) {
    //    if (EsValidoDigitoVerificador($('#txtRutProS').val(), $('#txtRutDigitoProS').val())) {
    //        $('#spanRutCorrectoProS').show();
    //        $('#spanRutIncorrectoProS').hide();
    //        cargarProfesional($('#txtRutProS').val(), true);
    //        $('#txtRutDigitoProS').removeClass('invalid');
    //    }
    //    else {
    //        $('#spanRutCorrectoProS').hide();
    //        $('#spanRutIncorrectoProS').show();
    //    }
    //});
    
    var arrGrupoGes = function () {
        var temp = [];
        $.ajax({
            type: "POST",
            url: ObtenerHost() + "/Vista/ModuloExamenes/NuevaSolicitud.aspx/getExamenes",
            contentType: "application/json",
            dataType: "json",
            async: false,
            success: function (data) {
                $.each(JSON.parse(data.d), function (key, val) {
                    temp.push({
                        "value": val.GEN_glosaArancel,
                        "data": val.GEN_idArancel
                    });
                });
            }
        });
        return temp;
    }();

    var lExamen = [];

    $('#txtExamen').autocomplete({
        lookup: arrGrupoGes,
        autoSelectFirst: true,
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'No se encontraron resultados',
        lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
            var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
            return re.test(suggestion.value);
        },
        onSelect: function (suggestion) {
            $(this).attr('sID', suggestion.data);
        }
    });

    $('#txtExamen').click(function () {
        $(this).val('');
        $('#txtExamen').removeAttr('sID'); 
    });

    $('#btnConfirmar').click(function () {
        var bValido = validarCampos('#divExamen', true);
        if ($('#divExamenes input:checked').length == 0) {
            bValido = false;
            $('#txtExamen').addClass('invalid');
        }
        else
            $('#txtExamen').removeClass('invalid');

        if ($('#idProSol').val() == '0') {
            bValido = false;
            $('#txtRutPro').addClass('invalid');
        } else
            $('#txtRutPro').removeClass('invalid');

        if ($('#idPac').val() == '0') {
            bValido = false;
            $('#txtRutPac').addClass('invalid');
        } else
            $('#txtRutPac').removeClass('invalid');

        if ($('#idProToma').val() == '0') {
            bValido = false;
            $('#txtRutProS').addClass('invalid');
        } else
            $('#txtRutProS').removeClass('invalid');

        if (bValido && $('#divExamenes input:checked').length > 0) {
            var fecha = moment(moment($('#txtFecha').val()).toDate()).format('DD-MM-YYYY') + ' ' + $('#txtHora').val();
            var examen = [];
            $('#divExamenes input:checked').each(function () {
                examen.push($(this).prop('id'));
            });

            var json = {
                fecha: fecha,
                idPaciente: $('#idPac').val(),
                idServicio: $('#ddlServicio').prop('value'),
                idSolicitante: $('#idProSol').val(),
                diagnostico: $('#txtDiagnostico').val(),
                idMuestra: $('#idProToma').val(),
                examenes: examen
            };
            $.ajax({
                type: "POST",
                url: ObtenerHost() + "/Vista/ModuloExamenes/NuevaSolicitud.aspx/insertExamen",
                data: JSON.stringify({ json: JSON.stringify(json) }),
                contentType: "application/json",
                dataType: "json",
                async: false,
                success: function (data) {
                    toastr.success('IPD INGRESADO CORRECTAMENTE');
                    $('.custom-checkbox').addClass('disabled');
                    $('#txtExamen').addClass('disabled');
                    $('#lnbAgregarEx').addClass('disabled');
                    $('#btnConfirmar').addClass('disabled');
                }
            });
        }
    });

    $('#lnbAgregarEx').click(function () {
        if ($('#txtExamen').attr('sID') == undefined) {
            $('#txtExamen').addClass('invalid');
        }
        else {
            var bAgregar = true;
            for (var i = 0; i < lExamen.length; i++) {
                if (lExamen[i].data == $('#txtExamen').attr('sID')) {
                    bAgregar = false;
                    break;
                }
            }
            if (bAgregar) {
                lExamen.push({
                    "data": $('#txtExamen').attr('sID'),
                    "value": $('#txtExamen').val()
                });
                var div =
                    '<div class="row"><div class="col-md-6">\
                <div class="custom-control custom-checkbox">\
                    <input id="' + $('#txtExamen').attr('sID') + '" type="checkbox" checked class="custom-control-input">\
                    <label class="custom-control-label" for="' + $('#txtExamen').attr('sID') + '">' + $('#txtExamen').val() + '</label>\
                </div>\
                </div></div>';
                $('#divExamenes').append(div);
                $('#txtExamen').removeClass('invalid')
            }
        }
    });
    $('.clockpicker').clockpicker();


    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 3000,
        "extendedTimeOut": 1500,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }
    toastr.options.onHidden = function () {
        quitarListener();
        window.location.replace(ObtenerHost() + "/Vista/ModuloExamenes/BandejaExamenes.aspx");
    }

});

function cargarPaciente(sRut)
{
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Paciente/BuscarporDocumento?GEN_idIdentificacion=1&GEN_numero_documentoPaciente=" + sRut,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            $('#txtUbInterna').val(data[0].GEN_Paciente.GEN_nuiPaciente);
            $('#txtNombrePac').val(data[0].GEN_Paciente.GEN_nombrePaciente);
            $('#txtApellidoPac').val(data[0].GEN_Paciente.GEN_ape_paternoPaciente);
            $('#txtSapellidoPac').val(data[0].GEN_Paciente.GEN_ape_maternoPaciente);
            $('#txtFechaNacPac').val(data[0].GEN_Paciente.GEN_fec_nacimientoPaciente);

            $('#idPac').val(data[0].GEN_Paciente.GEN_idPaciente);
        }
    });

    //$.getJSON(GetWebApiUrl() + "GEN_Paciente/BuscarporDocumento?GEN_idIdentificacion=1&GEN_numero_documentoPaciente=" + sRut, function (data) {
    //    $('#txtUbInterna').val(data[0].GEN_nuiPaciente);
    //    $('#txtNombrePac').val(data[0].GEN_nombrePaciente);
    //    $('#txtApellidoPac').val(data[0].GEN_ape_paternoPaciente);
    //    $('#txtSapellidoPac').val(data[0].GEN_ape_maternoPaciente);
    //    $('#txtFechaNacPac').val(data[0].GEN_fec_nacimientoPaciente);

    //    $('#idPac').val(data[0].GEN_idPaciente);
    //});
}

function cargarProfesional(sRut, sol)
{
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Profesional/BuscarporDocumento?GEN_rutProfesional=" + sRut,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            if (sol) {
                $('#idProSol').val(data[0].GEN_idProfesional);
                $('#txtNombreProS').val(data[0].GEN_nombreProfesional + ' ' + data[0].GEN_apellidoProfesional + ' ' + data[0].GEN_sapellidoProfesional);
            }
            else {
                $('#idProToma').val(data[0].GEN_idProfesional);
                $('#txtNombrePro').val(data[0].GEN_nombreProfesional + ' ' + data[0].GEN_apellidoProfesional + ' ' + data[0].GEN_sapellidoProfesional);
            }
        }
    });

    //$.getJSON(GetWebApiUrl() + "GEN_Profesional/BuscarporDocumento?GEN_rutProfesional=" + sRut, function (data) {
    //    if (sol) {
    //        $('#idProSol').val(data[0].GEN_idProfesional);
    //        $('#txtNombreProS').val(data[0].GEN_nombreProfesional + ' ' + data[0].GEN_apellidoProfesional + ' ' + data[0].GEN_sapellidoProfesional);
    //    }
    //    else {
    //        $('#idProToma').val(data[0].GEN_idProfesional);
    //        $('#txtNombrePro').val(data[0].GEN_nombreProfesional + ' ' + data[0].GEN_apellidoProfesional + ' ' + data[0].GEN_sapellidoProfesional);
    //    }
    //});
}

function comboServicio()
{
    $('#ddlServicio').empty();
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Servicio",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var obj = $.grep(data, function (d) {
                return d.GEN_estadoServicio.toLowerCase() == "activo";
            });
            $.each(obj, function (i, r) {
                $('#ddlServicio').append("<option value='" + r.GEN_idServicio + "'>" + r.GEN_nombreServicio + "</option>");
            });
            $('#ddlServicio').selectpicker('refresh');
        }
    });

    //$.getJSON(GetWebApiUrl() + "GEN_Servicio", function (data) {
    //    var obj = $.grep(data, function (d) {
    //        return d.GEN_estadoServicio.toLowerCase() == "activo";
    //    });
    //    $.each(obj, function (i, r) {
    //        $('#ddlServicio').append("<option value='" + r.GEN_idServicio + "'>" + r.GEN_nombreServicio + "</option>");
    //    });
    //    $('#ddlServicio').selectpicker('refresh');
    //});
}