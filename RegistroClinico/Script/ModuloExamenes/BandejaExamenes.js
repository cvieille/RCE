﻿
var idPro;
$(document).ready(function () {

    var sSession = getSession();
    RevisarAcceso(false, sSession);
    getTablaIPD(sSession);

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
        async: false,
        success: function (data) {
            idPro = data[0].GEN_idProfesional;
        }
    });
});

function LlenaGrillaIPD(datos, grilla, sSession) {

    //if (datos.length == 0)
    //    $('#lnbExportar').addClass('disabled');
    //else
    //    $('#lnbExportar').removeClass('disabled');

    $(grilla).addClass("nowrap").DataTable({
        data: datos,
        columns: [
            { title: "ID" },
            { title: "Fecha" },
            { title: "iPaciente" },
            { title: "Paciente" },
            { title: "iServicio" },
            { title: "Servicio" },
            { title: "uSolicitante" },
            { title: "Diagnóstico" },
            { title: "uMuestra" },
            { title: "" },
        ],
        "columnDefs": [
            { "targets": 1, "sType": "date-ukLong" },
            {
                "targets": -1,
                "data": null,
                orderable: false,
                "render": function (data, type, row, meta) {
                    var fila = meta.row;
                    var botones;
                    //if (datos[fila][6] == 79) { //ANULADO MUESTRA BOTON BLOQUEADO Y NO CARGA BOTONES
                    //    botones = "<button class='btn btn-primary disabled btn-block'>ACCIONES</button>";
                    //} else
                    {
                        botones = "<button class='btn btn-primary btn-block' data-toggle='collapse' data-target='#div_accionesIPD" + fila + "' onclick='return false;'>ACCIONES</button>";
                        botones += "<div id='div_accionesIPD" + fila + "' class='collapse'>";
                        botones += "<div class='btn-group-vertical w-100'>";
                        
                        botones += "<a id='linkImprimir' data-id='" + datos[fila][0] + "' class='btn btn-default btn-block load-click disabled' href='#/' onclick='linkImprimir(this);' data-print='true' data-frame='#frameIPD'><i class='fa fa-print disabled'></i> Imprimir</a>";
                        botones += "<a id='linkEditar' data-id='" + datos[fila][0] + "' class='btn btn-default btn-block load-click disabled' href='#/' onclick='linkEditar(this);' ><i class='fa fa-edit disabled'></i> Ver/Editar</a>";

                        botones += "</div>";
                        botones += "</div>";
                    }
                    return botones;
                }
            },
            {
                "targets": [2, 4, 6, 8],
                "visible": false
            }
        ],
        "bDestroy": true
    });
}

function getTablaIPD(sSession) {

    var adataset = [];
    
    $.ajax({
        type: "POST",
        url: ObtenerHost() + "/Vista/ModuloExamenes/BandejaExamenes.aspx/getTabla",
        data: JSON.stringify({ sIdPro: idPro }),
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            var json = JSON.parse(data.d);
            $.each(json, function (key, val) {
                //console.log(val.RCE_fechaSolicitud_Examen);
                adataset.push([
                    val.RCE_idSolicitud_Examen,
                    moment(moment(val.RCE_fechaSolicitud_Examen).toDate()).format('DD-MM-YYYY HH:mm:SS'),
                    val.GEN_idPaciente,
                    val.nombrepaciente,
                    val.GEN_idServicio,
                    val.GEN_nombreServicio,
                    val.GEN_idUsuarioSolicitante,
                    val.RCE_diagnosticoSolicitud_Examen,
                    val.GEN_idUsuarioMuestra,
                    //moment(new Date(val.RCE_fechaIPD)).format('DD-MM-YYYY'),
                ]);
            });
            LlenaGrillaIPD(adataset, '#tblExamen', sSession);
        }
    });
}