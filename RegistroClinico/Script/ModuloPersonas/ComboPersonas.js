﻿$(document).ready(function () {
    $('#selTipoIdentificacion').change(function (e) {
        $('#txtHosFiltroRut').val('');
        $('#txtHosFiltroDV').val('');
        if ($(this).val() == 1 || $(this).val() == 4) {
            $('.digito').show();
            $('#txtHosFiltroRut').attr('maxlength', 8);
        } else {
            $('.digito').hide();
            $('#txtHosFiltroRut').attr('maxlength', 10);
        }
        $("#lblTipoIdentificacion").text($("#selTipoIdentificacion").children("option:selected").text());
    });

});
function ComboNacionalidad() {
    let url = `${GetWebApiUrl()}GEN_Nacionalidad/Combo`;
    setCargarDataEnCombo(url, false, "#sltNacionalidad");
}

function ComboTipoEscolaridad() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Escolaridad/Combo`;
    setCargarDataEnCombo(url, false, "#sltEscolaridad");
}
function ComboOcupacion() {
    let url = `${GetWebApiUrl()}GEN_Ocupaciones/Combo`;
    setCargarDataEnCombo(url, false, "#sltOcupacion");
}
function ComboCategoriaOcupacion() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Categoria_Ocupacion/Combo`;
    setCargarDataEnCombo(url, false, "#sltCategoriaOcupacional");
}
function comboIdentificacion(selector) {
    
    $(selector).empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Identificacion/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (i, r) {
                $(selector).append('<option value="' + r.GEN_idIdentificacion + '">' + r.GEN_nombreIdentificacion + '</option>');
            });
            $("#lblTipoIdentificacion").text($(selector).children("option:selected").text());
        }
    })
}