﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Prevision
    {
        public int GEN_idPrevision { get; set; }
        public string GEN_nombrePrevision { get; set; }
        public string GEN_estadoPrevision { get; set; }
    }
}