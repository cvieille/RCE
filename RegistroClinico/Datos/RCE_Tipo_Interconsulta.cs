﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Datos
{
    public class RCE_Tipo_Interconsulta
    {
        public int RCE_idTipo_Interconsulta { get; set; }
        public string RCE_DescripcionTipo_Interconsulta { get; set; }
        public string RCE_estadoTipo_Interconsulta { get; set; }
    }
}