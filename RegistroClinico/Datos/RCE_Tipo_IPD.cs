﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class RCE_Tipo_IPD
    {
        public int RCE_idTipo_IPD { get; set; }
        public string RCE_descripcionTipo_IPD { get; set; }
        public string RCE_estadoTipo_IPD { get; set; }
    }
}