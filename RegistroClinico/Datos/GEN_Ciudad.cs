﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Ciudad
    {
        public int GEN_idCiudad { get; set; }
        public string GEN_nombreCiudad { get; set; }
        public int GEN_codigoCiudad { get; set; }
        public int GEN_idComuna { get; set; }
        public string GEN_estadoCiudad { get; set; }
    }
}