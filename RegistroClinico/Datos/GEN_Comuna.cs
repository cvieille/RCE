﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Comuna
    {
        public int GEN_idComuna { get; set; }
        public string GEN_nombreComuna { get; set; }
        public int GEN_codigoComuna { get; set; }
        public int GEN_idRegion { get; set; }
        public string GEN_estadoComuna { get; set; }
    }
}