﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class RCE_Subgrupo_Patologia_GES
    {
        public int RCE_idSubgrupo_Patologia_GES { get; set; }
        public string RCE_descripcion_Subgrupo_Patologia_GES { get; set; }
        public string RCE_estado_Subgrupo_Patologia_GES { get; set; }
        public int RCE_idGrupo_Patologia_GES { get; set; }
    }
}