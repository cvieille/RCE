﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class RCE_Movimientos_Eventos
    {
        public int RCE_idMovimientos_Eventos { get; set; }
        public int RCE_idEventos { get; set; }
        public int GEN_idUsuarios { get; set; }
        public DateTime RCE_fechaMovimientos_Eventos { get; set; }
        public int GEN_idTipo_Movimientos_Sistemas { get; set; }
    }
}