﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Perfil
    {
        public int GEN_idPerfil { get; set; }
        public int GEN_codigoPerfil { get; set; }
        public string GEN_descripcionPerfil { get; set; }
        public int GEN_idSistemas { get; set; }
    }
}