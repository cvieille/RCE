﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Prevision_Tramo
    {
        public int GEN_idPrevision_Tramo { get; set; }
        public string GEN_descripcionPrevision_Tramo { get; set; }
        public int GEN_idPrevision { get; set; }
        public string GEN_estadoPrevision_Tramo { get; set; }
    }
}