﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Paciente
    {
        public int GEN_idPaciente { get; set; }
        public string GEN_numero_documentoPaciente { get; set; }
        public char? GEN_digitoPaciente { get; set; }
        public string GEN_nombrePaciente { get; set; }
        public string GEN_ape_paternoPaciente { get; set; }
        public string GEN_ape_maternoPaciente { get; set; }
        public string GEN_dir_callePaciente { get; set; }
        public string GEN_dir_numeroPaciente { get; set; }
        public string GEN_dir_ruralidadPaciente { get; set; }
        public int GEN_idPais { get; set; }
        public int GEN_idRegion { get; set; }
        public int GEN_idComuna { get; set; }
        public int GEN_idCiudad { get; set; }
        public string GEN_telefonoPaciente { get; set; }
        public int GEN_idSexo { get; set; }
        public DateTime GEN_fec_nacimientoPaciente { get; set; }
        public string GEN_otros_fonosPaciente { get; set; }
        public string GEN_emailPaciente { get; set; }
        public int GEN_idPrevision { get; set; }
        public int GEN_idPrevision_Tramo { get; set; }
        public int GEN_nuiPaciente { get; set; }
        public int GEN_pac_pac_numeroPaciente { get; set; }
        public int GEN_idIdentificacion { get; set; }
        public DateTime GEN_fec_actualizacionPaciente { get; set; }
        public string GEN_PraisPaciente { get; set; }
        public string GEN_estadoPaciente { get; set; }
        public DateTime GEN_fecha_fallecimientoPaciente { get; set; }
    }
}