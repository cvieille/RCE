﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class RCE_Tipo_Evento
    {
        public int RCE_idTipo_Evento { get; set; }
        public string RCE_descripcionTipo_Evento { get; set; }
        public string RCE_estadoTipo_Evento { get; set; }
    }
}