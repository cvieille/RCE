﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Sist_Usuario
    {
        public int GEN_idSist_Usuario { get; set; }
        public int GEN_idUsuarios { get; set; }
        public int GEN_idSistemas { get; set; }
        public int GEN_idPerfil { get; set; }
        public string GEN_estadoSist_Usuario { get; set; }
        public DateTime GEN_ultimoAccesoSist_Usuario { get; set; }
    }
}