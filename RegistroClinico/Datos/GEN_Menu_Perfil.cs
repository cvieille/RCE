﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Menu_Perfil
    {
        public int GEN_idMenu_Perfil { get; set; }
        public int GEN_idMenu { get; set; }
        public int GEN_idPerfil { get; set; }
        public string GEN_estadoMenu_Perfil { get; set; }
    }
}