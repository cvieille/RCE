﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Datos
{
    public class Log
    {
        public struct Sistemas
        {
            public string nombreSistema { get; set; }
            public List<Modulos> modulos { get; set; }
        }

        public struct Modulos
        {
            public string nombreModulo { get; set; }
            public List<Formularios> formularios { get; set; }
        }

        public struct Formularios
        {
            public string nombreFormulario { get; set; }
            public List<Logs> logs { get; set; }
        }

        public struct Logs
        {
            public string fechaHora { get; set; }
            public string detalle { get; set; }
            public string stackTrace { get; set; }
            public string codigo { get; set; }
        }
    }
}