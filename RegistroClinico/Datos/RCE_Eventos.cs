﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class RCE_Eventos
    {
        public int RCE_idEventos { get; set; }
        public int RCE_idTipo_Evento { get; set; }
        public string RCE_anamnesisEventos { get; set; }
        public string RCE_diagnosticoEventos { get; set; }
        public DateTime RCE_alta_pacienteEventos { get; set; }
        public int GEN_idPaciente { get; set; }
        public int GEN_idProfesional { get; set; }
        public DateTime RCE_fecha_inicioEventos { get; set; }
        public DateTime RCE_fecha_terminoEventos { get; set; }
        public string RCE_causaEventos { get; set; }
    }
}