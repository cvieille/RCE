﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class RCE_IPD
    {
        public int RCE_idIPD { get; set; }
        public DateTime RCE_fechaIPD { get; set; }
        public int GEN_idEstablecimiento { get; set; }
        public int GEN_idEspecialidad { get; set; }
        public string RCE_diagnosticoIPD { get; set; }
        public string RCE_fundamentoIPD { get; set; }
        public string RCE_tratamientoIPD { get; set; }
        public DateTime RCE_inicio_tratamientoIPD { get; set; }
        public int RCE_idEventos { get; set; }
        public int GEN_idAmbito { get; set; }
        public int RCE_idGrupo_Patologia_GES { get; set; }
        public int RCE_idSubgrupo_Patologia_GES { get; set; }
        public int GEN_idTipo_Estados_Sistemas { get; set; }
        public string RCE_estadoIPD { get; set; }
        public int RCE_idTipo_IPD { get; set; }
        public int GEN_idProfesional { get; set; }
    }
}