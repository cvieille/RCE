﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Establecimiento
    {
        public int GEN_idEstablecimiento { get; set; }
        public int GEN_codigo_deisEstablecimiento { get; set; }
        public string GEN_rutEstablecimiento { get; set; }
        public char GEN_digitoEstablecimiento { get; set; }
        public string GEN_nombreEstablecimiento { get; set; }
        public string GEN_direccionEstablecimiento { get; set; }
        public int GEN_idCiudad { get; set; }
        public string GEN_estadoEstablecimiento { get; set; }
        public int GEN_idServicio_Salud { get; set; }
        public char GEN_prefijo_anexoEstablecimiento { get; set; }
    }
}