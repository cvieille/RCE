﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class RCE_Movimientos_Constancia
    {
        public int RCE_idMovimientos_Constancia { get; set; }
        public int RCE_idConstancia_GES { get; set; }
        public int GEN_idUsuarios { get; set; }
        public DateTime RCE_fechaMovimientos_Constancia { get; set; }
        public int GEN_idTipo_Movimientos_Sistemas { get; set; }
    }
}