﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class RCE_Grupo_Patologia_GES
    {
        public int RCE_idGrupo_Patologia_GES { get; set; }
        public string RCE_descripcionGrupo_Patologia_GES { get; set; }
        public string RCE_estadoGrupo_Patologia_GES { get; set; }
    }
}