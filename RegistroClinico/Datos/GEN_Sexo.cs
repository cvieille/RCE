﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Sexo
    {
        public int GEN_idSexo { get; set; }
        public int GEN_codSexo { get; set; }
        public string GEN_nomSexo { get; set; }
        public char GEN_codSexoOrden { get; set; }
        public string GEN_estadoSexo { get; set; }
    }
}