﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Tipo_Estados_Sistemas
    {
        public int GEN_idTipo_Estados_Sistemas { get; set; }
        public int GEN_idSistemas { get; set; }
        public string GEN_nombreTipo_Estados_Sistemas { get; set; }
    }
}