﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Datos
{
    public class RCE_Interconsulta
    {
        public int RCE_idInterconsulta { get; set; }
        public int GEN_idUbicacion_enviadoInterconsulta { get; set; }
        public int GEN_idUbicacion_derivadoInterconsulta { get; set; }
        public string RCE_diagnosticoInterconsulta { get; set; }
        public string RCE_sintomatologiaInterconsulta { get; set; }
        public string RCE_solicitudInterconsulta { get; set; }
        public DateTime RCE_fecha_hora_creacionInterconsulta { get; set; }
        public int GEN_idProfesional { get; set; }
        public int RCE_idEventos { get; set; }
        public string RCE_estadoInterconsulta { get; set; }
        public int GEN_idEspecialidad_enviadaInterconsulta { get; set; }
        public int GEN_idEspecialidad_derivadaInterconsulta { get; set; }
        public int GEN_idTipo_Estados_Sistemas { get; set; }
        public int? RCE_idInterconsulta_GES { get; set; }
    }
}