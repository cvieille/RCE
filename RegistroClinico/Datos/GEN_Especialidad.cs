﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Especialidad
    {
        public int GEN_idEspecialidad { get; set; }
        public string GEN_nombreEspecialidad { get; set; }
        public string GEN_operaEspecialidad { get; set; }
        public string GEN_estadoEspecialidad { get; set; }
    }
}