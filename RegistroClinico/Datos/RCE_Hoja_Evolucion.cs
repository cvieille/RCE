﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Datos
{
    public class RCE_Hoja_Evolucion
    {
        
        public int RCE_idHoja_Evolucion { get; set; }
        public int GEN_idUbicacion { get; set; }
        public DateTime RCE_fechaHoja_Evolucion { get; set; }
        public int RCE_idEventos { get; set; }
        public int GEN_idProfesional { get; set; }
        public string RCE_descripcionHoja_Evolucion { get; set; }
        public string RCE_estadoHoja_Evolucion { get; set; }
        public int GEN_idPaciente { get; set; }

    }
}