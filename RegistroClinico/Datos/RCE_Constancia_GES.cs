﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class RCE_Constancia_GES
    {
        public int RCE_idConstancia_GES { get; set; }
        public int RCE_idIPD { get; set; }
        public int GEN_idProfesional { get; set; }
        public DateTime RCE_fechaConstancia_GES { get; set; }
        public int GEN_id_representantePaciente { get; set; }
        public int GEN_idTipo_Representante { get; set; }
        public int GEN_idTipo_Estados_Sistemas { get; set; }
        public string RCE_estadoConstancia_GES { get; set; }
    }
}