﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Servicio_Salud
    {
        public int GEN_idServicio_Salud { get; set; }
        public int GEN_codigo_deisServicio_Salud { get; set; }
        public string GEN_nombreServicio_Salud { get; set; }
        public string GEN_estadoServicio_Salud { get; set; }
    }
}