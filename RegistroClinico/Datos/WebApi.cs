﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class WebApi
    {
        //me parece que ya nada de eso se usa
        public class Movimientos_IPD
        {
            [JsonProperty("RCE_idMovimientos_IPD")]
            public int RCE_idMovimientos_IPD { get; set; }
            [JsonProperty("RCE_idIPD")]
            public int RCE_idIPD { get; set; }
            [JsonProperty("RCE_fechaMovimientos_IPD")]
            public DateTime RCE_fechaMovimientos_IPD { get; set; }
            [JsonProperty("GEN_descripcionTipo_Movimientos_Sistemas")]
            public string GEN_descripcionTipo_Movimientos_Sistemas { get; set; }
            [JsonProperty("GEN_loginUsuarios")]
            public string GEN_loginUsuarios { get; set; }
        }

        public class Movimientos_IC
        {
            [JsonProperty("RCE_idMovimientos_Interconsulta")]
            public int RCE_idMovimientos_Interconsulta { get; set; }
            [JsonProperty("RCE_idInterconsulta")]
            public int RCE_idInterconsulta { get; set; }
            [JsonProperty("RCE_fechaMovimientos_Interconsulta")]
            public DateTime RCE_fechaMovimientos_Interconsulta { get; set; }
            [JsonProperty("GEN_descripcionTipo_Movimientos_Sistemas")]
            public string GEN_descripcionTipo_Movimientos_Sistemas { get; set; }
            [JsonProperty("GEN_loginUsuarios")]
            public string GEN_loginUsuarios { get; set; }
        }
        

        public class Paciente
        {
            //    [JsonProperty("RCE_Subgrupo_Patologia_GES")]
            //    public List<Subgrupo_Patologia_GES> RCE_Subgrupo_Patologia_GES { get; set; }
            [JsonProperty("GEN_Estado_Conyugal")]
            public GEN_Estado_Conyugal GEN_Estado_Conyugal { get; set; }
            //[JsonProperty("GEN_Identificacion")]
            //public List<Identificacion> GEN_Identificacion { get; set; }
            //[JsonProperty("GEN_Nacionalidad")]
            //public string GEN_Nacionalidad { get; set; }
            //[JsonProperty("GEN_idPais")]
            //public List<Pais> GEN_idPais { get; set; }
            //[JsonProperty("GEN_Pueblo_Originario")]
            //public string GEN_Pueblo_Originario { get; set; }
            //[JsonProperty("GEN_Sexo")]
            //public List<Sexo> GEN_Sexo { get; set; }
            //[JsonProperty("GEN_Tipo_Escolaridad")]
            //public string GEN_Tipo_Escolaridad { get; set; }
            [JsonProperty("GEN_idPaciente")]
            public int GEN_idPaciente { get; set; }
            [JsonProperty("GEN_numero_documentoPaciente")]
            public string GEN_numero_documentoPaciente { get; set; }
            [JsonProperty("GEN_digitoPaciente")]
            public string GEN_digitoPaciente { get; set; }
            [JsonProperty("GEN_nombrePaciente")]
            public string GEN_nombrePaciente { get; set; }
            [JsonProperty("GEN_ape_paternoPaciente")]
            public string GEN_ape_paternoPaciente { get; set; }
            [JsonProperty("GEN_ape_maternoPaciente")]
            public string GEN_ape_maternoPaciente { get; set; }
            [JsonProperty("GEN_dir_callePaciente")]
            public string GEN_dir_callePaciente { get; set; }
            [JsonProperty("GEN_dir_numeroPaciente")]
            public string GEN_dir_numeroPaciente { get; set; }
            [JsonProperty("GEN_dir_ruralidadPaciente")]
            public string GEN_dir_ruralidadPaciente { get; set; }
            [JsonProperty("GEN_idPais")]
            public int GEN_idPais { get; set; }
            [JsonProperty("GEN_idRegion")]
            public int GEN_idRegion { get; set; }
            [JsonProperty("GEN_idComuna")]
            public int GEN_idComuna { get; set; }
            [JsonProperty("GEN_idCiudad")]
            public int GEN_idCiudad { get; set; }
            [JsonProperty("GEN_telefonoPaciente")]
            public string GEN_telefonoPaciente { get; set; }
            [JsonProperty("GEN_idSexo")]
            public int GEN_idSexo { get; set; }
            [JsonProperty("GEN_idTipo_Genero")]
            public int GEN_idTipo_Genero { get; set; }
            [JsonProperty("GEN_fec_nacimientoPaciente")]
            public string GEN_fec_nacimientoPaciente { get; set; }
            [JsonProperty("GEN_otros_fonosPaciente")]
            public string GEN_otros_fonosPaciente { get; set; }
            [JsonProperty("GEN_emailPaciente")]
            public string GEN_emailPaciente { get; set; }
            [JsonProperty("GEN_idPrevision")]
            public int GEN_idPrevision { get; set; }
            [JsonProperty("GEN_idPrevision_Tramo")]
            public int GEN_idPrevision_Tramo { get; set; }
            [JsonProperty("GEN_nuiPaciente")]
            public int GEN_nuiPaciente { get; set; }
            [JsonProperty("GEN_pac_pac_numeroPaciente")]
            public int GEN_pac_pac_numeroPaciente { get; set; }
            [JsonProperty("GEN_idIdentificacion")]
            public int GEN_idIdentificacion { get; set; }
            [JsonProperty("GEN_fec_actualizacionPaciente")]
            public string GEN_fec_actualizacionPaciente { get; set; }
            [JsonProperty("GEN_PraisPaciente")]
            public string GEN_PraisPaciente { get; set; }
            [JsonProperty("GEN_idEstado_Conyugal")]
            public int GEN_idEstado_Conyugal { get; set; }
            [JsonProperty("GEN_idPueblo_Originario")]
            public int GEN_idPueblo_Originario { get; set; }
            [JsonProperty("GEN_idNacionalidad")]
            public int GEN_idNacionalidad { get; set; }
            [JsonProperty("GEN_idTipo_Escolaridad")]
            public int GEN_idTipo_Escolaridad { get; set; }
            [JsonProperty("GEN_estadoPaciente")]
            public string GEN_estadoPaciente { get; set; }
            [JsonProperty("GEN_fecha_fallecimientoPaciente")]
            public string GEN_fecha_fallecimientoPaciente { get; set; }
        }

        public class GEN_Estado_Conyugal
        {
            [JsonProperty("GEN_idEstado_Conyugal")]
            public int GEN_idEstado_Conyugal { get; set; }
            [JsonProperty("GEN_codigoEstado_Conyugal")]
            public int GEN_codigoEstado_Conyugal { get; set; }
            [JsonProperty("GEN_descripcionEstado_Conyugal")]
            public string GEN_descripcionEstado_Conyugal { get; set; }
            [JsonProperty("GEN_estadoEstado_Conyugal")]
            public string GEN_estadoEstado_Conyugal { get; set; }
        }

        public class Identificacion
        {
            [JsonProperty("GEN_idIdentificacion")]
            public int GEN_idIdentificacion { get; set; }
            [JsonProperty("GEN_nombreIdentificacion")]
            public string GEN_nombreIdentificacion { get; set; }
            [JsonProperty("GEN_estadoIdentificacion")]
            public string GEN_estadoIdentificacion { get; set; }
        }

        public class Pais
        {
            [JsonProperty("GEN_idPais")]
            public int GEN_idPais { get; set; }
            [JsonProperty("GEN_nombrePais")]
            public string GEN_nombrePais { get; set; }
            [JsonProperty("GEN_codigoPais")]
            public int GEN_codigoPais { get; set; }
            [JsonProperty("GEN_nacionalidadPais")]
            public string GEN_nacionalidadPais { get; set; }
            [JsonProperty("GEN_estadoPais")]
            public string GEN_estadoPais { get; set; }
        }

        public class Sexo
        {
            [JsonProperty("GEN_idSexo")]
            public int GEN_idSexo { get; set; }
            [JsonProperty("GEN_codSexo")]
            public int GEN_codSexo { get; set; }
            [JsonProperty("GEN_nomSexo")]
            public string GEN_nomSexo { get; set; }
            [JsonProperty("GEN_codSexoOrden")]
            public string GEN_codSexoOrden { get; set; }
            [JsonProperty("GEN_estadoSexo")]
            public string GEN_estadoSexo { get; set; }
        }

        public class Tipo_Evento
        {
            [JsonProperty("RCE_idTipo_Evento")]
            public int RCE_idTipo_Evento { get; set; }
            [JsonProperty("RCE_descripcionTipo_Evento")]
            public string RCE_descripcionTipo_Evento { get; set; }
            [JsonProperty("RCE_estadoTipo_Evento")]
            public string RCE_estadoTipo_Evento { get; set; }
        }

        public class Tipo_IPD
        {
            [JsonProperty("RCE_idTipo_IPD")]
            public int RCE_idTipo_IPD { get; set; }
            [JsonProperty("RCE_descripcionTipo_IPD")]
            public string RCE_descripcionTipo_IPD { get; set; }
            [JsonProperty("RCE_estadoTipo_IPD")]
            public string RCE_estadoTipo_IPD { get; set; }
        }

        public class Tipo_Representante
        {
            [JsonProperty("GEN_idTipo_Representante")]
            public int GEN_idTipo_Representante { get; set; }
            [JsonProperty("GEN_nombreTipo_Representante")]
            public string GEN_nombreTipo_Representante { get; set; }
        }

        public class Establecimiento
        {
            [JsonProperty("GEN_idEstablecimiento")]
            public int GEN_idEstablecimiento { get; set; }
            [JsonProperty("GEN_codigo_deisEstablecimiento")]
            public int GEN_codigo_deisEstablecimiento { get; set; }
            [JsonProperty("GEN_rutEstablecimiento")]
            public string GEN_rutEstablecimiento { get; set; }
            [JsonProperty("GEN_digitoEstablecimiento")]
            public string GEN_digitoEstablecimiento { get; set; }
            [JsonProperty("GEN_nombreEstablecimiento")]
            public string GEN_nombreEstablecimiento { get; set; }
            [JsonProperty("GEN_direccionEstablecimiento")]
            public string GEN_direccionEstablecimiento { get; set; }
            [JsonProperty("GEN_idCiudad")]
            public int GEN_idCiudad { get; set; }
            [JsonProperty("GEN_estadoEstablecimiento")]
            public string GEN_estadoEstablecimiento { get; set; }
            [JsonProperty("GEN_idServicio_Salud")]
            public int GEN_idServicio_Salud { get; set; }
            [JsonProperty("GEN_prefijo_anexoEstablecimiento")]
            public string GEN_prefijo_anexoEstablecimiento { get; set; }
        }

        public class Prevision
        {
            [JsonProperty("GEN_idPrevision")]
            public int GEN_idPrevision { get; set; }
            [JsonProperty("GEN_nombrePrevision")]
            public string GEN_nombrePrevision { get; set; }
            [JsonProperty("GEN_estadoPrevision")]
            public string GEN_estadoPrevision { get; set; }
        }

        public class Prevision_Tramo
        {
            [JsonProperty("GEN_idPrevision_Tramo")]
            public int GEN_idPrevision_Tramo { get; set; }
            [JsonProperty("GEN_descripcionPrevision_Tramo")]
            public string GEN_descripcionPrevision_Tramo { get; set; }
            [JsonProperty("GEN_idPrevision")]
            public int GEN_idPrevision { get; set; }
            [JsonProperty("GEN_estadoPrevision_Tramo")]
            public string GEN_estadoPrevision_Tramo { get; set; }
        }

        public class Eventos
        {
            [JsonProperty("RCE_idEventos")]
            public int RCE_idEventos { get; set; }
            [JsonProperty("RCE_anamnesisEventos")]
            public string RCE_anamnesisEventos { get; set; }
            [JsonProperty("RCE_diagnosticoEventos")]
            public string RCE_diagnosticoEventos { get; set; }
            [JsonProperty("RCE_alta_pacienteEventos")]
            public string RCE_alta_pacienteEventos { get; set; }
            [JsonProperty("GEN_idPaciente")]
            public int GEN_idPaciente { get; set; }
            [JsonProperty("GEN_idProfesional")]
            public int GEN_idProfesional { get; set; }
            [JsonProperty("RCE_fecha_inicioEventos")]
            public string RCE_fecha_inicioEventos { get; set; }
            [JsonProperty("RCE_fecha_terminoEventos")]
            public string RCE_fecha_terminoEventos { get; set; }
            [JsonProperty("RCE_causaEventos")]
            public string RCE_causaEventos { get; set; }
            [JsonProperty("RCE_Tipo_Evento_RCE")]
            public string RCE_Tipo_Evento_RCE { get; set; }
            [JsonProperty("RCE_idTipo_Evento")]
            public int RCE_idTipo_Evento { get; set; }
            [JsonProperty("RCE_descripcionTipo_Evento")]
            public string RCE_descripcionTipo_Evento { get; set; }
            [JsonProperty("RCE_estadoTipo_Evento")]
            public string RCE_estadoTipo_Evento { get; set; }
            [JsonProperty("RCE_estadoEventos")]
            public string RCE_estadoEventos { get; set; }
        }

        public class Interconsulta
        {

            [JsonProperty("RCE_idInterconsulta")]
            public int RCE_idInterconsulta { get; set; }
            [JsonProperty("GEN_idUbicacion_enviadoInterconsulta")]
            public int GEN_idUbicacion_enviadoInterconsulta { get; set; }
            [JsonProperty("GEN_idUbicacion_derivadoInterconsulta")]
            public int GEN_idUbicacion_derivadoInterconsulta { get; set; }
            [JsonProperty("RCE_diagnosticoInterconsulta")]
            public string RCE_diagnosticoInterconsulta { get; set; }
            [JsonProperty("RCE_sintomatologiaInterconsulta")]
            public string RCE_sintomatologiaInterconsulta { get; set; }
            [JsonProperty("RCE_solicitudInterconsulta")]
            public string RCE_solicitudInterconsulta { get; set; }
            [JsonProperty("RCE_fecha_hora_creacionInterconsulta")]
            public DateTime RCE_fecha_hora_creacionInterconsulta { get; set; }
            [JsonProperty("GEN_idProfesional")]
            public int GEN_idProfesional { get; set; }
            [JsonProperty("RCE_idEventos")]
            public int RCE_idEventos { get; set; }
            [JsonProperty("RCE_estadoInterconsulta")]
            public string RCE_estadoInterconsulta { get; set; }
            [JsonProperty("GEN_idEspecialidad_enviadaInterconsulta")]
            public int GEN_idEspecialidad_enviadaInterconsulta { get; set; }
            [JsonProperty("GEN_idEspecialidad_derivadaInterconsulta")]
            public int GEN_idEspecialidad_derivadaInterconsulta { get; set; }
            [JsonProperty("GEN_idTipo_Estados_Sistemas")]
            public int GEN_idTipo_Estados_Sistemas { get; set; }
            [JsonProperty("RCE_idInterconsulta_GES")]
            public int RCE_idInterconsulta_GES { get; set; }

        }

        public class Interconsulta_GES
        {
            [JsonProperty("RCE_idInterconsulta_GES")]
            public int RCE_idInterconsulta_GES { get; set; }
            [JsonProperty("RCE_idGrupo_Patologia_GES")]
            public int RCE_idGrupo_Patologia_GES { get; set; }
            [JsonProperty("RCE_idSubgrupo_Patologia_GES")]
            public int RCE_idSubgrupo_Patologia_GES { get; set; }
            [JsonProperty("RCE_idTipo_Interconsulta")]
            public int RCE_idTipo_Interconsulta { get; set; }
            [JsonProperty("RCE_fundamentos_diagnosticoInterconsulta_GES")]
            public string RCE_fundamentos_diagnosticoInterconsulta_GES { get; set; }
            [JsonProperty("RCE_tratamientoInterconsulta_GES")]
            public string RCE_tratamientoInterconsulta_GES { get; set; }
            [JsonProperty("RCE_estadoInterconsulta_GES")]
            public string RCE_estadoInterconsulta_GES { get; set; }
            [JsonProperty("RCE_idInterconsulta")]
            public int RCE_idInterconsulta { get; set; }
            [JsonProperty("RCE_otros_tipoInterconsulta_GES")]
            public string RCE_otros_tipoInterconsulta_GES { get; set; }
        }

        public class Ubicacion
        {
            [JsonProperty("GEN_idUbicacion")]
            public int GEN_idUbicacion { get; set; }
            [JsonProperty("GEN_nombreUbicacion")]
            public string GEN_nombreUbicacion { get; set; }
            [JsonProperty("GEN_idUbicacionPadre")]
            public int GEN_idUbicacionPadre { get; set; }
            [JsonProperty("GEN_estadoUbicacion")]
            public string GEN_estadoUbicacion { get; set; }
        }

        public class Tipo_Interconsulta
        {
            [JsonProperty("RCE_idTipo_Interconsulta")]
            public int RCE_idTipo_Interconsulta { get; set; }
            [JsonProperty("RCE_DescripcionTipo_Interconsulta")]
            public string RCE_DescripcionTipo_Interconsulta { get; set; }
            [JsonProperty("RCE_estadoTipo_Interconsulta")]
            public string RCE_estadoTipo_Interconsulta { get; set; }
        }

        public class Hoja_Evolucion
        {
            
            [JsonProperty("RCE_idHoja_Evolucion")]
            public int RCE_idHoja_Evolucion { get; set; }
            [JsonProperty("GEN_idUbicacion")]
            public int GEN_idUbicacion { get; set; }
            [JsonProperty("RCE_fechaHoja_Evolucion")]
            public DateTime RCE_fechaHoja_Evolucion { get; set; }
            [JsonProperty("RCE_idEventos")]
            public int RCE_idEventos { get; set; }
            [JsonProperty("GEN_idProfesional")]
            public int GEN_idProfesional { get; set; }
            [JsonProperty("RCE_descripcionHoja_Evolucion")]
            public string RCE_descripcionHoja_Evolucion { get; set; }
            [JsonProperty("RCE_estadoHoja_Evolucion")]
            public string RCE_estadoHoja_Evolucion { get; set; }
            [JsonProperty("GEN_idPaciente")]
            public int GEN_idPaciente { get; set; }

        }
    }
}