﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Datos
{
    public class RCE_Interconsulta_GES
    {
        public int RCE_idInterconsulta_GES { get; set; }
        public int RCE_idGrupo_Patologia_GES { get; set; }
        public int RCE_idSubgrupo_Patologia_GES { get; set; }
        public int RCE_idTipo_Interconsulta { get; set; }
        public string RCE_fundamentos_diagnosticoInterconsulta_GES { get; set; }
        public string RCE_tratamientoInterconsulta_GES { get; set; }
        public string RCE_estadoInterconsulta_GES { get; set; }
        public int RCE_idInterconsulta { get; set; }
        public string RCE_otros_tipoInterconsulta_GES { get; set; }
    }
}