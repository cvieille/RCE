﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Datos.DTO
{   
    public class PacienteDto
    {
        public Nullable<int> IdPaciente { get; set; }
        public int? Nui { get; set; }
        public Nullable<int> IdIdentificacion { get; set; }
        public string DescripcionIdentificacion { get; set; }
        public string NumeroDocumento { get; set; }
        public string Digito { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public Nullable<int> IdSexo { get; set; }
        public string NombreSexo { get; set; }
        public Nullable<int> IdGenero { get; set; }
        public string NombreGenero { get; set; }
        public Nullable<DateTime> FechaNacimiento { get; set; }
        public EdadDto Edad { get; set; }
        public string DireccionCalle { get; set; }
        public string DireccionNumero { get; set; }
        public string Telefono { get; set; }
        public string OtrosFonos { get; set; }
        public string Email { get; set; }
        public Nullable<int> IdRegion { get; set; }
        public Nullable<int> IdProvincia { get; set; }
        public Nullable<int> IdCiudad { get; set; }
        public Nullable<int> IdPrevision { get; set; }
        public Nullable<int> IdPrevisionTramo { get; set; }
        public string NombreCiudad { get; set; }
    }
}