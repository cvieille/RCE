﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Datos.DTO
{
    public class CategorizacionDto
    {
        public Nullable<DateTime> FechaHora { get; set; }
        public string Codigo { get; set; }
        public string Usuario { get; set; }
    }
}