﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Datos.DTO
{
    public class EdadDto
    {
        public int años { get; set; }
        public int meses { get; set; }
        public int dias { get; set; }
        public string edad { get; set; }
    }
}