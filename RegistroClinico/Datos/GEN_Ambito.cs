﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Ambito
    {
        public int GEN_idAmbito { get; set; }
        public string GEN_nombreAmbito { get; set; }
        public string GEN_estadoAmbito { get; set; }
    }
}