﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Tipo_Representante
    {
        public int GEN_idTipo_Representante { get; set; }
        public string GEN_nombreTipo_Representante { get; set; }
    }
}