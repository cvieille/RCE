﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Profesional
    {
        public int GEN_idProfesional { get; set; }
        public string GEN_rutProfesional { get; set; }
        public char? GEN_digitoProfesional { get; set; }
        public string GEN_nombreProfesional { get; set; }
        public string GEN_apellidoProfesional { get; set; }
        public string GEN_sapellidoProfesional { get; set; }
        public int GEN_idSexo { get; set; }
        public string GEN_dependenciaProfesional { get; set; }
        public string GEN_estadoProfesional { get; set; }
        public string GEN_emailProfesional { get; set; }
    }
}