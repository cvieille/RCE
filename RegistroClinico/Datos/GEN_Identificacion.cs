﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Datos
{
    public class GEN_Identificacion
    {
        public int GEN_idIdentificacion { get; set; }
        public string GEN_nombreIdentificacion { get; set; }
        public string GEN_estadoIdentificacion { get; set; }
    }
}