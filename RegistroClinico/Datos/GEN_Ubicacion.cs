﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Datos
{
    public class GEN_Ubicacion
    {
        public int GEN_idUbicacion { get; set; }
        public string GEN_nombreUbicacion { get; set; }
        public int GEN_idUbicacionPadre { get; set; }
        public string GEN_estadoUbicacion { get; set; }
    }
}