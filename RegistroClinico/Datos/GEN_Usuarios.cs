﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico
{
    public class GEN_Usuarios
    {
        public int GEN_idUsuarios { get; set; }
        public string GEN_loginUsuarios { get; set; }
        public string GEN_claveUsuarios { get; set; }
        public string GEN_nombreUsuarios { get; set; }
        public string GEN_apellido_paternoUsuarios { get; set; }
        public string GEN_apellido_maternoUsuarios { get; set; }
        public string GEN_rutUsuarios { get; set; }
        public char GEN_digitoUsuarios { get; set; }
        public string GEN_telefonoUsuarios { get; set; }
        public string GEN_recibe_notificacionesUsuarios { get; set; }
        public string GEN_emailUsuarios { get; set; }
    }
}