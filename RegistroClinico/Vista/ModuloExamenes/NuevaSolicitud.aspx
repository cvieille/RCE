﻿<%@ Page Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevaSolicitud.aspx.cs" Inherits="RegistroClinico.Vista.ModuloExamenes.NuevaSolicitud" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/bootstrap-clockpicker.css" rel="stylesheet"/>
    <script src="<%= ResolveClientUrl("~/Script/jquery.autocomplete.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/bootstrap-clockpicker.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/jquery-clockpicker.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloExamenes/NuevaSolicitud.js") + "?" + GetVersion() %>"></script>
    <style>
        label:not(.form-check-label):not(.custom-file-label) {
            font-weight:400;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">
    <input type="hidden" id="idProSol" value="0"/>
    <input type="hidden" id="idPac" value="0"/>
    <input type="hidden" id="idProToma" value="0"/>

    <div class="card card-body">
        <div class="card">
            <div class="card-body" id="divExamen">
                <p class="h4 text-center py-4">Solicitud de Exámenes de Laboratorio</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-2" style="margin-top:-2px;">
                                <div class="md-form">
                                    <label class="active">Fecha</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtFecha" class="form-control" type="date" required/>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top:8px;">
                                 <div class="md-form clockpicker" data-placement="top" data-align="bottom" data-autoclose="true">
                                    <i class="far fa-clock prefix"></i>
                                    <input type="time" id="txtHora" required class="form-control" />
                                    <label class="active" for="txtHora">Hora</label>
                                </div>
                            </div>
                        </div>
                        <h5 class="card-title">Datos Paciente</h5>                        
                        <div class="row">
                            <div class="col-md-2" style="margin-top:10px;">
                                <div class="md-form">
                                    <input id="txtRutPac" data-required="true" type="text" class="form-control" maxlength="10" onclick="$(this).select();"/>
                                    <label id="lblValidarRutPac" class="active" for="txtRutPac">RUT</label>
                                    <span style="color:crimson;" id="spanRutIncorrecto">
                                        <i class="fa fa-times"></i> <strong>Rut Incorrecto</strong>
                                    </span>
                                    <span style="color:darkgreen; display:none;" id="spanRutCorrecto">
                                        <i class="fa fa-check"></i> <strong>Rut Correcto</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-1" style="margin-top:10px;">
                                <div class="md-form">
                                    <input type="text" data-required="true" id="txtRutDigitoPac" class="form-control" maxlength="1" style="width: 40px; text-align: center;"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="md-form">
                                    <label for="txtUbInterna" class="active">N° Ubicación Interna</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtUbInterna" disabled placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="md-form">
                                    <label for="txtNombrePac" class="active">Nombre</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtNombrePac" disabled placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="md-form">
                                    <label for="txtApellidoPac" class="active">Apellido Paterno</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtApellidoPac" disabled placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="md-form">
                                    <label for="txtSapellidoPac" class="active">Apellido Materno</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtSapellidoPac" disabled placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top:-2px;">
                                <div class="md-form">
                                    <label for="txtFechaNacPac" class="active">Fecha Nacimiento</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtFechaNacPac" disabled class="disabled form-control" type="date" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="md-form">
                                    <label for="ddlServicio" class="active">Servicio</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <select id="ddlServicio" class="selectpicker" 
                                        data-style="btn-primary-dark waves-effect" data-required="true"
                                        title="Seleccione">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form">
                                    <label for="txtDiagnostico" class="active">Diagnóstico</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <textarea id="txtDiagnostico" class="md-textarea md-textarea-auto form-control" rows="1"></textarea>
                                </div>
                            </div>
                            <%--<div class="col-md-3 offset-3">
                                <hr style="border-top: 1px dotted #000000;" />
                                <strong>Informé diagnóstico GES</strong>
                                <p>Firma</p>
                            </div>--%>
                        </div>
                        <h5 class="card-title">Profesional Solicitante</h5>
                        <div class="row">
                            <div class="col-md-2" style="margin-top:10px;">
                                <div class="md-form">
                                    <label id="lblValidarRutPro" class="active" for="txtRutPro">RUT</label>
                                    <input id="txtRutPro" data-required="true" placeholder="&nbsp;" type="text" class="form-control disabled" maxlength="10" onclick="$(this).select();"/>
                                    <%--<span style="color:crimson;" id="spanRutIncorrectoPro">
                                        <i class="fa fa-times"></i> <strong>Rut Incorrecto</strong>
                                    </span>
                                    --%>
                                    <span style="color:darkgreen;" id="spanRutCorrectoPro">
                                        <i class="fa fa-check"></i> <strong>Rut Correcto</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-1" style="margin-top:10px;">
                                <div class="md-form">
                                    <input type="text" data-required="true" id="txtRutDigitoPro" class="form-control disabled" maxlength="1" style="width: 40px; text-align: center;"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="md-form">
                                    <label for="txtNombrePro" class="active">Nombre Profesional</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtNombrePro" placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <%--<div class="row">
                            <div class="col-md-6">
                                <h5 class="card-title">Exámenes Bioquímicos</h5>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExAlubimina" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExAlubimina">1370 ALBUMINA</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExAmilasemia" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExAmilasemia">1460 AMILASEMIA</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExCalcio" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExCalcio">1400 CALCIO</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExCkMb" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExCkMb">1340 CK MB</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExCkTotal" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExCkTotal">1330 CK TOTAL</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExColesterol" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExColesterol">1210 COLESTEROL TOTAL</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExCreatinina" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExCreatinina">1190 CREATININA</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExElectrolitos" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExElectrolitos">10 ELECTROLITOS PLASMÁTICOS</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExGlucosa" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExGlucosa">1000 GLUCOSA</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExHb" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExHb">14 HB GLICOSILADA A1c</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExLactaso" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExLactaso">1475 LACTATO</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExPcr" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExPcr">1480 PCR</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExPhGasesArteriales" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExPhGasesArteriales">11 pH Y GASES ARETERIALES</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExPhGasesVenosos" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExPhGasesVenosos">43 pH Y GASES VENOSOS</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExProteinas" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExProteinas">1360 PROTEINAS</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExPruebasH" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExPruebasH">3 PRUEBAS HEPÁTICAS</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExTrigli" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExTrigli">1240 TRIGLICERIDOS</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExUrea" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExUrea">1180 UREA/NITRÓGENO UREICO</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h5 class="card-title">Exámenes Hematológicos</h5>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExPerfil" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExPerfil">21 PERFIL HEMATOLÓGICO (PH)</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExHemoGrama" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExHemoGrama">23 HEMOGRAMA (PH + FROTIS)</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExVhs" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExVhs">3220 VHS</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExProt" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExProt">6010 PROTROMBINA/INR</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExTTPK" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExTTPK">6040 TTPK</label>
                                </div>
                                <br />
                                <h5 class="card-title">Exámenes Bacteriológicos</h5>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExCultivo" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExCultivo">16 CULTIVO L.C.R.</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExHemocultivo" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExHemocultivo">55 HEMOCULTIVO PERIFÉRICO</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExUro" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExUro">1 UROCULTIVO</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExSedimento" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExSedimento">31 SEDIMENTO ORINA</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExCitoLCR" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExCitoLCR">90 CITOQUÍMICO LCR</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExCitoLiquido" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExCitoLiquido">93 CITOQUÍMICO LÍQUIDOS</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExBHCG" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExBHCG">5090 BHCG</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExTSH" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExTSH">5010 TSH</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExT4" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExT4">5030 T4 LIBRE</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input id="chkExVDRL" type="checkbox" class="custom-control-input">
                                    <label class="custom-control-label" for="chkExVDRL">5510 VDRL</label>
                                </div>
                            </div>
                        </div>
                        <br />--%>
                        <h5 class="card-title">Exámenes</h5>
                        <div id="divExamenes">
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form">
                                    <label class="active">Examen</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtExamen" placeholder="Escriba el nombre del examen" type="text" class="form-control form-control-sm"/>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="md-form">
                                    <a id="lnbAgregarEx" class="btn btn-info waves-effect waves-light" href="#\">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <h5 class="card-title">Profesional Toma de Muestra</h5>
                        <div class="row">
                            <div class="col-md-2" style="margin-top:10px;">
                                <div class="md-form">
                                    <input id="txtRutProS" data-required="true" type="text" class="form-control" maxlength="10" onclick="$(this).select();"/>
                                    <label id="lblValidarRutProS" class="active" for="txtRutProS">RUT</label>
                                    <span style="color:crimson;" id="spanRutIncorrectoProS">
                                        <i class="fa fa-times"></i> <strong>Rut Incorrecto</strong>
                                    </span>
                                    <span style="color:darkgreen; display:none;" id="spanRutCorrectoProS">
                                        <i class="fa fa-check"></i> <strong>Rut Correcto</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-1" style="margin-top:10px;">
                                <div class="md-form">
                                    <input type="text" data-required="true" id="txtRutDigitoProS" class="form-control" maxlength="1" style="width: 40px; text-align: center;"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="md-form">
                                    <label for="txtNombrePro" class="active">Nombre Profesional</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtNombreProS" disabled placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                 <div class="text-center py-3 mt-3">
                    <button id="btnConfirmar" class="btn btn-cyan" onclick="return false;" type="submit">CONFIRMAR</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>