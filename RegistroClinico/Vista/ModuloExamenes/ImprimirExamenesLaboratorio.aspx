﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/Impresion.Master" AutoEventWireup="true" CodeBehind="ImprimirExamenesLaboratorio.aspx.cs" Inherits="RegistroClinico.Vista.ModuloExamenes.ImprimirExamenesLaboratorio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="panel">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-2">
                    <br />
                </div>
                <div class="col-xs-8">
                    <h4 class="text-center"><strong>SOLICITUD DE EXÁMENES DE LABORATORIO</strong></h4>
                </div>
                <div class="col-xs-2">
                    <br />
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding: 0px;">

            <div class="estilo-titulo">
                <div class="row">
                    <div class="col-xs-12" style="padding-left: 0px !important; padding-right: 0px;">
                        1- Datos de Paciente
                    </div>
                </div>
            </div>

            <table id="tblIdentificaPaciente" style="width: 100%; border: 1px solid black;">
                <tr>
                    <td class="fondo-titulos contenido-celda col-td-1">
                        <asp:Label ID="lblidentificacion" runat="server" Text="">
                        </asp:Label>
                    </td>
                    <td class="contenido-celda col-td-3">
                        <asp:Label ID="lblRutPac" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda col-td-1">Nombre</td>
                    <td class="contenido-celda col-td-7" colspan="3">
                        <asp:Label ID="lblNombrePac" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda col-td-1">NUI</td>
                    <td class="contenido-celda col-td-3">
                        <asp:Label ID="lblNumeroUbicacionInterna" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda col-td-2">Fecha de nacimiento</td>
                    <td class="contenido-celda col-td-3">
                        <asp:Label ID="lblFechaNacimiento" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda col-td-1">Edad</td>
                    <td class="contenido-celda col-td-2">
                        <asp:Label ID="lblEdad" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>

            <div class="estilo-titulo">2- Datos de clínicos</div>
            <table style="width: 100%; border: 1px solid black;">
                <tr>
                    <td class="fondo-titulos contenido-celda col-td-1">Servicio</td>
                    <td class="contenido-celda col-td-11">
                        <asp:Label ID="lblServicio" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda col-td-12" colspan="6">Hipótesis Diagnóstico</td>
                </tr>
                <tr>
                    <td class="contenido-celda col-td-12" colspan="6">
                        <asp:Label ID="lblDiagnostico" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>

            <div class="estilo-titulo">3- Datos de solicitud exámenes</div>
            <asp:Literal ID="ltlSolicitudesExamenes" runat="server">
            </asp:Literal>

            <div style="margin-top: 150px; text-align:center;">
                <asp:Label ID="lblNombreSolicitante" runat="server" style="width: 300px; border-top: solid 1px black;">
                </asp:Label>
            </div>

        </div>
    </div>
</asp:Content>
