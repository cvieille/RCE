﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BandejaExamenes.aspx.cs" Inherits="RegistroClinico.Vista.ModuloExamenes.BandejaExamenes" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master"%>

<asp:Content ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Solicitud de Exámenes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="#">Bandeja</a></li>
                        <li class="breadcrumb-item">Otro</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Bandeja Exámenes</h3>
                    </div>
                    <div class="card-body">
                        <div class="text-right">
                           <%-- <asp:LinkButton runat="server" CommandArgument="asdasdasd" OnClick="lnbExportar_Click" class="btn btn-default waves-effect waves-light" ID="lnbExportar">
                                <i class="fa fa-file-excel-o"></i> Exportar XLS
                            </asp:LinkButton>--%>
                            <asp:Button runat="server" ID="lnbExportarH" OnClick="lnbExportarH_Click" style="display:none;"/>
                            <a id="lnbExportar" class="btn btn-default waves-effect waves-light disabled" href="#\">
                                <i class="fa fa-file-excel-o"></i> Exportar XLS
                            </a>
                            <a id="lnbNuevoIPD" class="btn btn-default load-click waves-effect waves-light" href="NuevaSolicitud.aspx">
                                <i class="fa fa-plus"></i> Nueva Solicitud
                            </a>
                        </div>
                        <table id="tblExamen" class="table table-bordered table-sm table-hover" style="font-size: smaller"></table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<%= ResolveClientUrl("~/Script/ModuloExamenes/BandejaExamenes.js") + "?" + GetVersion() %>"></script>
</asp:Content>