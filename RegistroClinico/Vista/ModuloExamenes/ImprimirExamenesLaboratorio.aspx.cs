﻿using Newtonsoft.Json;
using RegistroClinico.Datos.DTO;
using RegistroClinico.Negocio;
using RegistroClinico.Vista.ModuloUrgencia;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;

namespace RegistroClinico.Vista.ModuloExamenes
{
    public partial class ImprimirExamenesLaboratorio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            // Valida sesión según los permisos que tenga el perfil
            NegSession negSession = new NegSession(Page);
            negSession.ValidarPermisosSession();

            try
            {

                HttpContext context = HttpContext.Current;
                string idDAU = Request.QueryString["id"];
                string dominio = Funciones.GetUrlWebApi();
                string url = string.Format("{0}URG_Atenciones_Urgencia/{1}?" +
                                                "extens=INDICACIONESCLINICAS&" + 
                                                "extens=ATENCIONMEDICA", dominio, idDAU);

                string token = string.Format("Bearer {0}", Session["TOKEN"]/*Page.Request.Cookies["DATA-TOKEN"].Value*/);
                string sJson = Funciones.ObtenerJsonWebApi(url, token);

                SolicitudExamen solicitudExamenes = JsonConvert.DeserializeObject<SolicitudExamen>(sJson);

                // PACIENTE
                DibujarDatosPaciente(solicitudExamenes.Paciente);
                
                // DATOS CLÍNICOS
                DibujarDatosClinicos(solicitudExamenes.AtencionMedica);

                // Dibujar exámenes de laboratorio
                DibujarDatosAtencionClinica(solicitudExamenes.IndicacionesClinicas.ExamenesLaboratorio);
                
                Funciones funciones = new Funciones();
                string doc = "ExamLab";
                funciones.ExportarPDFWK(this.Page, doc, idDAU);

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al imprimir DAU", ex.Message + ex.StackTrace + ex.InnerException);
            }

        }

        private void DibujarDatosPaciente(PacienteDto paciente)
        {
            
            lblidentificacion.Text = paciente.DescripcionIdentificacion;
            lblRutPac.Text = paciente.NumeroDocumento;

            if (paciente.Digito != null)
                lblRutPac.Text += "-" + paciente.Digito;

            if (paciente.Nombre != null)
                lblNombrePac.Text = paciente.Nombre;

            if (paciente.ApellidoPaterno != null)
                lblNombrePac.Text += " " + paciente.ApellidoPaterno;

            if (paciente.ApellidoMaterno != null)
                lblNombrePac.Text += " " + paciente.ApellidoMaterno;

            lblNumeroUbicacionInterna.Text = Convert.ToString(paciente.Nui);

            if (paciente.FechaNacimiento != null)
            {
                lblFechaNacimiento.Text = DateTime.Parse(paciente.FechaNacimiento.ToString()).ToString("dd-MM-yyyy");
                Dictionary<string, object> edad = JsonConvert.DeserializeObject<Dictionary<string, object>>(Funciones.CalculaEdad(lblFechaNacimiento.Text));
                lblEdad.Text = Convert.ToString(edad["edad"]);
            }

        }
        private void DibujarDatosClinicos(AtencionMedica atencionMedica)
        {
            lblServicio.Text = "Urgencia";
            if(atencionMedica != null)
            {
                if (atencionMedica.Descripcion.Count > 0)
                {
                    lblDiagnostico.Text = atencionMedica.Descripcion[atencionMedica.Descripcion.Count - 1].HipotesisDiagnostica;
                }
            }
        }

        private void DibujarDatosAtencionClinica(List<ExamenesLaboratorio> examenesLaboratorio)
        {

            // Examenes Laboratorio
            ltlSolicitudesExamenes.Text =
                "<table style='width: 100%; border: 1px solid black;'>" +
                    "<thead>" +
                        "<tr>" +
                            "<th class='fondo-titulos contenido-celda col-td-3' style='text-align: center;'>Fecha Ingreso</th>" +
                            "<th class='fondo-titulos contenido-celda col-td-5' style='text-align: center;'>Profesional</th>" +
                            "<th class='fondo-titulos contenido-celda col-td-4' style='text-align: center;'>Descripción</th>" +
                        "</tr>" +
                    "</thead>" +
                    "<tbody>";

            foreach (ExamenesLaboratorio item in examenesLaboratorio)
            {

                string profesional = (item.ProfesionalSolicitante.Contains("|")) ?
                                        item.ProfesionalSolicitante.Substring(0, item.ProfesionalSolicitante.IndexOf("|")) :
                                        item.ProfesionalSolicitante;

                ltlSolicitudesExamenes.Text += 
                    string.Format(
                        "<tr style='text-align: center;'>" +
                            "<td class='contenido-celda'>{0}</td>" +
                            "<td class='contenido-celda'>{1}</td>" +
                            "<td class='contenido-celda'>{2}</td>" +
                        "</tr>", item.FechaIngreso, profesional.Trim(), item.Descripcion);

            }

            ltlSolicitudesExamenes.Text += "</tbody></table>";
            lblNombreSolicitante.Text = examenesLaboratorio[examenesLaboratorio.Count - 1].ProfesionalSolicitante;

        }

    }

    #region Clases

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class SolicitudExamen
    {
        public PacienteDto Paciente { get; set; }
        public AtencionMedica AtencionMedica { get; set; }
        public IndicacionesClinicas IndicacionesClinicas { get; set; }
    }

    public class IndicacionesClinicas
    {
        public List<ExamenesLaboratorio> ExamenesLaboratorio { get; set; }
    }

    public class ExamenesLaboratorio
    {
        public string Descripcion { get; set; }
        public string ProfesionalSolicitante { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string ObservacionesSolicitud { get; set; }
    }

    #endregion

}