﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="CambioContraseña.aspx.cs" Inherits="RegistroClinico.Vista.CambioContraseñaUSU" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    
    <script src="<%= ResolveClientUrl("~/Script/CryptoJS/core-min.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/Script/CryptoJS/md5.js")%>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Mantenedor de contraseña</h1>
                </div>
                <div class="col-sm-6">
                    <br />
                </div>
            </div>
        </div>
    </section>
    
    <section class="content">
        
        <div class="alert alert-danger text-center">
            <i class="fa fa-exclamation-circle"></i> POR POLÍTICAS DE SEGURIDAD DEBE CAMBIAR SU CONTRASEÑA POR UNA MÁS SEGURA<br />
            Mínimo 8 caracteres<br />
            Máximo 16 caracteres<br />
            Debe contener al menos una letra<br />
            Debe contener al menos un número
        </div>

        <div class="container">
            <div class="card">
                <div class="card-header grey lighten-2 text-bold">
                    Cambio de contraseña
                </div>
                <div id="divReinicioContraseña" class="card-body">

                    <div id="divContraseñaActual" class="input-group">
                        <div class="input-group-append">
                            <i class="fa fa-key center-horizontal-vertical m-3"></i>
                        </div>
                        <input id="txtContraseñaActual" type="password" class="form-control" placeholder="CONTRASEÑA ACTUAL" aria-label="CONTRASEÑA ACTUAL" aria-describedby="basic-addon1"
                            data-required="true" />
                    </div>

                    <div id="divContraseñaNueva" class="input-group">
                        <div class="input-group-append">
                            <i class="fa fa-key center-horizontal-vertical m-3"></i>
                        </div>
                        <input id="txtContraseñaNueva" type="password" class="form-control" placeholder="CONTRASEÑA NUEVA" aria-label="CONTRASEÑA NUEVA" aria-describedby="basic-addon1" 
                            data-required="true" />
                    </div>

                    <div id="divContraseñaNuevaRepetida" class="input-group">
                        <div class="input-group-append">
                            <i class="fa fa-key center-horizontal-vertical m-3"></i>
                        </div>
                        <input id="txtContraseñaNuevaRepetida" type="password" class="form-control" placeholder="REPITA CONTRASEÑA NUEVA" aria-label="REPITA CONTRASEÑA NUEVA" 
                            aria-describedby="basic-addon1" data-required="true" />
                    </div>

                    <div class="center-horizontal-vertical">
                        <a id="btnCambiarContraseña" class="btn btn-default">
                            <i class="fa fa-refresh"></i> Cambiar contraseña
                        </a>
                    </div>

                </div>
                <div class="card-footer lighten-2 text-bold">
                    Recuerde que el cambio de clave afecta a su usuario en los módulos/sistemas:
                    <ul>
                        <li>Protocolo Operatorio</li>
                        <li>Epicrisis</li>
                        <li>Receta e Indicaciones Médicas</li>
                        <li>Sistema de Pabellones</li>
                        <li>Anatomia Patológica</li>
                        <li>Exámenes de Laboratorio</li>
                        <li>Categorización</li>
                    </ul>
                </div>
            </div>    
        </div>

    </section>
    <script src="<%= ResolveClientUrl("~/Script/CambioContraseña.js") + "?" + GetVersion() %>"></script>

</asp:Content>
