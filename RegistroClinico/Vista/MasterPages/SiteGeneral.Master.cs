﻿using System;
using System.Web.UI;

namespace RegistroClinico.MasterPages
{
    public partial class SiteGeneral : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        { }
        public String ObtenerEstilo()
        {
            var pathForm = Page.AppRelativeVirtualPath;
            return (pathForm.Contains("Vista/Default.aspx")) ? "margin-left: 0px !important;" : "";
        }
    }
}