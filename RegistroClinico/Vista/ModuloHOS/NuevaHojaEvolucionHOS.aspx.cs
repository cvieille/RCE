﻿using System;
using System.Globalization;

namespace RegistroClinico.Vista.ModuloHOS
{
    public partial class NuevaHojaEvolucionHOS : Handlers.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack) {

                CultureInfo myCIintl = new CultureInfo("es-ES", false);

                DateTime dt = DateTime.Now;
                lbl_fechaActual.Text = dt.ToString("dddd dd 'de' MMMM 'del' yyyy", myCIintl).ToUpper();

            }
                
        }
    }
}