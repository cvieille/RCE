﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/Impresion.Master" AutoEventWireup="true" CodeBehind="ImprimirHojaEnfermeria.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.ImprimirHojaEnfermeria" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <link rel="stylesheet" href="<%= GetHost() %>/Style/Bootstrap_3_3_7/bootstrap.min.css" />
    <link rel="stylesheet" href="<%= GetHost() %>/Style/impresion.css" />
    <script type="text/javascript" src="<%= GetHost() %>/Script/Bootstrap_3_3_7/bootstrap.min.js"></script>

    <style>
        .tabla-custom{
            table-layout: fixed;
            width: 100%;
        }

        .base td{
            width: 8.333%;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">
    <div class="panel">
        <div class="panel-heading white">
            <div class="row">
                <div class="col-xs-4 text-left">
                    <img src="<%= GetHost() %>/Style/IMG/MINSAL.jpg" style="width: 100px;" />
                </div>
                <div class="col-xs-4 text-center">
                    <img src="<%= GetHost() %>/Style/IMG/ACRLOGO.jpg" style="width: 100px;" />
                </div>
                <div class="col-xs-4 text-right">
                    <img src="<%= GetHost() %>/Style/IMG/HOSPLOGO.jpg" style="width: 100px;" />
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding: 0px;">
            <table class="tabla-custom">
                <tr>
                    <td style="border: none;" colspan="2"></td>
                    <td style="border: none;" class="text-center" colspan="8"><h4><b>HOJA DE ENFERMERÍA - Hospital Clínico de Magallanes</b></h4></td><td style="border: none;" class="text-right" colspan="2"><h4><b><asp:Label ID="lblIdHojaEnfermeria" runat="server"></asp:Label></b></h4></td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12"><h4><b>DATOS PACIENTE</b></td>
                </tr>
                 <tr>
                    <td class="fondo-titulos contenido-celda" colspan="4">IDENTIFICACION: <asp:Label ID="identificacionLbl" runat="server"></asp:Label></td>
                    <td class="fondo-titulos contenido-celda" colspan="4">RUT: <asp:Label ID="rutLbl" runat="server"></asp:Label></td>
                    <td class="fondo-titulos contenido-celda" colspan="4">NOMBRE: <asp:Label ID="nombreLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="3">SEXO: <asp:Label ID="sexoLbl" runat="server"></asp:Label></td>
                    <td class="fondo-titulos contenido-celda" colspan="3">GENERO: <asp:Label ID="generoLbl" runat="server"></asp:Label></td>
                    <td class="fondo-titulos contenido-celda" colspan="3">TELEFONO: <asp:Label ID="telefonoLbl" runat="server"></asp:Label></td>
                    <td class="fondo-titulos contenido-celda" colspan="3">OTRO TELEFONO: <asp:Label ID="otroTelefonoLbl" runat="server"></asp:Label></td>
                
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="3">FECHA NACIMIENTO: <asp:Label ID="fechaNacimientoLbl" runat="server"></asp:Label></td>
                    <td class="fondo-titulos contenido-celda" colspan="2">EDAD: <asp:Label ID="edadLbl" runat="server"></asp:Label></td>
                    <td class="fondo-titulos contenido-celda" colspan="5">DIRECCIÓN: <asp:Label ID="direccionLbl" runat="server"></asp:Label></td>
                    <td class="fondo-titulos contenido-celda" colspan="2">NÚMERO: <asp:Label ID="direccionNumeroLbl" runat="server"></asp:Label></td>
                
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12"><h4><b>Nº HOSPITALIZACIÓN <asp:Label ID="numHospitalizacionLbl" runat="server"></asp:Label> </b></h4></td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12"><h5><b> HOJA GENERAL / FECHA <asp:Label ID="hgFecha" runat="server"></asp:Label> / TURNO <asp:Label ID="hgTurno" runat="server"></asp:Label></b> </h5></td>
                </tr>

                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12">DIAGNOSTICO: <asp:Label ID="diagnosticoLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12">MEDICAMENTOS HABITUALES: <asp:Label ID="medicamentosLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12">MORBIDOS: <asp:Label ID="morbidosLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12">ALERGIA: <asp:Label ID="alergiaLbl" runat="server"></asp:Label></td>
                </tr>

                <%-- PLAN DE CUIDADO --%>
                <tr>
                    <td class="fondo-titulos contenido-celda text-center" colspan="10"><h4><b>Plan de Cuidado</b></h4> </td>
                    <td class="fondo-titulos contenido-celda text-center" style= "color:red;" colspan="2">
                        <h6>
                            N =No Realizado
                            <br /> 
                            P = Pendiente
                            <br />
                            R = Realizado 
                        </h6>
                    </td>

                </tr>
            </table>
                 <asp:Label ID="planCuidado" runat="server"></asp:Label>

                <%-- Signos Vitales --%>
                <table class="tabla-custom">
                    <tr>
                        <td class="fondo-titulos contenido-celda text-center" colspan="12"><h4><b>Signos Vitales</b> </td>
                    </tr>
                </table>

                <asp:Label ID="signosVitalesLbl" runat="server"></asp:Label>

                <%-- Examenes laboratorio --%>
                <table class="tabla-custom">
                    <tr>
                        <td class="fondo-titulos contenido-celda text-center" colspan="12"><h4><b>Examenes Laboratorio</b> </td>
                    </tr>
                </table>

                <asp:Label ID="examenesLabLbl" runat="server"></asp:Label>

            <div style='page-break-after:always'></div>
                <%-- Evolucion --%>
                <table class="tabla-custom">
                    <tr>
                        <td class="fondo-titulos contenido-celda text-center"  colspan="12"><h4><b>Evoluciones</b></h4> </td>
                    </tr>
                    <tr>
                        <td class="fondo-titulos contenido-celda" colspan="12"><h6><b>Registro de Pesos</b></h6> </td>
                    </tr>
                </table>

                <asp:Label ID="evolucionesPesoLbl" runat="server"></asp:Label>

                <table class="tabla-custom">
                    <tr>
                        <td class="fondo-titulos contenido-celda" colspan="12"><h6><b>Registro de Tallas</b></h6> </td>
                    </tr>
                </table>
                <asp:Label ID="evolucionesTallaLbl" runat="server"></asp:Label>

                <table class="tabla-custom">
                    <tr>
                        <td class="fondo-titulos contenido-celda" colspan="12"><h6><b>Evolución del Paciente</b></h6> </td>
                    </tr>
                </table>
                <asp:Label ID="evolucionPacienteLbl" runat="server"></asp:Label>


                <%-- Invasivo --%>
                <table class="tabla-custom">
                    <tr>
                        <td class="fondo-titulos contenido-celda text-center"  colspan="12"><h4><b>Invasivos</b></h4> </td>
                    </tr>
                </table>

                <asp:Label ID="invasivosLbl" runat="server"></asp:Label>


             <div style='page-break-after:always'></div>

            <%-- Riesgos --%>
                <table class="tabla-custom">
                    <tr>
                        <td class="fondo-titulos contenido-celda text-center"  colspan="12"><h4><b>Riesgos</b></h4> </td>
                    </tr>
                </table>

                <asp:Label ID="riesgosLbl" runat="server"></asp:Label>


            <%-- Valoraciones --%>
                <table class="tabla-custom">
                    <tr>
                        <td class="fondo-titulos contenido-celda text-center"  colspan="12"><h4><b>Valoraciones</b></h4> </td>
                    </tr>
                </table>

                <asp:Label ID="valoracionesLbl" runat="server"></asp:Label>

            <div style='page-break-after:always'></div>

            <%-- Balance Hídrico --%>
                <table class="tabla-custom">
                    <tr>
                        <td class="fondo-titulos contenido-celda text-center"  colspan="12"><h4><b>Balance Hídrico</b></h4> </td>
                    </tr>
                    <tr>
                        <td class="fondo-titulos contenido-celda"  colspan="12"><h6><b>Ingresos</b></h6> </td>
                    </tr>
                </table>

                <asp:Label ID="balanceIngresoLbl" runat="server"></asp:Label>

            <table class="tabla-custom">
                    <tr>
                        <td class="fondo-titulos contenido-celda"  colspan="12"><h6><b>Egresos</b></h6> </td>
                    </tr>
                </table>

                <asp:Label ID="balanceEgresoLbl" runat="server"></asp:Label>


                <%--               Salto de pagina  
                    <div style='page-break-after:always'></div>
               --%>

                    
        </div>
    </div>
</asp:Content>
