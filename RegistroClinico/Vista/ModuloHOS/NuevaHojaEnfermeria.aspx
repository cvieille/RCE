﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NuevaHojaEnfermeria.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.NuevaHojaEnfermeria" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/SignosVitales.ascx" TagName="SignosVitales" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalSignosVitales.ascx" TagName="ModalSignosVitales" TagPrefix="wuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/jquery.typeahead.min.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />

    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <h3 class="text-center mb-0">
        <i class="fa fa-file-alt"></i>Hoja Enfermería / Matronería
    </h3>

    <wuc:Paciente ID="wucPaciente" runat="server" />
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:ModalSignosVitales ID="ModalSigonsVitales" runat="server" />
    <div class="row bg-primary ml-1 mr-1 p-1">
        <div class="col-md-2">
            <i class="fas fa-bed fa-2x mt-2"></i>
        </div>
        <div class="col-md-4">
            <h5 class="mt-2">Fecha: <span id="fechaHosp" class="badge badge-light badge-success text-bold ml-1"></span>
            </h5>
        </div>
        <div class="col-md-3">
            <h5 class="mt-2 text-white" for="diasEstadia">Días Estadía: <b><span id="diasEstadia"></span></b></h5>
        </div>
        <div class="col-md-3">
            <h5 class="mt-2 text-white" for="cama">N° Cama: <b><span id="camaHosp"></span></b></h5>
        </div>
    </div>
    <div class="card">
        <%--            <div class="card-header bg-dark text-white">
                <h5>Intervenciones y Procedimientos</h5>
            </div>--%>
        <div class="card-body">
            <ul class="nav nav-tabs" id="intervencionProcedimiento" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="intervenciones" data-toggle="tab" href="#intervenciones-tab" role="tab"
                        aria-controls="intervenciones" aria-selected="true">
                        <h5 class="mt-2"><strong><i class="fas fa-user-md fa-lg mr-2"></i></strong>Intervenciones Realizadas <span id="cantidadIntervenciones" class="badge badge-pill badge-success ml-1"></span></h5>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="procedimientos" data-toggle="tab" href="#procedimientos-tab" role="tab" aria-controls="procedimientos"
                        aria-selected="false">
                        <h5 class="mt-2"><strong><i class="fas fa-tasks fa-lg mr-2"></i></strong>Cirugias Programadas <span id="cantidadProcedimientosProgramados" class="badge badge-pill badge-success ml-1"></span></h5>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="otros-procedimientos" data-toggle="tab" href="#otros-procedimientos-tab" role="tab" aria-controls="otros-procedimientos"
                        aria-selected="false">
                        <h5 class="mt-2"><strong><i class="fas fa-tasks fa-lg mr-2"></i></strong>Otros Procedimientos <span id="cantidadOtrosProcedimientos" class="badge badge-pill badge-success ml-1"></span></h5>
                    </a>
                </li>
            </ul>
            <div class="tab-content" id="nav-invervencionProcedimiento">
                <div class="tab-pane fade show active" id="intervenciones-tab" role="tabpanel" aria-labelledby="nav-invervenciones-tab">
                    <div class="row">
                        <div class="col-md mt-4">
                            <div id="alertIntervenciones">
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover col-xs-12 col-md-12" style="width: 100%;" id="tblIntervenciones">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="procedimientos-tab" role="tabpanel" aria-labelledby="nav-procedimientos-tab">
                    <div class="row">
                        <div class="col-md mt-4">
                            <div id="alertProcedimientos">
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover col-xs-12 col-md-12" style="width: 100%;" id="tblProcedimientosProgramados">
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 mt-4">
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault1">
                                <label class="form-check-label" for="flexSwitchCheckDefault">Intervención QX</label>
                            </div>
                        </div>
                        <div class="col-md-4 mt-4">
                            <label for="nombreIntervencion">Nombre Intervención: <b>5</b></label>
                        </div>
                        <div class="col-md-4 mt-4">
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault2">
                                <label class="form-check-label" for="flexSwitchCheckDefault">Ayuno</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="otros-procedimientos-tab" role="tabpanel" aria-labelledby="nav-otros-procedimientos-tab">
                    <div class="row">
                        <div class="col-12 col-md-6 mt-4">
                            <div id="alertOtrosProcedimientos">
                            </div>
                            <label for="txtOtrosProcedimientos">Otros Procedimientos:</label>
                            <textarea class="form-control" id="txtOtrosProcedimientos" rows="4" maxlength="300"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header bg-dark text-white">
            <h5><strong><i class="fas fa-procedures fa-lg mr-2"></i></strong><span id="cardIdHospitalizacion"></span></h5>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs" id="hojaEnfermeriaNav" role="tablist">
                <li class="nav-item h5">
                    <a class="nav-link active" id="hojaGeneral-tab" data-toggle="tab" href="#hojaEnfermeriaGeneral" role="tab" aria-controls="general" aria-selected="true">Hoja General
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja1" data-toggle="tab" href="#tipoHoja1" role="tab" aria-controls="tipoHoja1" aria-selected="false">
                        <span id="textTipoHojaEnfermeria1"></span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja2" data-toggle="tab" href="#tipoHoja2" role="tab" aria-controls="tipoHoja2" aria-selected="false">
                        <span id="textTipoHojaEnfermeria2"></span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja3" data-toggle="tab" href="#tipoHoja3" role="tab" aria-controls="tipoHoja3" aria-selected="false">
                        <span id="textTipoHojaEnfermeria3"></span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja4" data-toggle="tab" href="#tipoHoja4" role="tab" aria-controls="tipoHoja4" aria-selected="false">
                        <span id="textTipoHojaEnfermeria4"></span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja5" data-toggle="tab" href="#tipoHoja5" role="tab" aria-controls="tipoHoja5" aria-selected="false">
                        <span id="textTipoHojaEnfermeria5"></span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja6" data-toggle="tab" href="#tipoHoja6" role="tab" aria-controls="tipoHoja6" aria-selected="false">
                        <span id="textTipoHojaEnfermeria6"></span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja7" data-toggle="tab" href="#tipoHoja7" role="tab" aria-controls="tipoHoja7" aria-selected="false">
                        <span id="textTipoHojaEnfermeria7"></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="hojaEnfermeriaGeneral" role="tabpanel" aria-labelledby="nav-general-tab">
                    <div class="row">
                        <div class="col-md-2">
                            <label for="txtFechaHojaEnfermeria" class="mt-3">Fecha</label>
                            <input id="txtFechaHojaEnfermeria" type="date" class="form-control mt-1" placeholder="Fecha" disabled autocomplete="off" />
                        </div>
                        <div class="col-md-2">
                            <label for="sltIdTurnoHojaEnfermeria" class="form-label mt-3">Turno</label>
                            <select id="sltIdTurnoHojaEnfermeria" class="form-control mt-1" data-required="true">
                            </select>
                        </div>
                        <div class="col-6">
                            <label for="sltTipoHojaEnfermeria" class="form-label mt-3">Tipo Hoja Enfermería / Matronería</label>
                            <select id="sltTipoHojaEnfermeria" class="form-control mt-1" data-required="true">
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="txtDiagnosticoHojaEnfermeria" class="mt-3">Diagnostico</label>
                            <textarea id="txtDiagnosticoHojaEnfermeria" class="form-control" rows="3" maxlength="300" data-required="true" disabled></textarea>
                        </div>
                        <div class="col-md-6">
                            <label for="txtMedicamentosHabituales" class="mt-3">Medicamentos habituales</label>
                            <textarea id="txtMedicamentosHabituales" class="form-control" rows="3" maxlength="300"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="txtMorbidosHojaEnfermeria" class="mt-3">Morbidos</label>
                            <textarea id="txtMorbidosHojaEnfermeria" class="form-control" rows="3" maxlength="300" data-required="true" disabled></textarea>
                        </div>
                        <div class="col-md-6">
                            <label for="txtAlergiaHojaEnfermeria" class="mt-3">Alergia</label>
                            <textarea id="txtAlergiaHojaEnfermeria" class="form-control" rows="3" maxlength="300"></textarea>
                        </div>
                    </div>
                    <div class="row" id="divBrazalete">
                        <div class="col-md-2">
                            <div class="custom-control custom-switch mt-3">
                                <input type="checkbox" class="custom-control-input" id="switchBrazalete">
                                <label class="custom-control-label" for="switchBrazalete">¿Uso de brazalete?</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="sltIdTipoExtremidad" id="lblIdTipoExtremidad" class="form-label mt-3">Tipo Extremidad</label>
                            <select id="sltIdTipoExtremidad" class="form-control">
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="sltIdTipoPlano" id="lblIdTipoPlano" class="form-label mt-3">Plano</label>
                            <select id="sltIdTipoPlano" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div id="icon-info" class="alert alert-info text-center mt-3">
                        <h4><strong><i class="fa fa-info-circle fa-lg"></i></strong>
                            La información solamente será guardada con el boton ingresar al final del formulario.
                        </h4>
                    </div>
                </div>
                <!--inicio navbar-->
                <ul class="nav nav-tabs mt-5" id="plancuidadosSignosVitales" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="planCuidadosEnfermeria" data-toggle="tab" href="#plancuidadosenfermeria-tab" role="tab"
                            aria-controls="planCuidadosEnfermeria" aria-selected="true">
                            <h5 class="mt-2"><strong><i class="fas fa-tasks fa-lg mr-2"></i></strong>Plan de cuidados</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Signos-tab" data-toggle="tab" href="#Signos" role="tab"
                            aria-controls="Signos" aria-selected="false">
                            <h5 class="mt-2"><strong><i class="fa fa-heartbeat fa-lg mr-2"></i></strong>Signos vitales</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Examenes-tab" data-toggle="tab" href="#Examenes" role="tab"
                            aria-controls="Examenes" aria-selected="false">
                            <h5 class="mt-2"><strong><i class="fa fa-flask fa-lg mr-2"></i></strong>Examenes laboratorio</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Evolucion-tab" data-toggle="tab" href="#Evolucion" role="tab"
                            aria-controls="Examenes" aria-selected="false">
                            <h5 class="mt-2"><strong><i class="fa fa-file-alt fa-lg mr-2"></i></strong>Evoluciones</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Invasivo-tab" data-toggle="tab" href="#Invasivo" role="tab"
                            aria-controls="Examenes" aria-selected="false">
                            <h5 class="mt-2"><strong><i class="fas fa-diagnoses fa-lg mr-2"></i></strong>Invasivos</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Riesgos-tab" data-toggle="tab" href="#Riesgos" role="tab"
                            aria-controls="Riesgos" aria-selected="false">
                            <h5 class="mt-2"><strong><i class="fa fa-procedures"></i></strong>Riesgos</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Valoracion-tab" data-toggle="tab" href="#Valoracion" role="tab"
                            aria-controls="Valoracion" aria-selected="false">
                            <h5 class="mt-2"><strong><i class="fas fa-crutch"></i></strong>&nbsp; Valoraciones</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Hidrico-tab" data-toggle="tab" href="#Hidrico" role="tab"
                            aria-controls="Hidrico" aria-selected="false">
                            <h5 class="mt-2"><strong><i class="fas fa-water fa-lg"></i></strong>&nbsp; Balance hídrico</h5>
                        </a>
                    </li>
                </ul>
                <!--Fin navbar-->
                <!--Inicio tab content-->
                <div class="tab-content" id="nav-PlanCuidadosSignosVitales">
                    <div class="tab-pane fade active show" id="plancuidadosenfermeria-tab" role="tabpanel"
                        aria-labelledby="nav-plancuidadosenfermeria-tab">
                        <div id="alertPlanCuidadoEnfermeria" class="alert alert-secondary mt-3 text-center" role="alert" style="background-color: darkgray; border-color: darkgray; color: white">
                            Nada para mostrar
                        </div>
                        <div id="divPlanCuidadoEnfermeria" class="row">
                            <div class="col-md mt-4">
                                <div class="row">
                                    <div class="col-md mt-3">
                                        <h1>Plan de cuidado</h1>
                                        <%--<h5 id="titleTipoHojaEnfermeria4"></h5>--%>
                                        <div class="row mt-4">
                                            <div class="col-md-4">
                                                <label for="sltActividadesEnfermeria">Actividades</label>
                                                <select id="sltActividadesEnfermeria" class="form-control mb-4" data-required="true">
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="sltCantidadActividades">Cantidad</label>
                                                <select id="sltCantidadActividades" class="form-control mb-4" data-required="true">
                                                </select>
                                            </div>
                                            <div class="col-md-4 mt-4">
                                                <button id="btnAgregarActividades" class="btn btn-primary mt-2" type="button">Agregar Horarios</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md">
                                        <div id="divPlanCuidadoEnfermeria1" class="table-responsive">
                                            <table id="tblPlanCuidadoEnfermeria" class="table table-striped table-bordered table-hover col-xs-12 col-md-12">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Signos vitales-->
                    <div class="tab-pane fade show" id="Signos" role="tabpanel" aria-labelledby="pills-Signos-tab">
                        <div class="row mt-4">
                            <div class="col">
                                <a class="btn btn-success" id="aIngresosVitales">Ingreso signos vitales </a>
                            </div>
                        </div>
                        <div id="accordion" class="m-3" style="margin: 0 auto;">
                            <h4 id="titleIngresoActual"></h4>
                            <div id="infoSignosVitales" class="row"></div>
                        </div>
                        <div>
                            <h3 id="titleConSignos"></h3>
                            <div id="historialSignosVitales" class="row"></div>
                        </div>
                    </div>
                    <!-- fin singnos vitales-->
                    <!-- Examenes de laboratorio-->
                    <div class="tab-pane fade" id="Examenes" role="tabpanel" aria-labelledby="pills-Examenes-tab">
                        <div class="w-50" id="divExamLab" style="margin: 0 auto;">
                            <div class="row">
                                <div class="col-md-10">
                                    <label>Filtrar exámenes <b class="color-error">(*)</b></label>
                                    <div class="typeahead__container">
                                        <div class="typeahead__field">
                                            <div class="typeahead__query">
                                                <input id="thExamenesLaboratorio" name="hockey_v1[query]" type="search" placeholder="Buscar más exámenes"
                                                    autocomplete="off" data-required="true" onclick="this.select();">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <label>&nbsp;</label><br />
                                    <a id="addElementTableExam" class="btn btn-success"><i class="fa fa-plus"></i></a>
                                </div>
                                <!--<div class="col-md-1">
                                            <label>&nbsp;</label><br />
                                            <i id="icon-info" class="fa fa-question-circle fa-2x"></i>
                                        </div>-->
                            </div>
                        </div>

                        <!--- Tabla con eexamenes de laboratorio-->
                        <div style="margin: 0 auto;" id="divExamenesLaboratorio">
                            <table id="tblExamenesLaboratorio" class="w-100 table table-striped table-bordered table-hover dataTable no-footer">
                                <thead id="theadExamLab">
                                    <tr>
                                        <th>Nombre examen
                                        </th>
                                        <th>Fecha y profesional solicitud
                                        </th>
                                        <th>Estado
                                        </th>
                                        <th>Fecha y prof. cierre solicitud
                                        </th>
                                        <th>Acciones
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="tbodyExamLab">
                                </tbody>
                            </table>
                            <span id="iconografia"></span>
                        </div>
                    </div>
                    <!--Fin examenes laboratorio-->
                    <!--Hoja de evoluciones-->
                    <div class="tab-pane fade show" id="Evolucion" role="tabpanel" aria-labelledby="pills-Evolucion-tab">
                        <!--otro nav bar-->
                        <nav class="mt-5">
                            <div class="nav nav-tabs" id="Tab-HojaEvoluciones" role="tablist">
                                <a class="nav-item nav-link active" id="nav-ingresoEvolucion-tab" data-toggle="tab" href="#ingresoEvolucion" role="tab" aria-controls="nav-ingresoEvolucion" aria-selected="true">Nueva evolución</a>
                                <a class="nav-item nav-link" id="nav-historialEvolucion-tab" data-toggle="tab" href="#historialEvolucion" role="tab" aria-controls="nav-historialEvolucion" aria-selected="false">Historial de evolución</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent-Evoluciones">
                            <div class="tab-pane fade show active" id="ingresoEvolucion" role="tabpanel" aria-labelledby="nav-ingresoEvolucion-tab">
                                <div class="row m-3">
                                    <div class="col-md-1">
                                        <label>Peso</label>
                                        <input type="text" class="form-control decimal" placeholder="Ej:45" id="txtPesoPaciente" onkeyup="validarDecimales()" onkeypress="validarLongitud(this, 5)" />
                                    </div>
                                    <div class="col-md-1">
                                        <label>Medidas</label>
                                        <select class="form-control" id="sltTipoPeso"></select>
                                    </div>
                                    <div class="col-md-1">
                                        <label>Hora</label>
                                        <input type='time' class='form-control' id='tomaHoraPeso' placeholder='Hora'>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Fecha</label>
                                        <input type='date' class='form-control' id='tomaFechaPeso' placeholder='Fecha'>
                                    </div>
                                    <div class="col-md-1">
                                        <a class="btn btn-success" id="btnGuardarPeso" data-tipo="peso" onclick="AgregarPesoTalla(this)" style="margin-top: 30px;"><i class="fa fa-save"></i></a>
                                    </div>
                                    <div class="col-md-1">
                                        <label>Altura</label>
                                        <input type="text" class="form-control decimal" placeholder="Ej:170" id="txtTallaPacienteHojaEnfermeria" onkeypress="validarLongitud(this, 5)" />
                                    </div>
                                    <div class="col-md-1">
                                        <label>Medidas</label>
                                        <select class="form-control" id="sltTipoAltura"></select>
                                    </div>
                                    <div class="col-md-1">
                                        <label>Hora</label>
                                        <input type='time' class='form-control' id='tomaHoraAltura' placeholder='Horario'>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Fecha</label>
                                        <input type='date' class='form-control' id='tomaFechaAltura' placeholder='Fecha'>
                                    </div>
                                    <div class="col-md-1">
                                        <a class="btn btn-success" id="btnGuardarAltura" data-tipo="altura" onclick="AgregarPesoTalla(this)" style="margin-top: 30px;"><i class="fa fa-save"></i></a>
                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <table class="col-md-12 table table-striped table-bordered table-hover dataTable no-footer">
                                            <thead>
                                                <tr>
                                                    <th>Valor</th>
                                                    <th>Medida</th>
                                                    <th>Hora</th>
                                                    <th>Accion</th>
                                                </tr>
                                            </thead>
                                            <tbody id="listPeso">
                                                <tr id="infoIngresoPeso">
                                                    <td></td>
                                                    <td><i class='fa fa-info'></i>&nbsp; No se ha agregado información </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <table class="col-md-12 table table-striped table-bordered table-hover dataTable no-footer">
                                            <thead>
                                                <tr>
                                                    <th>Valor</th>
                                                    <th>Medida</th>
                                                    <th>Hora</th>
                                                    <th>Accion</th>
                                                </tr>
                                            </thead>
                                            <tbody id="listAltura">
                                                <tr id="infoIngresoAltura">
                                                    <td></td>
                                                    <td><i class='fa fa-info'></i>&nbsp; No se ha agregado información </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col">
                                        <label>Descripción de evolución</label>
                                        <textarea id="txtEvolucionAtencion"
                                            style="resize: none;"
                                            class="form-control"
                                            rows="10"
                                            placeholder="Describa la evolución del paciente"
                                            maxlength="3000">
                                            </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="historialEvolucion" role="tabpanel" aria-labelledby="nav-historialEvolucion-tab">
                                <div class="row mt-2">
                                    <div class="col-md-3" style="max-height: 400px; overflow: auto;">
                                        <h3><i class="fas fa-weight"></i>&nbsp; Registro de Pesos</h3>
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Fecha hora</th>
                                                    <th>Descripción</th>
                                                </tr>
                                            </thead>

                                            <tbody id="tbodyPesos">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3" style="max-height: 400px; overflow: auto;">
                                        <h3><i class="fas fa-ruler-vertical"></i>&nbsp; Registro de tallas</h3>
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Fecha hora</th>
                                                    <th>Descripción</th>
                                                </tr>
                                            </thead>

                                            <tbody id="tbodyTallas">
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-6">
                                        <h3><i class="far fa-list-alt"></i>&nbsp; Evolución del paciente</h3>

                                        <div id="tbodyDescripcion" style="max-height: 400px; overflow: auto;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- f otro nav-->
                    </div>
                    <!-- Fin hoja de evoluciones-->
                    <!--tab Invasivos-->
                    <div class="tab-pane fade show" id="Invasivo" role="tabpanel" aria-labelledby="pills-Invasivo-tab">
                        <!--otro nav bar-->
                        <nav class="mt-5">
                            <div class="nav nav-tabs" id="Tab-invasivos" role="tablist">
                                <a class="nav-item nav-link active" id="nav-invasivos-tab" data-toggle="tab" href="#ingresoInvasivo" role="tab" aria-controls="nav-ingresoEvolucion" aria-selected="true">Nueva invasivo</a>
                                <a class="nav-item nav-link" id="nav-historialInvasivos-tab" data-toggle="tab" href="#historialInvasivo" role="tab" aria-controls="nav-historialEvolucion" aria-selected="false">Historial de invasivos</a>
                            </div>
                        </nav>

                        <div class="tab-content" id="nav-tabContent-Invasivos">
                            <div class="tab-pane fade show active" id="ingresoInvasivo" role="tabpanel" aria-labelledby="nav-invasivos-tab">
                                <div id="cuerpoModalVigilancia" class="modal-body">
                                    <div class="row">
                                        <div class=" col-md-3">
                                            <label for="sltTipoInvasivoVigilancia">Invasivo:</label>
                                            <select id="sltTipoInvasivoVigilancia" class="form-control" data-required="true"></select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="sltTipoExtremidadVigilancia">Extremidad: </label>
                                            <select id="sltTipoExtremidadVigilancia" class="form-control" data-required="true"></select>
                                        </div>
                                        <div class="col-md-2">
                                            <label for="sltTipoPlanoVigilancia">Plano: </label>
                                            <select id="sltTipoPlanoVigilancia" class="form-control" data-required="true"></select>
                                        </div>

                                        <div class="col-md-2">
                                            <label for="dtFechaVigilancia">Fecha: </label>
                                            <input id="dtFechaVigilancia" type="date" class="form-control" data-required="true" />
                                        </div>
                                        <div class="col-md-2">
                                            <label for="tmHoraVigilancia">Hora: </label>
                                            <input id="tmHoraVigilancia" type="time" class="form-control" data-required="true" />
                                        </div>
                                    </div>

                                    <br />
                                    <div class="row">
                                        <label for="txtObservacionesVigilancia">&nbsp; Observaciones</label>
                                        <br />
                                        <div class="col-md-12">
                                            <textarea id="txtObservacionesVigilancia" class="form-control" maxlength="300" rows="3" placeholder="Ingrese las observaciones.." data-required="false"></textarea>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center justify-content-md-end">
                                        <div class="col-4 col-sm-3 col-md-2 col-lg-1">
                                            <br />
                                            <a href="javascript:void(0)" id="btnAgregarVigilancia" class="btn btn-success form-control" onclick="agregarVigilancia()">
                                                <i class="fa fa-plus" style="font-size: 20pt;"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <br />
                                    <hr />
                                    <div id="ocultarTablaVigilanciaIAAS">
                                        <table class="table table-bordered table-sm table-hover" style="font-size: smaller; width: 100%;" id="tblVigilanciaIAAS">
                                        </table>
                                    </div>

                                    <span id="alertVigilanciaIAASPorHospitalizacion"></span>
                                </div>
                            </div>
                            <div class="tab-pane fade " id="historialInvasivo" role="tabpanel" aria-labelledby="nav-invasivos-tab">
                                <div class="row mt-2">
                                    <div class="col">

                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="col-1">Invasivo</th>
                                                    <th class="col-1">Extremidad</th>
                                                    <th class="col-1">Plano</th>
                                                    <th class="col-1">Fecha, prof. solicita</th>
                                                    <th class="col-1">Fecha, prof retira</th>
                                                    <th class="col-3">Observaciones</th>
                                                    <th class="col-1">Acciones</th>
                                                </tr>
                                            </thead>

                                            <tbody id="tbodyInvasivos">
                                            </tbody>
                                        </table>
                                        <div>
                                            <span style="margin-left: 20px; height: 20px; width: 20px; margin-right: 10px; background-color: rgb(223, 240, 216); border-radius: 50%; display: inline-block; border: 1px solid black;"></span>
                                            <span>Dispositivo en uso</span>
                                            <span style="margin-left: 20px; height: 20px; width: 20px; margin-right: 10px; background-color: #ed969e; border-radius: 50%; display: inline-block; border: 1px solid black;"></span>
                                            <span>Dispositivo retirado</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--fin otro navbar-->
                    </div>
                    <!--Fin tab invasivos-->

                    <!--Riesgos tab-->
                    <div class="tab-pane fade show" id="Riesgos" role="tabpanel" aria-labelledby="pills-Riesgos-tab">
                        <div>
                            <button type="button" id="addRiesgos" class="btn btn-success mt-2" onclick="getRiesgosSeguridad(1)">Agregar evaluación de Riesgo</button>
                            <button type="button" id="aRiesgos" class="btn btn-danger mt-2 d-none" onclick="getRiesgosSeguridad(1)">Cancelar evaluación de Riesgo</button>
                        </div>
                        <div class="row m-3" id="divRiesgos">
                            <div class="nav flex-column nav-pills col-lg-3" id="riesgos-pills-tab" role="tablist" aria-orientation="vertical">
                            </div>
                            <div class="tab-content col-lg-9" id="riesgos-pills-tabContent" style="max-height: 800px; overflow: auto;">
                            </div>
                        </div>
                        <div class="row" id="divHistorialRiesgos">
                            <div id="titleHistorialRiesgo" class="col-md-5"></div>
                            <div class="col-md-7"></div>
                            <br />
                            <div id="historialRiesgos" class="col-md-12">
                            </div>
                        </div>
                    </div>
                    <!--Fin riesgos tab-->
                    <!--Valoraciones tab-->
                    <div class="tab-pane fade show" id="Valoracion" role="tabpanel" aria-labelledby="pills-Valoracion-tab">
                        <div>
                            <button type="button" id="addValoracion" class="btn btn-success mt-2" onclick="getRiesgosSeguridad(10)">Agregar valoración</button>
                            <button type="button" id="aValoracion" class="btn btn-danger mt-2 d-none" onclick="getRiesgosSeguridad(10)">Cancelar valoración</button>
                        </div>
                        <div class="row m-3" id="divValoracion">
                        </div>
                        <div class="row" id="divHistorialValoracion">
                            <div id="titleHistorialValoracion" class="col-md-5"></div>
                            <div class="col-md-7"></div>
                            <br />
                            <div id="historialValoracion" class="col-md-12">
                            </div>
                        </div>
                    </div>
                    <!--Fin valoraciones tab-->
                    <!--tab balance hidrico-->
                    <div class="tab-pane fade show" id="Hidrico" role="tabpanel" aria-labelledby="pills-Hidrico-tab">
                        <!--otro nav bar-->
                        <nav class="mt-4">
                            <div class="nav nav-tabs" id="Tab-hidrico" role="tablist">
                                <a class="nav-item nav-link active" id="nav-hidrico-tab" data-toggle="tab" href="#ingresoHidrico" role="tab" aria-controls="nav-ingresoHidrico" aria-selected="true">Ingreso balances Hidrico</a>
                                <a class="nav-item nav-link" id="nav-historialHidrico-tab" data-toggle="tab" href="#historialHidrico" role="tab" aria-controls="nav-historialHidrico" aria-selected="false">Historial de balances Hidricos</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent-Hidricos">
                            <div class="tab-pane fade show active" id="ingresoHidrico" role="tabpanel" aria-labelledby="nav-invasivos-tab">
                                <div class="row mt-3 mb-3" id="divInputHidricos">
                                    <div class="col-md-3 "></div>
                                    <div class="col-md-3">
                                        <select id="selectHidrico" class="form-control">
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control decimal" id="valorHidrico" placeholder="Ingrese valor:ej 45.5" onkeyup="validarDecimales()" onkeypress="validarLongitud(this, 6)" data-required="true" />
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" id="addHidrico" class="btn btn-outline-success" onclick="agregarHidricos()"><i class="fa fa-plus"></i>Agregar</button></div>
                                    <div class="col-md-2"></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4>Tabla de ingresos</h4>
                                        <table class="table table-bordered table-sm table-hover" style="font-size: smaller; width: 100%;" id="dataTableIngresoHidrico">
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Tabla de egresos</h4>
                                        <table class="table table-bordered table-sm table-hover" style="font-size: smaller; width: 100%;" id="dataTableEgresoHidrico">
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="historialHidrico" role="tabpanel" aria-labelledby="nav-invasivos-tab">
                                <div class="row mt-3">
                                    <div class="col-md-2">
                                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <a class="nav-link active" id="v-pills-Ingresos-tab" data-toggle="pill" href="#v-pills-Ingresos" role="tab" aria-controls="v-pills-Ingresos" aria-selected="true">Historial ingresos</a>
                                            <a class="nav-link" id="v-pills-Totales-tab" data-toggle="pill" href="#v-pills-Totales" role="tab" aria-controls="v-pills-Totales" aria-selected="false">Totales</a>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="tab-content" id="v-pills-tabContent">
                                            <div class="tab-pane fade show active" id="v-pills-Ingresos" role="tabpanel" aria-labelledby="v-pills-Ingresos-tab">
                                                <div id="divHistorialHidricos" class="row mt-2"></div>
                                            </div>
                                            <div class="tab-pane fade" id="v-pills-Totales" role="tabpanel" aria-labelledby="v-pills-Totales-tab">
                                                <div id="totalHistorialHidrico" class="row mt-2"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- Fin tab balance hidrico-->
                    <!--Modal cambio de estado examen-->
                    <div class="modal" tabindex="-1" role="dialog" id="mdlCambioEstado">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header" id="mdlHeadCambioEstado">
                                    <h5 class="modal-title">Cambiando estado del examen</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" id="mdlBodyCambioEstado">
                                    <h5 id="TitleNombreExamen"></h5>
                                    <label>Seleccione estado</label>
                                    <select id="sltEstadosExamen" class="form-control" data-required="true">
                                        <option value="118">Examen solicitado</option>
                                        <option value="115">Examen tomado</option>
                                        <option value="116">Resultado pendiente</option>
                                        <option value="117">Informado a médico</option>
                                    </select>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" id="btnCambioEstado" onclick="modificarEstadoExamen(this)">Cambiar estado</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Foin modal cambio de estado examen-->
                </div>

                <!-- fin tab content-->
                <div class="row">
                    <div class="col-md text-right mt-3">
                        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                            <button id="btnCancelarHojaEnfermeria" class="btn btn-warning mr-2" type="button">Cancelar</button>
                            <button id="btnIngresoNuevaHojaEnfermeria" class="btn btn-primary" type="button">Ingresar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tipoHoja1" role="tabpanel" aria-labelledby="tipoHoja1">
                <div class="row">
                    <div class="col-md mt-3">
                        <span id="titleTipoHojaEnfermeria1"></span>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tipoHoja2" role="tabpanel" aria-labelledby="tipoHoja2">
                <div class="row">
                    <div class="col-md mt-3">
                        <span id="titleTipoHojaEnfermeria2"></span>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tipoHoja3" role="tabpanel" aria-labelledby="tipoHoja3">
                <div class="row">
                    <div class="col-md mt-3">
                        <span id="titleTipoHojaEnfermeria3"></span>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tipoHoja4" role="tabpanel" aria-labelledby="tipoHoja4">
                <div class="row">
                    <div class="col-md mt-3">
                        <span id="titleTipoHojaEnfermeria4"></span>
                    </div>
                </div>
            </div>
            <!-- MODAL HORARIOS PLAN CUIDADO MATRONERIA -->
            <div class="modal fade" id="mdlHorariosPlanCuidadoEnfermeria" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header modal-header bg-primary">
                            <h5 class="modal-title"><span id="tituloHorariosTipoCuidados"></span></h5>
                        </div>
                        <div class="modal-body">
                            <div id="formHorarios" class="row">
                            </div>
                            <div class="row">
                                <div class="col-md text-right mt-3">
                                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                        <button id="btnCancelarModalCuidadoEnfermeria" class="btn btn-outline-secondary mr-2" type="button">Cancelar</button>
                                        <button id="btnAgregarModalCuidadoEnfermeria" class="btn btn-primary" type="button">Aceptar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- MODAL ESTADO HORARIOS PLAN DE CUIDADOS -->
            <div class="modal" tabindex="-1" role="dialog" id="mdlEstadoHorariosPlanCuidadoEnfermeria">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="tituloEstadoHorariosTipoCuidados">Cambiando estado del examen</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5>Seleccione Estado:</h5>
                            <div class="form-check">
                                <input class="form-check-input" id="radioPendiente" type="radio" name="estadoHorario" value="119">
                                <label class="form-check-label" for="radioPendiente">
                                    Pendiente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" id="radioRealizado" type="radio" name="estadoHorario" value="120">
                                <label class="form-check-label" for="radioRealizado">
                                    Realizado
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" id="radioNoRealizado" type="radio" name="estadoHorario" value="121">
                                <label class="form-check-label" for="radioNoRealizado">
                                    No realizado
                                </label>
                            </div>
                            <div class="col-md mt-2">
                                <textarea class="form-control" id="txtObsHorario" rows="4" maxlength="300" placeholder="Observaciones de cierre..."></textarea>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btnCambiarEstadoModalCuidadoEnfermeria" onclick="">Cambiar estado</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tipoHoja5" role="tabpanel" aria-labelledby="tipoHoja5">
                <div class="row">
                    <div class="col-md mt-3">
                        <span id="titleTipoHojaEnfermeria5"></span>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tipoHoja6" role="tabpanel" aria-labelledby="tipoHoja6">
                <div class="row">
                    <div class="col-md mt-3">
                        <span id="titleTipoHojaEnfermeria6"></span>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tipoHoja7" role="tabpanel" aria-labelledby="tipoHoja7">
                <div class="row">
                    <div class="col-md mt-3">
                        <span id="titleTipoHojaEnfermeria7"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/NuevaHojaEnfermeria.js") + "?" + GetVersion() %>"></script>
</asp:Content>


