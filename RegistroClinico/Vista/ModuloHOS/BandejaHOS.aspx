﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="BandejaHOS.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.BandejaHOS" %>

<%@ Register Src="~/Vista/UserControls/ModalHojasEvolucion.ascx" TagName="HojaEvolucion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalHospitalizaciones.ascx" TagName="VerHospitalizaciones" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/bootstrap-clockpicker.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/BandejaHOS.js") + "?" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/Funciones/FuncionesApi.js") + "?" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/jquery-clockpicker.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/bootstrap-clockpicker.js") %>"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('.tabular.menu .item').tab();
            $('#btnHosExportar').click(function () {
                //var tblObj = $('#tblHospitalizacion').DataTable();
                __doPostBack("<%= btnHosExportarH.UniqueID %>", JSON.stringify(datosExportarHOS));
            });

            $('#btnIngExportar').click(function () {
                //var tblObj = $('#tblIngreso').DataTable();
                __doPostBack("<%= btnIngExportarH.UniqueID %>", JSON.stringify(datosExportarIM));
            });

            $('#lnkExportarMovIngreso').click(function () {
                var tblObj = $('#tblMovimientosIng').DataTable();
                __doPostBack("<%= btnExportarMovIngH.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });

            $('#lnkExportarMovHospitalizacion').click(function () {
                var tblObj = $('#tblMovHospitalizacion').DataTable();
                __doPostBack("<%= btnExportarMovHospitalizacionH.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });
        });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />

    <input type="hidden" id="idHos" value="0" />
    <input type="hidden" id="idTraslado" value="0" />
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-12 text-center">
                <h6>&nbsp;</h6>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <ul id="ulTablist" class="nav nav-tabs nav-justified md-tabs ml-0 mr-0" role="tablist" style="z-index: 0; display: none;">
                        <li id="liBandejaHospitalizacion" class="nav-item">
                            <a id="aBandejaHospitalizacion" class="nav-link active" data-toggle="tab" href="#divBandejaHospitalizacion" role="tab" aria-controls="home-just"
                                aria-selected="true">
                                <h3>Bandeja Hospitalización</h3>
                            </a>
                        </li>
                        <li id="liDatosIngreso" class="nav-item" style="display: none;">
                            <a id="aBandejaIngresoMedico" class="nav-link" data-toggle="tab" href="#divBandejaIngresoMedico" role="tab" aria-controls="contact-just"
                                aria-selected="false">
                                <h3>Bandeja Ingreso Médico</h3>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="divBandejaHospitalizacion" class="tab-pane fade show active" role="tabpanel"
                            aria-labelledby="home-tab-just">
                            <div class="card-body">
                                <button data-toggle="collapse" class="btn btn-outline-primary container-fluid"
                                    data-target="#divFiltroHos" onclick="return false;">
                                    FILTRAR BANDEJA
                                </button>
                                <div id="divFiltroHos" class="collapse card p-3">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label  for="sltTipoIdentificacion">Tipo de identificación</label>
                                                <select id="sltTipoIdentificacion" class="form-control">
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label id="lblTipoIdentificacion" for="txtHosFiltroRut"></label>
                                                <div class="input-group">
                                                    <input id="txtHosFiltroRut" type="text" class="form-control" style="width: 110px;" />
                                                    <div class="input-group-prepend digito">
                                                        <div class="input-group-text">-</div>
                                                    </div>

                                                    <input type="text" id="txtHosFiltroDV" class="form-control digito" style="width: 10px" disabled />
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <label for="txtHosFiltroNombre">Nombre</label>
                                                <input id="txtHosFiltroNombre" type="text" class="form-control" placeholder="Del Paciente" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="txtHosFiltroApe">Apellido paterno</label>
                                                <input id="txtHosFiltroApe" type="text" class="form-control" placeholder="Del Paciente" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="txtHosFiltroSApe">Apellido materno</label>
                                                <input id="txtHosFiltroSApe" type="text" class="form-control" placeholder="Del Paciente" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="ddlHosEstado">Estado</label>
                                                <select id="ddlHosEstado" class="form-control">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <a id="btnHosFiltro" class="btn btn-success"><i class="fa fa-search"></i>Buscar</a>
                                            <a id="btnHosLimpiarFiltro" class="btn btn-blue-grey"><i class="fa fa-eraser"></i>Limpiar Filtros</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-bottom: 20px; padding-top: 20px;">
                                    <div class="col-md-12 text-right">
                                        <asp:Button runat="server" ID="btnHosExportarH" OnClick="btnHosExportarH_Click" Style="display: none;" />
                                        <a id="btnHosExportar" class="btn btn-info waves-effect waves-light" href="#\">
                                            <i class="fa fa-file-excel"></i>&nbsp;&nbsp;Exportar Excel
                                        </a>
                                        <a id="btnNuevoHos" style="display: none;" class="btn btn-info waves-effect waves-light" href="NuevoFormularioAdmision.aspx?id=0">
                                            <i class="fa fa-plus"></i>&nbsp;&nbsp;Nuevo Ingreso Admisión
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table id="tblHospitalizacion" class="table table-striped table-bordered table-hover">
                                        </table>
                                    </div>
                                </div>
                                <br />
                                <div class="card" style="padding: 10px; background: #f0f0f0;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div>
                                                <span style="margin-left: 20px; height: 20px; width: 20px; margin-right: 10px; background-color: rgb(223, 240, 216); border-radius: 50%; display: inline-block; border: 1px solid black;"></span>
                                                <%--Hospitalización con egreso creado.--%>
                                                Hospitalización con epicrisis.<%--?--%>
                                            </div>
                                            <div>
                                                <h5 style="display: inline-block;"><span class="badge badge-pill btn-danger badge-count">&nbsp;A&nbsp;</span></h5>
                                                Hospitalización con aislamiento.
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div>
                                                <h5 style="display: inline-block;"><span class="badge badge-pill btn-secondary badge-count">&nbsp;P&nbsp;</span></h5>
                                                Hospitalización pre-ingresada
                                            </div>

                                            <div>
                                                <h5 style="display: inline-block;"><span class="badge badge-pill btn-success badge-count">&nbsp;I&nbsp;</span></h5>
                                                Hospitalización ingresada
                                            </div>
                                            <div>
                                                <h5 style="display: inline-block;"><span class="badge badge-pill btn-warning badge-count">&nbsp;E&nbsp;</span></h5>
                                                Hospitalización egresada
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="divBandejaIngresoMedico" class="tab-pane fade" role="tabpanel" aria-labelledby="contact-tab-just" style="display: none;">
                            <div class="card-body">
                                <button data-toggle="collapse" class="btn btn-outline-primary container-fluid"
                                    data-target="#divFiltroIngreso" onclick="return false;">
                                    FILTRAR BANDEJA
                                </button>
                                <div id="divFiltroIngreso" class="collapse card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="txtIngFiltroRut">Rut</label>
                                                <input id="txtIngFiltroRut" type="text" placeholder="Del Paciente"
                                                    class="form-control" />
                                            </div>
                                            <div class="col-md-3">
                                                <label for="txtIngFiltroNombre">Nombre</label>
                                                <input id="txtIngFiltroNombre" type="text" placeholder="Del Paciente"
                                                    class="form-control" />
                                            </div>
                                            <div class="col-md-3">
                                                <label for="txtIngFiltroApe">Apellido Paterno</label>
                                                <input id="txtIngFiltroApe" type="text" placeholder="Del Paciente"
                                                    class="form-control" />
                                            </div>
                                            <div class="col-md-3">
                                                <label for="txtIngFiltroSApe">Apellido Materno</label>
                                                <input id="txtIngFiltroSApe" type="text" placeholder="Del Paciente"
                                                    class="form-control" />
                                            </div>
                                        </div>
                                        <div class="row mt-1">
                                            <div class="col-md-3">
                                                <label for="ddlUbicacion">Ubicación</label>
                                                <select id="ddlUbicacion" class="form-control">
                                                </select>
                                            </div>
                                            <div class="col-md-3" id="divProfesional" style="display: none;">
                                                <label class="active">Profesional Médico</label>
                                                <div style="height: 10px;">&nbsp;</div>
                                                <select id="ddlMedicoIngresoMedico" class="form-control">
                                                </select>
                                            </div>
                                        </div>

                                        <div class="text-right">
                                            <a id="btnIngFiltro" class="btn btn-success"><i class="fa fa-search"></i>Buscar</a>
                                            <a id="btnIngLimpiarFiltro" class="btn btn-blue-grey"><i class="fa fa-eraser"></i>Limpiar Filtros</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="padding-bottom: 20px; padding-top: 20px;">
                                    <div class="col-md-12 text-right">
                                        <asp:Button ID="btnIngExportarH" runat="server" OnClick="btnIngExportarH_Click" Style="display: none;" />
                                        <a id="btnIngExportar" class="btn btn-info waves-effect waves-light" href="#\">
                                            <i class="fa fa-file-excel"></i>&nbsp;&nbsp;Exportar Excel
                                        </a>
                                        <a id="btnNuevoIng" style="display: none;" class="btn btn-info waves-effect waves-light" href="NuevoIngresoMedico.aspx?id=0">
                                            <i class="fa fa-plus"></i>&nbsp;&nbsp;Nuevo Ingreso
                                        </a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="tblIngreso" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="mdlAislamiento" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="width: 1000px; min-width: 1000px;">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Aislamientos del paciente</p>
                </div>
                <div class="modal-body">
                    <div class="card" style="padding: 20px;">
                        <div id="divNuevoAislamiento" style="display: none;">
                            Nuevo Aislamiento: 
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="md-form">
                                        <label class="active">Motivo aislamiento</label>
                                        <select id="ddlTipoAislamiento" class="form-control" data-required="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="md-form">
                                        <label class="active">Inicio aislamiento</label>
                                        <input id="txtFechaInicioAisl" type="date" class="form-control" data-required="true" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="md-form">
                                        <label class="active">Fin aislamiento</label>
                                        <input id="txtFechaFinAisl" type="date" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="md-form">
                                        <button id="btnNuevoAislamiento" class="btn btn-info">Agregar Aislamiento</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        Aislamientos:
                        <table id="tblAislamientoPaciente" class="table table-bordered table-hover">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mdlTraslados" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info pb-1">
                    <h4><b>Traslados del Paciente</b></h4>
                </div>
                <div class="modal-body">
                    <div id="divTraslado">
                        <div id="divNuevoTraslado">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Nombre de paciente:</label>
                                    <input id="txtPacienteTraslado" class="form-control" disabled="disabled" />
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-2">
                                    <label>Cama actual:</label>
                                    <input id="txtCamaActualTraslado" class="form-control" disabled="disabled" />
                                </div>
                                <div class="col-md-2" id="dvSwitch">
                                    <center>
                                        <label>¿S. Externo?</label>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" id="swTrasladoExterno" class="custom-control-input form-control">
                                            <label for="swTrasladoExterno" class="custom-control-label">No/Si</label>
                                        </div>
                                    </center>
                                </div>
                                <div class="col-md-4 external-input">
                                    <label id="lblServicioDestinoTraslado">Servicio destino</label>
                                    <select id="ddlServicioDestino" class="form-control" data-required="true">
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label id="lblCamaDestinoTraslado">Cama destino</label>
                                    <select id="ddlCamaDestino" class="form-control" data-required="true">
                                    </select>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-lg-9 col-md-6"></div>
                                <div class="col-lg-3 col-md-6 text-center">
                                    <a id="aTraslado" class="btn btn-info mt-3">
                                        <i class="fa fa-plus"></i>Agregar Traslado
                                    </a>
                                </div>
                            </div>
                        </div>
                        <hr />

                        <h5>Listado de Traslados:</h5>

                        <table id="tblTrasladosPaciente" class="table table-bordered table-hover" style="width: 100%">
                        </table>

                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning" onclick="$('#mdlTraslados').modal('hide'); return false;">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="mdlCamaOrigen" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead mb-0">
                        <strong><i class='fa fa-bed nav-icon mr-2'></i>Asignar primera cama del paciente</strong>
                    </p>
                </div>
                <div class="modal-body">
                    <p>
                        <span class="mr-1">Paciente:</span>
                        <b><span id="nomPacienteCamaOrigen" class="mr-1"></span></b>
                    </p>
                    <p>
                        <span class="mr-1">N° Hospitalizacion:</span>
                        <b><span id="numHospitalizacionCamaOrigen"></span></b>
                    </p>
                    <div id="divCamaOrigen" class="card" style="padding: 20px;">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="ddlServicioOrigen">Servicio</label>
                                <select id="ddlServicioOrigen" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="ddlCamaOrigenNuevo">Cama</label>
                                <select id="ddlCamaOrigenNuevo" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="txtAntecedentesMorbidos" class="mt-3">Antecedentes mórbidos</label>
                                <textarea id="txtAntecedentesMorbidos" class="form-control" rows="3" maxlength="3000" data-required="true"></textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="txtMotivoConsultaPaciente" class="mt-3">Motivo de consulta del paciente</label>
                                <textarea id="txtMotivoConsultaPaciente" class="form-control" rows="3" maxlength="3000" data-required="true"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="txtDiagnosticoHospitalizacion" class="mt-3">Diagnóstico</label>
                                <textarea id="txtDiagnosticoHospitalizacion" class="form-control" rows="3" maxlength="3000" data-required="true"></textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="txtCondicionActualPaciente" class="mt-3">Condición actual del paciente</label>
                                <textarea id="txtCondicionActualPaciente" class="form-control" rows="3" maxlength="3000" data-required="true"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnExamenesLab" onclick="return false;" class="btn btn-primary">
                        <i class='fa fa-bed nav-icon mr-2'></i>Exámenes de Laboratorio
                    </button>
                    <button id="btnOrigen" onclick="return false;" class="btn btn-info">
                        <i class='fa fa-file-text nav-icon mr-2'></i>Asignar cama origen
                    </button>
                    <button class="btn btn-warning" onclick="$('#mdlCamaOrigen').modal('hide'); return false;">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdlAlerta" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info center-horizontal" style="justify-content: center">
                    <div class="center-horizontal">Alerta de sistema</div>
                </div>
                <div class="modal-body">
                    <div id="mdlAlertaCuerpo" class="row center-horizontal">
                    </div>
                    <div class="row" style="padding-top: 5px">
                        <hr />
                    </div>
                    <div id="mdlAlertaBoton" class="row center-horizontal">
                        <div class='col-sm-2'><a id='linkConfirmar' class='btn btn-block' href='#/'>Confirmar </a></div>
                        <div class='col-sm-2'><a id='linkCancelar' class='btn btn-warning btn-block' href='#/' onclick="$('#mdlAlerta').modal('hide');">Cancelar </a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdlMovimientosIngresoMedico" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">

                <div id="divCarouselIngresoMedico" class="carousel slide" data-ride="carousel" data-interval="false">
                    <div class="carousel-inner">
                        <div class="modal-header modal-header-info">
                            <p class="heading lead mb-0">Ingreso Médico</p>
                            <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                                <span aria-hidden="true" class="white-text">x</span>
                            </button>
                        </div>
                        <div class="carousel-item active">
                            <div class="modal-body">

                                <h3 class="text-center mt-2">Ingresos Médicos</h3>

                                <div class="text-right mb-2">
                                    <a id="aIngresoMedico" class="btn btn-info" onclick="LinkCrearNuevoIngresoMedico();">
                                        <i class="fa fa-plus"></i>Nuevo Ingreso Médico 
                                    </a>
                                </div>
                                <div class="table-responsive">
                                    <table id="tblIngresoMedico" class="table table-bordered table-hover" style="font-size: smaller">
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="modal-body">

                                <h3 class="text-center mt-2">Movimientos ingreso médico</h3>

                                <div class="row mb-2">
                                    <asp:Button runat="server" ID="btnExportarMovIngH" OnClick="btnExportarMovIngH_Click" Style="display: none;" />
                                    <a id="lnkExportarMovIngreso" class="btn btn-info btn-sm ml-auto" href="#\">
                                        <i class="fa fa-file-excel-o" style="font-size: 18px;"></i>Exportar a Excel
                                    </a>
                                </div>

                                <table id="tblMovimientosIng" class="table table-bordered table-sm table-hover" style="font-size: smaller">
                                </table>

                            </div>
                            <div class="modal-footer">
                                <a id="aVolverCarouselIngresoMedico" class="btn btn-secondary" onclick="$('#divCarouselIngresoMedico').carousel(0);"><i class="fa fa-arrow-left"></i>Volver</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="mdlMovimientosHospitalizacion" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <div class="row w-100">
                        <div class="col-md-6 col-lg-9">
                            <h4>Movimientos de hospitalización  #<span id="idHosModalMovimientos"></span></h4>
                            <asp:Button runat="server" ID="btnExportarMovHospitalizacionH" OnClick="btnExportarMovHospitalizacionH_Click" Style="display: none;" />
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <button id="lnkExportarMovHospitalizacion" class="btn btn-info form-control btn-exportar">
                                <i class="fa fa-file-excel-o" style="font-size: 18px;"></i>Exportar a excel 
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <h6 id="nomPacModalMovimientos"></h6>
                    <hr />
                    <br />
                    <table id="tblMovHospitalizacion" class="table table-bordered table-sm table-hover" style="width: 100%;"></table>

                </div>
            </div>
        </div>
    </div>
    <!-- MODAL HOJA ENFERMERIA -->
    <div class="modal fade" id="mdlVerHojaEnfermeria" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <div class="row w-100">
                        <div class="col-md-6 col-lg-9">
                            <h4 class="mt-3">Hoja de Enfermería / Matronería</h4>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <a id="btnNuevaHojaEnfermeria" class="btn btn-primary form-control mt-2">
                                <i class="far fa-file mr-1"></i>Nuevo
                            </a>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <h6>Paciente: <b><span id="nomPacModalHojaEnfermeria"></span></b></h6>
                    <h6>Cama: <b><span id="numCamaModalHojaEnfermeria" class="mr-1"></span></b>N° Hospitalizacion: <b><span id="numHospitalizacion"></span></b></h6>
                    <hr />
                    <br />
                    <div id="ocultarTablaHojaEnfermeriaPorHospitalizacion">
                        <table class="table table-bordered table-hover" style="width: 100%;" id="tblHojaEnfermeriaPorHospitalizacion">
                        </table>
                    </div>
                    <span id="alertHojaEnfermeriaPorHospitalizacion"></span>
                </div>
            </div>
        </div>
    </div>
    <!--MODAL IMPRIMIR-->
    <div class="modal modal-print fade" id="mdlImprimir" role="dialog" aria-labelledby="mdlImprimir" aria-hidden="true" tabindex="0">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i>Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameImpresion" frameborder="0" onload="$('#modalCargando').hide();"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade right" id="mdlFormularios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
        <div class="modal-dialog modal-side modal-top-right modal-notify" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <p class="heading lead">Formularios</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="center-horizontal">
                        <div class="btn-group-vertical" role="group" aria-label="Vertical button group">

                            <button id="btnNuevoIPD" type="button" class="btn btn-primary ml-0" onclick="BtnNuevoFormRCE(this);"
                                data-href="Vista/ModuloIPD/NuevoIPD.aspx">
                                <i class="fa fa-plus text-white"></i>IPD
                            </button>
                            <button id="btnNuevoIC" type="button" class="btn btn-primary" onclick="BtnNuevoFormRCE(this);"
                                data-href="Vista/ModuloIC/NuevoIC.aspx">
                                <i class="fa fa-plus text-white"></i>Interconsulta
                            </button>
                            <button id="btnNuevoEXA" type="button" class="btn btn-primary" disabled>
                                <i class="fa fa-plus text-white"></i>Exámenes
                            </button>
                            <button id="btnNuevoFQX" type="button" class="btn btn-primary" onclick="BtnCargarNuevoForm(this);">
                                <i class="fa fa-plus text-white"></i>Formulario Ind Qx
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <wuc:HojaEvolucion ID="mdlHojaEvolucion" runat="server" />
    <wuc:VerHospitalizaciones ID="mdlVerHospitalizaciones" runat="server" />
</asp:Content>
