﻿using Newtonsoft.Json;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;

namespace RegistroClinico.Vista.ModuloHOS
{
    public partial class ImprimirHojaEnfermeria : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            // Valida sesión según los permisos que tenga el perfil
            NegSession negSession = new NegSession(Page);
            negSession.ValidarPermisosSession();
            string idDAU = Request.QueryString["id"];
            string usuarioLogeado = Session["LOGIN_USUARIO"].ToString();


            try
            {
                HttpContext context = HttpContext.Current;
                string dominio = Funciones.GetUrlWebApi();

                idDAU = Request.QueryString["id"];
                usuarioLogeado = Session["LOGIN_USUARIO"].ToString();
                string url = string.Format("{0}HOS_Hoja_Enfermeria/{1}", dominio, idDAU);
                string token = string.Format("Bearer {0}", Session["TOKEN"]/*Page.Request.Cookies["DATA-TOKEN"].Value*/);
                string sJson = Funciones.ObtenerJsonWebApi(url, token);
                System.Diagnostics.Debug.WriteLine(sJson);
                RootHos datosHos = JsonConvert.DeserializeObject<RootHos>(sJson);

                DibujaDatos(datosHos);

            } catch (Exception ex) {
                NegLog.GuardarLog("Excepción Producida al Imprimir HENF->", ex.Message +" "+ex.StackTrace );
            }
            Funciones funciones = new Funciones();
            string doc = "Hoja de Enfermería";
            funciones.ExportarPDFWK(this.Page, doc, idDAU, usuarioLogeado);
        }

        public string GetHost()
        {
            return Funciones.GetHost(HttpContext.Current);
        }
        public void DibujaDatos(RootHos datos)
        {
            Dictionary<string, object> edad = JsonConvert.DeserializeObject<Dictionary<string, object>>(Funciones.CalculaEdad(((DateTime)datos.Paciente.FechaNacimiento).ToString("yyyy-MM-dd")));

            identificacionLbl.Text = datos.Paciente.DescripcionIdentificacion;
            rutLbl.Text = datos.Paciente.NumeroDocumento + "-" + datos.Paciente.Digito;
            nombreLbl.Text = datos.Paciente.Nombre + " " + datos.Paciente.ApellidoPaterno + " " + datos.Paciente.ApellidoMaterno;

            sexoLbl.Text = datos.Paciente.NombreSexo;
            generoLbl.Text = datos.Paciente.NombreGenero;
            telefonoLbl.Text = datos.Paciente.Telefono;
            otroTelefonoLbl.Text = datos.Paciente.OtrosFonos;

            fechaNacimientoLbl.Text = ((DateTime)datos.Paciente.FechaNacimiento).ToString("dd-MM-yyyy");
            edadLbl.Text = edad["años"].ToString();
            direccionLbl.Text = datos.Paciente.DireccionCalle;
            direccionNumeroLbl.Text = datos.Paciente.DireccionNumero;


            numHospitalizacionLbl.Text = datos.IdHospitalizacion.ToString();
            hgFecha.Text = ((DateTime)datos.Fecha).ToString("dd-MM-yyyy HH:mm:ss");
            hgTurno.Text = datos.Turno.Valor;

            diagnosticoLbl.Text = datos.Diagnostico;
            medicamentosLbl.Text = datos.MedicamentosHabituales;
            morbidosLbl.Text = datos.Morbidos;
            alergiaLbl.Text = datos.Alergias;

            /*Plan de Cuidado*/

            StringBuilder html = new StringBuilder();
            html.AppendLine("<table class='tabla-custom'>");
            html.AppendLine("<tr>");
            html.AppendLine("<th> Actividades </th>");
            html.AppendLine("<th> Horario </th>");
            html.AppendLine("<tr>");
            int index = 1;
            int tipoExamen = 0;
            int tipoExamenOrigen = 0;
            foreach (var tCuidado in datos.TipoCuidado)
            {
                tipoExamen = tCuidado.Id;

                if (tipoExamenOrigen == 0)
                { //Si se inicia 
                    tipoExamenOrigen = tipoExamen;
                    html.AppendLine("<tr>");
                    html.AppendLine("<td> " + tCuidado.Descripcion + "</td>");
                    html.AppendLine("<td>");
                }

                if (tipoExamen == tipoExamenOrigen) //Si es el mismo tipo de examen se agrega el horario
                {
                    if (tCuidado.IdEstado == 119)
                    {
                        html.AppendLine("P");//Pendiente
                    }
                    else if (tCuidado.IdEstado == 120)
                    {
                        html.AppendLine("R");//Realizado
                    }
                    else
                    {
                        html.AppendLine("N");//No Realizado
                    }
                    html.AppendLine(tCuidado.Horario + "/");
                }
                else
                {
                    html.AppendLine("</td>");
                    html.AppendLine("</tr>");

                    html.AppendLine("<tr>");//Nueva Fila
                    html.AppendLine("<td> " + tCuidado.Descripcion + "</td>");
                    html.AppendLine("<td>");
                    tipoExamenOrigen = tCuidado.Id;
                    if (tCuidado.IdEstado == 119) //Agrega inicial de estado
                    {
                        html.AppendLine("P"); //Pendiente
                    }
                    else if (tCuidado.IdEstado == 120)
                    {
                        html.AppendLine("R"); //Realizado
                    }
                    else
                    {
                        html.AppendLine("N"); //No realizado
                    }
                    html.AppendLine(tCuidado.Horario + "/");

                }

            }
            html.AppendLine("</table>");
            planCuidado.Text = html.ToString();

            /*Signos Vitales*/
            StringBuilder htmlSV = new StringBuilder();
            htmlSV.AppendLine("<table class='tabla-custom'>");
            htmlSV.AppendLine("<tr>");
            htmlSV.AppendLine("<th colspan='2'> Fecha </th>");
            htmlSV.AppendLine("<th colspan='10'> Signos Vitales </th>");
            htmlSV.AppendLine("<tr>");
            index = 1;
            string fechaSV = "";
            string fechaSVOrigen = "";
            foreach (var sVitales in datos.SignosVitales)
            {
                fechaSV = sVitales.Fecha.ToString();

                if (fechaSVOrigen == "")
                { //Si se inicia 
                    fechaSVOrigen = fechaSV;
                    htmlSV.AppendLine("<tr>");
                    htmlSV.AppendLine("<td colspan='2'> " + sVitales.Fecha + "</td>");
                    htmlSV.AppendLine("<td colspan=10'>");
                }

                if (fechaSVOrigen == fechaSV) //Si la fecha es igual, agrega el SV
                {
                    htmlSV.AppendLine( sVitales.Descripcion + ":" + sVitales.Valor + "/");
                }
                else
                {
                    htmlSV.AppendLine("</td>");
                    htmlSV.AppendLine("</tr>");

                    htmlSV.AppendLine("<tr>");//Nueva Fila
                    htmlSV.AppendLine("<td colspan='2'> " + sVitales.Fecha + " </td>");
                    htmlSV.AppendLine("<td colspan='10'>");
                    fechaSVOrigen = sVitales.Fecha.ToString();
                }
            }
            htmlSV.AppendLine("</table>");
            signosVitalesLbl.Text = htmlSV.ToString();

            /*Examenes Laboratorio*/
            StringBuilder htmlEL = new StringBuilder();
            htmlEL.AppendLine("<table class='tabla-custom'>");
            htmlEL.AppendLine("<tr>");
            htmlEL.AppendLine("<th> Nombre Examen </th>");
            htmlEL.AppendLine("<th> Solicitud </th>");
            htmlEL.AppendLine("<th> Estado </th>");
            htmlEL.AppendLine("<th> Cierre Solicitud </th>");
            htmlEL.AppendLine("</tr>");

            foreach (var eLab in datos.Examenes)
            {
                htmlEL.AppendLine("<tr>");
                htmlEL.AppendLine("<td>" + eLab.DescripcionExamen + "</td>");
                htmlEL.AppendLine("<td>" + eLab.FechaSolicitud + "/" + eLab.ProfesionalSolicitud + "</td>");
                htmlEL.AppendLine("<td>" + eLab.DescripcionEstado + "</td>");
                htmlEL.AppendLine("<td>" + eLab.FechaTermino + "/" + eLab.ProfesionalTermina + "</td>");
                htmlEL.AppendLine("</tr>");
            }
            htmlEL.AppendLine("</table>");
            examenesLabLbl.Text = htmlEL.ToString();


            /*Evoluciones*/
            //Registro de peso
            StringBuilder htmlPeso = new StringBuilder();
            htmlPeso.AppendLine("<table class='tabla-custom width='30%'>");
            htmlPeso.AppendLine("<tr>");
            htmlPeso.AppendLine("<th> Fecha </th>");
            htmlPeso.AppendLine("<th> Descripción </th>");
            htmlPeso.AppendLine("</tr>");

            foreach (var ePeso in datos.TipoPeso)
            {
                htmlPeso.AppendLine("<tr>");
                htmlPeso.AppendLine("<td>" + ePeso.Fecha + "</td>");
                htmlPeso.AppendLine("<td>" + ePeso.Peso + " " + ePeso.DescripcionPeso + "</td>");
                htmlPeso.AppendLine("</tr>");
            }
            htmlPeso.AppendLine("</table>");
            evolucionesPesoLbl.Text = htmlPeso.ToString();
            //Registro de Talla
            StringBuilder htmlTalla = new StringBuilder();
            htmlTalla.AppendLine("<table class='tabla-custom width='30%'>");
            htmlTalla.AppendLine("<tr>");
            htmlTalla.AppendLine("<th> Fecha </th>");
            htmlTalla.AppendLine("<th> Descripción </th>");
            htmlTalla.AppendLine("</tr>");

            foreach (var eTalla in datos.TipoTalla)
            {
                htmlTalla.AppendLine("<tr>");
                htmlTalla.AppendLine("<td>" + eTalla.Fecha + "</td>");
                htmlTalla.AppendLine("<td>" + eTalla.Talla + " " + eTalla.DescripcionTalla + "</td>");
                htmlTalla.AppendLine("</tr>");
            }
            htmlTalla.AppendLine("</table>");
            evolucionesTallaLbl.Text = htmlTalla.ToString();

            //Registro de Evolucion Paciente
            StringBuilder htmlPaciente = new StringBuilder();
            htmlPaciente.AppendLine("<table class='tabla-custom width='30%'>");
            htmlPaciente.AppendLine("<tr>");
            htmlPaciente.AppendLine("<th> Fecha </th>");
            htmlPaciente.AppendLine("<th> Descripción </th>");
            htmlPaciente.AppendLine("</tr>");

            foreach (var eEvolucion in datos.Evolucion)
            {
                htmlPaciente.AppendLine("<tr>");
                htmlPaciente.AppendLine("<td>" + eEvolucion.Fecha + "|" + eEvolucion.NombreProfesional + "</td>");
                htmlPaciente.AppendLine("<td>" + eEvolucion.Descripcion + "</td>");
                htmlPaciente.AppendLine("</tr>");
            }
            htmlPaciente.AppendLine("</table>");
            evolucionPacienteLbl.Text = htmlPaciente.ToString();


            /*Invasivos*/
            StringBuilder htmlInvasivos = new StringBuilder();
            htmlInvasivos.AppendLine("<table class='tabla-custom width='30%'>");
            htmlInvasivos.AppendLine("<tr>");
            htmlInvasivos.AppendLine("<th> Invasivo </th>");
            htmlInvasivos.AppendLine("<th> Extremidad </th>");
            htmlInvasivos.AppendLine("<th> Solicitud </th>");
            htmlInvasivos.AppendLine("<th> Retiro </th>");
            htmlInvasivos.AppendLine("<th> Observaciones </th>");
            htmlInvasivos.AppendLine("</tr>");

            foreach (var invasivos in datos.TipoInvasivo)
            {
                htmlInvasivos.AppendLine("<tr>");
                htmlInvasivos.AppendLine("<td>" + invasivos.DescripcionTipoInvasivo + "</td>");
                htmlInvasivos.AppendLine("<td>" + invasivos.DescripcionExtremidad + " " + invasivos.DescripcionPlano + "</td>");
                htmlInvasivos.AppendLine("<td>" + invasivos.FechaCreacion + "|" + invasivos.NombreProfesionalCrea + "</td>");
                htmlInvasivos.AppendLine("<td>" + invasivos.FechaCierre + "|" + invasivos.NombreProfesionalCierre + "</td>");
                htmlInvasivos.AppendLine("<td>" + invasivos.Observacion + "</td>");
                htmlInvasivos.AppendLine("</tr>");
            }
            htmlInvasivos.AppendLine("</table>");
            invasivosLbl.Text = htmlInvasivos.ToString();


            /*Balance hidrico*/
            StringBuilder htmbalIng = new StringBuilder();
            StringBuilder htmbalEgr = new StringBuilder();

            htmbalIng.AppendLine("<table class='tabla-custom width='30%'>");
            htmbalIng.AppendLine("<tr>");
            htmbalIng.AppendLine("<th> Nombre </th>");
            htmbalIng.AppendLine("<th> Fecha </th>");
            htmbalIng.AppendLine("<th> Valor </th>");
            htmbalIng.AppendLine("<th> Profesional </th>");
            htmbalIng.AppendLine("</tr>");

            htmbalEgr.AppendLine("<table class='tabla-custom width='30%'>");
            htmbalEgr.AppendLine("<tr>");
            htmbalEgr.AppendLine("<th> Nombre </th>");
            htmbalEgr.AppendLine("<th> Fecha </th>");
            htmbalEgr.AppendLine("<th> Valor </th>");
            htmbalEgr.AppendLine("<th> Profesional </th>");
            htmbalEgr.AppendLine("</tr>");

            foreach (var balanceIngreso in datos.TipoBalance)
            {
                if (balanceIngreso.Tipo == "INGRESO")
                { //Igreso
                    htmbalIng.AppendLine("<tr>");
                    htmbalIng.AppendLine("<td>" + balanceIngreso.DescripcionBalance + "</td>");
                    htmbalIng.AppendLine("<td>" + balanceIngreso.Fecha + "</td>");
                    htmbalIng.AppendLine("<td>" + balanceIngreso.Balance + "</td>");
                    htmbalIng.AppendLine("<td>" + balanceIngreso.Profesional + "</td>");
                    htmbalIng.AppendLine("</tr>");
                }
                else
                {
                    htmbalEgr.AppendLine("<tr>");
                    htmbalEgr.AppendLine("<td>" + balanceIngreso.DescripcionBalance + "</td>");
                    htmbalEgr.AppendLine("<td>" + balanceIngreso.Fecha + "</td>");
                    htmbalEgr.AppendLine("<td>" + balanceIngreso.Balance + "</td>");
                    htmbalEgr.AppendLine("<td>" + balanceIngreso.Profesional + "</td>");
                    htmbalEgr.AppendLine("</tr>");
                }
            }
            htmbalIng.AppendLine("</table>");
            balanceIngresoLbl.Text = htmbalIng.ToString();
            htmbalEgr.AppendLine("</table>");
            balanceEgresoLbl.Text = htmbalEgr.ToString();

            /*Riesgos*/

            //Obtener ultimo ID
            DateTime fechaAlmacenada = new DateTime();
            int idRiesgo = 0;
            foreach (var riesgos in datos.RespuestasItem)
            {
                if (riesgos.IdItem == 1) //Riesgo
                {
                    if (idRiesgo == 0)
                    { //Inicial
                        idRiesgo = riesgos.IdHojaEnfermeriaRespuesta;
                        fechaAlmacenada = riesgos.Fecha;
                    }
                    if (fechaAlmacenada < riesgos.Fecha)
                    {
                        idRiesgo = riesgos.IdHojaEnfermeriaRespuesta;
                        fechaAlmacenada = riesgos.Fecha;
                    }
                }
            }
            //Buscar en BD
            try
            {
                HttpContext context = HttpContext.Current;
                string dominio = Funciones.GetUrlWebApi();
                string usuarioLogeado = Session["LOGIN_USUARIO"].ToString();
                string url = string.Format("{0}HOS_Hoja_Enfermeria_Item/{1}/RespuestaDetalle", dominio, idRiesgo);
                string token = string.Format("Bearer {0}", Session["TOKEN"]);
                string sJson = Funciones.ObtenerJsonWebApi(url, token);
                System.Diagnostics.Debug.WriteLine(sJson);

                List<Item> riesgos = JsonConvert.DeserializeObject<List<Item>>(sJson);

                StringBuilder htmlRiesgos = new StringBuilder();

                htmlRiesgos.AppendLine("<table class='tabla-custom width='30%'>");

                foreach (var riesgosA in riesgos) {
                    htmlRiesgos.AppendLine("<tr>");
                    htmlRiesgos.AppendLine("<td colspan='12'> <b>" + riesgosA.DescripcionItem + "</b></td>");
                    htmlRiesgos.AppendLine("</tr>");


                    foreach (var riesgosPreguntas in riesgosA.Preguntas)
                    {
                        htmlRiesgos.AppendLine("<tr>");

                        htmlRiesgos.AppendLine("<td colspan='6'>");
                        htmlRiesgos.AppendLine(riesgosPreguntas.DescripcionPregunta);
                        htmlRiesgos.AppendLine("</td>");

                        if (riesgosPreguntas.Alternativas.Count == 1) {
                            htmlRiesgos.AppendLine("<td colspan='6'>");
                            htmlRiesgos.AppendLine(riesgosPreguntas.Alternativas[0].DescripcionAlternativa);
                            htmlRiesgos.AppendLine("</td>");
                        }
                        else
                        {
                            htmlRiesgos.AppendLine("<td colspan='6'>");
                            foreach (var riesgosAlternativas in riesgosPreguntas.Alternativas)
                            {
                                htmlRiesgos.AppendLine(riesgosAlternativas.DescripcionAlternativa);
                            }
                            htmlRiesgos.AppendLine("</td>");
                        }

                        htmlRiesgos.AppendLine("</tr>");

                    }
                }

                htmlRiesgos.AppendLine("</table>");
                riesgosLbl.Text = htmlRiesgos.ToString();

            } catch (Exception ex)
            {
                NegLog.GuardarLog("Excepción Producida al Imprimir HENF->", ex.Message + " " + ex.StackTrace);
            }


            /*Valoraciones*/

            //Obtener ultimo ID
            fechaAlmacenada = new DateTime();
            int idValoraciones = 0;
            foreach (var valoraciones in datos.RespuestasItem)
            {
                if (valoraciones.IdItem == 10) //Valoraciones
                {
                    if (idValoraciones == 0)
                    { //Inicial
                        idValoraciones = valoraciones.IdHojaEnfermeriaRespuesta;
                        fechaAlmacenada = valoraciones.Fecha;
                    }
                    if (fechaAlmacenada < valoraciones.Fecha)
                    {
                        idValoraciones = valoraciones.IdHojaEnfermeriaRespuesta;
                        fechaAlmacenada = valoraciones.Fecha;
                    }
                }
            }


            //Buscar en BD
            try
            {
                HttpContext context = HttpContext.Current;
                string dominio = Funciones.GetUrlWebApi();
                string usuarioLogeado = Session["LOGIN_USUARIO"].ToString();
                string url = string.Format("{0}HOS_Hoja_Enfermeria_Item/{1}/RespuestaDetalle", dominio, idValoraciones);
                string token = string.Format("Bearer {0}", Session["TOKEN"]);
                string sJson = Funciones.ObtenerJsonWebApi(url, token);

                List<Item> valJson = JsonConvert.DeserializeObject<List<Item>>(sJson);

                StringBuilder htmlValoraciones = new StringBuilder();

                htmlValoraciones.AppendLine("<table class='tabla-custom width='30%'>");

                foreach (var valoracionA in valJson)
                {
                    htmlValoraciones.AppendLine("<tr>");
                    htmlValoraciones.AppendLine("<td colspan='12'> <b>" + valoracionA.DescripcionItem + "</b></td>");
                    htmlValoraciones.AppendLine("</tr>");


                    foreach (var valoracionPreguntas in valoracionA.Preguntas)
                    {
                        htmlValoraciones.AppendLine("<tr>");

                        htmlValoraciones.AppendLine("<td colspan='6'>");
                        htmlValoraciones.AppendLine(valoracionPreguntas.DescripcionPregunta);
                        htmlValoraciones.AppendLine("</td>");

                        if (valoracionPreguntas.Alternativas.Count == 1)
                        {
                            htmlValoraciones.AppendLine("<td colspan='6'>");
                            htmlValoraciones.AppendLine(valoracionPreguntas.Alternativas[0].DescripcionAlternativa);
                            htmlValoraciones.AppendLine("</td>");
                        }
                        else
                        {
                            htmlValoraciones.AppendLine("<td colspan='6'>");
                            foreach (var riesgosAlternativas in valoracionPreguntas.Alternativas)
                            {
                                htmlValoraciones.AppendLine(riesgosAlternativas.DescripcionAlternativa);
                            }
                            htmlValoraciones.AppendLine("</td>");
                        }

                        htmlValoraciones.AppendLine("</tr>");

                    }
                }

                htmlValoraciones.AppendLine("</table>");
                valoracionesLbl.Text = htmlValoraciones.ToString();

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Excepción Producida al Imprimir HENF->", ex.Message + " " + ex.StackTrace);
            }





        }

    }

    //Clases Hoja Enfermeria
    public class RootHos
    {
        public int IdHospitalizacion { get; set; }
        public DateTime FechaHospitalizacion { get; set; }
        public DateTime Fecha { get; set; }
        public string Diagnostico { get; set; }
        public string Morbidos { get; set; }
        public string Alergias { get; set; }
        public string MedicamentosHabituales { get; set; }
        public Paciente Paciente { get; set; }
        //public object OtrosProcedimientos { get; set; }
        //public Profesional Profesional { get; set; }
        public Turno Turno { get; set; }
        //public Responsable Responsable { get; set; }
        //public TipoResponsable TipoResponsable { get; set; }
        //public Extremidad Extremidad { get; set; }
        //public Plano Plano { get; set; }
        //public TipoHoja TipoHoja { get; set; }
        public List<SignosVitale> SignosVitales { get; set; }
        public List<TipoCuidado> TipoCuidado { get; set; }
        public List<TipoInvasivo> TipoInvasivo { get; set; }
        public List<TipoPeso> TipoPeso { get; set; }
        public List<TipoTalla> TipoTalla { get; set; }
        public List<TipoBalance> TipoBalance { get; set; }
        public List<Evolucion> Evolucion { get; set; }
        public List<Examene> Examenes { get; set; }
        public List<RespuestasItem> RespuestasItem { get; set; }
    }

    public class Paciente
    {
        public int IdPaciente { get; set; }
        public int Nui { get; set; }
        public string NombreNacionalidad { get; set; }
        public object NivelEducacional { get; set; }
        public string NombreSocial { get; set; }
        public object IdPersona { get; set; }
        public int IdIdentificacion { get; set; }
        public string DescripcionIdentificacion { get; set; }
        public string NumeroDocumento { get; set; }
        public string Digito { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public int IdSexo { get; set; }
        public string NombreSexo { get; set; }
        public object IdGenero { get; set; }
        public string NombreGenero { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public Nullable<int> Edad { get; set; }
        public string DireccionCalle { get; set; }
        public string DireccionNumero { get; set; }
        public string Telefono { get; set; }
        public string OtrosFonos { get; set; }
        public object Email { get; set; }
        public int IdRegion { get; set; }
        public int IdProvincia { get; set; }
        public object IdCiudad { get; set; }
        public int IdPrevision { get; set; }
        public object IdPrevisionTramo { get; set; }
        public object NombreCiudad { get; set; }
        public object IdPuebloOriginario { get; set; }
        public object DescripcionPuebloOriginario { get; set; }
        public object IdNacionalidad { get; set; }
        public object DescripcionNacionalidad { get; set; }
        public object IdNivelEducacional { get; set; }
        public object DescripcionNivelEducacional { get; set; }
    }

    public class Profesional
    {
        public int Id { get; set; }
        public object IdIdentificacion { get; set; }
        public string DescripcionIdentificacion { get; set; }
        public string NumeroDocumento { get; set; }
        public object Digito { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public object IdSexo { get; set; }
        public object NombreSexo { get; set; }
        public object IdGenero { get; set; }
        public object NombreGenero { get; set; }
        public object FechaNacimiento { get; set; }
        public object Edad { get; set; }
        public object DireccionCalle { get; set; }
        public object DireccionNumero { get; set; }
        public object Telefono { get; set; }
        public object OtrosFonos { get; set; }
        public object Email { get; set; }
        public object IdRegion { get; set; }
        public object IdProvincia { get; set; }
        public object IdCiudad { get; set; }
        public object IdPrevision { get; set; }
        public object IdPrevisionTramo { get; set; }
        public object NombreCiudad { get; set; }
    }

    public class Turno
    {
        public int Id { get; set; }
        public string Valor { get; set; }
    }

    public class Responsable
    {
        public int IdPersona { get; set; }
        public int IdIdentificacion { get; set; }
        public string DescripcionIdentificacion { get; set; }
        public string NumeroDocumento { get; set; }
        public string Digito { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public int IdSexo { get; set; }
        public string NombreSexo { get; set; }
        public object IdGenero { get; set; }
        public object NombreGenero { get; set; }
        public object FechaNacimiento { get; set; }
        public object Edad { get; set; }
        public object DireccionCalle { get; set; }
        public object DireccionNumero { get; set; }
        public object Telefono { get; set; }
        public object OtrosFonos { get; set; }
        public string Email { get; set; }
        public object IdRegion { get; set; }
        public object IdProvincia { get; set; }
        public object IdCiudad { get; set; }
        public object IdPrevision { get; set; }
        public object IdPrevisionTramo { get; set; }
        public object NombreCiudad { get; set; }
        public object IdPuebloOriginario { get; set; }
        public object DescripcionPuebloOriginario { get; set; }
        public object IdNacionalidad { get; set; }
        public object DescripcionNacionalidad { get; set; }
        public object IdNivelEducacional { get; set; }
        public object DescripcionNivelEducacional { get; set; }
    }

    public class TipoResponsable
    {
        public int Id { get; set; }
        public string Valor { get; set; }
    }

    public class Extremidad
    {
        public object Id { get; set; }
        public object Valor { get; set; }
    }

    public class Plano
    {
        public object Id { get; set; }
        public object Valor { get; set; }
    }

    public class TipoHoja
    {
        public int Id { get; set; }
        public string Valor { get; set; }
    }

    public class SignosVitale
    {
        public DateTime Fecha { get; set; }
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
    }

    public class TipoCuidado
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Horario { get; set; }
        public int IdEstado { get; set; }
        public string DescripcionEstado { get; set; }
        public string Observacion { get; set; }
        public string ObservacionCierre { get; set; }
    }

    public class TipoInvasivo
    {
        public int IdHojaEnfermeriaInvasivo { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int IdProfesionalCrea { get; set; }
        public string NombreProfesionalCrea { get; set; }
        public int IdTipoInvasivo { get; set; }
        public string DescripcionTipoInvasivo { get; set; }
        public int IdTipoPlano { get; set; }
        public string DescripcionPlano { get; set; }
        public int IdTipoExtremidad { get; set; }
        public string DescripcionExtremidad { get; set; }
        public DateTime FechaCierre { get; set; }
        public int IdProfesionalCierra { get; set; }
        public string NombreProfesionalCierre { get; set; }
        public string Observacion { get; set; }
    }

    public class TipoPeso
    {
        public int Id { get; set; }
        public int IdTipoPeso { get; set; }
        public string DescripcionPeso { get; set; }
        public double Peso { get; set; }
        public DateTime Fecha { get; set; }
    }

    public class TipoTalla
    {
        public int Id { get; set; }
        public int IdTipoTalla { get; set; }
        public string DescripcionTalla { get; set; }
        public double Talla { get; set; }
        public DateTime Fecha { get; set; }
    }

    public class TipoBalance
    {
        public int Id { get; set; }
        public int IdTipoBalance { get; set; }
        public string Tipo { get; set; }
        public string DescripcionBalance { get; set; }
        public double Balance { get; set; }
        public DateTime Fecha { get; set; }
        public string Profesional { get; set; }
    }

    public class Evolucion
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public string NombreProfesional { get; set; }
    }

    public class Examene
    {
        public int IdHojaEnfermeriaExamen { get; set; }
        public string DescripcionExamen { get; set; }
        public int IdServicioCartera { get; set; }
        public string DescripcionEstado { get; set; }
        public int IdEstado { get; set; }
        public DateTime FechaSolicitud { get; set; }
        public DateTime? FechaTermino { get; set; }
        public string ProfesionalSolicitud { get; set; }
        public string ProfesionalTermina { get; set; }
    }

    public class RespuestasItem
    {
        public int IdHojaEnfermeriaRespuesta { get; set; }
        public string Profesional { get; set; }
        public DateTime Fecha { get; set; }
        public int IdItem { get; set; }
    }

    //Clases Riesgo y Valoraciones
    public class Alternativa
    {
        public int IdRespuesta { get; set; }
        public string DescripcionAlternativa { get; set; }
    }

    public class Pregunta
    {
        public int IdPregunta { get; set; }
        public string DescripcionPregunta { get; set; }
        public bool MultipleSeleccion { get; set; }
        public List<Alternativa> Alternativas { get; set; }
    }

    public class Item
    {
        public int IdItem { get; set; }
        public string DescripcionItem { get; set; }
        public List<Pregunta> Preguntas { get; set; }
    }

}