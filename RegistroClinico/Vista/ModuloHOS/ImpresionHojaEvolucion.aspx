﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/Impresion.Master" AutoEventWireup="true" CodeBehind="ImpresionHojaEvolucion.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.ImpresionHojaEvolucion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {

            var urlParams = window.location.search;
            var getQuery = urlParams.split('?')[1];
            var idHE = getQuery.split('id=')[1];
            var json = {};

            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
                async: false,
                headers: { 'Authorization': GetToken() },
                success: function (data) {
                    json.LoginUsuario = data[0].GEN_loginUsuarios;
                }
            });
            
            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + "RCE_Hoja_Evolucion/" + idHE,
                async: false,
                success: function (data) {

                    json.GEN_idPaciente = data[0].GEN_idPaciente;
                    json.RCE_fechaHoja_Evolucion = moment(moment(data[0].RCE_fechaHoja_Evolucion).toDate()).format('DD-MM-YYYY');
                    json.GEN_nombreUbicacion = data[0].GEN_nombreUbicacion;
                    json.RCE_evolucionHoja_Evolucion = data[0].RCE_evolucionHoja_Evolucion;
                    json.RCE_examen_FisicoHoja_Evolucion = data[0].RCE_examen_FisicoHoja_Evolucion;
                    json.RCE_descripcionHoja_Evolucion = data[0].RCE_descripcionHoja_Evolucion;
                    json.RCE_diagnostico_principalHoja_Evolucion = data[0].RCE_diagnostico_principalHoja_Evolucion;
                    json.RCE_otros_diagnosticosHoja_Evolucion = data[0].RCE_otros_diagnosticosHoja_Evolucion;
                    json.GEN_nombrePersonasProfesional = data[0].GEN_nombrePersonasProfesional;
                    json.GEN_apellido_paternoPersonasProfesional = data[0].GEN_apellido_paternoPersonasProfesional;
                    json.GEN_apellido_maternoPersonasProfesional = data[0].GEN_apellido_maternoPersonasProfesional;
                    json.GEN_numero_documentoPersonasProfesional = data[0].GEN_numero_documentoPersonasProfesional;
                    
                }
            });
            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + "GEN_Paciente/" + json.GEN_idPaciente,
                async: false,
                success: function (data) {
                    json.Paciente = {};
                    json.Paciente.GEN_nomSexo = data.GEN_Sexo.GEN_nomSexo;
                    json.Paciente.GEN_numero_documentoPaciente = data.GEN_numero_documentoPaciente;
                    json.Paciente.GEN_digitoPaciente = data.GEN_digitoPaciente;
                    json.Paciente.GEN_nombrePaciente = data.GEN_nombrePaciente;
                    json.Paciente.GEN_ape_paternoPaciente = data.GEN_ape_paternoPaciente;
                    json.Paciente.GEN_ape_maternoPaciente = data.GEN_ape_maternoPaciente;
                    json.Paciente.EdadPaciente = CalcularEdad(moment(moment(data.GEN_fec_nacimientoPaciente).toDate()).format('YYYY-MM-DD'));
                    json.Paciente.GEN_fec_nacimientoPaciente = moment(moment(data.GEN_fec_nacimientoPaciente).toDate()).format('DD-MM-YYYY');
                    json.Paciente.GEN_dir_callePaciente = data.GEN_dir_callePaciente;
                    json.Paciente.GEN_telefonoPaciente = data.GEN_telefonoPaciente;
                    json.Paciente.GEN_otros_fonosPaciente = data.GEN_otros_fonosPaciente;
                }
            });
                    
            __doPostBack("<%= btnHidden.UniqueID %>", JSON.stringify(json));

        });

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <asp:Button runat="server" ID="btnHidden" OnClick="btnHidden_Click" style="display:none;" />

    <div class="card">
        <div class="card-header white">
            <div class="row">
                <div class="col-md-4 text-left">
                    <img src="<%= ResolveClientUrl("~/Style/IMG/MINSAL.jpg") %>" style="width:100px;"/>
                </div>
                <div class="col-md-4 text-center">
                    <img src="<%= ResolveClientUrl("~/Style/IMG/ACRLOGO.jpg") %>" style="width:100px;"/>
                </div>
                <div class="col-md-4 text-right">
                    <img src="<%= ResolveClientUrl("~/Style/IMG/HOSPLOGO.jpg") %>" style="width:100px;"/>
                </div>
            </div>
        </div>
        <div class="card-body"  style="padding:0px;">

            <div style="margin: 5px 0px 5px 0px;">
                <h4 class="text-center">
                    <strong>HOJA DE EVOLUCIÓN</strong>
                </h4>
            </div>
            
            <div class="card">
                <div class="card-header"  style="padding: 8px;">
                    <h5> 
                        <strong>Datos Paciente</strong> 
                    </h5>
                </div>
                <div class="card-body">
                    <div class="row mt-1">
                        <div class="col-md-3">
                            <strong><asp:Label id="lblNumeroDocumento" runat="server" Text="RUT"></asp:Label></strong> 
                            <asp:TextBox ID="txtNumeroDocumentoPaciente" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-9">
                            <strong>Nombre completo</strong>
                            <asp:TextBox ID="txtNombresPaciente" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-md-3">
                            <strong>Fecha de Nacimiento</strong>
                            <asp:TextBox ID="txtFechaNacimiento" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <strong>Edad</strong>
                            <asp:TextBox ID="txtEdad" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <strong>Dirección</strong>    
                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control">
                            </asp:TextBox>    
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-md-2">
                            <strong>Sexo</strong>
                            <asp:TextBox ID="txtSexo" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <strong>Teléfono</strong> 
                            <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control">
                            </asp:TextBox>    
                        </div>
                        <div class="col-md-3">
                            <strong>Otro teléfono</strong>
                            <asp:TextBox ID="txtOtroTelefono" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">

                <div class="card-header" style="padding-top: 8px">
                    <h5>
                        <strong>Datos Clínicos</strong>
                    </h5>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-3">
                            <strong>Fecha</strong>
                            <asp:TextBox ID="txtFechaHojaEvolucion" runat="server" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-9">
                            <strong>Servicio / Unidad</strong>
                            <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" text="Unidad Endocrinología, Nutrición y Diabetes">
                            </asp:TextBox>
                        </div>
                    </div>
                    
                    <div class="row mt-1">
                        <div class="col-md-6">
                            <strong>Diagnóstico Principal</strong>
                            <asp:TextBox ID="txtDiagnosticoPrincipal" runat="server" TextMode="MultiLine" CssClass="md-textarea md-textarea-auto form-control" Rows="1">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <strong>Diagnósticos Secundarios</strong>
                            <asp:TextBox ID="txtOtrosDiagnosticos" runat="server" TextMode="MultiLine" CssClass="md-textarea md-textarea-auto form-control" Rows="1">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-md-12">
                            <strong>Evolución</strong>
                            <asp:Panel id="pnlEvolucion" runat="server" BorderWidth="1px" BorderColor="Silver" style="padding: 5px !important;">
                            </asp:Panel>
                        </div>
                    </div>
                    
                    <div class="row mt-1">
                        <div class="col-md-12">
                            <strong>Examen Físico</strong>
                            <asp:Panel id="pnlExamenFisico" runat="server" BorderWidth="1px" BorderColor="Silver" style="padding: 5px !important;">
                            </asp:Panel>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-md-12">
                            <strong>Plan de manejo y/o indicaciones</strong>
                            <asp:Panel id="pnlDetalleEvolucion" runat="server" BorderWidth="1px" BorderColor="Silver" style="padding: 5px !important;">
                            </asp:Panel>
                        </div>
                    </div>
                </div>
                
                <div class="text-center" style="margin-top: 100px;">
                    <strong style="border-top: 2px solid black; padding: 0px 50px 0px 50px;">
                        FIRMA: <asp:Label ID="lblProfesional" runat="server" Text="SAMUEL ALEJANDRO FLORES IGOR - 10306215-2"></asp:Label>
                    </strong>
                </div>
            
            </div>

        </div>
    </div>
</asp:Content>
