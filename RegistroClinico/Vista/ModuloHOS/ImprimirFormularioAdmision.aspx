﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/Impresion.Master" AutoEventWireup="true" CodeBehind="ImprimirFormularioAdmision.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.ImprimirFormularioAdmision" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link rel="stylesheet" href="<%= GetHost() %>/Style/Bootstrap_3_3_7/bootstrap.min.css" />
    <link rel="stylesheet" href="<%= GetHost() %>/Style/impresion.css" />
    <script type="text/javascript" src="<%= GetHost() %>/Script/Bootstrap_3_3_7/bootstrap.min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="panel">
        <div class="panel-heading white">
            <div class="row">
                <div class="col-xs-4 text-left">
                    <img src="<%= GetHost() %>/Style/IMG/MINSAL.jpg" style="width: 100px;" />
                </div>
                <div class="col-xs-4 text-center">
                    <img src="<%= GetHost() %>/Style/IMG/ACRLOGO.jpg" style="width: 100px;" />
                </div>
                <div class="col-xs-4 text-right">
                    <img src="<%= GetHost() %>/Style/IMG/HOSPLOGO.jpg" style="width: 100px;" />
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding: 0px;">

            <div style="padding: 10px 10px 10px 10px;">
                <h4 class="text-center">
                    <strong>FORMULARIO INGRESO Y EGRESO PACIENTE HOSPITALIZADO Nº                        
                        <asp:Label ID="lblIdHospitalizacion" runat="server"></asp:Label>
                    </strong>
                </h4>
            </div>

            <div class="estilo-titulo">1- Identificación del Paciente</div>
            <table id="tblIdentificaPaciente" style="width: 100%; border: 1px solid black;">
                <tr>
                    <td class="fondo-titulos contenido-celda">
                        <asp:Label ID="lblNumeroDocumento" runat="server" Text="RUT"></asp:Label>
                    </td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblNumeroDocumentoPaciente" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">NUI</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblNuiPaciente" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Nombre</td>
                    <td class="contenido-celda" colspan="3">
                        <asp:Label ID="lblNombresPaciente" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Sexo</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblSexo" runat="server"></asp:Label></td>
                    <td class="fondo-titulos contenido-celda">Genero</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblGenero" runat="server">
                        </asp:Label></td>
                    <td class="fondo-titulos contenido-celda">Fecha Nacimiento</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblFechaNacimiento" runat="server">
                        </asp:Label></td>
                    <td class="fondo-titulos contenido-celda">Edad</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblEdad" runat="server">
                        </asp:Label></td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Dirección</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblDireccion" runat="server">
                        </asp:Label></td>
                    <td class="fondo-titulos contenido-celda">Telefono</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblTelefono" runat="server">
                        </asp:Label></td>
                    <td class="fondo-titulos contenido-celda">Telefono 2</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblOtroTelefono" runat="server">
                        </asp:Label></td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Pueblo Originario</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblPuebloOriginario" runat="server">
                        </asp:Label></td>

                    <td class="fondo-titulos contenido-celda">Nacionalidad</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblNacionalidad" runat="server">
                        </asp:Label></td>
                    <td class="fondo-titulos contenido-celda">Correo</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblCorreoPaciente" runat="server">
                        </asp:Label></td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Categoría Ocupacional</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblCategoriaOcupacional" runat="server">
                        </asp:Label></td>
                    <td class="fondo-titulos contenido-celda">Ocupación</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblOcupacion" runat="server">
                        </asp:Label></td>
                    <td class="fondo-titulos contenido-celda">Nivel Educacional</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblNivelEducacional" runat="server">
                        </asp:Label></td>
                </tr>

                <tr>
                    <td class="fondo-titulos contenido-celda">Previsión</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblPrevision" runat="server">
                        </asp:Label></td>
                    <td class="fondo-titulos contenido-celda">Tramo</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblPrevisionTramo" runat="server">
                        </asp:Label></td>
                    <td class="fondo-titulos contenido-celda">Modalidad</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblModalidadIngreso" runat="server">
                        </asp:Label></td>
                    <td class="fondo-titulos contenido-celda">Ley Previsional</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblLeyPrevisional" runat="server">
                        </asp:Label></td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Comuna de Residencia</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblCiudad" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda" colspan="2">Nombre Social</td>
                    <td class="contenido-celda" colspan="3">
                        <asp:Label ID="lblNombreSocial" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>
            <div class="estilo-titulo">2- Datos de Ingreso y Egreso del Paciente</div>
            <table id="tblIngresoAdmision" style="width: 100%; border: 1px solid black;">
                <tr>
                    <td class="fondo-titulos contenido-celda">Procedencia</td>
                    <td class="fondo-titulos contenido-celda">Establecimiento</td>
                    <td class="fondo-titulos contenido-celda">Cama Ingreso</td>
                    <td class="fondo-titulos contenido-celda">Cama Egreso</td>
                </tr>
                <tr>
                    <td class="contenido-celda">
                        <asp:Label ID="lblProcedencia" runat="server">
                        </asp:Label></td>

                    <td class="contenido-celda">
                        <asp:Label ID="lblEstablecimiento" runat="server">
                        </asp:Label></td>

                    <td class="contenido-celda">
                        <asp:Label ID="lblCamaIngreso" runat="server">
                        </asp:Label></td>

                    <td class="contenido-celda">
                        <asp:Label ID="lblCamaEgreso" runat="server">
                        </asp:Label></td>
                </tr>
            </table>
            <table style="width: 100%">
                <thead class="fondo-titulos contenido-celda">
                    <tr>
                        <td class="fondo-titulos contenido-celda">Movimiento</td>
                        <td class="fondo-titulos contenido-celda">Fecha</td>
                        <td class="fondo-titulos contenido-celda">Hora</td>
                        <td class="fondo-titulos contenido-celda">Servicio</td>
                        <td class="fondo-titulos contenido-celda">Codificación</td>
                    </tr>
                </thead>
                <tr>
                    <td class="fondo-titulos contenido-celda">Ingreso</td>
                    <td>
                        <asp:Label ID="lblFechaIngreso" runat="server">
                        </asp:Label></td>
                    <td>
                        <asp:Label ID="lblHoraIngreso" runat="server">
                        </asp:Label></td>
                    <td>
                        <asp:Label ID="lblUbicacionIngreso" runat="server">
                        </asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblCodificacionIngreso" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <asp:Literal ID="ltlTraslados" runat="server">
                </asp:Literal>
                <tr>
                    <td class="fondo-titulos contenido-celda">Egreso</td>
                    <td>
                        <asp:Label ID="lblFechaEgreso" runat="server">
                        </asp:Label></td>
                    <td>
                        <asp:Label ID="lblHoraEgreso" runat="server">
                        </asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblUbicacionEgreso" runat="server">
                        </asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblCodificacionEgreso" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>
            <div class="estilo-titulo">3- Datos Clínicos</div>
            <table style="width: 100%">
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="2">Diagnóstico Principal</td>
                    <td class="fondo-titulos contenido-celda">Causa externa</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                        <asp:Label ID="lblDiagnostico" runat="server"> 
                        </asp:Label></td>
                    <td>
                        <asp:Label ID="lblCausaExterna" runat="server">
                        </asp:Label></td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Condición de Egreso (Vivo/Fallecido)</td>
                    <td class="fondo-titulos contenido-celda">Parto (SI/NO)</td>
                    <td class="fondo-titulos contenido-celda">Nacimiento (Vivo/Fallecido)</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblCondicionEgreso" runat="server">
                        </asp:Label>
                    </td>
                    <td><asp:Label ID="lblParto" runat="server">
                        </asp:Label>
                    </td>
                    <td><asp:Label ID="lblNacimiento" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">RUT Médico Tratante</td>
                    <td class="fondo-titulos contenido-celda">Nombre Completo Médico Tratante</td>
                    <td class="fondo-titulos contenido-celda">Especialidad</td>
                </tr>
                <tr>
                    <td class=" contenido-celda">
                        <br />
                        <asp:Label ID="lblRunMedico" runat="server">
                        </asp:Label></td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblNombreMedico" runat="server">
                        </asp:Label></td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblEspecialidad" runat="server">
                        </asp:Label></td>
                </tr>
            </table>
        </div>
        <div class="panel-footer">
            <div class="row" style="margin-top: 150px !important;">
                <div class="col-xs-4">
                    <br />
                </div>
                <div class="col-xs-4 text-center" style="border-top: 1px solid #000 !important;">
                    Firma Médico
                </div>
                <div class="col-xs-4">
                    <br />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
