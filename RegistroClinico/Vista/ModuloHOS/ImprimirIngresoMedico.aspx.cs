﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using RegistroClinico.Negocio;

namespace RegistroClinico.Vista.ModuloHOS
{
    public partial class ImprimirIngresoMedico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            // Valida sesión según los permisos que tenga el perfil
            NegSession negSession = new NegSession(Page);
            negSession.ValidarPermisosSession();

            //Para no hacer el Postback
            HttpContext context = HttpContext.Current;
            string dominio = Funciones.GetUrlWebApi();
            string idHos = Request.QueryString["id"];

            // ${GetWebApiUrl()}
            string url = string.Format("{0}RCE_Ingreso_Medico/{1}", dominio, idHos);
            string token = string.Format("Bearer {0}", Session["TOKEN"]/*Page.Request.Cookies["DATA-TOKEN"].Value*/);
            string sJson = Funciones.ObtenerJsonWebApi(url, token);

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;
            List<Dictionary<string, object>> list = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(sJson, settings);
            Dictionary<string, object> json = list[0];
            Dictionary<string, object> paciente = JsonConvert.DeserializeObject<Dictionary<string, object>>(json["Paciente"].ToString(), settings);
            Dictionary<string, object> identificacion = JsonConvert.DeserializeObject<Dictionary<string, object>>(paciente["Identificacion"].ToString(), settings);
            Dictionary<string, object> sexo = JsonConvert.DeserializeObject<Dictionary<string, object>>(paciente["Sexo"].ToString(), settings);
            Dictionary<string, object> direccion = JsonConvert.DeserializeObject<Dictionary<string, object>>(paciente["Direccion"].ToString(), settings);

            lblidentificacion.Text = identificacion["GEN_nombreIdentificacion"].ToString();
            txtNumeroDocumentoPaciente.Text = identificacion["GEN_numero_documentoPaciente"].ToString() + "-" + identificacion["GEN_digitoPaciente"];
            lblFechaCreacion.Text = json["RCE_fecha_hora_creacionIngreso_Medico"].ToString();
            txtNombrePaciente.Text = paciente["GEN_nombrePersonasPaciente"].ToString() + " "
            + paciente["GEN_apellido_paternoPersonasPaciente"].ToString() + " "
            + paciente["GEN_apellido_maternoPersonasPaciente"].ToString();
            txtSexo.Text = sexo["GEN_nomSexoPaciente"].ToString();
            txtFechaNacimiento.Text = DateTime.Parse(paciente["GEN_fec_nacimientoPaciente"].ToString()).ToString("dd-MM-yyyy");
            string sEdad = Funciones.CalculaEdad(Convert.ToString(paciente["GEN_fec_nacimientoPaciente"]));
            Dictionary<string, string> jEdad = JsonConvert.DeserializeObject<Dictionary<string, string>>(sEdad);
            txtEdad.Text = jEdad["edad"].ToString();
            //Hay que deshabilitar la Excepcion con llaves nulas para evitar hacer estas comprobaciones dijo
            txtDireccion.Text = direccion["GEN_dir_callePaciente"] != null ? direccion["GEN_dir_callePaciente"].ToString() : " ";
            txtDireccion.Text += direccion["GEN_dir_numeroPaciente"] != null ? " " + direccion["GEN_dir_numeroPaciente"].ToString() : " " ;
            txtTelefono.Text = paciente["GEN_telefonoPaciente"] != null ? paciente["GEN_telefonoPaciente"].ToString(): " " ;
            txtOtroTelefono.Text = paciente["GEN_otros_fonosPaciente"] != null ? paciente["GEN_otros_fonosPaciente"].ToString() : " ";

            List<Dictionary<string, object>> uciLista = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_UCI"].ToString(), settings);
            List<Dictionary<string, object>> obstetricoLista = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Obstetricia"].ToString(), settings);
            List<Dictionary<string, object>> ginecologicoLista = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Ginecologia"].ToString(), settings);
            List<Dictionary<string, object>> pediatricaLista = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Pediatrico"].ToString(), settings);

            //IM UCI
            if (uciLista.Count > 0)
            {
                pnlUCI.Visible = true;
                lblTipoIngresoMedico.Text = "UCI";
                Dictionary<string, object> imUCI = uciLista[0];
                Dictionary<string, object> escalaRanking = JsonConvert.DeserializeObject<Dictionary<string, object>>(imUCI["GEN_Escala_Rankin"].ToString(), settings);
                txtEscalaRakin.Text = escalaRanking["GEN_nombreEscala_Rankin"].ToString();
                if (imUCI["RCE_apache2Ingreso_Medico_UCI"] != null)
                {
                    txtApache2.Text = imUCI["RCE_apache2Ingreso_Medico_UCI"].ToString();
                    txtMortalidad.Text = GetPorcentajeMortalidad(int.Parse(imUCI["RCE_apache2Ingreso_Medico_UCI"].ToString()));

                }
                //Revisar el marcado del badge
                lblViaAereaDificilSi.CssClass += (imUCI["RCE_via_aerea_dificilIngreso_Medico_UCI"].ToString() == "SI") ? " badge-dark" : " badge-light";
                lblViaAereaDificilNo.CssClass += (imUCI["RCE_via_aerea_dificilIngreso_Medico_UCI"].ToString() == "NO") ? " badge-dark" : " badge-light";
            }

            //Via aerea dificil
            //Antecedentes Clinicos (Son un grupo de cosas)
            txtFechaIngresoMedico.Text = DateTime.Parse(json["RCE_fecha_horaIngreso_Medico"].ToString()).ToString("dd-MM-yyyy");
            Dictionary<string, object> ubicacion = JsonConvert.DeserializeObject<Dictionary<string, object>>(json["Ubicacion"].ToString(), settings);
            txtUbicacion.Text = ubicacion["GEN_nombreUbicacion"].ToString();
            txtMotivoSolicitud.Text = json["RCE_motivo_consultaIngreso_Medico"].ToString();
            txtAnamnesis.Text = json["RCE_anamnesisIngreso_Medico"].ToString();
            lblExamenFisico.Text = json["RCE_examen_fisicoIngreso_Medico"] != null ? json["RCE_examen_fisicoIngreso_Medico"].ToString() : "";
            lblExamenSegmentario.Text = json["RCE_examen_segmentadoIngreso_medico"] != null ? json["RCE_examen_segmentadoIngreso_medico"].ToString() : "";
            txtDiagnostico.Text += json["RCE_diagnosticoIngreso_Medico"].ToString();

            Dictionary<string, object> objetoDiagnosticos = JsonConvert.DeserializeObject<Dictionary<string, object>>(json["Diagnostico_CIE10"].ToString(), settings);
            List<Dictionary<string, object>> listaDiagnosticos = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(objetoDiagnosticos["RCE_Ingreso_Medico_Diagnostico_CIE10"].ToString(), settings);
            foreach (Dictionary<string, object> diagnostico in listaDiagnosticos) {
                Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(diagnostico["GEN_Diagnostico_CIE10"].ToString(), settings);
                ltlDiagnosticosCIE10.Text += data["GEN_cod_muestraDiagnostico_CIE10"] +" - "+ data["GEN_descripcionDiagnostico_CIE10"] + "<br/>";
            }

            txtPlanManejo.Text = json["RCE_plan_manejoIngreso_Medico"].ToString();

            //EXAMEN FÍSICO GENERAL
            string htmlExamenFisico = "";

            htmlExamenFisico = GetBloqueAntecedenteClinico("Peso", Convert.ToString(json["RCE_pesoIngreso_Medico"]));
            htmlExamenFisico += GetBloqueAntecedenteClinico("Talla", Convert.ToString(json["RCE_tallaIngreso_Medico"]));
            htmlExamenFisico += GetBloqueAntecedenteClinico("PA", Convert.ToString(json["RCE_presion_arterialIngreso_Medico"]));
            htmlExamenFisico += GetBloqueAntecedenteClinico("FC", Convert.ToString(json["RCE_frecuencia_arterialIngreso_Medico"]));
            htmlExamenFisico += GetBloqueAntecedenteClinico("FR", Convert.ToString(json["RCE_frecuencia_respiratoriaIngreso_Medico"]));
            htmlExamenFisico += GetBloqueAntecedenteClinico("T°", Convert.ToString(json["RCE_temperaturaIngreso_Medico"]));

            if (int.Parse(json["RCE_idTipo_Ingreso_Medico"].ToString()) == 4)
            {
                //List<Dictionary<string, object>> imPediatria = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Pediatrico"].ToString(), settings);
                //htmlExamenFisico += GetBloqueAntecedenteClinico("Tanner", Convert.ToString(imPediatria[0]["RCE_tannerIngreso_Medico_Pediatrico"]));
                //htmlExamenFisico += GetBloqueAntecedenteClinico("Dentición", Convert.ToString(imPediatria[0]["RCE_denticionIngreso_Medico_Pediatrico"]));
                //htmlExamenFisico += GetBloqueAntecedenteClinico("P art.", Convert.ToString(imPediatria[0]["RCE_partIngreso_Medico_Pediatrico"]));
                //htmlExamenFisico += GetBloqueAntecedenteClinico("Saturometría", Convert.ToString(imPediatria[0]["RCE_saturometriaIngreso_Medico_Pediatrico"]));
                //htmlExamenFisico += GetBloqueAntecedenteClinico("Score", Convert.ToString(imPediatria[0]["RCE_scoreIngreso_Medico_Pediatrico"]));
            }

            htmlExamenFisico += GetBloqueAntecedenteClinico("Info. adicional", Convert.ToString(json["RCE_examen_fisicoIngreso_Medico"]));

            if (htmlExamenFisico != "")
                lblExamenFisico.Text = htmlExamenFisico;
            else
                pnlExamenFisico.Visible = false;

            string htmlMorbidos = "";
            string htmlHabitos = "";

            //ANTECEDENTES PACIENTE
            if (int.Parse(json["RCE_idTipo_Ingreso_Medico"].ToString()) == 4)
            {

                List<Dictionary<string, object>> imPediatria = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Pediatrico"].ToString(), settings);
                // Personales
                htmlMorbidos += GetBloqueAntecedenteClinico("Convulsiones", Convert.ToString(imPediatria[0]["RCE_convulsiones_personalIngreso_Medico_Pediatrico"]));
                htmlMorbidos += GetBloqueAntecedenteClinico("SBOR", Convert.ToString(imPediatria[0]["RCE_SBOR_personalIngreso_Medico_Pediatrico"]));
                htmlMorbidos += GetBloqueAntecedenteClinico("Neumonías", Convert.ToString(imPediatria[0]["RCE_neumonias_personalIngreso_Medico_Pediatrico"]));
                htmlMorbidos += GetBloqueAntecedenteClinico("Gastrointest", Convert.ToString(imPediatria[0]["RCE_gastrointest_personaltIngreso_Medico_Pediatrico"]));
                htmlMorbidos += GetBloqueAntecedenteClinico("Urinario", Convert.ToString(imPediatria[0]["RCE_urinario_personalIngreso_Medico_Pediatrico"]));

            }

            // 1. Morbidos
            htmlMorbidos += GetBloqueAntecedenteClinico("Medicos", Convert.ToString(json["RCE_medicosIngreso_Medico"]));
            htmlMorbidos += GetBloqueAntecedenteClinico("Cirugías", Convert.ToString(json["RCE_cirugiasIngreso_Medico"]));
            htmlMorbidos += GetBloqueAntecedenteClinico("Alergías", Convert.ToString(json["RCE_alergiasIngreso_Medico"]));
            htmlMorbidos += GetBloqueAntecedenteClinico("Hipertensión", Convert.ToString(json["RCE_hipertensionIngreso_Medico"]));
            htmlMorbidos += GetBloqueAntecedenteClinico("Diabetes", Convert.ToString(json["RCE_diabetesIngreso_Medico"]));
            htmlMorbidos += GetBloqueAntecedenteClinico("Asma", Convert.ToString(json["RCE_asmaIngreso_Medico"]));

            if (htmlMorbidos != "")
            {
                string titulo = "<strong><i class='fa fa-angle-double-right'></i> Mórbidos</strong><br />";

                if (int.Parse(json["RCE_idTipo_Ingreso_Medico"].ToString()) == 4)
                    titulo += "<strong><i class='fa fa-angle-double-right'></i> Personales</strong><br />";

                htmlMorbidos = titulo + htmlMorbidos;

            }

            lblAntecedentesClinicos.Text = htmlMorbidos;

            // 2. Hábitos
            htmlHabitos += GetBloqueAntecedenteClinico("Tabaco", Convert.ToString(json["RCE_tabacoIngreso_Medico"]));
            htmlHabitos += GetBloqueAntecedenteClinico("OH", Convert.ToString(json["RCE_OHIngreso_Medico"]));
            htmlHabitos += GetBloqueAntecedenteClinico("Drogas", Convert.ToString(json["RCE_drogasIngreso_Medico"]));

            if (htmlHabitos != "")
                htmlHabitos = (lblAntecedentesClinicos.Text != "") ?
                                    "<br /><strong><i class='fa fa-angle-double-right'></i> Hábitos</strong><br />" + htmlHabitos :
                                    "<strong><i class='fa fa-angle-double-right'></i> Hábitos</strong><br />" + htmlHabitos;


            lblAntecedentesClinicos.Text += htmlHabitos;

            // 3. Medicamentos
            if (Convert.ToString(json["RCE_medicamentosIngreso_Medico"]) != "")
            {
                lblAntecedentesClinicos.Text += (lblAntecedentesClinicos.Text != "") ?
                    "<br /><strong><i class='fa fa-angle-double-right'></i> Medicamentos</strong><br />" + Convert.ToString(json["RCE_medicamentosIngreso_Medico"]) :
                    "<strong><i class='fa fa-angle-double-right'></i> Medicamentos</strong><br />" + Convert.ToString(json["RCE_medicamentosIngreso_Medico"]);
            }

            // 4. Otros
            if (Convert.ToString(json["RCE_otrosIngreso_Medico"]) != "")
            {
                lblAntecedentesClinicos.Text += (lblAntecedentesClinicos.Text != "") ?
                    "<br /><strong><i class='fa fa-angle-double-right'></i> Otros</strong><br />" + Convert.ToString(json["RCE_otrosIngreso_Medico"]) :
                    "<strong><i class='fa fa-angle-double-right'></i> Otros</strong><br />" + Convert.ToString(json["RCE_otrosIngreso_Medico"]);
            }

            pnlAntecedentesClinicos.Visible = (lblAntecedentesClinicos.Text != "");

            //IM Obstetricia
            if (obstetricoLista.Count > 0)
            {
                pnlObstetricia.Visible = true;
                lblTipoIngresoMedico.Text = "Obstetricia/Ginecología";
                Dictionary<string, object> imObstetricia = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Obstetricia"].ToString(), settings)[0];
                txtFechaUltimaReglaObstetricia.Text = DateTime.Parse(imObstetricia["RCE_fecha_ultima_reglaIngreso_Medico_Obstetricia"].ToString()).ToString("dd-MM-yyyy");
                txtGestacionesObstetricia.Text = imObstetricia["RCE_gestacionesIngreso_Medico_Obstetricia"].ToString();
                txtPartosObstetricia.Text = imObstetricia["RCE_partosIngreso_Medico_Obstetricia"].ToString();
                txtAbortosObstetricia.Text = imObstetricia["RCE_abortosIngreso_Medico_Obstetricia"].ToString();
                lblOperacionalSi.CssClass += (imObstetricia["RCE_operacionalIngreso_Medico_Obstetricia"].ToString() == "SI") ? " badge-dark" : " badge-light";
                lblOperacionalNo.CssClass += (imObstetricia["RCE_operacionalIngreso_Medico_Obstetricia"].ToString() == "NO") ? " badge-dark" : " badge-light";

                Dictionary<string, object> tipoEmbarazo = JsonConvert.DeserializeObject<Dictionary<string, object>>(imObstetricia["RCE_Tipo_Embarazo"].ToString(), settings);
                txtTipoEmbarazo.Text = tipoEmbarazo["RCE_nombreTipo_Embarazo"].ToString();
                txtDescripcionEmbarazo.Text = imObstetricia["RCE_descripcion_embarazoIngreso_Medico_Obstetricia"].ToString();
                txtOtrosAntecedentes.Text = imObstetricia["RCE_otros_antecedentesIngreso_Medico_Obstetricia"].ToString();
                ltlExamenGinecoObstetrico.Text = imObstetricia["RCE_examen_fisico_obstetricoIngreso_Medico_Obstetricia"].ToString();
                ltlEcografiaObstetrica.Text = imObstetricia["RCE_ecografia_obstetricaIngreso_Medico_Obstetricia"].ToString();
                txtRbns.Text = imObstetricia["RCE_rbneIngreso_Medico_Obstetricia"].ToString();
                txtDu.Text = imObstetricia["RCE_duIngreso_Medico_Obstetricia"].ToString();
                txtPerfilBiofisico.Text = imObstetricia["RCE_perfil_biofisicoIngreso_Medico_Obstetricia"].ToString();
                txtLaboratorio.Text = imObstetricia["RCE_laboratorioIngreso_Medico_Obstetricia"].ToString();
                txtReposo.Text = imObstetricia["RCE_reposoIngreso_Medico_Obstetricia"].ToString();
                txtRegimen.Text = imObstetricia["RCE_regimenIngreso_Medico_Obstetricia"].ToString();
                txtPrescripciones.Text = imObstetricia["RCE_prescripcionIngreso_Medico_Obstetricia"].ToString();
                txtCsv.Text = imObstetricia["RCE_csvIngreso_Medico_Obstetricia"].ToString();
                txtControlObstetrico.Text = imObstetricia["RCE_control_obstetricoIngreso_Medico_Obstetricia"].ToString();
                txtHgt.Text = imObstetricia["RCE_hgtIngreso_Medico_Obstetricia"].ToString();
                txtExamenes.Text = imObstetricia["RCE_examenesIngreso_Medico_Obstetricia"].ToString();
                txtInterconsultas.Text = imObstetricia["RCE_interconsultasIngreso_Medico_Obstetricia"].ToString();
                txtProcedimientos.Text = imObstetricia["RCE_procedimientosIngreso_Medico_Obstetricia"].ToString();
            }

            //IM Ginecología
            if (ginecologicoLista.Count > 0)
            {
                pnlGinecologia.Visible = true;
                lblTipoIngresoMedico.Text = "Ginecología";
                Dictionary<string, object> imGinecologia = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Ginecologia"].ToString(), settings)[0];
                Dictionary<string, object> metodoAnticonceptivo = JsonConvert.DeserializeObject<Dictionary<string, object>>(imGinecologia["RCE_Metodo_Anticonceptivo"].ToString(), settings);
                txtFechaUltimaRegla.Text = DateTime.Parse(imGinecologia["RCE_fecha_ultima_reglaIngreso_Medico_Ginecologia"].ToString()).ToString("dd-MM-yyyy");
                txtGestaciones.Text = imGinecologia["RCE_gestacionesIngreso_Medico_Ginecologia"].ToString();
                txtPartos.Text = imGinecologia["RCE_partosIngreso_Medico_Ginecologia"].ToString();
                txtAbortos.Text = imGinecologia["RCE_abortosIngreso_Medico_Ginecologia"].ToString();
                txtCiclos.Text = imGinecologia["RCE_ciclosIngreso_Medico_Ginecologia"].ToString();
                txtPapanicolaou.Text = imGinecologia["RCE_papanicolaouIngreso_Medico_Ginecologia"].ToString();
                txtMamografia.Text = imGinecologia["RCE_mamografiaIngreso_Medico_Ginecologia"].ToString();
                txtEcografiaTV.Text = imGinecologia["RCE_ecogtafia_normalIngreso_Medico_Ginecologia"] != null ? imGinecologia["RCE_ecogtafia_normalIngreso_Medico_Ginecologia"].ToString() : "";
                txtDescripcionEcoAnormal.Text = imGinecologia["RCE_ecogtafia_anormalIngreso_Medico_Ginecologia"] != null ? imGinecologia["RCE_ecogtafia_anormalIngreso_Medico_Ginecologia"].ToString() : "";
                txtMetodoConceptivo.Text = metodoAnticonceptivo["RCE_descripcionMetodo_Anticonceptivo"].ToString();
                chbPipelleBiopsia.Checked = imGinecologia["RCE_pipelleIngreso_Medico_Ginecologia"].ToString() == "SI" ? true : false;
                chbPolipoBiopsia.Checked = imGinecologia["RCE_polipoIngreso_Medico_Ginecologia"].ToString() == "SI" ? true : false;
            }

            //IM Pediatría
            if (pediatricaLista.Count > 0) {
                pnlPediatrico.Visible = true;
                lblTipoIngresoMedico.Text = "Pediatría";
                List<Dictionary<string, object>> imPediatria = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Pediatrico"].ToString(), settings);

                // CUIDADOR
                if (Convert.ToString(imPediatria[0]["GEN_Personas2"]) != "")
                {

                    Dictionary<string, object> jsonPersonasCuidador = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Personas2"].ToString(), settings);
                    Dictionary<string, object> jsonIdentificacionCuidador = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonPersonasCuidador["GEN_Identificacion"].ToString(), settings);

                    // INFORMACIÓN CUIDADOR
                    lblIdentificacionCuidador.Text = Convert.ToString(jsonIdentificacionCuidador["GEN_nombreIdentificacion"]);
                    txtNumeroDocCuidador.Text = Convert.ToString(jsonPersonasCuidador["GEN_numero_documentoPersonas"]) +
                                            ((Convert.ToString(jsonPersonasCuidador["GEN_digitoPersonas"]) == "") ? "" :
                                                "-" + Convert.ToString(jsonPersonasCuidador["GEN_digitoPersonas"]));

                    txtNombreCompletoCuidador.Text = Convert.ToString(jsonPersonasCuidador["GEN_nombrePersonas"]) + " " +
                                                    Convert.ToString(jsonPersonasCuidador["GEN_apellido_paternoPersonas"]) + " " +
                                                    Convert.ToString(jsonPersonasCuidador["GEN_apellido_maternoPersonas"]);

                    txtParentescoCuidador.Text = Convert.ToString(imPediatria[0]["RCE_parentesco_cuidadorIngreso_Medico_Pediatrico"]);
                    txtTelefonoCuidador.Text = Convert.ToString(jsonPersonasCuidador["GEN_telefonoPersonas"]);
                    txtDireccionCuidador.Text = Convert.ToString(jsonPersonasCuidador["GEN_dir_callePersonas"]);

                }

                // 2. Antecedentes personales
                if (Convert.ToString(imPediatria[0]["GEN_Establecimiento"]) != "")
                {
                    Dictionary<string, object> jsonEstablecimiento = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Establecimiento"].ToString(), settings);
                    txtConsultorioOrigen.Text = jsonEstablecimiento["GEN_nombreEstablecimiento"].ToString();
                }

                if (Convert.ToString(imPediatria[0]["RCE_Tipo_Recien_Nacido"]) != "")
                {
                    Dictionary<string, object> jsonTipoRecienNacido = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["RCE_Tipo_Recien_Nacido"].ToString(), settings);
                    txtTipoRecienNacido.Text = jsonTipoRecienNacido["RCE_nombreTipo_Recien_Nacido"].ToString() + "(" +
                                                jsonTipoRecienNacido["RCE_descripcionTipo_Recien_Nacido"].ToString() + ")";
                }

                if (Convert.ToString(imPediatria[0]["GEN_Tipo_Parto"]) != "")
                {
                    Dictionary<string, object> jsonTipoParto = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Tipo_Parto"].ToString(), settings);
                    txtParto.Text = jsonTipoParto["GEN_nombreTipo_Parto"].ToString();
                }

                txtPatologiaPerinatal.Text = Convert.ToString(imPediatria[0]["RCE_patologia_perinatalIngreso_Medico_Pediatrico"]);
                txtOtrosAntecedentesPersonales.Text = Convert.ToString(imPediatria[0]["RCE_otros_antecedentes_personalesIngreso_Medico_Pediatrico"]);

                // VACUNAS
                // lblVacunasPaciente
                lblVacunasPaciente.Text =
                    "<div class='alert alert-secondary mt-2'>" +
                        "<i class='fa fa-exclamation-triangle'></i> El paciente no registra vacunas" +
                    "</div>";

                if (Convert.ToString(imPediatria[0]["GEN_Tipo_Edad"]) != "")
                {
                    Dictionary<string, object> jsonTipoEdad = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Tipo_Edad"].ToString(), settings);
                    txtEdadDuracionLMaterna.Text = Convert.ToString(imPediatria[0]["RCE_duracion_lactancia_añosIngreso_Medico_Pediatrico"]) + " " +
                                                    jsonTipoEdad["GEN_nombreTipo_Edad"].ToString().Trim();
                }

                if (Convert.ToString(imPediatria[0]["GEN_Tipo_Edad1"]) != "")
                {
                    Dictionary<string, object> jsonTipoEdad = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Tipo_Edad1"].ToString(), settings);
                    txtEdadInicioLArtificial.Text = Convert.ToString(imPediatria[0]["RCE_inicio_lactancia_añosIngreso_Medico_Pediatrico"]) + " " +
                                                    jsonTipoEdad["GEN_nombreTipo_Edad"].ToString().Trim();
                }

                txt1Comida.Text = (Convert.ToString(imPediatria[0]["RCE_fecha_primera_comidaIngreso_Medico_Pediatrico"]) == "") ? "" :
                        DateTime.Parse(imPediatria[0]["RCE_fecha_primera_comidaIngreso_Medico_Pediatrico"].ToString()).ToString("dd-MM-yyyy");
                txt2Comida.Text = (Convert.ToString(imPediatria[0]["RCE_fecha_segunda_comidaIngreso_Medico_Pediatrico"]) == "") ? "" :
                        DateTime.Parse(imPediatria[0]["RCE_fecha_segunda_comidaIngreso_Medico_Pediatrico"].ToString()).ToString("dd-MM-yyyy");

                lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("Alergias", Convert.ToString(imPediatria[0]["RCE_alergias_familiaresIngreso_Medico_Pediatrico"]));
                lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("Asma", Convert.ToString(imPediatria[0]["RCE_asma_familiaresIngreso_Medico_Pediatrico"]));
                lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("Epilepsia", Convert.ToString(imPediatria[0]["RCE_epilepsia_familiaresIngreso_Medico_Pediatrico"]));
                lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("Diabetes", Convert.ToString(imPediatria[0]["RCE_diabetes_familiaresIngreso_Medico_Pediatrico"]));
                lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("Cardiovasc.", Convert.ToString(imPediatria[0]["RCE_cardiovasc_familiaresIngreso_Medico_Pediatrico"]));
                lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("HTA", Convert.ToString(imPediatria[0]["RCE_hta_familiaresIngreso_Medico_Pediatrico"]));

                if (lblAntecedentesFamiliares.Text == "")
                    lblAntecedentesFamiliares.Visible = false;

                txtMuertesFamiliares.Text = Convert.ToString(imPediatria[0]["RCE_muertes_familiaresIngreso_Medico_Pediatrico"]);
                txtAlergiaFarmacosFamiliares.Text = Convert.ToString(imPediatria[0]["RCE_alergias_farmacos_familiaresIngreso_Medico_Pediatrico"]);
                txtOtrosAntecendentesFamiliares.Text = Convert.ToString(imPediatria[0]["RCE_otros_familiaresIngreso_Medico_Pediatrico"]);

                // Hospitalizaciones
                // lblHospitalizacionesPaciente.Text = "";
                lblHospitalizacionesPaciente.Text =
                    "<div class='alert alert-secondary'>" +
                        "<i class='fa fa-exclamation-triangle'></i> El paciente no registra hospitalizaciones." +
                    "</div>"; ;

                // PARENTAL 1
                if (Convert.ToString(imPediatria[0]["GEN_Personas"]) != "")
                {

                    Dictionary<string, object> jsonParental1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Personas"].ToString(), settings);
                    Dictionary<string, object> jsonIdentificacionParental1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental1["GEN_Identificacion"].ToString(), settings);

                    /*if (Convert.ToString(jsonParental1["GEN_Categoria_Ocupacion"]) != "")
                    {
                        Dictionary<string, object> jsonActividadParental1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental1["GEN_Categoria_Ocupacion"].ToString(), settings);
                        txtActividadParental1.Text = Convert.ToString(jsonActividadParental1["GEN_descripcionCategorias_Ocupacion"]);
                    }*/

                    if (Convert.ToString(jsonParental1["GEN_Tipo_Escolaridad"]) != "")
                    {
                        Dictionary<string, object> jsonEducacionParental1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental1["GEN_Tipo_Escolaridad"].ToString(), settings);
                        txtEducacionParental1.Text = Convert.ToString(jsonEducacionParental1["GEN_nombreTipo_Escolaridad"]);
                    }

                    // INFORMACIÓN CUIDADOR
                    lblIdentificionParental1.Text = Convert.ToString(jsonIdentificacionParental1["GEN_nombreIdentificacion"]);
                    txtNumeroDocParental1.Text = Convert.ToString(jsonParental1["GEN_numero_documentoPersonas"]) +
                                            ((Convert.ToString(jsonParental1["GEN_digitoPersonas"]) == "") ? "" :
                                                "-" + Convert.ToString(jsonParental1["GEN_digitoPersonas"]));

                    txtNombreParental1.Text = Convert.ToString(jsonParental1["GEN_nombrePersonas"]) + " " +
                                                    Convert.ToString(jsonParental1["GEN_apellido_paternoPersonas"]) + " " +
                                                    Convert.ToString(jsonParental1["GEN_apellido_maternoPersonas"]);
                    txtEdadParental1.Text = "";

                }

                // PARENTAL 2
                if (Convert.ToString(imPediatria[0]["GEN_Personas1"]) != "")
                {

                    Dictionary<string, object> jsonParental2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Personas1"].ToString(), settings);
                    Dictionary<string, object> jsonIdentificacionParental2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental2["GEN_Identificacion"].ToString(), settings);
                    /*
                    if (Convert.ToString(jsonParental2["GEN_Categoria_Ocupacion"]) != "")
                    {
                        Dictionary<string, object> jsonActividadParental2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental2["GEN_Categoria_Ocupacion"].ToString(), settings);
                        txtActividadParental2.Text = Convert.ToString(jsonActividadParental2["GEN_descripcionCategorias_Ocupacion"]);
                    }*/

                    if (Convert.ToString(jsonParental2["GEN_Tipo_Escolaridad"]) != "")
                    {
                        Dictionary<string, object> jsonEducacionParental2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental2["GEN_Tipo_Escolaridad"].ToString(), settings);
                        txtEducacionParental2.Text = Convert.ToString(jsonEducacionParental2["GEN_nombreTipo_Escolaridad"]);
                    }


                    // INFORMACIÓN CUIDADOR
                    lblIdentificionParental2.Text = Convert.ToString(jsonIdentificacionParental2["GEN_nombreIdentificacion"]);
                    txtNumeroDocParental2.Text = Convert.ToString(jsonParental2["GEN_numero_documentoPersonas"]) +
                                            ((Convert.ToString(jsonParental2["GEN_digitoPersonas"]) == "") ? "" :
                                                "-" + Convert.ToString(jsonParental2["GEN_digitoPersonas"]));

                    txtNombreParental2.Text = Convert.ToString(jsonParental2["GEN_nombrePersonas"]) + " " +
                                                    Convert.ToString(jsonParental2["GEN_apellido_paternoPersonas"]) + " " +
                                                    Convert.ToString(jsonParental2["GEN_apellido_maternoPersonas"]);
                    txtEdadParental2.Text = "";

                }

                if (Convert.ToString(imPediatria[0]["GEN_Estado_Conyugal"]) != "")
                {
                    Dictionary<string, object> jsonEstadoConyugal = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Estado_Conyugal"].ToString(), settings);
                    txtSituacionMarital.Text = jsonEstadoConyugal["GEN_descripcionEstado_Conyugal"].ToString();
                }

                // 6. OTROS DATOS
                if (Convert.ToString(imPediatria[0]["RCE_Tipo_Desarrollo_Psico_Motor"]) != "")
                {
                    Dictionary<string, object> jsonpSpicoMotor = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["RCE_Tipo_Desarrollo_Psico_Motor"].ToString(), settings);
                    txtDSMOtrosDatos.Text = jsonpSpicoMotor["RCE_nombreTipo_Desarrollo_Psico_Motor"].ToString();
                }

                if (Convert.ToString(imPediatria[0]["GEN_Tipo_Datos_Nutricionales"]) != "")
                {
                    Dictionary<string, object> jsonTipoDatosNutricionales = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Tipo_Datos_Nutricionales"].ToString(), settings);
                    txtNutricionalOtrosDatos.Text = jsonTipoDatosNutricionales["GEN_nombreTipo_Datos_Nutricionales"].ToString();
                }
            }

            Funciones funciones = new Funciones();
            string doc = "IngrMedic";
            funciones.ExportarPDFWK(this.Page, doc, idHos);
        }

        protected void btnHidden_Click(object sender, EventArgs e)
        {
            /*
            string sCommand = Request.Form["__EVENTARGUMENT"];

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            //JObject des = JsonConvert.DeserializeObject<JObject>(sCommand, settings);
            Dictionary<string,object> des = JsonConvert.DeserializeObject<Dictionary<string, object>>(sCommand, settings);

            pnlGinecologia.Visible = pnlUCI.Visible = pnlAnornal.Visible = pnlObstetricia.Visible = pnlPediatrico.Visible = false;
            pnlInfoTipoIngreos.Visible = true;

            // DATOS DE PACIENTE

            lblidentificacion.Text = des["GEN_nombreIdentificacion"].ToString();
            
            txtNumeroDocumentoPaciente.Text = des["GEN_numero_documentoPersonasPaciente"].ToString();
            txtNombrePaciente.Text = Convert.ToString(des["GEN_nombrePersonasPaciente"]) + " "
                                    + Convert.ToString(des["GEN_apellido_paternoPersonasPaciente"]) 
                                    + ((Convert.ToString(des["GEN_apellido_maternoPersonasPaciente"]) != "") ? " " + Convert.ToString(des["GEN_apellido_maternoPersonasPaciente"]) : "");

            txtSexo.Text = Convert.ToString(des["GEN_nomSexoPaciente"]);
            txtGenero.Text = Convert.ToString(des["GEN_nombreTipo_Genero"]);
            txtFechaNacimiento.Text = Convert.ToString(des["GEN_fec_nacimientoPaciente"]);
            txtEdad.Text = Convert.ToString(des["edad"]);
            txtDireccion.Text = Convert.ToString(des["GEN_dir_callePaciente"]);
            txtTelefono.Text = Convert.ToString(des["GEN_telefonoPaciente"]); 
            txtOtroTelefono.Text = Convert.ToString(des["GEN_otros_fonosPaciente"]);

            Dictionary<string, object> json = JsonConvert.DeserializeObject<Dictionary<string, object>>(des["RCE_Ingreso_Medico"].ToString(), settings);

            // DATOS DEL INGRESO MÉDICO
            lblFechaCreacion.Text = DateTime.Parse(json["RCE_fecha_hora_creacionIngreso_Medico"].ToString()).ToString("dd-MM-yyyy HH:mm:ss"); 
            txtFechaIngresoMedico.Text = DateTime.Parse(json["RCE_fecha_horaIngreso_Medico"].ToString()).ToString("dd-MM-yyyy HH:mm:ss");

            Dictionary<string, object> jsonUbicacion = JsonConvert.DeserializeObject<Dictionary<string, object>>(json["GEN_Ubicacion"].ToString(), settings);
            txtUbicacion.Text = Convert.ToString(jsonUbicacion["GEN_nombreUbicacion"]); 
            txtMotivoSolicitud.Text = Convert.ToString(json["RCE_motivo_consultaIngreso_Medico"]); 
            txtAnamnesis.Text = Convert.ToString(json["RCE_anamnesisIngreso_Medico"]);

            // EXAMEN FÍSICO GENERAL
            string htmlExamenFisico = "";

            htmlExamenFisico = GetBloqueAntecedenteClinico("Peso", Convert.ToString(json["RCE_pesoIngreso_Medico"]));
            htmlExamenFisico += GetBloqueAntecedenteClinico("Talla", Convert.ToString(json["RCE_tallaIngreso_Medico"]));
            htmlExamenFisico += GetBloqueAntecedenteClinico("PA", Convert.ToString(json["RCE_presion_arterialIngreso_Medico"]));
            htmlExamenFisico += GetBloqueAntecedenteClinico("FC", Convert.ToString(json["RCE_frecuencia_arterialIngreso_Medico"]));
            htmlExamenFisico += GetBloqueAntecedenteClinico("FR", Convert.ToString(json["RCE_frecuencia_respiratoriaIngreso_Medico"]));
            htmlExamenFisico += GetBloqueAntecedenteClinico("T°", Convert.ToString(json["RCE_temperaturaIngreso_Medico"]));

            if (int.Parse(json["RCE_idTipo_Ingreso_Medico"].ToString()) == 4) {

                //List<Dictionary<string, object>> imPediatria = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Pediatrico"].ToString(), settings);
                //htmlExamenFisico += GetBloqueAntecedenteClinico("Tanner", Convert.ToString(imPediatria[0]["RCE_tannerIngreso_Medico_Pediatrico"]));
                //htmlExamenFisico += GetBloqueAntecedenteClinico("Dentición", Convert.ToString(imPediatria[0]["RCE_denticionIngreso_Medico_Pediatrico"]));
                //htmlExamenFisico += GetBloqueAntecedenteClinico("P art.", Convert.ToString(imPediatria[0]["RCE_partIngreso_Medico_Pediatrico"]));
                //htmlExamenFisico += GetBloqueAntecedenteClinico("Saturometría", Convert.ToString(imPediatria[0]["RCE_saturometriaIngreso_Medico_Pediatrico"]));
                //htmlExamenFisico += GetBloqueAntecedenteClinico("Score", Convert.ToString(imPediatria[0]["RCE_scoreIngreso_Medico_Pediatrico"]));
            }

            htmlExamenFisico += GetBloqueAntecedenteClinico("Info. adicional", Convert.ToString(json["RCE_examen_fisicoIngreso_Medico"]));

            if (htmlExamenFisico != "")
                lblExamenFisico.Text = htmlExamenFisico;
            else
                pnlExamenFisico.Visible = false;

            txtDiagnostico.Text = Convert.ToString(json["RCE_diagnosticoIngreso_Medico"]); 
            txtPlanManejo.Text = Convert.ToString(json["RCE_plan_manejoIngreso_Medico"]);
            
            string htmlMorbidos = "";
            string htmlHabitos = "";

            // ANTECEDENDETES PACIENTE

            if (int.Parse(json["RCE_idTipo_Ingreso_Medico"].ToString()) == 4) {

                List<Dictionary<string, object>> imPediatria = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Pediatrico"].ToString(), settings);
                // Personales
                htmlMorbidos += GetBloqueAntecedenteClinico("Convulsiones", Convert.ToString(imPediatria[0]["RCE_convulsiones_personalIngreso_Medico_Pediatrico"]));
                htmlMorbidos += GetBloqueAntecedenteClinico("SBOR", Convert.ToString(imPediatria[0]["RCE_SBOR_personalIngreso_Medico_Pediatrico"]));
                htmlMorbidos += GetBloqueAntecedenteClinico("Neumonías", Convert.ToString(imPediatria[0]["RCE_neumonias_personalIngreso_Medico_Pediatrico"]));
                htmlMorbidos += GetBloqueAntecedenteClinico("Gastrointest", Convert.ToString(imPediatria[0]["RCE_gastrointest_personaltIngreso_Medico_Pediatrico"]));
                htmlMorbidos += GetBloqueAntecedenteClinico("Urinario", Convert.ToString(imPediatria[0]["RCE_urinario_personalIngreso_Medico_Pediatrico"]));

            }

            // 1. Morbidos
            htmlMorbidos += GetBloqueAntecedenteClinico("Medicos", Convert.ToString(json["RCE_medicosIngreso_Medico"]));
            htmlMorbidos += GetBloqueAntecedenteClinico("Cirugías", Convert.ToString(json["RCE_cirugiasIngreso_Medico"]));
            htmlMorbidos += GetBloqueAntecedenteClinico("Alergías", Convert.ToString(json["RCE_alergiasIngreso_Medico"]));
            htmlMorbidos += GetBloqueAntecedenteClinico("Hipertensión", Convert.ToString(json["RCE_hipertensionIngreso_Medico"]));
            htmlMorbidos += GetBloqueAntecedenteClinico("Diabetes", Convert.ToString(json["RCE_diabetesIngreso_Medico"]));
            htmlMorbidos += GetBloqueAntecedenteClinico("Asma", Convert.ToString(json["RCE_asmaIngreso_Medico"]));

            if (htmlMorbidos != "") {

                string titulo = "<strong><i class='fa fa-angle-double-right'></i> Mórbidos</strong><br />";

                if (int.Parse(json["RCE_idTipo_Ingreso_Medico"].ToString()) == 4)
                    titulo += "<strong><i class='fa fa-angle-double-right'></i> Personales</strong><br />";

                htmlMorbidos = titulo + htmlMorbidos;

            }

            lblAntecedentesClinicos.Text = htmlMorbidos;

            // 2. Hábitos
            htmlHabitos += GetBloqueAntecedenteClinico("Tabaco", Convert.ToString(json["RCE_tabacoIngreso_Medico"]));
            htmlHabitos += GetBloqueAntecedenteClinico("OH", Convert.ToString(json["RCE_OHIngreso_Medico"]));
            htmlHabitos += GetBloqueAntecedenteClinico("Drogas", Convert.ToString(json["RCE_drogasIngreso_Medico"]));

            if (htmlHabitos != "")
                htmlHabitos = (lblAntecedentesClinicos.Text != "") ?
                                    "<br /><strong><i class='fa fa-angle-double-right'></i> Hábitos</strong><br />" + htmlHabitos :
                                    "<strong><i class='fa fa-angle-double-right'></i> Hábitos</strong><br />" + htmlHabitos;


            lblAntecedentesClinicos.Text += htmlHabitos;

            // 3. Medicamentos
            if (Convert.ToString(json["RCE_medicamentosIngreso_Medico"]) != "")
            {
                lblAntecedentesClinicos.Text += (lblAntecedentesClinicos.Text != "") ?
                    "<br /><strong><i class='fa fa-angle-double-right'></i> Medicamentos</strong><br />" + Convert.ToString(json["RCE_medicamentosIngreso_Medico"]) :
                    "<strong><i class='fa fa-angle-double-right'></i> Medicamentos</strong><br />" + Convert.ToString(json["RCE_medicamentosIngreso_Medico"]);
            }

            // 4. Otros
            if (Convert.ToString(json["RCE_otrosIngreso_Medico"]) != "")
            {
                lblAntecedentesClinicos.Text += (lblAntecedentesClinicos.Text != "") ?
                    "<br /><strong><i class='fa fa-angle-double-right'></i> Otros</strong><br />" + Convert.ToString(json["RCE_otrosIngreso_Medico"]) :
                    "<strong><i class='fa fa-angle-double-right'></i> Otros</strong><br />" + Convert.ToString(json["RCE_otrosIngreso_Medico"]);
            }

            pnlAntecedentesClinicos.Visible = (lblAntecedentesClinicos.Text != "");

            StringBuilder html = new StringBuilder();
            ltlDiagnosticosCIE10.Text = "<table class='table'>";
            ltlDiagnosticosCIE10.Text += "   <thead class='thead-dark'>";
            ltlDiagnosticosCIE10.Text += "       <tr>";
            ltlDiagnosticosCIE10.Text += "           <th>Diagnóstico CIE-10</th>";
            ltlDiagnosticosCIE10.Text += "       </tr>";
            ltlDiagnosticosCIE10.Text += "   </thead>";
            ltlDiagnosticosCIE10.Text += "   <tbody>";
            ltlDiagnosticosCIE10.Text += "       {0}";
            ltlDiagnosticosCIE10.Text += "   </tbody>";
            ltlDiagnosticosCIE10.Text += "</table>";

            List<Dictionary<string, string>> list = 
                JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(des["diagnosticos"].ToString());

            for (int i = 0; i < list.Count; i++) {

                Dictionary<string, string> json = list[i];
                html.AppendLine("<tr>");
                html.AppendLine("   <td>");
                html.AppendLine("       " + json["GEN_descripcionDiagnostico_CIE10"]);
                html.AppendLine("   </td>");
                html.AppendLine("</tr>");

            }

            ltlDiagnosticosCIE10.Text = (list.Count > 0) ? string.Format(ltlDiagnosticosCIE10.Text, html.ToString()) : "";

            Label lblExamenSegmentario = new Label();
            lblExamenSegmentario.Text = Convert.ToString(json["RCE_examen_segmentadoIngreso_medico"]);
            pnlExamenSegmentario.Controls.Add(lblExamenSegmentario);

            // TIPO DE INGRESO MÉDICO
            Dictionary<string, object> jsonTipo = JsonConvert.DeserializeObject<Dictionary<string, object>>(json["RCE_Tipo_Ingreso_Medico"].ToString(), settings);
            lblTipoIngresoMedico.Text = jsonTipo["RCE_descripcionTipo_Ingreso_Medico"].ToString();

            switch (int.Parse(jsonTipo["RCE_idTipo_Ingreso_Medico"].ToString())) {

                case 2: // GINECOLOGÍA

                    pnlGinecologia.Visible = true;
                    List<Dictionary<string, object>> jsonIMGinecologia = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Ginecologia"].ToString(), settings);
                    Dictionary<string, object> jsonMA = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonIMGinecologia[0]["RCE_Metodo_Anticonceptivo"].ToString(), settings);

                    txtFechaUltimaRegla.Text = Convert.ToString(jsonIMGinecologia[0]["RCE_fecha_ultima_reglaIngreso_Medico_Ginecologia"]);
                    txtMetodoConceptivo.Text = Convert.ToString(jsonMA["RCE_descripcionMetodo_Anticonceptivo"]);

                    txtGestaciones.Text = Convert.ToString(jsonIMGinecologia[0]["RCE_gestacionesIngreso_Medico_Ginecologia"]);
                    txtPartos.Text = Convert.ToString(jsonIMGinecologia[0]["RCE_partosIngreso_Medico_Ginecologia"]);
                    txtAbortos.Text = Convert.ToString(jsonIMGinecologia[0]["RCE_abortosIngreso_Medico_Ginecologia"]);
                    txtCiclos.Text = Convert.ToString(jsonIMGinecologia[0]["RCE_ciclosIngreso_Medico_Ginecologia"]);

                    txtPapanicolaou.Text = Convert.ToString(jsonIMGinecologia[0]["RCE_papanicolaouIngreso_Medico_Ginecologia"]);
                    txtMamografia.Text = Convert.ToString(jsonIMGinecologia[0]["RCE_mamografiaIngreso_Medico_Ginecologia"]);

                    if (Convert.ToString(jsonIMGinecologia[0]["RCE_ecogtafia_normalIngreso_Medico_Ginecologia"]) == "SI")
                        txtEcografiaTV.Text = "Normal";
                    else if (Convert.ToString(jsonIMGinecologia[0]["RCE_ecogtafia_anormalIngreso_Medico_Ginecologia"]) != "") {
                        pnlAnornal.Visible = true;
                        txtEcografiaTV.Text = "Anormal";
                        txtDescripcionEcoAnormal.Text = Convert.ToString(jsonIMGinecologia[0]["RCE_ecogtafia_anormalIngreso_Medico_Ginecologia"]);
                    }

                    chbPipelleBiopsia.Checked = (Convert.ToString(jsonIMGinecologia[0]["RCE_pipelleIngreso_Medico_Ginecologia"]) == "SI");
                    chbPolipoBiopsia.Checked = (Convert.ToString(jsonIMGinecologia[0]["RCE_polipoIngreso_Medico_Ginecologia"]) == "SI");

                    break;

                case 3: // OBSTETRICIA

                    pnlObstetricia.Visible = true;
                    List<Dictionary<string, object>> jsonIMObstetricia = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Obstetricia"].ToString(), settings);
                    Dictionary<string, object> jsonTE = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonIMObstetricia[0]["RCE_Tipo_Embarazo"].ToString(), settings);

                    // 1. Antecedentes Gineco-Obstétrico

                    txtFechaUltimaReglaObstetricia.Text = jsonIMObstetricia[0]["RCE_fecha_ultima_reglaIngreso_Medico_Obstetricia"].ToString();
                    txtGestacionesObstetricia.Text = jsonIMObstetricia[0]["RCE_gestacionesIngreso_Medico_Obstetricia"].ToString();
                    txtPartosObstetricia.Text = jsonIMObstetricia[0]["RCE_gestacionesIngreso_Medico_Obstetricia"].ToString();
                    txtAbortosObstetricia.Text = jsonIMObstetricia[0]["RCE_gestacionesIngreso_Medico_Obstetricia"].ToString();

                    lblViaAereaDificilSi.CssClass += (Convert.ToString(jsonIMObstetricia[0]["RCE_operacionalIngreso_Medico_Obstetricia"]) == "SI") ? " badge-dark" : " badge-light";
                    lblViaAereaDificilNo.CssClass += (Convert.ToString(jsonIMObstetricia[0]["RCE_operacionalIngreso_Medico_Obstetricia"]) == "NO") ? " badge-dark" : " badge-light";

                    txtTipoEmbarazo.Text = jsonTE["RCE_nombreTipo_Embarazo"].ToString();
                    txtDescripcionEmbarazo.Text = jsonIMObstetricia[0]["RCE_descripcion_embarazoIngreso_Medico_Obstetricia"].ToString();
                    txtOtrosAntecedentes.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_otros_antecedentesIngreso_Medico_Obstetricia"]);

                    // 2. Exámenes Complementarios

                    Label lblExamenObstetrico = new Label();
                    lblExamenObstetrico.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_examen_fisico_obstetricoIngreso_Medico_Obstetricia"]);
                    pnlExamenFisicoObstetrico.Controls.Add(lblExamenObstetrico);

                    Label lblEcografiaObstetrica = new Label();
                    lblEcografiaObstetrica.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_ecografia_obstetricaIngreso_Medico_Obstetricia"]);
                    pnlEcografiaObstetrica.Controls.Add(lblEcografiaObstetrica);

                    txtRbns.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_rbneIngreso_Medico_Obstetricia"]);
                    txtDu.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_duIngreso_Medico_Obstetricia"]);
                    txtPerfilBiofisico.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_perfil_biofisicoIngreso_Medico_Obstetricia"]);
                    txtLaboratorio.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_laboratorioIngreso_Medico_Obstetricia"]);

                    // 3. Indicaciones

                    txtReposo.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_reposoIngreso_Medico_Obstetricia"]);
                    txtRegimen.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_regimenIngreso_Medico_Obstetricia"]);
                    txtPrescripciones.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_prescripcionIngreso_Medico_Obstetricia"]);
                    txtCsv.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_csvIngreso_Medico_Obstetricia"]);
                    txtControlObstetrico.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_control_obstetricoIngreso_Medico_Obstetricia"]);
                    txtHgt.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_hgtIngreso_Medico_Obstetricia"]);
                    txtExamenes.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_examenesIngreso_Medico_Obstetricia"]);
                    txtInterconsultas.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_interconsultasIngreso_Medico_Obstetricia"]);
                    txtProcedimientos.Text = Convert.ToString(jsonIMObstetricia[0]["RCE_procedimientosIngreso_Medico_Obstetricia"]);
                    
                    break;

                case 4: // Pediatrico

                    pnlPediatrico.Visible = true;
                    List<Dictionary<string, object>> imPediatria = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_Pediatrico"].ToString(), settings);

                    // CUIDADOR
                    if (Convert.ToString(imPediatria[0]["GEN_Personas2"]) != "")
                    {

                        Dictionary<string, object> jsonPersonasCuidador = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Personas2"].ToString(), settings);
                        Dictionary<string, object> jsonIdentificacionCuidador = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonPersonasCuidador["GEN_Identificacion"].ToString(), settings);

                        // INFORMACIÓN CUIDADOR
                        lblIdentificacionCuidador.Text = Convert.ToString(jsonIdentificacionCuidador["GEN_nombreIdentificacion"]);
                        txtNumeroDocCuidador.Text = Convert.ToString(jsonPersonasCuidador["GEN_numero_documentoPersonas"]) + 
                                                ((Convert.ToString(jsonPersonasCuidador["GEN_digitoPersonas"]) == "") ? "" : 
                                                    "-" + Convert.ToString(jsonPersonasCuidador["GEN_digitoPersonas"]));

                        txtNombreCompletoCuidador.Text = Convert.ToString(jsonPersonasCuidador["GEN_nombrePersonas"]) + " " +
                                                        Convert.ToString(jsonPersonasCuidador["GEN_apellido_paternoPersonas"]) + " " +
                                                        Convert.ToString(jsonPersonasCuidador["GEN_apellido_maternoPersonas"]);

                        txtParentescoCuidador.Text = Convert.ToString(imPediatria[0]["RCE_parentesco_cuidadorIngreso_Medico_Pediatrico"]);
                        txtTelefonoCuidador.Text = Convert.ToString(jsonPersonasCuidador["GEN_telefonoPersonas"]);
                        txtDireccionCuidador.Text = Convert.ToString(jsonPersonasCuidador["GEN_dir_callePersonas"]);

                    }

                    // 2. Antecedentes personales

                    if (Convert.ToString(imPediatria[0]["GEN_Establecimiento"]) != "")
                    {
                        Dictionary<string, object> jsonEstablecimiento = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Establecimiento"].ToString(), settings);
                        txtConsultorioOrigen.Text = jsonEstablecimiento["GEN_nombreEstablecimiento"].ToString();
                    }

                    if (Convert.ToString(imPediatria[0]["RCE_Tipo_Recien_Nacido"]) != "")
                    {
                        Dictionary<string, object> jsonTipoRecienNacido = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["RCE_Tipo_Recien_Nacido"].ToString(), settings);
                        txtTipoRecienNacido.Text = jsonTipoRecienNacido["RCE_nombreTipo_Recien_Nacido"].ToString() + "(" +
                                                    jsonTipoRecienNacido["RCE_descripcionTipo_Recien_Nacido"].ToString() + ")";
                    }
                    
                    if (Convert.ToString(imPediatria[0]["GEN_Tipo_Parto"]) != "")
                    {
                        Dictionary<string, object> jsonTipoParto = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Tipo_Parto"].ToString(), settings);
                        txtParto.Text = jsonTipoParto["GEN_nombreTipo_Parto"].ToString();
                    }

                    txtPatologiaPerinatal.Text = Convert.ToString(imPediatria[0]["RCE_patologia_perinatalIngreso_Medico_Pediatrico"]);
                    txtOtrosAntecedentesPersonales.Text = Convert.ToString(imPediatria[0]["RCE_otros_antecedentes_personalesIngreso_Medico_Pediatrico"]);

                    // VACUNAS
                    // lblVacunasPaciente
                    lblVacunasPaciente.Text =
                        "<div class='alert alert-secondary mt-2'>" +
                            "<i class='fa fa-exclamation-triangle'></i> El paciente no registra vacunas" +
                        "</div>";


                    if (Convert.ToString(imPediatria[0]["GEN_Tipo_Edad"]) != "")
                    {
                        Dictionary<string, object> jsonTipoEdad = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Tipo_Edad"].ToString(), settings);
                        txtEdadDuracionLMaterna.Text = Convert.ToString(imPediatria[0]["RCE_duracion_lactancia_añosIngreso_Medico_Pediatrico"]) + " " + 
                                                        jsonTipoEdad["GEN_nombreTipo_Edad"].ToString().Trim();
                    }

                    if (Convert.ToString(imPediatria[0]["GEN_Tipo_Edad1"]) != "")
                    {
                        Dictionary<string, object> jsonTipoEdad = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Tipo_Edad1"].ToString(), settings);
                        txtEdadInicioLArtificial.Text = Convert.ToString(imPediatria[0]["RCE_inicio_lactancia_añosIngreso_Medico_Pediatrico"]) + " " +
                                                        jsonTipoEdad["GEN_nombreTipo_Edad"].ToString().Trim();
                    }

                    txt1Comida.Text = (Convert.ToString(imPediatria[0]["RCE_fecha_primera_comidaIngreso_Medico_Pediatrico"]) == "") ? "" :
                            DateTime.Parse(imPediatria[0]["RCE_fecha_primera_comidaIngreso_Medico_Pediatrico"].ToString()).ToString("dd-MM-yyyy");
                    txt2Comida.Text = (Convert.ToString(imPediatria[0]["RCE_fecha_segunda_comidaIngreso_Medico_Pediatrico"]) == "") ? "" :
                            DateTime.Parse(imPediatria[0]["RCE_fecha_segunda_comidaIngreso_Medico_Pediatrico"].ToString()).ToString("dd-MM-yyyy");

                    lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("Alergias", Convert.ToString(imPediatria[0]["RCE_alergias_familiaresIngreso_Medico_Pediatrico"]));
                    lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("Asma", Convert.ToString(imPediatria[0]["RCE_asma_familiaresIngreso_Medico_Pediatrico"]));
                    lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("Epilepsia", Convert.ToString(imPediatria[0]["RCE_epilepsia_familiaresIngreso_Medico_Pediatrico"]));
                    lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("Diabetes", Convert.ToString(imPediatria[0]["RCE_diabetes_familiaresIngreso_Medico_Pediatrico"]));
                    lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("Cardiovasc.", Convert.ToString(imPediatria[0]["RCE_cardiovasc_familiaresIngreso_Medico_Pediatrico"]));
                    lblAntecedentesFamiliares.Text += GetBloqueAntecedenteClinico("HTA", Convert.ToString(imPediatria[0]["RCE_hta_familiaresIngreso_Medico_Pediatrico"]));

                    if (lblAntecedentesFamiliares.Text == "")
                        lblAntecedentesFamiliares.Visible = false;

                    txtMuertesFamiliares.Text = Convert.ToString(imPediatria[0]["RCE_muertes_familiaresIngreso_Medico_Pediatrico"]);
                    txtAlergiaFarmacosFamiliares.Text = Convert.ToString(imPediatria[0]["RCE_alergias_farmacos_familiaresIngreso_Medico_Pediatrico"]);
                    txtOtrosAntecendentesFamiliares.Text = Convert.ToString(imPediatria[0]["RCE_otros_familiaresIngreso_Medico_Pediatrico"]);

                    // Hospitalizaciones
                    // lblHospitalizacionesPaciente.Text = "";
                    lblHospitalizacionesPaciente.Text =
                        "<div class='alert alert-secondary'>" +
                            "<i class='fa fa-exclamation-triangle'></i> El paciente no registra hospitalizaciones." +
                        "</div>"; ;

                    // PARENTAL 1
                    if (Convert.ToString(imPediatria[0]["GEN_Personas"]) != "") {

                        Dictionary<string, object> jsonParental1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Personas"].ToString(), settings);
                        Dictionary<string, object> jsonIdentificacionParental1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental1["GEN_Identificacion"].ToString(), settings);

                        if (Convert.ToString(jsonParental1["GEN_Categoria_Ocupacion"]) != "") {
                            Dictionary<string, object> jsonActividadParental1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental1["GEN_Categoria_Ocupacion"].ToString(), settings);
                            txtActividadParental1.Text = Convert.ToString(jsonActividadParental1["GEN_descripcionCategorias_Ocupacion"]);
                        }

                        if (Convert.ToString(jsonParental1["GEN_Tipo_Escolaridad"]) != "") {
                            Dictionary<string, object> jsonEducacionParental1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental1["GEN_Tipo_Escolaridad"].ToString(), settings);
                            txtEducacionParental1.Text = Convert.ToString(jsonEducacionParental1["GEN_nombreTipo_Escolaridad"]);
                        }
                        
                        // INFORMACIÓN CUIDADOR
                        lblIdentificionParental1.Text = Convert.ToString(jsonIdentificacionParental1["GEN_nombreIdentificacion"]);
                        txtNumeroDocParental1.Text = Convert.ToString(jsonParental1["GEN_numero_documentoPersonas"]) +
                                                ((Convert.ToString(jsonParental1["GEN_digitoPersonas"]) == "") ? "" :
                                                    "-" + Convert.ToString(jsonParental1["GEN_digitoPersonas"]));

                        txtNombreParental1.Text = Convert.ToString(jsonParental1["GEN_nombrePersonas"]) + " " +
                                                        Convert.ToString(jsonParental1["GEN_apellido_paternoPersonas"]) + " " +
                                                        Convert.ToString(jsonParental1["GEN_apellido_maternoPersonas"]);
                        txtEdadParental1.Text = "";
                        
                    }
                    
                    // PARENTAL 2
                    if (Convert.ToString(imPediatria[0]["GEN_Personas1"]) != "")
                    {

                        Dictionary<string, object> jsonParental2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Personas1"].ToString(), settings);
                        Dictionary<string, object> jsonIdentificacionParental2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental2["GEN_Identificacion"].ToString(), settings);

                        if (Convert.ToString(jsonParental2["GEN_Categoria_Ocupacion"]) != "")
                        {
                            Dictionary<string, object> jsonActividadParental2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental2["GEN_Categoria_Ocupacion"].ToString(), settings);
                            txtActividadParental2.Text = Convert.ToString(jsonActividadParental2["GEN_descripcionCategorias_Ocupacion"]);
                        }

                        if (Convert.ToString(jsonParental2["GEN_Tipo_Escolaridad"]) != "")
                        {
                            Dictionary<string, object> jsonEducacionParental2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParental2["GEN_Tipo_Escolaridad"].ToString(), settings);
                            txtEducacionParental2.Text = Convert.ToString(jsonEducacionParental2["GEN_nombreTipo_Escolaridad"]);
                        }


                        // INFORMACIÓN CUIDADOR
                        lblIdentificionParental2.Text = Convert.ToString(jsonIdentificacionParental2["GEN_nombreIdentificacion"]);
                        txtNumeroDocParental2.Text = Convert.ToString(jsonParental2["GEN_numero_documentoPersonas"]) +
                                                ((Convert.ToString(jsonParental2["GEN_digitoPersonas"]) == "") ? "" :
                                                    "-" + Convert.ToString(jsonParental2["GEN_digitoPersonas"]));

                        txtNombreParental2.Text = Convert.ToString(jsonParental2["GEN_nombrePersonas"]) + " " +
                                                        Convert.ToString(jsonParental2["GEN_apellido_paternoPersonas"]) + " " +
                                                        Convert.ToString(jsonParental2["GEN_apellido_maternoPersonas"]);
                        txtEdadParental2.Text = "";

                    }

                    if (Convert.ToString(imPediatria[0]["GEN_Estado_Conyugal"]) != "")
                    {
                        Dictionary<string, object> jsonEstadoConyugal = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Estado_Conyugal"].ToString(), settings);
                        txtSituacionMarital.Text = jsonEstadoConyugal["GEN_descripcionEstado_Conyugal"].ToString();
                    }

                    // 6. OTROS DATOS

                    if (Convert.ToString(imPediatria[0]["RCE_Tipo_Desarrollo_Psico_Motor"]) != "")
                    {
                        Dictionary<string, object> jsonpSpicoMotor = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["RCE_Tipo_Desarrollo_Psico_Motor"].ToString(), settings);
                        txtDSMOtrosDatos.Text = jsonpSpicoMotor["RCE_nombreTipo_Desarrollo_Psico_Motor"].ToString();
                    }

                    if (Convert.ToString(imPediatria[0]["GEN_Tipo_Datos_Nutricionales"]) != "")
                    {
                        Dictionary<string, object> jsonTipoDatosNutricionales = JsonConvert.DeserializeObject<Dictionary<string, object>>(imPediatria[0]["GEN_Tipo_Datos_Nutricionales"].ToString(), settings);
                        txtNutricionalOtrosDatos.Text = jsonTipoDatosNutricionales["GEN_nombreTipo_Datos_Nutricionales"].ToString();
                    }
                    
                    break;

                case 6: // UCI

                    pnlInfoTipoIngreos.Visible = false;
                    pnlUCI.Visible = true;
                    List<Dictionary<string, object>> jsonIMUCI = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RCE_Ingreso_Medico_UCI"].ToString(), settings);
                    Dictionary<string, object> jsonEscalaRakin = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonIMUCI[0]["GEN_Escala_Rankin"].ToString(), settings);
                    txtEscalaRakin.Text = jsonEscalaRakin["GEN_codigoEscala_Rankin"].ToString() + " - " + jsonEscalaRakin["GEN_nombreEscala_Rankin"].ToString();
                    txtApache2.Text = Convert.ToString(jsonIMUCI[0]["RCE_apache2Ingreso_Medico_UCI"]);
                    lblViaAereaDificilSi.CssClass += (Convert.ToString(jsonIMUCI[0]["RCE_via_aerea_dificilIngreso_Medico_UCI"]) == "SI") ? " badge-dark" : " badge-light";
                    lblViaAereaDificilNo.CssClass += (Convert.ToString(jsonIMUCI[0]["RCE_via_aerea_dificilIngreso_Medico_UCI"]) == "NO") ? " badge-dark" : " badge-light";

                    if(txtApache2.Text != "")
                        txtMortalidad.Text = GetPorcentajeMortalidad(int.Parse(txtApache2.Text));

                    break;

                default:

                    pnlInfoTipoIngreos.Visible = false;
                    break;

            }
            

            // DATOS DEL PROFESIONAL
            lblProfesional.Text = Convert.ToString(des["GEN_nombrePersonasProfesional"]) 
                                    + " " + Convert.ToString(des["GEN_apellido_paternoPersonasProfesional"]) 
                                    + ((Convert.ToString(des["GEN_apellido_maternoPersonasProfesional"]) != "") ? " " + Convert.ToString(des["GEN_apellido_maternoPersonasProfesional"]) : "")
                                    + " / " + Convert.ToString(des["GEN_numero_documentoPersonasProfesional"]);

            string sName = string.Join(" ", new string[] { "INGRESO_MEDICO", DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") });
            string sUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            Funciones.ExportarPDF(sUrl, this.Page, sName, des["LoginUsuario"].ToString());
            */
        }

        private string GetPorcentajeMortalidad(int puntuacion)
        {
            
            try
            {

                if (puntuacion >= 0 && puntuacion <= 4)
                    return "4 % mortalidad";
                else if (puntuacion >= 5 && puntuacion <= 9)
                    return "8 % mortalidad";
                else if (puntuacion >= 10 && puntuacion <= 14)
                    return "15 % mortalidad";
                else if (puntuacion >= 15 && puntuacion <= 19)
                    return "25 % mortalidad";
                else if (puntuacion >= 20 && puntuacion <= 24)
                    return "40 % mortalidad";
                else if (puntuacion >= 25 && puntuacion <= 29)
                    return "55 % mortalidad";
                else if (puntuacion >= 30 && puntuacion <= 34)
                    return "75 % mortalidad";
                else if (puntuacion > 34)
                    return "85 % mortalidad";    

            }
            catch (Exception ex)
            {
                string fail = ex.Message + " " + ex.StackTrace;
            }

            return "Puntuación Invalida.";

        }

        private string GetBloqueAntecedenteClinico(string nombreAntecedenteClinico, string antecedenteClinico)
        {
            return  (antecedenteClinico != "") ? string.Format(" <strong>{0}: </strong> {1} ", nombreAntecedenteClinico, antecedenteClinico) : "";
        }

        public string GetHost()
        {
            return Funciones.GetHost(HttpContext.Current);
        }

    }
}