﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/Impresion.Master" AutoEventWireup="true" CodeBehind="ImprimirIngresoMedico.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.ImprimirIngresoMedico" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link rel="stylesheet" href="<%= GetHost() %>/Style/Bootstrap_3_3_7/bootstrap.min.css" />
    <link rel="stylesheet" href="<%= GetHost() %>/Style/impresion.css" />
    <script type="text/javascript" src="<%= GetHost() %>/Script/Bootstrap_3_3_7/bootstrap.min.js"></script>
    <style>
        
        .md-form {
            margin-top: 20px !important;
            margin-bottom: 0px !important;
        }

        .form-control {
            min-height: 35px;
        }

    </style>
    <script type="text/javascript">

        $(document).ready(function () {

            var urlParams = window.location.search;
            var getQuery = urlParams.split('?')[1];
            var idIM = getQuery.split('id=')[1];
            var json = {};

            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
                async: false,
                headers: { 'Authorization': GetToken() },
                success: function (data) {
                    json.LoginUsuario = data[0].GEN_loginUsuarios;
                }
            });

            console.log("RCE_Ingreso_Medico/" + idIM);
            console.log("Token >" + GetToken());
            /*
            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + "RCE_Ingreso_Medico/" + idIM,
                async: false,
                success: function (data) {
                    
                    if (data.length > 0) {

                        $.ajax({
                            type: 'GET',
                            url: GetWebApiUrl() + "RCE_Ingreso_Medico_Diagnostico_CIE10/RCE_idIngreso_Medico/" + idIM,
                            async: false,
                            headers: { 'Authorization': GetToken() },
                            success: function (d) {
                                data[0].diagnosticos = d; 
                            }
                        });
                    
                        data[0].edad = (data[0].GEN_fec_nacimientoPaciente != null) ? CalcularEdad(data[0].GEN_fec_nacimientoPaciente) : '';
                        data[0].GEN_fec_nacimientoPaciente = (data[0].GEN_fec_nacimientoPaciente != null)
                                                                ? moment(moment(data[0].GEN_fec_nacimientoPaciente).toDate()).format('DD-MM-YYYY')
                                                                : '';
                        data[0].LoginUsuario = json.LoginUsuario;

                        __doPostBack("<%= btnHidden.UniqueID %>", JSON.stringify(data[0]));
                    }
                }
            });
            */
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <asp:Button runat="server" ID="btnHidden" OnClick="btnHidden_Click" style="display:none;" />
    <div class="row">
        <div class="col-xs-4 text-left">
            <img src="<%= GetHost() %>/Style/IMG/MINSAL.jpg" style="width: 100px;" />
        </div>
        <div class="col-xs-4 text-center">
            <img src="<%= GetHost() %>/Style/IMG/ACRLOGO.jpg" style="width: 100px;" />
        </div>
        <div class="col-xs-4 text-right">
            <img src="<%= GetHost() %>/Style/IMG/HOSPLOGO.jpg" style="width: 100px;" />
        </div>
    </div>
    <div style="padding: 10px 10px 10px 10px;">
        <h4 class="text-center">
            <strong>INGRESO MÉDICO</strong>
        </h4>
    </div>
    <table style="width: 100%;">
        <thead>
        </thead>
        <tbody>
            <tr>
                <td colspan="12" class="contenido-celda" style="text-align: right; border: none;">
                    Fecha de IM > <asp:Label ID="lblFechaCreacion" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <table style="width: 100%;">
                <thead>
                    <tr><th colspan="12" class="estilo-titulo contenido-celda">
                        Datos de Paciente
                    </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan=3 class="contenido-celda">
                            <asp:Label ID="lblidentificacion" runat="server" Font-Bold="true">
                            </asp:Label>
                            <asp:Label ID="txtNumeroDocumentoPaciente" placeholder="&nbsp;" runat="server" >
                            </asp:Label>
                        </td>
                        <td colspan=9 class="contenido-celda">
                            <strong>Nombre Completo</strong>
                            <asp:Label ID="txtNombrePaciente" placeholder="&nbsp;" runat="server" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=3 class="contenido-celda">
                            <strong>Fecha de Nacimiento</strong>
                            <asp:Label placeholder="&nbsp;" runat="server" ID="txtFechaNacimiento" >
                            </asp:Label>    
                        </td>
                        <td colspan=3 class="contenido-celda">
                            <strong>Edad</strong>
                            <asp:Label placeholder="&nbsp;" runat="server" ID="txtEdad" >
                            </asp:Label>
                        </td>
                        <td colspan=6 class="contenido-celda">
                            <strong>Dirección</strong>
                            <asp:Label placeholder="&nbsp;" runat="server" ID="txtDireccion" >
                            </asp:Label>
                        </td>                        
                    </tr>
                    <tr>
                        <td colspan=2 class="contenido-celda">
                            <strong>Sexo</strong>
                            <asp:Label placeholder="&nbsp;" runat="server" ID="txtSexo" >
                            </asp:Label>
                        </td>
                        <td colspan=2 class="contenido-celda">
                            <strong>Genero</strong>
                            <asp:Label placeholder="&nbsp;" runat="server" ID="txtGenero" >
                            </asp:Label>
                        </td>
                        <td colspan=3 class="contenido-celda">
                            <strong>Teléfono</strong> 
                            <asp:Label placeholder="&nbsp;" ID="txtTelefono" runat="server" >
                            </asp:Label>
                        </td>
                        <td colspan=3 class="contenido-celda">
                            <strong>Otro teléfono</strong>
                            <asp:Label placeholder="&nbsp;" ID="txtOtroTelefono" runat="server" >
                            </asp:Label>
                        </td>
                    </tr>
                </tbody>
            </table>
            <asp:Panel ID="pnlUCI" runat="server" Visible="false">
                <table style="width: 100%;">
                    <thead>
                        <tr>
                            <th colspan="12" class="estilo-titulo contenido-celda">
                                Datos de UCI
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan=4 class="contenido-celda">
                                <strong>Escala rankin</strong>
                                <asp:Label ID="txtEscalaRakin" runat="server"  placeholder="&nbsp;">
                                </asp:Label>
                            </td>
                            <td colspan=3 class="contenido-celda">
                                <strong>Apache 2</strong>
                                <asp:Label ID="txtApache2" runat="server"  placeholder="&nbsp;">
                                </asp:Label>
                            </td>
                            <td colspan=2 class="contenido-celda">
                                <strong>Mortalidad</strong>
                                <asp:Label ID="txtMortalidad" runat="server"  placeholder="&nbsp;">
                                </asp:Label>
                            </td>
                            <td colspan=3 class="contenido-celda">
                                <strong>Vía aerea dificil:</strong> 
                                <asp:Label ID="lblViaAereaDificilSi" runat="server" CssClass="badge badge-pill">SI</asp:Label> / 
                                <asp:Label ID="lblViaAereaDificilNo" runat="server" CssClass="badge badge-pill">NO</asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            
            <asp:Panel ID="pnlAntecedentesClinicos" runat="server" CssClass="card">
                <table style="width: 100%;">
                    <thead>
                        <tr><th colspan="12" class="estilo-titulo contenido-celda">
                            Antecedentes Clínicos
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan=12 class="contenido-celda">
                                <asp:Panel runat="server" BorderWidth="1px" BorderColor="Silver"
                                    style="padding: 5px !important;">
                                    <asp:Label ID="lblAntecedentesClinicos" runat="server">
                                    </asp:Label>
                                </asp:Panel>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <table style="width: 100%;">
                <thead>
                    <tr>
                        <th colspan="12" class="estilo-titulo contenido-celda">
                            Datos de Ingreso Médico
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan=3 class="contenido-celda">
                            <strong>Fecha de Ingreso Medico</strong>
                            <asp:Label ID="txtFechaIngresoMedico" runat="server"  placeholder="&nbsp;"> 
                            </asp:Label>
                        </td>
                        <td colspan=9 class="contenido-celda">
                            <strong>Servicio</strong>
                            <asp:Label ID="txtUbicacion" runat="server" 
                                placeholder="&nbsp;">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=12 class="contenido-celda">
                            <strong>Motivo de Solicitud</strong>
                            <asp:Label ID="txtMotivoSolicitud" runat="server"
                                placeholder="&nbsp;" TextMode="MultiLine" Text="Motivo Solicitud">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=12 class="contenido-celda">
                            <strong>Anamnesis</strong>
                            <br/>
                            <asp:Label ID="txtAnamnesis" runat="server"
                                TextMode="MultiLine" Text="Anamnesis" placeholder="&nbsp;">
                            </asp:Label>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlExamenFisico" runat="server" CssClass="row mt-2">
                        <table style="width: 100%;">
                            <tr>
                                <td colspan=12 class="contenido-celda">
                                    <strong>Examen físico general</strong>
                                    <div style="border: 1px solid Silver; padding: 5px !important;">
                                        <asp:Label ID="lblExamenFisico" runat="server">
                                        </asp:Label>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <tr>
                        <td colspan=12 class="contenido-celda">
                            <strong>Examen segmentario</strong> 
                            <asp:Panel ID="pnlExamenSegmentario" runat="server" BorderWidth="1px" BorderColor="Silver"
                                style="padding: 5px !important;">
                                <asp:Label ID="lblExamenSegmentario" runat="server">
                                </asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="12">
                            <h6 class="mt-3 mb-3"><strong>Hipotesis diagnóstica o Diagnóstico de Ingreso</strong></h6>
                        </td>

                    </tr>
                    <tr>
                        <td colspan=12 class="contenido-celda">
                            <strong>Diagnóstico</strong>
                            <asp:Label ID="txtDiagnostico" runat="server" 
                                 placeholder="&nbsp;" TextMode="MultiLine">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=12 class="contenido-celda">
                            <asp:Literal ID="ltlDiagnosticosCIE10" runat="server">
                            </asp:Literal>
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=12 class="contenido-celda">
                            <strong>Plan de Manejo</strong>
                            <asp:Label ID="txtPlanManejo" runat="server" CssClass="text-center" 
                                placeholder="&nbsp;" TextMode="MultiLine" Text="Descripción de Plan de Manejo">
                            </asp:Label>
                        </td>
                    </tr>
                </tbody>
            </table>
            <asp:Panel ID="pnlInfoTipoIngreos" runat="server" CssClass="card">
            <table style="width: 100%;">
                <thead>
                    <tr><th colspan="12" class="fondo-titulos contenido-celda">
                        <h5><strong><u>Datos de <asp:Label ID="lblTipoIngresoMedico" runat="server"></asp:Label></u></strong></h5>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Panel ID="pnlObstetricia" runat="server" Visible="false">
                        <tr><th colspan="12" class="fondo-titulos contenido-celda">
                            <h5 class="mb-3 mt-3"><strong>1. Antecedentes Gineco-Obstétrico</strong></h5></th>
                        </tr>
                        <tr>
                            <td colspan=2 class="contenido-celda">
                                <strong>FUR</strong>
                                <asp:Label ID="txtFechaUltimaReglaObstetricia" runat="server" type="date" >
                                </asp:Label>
                            </td>
                            <table style="width: 100%;">
                                <tr class="contenido-celda">
                                    <td colspan=4 class="contenido-celda">
                                        <strong>G</strong>
                                        <asp:Label ID="txtGestacionesObstetricia" runat="server" >
                                        </asp:Label>
                                    </td>
                                    <td colspan=4 class="contenido-celda">
                                        <strong>P</strong>
                                        <asp:Label ID="txtPartosObstetricia" runat="server" >
                                        </asp:Label>
                                    </td>
                                    <td colspan=4 class="contenido-celda">
                                        <strong>A</strong>
                                        <asp:Label ID="txtAbortosObstetricia" runat="server" >
                                        </asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <td colspan=2 class="contenido-celda">
                                <strong>Operacional:</strong> <br />
                                <asp:Label ID="lblOperacionalSi" runat="server" CssClass="badge badge-pill">SI</asp:Label> / 
                                <asp:Label ID="lblOperacionalNo" runat="server" CssClass="badge badge-pill">NO</asp:Label>
                            </td>
                            <td colspan=2 class="contenido-celda">
                                <strong>Embarazo actual</strong>
                                <asp:Label ID="txtTipoEmbarazo" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=3 class="contenido-celda">    
                                <strong>Embarazo actual</strong>
                                <asp:Label ID="txtDescripcionEmbarazo" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=6 class="contenido-celda">
                                <strong>Otros antecedentes</strong>
                                <asp:Label ID="txtOtrosAntecedentes" runat="server" CssClass="md-textarea md-textarea-auto form-control" TextMode="MultiLine">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=12 class="contenido-celda">
                                <strong>Examen Físico Gineco-Obstétrico</strong>
                                <asp:Panel ID="pnlExamenFisicoObstetrico" runat="server" BorderWidth="1px" BorderColor="Silver"
                                    style="padding: 5px !important;">
                                        <asp:Literal ID="ltlExamenGinecoObstetrico" runat="server">
                                        </asp:Literal>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" class="fondo-titulos contenido-celda"><h5 class="mb-3 mt-3"><strong>2. Exámenes Complementarios</strong></h5></td>
                        </tr>
                        <tr>
                            <td colspan=12 class="contenido-celda">
                                <strong>Ecografía Obstétrica</strong>
                                <asp:Panel ID="pnlEcografiaObstetrica" runat="server" BorderWidth="1px" BorderColor="Silver"
                                    style="padding: 5px !important;">
                                        <asp:Literal ID="ltlEcografiaObstetrica" runat="server">
                                        </asp:Literal>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2 class="contenido-celda">
                                <strong>RBNS</strong>
                                <asp:Label ID="txtRbns" runat="server"  >
                                </asp:Label>
                            </td>
                            <td colspan=2 class="contenido-celda">
                                <strong>DU</strong>
                                <asp:Label ID="txtDu" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=2 class="contenido-celda">
                                <strong>Perfil Biofísico</strong>
                                <asp:Label id="txtPerfilBiofisico" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=6 class="contenido-celda">
                                <strong>Laboratorio</strong>
                                <asp:Label ID="txtLaboratorio" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan ="12" class="fondo-titulos contenido-celda"><h5 class="mb-3 mt-3"><strong>3. Indicaciones</strong></h5></td>
                        </tr>
                        <tr>
                            <td colspan=6 class="contenido-celda">
                                <strong>Reposo</strong>
                                <asp:Label ID="txtReposo" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=6 class="contenido-celda">
                                <strong>Régimen</strong>
                                <asp:Label ID="txtRegimen" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=12 class="contenido-celda">
                                <strong>Prescripciones</strong>
                                <asp:Label ID="txtPrescripciones" runat="server" CssClass="md-textarea md-textarea-auto form-control" TextMode="MultiLine">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2 class="contenido-celda">
                                <strong>CSV</strong>
                                <asp:Label ID="txtCsv" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=4 class="contenido-celda">
                                <strong>Control Obstétrico</strong>
                                <asp:Label id="txtControlObstetrico" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=6 class="contenido-celda">
                                <strong>HGT</strong>
                                <asp:Label ID="txtHgt" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=12 class="contenido-celda">
                                <strong>Exámenes</strong>
                                <asp:Label ID="txtExamenes" runat="server" CssClass="md-textarea md-textarea-auto form-control" TextMode="MultiLine">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=12 class="contenido-celda">
                                <strong>Prestaciones</strong>
                                <asp:Label ID="txtInterconsultas" runat="server" CssClass="md-textarea md-textarea-auto form-control" 
                                    TextMode="MultiLine">
                                </asp:Label>    
                            </td>
                        </tr>
                        <tr>
                            <td colspan=12 class="contenido-celda">
                                <strong>Procedimientos</strong>
                                <asp:Label ID="txtProcedimientos" runat="server" CssClass="md-textarea md-textarea-auto form-control" 
                                    TextMode="MultiLine">
                                </asp:Label>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="pnlGinecologia" runat="server" Visible="false">
                        <tr>
                            <td colspan=2 class="contenido-celda">
                                <strong>FUR</strong>
                                <asp:Label ID="txtFechaUltimaRegla" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=2 class="contenido-celda">
                                <strong>Método anticonceptivo</strong>
                                <asp:Label ID="txtMetodoConceptivo" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=2 class="contenido-celda">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan=4 class="contenido-celda">
                                            <strong>G</strong>
                                            <asp:Label id="txtGestaciones" runat="server" >
                                            </asp:Label>
                                        </td>
                                        <td colspan=4 class="contenido-celda">
                                            <strong>P</strong>
                                            <asp:Label ID="txtPartos" runat="server" >
                                            </asp:Label>
                                        </td>
                                        <td colspan=4 class="contenido-celda">
                                            <strong>A</strong>
                                            <asp:Label id="txtAbortos"  runat="server" >
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td colspan=6 class="contenido-celda">
                                <strong>Ciclos</strong>
                                <asp:Label id="txtCiclos" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=6 class="contenido-celda">
                                <strong>Papanicolaou</strong>
                                <asp:Label id="txtPapanicolaou" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=6 class="contenido-celda">
                                <strong>Mamografía</strong>
                                <asp:Label id="txtMamografia" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=4 class="contenido-celda">
                                <strong>Ecografía TV: </strong><br/>
                                <asp:Label id="txtEcografiaTV" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan="4" class="contenido-celda">
                                <asp:Panel ID="pnlAnornal" runat="server" CssClass="col-md-4">
                                    <strong>Descripción Ecografía Anormal</strong><br/>
                                    <asp:Label id="txtDescripcionEcoAnormal" runat="server"  >
                                    </asp:Label>
                                </asp:Panel>
                            </td>
                            <td colspan=4>
                                <div class="mt-3">
                                    <strong class="mr-4">Biopsia</strong>
                                    <asp:CheckBox ID="chbPipelleBiopsia" runat="server" Font-Bold="true">
                                    </asp:CheckBox>
                                    <strong class="mr-3">Pipelle</strong>
                                    <asp:CheckBox id="chbPolipoBiopsia" runat="server" Font-Bold="true">
                                    </asp:CheckBox>
                                    <strong>Pólipo</strong>
                                </div>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="pnlPediatrico" runat="server" Visible="false">
                        <tr>
                            <td colspan="12" class="fondo-titulos contenido-celda">
                                <h5><strong>1. Datos de responsable</strong></h5>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=3 class="contenido-celda">
                                <asp:Label ID="lblIdentificacionCuidador" runat="server" Font-Bold="true">
                                </asp:Label>
                                <asp:Label ID="txtNumeroDocCuidador" placeholder="&nbsp;" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=9 class="contenido-celda">
                                <strong>Nombre Completo</strong>
                                <asp:Label ID="txtNombreCompletoCuidador" placeholder="&nbsp;" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=3 class="contenido-celda">
                                <strong>Parentesco</strong>
                                <asp:Label ID="txtParentescoCuidador" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=3 class="contenido-celda">
                                <strong>Teléfono</strong>
                                <asp:Label ID="txtTelefonoCuidador" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=6 class="contenido-celda">
                                <strong>Dirección</strong>
                                <asp:Label ID="txtDireccionCuidador" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" class="fondo-titulos contenido-celda">
                                <h5 class="mb-3 mt-3"><strong>2. Antecedentes personales</strong></h5>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=6 class="contenido-celda">
                                <strong>Consultorio de origen</strong>
                                <asp:Label ID="txtConsultorioOrigen" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=6 class="contenido-celda">
                                <strong>Tipo recién nacido</strong>
                                <asp:Label ID="txtTipoRecienNacido" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2 class="contenido-celda">
                                <strong>Parto</strong>
                                <asp:Label ID="txtParto" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan= 5>
                                <strong>Patología perinatal</strong>
                                <asp:Label ID="txtPatologiaPerinatal" runat="server" CssClass="md-textarea md-textarea-auto form-control" 
                                    TextMode="MultiLine">
                                </asp:Label>    
                            </td>
                            <td colspan= 5>
                                <strong>Otros</strong>
                                <asp:Label ID="txtOtrosAntecedentesPersonales" runat="server" CssClass="md-textarea md-textarea-auto form-control" 
                                    TextMode="MultiLine">
                                </asp:Label> 
                            </td>
                        </tr>
                        <tr>
                            <td colspan=12 class="contenido-celda">
                                <strong>Vacunas</strong>    
                                <asp:Literal ID="lblVacunasPaciente" runat="server">
                                </asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=3 class="contenido-celda">
                                <strong>Duración L. materna (Edad)</strong>
                                <asp:Label ID="txtEdadDuracionLMaterna" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=3 class="contenido-celda">
                                <strong>Inicio L. artificial (Edad)</strong>
                                <asp:Label ID="txtEdadInicioLArtificial" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=3 class="contenido-celda">
                                <strong>Fecha 1° comida</strong>
                                <asp:Label ID="txt1Comida" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=3 class="contenido-celda">
                                <strong>Fecha 2° comida</strong>
                                <asp:Label ID="txt2Comida" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" class="fondo-titulos contenido-celda">
                                <h5 class="mb-3 mt-3"><strong>3. Antecedentes familiares</strong></h5>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=12 class="contenido-celda">
                                <asp:Panel runat="server" BorderWidth="1px" BorderColor="Silver" style="padding: 5px !important;">
                                    <asp:Label ID="lblAntecedentesFamiliares" runat="server">
                                    </asp:Label>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=4 class="contenido-celda">
                                <strong>Muertes familiares</strong>
                                <asp:Label ID="txtMuertesFamiliares" runat="server" TextMode="MultiLine">
                                </asp:Label> 
                            </td>
                            <td colspan=4 class="contenido-celda">
                                <strong>Alergía a fármacos</strong>
                                <asp:Label ID="txtAlergiaFarmacosFamiliares" runat="server" TextMode="MultiLine">
                                </asp:Label> 
                            </td>
                            <td colspan=4 class="contenido-celda">
                                <strong>Otros</strong>
                                <asp:Label ID="txtOtrosAntecendentesFamiliares" runat="server" TextMode="MultiLine">
                                </asp:Label> 
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" class="fondo-titulos contenido-celda">
                                <h5 class="mb-3 mt-3"><strong>4. Hospitalización</strong></h5>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=12 class="contenido-celda">  
                                <asp:Literal ID="lblHospitalizacionesPaciente" runat="server">
                                </asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" class="fondo-titulos contenido-celda">
                                <h5 class="mb-3 mt-3"><strong>5. Información parental</strong></h5>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" class="fondo-titulos contenido-celda">
                                <h6 class="mb-2 mt-2"><strong>5.1. Información parental</strong></h6>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=3 class="contenido-celda">
                                <asp:Label ID="lblIdentificionParental1" runat="server" Font-Bold="true">
                                </asp:Label>
                                <asp:Label ID="txtNumeroDocParental1" placeholder="&nbsp;" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=9 class="contenido-celda">
                                <strong>Nombre Completo</strong>
                                <asp:Label ID="txtNombreParental1" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=4 class="contenido-celda">
                                <strong>Edad</strong>
                                <asp:Label ID="txtEdadParental1" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=4 class="contenido-celda">
                                <strong>Actividad</strong>
                                <asp:Label ID="txtActividadParental1" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=4 class="contenido-celda">
                                <strong>Educación</strong>
                                <asp:Label ID="txtEducacionParental1" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" class="fondo-titulos contenido-celda">
                                <h6 class="mb-2 mt-2"><strong>5.2. Información parental</strong></h6>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=3 class="contenido-celda">
                                <asp:Label ID="lblIdentificionParental2" runat="server" Font-Bold="true">
                                </asp:Label>
                                <asp:Label ID="txtNumeroDocParental2" placeholder="&nbsp;" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=9 class="contenido-celda">
                                <strong>Nombre Completo</strong>
                                <asp:Label ID="txtNombreParental2" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=4 class="contenido-celda">
                                <strong>Edad</strong>
                                <asp:Label ID="txtEdadParental2" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=4 class="contenido-celda">
                                <strong>Actividad</strong>
                                <asp:Label ID="txtActividadParental2" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=4 class="contenido-celda">
                                <strong>Educación</strong>
                                <asp:Label ID="txtEducacionParental2" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=3 class="contenido-celda">
                                <strong>Situación marital</strong>
                                <asp:Label ID="txtSituacionMarital" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" class="fondo-titulos contenido-celda">
                                <h5 class="mb-3 mt-3"><strong>6. Otros datos</strong></h5>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=3 class="contenido-celda">
                                <strong>DSM</strong>
                                <asp:Label ID="txtDSMOtrosDatos" runat="server" >
                                </asp:Label>
                            </td>
                            <td colspan=3 class="contenido-celda">
                                <strong>Nutricional</strong>
                                <asp:Label ID="txtNutricionalOtrosDatos" runat="server" >
                                </asp:Label>
                            </td>
                        </tr>
                    </asp:Panel>
                </tbody>
            </table>
            </asp:Panel>
        </tbody>
    </table>
    <div class="text-center" style="margin-top: 50px;">
        <strong style="border-top: 2px solid black; padding: 0px 50px 0px 50px;">
            FIRMA: 
            <asp:Label runat="server" ID="lblProfesional" Text="Exequiel Flores Arriagada / 18327192-k">
            </asp:Label>
        </strong>
    </div>
</asp:Content>
