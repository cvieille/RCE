﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;

namespace RegistroClinico.Vista.ModuloHOS
{
    public partial class VistaMapaCamas : Handlers.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnExportarMovHospitalizacion_Click(object sender, EventArgs e)
        {
            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.MissingMemberHandling = MissingMemberHandling.Ignore;

                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);

                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["ID"] = des[i][0].ToString(),
                        ["Fecha Movimiento"] = des[i][1].ToString(),
                        ["Usuario"] = des[i][2].ToString(),
                        ["Movimiento"] = des[i][3].ToString()
                    };
                    dic.Add(d);
                }

                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Movimientos Hospitalizacion", dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Movimientos Hospitalizacion", workbook);

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al exportar Excel de bandeja de hospitalización.", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }

        }

        protected void btnExportarMovTrasladosH_Click(object sender, EventArgs e)
        {

            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.MissingMemberHandling = MissingMemberHandling.Ignore;

                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);

                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["ID"] = des[i][0].ToString(),
                        ["Fecha Traslado"] = des[i][1].ToString(),
                        ["Cama Origen"] = des[i][2].ToString(),
                        ["Cama Destino"] = des[i][3].ToString()
                    };
                    dic.Add(d);
                }

                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Movimientos Traslados", dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Movimientos Traslados", workbook);

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al exportar Excel de movimientos de traslados.", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }

        }
    }
}