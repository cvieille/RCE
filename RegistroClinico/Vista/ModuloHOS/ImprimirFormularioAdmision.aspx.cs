﻿using Newtonsoft.Json;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;
using System.Web;

namespace RegistroClinico.Vista.ModuloHOS
{
    public partial class ImprimirFormularioAdmision : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NegSession negSession = new NegSession(Page);
            negSession.ValidarPermisosSession();
            HttpContext context = HttpContext.Current;
            string dominio = Funciones.GetUrlWebApi();
            string idHos = Request.QueryString["id"];

            // ${GetWebApiUrl()}
            string url = string.Format("{0}HOS_Hospitalizacion/{1}", dominio, idHos);
            string token = string.Format("Bearer {0}", Session["TOKEN"]);
            string usuarioLogeado = Session["LOGIN_USUARIO"].ToString();
            string sJson = Funciones.ObtenerJsonWebApi(url, token);
            //string value = "";
            object O = new { };
            List<DiccionarioPersonalizado<string, string>> arregloDiccionarios;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<Dictionary<string, object>> list = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(sJson, settings);
            Dictionary<string, object> json = list[0];

            DiccionarioPersonalizado<string, string> paciente;
            DiccionarioPersonalizado<string, string> ingreso;
            DiccionarioPersonalizado<string, string> egreso;

            /* Nota: Extracción de objeto en arreglo de largo 1
            List<DiccionarioPersonalizado<string, string>> arregloDiccionarios;
            if (json.TryGetValue("Paciente", out O)) {
                arregloDiccionarios = JsonConvert.DeserializeObject<List<DiccionarioPersonalizado<string, string>>>(O.ToString(), settings);
                paciente = arregloDiccionarios[0];
            } else {
                paciente = new DiccionarioPersonalizado<string, string>();
            }
            */

            paciente = json.TryGetValue("Paciente", out O)  ? JsonConvert.DeserializeObject<DiccionarioPersonalizado<string, string>>(O.ToString(), settings) : new DiccionarioPersonalizado<string, string>();
            ingreso = json.TryGetValue("Ingreso", out O) ? JsonConvert.DeserializeObject<DiccionarioPersonalizado<string, string>>(O.ToString(), settings) : new DiccionarioPersonalizado<string, string>();
            egreso = json.TryGetValue("Egreso", out O) ? JsonConvert.DeserializeObject<DiccionarioPersonalizado<string, string>>(O.ToString(), settings) : new DiccionarioPersonalizado<string, string>();
            
            //Paciente
            lblIdHospitalizacion.Text = idHos;
            lblNumeroDocumentoPaciente.Text = paciente.obtenerValor("NumeroDocumento") +"-"+ paciente.obtenerValor("Digito");
            lblNumeroDocumento.Text = paciente.obtenerValor("DescripcionIdentificacion");
            lblNombresPaciente.Text = paciente.obtenerValor("Nombre") + " " + paciente.obtenerValor("ApellidoPaterno")  + " " + paciente.obtenerValor("ApellidoMaterno");
            lblCorreoPaciente.Text = paciente.obtenerValor("Email");
            lblNuiPaciente.Text = paciente.obtenerValor("Nui");
            lblSexo.Text = paciente.obtenerValor("NombreSexo");
            lblGenero.Text = paciente.obtenerValor("NombreGenero");
            lblNombreSocial.Text = paciente.obtenerValor("NombreSocial");

            if (paciente["FechaNacimiento"] != null)
            {
                lblFechaNacimiento.Text = DateTime.Parse(paciente["FechaNacimiento"].ToString()).ToString("dd-MM-yyyy");
                string sEdad = Funciones.CalculaEdad(Convert.ToString(paciente["FechaNacimiento"]));
                Dictionary<string, string> jEdad = JsonConvert.DeserializeObject<Dictionary<string, string>>(sEdad);
                lblEdad.Text = jEdad["edad"].ToString();
            }

            lblDireccion.Text = paciente.obtenerValor("DireccionCalle") + " "+ paciente.obtenerValor("DireccionNumero");
            lblTelefono.Text = paciente.obtenerValor("Telefono");
            lblOtroTelefono.Text = paciente.obtenerValor("OtrosFonos");
            lblNacionalidad.Text = paciente.obtenerValor("DescripcionNacionalidad");
            lblCiudad.Text = paciente.obtenerValor("NombreCiudad");
            lblNivelEducacional.Text = paciente.obtenerValor("DescripcionNivelEducacional");
            lblPuebloOriginario.Text = paciente.obtenerValor("DescripcionPuebloOriginario");
            //Ocupación
            DiccionarioPersonalizado<string, string> ocupaciones;
            ocupaciones = json.TryGetValue("Ocupaciones", out O) ? JsonConvert.DeserializeObject<DiccionarioPersonalizado<string, string>>(O.ToString(), settings) : new DiccionarioPersonalizado<string, string>();
            lblOcupacion.Text = ocupaciones.obtenerValor("Valor");
            //Categoria Ocupacional
            DiccionarioPersonalizado<string, string> categoriaOcupacional;
            categoriaOcupacional = json.TryGetValue("CategorizacionOcupacion", out O) ? JsonConvert.DeserializeObject<DiccionarioPersonalizado<string, string>>(O.ToString(), settings) : new DiccionarioPersonalizado<string, string>();
            lblCategoriaOcupacional.Text = categoriaOcupacional.obtenerValor("Valor");
            //Ingreso
            lblPrevision.Text = ingreso.obtenerValor("DescripcionPrevision");
            lblPrevisionTramo.Text = ingreso.obtenerValor("DescripcionPrevisionTramo");
            lblModalidadIngreso.Text = ingreso.obtenerValor("DescripcionModalidad");
            lblLeyPrevisional.Text = ingreso.obtenerValor("DescripcionLeyPrevisional");
            lblProcedencia.Text = ingreso.obtenerValor("DescripcionProcedencia");
            lblEstablecimiento.Text = ingreso.obtenerValor("NombreEstablecimiento");
            lblFechaIngreso.Text = (ingreso["Fecha"] == null) ? "-" :
                                       DateTime.Parse(Convert.ToString(ingreso["Fecha"])).ToString("dd-MM-yyyy");
            lblHoraIngreso.Text = (ingreso["Hora"] == null) ? "-" :
                                   DateTime.Parse(Convert.ToString(ingreso["Hora"])).ToString("HH:mm:ss");
            lblUbicacionIngreso.Text = ingreso.obtenerValor("NombreUbicacion");
            lblCamaIngreso.Text = ingreso.obtenerValor("NombreCama");
            lblCodificacionIngreso.Text = ingreso.obtenerValor("NombreCodificacion");

            string trasladosVacia = "";
            for (int i = 0; i < 4; i++) {
                trasladosVacia += "<tr><td class='fondos-titulo contenido-celda'> " + (i + 1).ToString() + "°- Traslado </td>";
                trasladosVacia += "<td></td><td></td><td></td><td></td></tr>";
            }
            //Arreglo de Traslados
            if (json.TryGetValue("Traslados", out O))
            {
                arregloDiccionarios = JsonConvert.DeserializeObject<List<DiccionarioPersonalizado<string, string>>>(O.ToString(), settings);
                if (arregloDiccionarios.Count > 0)
                {
                    for (int i = 0; i < arregloDiccionarios.Count; i++)
                    {
                        DiccionarioPersonalizado<string, string> traslado = arregloDiccionarios[i];
                        string fecha = (traslado["Fecha"] == null) ? "-" : DateTime.Parse(Convert.ToString(traslado["Fecha"])).ToString("dd-MM-yyyy");
                        string hora = (traslado["Hora"] == null) ? "-" : DateTime.Parse(Convert.ToString(traslado["Hora"])).ToString("HH:mm:ss");
                        ltlTraslados.Text += "<tr>";
                        ltlTraslados.Text += "<td class='fondos-titulo contenido-celda'> " + (i + 1).ToString() + "°- Traslado </td>";
                        ltlTraslados.Text += "<td class='contenido-celda'>" + fecha + "</td>";
                        ltlTraslados.Text += "<td class='contenido-celda'>" + hora + "</td>";
                        ltlTraslados.Text += "<td class='contenido-celda'>" + traslado.obtenerValor("NombreUbicacion") + "</td>";
                        ltlTraslados.Text += "<td class='contenido-celda'>" + traslado.obtenerValor("NombreCodificacion") + "</td>";
                        ltlTraslados.Text += "</tr>";
                    }
                }
                else
                {
                    ltlTraslados.Text = trasladosVacia;
                }
            } else {
                arregloDiccionarios = new List<DiccionarioPersonalizado<string, string>>();
                ltlTraslados.Text = trasladosVacia;
            }
            //Egreso
            lblFechaEgreso.Text = egreso["Fecha"] == null ? "-" : DateTime.Parse(Convert.ToString(egreso["Fecha"])).ToString("dd-MM-yyyy");
            lblCamaEgreso.Text = egreso.obtenerValor("NombreCama");
            lblHoraEgreso.Text = (egreso["Hora"] == null) ? "-" : DateTime.Parse(Convert.ToString(egreso["Hora"])).ToString("HH:mm:ss");
            lblUbicacionEgreso.Text = egreso.obtenerValor("UbicacionEgreso");
            lblCodificacionEgreso.Text = egreso.obtenerValor("NombreCodificacion");
            //Nota: Extracción de un objeto
            DiccionarioPersonalizado<string, string> diagnostico;
            diagnostico = json.TryGetValue("Diagnostico", out O) ? JsonConvert.DeserializeObject<DiccionarioPersonalizado<string, string>>(O.ToString(), settings) : new DiccionarioPersonalizado<string, string>();
            //Diagnóstico
            lblDiagnostico.Text = diagnostico.obtenerValor("NombreDiagnostico");
            lblCausaExterna.Text = diagnostico.obtenerValor("NombreCausa");
            lblCondicionEgreso.Text = diagnostico.obtenerValor("NombreCondicionEgreso");
            lblParto.Text = diagnostico.obtenerValor("DescripcionParto");
            lblNacimiento.Text = diagnostico.obtenerValor("DescripcionNacimiento");
            lblRunMedico.Text = diagnostico.obtenerValor("RunMedico");
            lblNombreMedico.Text = diagnostico.obtenerValor("NombreMedico");
            lblEspecialidad.Text = diagnostico.obtenerValor("NombreEspecialidad");

            Funciones funciones = new Funciones();
            string doc = "F-Admisión-HOS";
            funciones.ExportarPDFWK(this.Page, doc, idHos, usuarioLogeado);
        }
         
        public string GetHost() 
        {
            return Funciones.GetHost(HttpContext.Current);
        }

    }
}