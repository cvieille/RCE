﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevaHojaEvolucionHOS.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.NuevaHojaEvolucionHOS" %>
<%@ Register Src="~/Vista/UserControls/CargadoDeArchivos.ascx" TagName="CargadorArchivos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalEvento.ascx" TagName="mdlEventos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPacienteEvento.ascx" TagName="mdlPacienteEvento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script src="<%= ResolveClientUrl("~/Script/Textbox.io/textboxio.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />

    <div class="card card-body">
        <div class="card-header bg-light">
            <h2 class="text-center py-4">
                <i class="fa fa-file-alt"></i> Hoja de Evolución
            </h2>

            <h5 class="text-right">
                <asp:Label ID="lbl_fechaActual" runat="server" Text="Lunes 23 de Julio del 2018" Font-Bold="true" Font-Size="Large" >
                </asp:Label>
            </h5>
        </div>
        <div class="card-body p-0">

            <wuc:Paciente ID="wucPaciente" runat="server" />
            
            <div class="card">
                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Datos de la atención</strong> </h5>
                </div>
                <div class="card-body p-2">
                    <div class="classic-tabs mx-2" style="margin: 0px !important;">

                        <ul id="ulTablist" class="nav nav-pills">
                            <li id="liAntecedentesClinicos" class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#divEvolucion">
                                    <i class="fa fa-check-square" aria-hidden="true"></i> Datos Evolución
                                </a>
                            </li>
                            <li id="liDatosIngreso" class="nav-item">
                                <a id="contact-tab-just" class="nav-link" data-toggle="pill" href="#nav-flujoExamenes">
                                    <i class="fa fa-heartbeat" aria-hidden="true"></i> Flujo Exámenes
                                </a>
                            </li>
                        </ul>
   
                        <div class="tab-content card" style="padding-top: 10px;">
                                    
                            <div id="divEvolucion" class="tab-pane fade show active p-4" role="tabpanel" aria-labelledby="home-tab-just">
                                
                                <h4 class="mb-3"> <strong>Datos de Evolución</strong> </h4>

                                <div id="divHojaEvolucion">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="txtFechaEvolucion">Fecha</label>
                                            <input id="txtFechaEvolucion" type="date" class="form-control" data-required="true" />
                                        </div>
                                        <div class="col-md-6">
                                            <label for="sltUbicacion">Servicio</label>
                                            <select id="sltUbicacion" class="form-control" data-required="true" >
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-6">
                                            <label for="txtDiagnosticoPrincipal">Diagnóstico Principal</label>
                                            <textarea ID="txtDiagnosticoPrincipal" class="form-control" Rows="4" data-required="true" maxlength="200" >
                                            </textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="txtOtrosDiagnosticos">Diagnósticos Secundarios</label>
                                            <textarea ID="txtOtrosDiagnosticos" class="form-control" Rows="4" MaxLength="200" >
                                            </textarea>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="custom-control custom-checkbox">
                                                <input id="defaultUnchecked" type="checkbox" class="custom-control-input" data-required='true'>
                                                <label class="custom-control-label" for="defaultUnchecked">Confirmo descripción del Diagnóstico</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <label for="txtEvolucion"> Evolución </label>
                                            <textarea ID="txtEvolucion" class="md-textarea md-textarea-auto form-control" Rows="4" data-required="true" MaxLength="125" >
                                            </textarea>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <label for="txtExamenFisico">Examen Físico</label>
                                            <textarea ID="txtExamenFisico" class="form-control" Rows="4" data-required="true" MaxLength="125" >
                                            </textarea>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <h5><strong>Plan de manejo y/o indicaciones</strong></h5>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-3">
                                            <label class="active">Seleccione Plantilla</label>
                                            <select id="sltPlantilla" class="form-control" data-required="true">
                                            </select>
                                        </div>
                                        <div class="col-md-2 center-vertical">
                                                <button id="btnLimpiar" class="btn btn-warning" onclick="return false;" type="submit">LIMPIAR PLANTILLA</button>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <textarea id="txtDetEvolucion" style="width: 100%; height: 400px;" data-required="true" placeholder="&nbsp;" class="textboxio">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                
                                <a id="aIrFlujoExamen" class="btn btn-primary pull-right" data-adjunto='true' style="margin-top: 20px;" >
                                    <i class="fa fa-heartbeat"></i> Ir a Flujo de Exámenes 
                                    <i class="fa fa-arrow-right"></i>
                                </a>

                            </div>

                            <div id="nav-flujoExamenes" class="tab-pane fade p-2" role="tabpanel" aria-labelledby="contact-tab-just">
                                
                                <ul class="nav nav-pills">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="pill" href="#divExamenes">
                                            <i class="fa fa-heartbeat"></i> Exámenes
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="pill" href="#divFlujoExamenes">
                                            <i class="fa fa-heartbeat" aria-hidden="true"></i> Flujo Exámenes
                                        </a>
                                    </li>
                                </ul>
                                
                                <div class="tab-content">
                                    
                                    <div class="tab-pane fade in show active p-4" id="divExamenes" role="tabpanel">
                                        <center>
                                        <h4> <strong>Exámenes</strong> </h4>

                                        <table id="tblExamenes" style="width:60% !important;" 
                                                class="table table-bordered table-sm table-hover">
                                            <thead class="btn-primary-dark">
                                                <tr class="text-center white-text">
                                                    <th>Examen</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        </center>
                                    </div>
                                    
                                    <div class="tab-pane fade p-4" id="divFlujoExamenes" role="tabpanel">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <center>
                                                <h4> <strong>Flujo Exámenes</strong> </h4>

                                                <table id="tblFlujoExamen" style="width: 100% !important;"
                                                    class="table table-bordered table-sm table-hover mt-3">
                                                    <thead class="btn-primary-dark">
                                                        <tr class="text-center white-text">
                                                            <th>Examen</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                  
                                </div>
                                
                                <a id="aIrDatosEvolucion" class="btn btn-primary pull-right" 
                                    data-adjunto='true' style="margin-top: 20px;">
                                    <i class="fa fa-arrow-left"></i> Ir a Datos Evolución 
                                    <i class="fa fa-check-square"></i>
                                </a>
                                
                            </div>
                                    
                        </div>

                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Datos de Profesional</strong> </h5>
                </div>
                <div class="card-body">
                    <div id="divDatosProfesionales">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="txtNumeroDocumentoProfesional" class="active">Número Documento</label>
                                <input id="txtNumeroDocumentoProfesional" type="text" class="form-control" placeholder="&nbsp;" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label class="active" for="txtNombreProfesional">Nombre/es</label>
                                <input id="txtNombreProfesional" type="text" class="form-control" placeholder="&nbsp;" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label for="txtApePatProfesional" class="active">Apellido Paterno</label>
                                <input id="txtApePatProfesional" type="text" class="form-control" placeholder="&nbsp;" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label for="txtApeMatProfesional" class="active">Apellido Materno</label>
                                <input id="txtApeMatProfesional" type="text" class="form-control" placeholder="&nbsp;" disabled="disabled" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Archivos adjuntos</strong></h5>
                </div>
                <div class="card-body">
                    <wuc:CargadorArchivos ID="wucCargadorArchivos" runat="server" />
                </div>
            </div>

        </div>
        <div class="card-footer">
            <div class="row">
                <div id="divFechaCreacion" class="col-md-4 center-vertical">
                    <h5 style="margin-bottom:0px !important;">
                        Fecha Límite de Edición: <strong id="strFechaCreacion"></strong>
                    </h5>
                </div>
                <div class="col-md-8 text-right">
                        
                    <a id="aGuardarHojaEvolucion" class="btn btn-primary" onclick="GuardarGeneral()" 
                        data-adjunto='true'>
                        <i class="fa fa-save"></i> Guardar
                    </a>

                    <a id="aCancelarHojaEvolucion" class="btn btn-default" onclick="CancelarHojaEvolucion()">
                        <i class="fa fa-remove"></i> Cancelar
                    </a>

                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/NuevaHojaEvolucionHOS.js") + "?" + GetVersion() %>"></script>

</asp:Content>
