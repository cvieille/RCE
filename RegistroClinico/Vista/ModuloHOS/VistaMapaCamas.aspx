﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="VistaMapaCamas.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.VistaMapaCamas" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/bootstrap-clockpicker.css" rel="stylesheet"/>
    
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/jquery-clockpicker.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/bootstrap-clockpicker.js") %>"></script>
    <style>

        .popover {
            z-index: 1062 !important;
        }

        .popover-body {
            padding: 0px !important;
        }

    </style>
    <script>
        $(document).ready(function () {

            $('#lnkExportarMovHospitalizacion').click(function () {
                var tblObj = $('#tblMovHospitalizacion').DataTable();
                __doPostBack($(this).data("btnExportarH"), JSON.stringify(tblObj.rows().data().toArray()));
            });

        });
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />

    <div class="card">
        <div class="card-header">
            <h2 class="text-center">Mapa de Camas</h2>
        </div>
        <div class="card-body">

            <div class="">
                <div id="divFiltro" class="row center-horizontal-vertical">
                    <div class="col-md-2">                        
                        <label for="txtFechaHospitalizacion" class="active">fecha</label>
                        <input id="txtFechaHospitalizacion" type="date" class="form-control" disabled="disabled" />
                    </div>
                    <div class="col-md-3">                        
                        <label for="sltUbicacion" class="active">Servicio</label>
                        <select id="sltUbicacion" class="form-control">
                        </select>
                    </div>
                    <div id="divAla" class="col-md-2">                        
                        <label for="sltAla" class="active">Ala</label>
                        <select id="sltAla" class="form-control" disabled >
                        </select>
                    </div>
                    <div class="col-md-2">
                        <br />
                        <a id="btnHosFiltro" class="btn btn-success">
                            Buscar
                        </a>
                    </div>
                    <div class="col-md-2 text-right icon-info">
                        <i class="fa fa-question-circle" data-container="body" id="icon-info" data-toggle="popover"  style="font-size:2em; cursor:pointer;"></i>
                    </div>
                </div>
            </div>

            <hr />
            <!--Iconos-->
            <div class="row  mt-3 mb-3" >
                <div class="col-md-6">
                    <div class="col text-center">
                        <h5>
                            <b>Estado de las camas</b>
                        </h5>
                    </div>
                    <br />
                    <div class="col" style="display:flex;">
                        <div class='col-md-3 col-sm-6 text-center'>
                           <strong>Disponible</strong><br>
                                <div class='cama disponible' style="margin:0 auto;">
                                    <img src='../../Style/img/bed.PNG' width="40"/>
                                    <i class='fa fa-check-circle' style='z-index:1;'></i>
                                </div>
                        </div>
                        <div class='col-md-3 col-sm-6 text-center' >
                            <strong>Ocupada</strong>
                                <div class='cama ocupada'  style="margin:0 auto;" >
                                    <img src='../../Style/img/bed.PNG' width='40' />
                                    <i class='fa fa-user-circle' style='z-index:1;'></i>
                                </div>
                        </div>    
                        <div class='col-md-3 col-sm-6 text-center'>
                                <strong>Bloqueada</strong>
                                <div class='cama bloqueada'  style="margin:0 auto;" >
                                    <img src='../../Style/img/bed.PNG' width='40' />
                                    <i class='fa fa-lock' style='z-index:1;'></i>
                                </div>
                                
                        </div>
                        <div class='col-md-3 col-sm-6 text-center'>
                            <strong>Deshabilitada</strong>
                                <div class='cama deshabilitada'  style="margin:0 auto;" >
                                    <img src='../../Style/img/bed.PNG' width='40' />
                                    <i class='fa fa-ban' style='z-index:1;'></i>
                                </div> 
                        </div>
                    </div>
                </div>


                <div class="col-md-6 " >
                    <div class="col text-center">
                        <h5>
                            <b class="text-center   ">Categorías y niveles de riesgo.</b>
                        </h5>
                    </div>
                    <div class="row" style="display:flex;">
                        <div class="col text-center" >
                            <p>
                            <strong>A</strong><br />
                             Máximo
                            </p>
                             <i  class='fa fa-user-circle' style=" font-size:3em; color:#dc3545;" ></i>
                        </div>
                        <div class="col text-center">
                            <p>
                            <strong>B</strong><br />
                             Alto
                            </p>
                             <i  class='fa fa-user-circle' style="font-size:3em; color:#DBA607;" ></i>
                        </div>
                        <div class="col text-center">
                             <p>
                             <strong>C</strong><br />
                             Mediano
                            </p>
                             <i  class='fa fa-user-circle' style="font-size:3em; color:#17a2b8;" ></i>
                        </div>
                        <div class="col  text-center">
                             <p>
                             <strong>D</strong><br />
                             Bajo
                             </p>
                             <i class='fa fa-user-circle' style="font-size:3em; color:#28a745;" ></i>
                        </div>
                        <div class="col text-center">
                            <p>
                            <strong>S/C</strong><br />
                             Sin categoría
                            </p>
                             <i class='fa fa-user-circle' style="font-size:3em; color:black;" ></i>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fin iconos-->
            <div id="divGeneralMapaCama"></div>

            <div class="fixed-bottom text-right">
                <a onclick="$('html, body').animate({ scrollTop: $('body').offset().top}, 500);">
                    <i class="fa fa-arrow-up fa-2x" style="cursor:pointer;"></i>
                </a>
            </div>

        </div>
    </div>

    <div id="mdlVerMasInformacion" class="modal fade" role="dialog" aria-labelledby="mdlVerMasInformacion" aria-hidden="true" tabindex="-1" style="z-index:1064 !important;">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead m-0 w-100 text-center">
                        <strong class="text-center"><i class="fa fa-info-circle fa-1x"></i> Detalles de la Hospitalización</strong>
                    </p>
                </div>
                <div class="modal-body">
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Movimiento Hospitalizacion -->
    <div class="modal fade" id="mdlMovimientoHospitalizacion" role="dialog" aria-labelledby="mdlMovimientoHospitalizacion" aria-hidden="true" 
        tabindex="-1" style="z-index:1063 !important;">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Movimientos de Hospitalización</p>
                </div>
                <div class="modal-body">
                    <div class="row">
                        
                        <asp:Button runat="server" ID="btnExportarMovHospitalizacion" OnClick="btnExportarMovHospitalizacion_Click" style="display:none;"/>
                        <a id="lnkExportarMovHospitalizacion" class="btn btn-info btn-sm ml-auto mr-2 mb-2" href="#\">
                            <i class="fa fa-file-excel-o" style="font-size:18px;"></i> Exportar a Excel 
                        </a>
                    </div>
                    <div id="traDisp">
                        <table id="tblTraHospitalizacion" class="table table-bordered table-hover" style="width: 100%;"></table>
                    </div>
                    <div id="movDisp">
                        <table id="tblMovHospitalizacion" class="table table-bordered table-hover" style="width: 100%;"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Confirmación de traspaso -->
    
    <div class="modal fade bd-example-modal-lg" id="mdlConfirmarTraspaso" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg-w-100" role="document">
            <div class="modal-content">

                <div class="modal-header bg-primary">
                    <p class="heading lead mb-0">Confirmar Traslado de Paciente</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="text-center">
                        
                        <h4 class="text-center mb-4">¿Está seguro que quiere realizar este traspaso?</h4>

                        <div id="divBodyMdlTraspaso" class="center-horizontal-vertical">
                        </div>

                    </div>
                </div>

                <div class="modal-footer justify-content-center">
                    <a id="aAceptarConfirmarTraspaso" type="button" class="btn btn-success" onclick="EjecutarGuardarTraslado();">
                        <i class="fa fa-check text-white"></i> Aceptar
                    </a>
                    <a type="button" class="btn btn-danger" data-dismiss="modal">
                        <i class="fa fa-remove text-white"></i> Cancelar
                    </a>
                </div>

            </div>
        </div>
    </div>
     <!---Modal información paciente-->
    <div class="modal fade" id="mdlInfoPaciente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header" style="background-color:lightyellow;" id="headerModalInfoPaciente">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="bodyModalInfoPaciente">
              </div>
              <div class="modal-footer" id="footerModalInfoPaciente">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
         </div>
     </div>
    <!---->
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/VistaMapaCamas.js") + "?" + GetVersion() %>"></script>

</asp:Content>
