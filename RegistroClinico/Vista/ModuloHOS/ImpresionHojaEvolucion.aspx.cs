﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RegistroClinico.Negocio;
using System;
using System.Web;
using System.Web.UI.WebControls;

namespace RegistroClinico.Vista.ModuloHOS
{
    public partial class ImpresionHojaEvolucion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnHidden_Click(object sender, EventArgs e)
        {

            // Valida sesión según los permisos que tenga el perfil
            NegSession negSession = new NegSession(Page);
            negSession.ValidarPermisosSession();

            string sCommand = Request.Form["__EVENTARGUMENT"];
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            JObject des = JsonConvert.DeserializeObject<JObject>(sCommand, settings);

            var Paciente = des["Paciente"];
            txtNumeroDocumentoPaciente.Text = Paciente["GEN_numero_documentoPaciente"].ToString();
            if (!String.IsNullOrEmpty(Paciente["GEN_digitoPaciente"].ToString()))
                txtNumeroDocumentoPaciente.Text += "-" + Paciente["GEN_digitoPaciente"].ToString();
            txtNombresPaciente.Text = Paciente["GEN_nombrePaciente"].ToString() + " " +
                Paciente["GEN_ape_paternoPaciente"].ToString() + " " +
                Convert.ToString(Paciente["GEN_ape_maternoPaciente"].ToString());
            txtEdad.Text = Paciente["EdadPaciente"].ToString();
            txtSexo.Text = Paciente["GEN_nomSexo"].ToString();
            txtFechaNacimiento.Text = Paciente["GEN_fec_nacimientoPaciente"].ToString();
            txtDireccion.Text = Paciente["GEN_dir_callePaciente"].ToString();
            txtTelefono.Text = Paciente["GEN_telefonoPaciente"].ToString();
            txtOtroTelefono.Text = Paciente["GEN_otros_fonosPaciente"].ToString();

            txtFechaHojaEvolucion.Text = des["RCE_fechaHoja_Evolucion"].ToString();
            txtUbicacion.Text = des["GEN_nombreUbicacion"].ToString();
            txtDiagnosticoPrincipal.Text = des["RCE_diagnostico_principalHoja_Evolucion"].ToString();
            txtOtrosDiagnosticos.Text = des["RCE_otros_diagnosticosHoja_Evolucion"].ToString();

            Label lblEvolucion = new Label();
            lblEvolucion.Text = Convert.ToString(des["RCE_evolucionHoja_Evolucion"]);
            pnlEvolucion.Controls.Add(lblEvolucion);

            Label lblExamenFisico = new Label();
            lblExamenFisico.Text = Convert.ToString(des["RCE_examen_FisicoHoja_Evolucion"]);
            pnlExamenFisico.Controls.Add(lblExamenFisico);

            Label lblDescripcionHE = new Label();
            lblDescripcionHE.Text = Convert.ToString(des["RCE_descripcionHoja_Evolucion"]);
            pnlDetalleEvolucion.Controls.Add(lblDescripcionHE);

            lblProfesional.Text = des["GEN_nombrePersonasProfesional"].ToString() + " " +
                des["GEN_apellido_paternoPersonasProfesional"].ToString();
            if (!String.IsNullOrEmpty(des["GEN_apellido_maternoPersonasProfesional"].ToString()))
                lblProfesional.Text += " " + des["GEN_apellido_maternoPersonasProfesional"].ToString();
            lblProfesional.Text += " " + des["GEN_numero_documentoPersonasProfesional"].ToString();

            Session.Remove("ID_HOJAEVOLUCION");
            string sName = string.Join(" ", new string[] { "HOJA_EVOLUCION", DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") });
            string sUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            Funciones.ExportarPDF(sUrl, this.Page, sName, des["LoginUsuario"].ToString());

        }
    }
}