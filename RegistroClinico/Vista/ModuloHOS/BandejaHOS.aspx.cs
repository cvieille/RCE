﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;

namespace RegistroClinico.Vista.ModuloHOS
{
    public partial class BandejaHOS : Handlers.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnHosExportarH_Click(object sender, EventArgs e)
        {
            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.MissingMemberHandling = MissingMemberHandling.Ignore;
                //Data viene de arreglo "data" de datatable de Bandeja
                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);
                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["ID Hosp."] = Convert.ToString(des[i][0]),
                        ["Ficha"] = Convert.ToString(des[i][1]),
                        ["Paciente"] = Convert.ToString(des[i][2]),
                        ["Cama"] = Convert.ToString(des[i][3]),
                        ["Fecha Hosp."] = Convert.ToString(des[i][4]),
                        ["Hora Hosp."] = Convert.ToString(des[i][5]),
                        ["Días"] = Convert.ToString(des[i][6]),
                        ["Fecha epicrisis"] = Convert.ToString(des[i][7]),
                        ["Aislamiento"] = Convert.ToString(des[i][8])
                    };
                    dic.Add(d);
                }
                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Bandeja Hospitalizacion " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Bandeja Hospitalizacion " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), workbook);

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al exportar Excel de la bandeja de hospitalización.", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }
        }

        protected void btnIngExportarH_Click(object sender, EventArgs e)
        {
            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);

                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["ID Ingreso"] = Convert.ToString(des[i][0]),
                        ["DNI Paciente"] = Convert.ToString(des[i][1]),
                        ["Paciente"] = Convert.ToString(des[i][2]),
                        ["Profesional"] = Convert.ToString(des[i][3]),
                        ["Ubicación"] = Convert.ToString(des[i][4]),
                        ["Fecha de ingreso"] = Convert.ToString(des[i][5])
                    };
                    dic.Add(d);
                }

                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Bandeja Ingreso Medico", dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Bandeja Ingreso Medico", workbook);

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al escribir log desde el cliente", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }
        }

        protected void btnExportarMovIngH_Click(object sender, EventArgs e)
        {
            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.MissingMemberHandling = MissingMemberHandling.Ignore;

                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);

                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["Fecha movimiento"] = Convert.ToString(des[i][1]),
                        ["Usuario"] = Convert.ToString(des[i][2]),
                        ["Movimiento"] = Convert.ToString(des[i][3])
                    };
                    dic.Add(d);
                }

                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Movimientos ingreso médico", dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Movimientos ingreso médico", workbook);
            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al exportar Excel de movimientos de ingreso médico.", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }
        }

        protected void btnExportarMovHospitalizacionH_Click(object sender, EventArgs e)
        {
            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.MissingMemberHandling = MissingMemberHandling.Ignore;

                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);
                //Todos Los exportar dependen del número de columnas en bandeja y o tablas...
                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["Fecha movimiento"] = Convert.ToString(des[i][0]),
                        ["Usuario"] = Convert.ToString(des[i][1]),
                        ["Movmiento"] = Convert.ToString(des[i][2])
                    };
                    dic.Add(d);
                }

                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Movimientos hospitalización", dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Movimientos hospitalización", workbook);
            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al exportar Excel de movimientos de hospitalización.", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }
        }
    }
}