﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevoFormularioAdmision.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.NuevoFormularioAdmision" %>

<%@ Register Src="~/Vista/UserControls/ModalEvento.ascx" TagName="mdlEventos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPerfiles.ascx" TagName="mdlPerfiles" TagPrefix="mdlPerfiles" %>
<%@ Register Src="~/Vista/UserControls/ModalPacienteEvento.ascx" TagName="mdlPacienteEvento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <style>
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }
    </style>
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:mdlEventos ID="wucMdlEventos" runat="server" />
    <wuc:mdlPacienteEvento ID="wucMdlPacienteEvento" runat="server" />

    <!-- HIDDEM(hidden) FIELDS -->

    <div class="alert-container">
        <div id="alertObligatorios" class="alert text-center alert-obligatorios">
            <b>Los campos marcados son obligatorios (*)</b>.
        </div>
        <div id="alertFormatos" class="alert text-center alert-formato">
            <b>Los campos marcados deben cumplir el formato o estandar necesario</b>.
        </div>
        <div id="alertHospitalizacionActiva" class="alert text-center alert-hospitalizacion-activa">
            <b>El paciente seleccionado tiene una Hospitalización ACTIVA. Se recomienda finalizar, antes de crear una nueva. Para más información, puede consultar el Historial del paciente en Bandeja</b>.
        </div>
    </div>
    
    <div class="card card-body">
        <div class="card-header bg-light">
            <h2 class="text-center py-4">
                <i class="fa fa-file-alt"></i> Formulario de Ingreso Paciente Hospitalizado
            </h2>
        </div>
        <div class="card-body p-0">

            <wuc:Paciente ID="wucPaciente" runat="server" />
            
            <div id="divDatosAdicionalesPaciente">

                <div class="row mt-2">
                    <div class="col-md-3">
                        <label for="sltPais" id="lblPais">País de residencia</label>
                        <select id="sltPais" class="form-control datos-pac" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltRegion">Región</label>
                        <select id="sltRegion" class="form-control datos-pac" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltProvincia">Provincia</label>
                        <select id="sltProvincia" class="form-control datos-pac" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltCiudad">Ciudad</label>
                        <select id="sltCiudad" class="form-control datos-pac" data-required="true">
                        </select>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-md-3">
                        <label for="sltPuebloOriginario">Pueblo Originario</label>
                        <select id="sltPuebloOriginario" class="form-control datos-pac">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltCategoriaOcupacional">Categoría Ocupacional</label>
                        <select id="sltCategoriaOcupacional" class="form-control datos-pac">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltOcupacion" class="datos-pac">Ocupación</label>
                        <select id="sltOcupacion" class="form-control datos-pac">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltEscolaridad">Nivel Educacional</label>
                        <select id="sltEscolaridad" class="form-control datos-pac">
                        </select>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-md-3">
                        <label for="sltNacionalidad">Nacionalidad</label>
                        <select id="sltNacionalidad" class="form-control datos-pac" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltPrevision">Previsión</label>
                        <select id="sltPrevision" class="form-control datos-pac" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltPrevisionTramo">Tramo</label>
                        <select id="sltPrevisionTramo" class="form-control datos-pac" data-required="true">
                        </select>
                    </div>
                </div>

            </div>
            
            <div id="divIngreso" class="card text-white">

                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Datos de Ingreso</strong> </h5>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-2">
                            <label id="lbltxtFechaIngreso" for="txtFechaIngreso">Fecha</label>
                            <input id="txtFechaIngreso" type="date" class="form-control datos-ingreso" data-required='true' />
                        </div>
                        <div class="col-md-2">
                            <label id="lblHoraIngreso" for="txtHoraIngreso">Hora</label>
                            <input id="txtHoraIngreso" type="time" class="form-control datos-ingreso" data-required='true' />
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-3">
                            <label for="sltProcedenciaIngreso">Procedencia de paciente</label>
                            <select id="sltProcedenciaIngreso" class="form-control datos-ingreso" data-required="true">
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="sltEstablecimientoProcedenciaIngreso">Otro Establecimiento</label>
                            <select id="sltEstablecimientoProcedenciaIngreso" class="form-control datos-ingreso">
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>¿Ley previsional?</label><br />
                            <input id="chbLeyPrevisionalIngreso" type="checkbox" class="editable-switch datos-ingreso" />
                        </div>
                        <div class="col-md-3">
                            <label for="sltLeyPrevisionalIngreso">Ley Previsional Ingreso</label>
                            <select id="sltLeyPrevisionalIngreso" class="form-control">
                            </select>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-3">
                            <label for="sltServicioSaludIngreso">Servicio de Salud</label>
                            <select id="sltServicioSaludIngreso" class="form-control datos-ingreso" data-required='true'>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="sltEstablecimientoIngreso">Establecimiento</label>
                            <select id="sltEstablecimientoIngreso" class="form-control datos-ingreso" data-required='true'>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="sltUbicacionIngreso">Ubicación</label>
                            <select id="sltUbicacionIngreso" class="form-control datos-ingreso" data-required="true">
                            </select>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-3">
                            <label for="sltTipoHospitalizacion">Tipo de Hospitalización</label>
                            <select id="sltTipoHospitalizacion" class="form-control datos-ingreso" data-required='true'>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="sltModalidadFonasaIngreso">Modalidad Fonasa</label>
                            <select id="sltModalidadFonasaIngreso" class="form-control datos-ingreso" data-required='true'>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="txtMotivoHospitalizacion">Motivo de hospitalización</label>
                            <textarea id="txtMotivoHospitalizacion" class="md-textarea form-control" Rows="3" data-required='true' 
                                MaxLength="100" placeholder="Escriba motivo(s) de hospitalización">
                            </textarea>
                        </div>
                    </div>

                </div>

            </div>
            
            <div id="divEgreso" class="card text-white">
                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Datos de Egreso</strong> </h5>
                </div>
                <div class="card-body">
                    <div id="divDatosEgreso">
                        <div class="row">
                            <div class="col-md-2">
                                <label id="lbltxtFechaEgreso" for="txtFechaEgreso">Fecha</label>
                                <input ID="txtFechaEgreso" type="date" class="form-control datos-egreso" data-required="false" />
                            </div>
                            <div class="col-md-1">
                                <label id="lbltxtHoraEgreso" for="txtHoraEgreso">Hora</label>
                                <input ID="txtHoraEgreso" type="time" class="form-control datos-egreso" data-required="false" />
                            </div>
                            <div class="col-md-2">
                                <label id="lblDiasEstada" for="txtDiasEstada">Días estadía</label>
                                <input ID="txtDiasEstada" type="text" data-required="false" class="form-control disabled datos-egreso text-center" />
                            </div>
                            <div class="col-md-3">
                                <label for="sltTipoEgreso">Tipo Egreso</label>
                                <select id="sltTipoEgreso" class="form-control datos-egreso" data-required="false">
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="sltDestinoAlta">Destino alta</label>
                                <select id="sltDestinoAlta" class="form-control datos-egreso" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>¿Ley previsional?</label><br />
                                <input ID="chbLeyPrevisionalEgreso" type="checkbox" class="editable-switch datos-egreso" />
                            </div>
                            <div class="col-md-3">
                                <label for="sltLeyPrevisionalEgreso">Ley Previsional Egreso</label>
                                <select id="sltLeyPrevisionalEgreso" class="form-control datos-egreso" data-required="false">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="sltModalidadFonasaEgreso">Modalidad Fonasa</label>
                            <select id="sltModalidadFonasaEgreso" class="form-control datos-egreso" data-required='true'>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <label id="lbltxtDiagnostico" for="txtDiagnostico">Diagnóstico Principal</label>
                            <textarea ID="txtDiagnostico" class="form-control datos-egreso" rows="4">
                            </textarea>
                        </div>
                        <div class="col-md-6">
                            <label id="lbltxtCausa" for="txtCausa">Causa externa</label>
                            <textarea ID="txtCausa" rows="4" class="form-control datos-egreso">
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <a id="btnGuardar" class="btn btn-primary waves-effect waves-light">
                <i class="fa fa-save"></i> GUARDAR
            </a>
            <a id="btnCancelar" class="btn btn-default waves-effect waves-light">
                <i class="fa fa-remove"></i> CANCELAR
            </a>
        </div>
    </div>
    <div class="modal fade" id="mdlAlerta" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info center-horizontal" style="justify-content:center">
                    <div class="center-horizontal">El paciente ha sido egresado</div>
                </div>
                <div class="modal-body">
                    <div id="mdlAlertaCuerpo" class="row center-horizontal">
                        ¿Desea imprimir el formulario de egreso?
                    </div>
                    <div class="row" style="padding-top:5px">
                        <hr/>
                    </div>
                    <div id="mdlAlertaBoton" class="row center-horizontal">
                        <div class='col-sm-2'>
                            <a id='linkConfirmar' class='btn btn-default btn-block' href='#/' onclick="imprimirFormulario()"> Imprimir </a>
                        </div>
                        <div class='col-sm-2'>
                            <a id='linkCancelar' class='btn btn-default btn-block' href='#/' onclick="OmitirImpresion()"> Omitir </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL HOSPITALIZACION ACTIVA -->
    <div class="modal fade" id="mdlHosActiva" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content" style="background-color: transparent !important; box-shadow: none; border: none;">
                <div class="modal-header modal-header-default center-horizontal" style="justify-content:center; background-color: transparent !important;">
                    <div class="row">
                        <!--
                        <div class="col-12 text-center">
                            <h1 style="color: white;">Advertencia!</h1>
                        </div>
                        -->
                        <div class="col-6 offset-3 center-horizontal">
                            <h1 class="text-center bg-warning rounded-circle p-3 display-3 m-0"  style="border: 5px solid #000000 !important;"><i class="fas fa-exclamation-triangle" style="width: 90px !important; height: 90px !important;"></i></h1>
                        </div>
                    </div>
                    <br/>
                </div>
                <div class="modal-body pb-0" style="background-color: white !important;">
                    <div class="text-center">
                        <h4>Existe un registro de hospitalización activo para este paciente. <br /></h4>
                        <div class="alert border border-warning mt-2">
                            <h5>
                            <strong id="stgHospitalizacion"></strong>
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="modal-footer center-horizontal" style="background-color: white !important;">
                    <a id="aNuevaHospitalizacion" class='btn btn-primary form-control' href='#/'>
                        <i class="fas fa-plus"></i> Nuevo Ingreso 
                    </a>
                    <a id="aEditarHospitalizacion" class='btn btn-secondary form-control' href='#/'> 
                        <i class='fa fa-edit'></i> Editar Ingreso
                    </a>
                    <a id="aCancelarIngresoHospitalizacion" class='btn btn-default form-control' href="#/"> 
                        <i class="fas fa-ban"></i> Cancelar 
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- -->
    <!-- MODAL IMPRIMIR -->
    <div class="modal modal-print fade" id="mdlImprimir" role="dialog" aria-labelledby="mdlImprimir" aria-hidden="true"
        tabindex="-1">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i> Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameHospitalizacion" frameborder="0" onload="$('#modalCargando').hide();">
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloPaciente/ComboPacientes.js") + "?" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/NuevoFormularioAdmision.js") + "?" + GetVersion() %>"></script>
</asp:Content>