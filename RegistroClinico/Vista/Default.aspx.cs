﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using RegistroClinico.Datos;

namespace RegistroClinico
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.RemoveAll();
                Application.RemoveAll();
                //Funciones.RemoveAllCookies(HttpContext.Current);
            }
        }
        public string GetVersion()
        {
            return Funciones.sVersion;
        }

        public string GetUrlWebApi() 
        {
            return Funciones.GetUrlWebApi();
        }
    }
}