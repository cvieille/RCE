﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="Inicio.aspx.cs" Inherits="RegistroClinico.Vista.Inicio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script>

        $(document).ready(function () {

            var sSession;
            sSession = getSession();
            RevisarAcceso(false, sSession.nombre);
            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
                contentType: "application/json",
                dataType: "json",
                async:true,
                success: function (data) {
                    let sN = data[0].GEN_nombrePersonas + ' ' + data[0].GEN_apellido_paternoPersonas + ' ' + data[0].GEN_apellido_maternoPersonas;
                    $("#lblNombreUsuario").text("Nombre: " + sN);
                    ShowModalCargando(false);
                }
            });

            ///Manuales de usuario            
            $.ajax({
                type: "GET",
                url: ObtenerHost() + '/Vista/Handlers/MetodosGenerales.ashx?method=getManuales',
                data: null,
                async: true,
                success: function (data) {
                    $("#tblManuales").empty();
                    var d = JSON.parse(data);

                    d.forEach((item, index) => {

                        var nombre = item;
                        
                        $("#divManuales").append(
                            `
                                <div class='col col-md-6'>
                                    <div id='divManual_${index}' class='alert alert-info text-center' style='text-transform: uppercase;cursor: pointer;'
                                        data-nombre-documento='${nombre}'>
                                        <i class='fa fa-file-pdf' style='padding-right:20px'></i> ${nombre}
                                    </div>
                                </div>
                            `);

                        $(`#divManual_${index}`).click(function () {

                            $.ajax({
                                cache: false,
                                contentType: false,
                                type: "GET",
                                url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=getManual&nombre=${$(this).data("nombre-documento")}`,
                                processData: false,
                                xhrFields: {
                                    responseType: 'blob'
                                },
                                success: function (response, status, xhr) {

                                    try {

                                        var blob = new Blob([response], { type: 'application/pdf' });
                                        var URL = window.URL || window.webkitURL;
                                        var downloadUrl = URL.createObjectURL(blob);
                                        window.open(downloadUrl);

                                    } catch (ex) {
                                        console.log(ex);
                                    }
                                },
                                error: function (err) {
                                    console.log("error al traer manual: " + JSON.stringify(err));
                                }
                            });

                            //axios({
                            //    method: "GET",
                            //    url: `${getHost()}/EA/GetManual?manual=${namePdf}`,
                            //    responseType: 'arraybuffer'
                            //}).then((response) => {


                            //});
                        });
                    });

                    $('#mdlManuales').modal("show");
                },
                error: function (err) {
                    console.log("error" + JSON.stringify(err));
                }
            });
            ShowModalCargando(false);
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="row ">
        <div class="col col-md-12">
            <div class="card mt-3">
                <div class="card-header ">Registro Clínico Electrónico</div>
                <div class="card-body">
                    <div class="card ">
                        <div class="card-body">
                            <div class="card-title alert alert-primary w-100  mb-4">USTED ESTA LOGEADO COMO:</div>
                            <label id="lblNombreUsuario" class="card-text text-uppercase" style="font-size: 35px;"></label>
                            <p>
                                Si sus datos son incorrectos o necesita generar nuevas cuentas, favor solicitar cambios a                                
                                <i class="fa fa-phone-square" aria-hidden="true"></i>
                                613669 - 613166 o al correo 
                                <i class="fa fa-phone-email" aria-hidden="true"></i>
                                desarrollo.hcm@redsalud.gov.cl
                            </p>

                            <a href="#" onclick="window.location.replace(ObtenerHost() + '/Vista/Default.aspx');" class="btn btn-secondary btn-block btn-lg">
                                <i class="fa fa-user"></i>
                                Cambiar de Usuario</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-md-12">
            <div class="card mt-3">
                <div class="card-header ">Manuales de Usuario</div>
                <div class="card-body">
                    <div class="card" style="padding: 20px;">
                        <div id="divManuales" class="row">
                        </div>
                        <table id="tblManuales">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
