﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VistaMapaCamasUrgencia.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.VistaMapaCamasUrgencia"  MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalMovimientoPaciente.ascx" TagName="mdMMP" TagPrefix="MMP" %>
<%@ Register Src="~/Vista/UserControls/AtencionClinica.ascx" TagName="AtencionClinica" TagPrefix="wuc" %>
<asp:Content ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/bootstrap-clockpicker.css" rel="stylesheet"/>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/jquery-clockpicker.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/bootstrap-clockpicker.js") %>"></script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Content" runat="server">
<!--include modal movimiento paciente-->
<MMP:mdMMP ID="MMP1" runat="server" />
<wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
<!-- fin include modal movimiento paciente = MMP-->
    <!--Iconografía de información de camas-->
           <div class="card">
        <div class="card-header">
            <h2 class="text-center">Mapa de camas urgencia</h2>
        </div>
        <div class="card-body">


            <!--Iconos-->
            <div class="row  mt-3 mb-3" >
                <div class="col-md-6">
                    <div class="col text-center">
                        <h5>
                            <b>Estado de las camas</b>
                        </h5>
                    </div>
                    <div class="col" style="display:flex;">
                        <div class='col-md-3 col-sm-6 text-center'>
                           <strong>Disponible</strong><br>
                                <div class='cama disponible' style="margin:0 auto;">
                                    <img src='../../Style/img/bed.PNG' width="40"/>
                                    <i class='fa fa-check-circle' style='z-index:1;'></i>
                                </div>
                        </div>
                        <div class='col-md-3 col-sm-6 text-center' >
                            <strong>Ocupada</strong>
                                <div class='cama ocupada'  style="margin:0 auto;" >
                                    <img src='../../Style/img/bed.PNG' width='40' />
                                    <i class='fa fa-user-circle' style='z-index:1;'></i>
                                </div>
                        </div>    
                        <div class='col-md-3 col-sm-6 text-center'>
                                <strong>Bloqueada</strong>
                                <div class='cama bloqueada'  style="margin:0 auto;" >
                                    <img src='../../Style/img/bed.PNG' width='40' />
                                    <i class='fa fa-lock' style='z-index:1;'></i>
                                </div>
                                
                        </div>
                        <div class='col-md-3 col-sm-6 text-center'>
                            <strong>Deshabilitada</strong>
                                <div class='cama deshabilitada'  style="margin:0 auto;" >
                                    <img src='../../Style/img/bed.PNG' width='40' />
                                    <i class='fa fa-ban' style='z-index:1;'></i>
                                </div> 
                        </div>
                    </div>
                </div>

                <div class="col-md-6 " >
                    <div class="col text-center">
                        <h5>
                            <b class="text-center   ">Categorización.</b><br>
                        </h5>
                    </div>
                    <div class="row" style="display:flex;">
                        <div class="col text-center" >
                            <p>
                            <strong>Categoría 1</strong>
                            </p>
                            <i class='fa fa-user-circle' style="font-size:2em; color:#dc3545;" ></i>
                            <br />
                             <span class="badge badge-pill bg-danger badge-count" style="color: white !important; font-size:1.1em;">&nbsp;C1&nbsp;</span>
                        </div>
                        <div class="col text-center">
                            <p>
                            <strong>Categoría 2</strong>
                            </p>
                            <i class='fa fa-user-circle' style="font-size:2em; color:#fd7e14;" ></i>
                            <br />
                             <span class="badge badge-pill bg-orange badge-count" style="color: white !important; font-size:1.1em;">&nbsp;C2&nbsp;</span>
                        </div>
                        <div class="col text-center">
                            <p>
                            <strong>Categoría 3</strong>
                            </p>
                            <i class='fa fa-user-circle' style="font-size:2em; color:#ffc107;" ></i>
                            <br />
                             <span class="badge badge-pill bg-warning badge-count" style="color: white !important; font-size:1.1em;">&nbsp;C3&nbsp;</span>
                        </div>
                        <div class="col  text-center">
                            <p>
                            <strong>Categoría 4</strong>
                            </p>
                            <i class='fa fa-user-circle' style="font-size:2em; color:#28a745;" ></i>
                            <br />
                             <span class="badge badge-pill bg-success badge-count" style="color: white !important; font-size:1.1em;">&nbsp;C4&nbsp;</span>
                        </div>
                        <div class="col text-center">
                            <p>
                            <strong>Categoría 5</strong>
                            </p>
                            <i class='fa fa-user-circle' style="font-size:2em; color:#17a2b8;" ></i>
                            <br />
                             <span class="badge badge-pill bg-info badge-count" style="color: white !important; font-size:1.1em;">&nbsp;C5&nbsp;</span>
                        </div>
                        <div class="col text-center">
                            <p>
                            <strong>Sin categoría</strong>
                            </p>
                            <i class='fa fa-user-circle' style="font-size:2em; color:#000000;" ></i>
                            <br />
                             <span class="badge badge-pill bg-default badge-count" style="font-size:1.1em;" >&nbsp;SC&nbsp;</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fin iconos-->
            <div class="row mt-3">
                <div class="col">
                    <div class="row">
                        <div class="col-md-10" id="DivSelectTipoAtencion">
                            <select class="form-control" id="selectTipoAtencion" data-required="true">
                            </select>
                        </div>
                        <div class="col-md-2">
                            <a class="btn btn-primary" id="btnBuscarCamas">
                                <i class="fa fa-search"></i>
                                Buscar
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h4 class='mt-1'><i class="fas fa-arrow-left randl"></i>&nbsp;&nbsp; <b>Seleccione una opción para buscar.</b></h4>
                </div>
            </div>
            <!--mapa de camas-->
            <div id="divGeneralMapaCama">
                <div id="infoInicial" class="mt-5"><h1 class='text-center w-100'><b><i class='fa fa-info-circle'></i>No hay realizado una búsqueda.</b></h1></div>
            </div>

            <div class="fixed-bottom text-right">
                <a onclick="$('html, body').animate({ scrollTop: $('body').offset().top}, 500);">
                    <i class="fa fa-arrow-up fa-2x" style="cursor:pointer;"></i>
                </a>
            </div>

        </div>
    </div>
    <!--Fin Iconografía -->

    <!--Mapa de camas-->
    <div id="ContCamasUrg" class="row">
            
    </div>
    <!--Fin mapa de camas-->

    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/VistaMapaCamasUrgencia.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/BandejaUrgencia.js") %>"></script>
</asp:Content>
 