﻿using Newtonsoft.Json;
using RegistroClinico.Datos.DTO;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;

namespace RegistroClinico.Vista.ModuloUrgencia
{
    public partial class ImprimirDAU : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            // Valida sesión según los permisos que tenga el perfil
            NegSession negSession = new NegSession(Page);
            negSession.ValidarPermisosSession();
            try
            {

                HttpContext context = HttpContext.Current;
                string dominio = Funciones.GetUrlWebApi();
                string idDAU = Request.QueryString["id"];
                string url = string.Format("{0}URG_Atenciones_Urgencia/{1}?" +
                                                "extens=ATENCIONMEDICA&" +
                                                "extens=SIGNOSVITALES&" +
                                                "extens=CATEGORIZACION&" +
                                                "extens=MEDICO&" +
                                                "extens=PROFESIONALES&" +
                                                "extens=GINECOOBSTETRICO&" +
                                                "extens=ALTA&" +
                                                "extens=INDICACIONESCLINICAS", dominio, idDAU);

                string token = string.Format("Bearer {0}", Session["TOKEN"]/*Page.Request.Cookies["DATA-TOKEN"].Value*/);
                string usuarioLogeado = Session["LOGIN_USUARIO"].ToString();
                string sJson = Funciones.ObtenerJsonWebApi(url, token);
                AtencionUrgenciaDto atencionUrgencia = JsonConvert.DeserializeObject<AtencionUrgenciaDto>(sJson);
                
                // PACIENTE
                DibujarDatosPaciente(atencionUrgencia);

                // CATEGORIZACIÓN
                if (atencionUrgencia.Categorizacion != null)
                {
                    if (atencionUrgencia.Categorizacion.Count > 0)
                    {
                        DibujarCategorizacion(atencionUrgencia);
                    }
                    else {
                        pnlCategorizacion.Visible = false;
                    }
                }
                else
                {
                    pnlCategorizacion.Visible = false;
                }

                // ATENCIÓN ADMINISTRATIVA
                DibujarAtencionAdministrativa(atencionUrgencia);
                pnlClinicoObstetrico.Visible = false;

                // CLÍNICOS
                if (atencionUrgencia.AtencionMedica != null)
                {

                    if (atencionUrgencia.AtencionMedica.Descripcion.Count > 0)
                    {
                        AtencionesUrgenciaMedico aum = atencionUrgencia.AtencionMedica.Descripcion[atencionUrgencia.AtencionMedica.Descripcion.Count - 1];
                        lblAnamnesis.Text = aum.Anamnesis != "" ? aum.Anamnesis : "<br/>";
                        lblExamenFisico.Text = aum.ExamenFisico ?? "<br />";
                        lblHipotesisDiagnostica.Text = aum.HipotesisDiagnostica ?? "<br />";
                        lblIndicacionesMedicas.Text = aum.IndicacionMedicas ?? "<br />";
                    } else {
                        lblAnamnesis.Text = "<br/>";
                        lblExamenFisico.Text = "<br/>";
                        lblHipotesisDiagnostica.Text = "<br/>";
                        lblIndicacionesMedicas.Text = "<br/>";
                    }

                    if (atencionUrgencia.AtencionMedica.Alcolemia != null)
                    {
                        if (atencionUrgencia.AtencionMedica.Alcolemia.ToString() == "SI")
                        {
                            lblAlcoholemiaSi.Text += "SI";
                            lblNumFrasco.Text = atencionUrgencia.AtencionMedica.NumeroFrascosAlcolemia.ToString();
                            lblFechaHoraAlcoholemia.Text = Convert.ToString(atencionUrgencia.AtencionMedica.FechaHoraAlcolemia);
                        }
                        else
                        {
                            lblAlcoholemiaSi.Text += "NO";
                            lblNumFrasco.Text = "<br />";
                            lblFechaHoraAlcoholemia.Text = "<br />";
                        }
                    }
                    else
                    {
                        lblAlcoholemiaSi.Text += "--";
                        lblNumFrasco.Text = "--";
                        lblFechaHoraAlcoholemia.Text = "--";
                    }

                }

                if (atencionUrgencia.SignosVitales != null)
                    DibujarSignosVitales(atencionUrgencia);

                DibujarSolicitudProcedimiento(atencionUrgencia);
                DibujarInterconsulto(atencionUrgencia);
                DibujarIndicacionMedicamento(atencionUrgencia);
                
                if (atencionUrgencia.Alta != null)
                    DibujarAltaMedica(atencionUrgencia);

                if (atencionUrgencia.AtencionMedica.MedicoAtencion != null)
                {
                    string medicoTurno = atencionUrgencia.AtencionMedica.MedicoAtencion.Nombre + " " + 
                                            atencionUrgencia.AtencionMedica.MedicoAtencion.ApellidoPaterno + " " + 
                                            (atencionUrgencia.AtencionMedica.MedicoAtencion.ApellidoMaterno ?? "");
                    lblMedicoAtencion.Text = medicoTurno;


                    //Calculo tiempo espera 
                    if (atencionUrgencia.AtencionMedica.FechaHoraAtencion != null && lblFechaCategorizacion.Text != "")
                    {
                        lblFechaHoraAtencion.Text = ((DateTime)atencionUrgencia.AtencionMedica.FechaHoraAtencion).ToString("dd-MM-yyyy HH:mm:ss");
                        DateTime? fechaAtencion = atencionUrgencia.AtencionMedica.FechaHoraAtencion;
                        DateTime fechaCat = DateTime.Parse(lblFechaCategorizacion.Text);
                        //tiempo de atencion transcurrido en minutos.
                        string tiempoEspera = Math.Round(fechaCat.Subtract((DateTime)fechaAtencion).TotalMinutes).ToString();
                        //comprueba si es mas de 60 minutos
                        if (Int32.Parse(tiempoEspera) > 60)
                        {
                            tiempoEspera = Math.Round(fechaCat.Subtract((DateTime)fechaAtencion).TotalHours).ToString();
                            lblTiempoEsperaAtencionMedica.Text = tiempoEspera + " Horas";
                        }
                        else
                        {
                            //Tiempo en minutos.
                            lblTiempoEsperaAtencionMedica.Text = tiempoEspera + " Minutos";
                        }
                    }

                }

                Funciones funciones = new Funciones();
                string doc = "DAU";
                funciones.ExportarPDFWK(this.Page, doc, idDAU, usuarioLogeado);

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al imprimir DAU", ex.Message + ex.StackTrace + ex.InnerException);
            }

        }

        private void DibujarIndicacionMedicamento(AtencionUrgenciaDto atencionUrgencia)
        {
            if (atencionUrgencia.IndicacionesClinicas.Medicamentos.Count > 0)
            {
                StringBuilder html = new StringBuilder();
                html.AppendLine("<table class='table table-striped table-bordered'>");
                html.AppendLine("<tr>");
                html.AppendLine("<th>Medicamentos en Box</th>");
                html.AppendLine("<th>F. Solicitud</th>");
                html.AppendLine("<th>U. Solicita</th>");
                html.AppendLine("<th>F. Cierre</th>");
                html.AppendLine("<th>U. Cierra</th>");
                html.AppendLine("</tr>");
                html.AppendLine("<tr>");

                foreach (var item in atencionUrgencia.IndicacionesClinicas.Medicamentos)

                {
                    string usuarioSolicitante = item.UsuarioSolicitante ?? "No especificado";
                    string usuarioCierra = (string)item.UsuarioCierra ?? "No Especificado";
                    html.AppendLine("<tr>");
                    html.AppendLine("<td>" + item.DescripcionMedicamento + "</td>");
                    html.AppendLine("<td>" + item.FechaIngreso + "</td>");
                    html.AppendLine("<td>" + usuarioSolicitante + "</td>");
                    html.AppendLine("<td>" + item.FechaCierre + "</td>");
                    html.AppendLine("<td>" + usuarioCierra + "</td>");

                    html.AppendLine("</tr>");
                }
                html.AppendLine("</table>");
                lblIndicacionesClinicas.Text += html.ToString();
            }
        }
        private void DibujarInterconsulto(AtencionUrgenciaDto atencionUrgencia)
        {
            if (atencionUrgencia.IndicacionesClinicas.Interconsultor.Count > 0)
            {
                StringBuilder html = new StringBuilder();
                html.AppendLine("<table class='table table-striped table-bordered'>");
                html.AppendLine("<tr>");
                html.AppendLine("<th>Especialidad Interconsultor</th>");
                html.AppendLine("<th>F. Solicitud</th>");
                html.AppendLine("<th>U. Solicita</th>");
                html.AppendLine("<th>F. Cierre</th>");
                html.AppendLine("<th>U. Cierra</th>");
                html.AppendLine("</tr>");
                html.AppendLine("<tr>");
                foreach (var item in atencionUrgencia.IndicacionesClinicas.Interconsultor)
                {
                    html.AppendLine("<tr>");
                    html.AppendLine("<td>" + item.DescripcionTipoConsultor + "</td>");
                    html.AppendLine("<td>" + item.Fecha + "</td>");
                    html.AppendLine("<td>" + item.LoginUsuarioSolicita + "</td>");
                    html.AppendLine("<td>" + item.FechaCierre + "</td>");
                    html.AppendLine("<td>" + item.LoginUsuarioRealiza + "</td>");

                    html.AppendLine("</tr>");
                }
                html.AppendLine("</table>");
                lblIndicacionesClinicas.Text += html.ToString();
            }
        }

        private void DibujarSolicitudProcedimiento(AtencionUrgenciaDto atencionUrgencia)
        {
            if (atencionUrgencia.IndicacionesClinicas.Procedimientos.Count > 0)
            {

                StringBuilder html = new StringBuilder();
                html.AppendLine("<table style='width: 100%;border: 1px solid black;margin: 0px !important;'>");
                html.AppendLine("<tr>");
                html.AppendLine("<td class='fondo-titulos contenido-celda col-td-4'>Procedimiento</td>");
                html.AppendLine("<td class='fondo-titulos contenido-celda col-td-2'>F. Solicitud</td>");
                html.AppendLine("<td class='fondo-titulos contenido-celda col-td-1'>Solicitante</td>");
                html.AppendLine("<td class='fondo-titulos contenido-celda col-td-2'>F. Realización</td>");
                html.AppendLine("<td class='fondo-titulos contenido-celda col-td-1'>Cierre</td>");
                html.AppendLine("</tr>");

                foreach (var item in atencionUrgencia.IndicacionesClinicas.Procedimientos)
                {

                    string usuarioSolicitante = GetInicialesPersona(item.ProfesionalSolicitante);
                    string usuarioCierra = GetInicialesPersona(item.ProfesionalRealiza);

                    html.AppendLine("<tr>");
                    html.AppendLine("<td class='contenido-celda'>" + item.Descripcion + "</td>");
                    html.AppendLine("<td class='contenido-celda'>" + item.FechaIngreso + "</td>");
                    html.AppendLine("<td class='contenido-celda'>" + usuarioSolicitante + "</td>");
                    html.AppendLine("<td class='contenido-celda'>" + item.FechaRealizacion + "</td>");
                    html.AppendLine("<td class='contenido-celda'>" + usuarioCierra + "</td>");
                    html.AppendLine("</tr>");
                }
                html.AppendLine("</table>");
                lblIndicacionesClinicas.Text += html.ToString();
            }
        }

        private string GetInicialesPersona(string personaStr)
        {

            if (personaStr != null)
            {
                personaStr = (personaStr.IndexOf("|") > -1)
                                ? personaStr.Substring(0, personaStr.IndexOf("|")).Trim()
                                : personaStr;
            }
            else
            {
                return "";
            }

            string iniciales = "";
            string[] arrayNombres = (personaStr.IndexOf(" ") > -1) ? personaStr.Split(' ') : new string[] { personaStr };
            
            foreach (string persona in arrayNombres)
            {
                iniciales += persona.ToCharArray()[0] + ". ";
            }
        
            return iniciales.Trim();

        }

        private void DibujarSignosVitales(AtencionUrgenciaDto atencionUrgencia)
        {
            string htmlSignosVitales = "", fechaHoraSignoVitales = "", signoVital = "";

            for (int i = 0; i < atencionUrgencia.SignosVitales.Count; i++)
            {

                if (fechaHoraSignoVitales != atencionUrgencia.SignosVitales[i].FechaHoraTipoMedida.ToString())
                {
                    fechaHoraSignoVitales = atencionUrgencia.SignosVitales[i].FechaHoraTipoMedida.ToString();
                    htmlSignosVitales += "<tr>";
                    htmlSignosVitales += "<td class='fondo-titulos contenido-celda col-td-12 text-center' colspan='6'>" +
                                            "<strong>" +
                                                Convert.ToString(atencionUrgencia.SignosVitales[i].FechaHoraTipoMedida) +
                                            "</strong>" +
                                         "</td>";
                    htmlSignosVitales += "</tr>";
                }

                signoVital += GetBloqueAntecedenteClinico(
                                       Convert.ToString(atencionUrgencia.SignosVitales[i].NombreTipoMedida),
                                       Convert.ToString(atencionUrgencia.SignosVitales[i].ValorTipoMedida));

                if ((((i + 1) % 3) == 0) || ((i + 1) == atencionUrgencia.SignosVitales.Count))
                {
                    for (int j = i + 1; (j % 3) != 0; j++)
                        signoVital += "<td class='contenido-celda col-td-2'><br /></td><td class='contenido-celda col-td-2'><br /></td>";

                    htmlSignosVitales += "<tr>" + signoVital + "</tr>";
                    signoVital = "";
                }
            }

            lblSignosVitales.Text = (htmlSignosVitales == "") ? "<tr><td class='contenido-celda col-td-12' colspan='6'><br /></td></tr>" :
                                                                "<tr>" + htmlSignosVitales + "</tr>";

        }

        private void DibujarAtencionAdministrativa(AtencionUrgenciaDto atencionUrgencia)
        {
            var dataAdmin = atencionUrgencia.AtencionAdministrativa;
            string fechaLlegada = Convert.ToString(dataAdmin.FechaLlegada);

            lblDAU.Text = lblDAU2.Text = dataAdmin.IdAtencionUrgencia.ToString();
            lblFechaLlegada.Text = fechaLlegada;
            lblTipoAtencion.Text = dataAdmin.NombreTipoAtencion;
            lblTipodeAccidente.Text = dataAdmin.NombreTipoAccidente ?? "No especificado";
            lblLugarAccidente.Text = dataAdmin.LugarAccidente ?? "No especificado";

            lblMotivoConsulta.Text = dataAdmin.MotivoConsulta;
            lblMedioTransporte.Text = dataAdmin.NombreMedioTransporte;
            lblNombreAdmisor.Text = dataAdmin.NombreAdmisor.Nombre;
            lblFechaAdmision.Text = fechaLlegada;
            lblPrevisionPacConst.Text = lblPrevisionPaciente2.Text = dataAdmin.NombrePrevision + " " + (dataAdmin.NombrePrevisionTramo ?? "");

            if (atencionUrgencia.Acompañante != null)
            {
                lblAcompañante.Text = "RUT "+ atencionUrgencia.Acompañante.Rut + ((atencionUrgencia.Acompañante.Digito != null) ? "-" + atencionUrgencia.Acompañante.Digito : "/");
                lblAcompañante.Text += atencionUrgencia.Acompañante.Nombre + " " ?? "";
                lblAcompañante.Text += atencionUrgencia.Acompañante.ApellidoPaterno + " " ?? "";
                lblAcompañante.Text += atencionUrgencia.Acompañante.ApellidoMaterno + " " ?? "";
            }

        }

        private void DibujarDatosPaciente(AtencionUrgenciaDto atencionUrgencia)
        {

            if (atencionUrgencia.Paciente != null)
            {
                NegLog.GuardarLog("Paso", "1");
                var dataPaciente = atencionUrgencia.Paciente;
                lblidentificacion.Text = lblidentificacion2.Text = dataPaciente.DescripcionIdentificacion;
                lblRutPac.Text = dataPaciente.NumeroDocumento;
                NegLog.GuardarLog("Paso", "2");

                if (dataPaciente.Digito != null)
                    lblRutPac.Text += "-" + dataPaciente.Digito;
                lblRutPac2.Text = lblRutPac.Text;

                if (dataPaciente.Nombre != null)
                    lblNombrePac.Text = dataPaciente.Nombre + " ";

                if (dataPaciente.ApellidoPaterno != null)
                    lblNombrePac.Text += dataPaciente.ApellidoPaterno + " ";

                if (dataPaciente.ApellidoMaterno != null)
                    lblNombrePac.Text += dataPaciente.ApellidoMaterno + " ";
                lblNombrePac2.Text = lblNombrePac.Text;
                lblDireccionPacIPD.Text = lblDireccionPacIPD2.Text = dataPaciente.DireccionCalle + " " + Convert.ToString(dataPaciente.DireccionNumero);
                lblFono.Text = lblTelefono2.Text = Convert.ToString(dataPaciente.Telefono);
                lblCiudad.Text = lblCiudad2.Text = Convert.ToString(dataPaciente.NombreCiudad);
                lblSexo.Text = lblSexo2.Text = Convert.ToString(dataPaciente.NombreSexo);
                lblGenero.Text = lblGenero2.Text = Convert.ToString(dataPaciente.NombreGenero);

                if (dataPaciente.FechaNacimiento != null)
                {
                    
                    lblFechaNacimiento.Text = lblFechaNacimiento2.Text = ((DateTime)dataPaciente.FechaNacimiento).ToString("dd-MM-yyyy");
                    Dictionary<string, object> edad = JsonConvert.DeserializeObject<Dictionary<string, object>>(Funciones.CalculaEdad(((DateTime)dataPaciente.FechaNacimiento).ToString("yyyy-MM-dd")));
                    string años = "";
                    if (edad["años"].ToString() != "0")
                        años = edad["años"].ToString() + ((edad["años"].ToString() == "1") ? " año" : " años");
                    else if (edad["meses"].ToString() != "0")
                        años = edad["meses"].ToString() + ((edad["meses"].ToString() == "1") ? " mes" : " meses");
                    else if (edad["dias"].ToString() != "0")
                        años = edad["dias"].ToString() + ((edad["dias"].ToString() == "1") ? " día" : " días");
                    else
                        años = edad["edad"].ToString();

                    lblEdad.Text = lblEdad2.Text = años;

                }
            }
            else
            {

            }
        }

        private void DibujarCategorizacion(AtencionUrgenciaDto atencionUrgencia)
        {
            var dataCategorizacion = atencionUrgencia.Categorizacion[0];
            pnlErrorCategorizacion.Visible = false;
            lblCategorizacion.Text = dataCategorizacion.Codigo;
            lblFechaCategorizacion.Text = DateTime.Parse(dataCategorizacion.FechaHora.ToString()).ToString("dd-MM-yyyy HH:mm:ss");

            /*****Calculo de tiempo de espera desde la llegada hasta la categorización*****/
            //Comprueba que tenga fecha de llegada y fecha de categorizacion
            if (atencionUrgencia.AtencionAdministrativa.FechaLlegada != null && dataCategorizacion.FechaHora != null)
            {
                DateTime fechaLlegada = atencionUrgencia.AtencionAdministrativa.FechaLlegada;
                DateTime fechaCat = (DateTime)dataCategorizacion.FechaHora;
                //tiempo de atencion transcurrido en minutos.
                string diferenciaMinutos = Math.Round(fechaCat.Subtract(fechaLlegada).TotalMinutes).ToString();

                //comprueba si es mas de 60 minutos
                if (Int32.Parse(diferenciaMinutos) > 60) {
                    String diferenciaHoras = Math.Round(fechaCat.Subtract(fechaLlegada).TotalHours).ToString();
                    lblTiempoEsperaCategorizacion.Text = diferenciaHoras + " Horas";
                }
                else {
                    //Tiempo en minutos.
                    lblTiempoEsperaCategorizacion.Text = diferenciaMinutos + " Minutos";
                }
            }
            if (dataCategorizacion.Usuario == null) { lblPriorizador.Text = "N/A"; } else { lblPriorizador.Text = dataCategorizacion.Usuario; }
        }

        private void DibujarAltaMedica(AtencionUrgenciaDto atencionUrgencia)
        {
            if (atencionUrgencia.Alta != null)
            {

                if (atencionUrgencia.MedicoAlta != null) {
                    lblMedicoAlta.Text = atencionUrgencia.MedicoAlta.Nombre + " " + atencionUrgencia.MedicoAlta.ApellidoPaterno + " " + atencionUrgencia.MedicoAlta.ApellidoMaterno;
                }
                else {
                    lblMedicoAlta.Text = "No especificado";
                }
                lblDestinoAlta.Text = atencionUrgencia.Alta.Destino.Value;
                lblHoraTerminoAtencion.Text = atencionUrgencia.Alta.Usuarios.Hora.ToString();
                //lblMedicoAlta.Text = atencionUrgencia.Alta.Medico ?? "No Especificado";
                lblIndicacionesAlta.Text = atencionUrgencia.Alta.Indicaciones;
                /*.Text = "<table class='table table-striped table-bordered'>";
                ltlDatosAlta.Text += "   {0}";
                ltlDatosAlta.Text += "</table>";*/
                StringBuilder html = new StringBuilder();

                /*lblDestinoAlta.Text = atencionUrgencia.Alta.Destino.Value.ToString();
                if (atencionUrgencia.Alta.Destino.Hospitalizacion.Id != null)
                {
                    lblIndicacionesAlta.Text += "<br>Ubicación / Establecimiento: " + atencionUrgencia.Alta.Destino.Hospitalizacion.Valor;
                }
                else if (atencionUrgencia.Alta.Destino.OtroEstablecimiento.Valor != null)
                {
                    lblIndicacionesAlta.Text += "<br>Establecimiento: " + atencionUrgencia.Alta.Destino.OtroEstablecimiento.Valor;
                }
                html.AppendLine("</tr>");*/

            }
        }

        public string GetHost()
        {
            return Funciones.GetHost(HttpContext.Current);
        }

        private string GetBloqueAntecedenteClinico(string nombreAntecedenteClinico, string antecedenteClinico)
        {
            return (antecedenteClinico != "") ? string.Format("<td class='fondo-titulos contenido-celda col-td-2'><strong>{0}</strong></td>" +
                                                                "<td class='contenido-celda col-td-2'>{1}</td>", nombreAntecedenteClinico, antecedenteClinico) : "";
        }

    }

    #region Clases

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Establecimiento
    {
        public Nullable<int> IdEstablecimiento { get; set; }
        public string RutEstablecimiento { get; set; }
        public string NombreEstablecimiento { get; set; }
        public Nullable<int> IdServicioSalud { get; set; }
    }

    public class Acompañante
    {
        public Nullable<int> Id { get; set; }
        public string Rut { get; set; }
        public string Digito { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
    }

    public class NombreAdmisor
    {
        public string Nombre { get; set; }
    }

    public class AtencionAdministrativa
    {
        public Nullable<int> IdAtencionUrgencia { get; set; }
        public Nullable<int> IdTipoAtencion { get; set; }
        public string NombreTipoAtencion { get; set; }
        public Nullable<int> IdMedioTransporte { get; set; }
        public string NombreMedioTransporte { get; set; }
        public Nullable<int> IdTipoAccidente { get; set; }
        public string NombreTipoAccidente { get; set; }
        public string LugarAccidente { get; set; }
        public string MotivoConsulta { get; set; }
        public DateTime FechaLlegada { get; set; }
        public Nullable<int> IdPrevision { get; set; }
        public string NombrePrevision { get; set; }
        public Nullable<int> IdPrevisionTramo { get; set; }
        public string NombrePrevisionTramo { get; set; }
        public Nullable<int> IdTipoEstadoSistema { get; set; }
        public string NombreTipoEstadoSistema { get; set; }
        public string Procedimiento { get; set; }
        public NombreAdmisor NombreAdmisor { get; set; }
    }

    public class AtencionMedica
    {
        public Nullable<DateTime> FechaHoraAtencion { get; set; }
        public string Anamnesis { get; set; }
        public object Alcolemia { get; set; }
        public object FechaHoraAlcolemia { get; set; }
        public object NumeroFrascosAlcolemia { get; set; }
        public string IndicacionMedicas { get; set; }
        public string HipotesisDiagnostica { get; set; }
        public string ExamenFisico { get; set; }
        public List<object> AtencionObstetrica { get; set; }
        public MedicoAtencion MedicoAtencion { get; set; }
        public List<AtencionesUrgenciaMedico> Descripcion { get; set; }
    }
    public class MedicoAtencion
    {
        public string NumeroDocumento { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
    }
    public class IndicacionesClinicas
    {
        public List<Medicamento> Medicamentos { get; set; }
        public List<Interconsultor> Interconsultor { get; set; }
        public List<Procedimiento> Procedimientos { get; set; }
    }

    public class Interconsultor
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public int IdUsuarioSolicita { get; set; }
        public string LoginUsuarioSolicita { get; set; }
        public object IdUsuarioRealiza { get; set; }
        public object LoginUsuarioRealiza { get; set; }
        public string AtencionConsultorCerrada { get; set; }
        public int IdTipoConsultor { get; set; }
        public string DescripcionTipoConsultor { get; set; }
        public string ActividadSolicitada { get; set; }
        public object ActividadRealizada { get; set; }
        public object FechaCierre { get; set; }
    }
    public class Medicamento
    {
        public int IdMedicamento { get; set; }
        public string DescripcionMedicamento { get; set; }
        public string UsuarioSolicitante { get; set; }
        public object UsuarioCierra { get; set; }
        public int IdTipoEstado { get; set; }
        public DateTime FechaIngreso { get; set; }
        public object FechaCierre { get; set; }
        public object Observaciones { get; set; }
        public int IdViaAdministracion { get; set; }
        public string DescripcionViaAdministracion { get; set; }
        public string Dosis { get; set; }
        public string Frecuencia { get; set; }
        public Acciones Acciones { get; set; }
    }
    public class SignosVitale
    {
        public Nullable<int> IdTipoMedida { get; set; }
        public Nullable<DateTime> FechaHoraTipoMedida { get; set; }
        public string NombreTipoMedida { get; set; }
        public string ValorTipoMedida { get; set; }
    }

    public class DiagnosticosAlta
    {
        public Nullable<int> Id { get; set; }
        public string Valor { get; set; }
    }

    public class Hospitalizacion
    {
        public object Id { get; set; }
        public object Valor { get; set; }
    }

    public class OtroEstablecimiento
    {
        public object Id { get; set; }
        public object Valor { get; set; }
    }

    public class Destino
    {
        public Nullable<int> Id { get; set; }
        public string Value { get; set; }
        public Hospitalizacion Hospitalizacion { get; set; }
        public OtroEstablecimiento OtroEstablecimiento { get; set; }
    }

    public class Medico
    {
        public string NumeroDocumento { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
    }


    public class Alta
    {
        public List<DiagnosticosAlta> DiagnosticosAlta { get; set; }
        public string Indicaciones { get; set; }
        public Destino Destino { get; set; }
        public DateTime? AltaMedica { get; set; }
        public ProfesionalAlta ProfesionalAlta { get; set; }
        public List<Medico> Medico { get; set; }
        public Usuarios Usuarios { get; set; }
    }

    public class AtencionUrgenciaDto
    {
        public Establecimiento Establecimiento { get; set; }
        public PacienteDto Paciente { get; set; }
        public Acompañante Acompañante { get; set; }
        public AtencionAdministrativa AtencionAdministrativa { get; set; }
        public List<CategorizacionDto> Categorizacion { get; set; }
        public AtencionMedica AtencionMedica { get; set; }
        public List<SignosVitale> SignosVitales { get; set; }
        public Alta Alta { get; set; }
        public Medico MedicoAlta { get; set; }
        public IndicacionesClinicas IndicacionesClinicas { get; set; }

    }

    public class AtencionesUrgenciaMedico
    {
        public string Profesional { get; set; }
        public DateTime FechaHora { get; set; }
        public string Anamnesis { get; set; }
        public string IndicacionMedicas { get; set; }
        public string HipotesisDiagnostica { get; set; }
        public string ExamenFisico { get; set; }
    }

    public class Acciones
    {
        public bool Cerrar { get; set; }
        public bool Quitar { get; set; }
        public bool Ver { get; set; }
        public bool Editar { get; set; }
    }

    public class Procedimiento
    {
        public int IdAtencionCarteraArancel { get; set; }
        public int IdCarteraArancel { get; set; }
        public int IdCartera { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionArancel { get; set; }
        public string UsuarioSolicitante { get; set; }
        public object UsuarioCierra { get; set; }
        public string ProfesionalSolicitante { get; set; }
        public string ProfesionalRealiza { get; set; }
        public int IdTipoEstado { get; set; }
        public DateTime FechaIngreso { get; set; }
        public object FechaRealizacion { get; set; }
        public object Observaciones { get; set; }
        public Acciones Acciones { get; set; }
    }

    public class Usuarios {
        public DateTime Hora { get; set; }
    }

    #endregion

}