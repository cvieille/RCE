﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="DashboardUrgencia.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.DashboardUrgencia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Dashboard Urgencia</h1>
                </div>
                <div class="col-sm-6">
                    <br />
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4>Atenciones por Estado</h4>
                    </div>
                    <div class="card-body">
                        <table id="tblEstadosUrgencia" class="table table-striped table-hover" ></table>
                    </div>
                    <div class="card-footer">
                        Total: <span id="spTotalEstadosUrgencia"></span>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                <div class="card ">
                    <div class="card-header bg-info">
                        <h4>Atenciones por Tipo</h4>
                    </div>
                    <div class="card-body">
                        <table id="tblTipoUrgencia" class="table table-striped table-hover"></table>
                    </div>
                    <div class="card-footer">
                        Total: <span id="spTotalTipoUrgencia"></span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                <div class="card ">
                    <div class="card-header bg-info">
                        <h4>Atenciones Categorizados</h4>
                    </div>
                    <div class="card-body">
                        <table id="tblCategorizacionUrgencia" class="table table-striped table-hover"></table>
                    </div>
                    <div class="card-footer">
                        Total: <span id="spTotalCategorizados"></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="mdlDetalle" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detalle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="tblDetalle" class="table table-bordered table-hover dataTable no-footer w-100">
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </section>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/Dashboard/DashboardUrgencia.js") + "?" + GetVersion() %>"></script>
</asp:Content>
