﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BandejaUrgencia.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.BandejaUrgencia" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalMovimientoPaciente.ascx" TagName="mdMMP" TagPrefix="MMP" %>
<%@ Register Src="~/Vista/UserControls/AtencionClinica.ascx" TagName="AtencionClinica" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalSignosVitales.ascx" TagName="ModalSignosVitales" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalHospitalizaciones.ascx" TagName="VerHospitalizaciones" TagPrefix="wuc" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/bootstrap-select.css" rel="stylesheet" />
    <style>
        .small-box {
            margin-bottom: 0px;
        }

        .info-box {
            min-height: unset;
            padding: unset;
            margin-bottom: unset;
        }

        .badge-orange {
            color: #ffffff !important;
            background-color: orange !important;
        }

        .dropdown.bootstrap-select .dropdown-toggle {
            border: 1px solid lightgray;
        }
    </style>
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-select.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>
    <script>

        $(document).ready(function () {

            $('#lnkExportarMovIngresoUrgencia').click(function () {
                var tblObj = $('#tblMovimientosIngresoUrgencia').DataTable();
                __doPostBack("<%= btnExportarMovIngresoUrgencia.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });

            $('#lnbExportar').click(function () {
                __doPostBack("<%= lnbExportarUrg.UniqueID %>", JSON.stringify(datosExportarURG));
            });

        });

    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">

    <!--Modal movimiento por paciente-->
    <MMP:mdMMP ID="MMP1" runat="server" />
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:ModalSignosVitales ID="ModalSignosVitales" runat="server" />
    <wuc:VerHospitalizaciones ID="mdlVerHospitalizaciones" runat="server" />

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Urgencia</h1>
                </div>
                <div class="col-sm-6">
                    <br />
                </div>
            </div>
        </div>
    </section>

    <section class="content">

        <div id="divBandejaUrgencia" class="tab-pane fade show active" role="tabpanel" aria-labelledby="home-tab-just">
            <button id="btnFiltroCollapse" data-toggle="collapse" class="btn btn-outline-primary container-fluid" data-target="#divFiltroUrg" onclick="return false;">
                FILTRAR BANDEJA
            </button>
            <div id="divFiltroUrg" class="collapse p-3">

                <div class="row">
                    <div class="col-md-2">
                        <label>Identificación</label>
                        <select id="sltIdentificacionPaciente" name="Identificacion" class="form-control">
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label id="lblTipoIdentificacion" for="txtHosFiltroRut"></label>
                        <label id="lblIdentificacionPaciente" for="sltIdentificacionPaciente"></label>
                        <div class="input-group">
                            <input id="txtnumeroDocPaciente" name="Numero-Documento" type="text" class="form-control numero-documento" style="width: 100px;"
                                data-required="false" />
                            <div class="input-group-prepend digito">
                                <div class="input-group-text">
                                    <strong>-</strong>
                                </div>
                            </div>
                            <input id="txtDigPaciente" type="text" class="form-control digito text-center" maxlength="1" style="width: 15px;"
                                placeholder="DV" disabled />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label for="txtnombrePaciente">Nombre</label>
                        <input id="txtNombrePaciente" type="text" name="Nombre" class="form-control datos-persona" maxlength="50"
                            placeholder="Nombre del Paciente" />
                    </div>
                    <div class="col-md-3">
                        <label for="txtApePatPaciente">Apellido paterno</label>
                        <input id="txtApePatPaciente" type="text" name="Apellido-Paterno" class="form-control datos-persona" maxlength="50"
                            placeholder="Apellido paterno del Paciente" />
                    </div>
                    <div class="col-md-2">
                        <label for="txtApeMatPaciente">Apellido Materno</label>
                        <input id="txtApeMatPaciente" type="text" name="Apellido-Materno" class="form-control datos-persona" maxlength="50"
                            placeholder="Apellido materno del Paciente" />
                    </div>

                </div>

                <div class="row mt-2">
                    <div class="col-md-3">
                        <label>Tipo de urgencia</label>
                        <select id="sltTipoUrg" class="selectpicker" name="Tipo-Urgencia" data-width="100%" data-actions-box="true" multiple title="Seleccione">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Prioridad</label>
                        <select id="sltPrioridad" class="selectpicker" name="Prioridad" data-width="100%" data-actions-box="true" multiple title="Seleccione">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Estado</label>
                        <select id="sltEstado" name="Estado" class="form-control">
                        </select>
                    </div>

                </div>

                <div class="row mt-3">
                    <div class="col-md-8" id="avisoFiltros"></div>
                    <div class="col-md-4 text-right">
                        <a id="btnUrgFiltro" class="btn btn-info"><i class="fa fa-search"></i>Buscar</a>
                        <a id="btnUrgLimpiarFiltro" class="btn btn-warning"><i class="fa fa-eraser"></i>Limpiar Filtros</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="post">
                            <div class="text-right">
                                <asp:Button runat="server" ID="lnbExportarUrg" OnClick="lnbExportarUrg_Click" Style="display: none;" />
                                <a id="lnbExportar" class="btn btn-info waves-effect waves-light" href="#\">
                                    <i class="fa fa-file-excel"></i>Exportar XLS
                                </a>
                                <a id="lblNuevoIngreso" style="display: none;" class="btn btn-info" href="NuevoIngreso.aspx">
                                    <i class="fa fa-plus"></i>Nuevo ingreso
                                </a>
                                <a id="btnRefrescar" class="btn btn-primary" href="BandejaUrgencia.aspx"><i class="fa fa-refresh"></i>Refrescar</a>
                            </div>
                        </div>
                        <div class="post">
                            <table id="tblUrgencia" class="table table-striped table-bordered table-hover dataTable" style="width: 100%;"></table>
                        </div>
                        <div class="card" style="padding: 10px; background: rgb(240, 240, 240);">
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-danger badge-count" style="color: white !important;">&nbsp;C1&nbsp;</span></h5>
                                    Categoría 1.
                                </div>
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-info badge-count" style="color: white !important;">&nbsp;P&nbsp;</span></h5>
                                    Pedíatrica.
                                </div>
                                <div class="col-md-3">
                                    <span class="fa fa-user" aria-hidden="true"></span>&nbsp;Paciente Acompañado
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-orange badge-count" style="color: white !important;">&nbsp;C2&nbsp;</span></h5>
                                    Categoría 2.
                                </div>
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-warning badge-count" style="color: white !important;">&nbsp;G&nbsp;</span></h5>
                                    Gineco-Obstétrica.
                                </div>
                                <div class="col-md-3">
                                    <i class="fa fa-medkit fa-lg text-red " aria-hidden="true"></i>
                                    Atención por Procedimiento
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-warning badge-count" style="color: white !important;">&nbsp;C3&nbsp;</span></h5>
                                    Categoría 3.
                                </div>
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-success badge-count" style="color: white !important;">&nbsp;A&nbsp;</span></h5>
                                    Adulto.
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-success badge-count" style="color: white !important;">&nbsp;C4&nbsp;</span></h5>
                                    Categoría 4.
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-info badge-count" style="color: white !important;">&nbsp;C5&nbsp;</span></h5>
                                    Categoría 5.
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-default badge-count" style="color: black !important;">&nbsp;SC&nbsp;</span></h5>
                                    Sin Categoría.
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="mdlAnularAtencion" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="card modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">ID Atención:
                        <label id="lblAtencionAnular"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning">
                        El registro se anulará y será quitado de la bandeja
                    </div>
                    <div>
                        <label>Motivo:</label>
                        <textarea id="txtMotivoAnulacion" class="form-control" data-required="true" maxlength="200" rows="5" style="resize: none;">                                            </textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" id="btnAnular">ANULAR</button>
                    <button class="btn btn-primary" data-dismiss="modal" aria-label="Close">CANCELAR</button>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlEgresarAtencion" class="modal fade">
        <div class="modal-dialog modal-fluid mt-0">
            <div class="card modal-content">
                <div class="modal-header modal-header-info center-horizontal">
                    <h4 class="heading form-inline">
                        <b>Alta / Egreso de Paciente</b>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <div id="divAltaPadre" class="row">
                                <div class="col-md-6 pr-3">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Número DAU:</label>
                                            <input id="txtDNumeroDau" type="text" class="form-control" disabled="disabled" />
                                        </div>
                                        <div class="col-md-9">
                                            <label>Nombre Paciente:</label>
                                            <input id="txtDPaciente" type="text" class="form-control" disabled="disabled" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Tipo de atención</label>
                                            <select id="sltProcedimiento" class="form-control" disabled="disabled">
                                                <option value="SI">Procedimiento</option>
                                                <option value="NO">Atención Clínica</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="divInfoClinicaUrgencia">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Médico tratante:</label>
                                                <input id="txtDMedicoTratante" type="text" class="form-control" disabled="disabled" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Hipótesis Diagnostica:</label>
                                                <textarea id="txaHipotesis" rows="5" class="form-control" maxlength="4000" disabled>
                                                </textarea>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Examen Fisico:</label>
                                                <textarea id="txaExamenFisico" rows="5" class="form-control" maxlength="4000" disabled>
                                                </textarea>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Anamnesis:</label>
                                                <textarea id="txaAnamnesis" rows="5" class="form-control" maxlength="4000" disabled>
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="alert alert-warning mt-3 text-center">
                                        <i class="fas fa-exclamation-triangle"></i>El paciente no posee un ingreso médico
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="divAltaEgreso" class="card">
                                        <div class="card-header text-center text-white bg-secondary">
                                            <p class="h3 mb-0">Detalles del alta/egreso Paciente</p>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <p class="h4">Tipo de egreso</p>
                                                    <input id="rdoTipoEgresoNormal" type="radio" name="TE" class="bootstrapSwitch" data-on-text="Clínico"
                                                        data-off-text="Clínico" data-on-color="success" data-size="large" />
                                                    <input id="rdoTipoEgresoIrregular" type="radio" name="TE" class="bootstrapSwitch" data-on-text="Administrativo"
                                                        data-off-text="Administrativo" data-on-color="warning" data-size="large" />
                                                </div>
                                            </div>
                                            <div id="divCondicionPaciente" class="row mt-3">
                                                <div class="col-md-12 text-center">
                                                    <p class="h4">Condición del paciente al final de la atención</p>
                                                    <input id="rdoVivo" type="radio" name="CP" class="bootstrapSwitch" data-on-text="Vivo"
                                                        data-off-text="Vivo" data-on-color="success" data-size="large" />
                                                    <input id="rdoFallecido" type="radio" name="CP" class="bootstrapSwitch" data-on-text="Fallecido"
                                                        data-off-text="Fallecido" data-on-color="warning" data-size="large" />
                                                </div>
                                            </div>
                                            <div id="divEgresoIrregular" class="mt-2">

                                                <p class="h4 text-center">Detalles del cierre de caso</p>

                                                <div class="row">
                                                    <div class="col-md-6 col-lg-4">
                                                        <label>Motivo de cierre</label>
                                                        <select id="sltMotivoCierre" class="form-control" data-required="true">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row mt-2">
                                                    <div class="col-md-12">
                                                        <label>Observaciones</label>
                                                        <textarea id="txtMotivoCierrePaciente" class="form-control" rows="4"
                                                            placeholder="Especifique motivo de cierre del caso" maxlength="100">
                                                        </textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div id="divEgresoNormal" class="mt-2">

                                                <p class="h4 text-center">Detalles del Alta</p>

                                                <div id="divEgresoMedico">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>Destino Alta</label>
                                                            <select id="sltDestinoAltaPaciente" class="form-control" data-required="true">
                                                            </select>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <label>Establecimiento</label>
                                                            <select id="sltEstablecimientoAltaPaciente" class="form-control" data-required="false">
                                                            </select>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <label>Ubicación</label>
                                                            <select id="sltUbicacionDerivado" class="form-control" data-required="false">
                                                            </select>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <label>Destino</label>
                                                            <select id="sltDestinoDerivacion" class="form-control" data-required="false">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col-md-6">
                                                            <label>Pronóstico Médico Legal</label>
                                                            <select id="sltPronosticoMedicoLegal" class="form-control">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col-md-12">
                                                            <label>Diagnóstico: <b class="color-error">(*)</b></label>
                                                            <div class="typeahead__container">
                                                                <div class="typeahead__field">
                                                                    <div class="typeahead__query">
                                                                        <input id="txtDiagnosticoCIE10" name="hockey_v1[query]" type="search" placeholder="Buscar Diagnóstico CIE-10" autocomplete="off"
                                                                            onclick="this.select();" class="typeahead" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <table id="tblDiagnosticos" class='table table-sm table-striped table-borderless table-hover' style='display: none; width: 100%;'>
                                                                <thead class="thead-light">
                                                                    <tr class="text-center">
                                                                        <th>Descripción Diagnóstico CIE-10</th>
                                                                        <th></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>Indicaciones al alta:</label>
                                                            <textarea id="txaIndicacionesAlta" rows="6" class="form-control" placeholder="Indicaciones al alta"
                                                                maxlength="3000" data-required="true">
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="divEgresoMatroneria"></div>

                                            </div>
                                            <div id="divAtencionClinica" class="mt-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right" style="background-color: white;">
                            <a class="btn btn-light" data-dismiss="modal" aria-label="Close">Cerrar</a>
                            <a id="btnEgresarAlta" class="btn btn-primary">Egresar / Dar alta Paciente</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="detalleAtencion">
        <div class="modal-dialog modal-lg">
            <div class="card modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">ID atención:
                        <label id="lblAtencion"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <%--<div class="form-group col-md-3">
                            <label>Fecha atención</label>
                            <input id="txtFechaLlegada" disabled type="text" required class="form-control" />
                        </div>
                        <div class="form-group col-md-3">
                            <label>Centro de atención</label>
                            <select id="selEstablecimiento" class="disabled form-control">
                            </select>
                        </div>--%>
                        <div class="form-group col-md-12">
                            <label>Motivo de consulta</label>
                            <textarea class="form-control" disabled id="txtMotivoConsulta" rows="5" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <%--<div class="form-row">
                        <div class="form-group col-md-5">
                            <label>Nombre del médico remitente</label>
                            <select id="selNombreMedicoRemitente" class="disabled form-control select2" style="width: 100%;">
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Previsión</label>
                            <select id="selPrevision" class="disabled form-control">
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Tramo</label>
                            <select id="selPrevisionTramo" class="disabled form-control">
                                <option selected>-Seleccione-</option>
                            </select>
                        </div>
                    </div>--%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" onclick="$('#detalleAtencion').modal('hide'); return false;">CERRAR</button>
                </div>
                <%--<div id="divOverlayPaciente" class="overlay dark">
                    <i class="fas fa-5x fa-spinner fa-pulse"></i>
                </div>--%>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pacienteCategorizado">
        <div class="modal-dialog modal-lg">
            <div class="card modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Categorización
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="divCategorizacion" class="post">
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="alert alert-info">
                                    <h1 id="tCat" class="text-center"></h1>
                                    <h5 id="tEstimado" class="text-center"></h5>
                                    <div class="text-center">
                                        <button class="btn btn-warning" id="btnCancelarCategorizacion">
                                            <i class="fas fa-retweet"></i>Cambiar Categorización
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12">
                                <label>Motivo Consulta</label>
                                <textarea id="txtMotivoConsultaCat" class="form-control" placeholder="Escriba Motivo de consulta" rows="3"
                                    maxlength="200" data-required="true">
                                </textarea>
                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="col-md-12">
                                <label>Alergías</label><br />
                                <input id="rdoAlergiaSi" type="radio" name="AL" class="bootstrapSwitch" data-on-text="SI"
                                    data-off-text="SI" data-on-color="primary" data-size="large" data-required="true" />
                                <input id="rdoAlergiaNo" type="radio" name="AL" class="bootstrapSwitch" data-on-text="NO"
                                    data-off-text="NO" data-on-color="primary" data-size="large" data-required="true" />
                                <input id="rdoAlergiaDesconocido" type="radio" name="AL" class="bootstrapSwitch" data-on-text="Desconocido"
                                    data-off-text="Desconocido" data-on-color="primary" data-size="large" data-required="true" />
                            </div>
                            <div id="divDetalleAlergias" class="col-md-12 mt-2">
                                <label>Detalles Alergías</label>
                                <textarea id="txtDetalleAlergias" class="form-control" placeholder="Escriba detalle alergías" rows="2" maxlength="50">
                                </textarea>
                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="col-md-6">
                                <label>Clasificación Atención</label>
                                <select id="sltTipoClasificacion" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="col-md-12">
                                <label>Antecedentes Mórbidos</label>
                                <textarea id="txtAntecedentesMorbidos" class="form-control" placeholder="Escriba antecedentes morbidos" rows="2" maxlength="100">
                                </textarea>
                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="col-md-12">
                                <label>Antecedentes Qx</label>
                                <textarea id="txtAntecedentesQx" class="form-control" placeholder="Escriba antecedentes Qx" rows="2" maxlength="100">
                                </textarea>
                            </div>
                        </div>
                        <div id="divAntecedentesGinecoObstetrico" class="form-row mt-2">
                            <div class="col-md-12">
                                <label>Antecedentes Gineco-Obstétrico</label>
                                <textarea id="txtAntecedentesGinecoObstetrico" class="form-control" placeholder="Escriba Gineco-Obstétrico" rows="2" maxlength="100">
                                </textarea>
                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="col-md-12">
                                <label>Observaciones</label>
                                <textarea id="txtObservacionesCategorizacion" class="form-control" placeholder="Escriba observaciones adicionales" rows="3"
                                    maxlength="300">
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="post" id="divCatEnfermero" style="display: none;">
                        <div class="form-row">
                            <label class="col-form-label" for="inputWarning"><i class="far fa-bell"></i>&nbsp;Considere las siguientes categorizaciones</label>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4" id="divRecategorizarC2" style="display: none;">
                                <div class="small-box bg-info">
                                    <div class="inner">
                                    </div>
                                    <div style="margin-left: 30px" class="icon">
                                        <h3>C2</h3>
                                    </div>
                                    <a href="#/" id="btnRecategorizarC2" class="small-box-footer">Asignar <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4" id="divRecategorizarC4" style="display: none;">
                                <div class="small-box bg-info">
                                    <div class="inner">
                                    </div>
                                    <div style="margin-left: 30px" class="icon">
                                        <h3>C4</h3>
                                    </div>
                                    <a href="#/" id="btnRecategorizarC4" class="small-box-footer">Asignar <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="small-box bg-success">
                                    <div class="inner">
                                    </div>
                                    <div style="margin-left: 30px" class="icon">
                                        <h3>C3</h3>
                                    </div>
                                    <a href="#/" id="btnRecategorizarC3" class="small-box-footer">Asignar <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4" id="divRecategorizarC5" style="display: none;">
                                <div class="small-box bg-info">
                                    <div class="inner">
                                    </div>
                                    <div style="margin-left: 30px" class="icon">
                                        <h3>C5</h3>
                                    </div>
                                    <a href="#/" id="btnRecategorizarC5" class="small-box-footer">Asignar <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-info" id="btnAceptarCategorizacion">Aceptar categorización</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-print fade" id="mdlImprimir" tabindex="-1">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i>Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameDAU" frameborder="0" onload="$('#modalCargando').hide();"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="categorizarPaciente">
        <div class="modal-dialog modal-lg">
            <div class="card modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nombre de paciente:
                        <label id="lblPacienteCat"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="divCategorizacionNormal">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="timeline">
                                    <div class="time-label">
                                        <button class="btn btn-danger" id="btnNuevo">Comenzar de nuevo</button>
                                    </div>
                                    <div id="catC1">
                                        <i class="fa bg-info" style="padding: 1px 10px 1px 8px; border-radius: 20px; font-size: 20px; font-family: monospace;">A</i>
                                        <div class="timeline-item">
                                            <div class="timeline-body">
                                                <div class="form-row">
                                                    <div class="col-md-12">
                                                        <div class="info-box" style="box-shadow: unset;">

                                                            <div class="info-box-content">
                                                                <span class="info-box-number">¿Paciente presenta una amenaza real para su vida, requiere una intervención inmediata?</span>
                                                            </div>

                                                            <div class="btn-group btn-group-toggle" id="toggle00" data-toggle="buttons">
                                                                <label id="btnSiAmenaza" class="btn bg-success">
                                                                    <input type="radio" name="options" checked="" />
                                                                    <i class="fas fa-check-circle" style="font-size: 30px; padding: 7px;"></i>
                                                                </label>
                                                                <label id="btnNoAmenaza" class="btn bg-danger">
                                                                    <input type="radio" name="options" />
                                                                    <i class="fas fa-times-circle" style="font-size: 30px; padding: 7px;"></i>
                                                                </label>
                                                            </div>

                                                            <!-- /.info-box-content -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divOverlayCat1" class="overlay"></div>
                                    </div>

                                    <div id="catC2">
                                        <i class="fa bg-info" style="padding: 1px 10px 1px 8px; border-radius: 20px; font-size: 20px; font-family: monospace;">B</i>
                                        <div class="timeline-item">
                                            <div class="timeline-body">
                                                <div class="form-row">
                                                    <div class="col-md-12">
                                                        <div class="info-box" style="box-shadow: unset;">
                                                            <div class="info-box-content">
                                                                <span class="info-box-number">¿Se trata de un paciente que no debe esperar, es una situación de alto riesgo?</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="timeline-item" id="item01" style="display: none;">
                                            <div class="timeline-body">
                                                <div class="form-row">
                                                    <div class="col-md-12">
                                                        <div class="info-box" style="box-shadow: unset;">
                                                            <div class="info-box-content">
                                                                <span class="info-box-number">AVDI</span>
                                                            </div>
                                                            <div class="btn-group btn-group-toggle" id="toggle01" data-toggle="buttons">
                                                                <label class="btn bg-info clickCat2">
                                                                    <input type="radio" name="options" cat="1" id="opcion01" checked="">
                                                                    Alerta
                                                                </label>
                                                                <label class="btn bg-info clickCat2">
                                                                    <input type="radio" name="options" cat="2" id="opcion02">
                                                                    Respuesta verbal
                                                                </label>
                                                                <label class="btn bg-info clickCat2">
                                                                    <input type="radio" name="options" cat="3" id="opcion03">
                                                                    Respuesta al dolor
                                                                </label>
                                                                <label class="btn bg-info clickCat2">
                                                                    <input type="radio" name="options" cat="4" id="opcion04">
                                                                    Inconsciente
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-item" id="item02" style="display: none;">
                                            <div class="timeline-body">
                                                <div class="form-row">
                                                    <div class="col-md-12">
                                                        <div class="info-box" style="box-shadow: unset;">
                                                            <div class="info-box-content">
                                                                <span class="info-box-number">Dolor, EVA</span>
                                                            </div>
                                                            <div class="btn-group btn-group-toggle" id="toggle02" data-toggle="buttons">
                                                                <label class="btn bg-info clickCat2">
                                                                    <input type="radio" name="options" cat="1" id="opcion05" checked="">
                                                                    Dolor menor a 7
                                                                </label>
                                                                <label class="btn bg-info clickCat2">
                                                                    <input type="radio" name="options" cat="2" id="opcion06">
                                                                    Dolor mayor/igual a 7
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-item" id="item03" style="display: none;">
                                            <div class="timeline-body">
                                                <div class="form-row">
                                                    <div class="col-md-12">
                                                        <div class="info-box" style="box-shadow: unset;">
                                                            <div class="info-box-content">
                                                                <span class="info-box-number">Distresado / ¿Situación de alto riesgo?</span>
                                                                <br />
                                                            </div>
                                                            <div class="btn-group btn-group-toggle" id="toggle03" data-toggle="buttons">
                                                                <label class="btn bg-success clickCat2">
                                                                    <input type="radio" name="options" cat="1" id="opcion07" checked="">
                                                                    <i class="fas fa-check-circle" style="font-size: 30px; padding: 7px;"></i>
                                                                </label>
                                                                <label class="btn bg-danger clickCat2">
                                                                    <input type="radio" name="options" cat="2" id="opcion08">
                                                                    <i class="fas fa-times-circle" style="font-size: 30px; padding: 7px;"></i>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divOverlayCat2" class="overlay"></div>

                                    </div>

                                    <div id="catC3">
                                        <i class="fa bg-info" style="padding: 1px 10px 1px 8px; border-radius: 20px; font-size: 20px; font-family: monospace;">C</i>
                                        <div class="timeline-item">
                                            <div class="timeline-body">
                                                <div class="form-row">
                                                    <div class="col-md-12">
                                                        <div class="info-box" style="box-shadow: unset;">
                                                            <div class="info-box-content">
                                                                <span class="info-box-number">¿Cuántos recursos cree que utilizará?</span>
                                                            </div>
                                                            <div class="btn-group btn-group-toggle" id="toggle04" data-toggle="buttons">
                                                                <label class="btn bg-info" id="btnNinguno">
                                                                    <input type="radio" name="options" checked="">
                                                                    Ninguno
                                                                </label>
                                                                <label class="btn bg-info" id="btnUno">
                                                                    <input type="radio" name="options">
                                                                    Uno
                                                                </label>
                                                                <label class="btn bg-info" id="btnMasdeuno">
                                                                    <input type="radio" name="options">
                                                                    Mas de uno
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divOverlayCat3" class="overlay"></div>
                                    </div>
                                    <div id="alertSignosVitales" style="display: none;">
                                        <div class="timeline-item">
                                            <div class="alert alert-info">
                                                La categorización no tiene datos correspondientes a 'signos vitales'
                                            </div>
                                        </div>
                                    </div>
                                    <div id="catC4">
                                        <i class="fa bg-info" style="padding: 1px 10px 1px 8px; border-radius: 20px; font-size: 20px; font-family: monospace;">D</i>
                                        <div class="timeline-item">
                                            <div class="timeline-body">
                                                <div class="info-box" style="box-shadow: unset;">
                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <div class="info-box-content">
                                                                <span class="info-box-number">¿Signos vitales en zona de riesgo?</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label>F. respiratoria</label>
                                                            <input id="txtFR" type="text" class="form-control" />
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label>F. cardíaca</label>
                                                            <input id="txtFC" type="text" class="form-control" />
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label>Saturación</label>
                                                            <input id="txtSat" type="text" class="form-control" />
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label>Temperatura</label>
                                                            <input id="txtTemp" type="text" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-12">
                                                        <button class="btn btn-success float-right" id="btnAceptarDatos">Aceptar datos</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divOverlayCat6" class="overlay"></div>
                                        </div>
                                        <div class="timeline-item">
                                            <div class="timeline-body" id="divEpidemio" style="display: none;">
                                                <div class="info-box" style="box-shadow: unset;">
                                                    <div class="form-row" style="display: contents;">
                                                        <div class="form-group col-md-6">
                                                            <label>Antecedentes epidemiológicos</label>
                                                            <br />
                                                            <input type="checkbox" id="switchEpidemiologicos" data-label-width="1" data-on-text="SI" data-off-text="NO" data-on-color="info" data-off-color="warning" />
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <div class="custom-control custom-radio">
                                                                <input class="custom-control-input" type="radio" id="checkRequiereRec" name="radioRecurso" checked>
                                                                <label class="custom-control-label" for="checkRequiereRec">Requiere 1 recurso</label>
                                                            </div>
                                                            <div class="custom-control custom-radio">
                                                                <input class="custom-control-input" type="radio" id="checkNoRequiereRec" name="radioRecurso">
                                                                <label class="custom-control-label" for="checkNoRequiereRec">Requiere 0 recursos</label>
                                                            </div>
                                                            <div id="divOverlayEpidemio" class="overlay"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-12">
                                                        <button class="btn btn-success float-right" id="btnAceptarAnte">Aceptar antecedentes</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divOverlayCat5" class="overlay"></div>
                                        </div>
                                        <div id="divOverlayCat4" class="overlay"></div>
                                    </div>
                                    <div>
                                        <i class="far fa-dot-circle bg-gray"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divCategorizacionObstetrico">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="sltCategorizacionObstetrico">Tipo Categorización Gineco-Obstetrico</label>
                                <select id="sltCategorizacionObstetrico" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>
                        <div class="text-right mt-4">
                            <button id="btnCategorizacionObstetrico" class="btn btn-success">
                                <i class="fas fa-check"></i>Confirmar Categorización
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlMovimientosIngresoUrgencia" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead mb-0">Movimientos de ingreso de urgencia</p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mb-2">
                        <asp:Button ID="btnExportarMovIngresoUrgencia" runat="server" OnClick="btnExportarMovIngresoUrgencia_Click"
                            Style="display: none;" />
                        <a id="lnkExportarMovIngresoUrgencia" class="btn btn-info btn-sm ml-auto" href="#\">Exportar a Excel <i class="fa fa-file-excel-o" style="font-size: 18px;"></i>
                        </a>
                    </div>
                    <table id="tblMovimientosIngresoUrgencia" class="table table-bordered table-striped table-hover">
                    </table>
                </div>
            </div>
        </div>
    </div>



    <div id="mdlRecaudacion" class="modal fade ml-2 mr-2" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content ml-4 mr-4">
                <div class="modal-header">
                    <h5 class="modal-title">ID Atención Urgencia #  
                        <label id="lblAtencionLiquidar"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 class="mb-4">
                        <strong>
                            <i class="fas fa-dollar-sign"></i>Liquidar Cuenta
                        </strong>
                    </h5>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-12">
                                    <h6><u>Datos de Atención</u></h6>
                                    Tipo de Atención:<br />
                                    <label id="lblTipoAtencionLiq"></label>
                                    <br />
                                    Motivo de Consulta:<br />
                                    <label id="lblMotivoConsultaLiq"></label>
                                    <h6><u>Datos de Paciente</u></h6>
                                    Nombre Paciente:<br />
                                    <label id="lblPacienteLiq"></label>
                                    <br />
                                    Prevision/Tramo:<br />
                                    <label id="lblPrevisionLiq"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-12">
                                    <h6>Observaciones: (Opcional)</h6>
                                    <textarea id="txtObservacionesLiq" class="form-control" rows="5" placeholder="Observaciones para esta liquidación"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='text-right mt-2'>
                        <a id='aRecaudacionProceder' class='btn btn-info'>Proceder y Liquidar <i class="fas fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/BandejaUrgencia.js") + "?" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/comboUrgencia.js") + "?" + GetVersion() %>"></script>

</asp:Content>
