﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RegistroClinico.Datos.DTO;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;

namespace RegistroClinico.Vista.ModuloUrgencia
{
    public partial class Reporte : Handlers.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.MissingMemberHandling = MissingMemberHandling.Ignore;

                
                List<ReporteDto> reporteDto = JsonConvert.DeserializeObject<List<ReporteDto>>(sCommand, settings);
                if (!ModelState.IsValid)
                    Console.WriteLine(ModelState);

                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                //JArray jsonPreservar = JArray.Parse(sCommand);

                foreach (var item in reporteDto)
                {

                    string fCategorizacion = "";
                    string fPrimeraAtencion = "";

                    string tCategorizacion = "";

                    string tEsperaCategorizacion = "";
                    string tPrimeraAtencion = "";
                    string eAños = "", eMeses = "", eDias = "";
                    string numeroDocumento = "", Nombre = "", ApellidoPaterno = "", ApellidoMaterno = "";
                    string numeroDocumentoAlta = "", NombreAlta = "", ApellidoPaternoAlta = "", ApellidoMaternoAlta = "";
                    if (item.Categorizacion != null)
                    {
                        fCategorizacion = Convert.ToString(item.Categorizacion.FechaHora);
                        tCategorizacion = Convert.ToString(item.Categorizacion.Codigo);
                        tEsperaCategorizacion = Convert.ToString(DateTime.Parse(fCategorizacion) - DateTime.Parse(Convert.ToString(item.FechaAdmision)));
                    }
                    
                    if (item.PrimeraAtencion != null)
                    {
                        fPrimeraAtencion = Convert.ToString(item.PrimeraAtencion.Fecha);
                        tPrimeraAtencion = Convert.ToString(DateTime.Parse(fPrimeraAtencion) - DateTime.Parse(Convert.ToString(item.FechaAdmision)));
                        if (item.PrimeraAtencion.MedicoPrimeraAtencion != null)
                        {
                            numeroDocumento = item.PrimeraAtencion.MedicoPrimeraAtencion.NumeroDocumento.ToString() ?? "";
                            Nombre = item.PrimeraAtencion.MedicoPrimeraAtencion.Nombre.ToString() ?? "";
                            ApellidoPaterno = item.PrimeraAtencion.MedicoPrimeraAtencion.ApellidoPaterno.ToString() ?? "";
                            ApellidoMaterno = item.PrimeraAtencion.MedicoPrimeraAtencion.ApellidoMaterno.ToString() ?? "";
                        }
                    }
                    if (item.Paciente.Edad != null)
                    {
                        eAños = item.Paciente.Edad.años.ToString();
                        eMeses = item.Paciente.Edad.meses.ToString();
                        eDias = item.Paciente.Edad.dias.ToString();
                    }
                    if (item.AltaMedica != null) {
                       if (item.AltaMedica.ProfesionalAlta != null)
                        {
                            numeroDocumentoAlta = item.AltaMedica.ProfesionalAlta.NumeroDocumento ?? "";
                            NombreAlta = item.AltaMedica.ProfesionalAlta.Nombre ?? "";
                            ApellidoPaternoAlta = item.AltaMedica.ProfesionalAlta.ApellidoPaterno ?? "";
                            ApellidoMaternoAlta = item.AltaMedica.ProfesionalAlta.ApellidoMaterno ?? "";
                        }
                    }

                    Dictionary<string, string> registro = new Dictionary<string, string>
                    {
                        ["Número DAU"] = Convert.ToString(item.Id),
                        ["Identificación"] = Convert.ToString(item.Paciente.DescripcionIdentificacion) ?? "",
                        ["Documento"] = Convert.ToString(item.Paciente.NumeroDocumento) ?? "",
                        ["Nombre"] = Convert.ToString(item.Paciente.Nombre) ?? "",
                        ["Apellido Paterno"] = Convert.ToString(item.Paciente.ApellidoPaterno) ?? "",
                        ["Apellido Materno"] = Convert.ToString(item.Paciente.ApellidoMaterno) ?? "",
                        ["NUI"] = Convert.ToString(item.Paciente.Nui) ?? "",
                        ["Años"] = Convert.ToString(eAños),
                        ["Meses"] = Convert.ToString(eMeses),
                        ["Días"] = Convert.ToString(eDias),
                        ["Sexo"] = Convert.ToString(item.Paciente.NombreSexo) ?? "",
                        ["Genero"] = Convert.ToString(item.Paciente.NombreGenero) ?? "",
                        ["Previsión"] = Convert.ToString(item.Prevision) ?? "",
                        ["Detalle"] = Convert.ToString(item.PrevisionTramo) ?? "",
                        ["Tipo Atención"] = Convert.ToString(item.DescripcionTipoAtencion) ?? "",
                        ["F.Ingreso"] = Convert.ToString(item.FechaAdmision) ?? "",
                        ["F.Categorización"] = Convert.ToString(fCategorizacion) ?? "",
                        ["Categorización"] = Convert.ToString(tCategorizacion) ?? "",
                        ["Tº Categorización"] = Convert.ToString(tEsperaCategorizacion) ?? "",
                        ["Nombre medico "] = Convert.ToString(Nombre) ?? "",
                        ["Apellido Paterno medico"] = Convert.ToString(ApellidoPaterno) ?? "",
                        ["Apellido Materno medico"] = Convert.ToString(ApellidoMaterno) ?? "",
                        ["Numero doc. medico"] = Convert.ToString(numeroDocumento) ?? "",
                        ["Motivo Consulta"] = Convert.ToString(item.MotivoConsulta) ?? "",
                        ["F. 1ª Atención"] = Convert.ToString(fPrimeraAtencion) ?? "",
                        ["Tº 1ª Atención"] = Convert.ToString(tPrimeraAtencion) ?? "",
                        ["Nombre Prof. alta"] = Convert.ToString(NombreAlta) ?? "",
                        ["Apellido Paterno Prof. alta"] = Convert.ToString(ApellidoPaternoAlta) ?? "",
                        ["Apellido Materno Prof alta"] = Convert.ToString(ApellidoMaternoAlta) ?? "",
                        ["Numero doc. medico alta"] = Convert.ToString(numeroDocumentoAlta) ?? "",

                    };
                    dic.Add(registro);
                }
                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Reporte" + DateTime.Now.ToString("dd-MM-yyyy"), dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Reporte Urgencia " + DateTime.Now.ToString("dd-MM-yyyy"), workbook);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error al exportar Excel de reporte de urgencia.", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
                NegLog.GuardarLog("Error al exportar Excel de reporte de urgencia.", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }
        }
    }
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 

    public class IngresoBox
    {
        public DateTime Fecha { get; set; }
    }

    public class ReporteDto
    {
        public int Id { get; set; }
        public int IdTipoEstadoSistema { get; set; }
        public PacienteDto Paciente { get; set; }
        public string Prevision { get; set; }
        public string PrevisionTramo { get; set; }
        public string DescripcionTipoAtencion { get; set; }
        public DateTime FechaAdmision { get; set; }
        public CategorizacionDto Categorizacion { get; set; }
        public string MotivoConsulta { get; set; }
        public IngresoBox IngresoBox { get; set; }
        public PrimeraAtencion PrimeraAtencion { get; set; }
        public AltaMedica AltaMedica { get; set; }
        public string NombreTipoEstadoSistema { get; set; }
    }
    public class PrimeraAtencion
    {
        public DateTime? Fecha { get; set; }
        public MedicoPrimeraAtencion MedicoPrimeraAtencion { get; set; }
    }
    public class MedicoPrimeraAtencion{
        public string NumeroDocumento { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
    }
    public class AltaMedica
    {
        public DateTime? FechaAlta { get; set; }
        public ProfesionalAlta ProfesionalAlta { get; set; }
    }
    public class ProfesionalAlta
    {
        public string NumeroDocumento { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
    }
    public class Root
    {
        public PrimeraAtencion PrimeraAtencion { get; set; }
    }
}