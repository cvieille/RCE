﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NuevoIngreso.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.NuevoIngreso" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/AtencionClinica.ascx" TagName="AtencionClinica" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/SignosVitales.ascx" TagName="SignosVitales" TagPrefix="wuc" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/jquery.typeahead.min.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />

    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>

    <style>
        .table.dataTable td, table.dataTable th {
            padding-top: 8px;
            padding-bottom: 8px;
        }
    </style>

</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">

    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />

    <div class="card card-body mb-0">
        <div class="card">
            <div class="card-header bg-light">
                <h3 class="text-center mb-0">
                    <i class="fa fa-file-alt"></i> Ingreso de atención de urgencia
                </h3>
            </div>
            <div class="card-body p-0">

                <ul id="ulTablist" class="nav nav-pills mt-2 mb-2">
                    <li id="liDatosPaciente" class="nav-item col-12 col-sm-4 col-md-4 col-lg-2">
                        <a class="nav-link active" data-toggle="pill" href="#divAntecedentesClinicos">Datos del paciente</a>
                    </li>
                    <li id="liDatosAtencionAdm" class="nav-item col-12 col-sm-4 col-md-4 col-lg-2">
                        <a class="nav-link" data-toggle="pill" href="#divDatosAtencionAdm">Datos de la atención</a>
                    </li>
                    <li id="liDatosClinicos" class="nav-item col-12 col-sm-4 col-md-4 col-lg-2">
                        <a class="nav-link" data-toggle="pill" href="#divDatosClinicos">Datos clínicos</a>
                    </li>
                </ul>

                <div class="tab-content card">

                    <div id="divAntecedentesClinicos" class="tab-pane fade show active pt-3" role="tabpanel" aria-labelledby="home-tab-just">

                        <div id="divDPaciente" data-loading="true">

                            <wuc:Paciente ID="wucPaciente" runat="server" />

                            <div id="divPrevision">

                                <div class="row mt-2">
                                    <div class="col-md-3">
                                        <label for="sltPais" id="lblPais">País de procedencia</label>
                                        <select id="sltPais" class="form-control datos-pac" data-required="true">
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="sltRegion">Región</label>
                                        <select id="sltRegion" class="form-control datos-pac" data-required="true">
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="sltProvincia">Provincia</label>
                                        <select id="sltProvincia" class="form-control datos-pac" data-required="true">
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="sltCiudad">Ciudad</label>
                                        <select id="sltCiudad" class="form-control datos-pac" data-required="true">
                                        </select>
                                    </div>
                                </div>

                                <div class="row mt-2">
                                    <div class="col-md-3">
                                        <label>Previsión</label>
                                        <select id="sltPrevision" class="form-control" data-required="true">
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Previsión tramo</label>
                                        <select id="sltPrevisionTramo" class="form-control" data-required="true">
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="card" data-loading="true">
                            <div class="card-header bg-dark">
                                <h5 class="mb-0">
                                    <strong><i class="fa fa-angle-double-right"></i>Establecimiento</strong>
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Servicio de salud</label>
                                        <select id="sltServicioSalud" class="form-control" data-required="true">
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Establecimiento</label>
                                        <select id="sltEstablecimiento" class="form-control" data-required="true">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <a class="btn btn-primary aSiguienteIngresoUrgencia" data-li="#liDatosPaciente">Siguiente <i class="fa fa-arrow-right"></i>
                                </a>
                            </div>
                        </div>

                    </div>

                    <div id="divDatosAtencionAdm" class="tab-pane fade pt-3" role="tabpanel" aria-labelledby="contact-tab-just">
                        <div class="card">
                            <div class="card-header bg-dark">
                                <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos de la atención</strong> </h5>
                            </div>
                            <div class="card-body">

                                <div id="divDatosAtencion">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Tipo de atención</label>
                                            <select id="sltTipoAtencion" data-required="true" class="form-control">
                                            </select>
                                            <br />
                                            <span id="msjAdulto" style="display: none;" class="text-primary text-center">El paciente es mayor de 15 años! Se sugiere atención de tipo Adulto.</span>
                                            <span id="msjPediatrica" style="display: none;" class="text-primary text-center">El paciente es menor de 15 años! Se sugiere atención de tipo Pediatrica.</span>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-4">
                                            <label>Medio de transporte</label>
                                            <select id="sltMedioTransporte" data-required="true" class="form-control">
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Tipo de accidente</label>
                                            <select id="sltTipoAccidente" class="form-control">
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Fecha llegada</label>
                                            <input id="txtFechaLlegada" disabled type="text" required class="form-control" />
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class="col-md-6">
                                            <label>Lugar de accidente</label>
                                            <textarea id="txtLugarAccidente" class="form-control" disabled maxlength="200" rows="5" style="resize: none;" onkeyup="checkWords(this)"></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Motivo de consulta</label>
                                            <textarea id="txtMotivoConsulta" class="form-control" data-required="true" maxlength="200" rows="5" style="resize: none;" onkeyup="checkWords(this)">
                                            </textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <a class="btn btn-primary aAtrasIngresoUrgencia" data-li="#liDatosAtencionAdm">
                                    <i class="fa fa-arrow-left"></i>Atrás
                                </a>
                                <a class="btn btn-primary aSiguienteIngresoUrgencia" data-li="#liDatosAtencionAdm">Siguiente <i class="fa fa-arrow-right"></i>
                                </a>
                                <button id="aaGuardarIngresoUrgencia" class="btn btn-success aGuardarIngresoUrgencia">
                                    <i class="fa fa-save"></i> Guardar
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="divDatosClinicos" class="tab-pane fade pt-3" role="tabpanel" aria-labelledby="contact-tab-just">

                        <div id="divMedico" class="card">

                            <div class="card-header bg-dark">
                                <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Datos clínicos</strong></h5>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-12 col-xl-9 d-flex align-items-center">
                                        <div style="width: 100%">
                                            <h4>Paciente</h4>
                                            <div id="divPaciente" class="form-control"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-12 col-lg-12 col-xl-3">
                                        <div class="card m-0">
                                            <div class="card-header bg-dark p-2">
                                                <h4 class="text-center m-0">Categorización</h4>
                                            </div>
                                            <div class="card-body p-2">
                                                <div id="divCategorizacion"></div>
                                                <div id="divCategorizacionVacio" class="alert alert-warning text-center mt-3">
                                                    <strong>
                                                        <i class="fa fa-exclamation-triangle"></i> Sin categorización.
                                                    </strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div id="divAntecedentesPacienteVacio" class="alert alert-warning mt-3 text-center">
                                        <strong>
                                            <i class="fa fa-exclamation-triangle"></i> Sin Antecedentes clínicos de paciente.
                                        </strong>
                                    </div>
                                    <div id="divAntecedentesPaciente">
                                        <h4 class='mt-2 text-black mb-2'>Antecedentes Clínicos Paciente</h4>
                                        <div class="row">
                                            <div class="col-sm-6 col-md-3 col-lg-3 col-xl-2">
                                                <label>Clasificación</label>
                                                <span id="spnClasificacion" class="form-control"></span>
                                            </div>
                                            <div class="col-sm-6 col-md-3 col-lg-3 col-xl-2">
                                                <label>Alergías</label>
                                                <span id="spnAlergias" class="form-control"></span>
                                            </div>
                                            <div id="divDescripcionAlergias" class="col-sm-12 col-md-6 col-lg-6 col-xl-8">
                                                <label>Detalle Alergías</label>
                                                <span id="spnDetalleAlergias" class='form-control form-control-no-height'></span>
                                            </div>
                                        </div>
                                        <div id="divAntecedentesMorbidos" class="row">
                                            <div class="col-md-12">
                                                <label>Antecedentes Mórbidos</label>
                                                <span id="spnAntecedentesMorbidos" class='form-control form-control-no-height'></span>
                                            </div>
                                        </div>
                                        <div id="divAntecedentesQx" class="row">
                                            <div class="col-md-12">
                                                <label>Antecedentes Qx</label>
                                                <span id="spnAntecedentesQx" class='form-control form-control-no-height'></span>
                                            </div>
                                        </div>
                                        <div id="divAntecedentesGinecoObstetrico" class="row">
                                            <div class="col-md-12">
                                                <label>Antecedentes Gineco-Obstitrico</label>
                                                <span id="spnAntecedentesGinecoObstetrico" class="form-control form-control-no-height">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                                        <wuc:SignosVitales ID="wucSignosVitales" runat="server" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                                        <h4 class="mt-2">Motivo de la consulta</h4>
                                        <div id="divMotivoConsulta" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <hr style="background-color: black;" />

                                <div class="row mt-2">
                                    <div class="col-md-3">
                                        <div class="form-check pl-0">
                                            <label class="mr-2">Alcoholemia</label>
                                            <input id="chkAlcoholemia" type="checkbox" class="bootstrapSwitch" data-on-text="SI" data-off-text="NO"
                                                data-on-color="success" data-off-color="warning" data-size="normal" />
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div id="divAlcolemia" class="row">
                                            <div class="col-md-4">
                                                <label>N° frasco:</label>
                                                <input id="txtNumeroFrasco" type="number" class="form-control text-center" placeholder="N° Frasco" maxlength="1" />
                                            </div>
                                            <div class="col-md-5">
                                                <label>Fecha:</label>
                                                <input id="txtFechaAlcoholemia" type="date" class="form-control" />
                                            </div>
                                            <div class="col-md-3">
                                                <label>Hora:</label>
                                                <input id="txtHoraAlcoholemia" type="time" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="divIngresosMedicos"></div>
                                
                                <div id="divIngresoMedico">
                                    
                                    <a id="btnCancelarEdicionIngresoMedico" class="btn btn-warning mt-3 mb-3"><i class="fas fa-minus-circle"></i> Cancelar Edición</a>

                                    <div class="row mt-2">
                                        <div class="col-12 col-sm-12 col-md-6 col-xl-4">
                                            <label>Anamnesis</label>
                                            <textarea id="txtAnamnesis" class="form-control" rows="5" placeholder="Escriba anamnesis" data-required="true" maxlength="3000">
                                            </textarea>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-xl-4">
                                            <label>Examen Físico</label>
                                            <textarea id="txtExamenFisico" class="form-control" rows="5" placeholder="Escriba Examen Físico" data-required="true" maxlength="1500">
                                            </textarea>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-xl-4">
                                            <label>Indicaciones al inicio de la atención</label>
                                            <textarea id="txtIndicacionesMedicas" class="form-control" placeholder="Escriba indicaciones médicas" rows="5"
                                                data-required="true" maxlength="1500">
                                            </textarea>
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class="col-md-12">
                                            <label>Hipótesis diagnóstica</label>
                                            <textarea id="txtDiagnostico" class="form-control" rows="5" maxlength="500"
                                                placeholder="Escriba diagnósticos complementarios" data-required="true">
                                            </textarea>
                                        </div>
                                    </div>

                                </div>

                                <div id="divDatosGinecoObstetrico">

                                    <hr style="background-color: black;" class="mt-3 mb-3" />
                                    <h4 class="mt-3">Datos Gineco-Obstétrico</h4>
                                    
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Fecha/hora Atención</label>
                                            <span id="spnFechaHoraAtencionObstetricia" class="form-control"></span>
                                        </div>
                                        <div class="col-md-9">
                                            <label>Última regla</label>
                                            <input id="txtUltimaRegla" type="text" class="form-control" maxlength="50" placeholder="Describa información sobre la última regla" />
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-2">
                                            <label>AU (Altura Uterina)</label>
                                            <input id="txtAlturaUterinaObstetrico" type="number" class="form-control" placeholder="AU" maxlength="3" />
                                        </div>
                                        <div class="col-md-3">
                                            <label>LCF (Latidos Cardio Fetales)</label>
                                            <input id="txtLatidosCardioFetalesObstetrico" type="number" class="form-control" placeholder="LCF" maxlength="3" />
                                        </div>
                                        <div class="col-md-2">
                                            <label>FG (Flujo Genital)</label><br />
                                            <input id="rdoFlujoGenitalObstetricoSi" type="radio" name="FG" class="bootstrapSwitch" data-on-text="SI" data-off-text="SI"
                                                data-on-color="primary" data-size="normal" />
                                            <input id="rdoFlujoGenitalObstetricoNo" type="radio" name="FG" class="bootstrapSwitch" data-on-text="NO" data-off-text="NO"
                                                data-on-color="primary" data-size="normal" />
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6">
                                            <label>Tacto vaginal</label>
                                            <textarea id="txtTactoVaginalObstetrico" class="form-control" placeholder="Describe tacto vaginal" maxlength="200" rows="4">
                                            </textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Especuloscopia</label>
                                            <textarea id="txtEspeculoscopiaObstetrico" class="form-control" placeholder="Describe especuloscopia" maxlength="300" rows="4">
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6">
                                            <label>Ecografía</label>
                                            <textarea id="txtEcografiaObstetrico" class="form-control" placeholder="Escriba ecografía" maxlength="300" rows="4">
                                            </textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Actividad Uterina</label>
                                            <textarea id="txtActividadUterinaObstetrico" class="form-control" placeholder="Escriba Actividad Uterina" maxlength="300" rows="4">
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-12">
                                            <label>Otros</label>
                                            <textarea id="txtOtrosGinecoObstetrico" class="form-control" placeholder="Escriba otras observaciones" maxlength="300" rows="2">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="divDatosProfesionales">
                                <h4 class="mt-3">Datos de Profesional Médico</h4>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="txtNumeroDocumentoProfesional" class="active">Número Documento</label>
                                        <input id="txtNumeroDocumentoProfesional" type="text" class="form-control" disabled="disabled" />
                                    </div>
                                    <div class="col-md-3">
                                        <label class="active" for="txtNombreProfesional">Nombre/es</label>
                                        <input id="txtNombreProfesional" type="text" class="form-control" disabled="disabled" />
                                    </div>
                                    <div class="col-md-3">
                                        <label for="txtApePatProfesional" class="active">Apellido Paterno</label>
                                        <input id="txtApePatProfesional" type="text" class="form-control" disabled="disabled" />
                                    </div>
                                    <div class="col-md-3">
                                        <label for="txtApeMatProfesional" class="active">Apellido Materno</label>
                                        <input id="txtApeMatProfesional" type="text" class="form-control" disabled="disabled" />
                                    </div>
                                </div>
                            </div>
                            <br />

                        </div>

                        <wuc:AtencionClinica ID="wucAtencionClinica" runat="server" />

                        <div id="divAtencionClinica" class="card">
                            <div class="card-header bg-dark">
                                <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Evolución de paciente</strong></h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card border-dark mb-3">
                                            <div class="card-header bg-light">
                                                <h3 class="mt-1 mb-1">Ingresar Evolución del paciente</h3>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Estamento/Profesión</label>
                                                        <span id="spnProfesionEvolucionAtencion" class="form-control"></span>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <label>Profesional</label>
                                                        <span id="spnProfesionalEvolucionAtencion" class="form-control" ></span>
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-md-12">
                                                        <label>Descripción de evolución</label>
                                                        <textarea id="txtEvolucionAtencion" class="form-control" rows="10" placeholder="Describa la evolución del paciente"
                                                            maxlength="3000">
                                                        </textarea>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card border-dark mb-3">
                                            <div class="card-header bg-light p-2">
                                                <h3 class="mt-1 mb-1">Resumen Evolución</h3>
                                            </div>
                                            <div id="divResumeEvolucion" class="card-body show-more">
                                                <div class="show-more-body d-flex justify-content-center">
                                                    <button type="button" class="btn btn-secondary"><i class="fas fa-arrow-down"></i> Ver más</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-right mt-1 mb-3 mr-3">
                            <a class="btn btn-primary aAtrasIngresoUrgencia" data-li="#liDatosClinicos">
                                <i class="fa fa-arrow-left"></i>Atrás
                            </a>
                            <a class="btn btn-primary aSiguienteIngresoUrgencia" data-li="#liDatosClinicos">Siguiente <i class="fa fa-arrow-right"></i>
                            </a>
                            <button class="btn btn-success aGuardarIngresoUrgencia">
                                <i class="fa fa-save"></i> Guardar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/NuevoIngreso.js") + "?" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/comboUrgencia.js") + "?" + GetVersion() %>"></script>

</asp:Content>
