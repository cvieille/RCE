﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="Reporte.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.Reporte" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Reporte de Urgencia</h1>
                </div>
                <div class="col-sm-6">
                    <br />
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4>Filtros</h4>
                    </div>
                    <div class="card-body">
                        <div class="row"> 
                                <h5>asd</h5>
                        </div>
                        <div class="row">
                            
                            <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-2">
                                <label>Fecha de inicio</label>
                                <input id="txtFechaInicio" type="date" class="form-control " data-required="true" />
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-2">
                                <label>Fecha de termino</label>
                                <input id="txtFechaTermino" type="date" class="form-control " data-required="true" />
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 " id="DivSelectTipoAtencion">
                                <label>Tipo de atención</label>
                            <select class="form-control" id="selectTipoAtencion" data-required="true">
                            </select>
                            </div> 
                            <div class="col-sm-12 col-md-3 " style="margin-top:32px;">
                                <a id="btnBuscar" class="btn btn-success"><i class="fa fa-search"></i>Buscar</a>
                                <a id="btnUrgLimpiar" class="btn btn-secondary text-white"><i class="fa fa-eraser"></i>Limpiar Filtros</a>
                                <asp:Button ID="btnExportar" CssClass="d-none"  runat="server" Text="Button" OnClick="btnExportar_Click" />
                            </div><!--CssClass="d-none"-->
                        </div>
                    </div>
                    <div class="card-footer">
                        Total: <span id="spTotalEstadosUrgencia"></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="mdlDetalle" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detalle</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table id="tblDetalle" class="table table-bordered table-hover dataTable no-footer w-100">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $("#btnBuscar").click(function () {
            //fechas para la url YYYY-MM-DD
            let idTipoAtencion = $("#selectTipoAtencion").val();
            let fechaInicio = $("#txtFechaInicio").val();
            let fechaTermino = $("#txtFechaTermino").val();
            let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/Reporte?fInicio=${fechaInicio}&fTermino=${fechaTermino}&idTipoAtencion=${idTipoAtencion}`;
            
            //Objetos moment para validar
            fechaInicio = moment(fechaInicio);
            fechaTermino = moment(fechaTermino);
            let fechaActual = moment()
            if (fechaInicio.isAfter(fechaActual) || fechaTermino.isAfter(fechaActual)) {
                toastr.error("La fecha de inicio y/o termino no puede ser superior a hoy.")
            } else if (fechaInicio.isAfter(fechaTermino)) {
                toastr.error("La fecha de termino no puede ser superior a la fecha de inicio.")
            } else {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Buscando registros',
                    showConfirmButton: false,
                    timer: 2000
                })
                $.ajax({
                    type: 'GET',
                    url: url,
                    contentType: 'application/json',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        __doPostBack("<%= btnExportar.UniqueID %>", JSON.stringify(data));
                    },
                    error: function (jqXHR, status) {
                        console.log(`Error URL: ${url} ${JSON.stringify(jqXHR)}`);
                    }
                });
            }
        });
    </script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/Reporte.js") + "?" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/BandejaUrgencia.js") %>"></script>
</asp:Content>
