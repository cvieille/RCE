﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;

namespace RegistroClinico.Vista.ModuloUrgencia
{
    public partial class BuscadorGeneral : Handlers.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void lnbExportarUrg_Click(object sender, EventArgs e)
        {
            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.MissingMemberHandling = MissingMemberHandling.Ignore;

                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);

                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["ID"] = Convert.ToString(des[i][0]),
                        ["N° Doc"] = Convert.ToString(des[i][1]),
                        ["Nombre"] = Convert.ToString(des[i][2]),
                        ["Edad"] = Convert.ToString(des[i][3]),
                        ["Motivo consulta"] = Convert.ToString(des[i][4]),
                        ["Estado"] = Convert.ToString(des[i][7])
                    };
                    dic.Add(d);
                }

                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Ingreso urgencia", dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Ingreso urgencia", workbook);
            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al exportar Excel de Bandeja de urgencia (Buscador general).", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }
        }

        protected void btnExportarMovIngresoUrgencia_Click(object sender, EventArgs e)
        {
            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.MissingMemberHandling = MissingMemberHandling.Ignore;

                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);

                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["Fecha movimiento"] = des[i][0].ToString(),
                        ["Usuario"] = des[i][1].ToString(),
                        ["Movmiento"] = des[i][2].ToString()
                    };
                    dic.Add(d);
                }

                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Movimientos ingreso urgencia", dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Movimientos ingreso urgencia", workbook);
            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al exportar Excel de Movimientos de DAU ().", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }
        }
    }
}