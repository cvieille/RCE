﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImprimirDAU.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.ImprimirDAU" MasterPageFile="~/Vista/MasterPages/Impresion.Master" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <style>
        .tabla-custom{
            table-layout: fixed;
            width: 100%;
        }

        .base td{
            width: 8.333%;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">
    <div class="panel">
        <div class="panel-heading">
            <div class="row">
                <asp:Label ID="prueba" runat="server" Text=""></asp:Label>
                <div class="col-xs-2">
                    <br/>
                </div>
                <div class="col-xs-8">
                    <h4 class="text-center"><strong>ATENCIÓN DE URGENCIA - Hospital Clínico Magallanes</strong></h4>
                </div>
                <div class="col-xs-2">
                    <h4 class="text-right"><strong>N° DAU:
                        <asp:Label ID="lblDAU" runat="server"></asp:Label></strong></h4>
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding: 0px;">

            <div class="estilo-titulo">
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px !important; padding-right: 0px;">
                        Datos de Paciente
                    </div>
                    <div class="col-xs-5 text-right" style="padding-left: 0px !important; padding-right: 0px;">
                        <strong>Fecha de llegada:
                            <asp:Label ID="lblFechaLlegada" runat="server"></asp:Label></strong>
                    </div>
                </div>
            </div>

            <table id="tblIdentificaPaciente" style="width: 100%; border: 1px solid black;">
                <tr>
                    <td class="fondo-titulos contenido-celda">
                        <asp:Label ID="lblidentificacion" runat="server" Text="">
                        </asp:Label>
                    </td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblRutPac" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Nombre</td>
                    <td class="contenido-celda" colspan="4">
                        <asp:Label ID="lblNombrePac" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Fecha de nacimiento</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblFechaNacimiento" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Edad</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblEdad" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Previsión</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblPrevisionPacConst" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Sexo</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblSexo" runat="server">
                        </asp:Label>
                    <td class="fondo-titulos contenido-celda">Genero</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblGenero" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Teléfono</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblFono" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Ciudad</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblCiudad" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Dirección</td>
                    <td class="contenido-celda" colspan="4">
                        <asp:Label ID="lblDireccionPacIPD" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>

            <div class="estilo-titulo">Datos de la categorización</div>
            <asp:Panel ID="pnlErrorCategorizacion" runat="server" Style="border: 1px solid black;">
                <p class="text-center" style="padding: 15px 15px 15px 15px !important;">
                    <i class="fa fa-exclamation-triangle"></i>
                    El paciente no posee <strong>categorización</strong>. 
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlCategorizacion" runat="server">
                <table style="border: 1px solid black; width: 100%;">
                    <tr>
                        <td class="fondo-titulos contenido-celda">Categorización</td>
                        <td class="contenido-celda">
                            <asp:Label ID="lblCategorizacion" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda">Fecha</td>
                        <td class="contenido-celda">
                            <asp:Label ID="lblFechaCategorizacion" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda">Priorizador</td>
                        <td class="contenido-celda">
                            <asp:Label ID="lblPriorizador" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda">Tº Espera</td>
                        <td class="contenido-celda">
                            <asp:Label ID="lblTiempoEsperaCategorizacion" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <div class="estilo-titulo">Datos de la atención</div>
            <table style="width: 100%; border: 1px solid black;">
                <tr>
                    <td class="fondo-titulos contenido-celda col-td-2">Tipo de atención</td>
                    <td class="contenido-celda col-td-2">
                        <asp:Label ID="lblTipoAtencion" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda col-td-2" style="">Tipo de accidente</td>
                    <td class="contenido-celda col-td-2">
                        <asp:Label ID="lblTipodeAccidente" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda col-td-2">Lugar accidente
                    </td>
                    <td class="contenido-celda col-td-2">
                        <asp:Label ID="lblLugarAccidente" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda col-td-12" colspan="6">Motivo de consulta</td>
                </tr>
                <tr>
                    <td class="contenido-celda col-td-12" colspan="6">
                        <asp:Label ID="lblMotivoConsulta" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda col-td-2">Acompañante
                    </td>
                    <td class="contenido-celda col-td-6" colspan="2">
                        <asp:Label ID="lblAcompañante" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda col-td-2">Medio de transporte</td>
                    <td class="contenido-celda col-td-2" colspan="2">
                        <asp:Label ID="lblMedioTransporte" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda col-td-2">Admisor</td>
                    <td class="contenido-celda col-td-2" colspan="2">
                        <asp:Label ID="lblNombreAdmisor" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda col-td-2">Fecha</td>
                    <td class="contenido-celda col-td-2" colspan="2">
                        <asp:Label ID="lblFechaAdmision" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>

            <asp:Panel ID="pnlClinicos" runat="server">
                <div class="estilo-titulo">Datos clínicos</div>
                <table style="width: 100%; border: 1px solid black;">
                    <tr>
                        <td class="fondo-titulos contenido-celda col-td-1">Medico</td>
                        <td class="contenido-celda col-td-4">
                            <asp:Label ID="lblMedicoAtencion" runat="server"></asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda col-td-1">Inicio Atención</td>
                        <td class="contenido-celda col-td-3">
                            <asp:Label ID="lblFechaHoraAtencion" runat="server"></asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda col-td-1">Tº Espera</td>
                        <td class="contenido-celda col-td-2">
                            <asp:Label ID="lblTiempoEsperaAtencionMedica" runat="server"></asp:Label>
                        </td>
                    </tr> 
                    <tr>
                        <td class="fondo-titulos contenido-celda col-td-2">Alcoholemia
                        </td>
                        <td class="contenido-celda col-td-2">
                            <asp:Label ID="lblAlcoholemiaSi" runat="server"></asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda col-td-2">N° frasco</td>
                        <td class="contenido-celda col-td-2">
                            <asp:Label ID="lblNumFrasco" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda col-td-2">Fecha/hora</td>
                        <td class="contenido-celda col-td-2">
                            <asp:Label ID="lblFechaHoraAlcoholemia" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%; border: 1px solid black;">
                    <tr>
                        <td class="fondo-titulos contenido-celda col-td-6" colspan="3">Anamnesis</td>
                        <td class="fondo-titulos contenido-celda col-td-6" colspan="3">Examen Físico</td>
                    </tr>
                    <tr>
                        <td class="contenido-celda col-td-6" colspan="3">
                            <asp:Label ID="lblAnamnesis" runat="server">
                            </asp:Label>
                        </td>
                        <td class="contenido-celda col-td-6" colspan="3">
                            <asp:Label ID="lblExamenFisico" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="fondo-titulos contenido-celda col-td-6" colspan="3">Indicaciones médicas</td>
                        <td class="fondo-titulos contenido-celda col-td-6" colspan="3">Hipótesis diagnóstica</td>
                    </tr>
                    <tr>
                        <td class="contenido-celda col-td-6" colspan="3">
                                <asp:Label ID="lblIndicacionesMedicas" runat="server">
                            </asp:Label>
                        </td>
                        <td class="contenido-celda col-td-6" colspan="3">
                            <asp:Label ID="lblHipotesisDiagnostica" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="fondo-titulos contenido-celda col-td-12 text-center" colspan="6">Signos Vitales</td>
                    </tr>
                    <asp:Label ID="lblSignosVitales" runat="server">
                    </asp:Label>
                    <tr>
                        <td class="fondo-titulos contenido-celda col-td-12 text-center" colspan="6">Indicaciones Clínicas</td>
                    </tr>
                    <tr>
                        <td class="col-td-12" colspan="6">
                            <asp:Label ID="lblIndicacionesClinicas" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel ID="pnlClinicoObstetrico" runat="server">
                <div class="estilo-titulo">Datos Gineco-Obstétrico</div>
                <table style="width: 100%; border: 1px solid black;">
                    <tr>
                        <td class="fondo-titulos contenido-celda col-td-2">Fecha y hora
                        </td>
                        <td class="contenido-celda col-td-2">
                            <asp:Label ID="lblFechaHoraAtencionObstetrica" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda col-td-2">Latidos cardio fetales
                        </td>
                        <td class="contenido-celda col-td-2">
                            <asp:Label ID="lblLatidosCardioFetales" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda col-td-2">Altura uterina
                        </td>
                        <td class="contenido-celda col-td-2">
                            <asp:Label ID="lblAlturaUterina" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="fondo-titulos contenido-celda col-td-2">Flujo genital
                        </td>
                        <td class="contenido-celda col-td-2">
                            <asp:Label ID="lblFlujoGenital" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda col-td-2">Tacto vaginal
                        </td>
                        <td class="contenido-celda col-td-2">
                            <asp:Label ID="lblTactoVaginal" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda col-td-2">Especuloscopia
                        </td>
                        <td class="contenido-celda col-td-2">
                            <asp:Label ID="lblEspeculoscopia" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="fondo-titulos contenido-celda col-td-2">Monitoreo basal
                        </td>
                        <td class="contenido-celda col-td-4" colspan="2">
                            <asp:Label ID="lblMonitoreoBasal" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda col-td-2">Actividad uterina
                        </td>
                        <td class="contenido-celda col-td-4" colspan="2">
                            <asp:Label ID="lblActividadUterina" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="fondo-titulos contenido-celda col-td-2">Ecografía
                        </td>
                        <td class="contenido-celda col-td-4" colspan="2">
                            <asp:Label ID="lblEcografia" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda col-td-2">Otros
                        </td>
                        <td class="contenido-celda col-td-4" colspan="2">
                            <asp:Label ID="lblOtrosObstetricos" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Literal ID="lblAtencionesClinicas" runat="server">
            </asp:Literal>
            <br />
            <!-- Ojo! Salto de Página-->
            <div style='page-break-after:always'></div>
            <div class="row">
                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                <div class="col-xs-2">
                    <br/>
                </div>
                <div class="col-xs-8">
                    <h4 class="text-center"><strong>ATENCIÓN DE URGENCIA - Sección Alta</strong></h4>
                </div>
                <div class="col-xs-2">
                    <h4 class="text-right"><strong>N° DAU:
                        <asp:Label ID="lblDAU2" runat="server"></asp:Label></strong></h4>
                </div>
            </div>
            <div class="estilo-titulo">
                <div class="row">
                    <div class="col-xs-" style="padding-left: 0px !important; padding-right: 0px;">
                        Datos de Paciente
                    </div>
                </div>
            </div>
            <table id="tblIdentificaPaciente2" style="width: 100%; border: 1px solid black;">
                <tr>
                    <td class="fondo-titulos contenido-celda">
                        <asp:Label ID="lblidentificacion2" runat="server" Text="">
                        </asp:Label>
                    </td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblRutPac2" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Nombre paciente</td>
                    <td class="contenido-celda" colspan="4">
                        <asp:Label ID="lblNombrePac2" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Fecha de nacimiento</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblFechaNacimiento2" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Edad</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblEdad2" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Previsión paciente</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblPrevisionPaciente2" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Sexo</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblSexo2" runat="server">
                        </asp:Label>
                    <td class="fondo-titulos contenido-celda">Genero</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblGenero2" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Teléfono</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblTelefono2" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda">Ciudad</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblCiudad2" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Dirección</td>
                    <td class="contenido-celda" colspan="4">
                        <asp:Label ID="lblDireccionPacIPD2" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>
            <div class="estilo-titulo">Alta Médica</div>
            <table class="tabla-custom">
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="2">Médico que da el alta</td>
                    <td class="contenido-celda" colspan="3">
                        <asp:Label ID="lblMedicoAlta" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda" colspan="2">Hora término atención</td>
                    <td class="contenido-celda">
                        <asp:Label ID="lblHoraTerminoAtencion" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda" colspan="2">Destino Alta</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="lblDestinoAlta" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="2">Indicaciones al Alta</td>
                    <td class="contenido-celda" colspan="10">
                        <asp:Label ID="lblIndicacionesAlta" runat="server">
                        </asp:Label>
                    </td>                    
                </tr>
                <tr class="base">
                    <td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td>
                </tr>
            </table>
            <asp:Literal ID="ltlDiagnosticoCIE10" runat="server">
            </asp:Literal>
            <asp:Literal ID="ltlDatosAlta" runat="server">
            </asp:Literal></div>
    </div>
</asp:Content>
