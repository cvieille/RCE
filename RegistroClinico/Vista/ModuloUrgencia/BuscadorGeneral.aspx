﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="BuscadorGeneral.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.BuscadorGeneral" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script>

        $(document).ready(function () {

            $('#lnkExportarMovIngresoUrgencia').click(function () {
                var tblObj = $('#tblMovimientosIngresoUrgencia').DataTable();
                __doPostBack("<%= btnExportarMovIngresoUrgencia.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });

            $('#lnbExportar').click(function () {
                var tblObj = $('#tblUrgencia').DataTable();
                __doPostBack("<%= lnbExportarUrg.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Buscador General</h1>
                </div>
                <div class="col-sm-6">
                    <br />
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        
        <div id="divBandejaUrgencia" class="tab-pane fade show active" role="tabpanel" aria-labelledby="home-tab-just">
            <button id="btnFiltroUrg" data-toggle="collapse" class="btn btn-outline-primary container-fluid" data-target="#divFiltroUrg" onclick="return false;">
                FILTRAR BANDEJA
            </button>
            <div id="divFiltroUrg" class="collapse p-3">
                
                <div class="row">
                    <div class="col-md-2">
                        <label>Identificación</label>
                        <select id="sltIdentificacionPaciente" class="form-control">
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label id="lblIdentificacionPaciente" for="sltIdentificacionPaciente"></label>
                        <div class="input-group">
                            <input id="txtnumeroDocPaciente" type="text" class="form-control numero-documento" style="width:100px;"
                                data-required="false" />
                            <div class="input-group-prepend digito">
                                <div class="input-group-text">
                                    <strong>-</strong>
                                </div>
                            </div>
                            <input id="txtDigPaciente" type="text" class="form-control digito text-center" maxlength="1" style="width:15px;" 
                                placeholder="DV" disabled />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label for="txtnombrePaciente">Nombre</label>
                        <input id="txtNombrePaciente" type="text" class="form-control datos-persona" maxlength="50" 
                            placeholder="Nombre del Paciente" />
                    </div>
                    <div class="col-md-3">
                        <label for="txtApePatPaciente">Apellido paterno</label>
                        <input id="txtApePatPaciente" type="text" class="form-control datos-persona" maxlength="50"
                            placeholder="Apellido paterno del Paciente" />
                    </div>
                    <div class="col-md-2">
                        <label for="txtApeMatPaciente">Apellido Materno</label>
                        <input id="txtApeMatPaciente" type="text" class="form-control datos-persona" maxlength="50"
                            placeholder="Apellido materno del Paciente" />
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-4">
                        <label for="txtFechaInicio">Fecha Inicio</label>
                        <input id="txtFechaInicio" type="date" class="form-control datos-persona"  />
                    </div>
                    <div class="col-md-4">
                        <label for="txtFechaTermino">Fecha Termino</label>
                        <input id="txtFechaTermino" type="date" class="form-control datos-persona" />

                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-md-12 text-right">
                        <a id="btnUrgFiltro" class="btn btn-info"><i class="fa fa-search"></i> Buscar</a> 
                        <a id="btnUrgLimpiarFiltro" class="btn btn-warning"><i class="fa fa-eraser"></i> Limpiar Filtros</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="post">
                            <div class="text-right">
                                <asp:Button runat="server" ID="lnbExportarUrg" OnClick="lnbExportarUrg_Click" Style="display: none;" />
                                <a id="lnbExportar" class="btn btn-info waves-effect waves-light" href="#\">
                                    <i class="fa fa-file-excel"></i> Exportar XLS
                                </a>
                                <a id="lblNuevoIngreso" style="display: none;" class="btn btn-info" href="NuevoIngreso.aspx">
                                    <i class="fa fa-plus"></i> Nuevo ingreso
                                </a>
                                <a id="btnRefrescar" class="btn btn-primary"><i class="fa fa-refresh"></i> Refrescar</a>
                            </div>
                        </div>
                        <div class="post">
                            <table id="tblUrgencia" class="table table-striped table-bordered table-hover" style="width: 100%;"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    
    
    <div class="modal modal-print fade" id="mdlImprimir" tabindex="-1">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i>Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameDAU" frameborder="0" onload="$('#modalCargando').hide();"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlAtencionesClinicas" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="card modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center">Atenciones Clínicas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-pills" style="display:none;">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="pill" href="#divAtencionesClinica">
                                Atenciones clínica
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#divVistaAtencionClinica">
                                Vista Atención clínica
                            </a>
                        </li>
                    </ul>
                    
                    <div class="tab-content">
                        <div id="divAtencionesClinica" class="tab-pane fade show active" role="tabpanel" aria-labelledby="home-tab-just">
                            <table id="tblAtencionesClinicas" class="table table-striped table-bordered table-hover" style="width: 100%;">
                            </table>
                        </div>
                        <div id="divVistaAtencionClinica" class="tab-pane fade" role="tabpanel" aria-labelledby="contact-tab-just">
                            
                            <div class="row mt-2">
                                <div class="col-md-3">
                                    <label>Fecha</label>
                                    <input id="txtVerFechaAtencionClinica" type="text" class="form-control" disabled="disabled" />
                                </div>
                                <div class="col-md-6">
                                    <label>Estamento</label>
                                    <input id="txtVerEstamentoAtencionClinica" type="text" class="form-control" disabled="disabled" />
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-12">
                                    <label>Profesional</label>
                                    <div id="divVerProfesional" class="p-1" style="border: 1px solid lightgray"></div>
                                </div>
                            </div>
                                
                            <div class="row mt-2">
                                <div class="col-md-12">
                                    <label>Aranceles</label>
                                    <div id="divVerAranceles" class="p-2" style="border: 1px solid lightgray">
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-12">
                                    <label>Descripción de Atención</label>
                                    <div id="divVerDescripcionAtencionClinica" class="p-2" style="border: 1px solid lightgray">
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-12">
                                    <a id="aVolverAtencionesClinicas" class="btn btn-info">
                                        <i class="fa fa-arrow-left"></i> Volver
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="mdlMovimientosIngresoUrgencia" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead mb-0">Movimientos de ingreso de urgencia</p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mb-2">
                        <asp:Button ID="btnExportarMovIngresoUrgencia" runat="server" OnClick="btnExportarMovIngresoUrgencia_Click"
                            Style="display: none;" />
                        <a id="lnkExportarMovIngresoUrgencia" class="btn btn-info btn-sm ml-auto" href="#\">Exportar a Excel <i class="fa fa-file-excel-o" style="font-size: 18px;"></i>
                        </a>
                    </div>
                    <table id="tblMovimientosIngresoUrgencia" class="table table-bordered table-striped table-hover" >
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/BuscadorGeneral.js") + "?" + GetVersion() %>"></script>

</asp:Content>
