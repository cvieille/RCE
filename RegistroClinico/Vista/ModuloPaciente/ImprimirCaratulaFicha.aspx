﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImprimirCaratulaFicha.aspx.cs" Inherits="RegistroClinico.Vista.ModuloPaciente.ImprimirCaratulaFicha" MasterPageFile="~/Vista/MasterPages/Impresion.Master" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <style>
        .cell-title {
            font-weight: bold;
            background-color: #f1f1f1;
        }

        .nui-box {
            border-left: 2px solid grey;
            border-bottom: 2px solid grey;
            border-right: 2px solid grey;
        }

        body {
            font-family: Arial;
             margin-left:250px;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {

            var urlParams = window.location.search;
            var getQuery = urlParams.split('?')[1];
            var idPaciente = getQuery.split('id=')[1];

            var json = {};

            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/${idPaciente}`,
                async: false,
                headers: { 'Authorization': GetToken() },
                success: function (data) {
                    json = data;

                }
            });

            if (json[0].GEN_idPrevision != null) {
                $.ajax({
                    type: 'GET',
                    url: `${GetWebApiUrl()}GEN_Prevision/${json[0].GEN_idPrevision}`,
                    async: false,
                    headers: { 'Authorization': GetToken() },
                    success: function (data) {
                        json[0]["GEN_nombrePrevision"] = data.GEN_nombrePrevision;
                    }
                });
            }

            if (json[0].GEN_idSexo != null) {
                $.ajax({
                    type: 'GET',
                    url: `${GetWebApiUrl()}GEN_Sexo/${json[0].GEN_idSexo}`,
                    async: false,
                    headers: { 'Authorization': GetToken() },
                    success: function (data) {
                        json[0]["GEN_nombreSexo"] = data.GEN_nomSexo;
                    }
                });
            }

            if (json[0].GEN_idCiudad != null) {
                $.ajax({
                    type: 'GET',
                    url: `${GetWebApiUrl()}GEN_Ciudad/${json[0].GEN_idCiudad}`,
                    async: false,
                    headers: { 'Authorization': GetToken() },
                    success: function (data) {
                        json[0]["GEN_nombreCiudad"] = data.GEN_nombreCiudad;
                    }
                });
            }

            console.log("Datos del Paciente: " + JSON.stringify(json[0]));
            __doPostBack("<%= btnHidden.UniqueID %>", JSON.stringify(json[0]));

        });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">
    <asp:Button runat="server" ID="btnHidden" OnClick="btnHidden_Click" Style="display: none;" />
    <div class="content">
        <div class="row">
            <div class="col-md-11">
                <div class="row">
                    <div class="col-sm-7"></div>
                    <div class="col-sm-4 nui-box text-center" style="font-size: 20pt;">
                        UBICACION INTERNA<br />
                        <asp:Label ID="lblUbicacionInterna" runat="server"></asp:Label>
                    </div>
                </div>
                <br/>
                <br/>
                <center>
                    <h4><b>HOSPITAL CLINICO DE MAGALLANES <br />61.607.901-8</b></h4>
                    <br>
                    <h2><b>FICHA CLINICA: <asp:Label id="lblRutPac" runat="server"></asp:Label>-<asp:Label id="lblDigitoPac" runat="server"></asp:Label></b></h2>
                    <br/>
                    <br/>
                </center>
                <div class="row" style="font-size: 18pt;">
                    <div class="col-md-6">
                        <asp:Label ID="lblPaternoPac" runat="server"></asp:Label>
                        <hr />
                        <b>Apellido Paterno</b>
                    </div>
                    <div class="col-md-6">
                        <asp:Label ID="lblMaternoPac" runat="server"></asp:Label>
                        <hr />
                        <b>Apellido Materno</b>
                    </div>
                </div>
                <br />
                <br />
                <div class="row" style="font-size: 18pt;">
                    <div class="col-md-12">
                        <asp:Label ID="lblNombrePac" runat="server"></asp:Label>
                        <hr />
                        <b>Nombres</b>
                    </div>
                </div>
                <br />
                <br />
                <div class="row" style="font-size: 18pt;">
                    <div class="col-md-6">
                        <b>Fecha de Nacimiento</b>
                        <asp:Label ID="lblFechaNacimientoPac" runat="server"></asp:Label>
                        <hr />
                    </div>
                    <hr />
                    <div class="col-md-6">
                        <b>Sexo</b>
                        <asp:Label ID="lblSexoPac" runat="server"></asp:Label>
                        <hr />
                    </div>
                </div>
                <br />
                <div class="row" style="font-size: 18pt;">
                    <div class="col-md-12">
                        <asp:Label ID="lblDireccionPac" runat="server"></asp:Label>
                        <hr />
                        <b>Dirección</b>
                    </div>
                </div>
                <br />
                <div class="row" style="font-size: 18pt;">
                    <div class="col-md-6">
                        <asp:Label ID="lblComunaPac" runat="server"></asp:Label>
                        <hr />
                        <b>Ciudad</b>
                    </div>
                    <div class="col-md-6" style="font-size: 18pt;">
                        <asp:Label ID="lblFonoPac" runat="server"></asp:Label>
                        <hr />
                        <b>Teléfono</b>
                    </div>
                </div>
                <br />
                <div class="row" style="font-size: 18pt;">
                    <div class="col-md-6">
                        <asp:Label ID="lblProfesionPac" runat="server"></asp:Label>
                        <br />
                        <hr />
                        <b>Profesion u Ocupación</b>
                    </div>
                    <div class="col-md-6">
                        <br />
                        <asp:Label ID="lblSistemaSaludPac" runat="server"></asp:Label>
                        <hr />
                        <b>Sistema de Salud</b>
                    </div>
                </div>
                <br />
                <br />
                <div class="row">
                    <div class="col-md-4" style="height: 150px; border-style: solid; border-width: 1px;">
                        Grupo Sanguineo
                    </div>
                    <div class="col-md-4" style="height: 150px; border-style: solid; border-width: 1px;">
                        Alergias y/o Tratamiento
                    </div>
                    <div class="col-md-3" style="height: 150px; border-style: solid; border-width: 1px;">
                        Fichas en Otros Servicios
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>