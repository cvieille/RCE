﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reporte.aspx.cs" Inherits="RegistroClinico.Vista.ModuloPaciente.Reporte" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <script>
        $(document).ready(function () {
            $('#btnExportar').click(function () {
                var tblObj = $('#tblReporte').DataTable(); 
                __doPostBack("<%= btnExportarH.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });
        });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Reportes</h1>
                </div>
                <div class="col-sm-6">
                    <br />
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Reportes fichas</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2 offset-md-1">
                                <label>Tipo de reporte</label>
                                <select id="selFiltroReporte" class="form-control">
                                    <option value="1">Fichas creadas</option>
                                    <option value="2">Fichas modificadas odontología</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Desde</label>
                                <input id="txtFiltroDesde" type="date" required class="form-control" />
                            </div>
                            <div class="col-md-2">
                                <label>Hasta</label>
                                <input id="txtFiltroHasta" type="date" required class="form-control"/>
                            </div>
                            <div class="col-md-1" style="margin-top:-6px;">
                                <label>&nbsp;</label>
                                <input type="button" id="btnFiltro" class="btn btn-default" value="BUSCAR"/>
                            </div>
                            <div class="col-md-2" style="margin-top:-6px;">
                                <asp:Button runat="server" ID="btnExportarH" Text="1" OnClick="btnExportarH_Click" style="display:none;"/>
                                <label>&nbsp;</label>
                                <input type="button" id="btnExportar" class="btn btn-default" value="EXPORTAR EXCEL"/>
                            </div>
                        </div>
                        <hr />
                        <table id="tblReporte" class="table table-bordered table-sm table-hover"></table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<%= ResolveClientUrl("~/Script/ModuloPaciente/Reporte.js") + "?" + GetVersion() %>"></script>
</asp:Content>