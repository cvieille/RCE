﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="BandejaPAC.aspx.cs" Inherits="RegistroClinico.Vista.ModuloPaciente.BandejaPAC" %>

<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/BotonesFlotantes.css" rel="stylesheet" />
    <style>
        /**************** DISEÑO RESPONSIVO DE BOTONERA PACIENTE ****************/

        a.material-icons.nav-link.disabled {
            background-color: lightgray !important;
            color: white !important;
        }

        .badge-count {
            position: absolute;
            margin-top: 20px;
        }

        @media only screen and (max-width: 1234px) {

            .nav-paciente {
                padding-right: 0 !important;
                width: 100%;
            }

            .nav-item-paciente {
                width: 100%;
            }

            .nav-text-paciente {
                display: none !important;
            }

            .nav-icon-paciente {
                transform: scale(0.6) !important;
                transform-origin: center !important;
            }
        }

        @media only screen and (max-device-width: 1950) {

            .nav-paciente {
                flex-direction: row !important;
                padding-right: 0 !important;
                width: 100%;
            }

            .nav-item-paciente {
                width: 100%;
            }

            .nav-text-paciente {
                display: none !important;
            }

            .nav-icon-paciente {
                transform: scale(0.6) !important;
                transform-origin: center !important;
            }
        }

        @media only screen and (max-device-width: 1024px) {

            /**************** FIN BOTONERA PACIENTE ******************/
    </style>
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/prefixfree.min.js") %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <!--HIDDEN FIELDS-->

    <input id="hdfEditable" type="hidden" />
    <input id="hdfTransicion" type="hidden" />
    <input id="hdfFallecido" type="hidden" value="NO" />
    <input id="idPaciente" type="hidden" value="0" />
    <input id="idPacienteDClinico" type="hidden" value="0" />

    <!--FIN HIDDEN FIELDS-->

    <section class="content-header">

        <!--MIGAS DE PAN-->

        <div class="container-fluid">
            <h1 class="mb-3">Paciente</h1>
        </div>

        <div id="lblPaciente" class="form-row" style="display: none;">
            <div class="col-md-12 text-center">
                <div class="alert alert-info">
                    <span id="lblNombrePacientePill"></span>&nbsp;&nbsp;&nbsp;<span id="lblRutPacientePill"></span>&nbsp;&nbsp;&nbsp;<span id="lblNUIPacientePill"></span>
                </div>
            </div>
        </div>
        <div id="lblPacienteFallecido" class="form-row" style="display: none;">
            <div class="col-md-12 text-center">
                <div class="alert alert-danger">
                    Paciente con fecha de fallecimiento:&nbsp;&nbsp;&nbsp;<span id="lblFechaFallecimientoPacientePill"></span>
                </div>
            </div>
        </div>

        <!--FIN MIGAS DE PAN-->

    </section>

    <section class="content">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                <ul class="nav md-pills pills-primary " role="tablist">
                    <li class="nav-item nav-item-paciente col-12 col-sm-6 col-md-12">
                        <a id="btnNuevoPacienteModal" class="nav-link panel-paciente small-box bg-info customForPACMenu" href="#" role="tab">
                            <div class="icon customForPACMenu text-center">
                                <i class="fa fa-user-plus text-white" style="font-size: 30px;"></i>
                            </div>
                            <div class="inner text-center text-white mt-1 p-0">
                                <h6>Nuevo</h6>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item nav-item-paciente col-12 col-sm-6 col-md-12">
                        <a id="btnBusqueda" class="nav-link panel-paciente active small-box bg-info customForPACMenu" data-toggle="tab"
                            href="#panel1" role="tab">
                            <div class="icon customForPACMenu text-center">
                                <i class="fa fa-search" style="font-size: 30px; color: ghostwhite;"></i>
                            </div>
                            <div class="inner text-center" style="margin-top: 5px; padding: 0px;">
                                <h6>Buscador</h6>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item nav-item-paciente col-12 col-sm-6 col-md-12">
                        <a id="btnPaciente" class="nav-link panel-paciente disabled small-box bg-secondary customForPACMenu" data-toggle="tab"
                            href="#panel2" role="tab">
                            <div class="icon customForPACMenu text-center">
                                <i class="fa fa-user" style="font-size: 30px; color: ghostwhite;"></i>
                            </div>
                            <div class="inner text-center" style="margin-top: 5px; padding: 0px;">
                                <h6>Datos Paciente</h6>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item nav-item-paciente col-12 col-sm-6 col-md-12">
                        <a id="btnDatosClinicos" class="nav-link panel-paciente disabled small-box bg-secondary customForPACMenu"
                            data-toggle="tab" href="#panel3" role="tab">
                            <div class="icon customForPACMenu text-center">
                                <i class="fa fa-stethoscope" style="font-size: 30px; color: ghostwhite;"></i>
                            </div>
                            <div class="inner text-center" style="margin-top: 5px; padding: 0px;">
                                <h6>Datos Clínicos</h6>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item nav-item-paciente col-12 col-sm-6 col-md-12">
                        <a id="btnDatosPrev" class="nav-link panel-paciente btn-blue-grey disabled small-box bg-secondary customForPACMenu"
                            data-toggle="tab" href="#panel4" role="tab">
                            <div class="icon customForPACMenu text-center">
                                <i class="fa fa-info" style="font-size: 30px; color: ghostwhite;"></i>
                            </div>
                            <div class="inner text-center" style="margin-top: 5px; padding: 0px;">
                                <h6>Datos Previsionales</h6>
                            </div>
                        </a>
                    </li>
                    <%--<li class="nav-item nav-item-paciente">
                    <a ID="btnEventos" class="nav-link panel-paciente btn-light disabled small-box bg-secondary customForPACMenu" 
                        data-toggle="tab" href="#panel5" role="tab" onclick="getEventosPaciente()">
                        <div class="icon customForPACMenu">
                            <center><i class="far fa-calendar-check" style="font-size: 30px; color: ghostwhite;"></i></center>
                        </div>
                        <div class="inner" style="margin-top: 5px; padding: 0px;">
                            <center><h6>Eventos Paciente</h6></center>
                        </div>
                    </a>
                </li>--%>
                    <li class="nav-item nav-item-paciente col-12 col-sm-6 col-md-12">
                        <a id="btnRegistroClinico" class="nav-link panel-paciente btn-blue-grey disabled small-box bg-secondary customForPACMenu"
                            data-toggle="tab" href="#panel6" role="tab">
                            <div class="icon customForPACMenu text-center">
                                <i class="far fa-address-book" style="font-size: 30px; color: ghostwhite;"></i>
                            </div>
                            <div class="inner text-center" style="margin-top: 5px; padding: 0px;">
                                <h6>Registro Clínico</h6>
                            </div>
                        </a>
                    </li>
                    <%--<li class="nav-item nav-item-paciente">
                    <a id="btnAtencionesPaciente" class="nav-link btn-blue-grey disabled small-box bg-secondary customForPACMenu" data-toggle="tab" href="#panel7" role="tab">
                        <div class="icon customForPACMenu">
                            <center><i class="far fa-hospital" style="font-size: 30px; color: ghostwhite;"></i></center>
                        </div>
                        <div class="inner" style="margin-top: 5px; padding: 0px;">
                            <center><h6>Atención Paciente</h6></center>
                        </div>
                    </a>
                </li>--%>
                </ul>

            </div>
            <div class="col-12 col-sm-12  col-md-10  col-lg-10  col-xl-10">

                <!--INICIO BOTONERA PACIENTE-->

                <div class="tab-content vertical">
                    <div class="tab-pane fade in show active" id="panel1" role="tabpanel">

                        <div class="card">
                            <div class="card-header">
                                <h4>Buscador de Paciente</h4>
                            </div>
                            <div class="card-body">

                                <!--INICIO FORMULARIO DE BUSQUEDA-->

                                <div class="row mb-4">
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-2">
                                        <label class="active">Tipo Identificacion</label>
                                        <select id="sltTipoIdentificacionBusqueda" class="form-control"></select>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
                                        <label for="txtFiltroRut">Ficha Clínica</label>
                                        <div class="input-group">
                                            <input id="txtFiltroRut" type="text" class="form-control buscar" style="width: 90px" maxlength="8"
                                                placeholder="RUT" autocomplete="off" />
                                            <div class="input-group-prepend digito">
                                                <div class="input-group-text">-</div>
                                            </div>
                                            <input id="txtDv" type="text" class="form-control digito" style="width: 10px" disabled="disabled"
                                                placeholder="DV" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-1">
                                        <label for="txtFiltroNUI">NUI</label>
                                        <input id="txtFiltroNUI" type="text" class="form-control buscar" maxlength="50" placeholder="N° Ubicación Interna"
                                            autocomplete="off" />
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-2">
                                        <label for="txtFiltroNombre">Nombre</label>
                                        <input id="txtFiltroNombre" type="text" class="form-control buscar" maxlength="50" placeholder="Nombre/es"
                                            autocomplete="off" />
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-2">
                                        <label for="txtFiltroApe">Apellido Paterno</label>
                                        <input id="txtFiltroApe" type="text" class="form-control buscar" maxlength="50" placeholder="1° Apellido"
                                            autocomplete="off" />
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-2">
                                        <label for="txtFiltroSApe">Apellido Materno</label>
                                        <input id="txtFiltroSApe" type="text" class="form-control buscar" maxlength="50" placeholder="2° Apellido"
                                            autocomplete="off" />
                                    </div>
                                </div>

                                <div class="btn-group" role="group" aria-label="...">
                                    <button id="btnFiltroBandeja" type="button" class="btn btn-success mr-3"> <i class="fa fa-search"></i>Buscar</button>
                                    <button id="btnLimpiarFiltros" type="button" class="btn btn-secondary"> <i class="fa fa-eraser"></i>Limpiar filtros</button>
                                </div>

                                <div id="divPacientes" class="row center-horizontal mt-4">
                                    <div class="col-md-12">
                                        <table id="tblPacientes" class="table table-bordered table-hover"></table>
                                    </div>
                                </div>

                                <!--FIN FORMULARIO DE BUSQUEDA-->

                            </div>
                        </div>
                    </div>

                    <!--/.Panel 1-->

                    <!--Panel 2-->

                    <div id="panel2" class="tab-pane fade" role="tabpanel">
                        <div id="divDPaciente" class="card">
                            <div class="card-header">
                                <h4>Datos de paciente</h4>
                            </div>
                            <div class="card-body">
                                <!--INICIO FORMULARIO DATOS PACIENTE-->
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <div class="form-row">

                                            <div class="col-md-3">
                                                <label>Tipo de identificacion</label>
                                                <select id="sltTipoIdentificacion" class="form-control editable">
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <label>Ficha clínica</label>
                                                <div class="input-group">
                                                    <input type="text" maxlength="8" id="txtRutPaciente" class="form-control buscar" disabled
                                                        style="width: 90px">
                                                    <div class="input-group-prepend digito">
                                                        <div class="input-group-text">-</div>
                                                    </div>
                                                    <input id="txtRutDigitoPaciente" type="text" class="form-control digito" disabled
                                                        style="width: 10px" />
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <label id="lblNuiPaciente">Número de ubicación interna</label>
                                                <input id="txtNuiPaciente" type="text" class="form-control editable" />
                                            </div>
                                            <div id="dvCaratula" class="col-md-2" style="display: none;">
                                                <div class="row h-100 align-items-end">
                                                    <div class="col-12">
                                                        <a href="#" class="btn btn-outline-info form-control" onclick="linkPacienteCaratula()">Imprimir Portada</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <label>Nombre</label>
                                                <input id="txtNombrePaciente" type="text" class="form-control editable" data-required="true" maxlength="50" />
                                            </div>
                                            <div class="col-md-3">
                                                <label>Apellido paterno</label>
                                                <input id="txtApellidoPaciente" type="text" class="form-control editable" data-required="true" maxlength="50" />
                                            </div>
                                            <div class="col-md-3">
                                                <label>Apellido materno</label>
                                                <input id="txtApellidoMPaciente" class="form-control editable" maxlength="50" />
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <label>Fecha de nacimiento</label>
                                                <input id="txtFechaNacPaciente" type="date" class="form-control editable" data-required="true" />
                                            </div>
                                            <div class="col-md-3">
                                                <label>Edad</label>
                                                <input id="txtEdadPaciente" type="text" class="form-control disabled" />
                                            </div>
                                            <div class="col-md-3">
                                                <label>Sexo</label>
                                                <select id="ddlSexoPaciente" class="form-control editable" data-required="true">
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Genero</label>
                                                <select id="ddlGeneroPaciente" class="form-control editable" data-required="true">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <label>Nacionalidad</label>
                                                <select id="ddlNacionalidadPaciente" class="form-control editable" data-required="true">
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>País de Origen</label>
                                                <select id="ddlPaisPaciente" class="form-control editable" data-required="true">
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Región</label>
                                                <select id="ddlRegionPaciente" class="form-control editable" data-required="true">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <label>Provincia</label>
                                                <select id="ddlProvinciaPaciente" class="form-control editable" data-required="true">
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Ciudad</label>
                                                <select id="ddlCiudadPaciente" class="form-control editable" data-required="true">
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Dirección</label>
                                                <input id="txtDireccionPaciente" type="text" class="form-control editable" maxlength="50"/>
                                            </div>
                                            <div class="col-md-1">
                                                <label>N°</label>
                                                <input id="txtNumeroDireccionPaciente" type="text" class="form-control editable" maxlength="50"/>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <label>Teléfono</label>
                                                <input id="txtTelefonoPaciente" type="text" class="form-control editable" maxlength="50"/>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Otro teléfono</label>
                                                <input id="txtOtroTelefonoPaciente" type="text" class="form-control editable" maxlength="50" />
                                            </div>
                                            <div class="col-md-3">
                                                <label>¿Dirección Rural?</label><br />
                                                <input id="switchRural" type="checkbox" class="editable-switch" />
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <label>Email</label>
                                                <input id="txtEmailPaciente" type="email" class="form-control editable" maxlength="50" />
                                            </div>
                                            <div class="col-md-3">
                                                <label>Responde al nombre de</label>
                                                <input id="txtRespondePaciente" type="text" class="form-control editable" maxlength="50" />
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <label>Pueblo originario</label>
                                                <select id="ddlPuebloOrigPaciente" class="form-control editable">
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Nivel de escolaridad</label>
                                                <select id="sltEscolaridad" class="form-control editable">
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Estado conyugal</label>
                                                <select id="ddlEstadoConyuPaciente" class="form-control editable">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <label>Fecha de actualización</label>
                                                <input id="txtFechaActualizacionPaciente" type="text" class="form-control disabled" />
                                            </div>
                                            <div class="col-md-3">
                                                <label>Fecha de fallecimiento</label>
                                                <input id="txtFechaFallecimiento" type="date" class="form-control editable hide" />
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-footer">
                                <input type="button" id="btnGuardaDatos" class="btn btn-success float-right btnguardar" value="GUARDAR"
                                    onclick="guardarPaciente_click(this)" />
                            </div>
                            <!--FIN FORMULARIO DATOS PACIENTE-->
                        </div>

                    </div>

                    <!--/.Panel 2-->

                    <!--Panel 3-->

                    <div class="tab-pane fade" id="panel3" role="tabpanel">

                        <!--INICIO FORMULARIO DATOS CLINICOS-->

                        <div class="card">
                            <div class="card-header">
                                <h4>Datos clínicos</h4>
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Grupo Sanguineo</label>
                                        <select id="ddlGrupoSanguineoPaciente" class="form-control editable">
                                        </select>
                                    </div>
                                    <div class="col-md-3">

                                        <label>¿Alergia al latex?</label><br />
                                        <input id="rdoLatexSi" type="radio" class="editable-switch" name="latex"
                                            data-on-text="SI" data-off-text="SI" data-on-color="success" data-off-color="default" />
                                        <input id="rdoLatexNo" type="radio" class="editable-switch" name="latex"
                                            data-on-text="NO" data-off-text="NO" data-on-color="danger" data-off-color="default" />

                                    </div>
                                    <div class="col-md-3">
                                        <label>¿Otras alergias?</label><br />
                                        <input id="switchOtrasAlergias" type="checkbox" class="editable-switch" />
                                    </div>
                                </div>

                                <div id="divTxtOtrasAlergias" class="row hide">
                                    <div class="col-md-12">
                                        <label id="lblOtrasAlergias">Otras alergias</label>
                                        <textarea id="txtOtrasAlergias" class="form-control editable" rows="3" maxlength="300">
                                        </textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Antecedentes familiares</label>
                                        <textarea id="txtAntecedentesFamiliares" class="form-control editable" rows="2" maxlength="300">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-11">
                                        <label>Enfermedades hereditarias</label>
                                        <textarea id="txtEnfermedadesHereditarias" class="form-control editable" rows="2" maxlength="300">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-11">
                                        <label>Enfermedades previas</label>
                                        <textarea id="txtEnfermedadesPrevias" class="form-control editable" rows="2" maxlength="300">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-11">
                                        <label>Antecedentes neonatales</label>
                                        <textarea id="txtAntecedentesNeonatales" class="form-control editable" rows="2" maxlength="300">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-11">
                                        <label>Medicación previa</label>
                                        <textarea id="txtMedicacionPrevia" class="form-control editable" rows="2" maxlength="300">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-11">
                                        <label>Tratamientos farmacológicos</label>
                                        <textarea id="txtTratFarmacologico" class="form-control editable" rows="3" maxlength="300">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-11">
                                        <label>Antecedentes obstétricos</label>
                                        <textarea id="txtAntecedentesObstetricos" class="form-control editable" rows="2" maxlength="300">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-11">
                                        <label>Antecedentes quirúrgicos</label>
                                        <textarea id="txtAntecedentesQuirurgicos" class="form-control editable" rows="2" maxlength="300">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-row mb-4">
                                    <div id="divHabitos" class="col-md-11">
                                        <h5 class="mb-3 mt-3"><strong>Hábitos</strong></h5>

                                        <div class="form-row">
                                            <div class="col-md-11">
                                                <label>Tabaco</label>
                                                <textarea id="txtDetalleTabaco" class="form-control" rows="2" maxlength="200"
                                                    placeholder="Descripción Tabaco"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-11">
                                                <label>OH</label>
                                                <textarea id="txtDetalleOH" class="form-control" rows="2" maxlength="200"
                                                    placeholder="Descripción OH"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-11">
                                                <label>Drogas</label>
                                                <input id="txtDrogas" class="form-control editable" data-required="false" type="text"
                                                    maxlength="300" placeholder="Descripción Drogas" />
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-11">
                                                <label>Hipertensión arterial</label>
                                                <input id="txtHipertension" class="form-control editable" data-required="false" type="text"
                                                    maxlength="300" placeholder="Descripción Hipertensión Arterial" />
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-11">
                                                <label>Diabetes mellitus</label>
                                                <input id="txtDiabetes" class="form-control editable" data-required="false" type="text"
                                                    maxlength="300" placeholder="Descripción Diabetes Mellitus" />
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-11">
                                                <label>Asma</label>
                                                <input id="txtAsma" class="form-control editable" data-required="false" type="text"
                                                    maxlength="300" placeholder="Descripción Asma" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="text-right">

                                    <a id="btnGuardaClinicos" class="btn btn-info btnguardar" onclick="guardarDatosClinicos_click(this)">GUARDAR
                                    </a>

                                </div>
                            </div>
                        </div>

                        <!--FIN FORMULARIO DATOS CLINICOS-->

                    </div>

                    <!--/.Panel 3-->

                    <!--Panel 3-->

                    <div class="tab-pane fade" id="panel4" role="tabpanel">

                        <!--INICIO FORMULARIO DATOS PREVISIONALES-->

                        <div class="card">
                            <div class="card-header">
                                <h4>Datos previsionales</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col-md-11">
                                        <label>Aseguradora</label>
                                        <select id="ddlAseguradora" class="form-control editable">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-11">
                                        <label>Tramo</label>
                                        <select id="ddlTramo" class="form-control editable disabled">
                                        </select>
                                    </div>
                                </div>
                                <div id="divPrais" class="form-row">
                                    <div class="col-md-11">
                                        <label>¿Pertenece a pacientes PRAIS?</label><br />
                                        <input id="switchPrais" type="checkbox" class="editable-switch" />
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <input type="button" id="btnGuardaPrevision" class="btn btn-success btnguardar" value="Guardar" onclick="guardarDatosPrevisionales_click(this)" />
                            </div>
                        </div>
                        <!--FIN FORMULARIO DATOS PREVISIONALES-->
                    </div>
                    <!--/.Panel 3-->
                    <!--Panel 3-->
                    <div class="tab-pane fade" id="panel5" role="tabpanel">
                        <!--INICIO EVENTOS PACIENTE-->
                        <div class="card">
                            <div class="card-header">
                                <h4>Eventos</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <a id="lnkNuevoEvento" class="btn btn-default" onclick="showNuevoEvento();return false;">
                                            <i class="fa fa-plus"></i>AGREGAR EVENTO
                                        </a>
                                    </div>
                                    <div class="col-md-6"></div>
                                    <div class="col-md-3" style="display: flex; flex-direction: row; justify-content: flex-end;">
                                        <div id="btnExportarEvento" class="btn-group">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false">
                                                Exportar</button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#" onclick="javascript:pdfGrilla('eventos');">
                                                    <i class="fa fa-file-pdf-o" style="color: orangered"></i>PDF
                                                </a>
                                                <a class="dropdown-item" href="#">
                                                    <i class="fa fa-file-excel-o" style="color: green"></i>Excel
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row center-horizontal">
                                    <div class="col-md-12">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="row center-horizontal">
                                            <div id="errGrillaEventos" class="alert alert-secondary center-horizontal hide" style="width: 100%" role="alert"></div>
                                            <div id="eventos" style="width: 100%">
                                                <table id="tblEventos" class="table table-bordered table-hover" ></table>
                                            </div>
                                        </div>
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--FIN EVENTOS PACIENTE-->
                    </div>
                    <!--/.Panel 3-->
                    <div class="tab-pane fade" id="panel6" role="tabpanel">
                        <!--INICIO Registro Clínico-->
                        <div class="card">
                            <div class="card-header">
                                <h4>Registro Clínico</h4>
                            </div>
                            <div class="card-body" style="padding: 20px">
                                <ul class="nav  md-pills pills-primary center-horizontal" role="tablist" style="padding-top: 10px;">
                                    <li class="nav-item">
                                        <a id="btnSolPabellon" data-id-sistema="2" class="material-icons nav-link disabled" data-toggle="tab"
                                            data-placement="bottom" title="Formulario de Indicación Qx" href="#panel10" role="tab">FIQ <span id="bdFormularioIndQx" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="btnProtocolo" data-id-sistema="17" class="material-icons nav-link disabled" data-toggle="tab"
                                            data-placement="bottom" title="Protocolo Operatorio" href="#panel11" role="tab">PO <span id="bdProtocoloOperatorio" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <%--<li class="nav-item">
                                        <asp:LinkButton ID="btnExamen" runat="server" data-id-sistema="" CssClass="material-icons nav-link disabled" data-toggle="tab" data-placement="bottom" title="Exámenes" href="#panel12" role="tab">&nbsp;EX&nbsp;<span id="bdExamen" class="badge badge-pill btn-secondary badge-count"></span></asp:LinkButton>
                                    </li>--%>
                                    <li class="nav-item">
                                        <a id="btnReceta" data-id-sistema="17" class="material-icons nav-link disabled" data-toggle="tab"
                                            data-placement="bottom" title="Recetas" href="#panel13" role="tab">RC <span id="bdReceta" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="btnBiopsia" data-id-sistema="1" class="material-icons nav-link disabled" data-toggle="tab"
                                            data-placement="bottom" title="Biopsias" href="#panel14" role="tab">BI <span id="bdBiopsia" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="btnHospitalizacion" data-id-sistema="14" class="material-icons nav-link disabled"
                                            data-toggle="tab" data-placement="bottom" title="Hospitalizacion" href="#panel15" role="tab">HP <span id="bdHospitalizacion" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="btnUrgencia" data-id-sistema="3" class="material-icons nav-link disabled" data-toggle="tab"
                                            data-placement="bottom" title="Atencion Urgencia" href="#panel16" role="tab">UR <span id="bdUrgencia" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="btnEpicrisis" data-id-sistema="17" class="material-icons nav-link disabled" data-toggle="tab"
                                            data-placement="bottom" title="Epicrisis" href="#panel17" role="tab">EC <span id="bdEpicrisis" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="btnIPD" data-id-sistema="14" class="material-icons nav-link disabled" data-toggle="tab"
                                            data-placement="bottom" title="IPD" href="#panel18" role="tab">IPD <span id="bdIPD" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="btnHojaEvolucion" data-id-sistema="14" class="material-icons nav-link disabled" data-toggle="tab"
                                            data-placement="bottom" title="Hojas de Evolucion" href="#panel19" role="tab">HE <span id="bdHE" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="btnInterconsulta" data-id-sistema="14" class="material-icons nav-link disabled" data-toggle="tab"
                                            data-placement="bottom" title="Interconsultas" href="#panel20" role="tab">IC <span id="bdIC" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="btnLaboratorio" data-id-sistema="7" class="material-icons nav-link disabled" data-toggle="tab"
                                            data-placement="bottom" title="Laboratorio" href="#panel21" role="tab">LAB <span id="bdLAB" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="btnEventoAdverso" data-id-sistema="20" class="material-icons nav-link disabled" data-toggle="tab"
                                            data-placement="bottom" title="Evento Adverso" href="#panel22" role="tab">NEA <span id="bdNEA" class="badge badge-pill btn-secondary badge-count hide"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="btnRefresh" class="material-icons nav-link nav-refresh" data-placement="bottom" title="Actualizar">
                                            <i class="fa fa-sync"></i>
                                        </a>
                                    </li>
                                </ul>
                                <div class="col-md-12 mt-3">
                                    <div id="alertRegistroClinico" class="alert alert-secondary center-horizontal"
                                        style="width: 100%; display:none;" role="alert">
                                    </div>
                                </div>
                                <div class="tab-content horizontal">
                                    <div class="tab-pane fade panel-rc" id="panel10" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <table id="tblFormularioIndQx" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel11" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <table id="tblProtocoloOperatorio" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel12" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <div id="alertExamen" class="alert alert-secondary center-horizontal hide"
                                                    style="width: 100%" role="alert">
                                                    Este paciente no posee Examenes
                                                </div>
                                                <table id="tblExamen" class="table table-bordered table-hover" style="width: 100%"></table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel13" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <table id="tblReceta" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel14" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="row">
                                            <div class="col-md-12">                                                
                                                <table id="tblBiopsia" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel15" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="row">
                                            <div class="col-md-12">                                                
                                                <table id="tblHospitalizacion" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel16" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="row">
                                            <div class="col-md-12">                                                
                                                <table id="tblUrgencia" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel17" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="row">
                                            <div class="col-md-12">                                                
                                                <table id="tblEpicrisis" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel18" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="row">
                                            <div class="col-md-12">                                                
                                                <table id="tblIPD" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel19" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <table id="tblHojaEvolucion" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel20" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <table id="tblInterconsulta" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel21" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <div id="alertLaboratorio" class="alert alert-secondary center-horizontal hide"
                                                    style="width: 100%" role="alert">
                                                    Este paciente no posee exámenes de laboratorio
                                                </div>
                                                <table id="tblLaboratorio" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                        <div id="divLeyendaMicro" style="display: none;">
                                            <span style="height: 25px; width: 25px; background-color: #eeffdd; border-radius: 50%; display: inline-block; border: 1px solid black;"></span>
                                            Exámenes microbiología
                                        </div>
                                    </div>
                                    <div class="tab-pane fade panel-rc" id="panel22" role="tabpanel">
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <table id="tblEventoAdverso" class="table table-bordered table-hover" style="width: 100%">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--FIN Registro Clínico-->
                    </div>
                    <div class="tab-pane fade" id="panel7" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h4>Registro Clínico</h4>
                            </div>
                            <div class="card-body">
                                <div>&nbsp;</div>
                                <div>&nbsp;</div>
                                <div class="row center-horizontal">
                                    <table id="tblAtenciones" class="table table-bordered table-sm table-hover" style="font-size: smaller">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--FIN BOTONERA PACIENTE-->

            </div>
        </div>

    </section>

    <!-- MODAL GUARDAR CAMBIOS -->

    <div class="modal fade left" id="mdlGuardado" role="dialog" aria-labelledby="mdlGuardado" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info center-horizontal">
                    <div class="heading lead form-inline"><b><u>GUARDADO DE DATOS</u></b></div>
                </div>
                <div class="modal-body">
                    <div class="active row center-horizontal">Se han realizado cambios en los datos del paciente</div>
                    <div class="active row center-horizontal">¿Desea guardar los cambios?</div>
                    <div style="height: 10px;">&nbsp;</div>
                </div>
                <div class="modal-footer" style="display: initial !important">
                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <center>
                                <a ID="btnCancelarCambios" class="btn btn-danger" OnClick="$('#mdlGuardado').modal('toggle')">
                                CANCELAR
                                </a>
                                <a ID="btnDescartarCambios" class="btn btn-info" OnClick="descartaGuardar()">
                                DESCARTAR
                                </a>
                                <a ID="btnGuardarCambios" class="btn btn-default" OnClick="btnGuardaDatosModal()" data-pacienteDclinico="false">
                                GUARDAR
                                </a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN MODAL GUARDAR CAMBIOS -->

    <div class="modal modal-print fade" id="mdlImprimir" role="dialog" aria-labelledby="mdlImprimir" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i>Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameImprimir" frameborder="0" onload="$('#modalCargando').hide();"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade left" id="mdlDocumentosLab" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info center-horizontal">
                    <div class="heading lead form-inline">Documentos</div>
                </div>
                <div class="modal-body">
                    <table id="tblDocumentosLab" class="table table-bordered table-sm table-hover" style="font-size: smaller"></table>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-info float-right" value="CERRAR" data-dismiss="modal" />
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade left" id="mdlNuevoPaciente" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-dialog-full-width">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead mb-0">
                        <strong><i class="fas fa-user"></i>Nuevo paciente</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">x</span>
                    </button>
                </div>
                <div class="modal-body" id="divNuevoModalBody">
                    <wuc:Paciente ID="wucPaciente" runat="server" />
                    <div id="divDatosAdicionalesPaciente">

                        <div id="divUbicacionPaciente" class="row mt-2">
                            <div class="col-md-3">
                                <label for="sltNacionalidad">Nacionalidad</label>
                                <select id="sltNacionalidad" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="sltPais">País</label>
                                <select id="sltPais" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="sltRegion">Región</label>
                                <select id="sltRegion" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="sltProvincia">Provincia</label>
                                <select id="sltProvincia" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="sltCiudad">Ciudad</label>
                                <select id="sltCiudad" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>

                        <div id="divExistente" class="form-row mt-2">
                            <div class="col-md-12 text-center">
                                <div class="alert alert-warning">
                                    Este paciente ya existe.
                                    <br />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <a id="btnNuevoPaciente" class="btn btn-success">Nuevo paciente</a>
                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/ModuloPaciente/BandejaPAC.js") + "?" + GetVersion() %>"></script>
     <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?" + GetVersion() %>"></script>

</asp:Content>
