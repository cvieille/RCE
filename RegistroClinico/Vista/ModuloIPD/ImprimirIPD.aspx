﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImprimirIPD.aspx.cs" Inherits="RegistroClinico.Vista.ImprimirIPD" MasterPageFile="~/Vista/MasterPages/Impresion.Master" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <style>
        .tabla-custom{
            table-layout: fixed;
            width: 100%;
        }

        .base td{
            width: 8.333%;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">
    <asp:Button runat="server" ID="btnHidden" OnClick="btnHidden_Click" style="display:none;" />

        <div class="panel">
            <div class="panel-heading white">
                <div class="row">
                    <div class="col-xs-4 text-left">
                        <img src="<%= GetHost() %>/Style/IMG/MINSAL.jpg" style="width: 100px;" />
                    </div>
                    <div class="col-xs-4 text-center">
                        <img src="<%= GetHost() %>/Style/IMG/ACRLOGO.jpg" style="width: 100px;" />
                    </div>
                    <div class="col-xs-4 text-right">
                        <img src="<%= GetHost() %>/Style/IMG/HOSPLOGO.jpg" style="width: 100px;" />
                    </div>
                </div>
            </div>
        <div>

       <div class="panel-body" style="padding: 0px;">
            <table class="tabla-custom">

                <tr>
                    <td style="border: none;" class="text-center" colspan="12"><h3><b>INFORME DEL PROCESO DIAGNÓSTICO</b></h3></td>
                </tr>

                <%-- Establecimiento  --%>
                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12"><h4><b>Establecimiento</b></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="4">Servicio de Salud: <asp:Label ID="servicioLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="4">Establecimiento: <asp:Label ID="establecimientoLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="4">Especialidad: <asp:Label ID="especialidadLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="4">Tipo de Ingreso: <asp:Label ID="tipoIngresoLbl" runat="server"></asp:Label></td>
                </tr>

                <%-- Paciente  --%>

                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12"><h4><b>Datos del Paciente</b></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="4">RUT <asp:Label ID="rutPacienteLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="8">Nombre: <asp:Label ID="nombrepacienteLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="6">Dirección <asp:Label ID="direccionLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="3">Teléfono: <asp:Label ID="telefonoLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="3">Otro teléfono: <asp:Label ID="oTelefonoLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="3">Sexo <asp:Label ID="sexoLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="3">Genero: <asp:Label ID="generoLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="4">Fecha de Nacimiento: <asp:Label ID="fNacimientoLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="2">Edad: <asp:Label ID="edadLbl" runat="server"></asp:Label></td>
                 </tr>
               
                    <%-- Datos Clínicos  --%>

                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12"><h4><b>Datos Clínicos</b></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="2">Sistema AUGE: <asp:Label ID="auge" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="10">Problema de Salud Auge: <asp:Label ID="problemaAugeLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="12">Diagnóstico: <asp:Label ID="diagnosticoLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="12">Fundamento Diagnóstico: <asp:Label ID="fundamentoLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="12">Tratamiento e Indicaciones: <asp:Label ID="tratamientoLbl" runat="server"></asp:Label></td>
                </tr>

                <tr>
                    <td class="contenido-celda" colspan="5">¿Confirmar diagnóstico?: <asp:Label ID="confirmacionLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="7"> El tratamiento debe iniciarse a mas tardar el: <asp:Label ID="inicioTratamientolbl" runat="server"></asp:Label></td>
                </tr>


                <%-- Datos Profesional  --%>

                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12"><h4><b>Datos Profesional</b></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="3">RUT: <asp:Label ID="rutProfesional" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="9">Nombre Profesional: <asp:Label ID="nombreProfesionalLbl" runat="server"></asp:Label></td>
                </tr>

            </table>

         <asp:Panel ID="panelConstancia" runat="server">


          <div style='page-break-after:always'></div>


           <div class="panel">
            <div class="panel-heading white">
                <div class="row">
                    <div class="col-xs-4 text-left">
                        <img src="<%= GetHost() %>/Style/IMG/MINSAL.jpg" style="width: 100px;" />
                    </div>
                    <div class="col-xs-4 text-center">
                        <img src="<%= GetHost() %>/Style/IMG/ACRLOGO.jpg" style="width: 100px;" />
                    </div>
                    <div class="col-xs-4 text-right">
                        <img src="<%= GetHost() %>/Style/IMG/HOSPLOGO.jpg" style="width: 100px;" />
                    </div>
                </div>
            </div>
            <div>
            
               <table class="tabla-custom">

                    <tr>
                    <td style="border: none;" class="text-center" colspan="12"><h3><b>Constancia Paciente</b></h3></td>
                </tr>

                    <%-- Constancia  --%>
                    <tr>
                        <td class="fondo-titulos contenido-celda" colspan="12"><h4><b>Constancia Paciente</b></td>
                    </tr>

                   <tr>
                    <td class="contenido-celda" colspan="5">Nombre Institución: <asp:Label ID="CinstitucionLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="5">Dirección: <asp:Label ID="CdireccionLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="2">Ciudad: <asp:Label ID="CciudadLbl" runat="server"></asp:Label></td>
                   </tr>


                   <%-- Paciente  --%>

                <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12"><h4><b>Datos del Paciente</b></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="4">RUT <asp:Label ID="CrutPacienteLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="8">Nombre: <asp:Label ID="CnombrepacienteLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="6">Dirección <asp:Label ID="CdireccionPLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="3">Teléfono: <asp:Label ID="CtelefonoLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="3">Otro teléfono: <asp:Label ID="CoTelefonoLbl" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="contenido-celda" colspan="3">Sexo <asp:Label ID="CsexoLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="3">Genero: <asp:Label ID="CgeneroLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="4">Fecha de Nacimiento: <asp:Label ID="CfNacimientoLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="2">Edad: <asp:Label ID="CedadLbl" runat="server"></asp:Label></td>
                 </tr>
                   <%-- info Médica  --%>
                 <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12"><h4><b>Información Médica</b></td>
                </tr>
                 <tr>
                    <td class="contenido-celda" colspan="5">¿Confirmar diagnóstico?: <asp:Label ID="cConfirmacionLbl" runat="server"></asp:Label></td>
                </tr>

                   <%-- Representante  --%>
                   <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12"><h4><b>Representante</b></td>
                </tr>
                 <tr>
                    <td class="contenido-celda" colspan="3">Tipo Representante: <asp:Label ID="tipoRepresentanteLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="3">Fecha: <asp:Label ID="CfechaConstanciaLbl" runat="server"></asp:Label></td>
                    <td class="contenido-celda" colspan="6">Nombre: <asp:Label ID="CnombreRepresentanteLbl" runat="server"></asp:Label></td>
                 </tr>

                   <%-- Constancia  --%>
                   <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12"><h4><b>Constancia</b></td>
                </tr>
                 <tr>
                    <td class="fondo-titulos contenido-celda" colspan="12">
                        Declaro que, con esta fecha y hora, he tomado conocimiento que tengo derecho a acceder a las "Garantias Eplícitas en Salud",
                        GES, siempre que la atenció sea otorgada en la "Red de Prestadores" que me corresponde según Fonasa o Isapre, a la que me encuentro
                        adscrito.
                    </td>
                 </tr>
                  <tr>
                      <td class="contenido-celda text-center" colspan="6"> 
                          <br />
                          <br />
                          <br />
                          _________________________
                          <br />
                          Informé diagnóstico GES
                          <br />
                          Firma de la persona que notifica
                      </td>
                      <td class="contenido-celda text-center" colspan="6">  
                          <br />
                          <br />
                          <br />
                          _________________________
                          <br />
                          Tomé conocimiento
                          <br />
                          Firma o huella digital del paciente o representante
                      </td>
                  </tr>



                </table>
           </asp:Panel>
              

        </div>
    </div>
        



</asp:Content>