﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NuevoIPD.aspx.cs" Inherits="RegistroClinico.Vista.ModuloIPD.NuevoIPD" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/ModalEvento.ascx" TagName="mdlEventos" TagPrefix="mdlEventos" %>
<%@ Register Src="~/Vista/UserControls/ModalPerfiles.ascx" TagName="mdlPerfiles" TagPrefix="mdlPerfiles" %>

<%--CONTROL EVENTO--%>
<%@ Register Src="~/Vista/UserControls/ModalEvento.ascx" TagName="mdlEventos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPacienteEvento.ascx" TagName="mdlPacienteEvento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <%--CONTROL EVENTO--%>
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:mdlEventos ID="wucMdlEventos" runat="server" />
    <wuc:mdlPacienteEvento ID="wucMdlPacienteEvento" runat="server" />

    <div id="alertObligatorios" class="alert text-center alert-obligatorios">
        <b>Los campos marcados son obligatorios (*)</b>.
    </div>

    <input type="hidden" id="fechaEvento" value="0" />
    <input type="hidden" id="idRepresentante" value="0" />
    <input type="hidden" id="idConstancia" value="0" />
    <input type="hidden" id="idIPD" value="0" />
    <input type="hidden" id="idProfesional" value="0" />
    <input type="hidden" id="idPaciente" value="0" />

    <div class="card card-body">
        <div class="card">
            <div class="card-header bg-light">
                <h2 class="text-center py-4">
                    <i class="fa fa-file-alt"></i> IPD
                </h2>
            </div>
            <div id="divIPD" class="card-body p-0">
                
                <div class="card">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Informe del proceso diagnóstico</strong> </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Servicio de salud</label>
                                <select id="ddlServicio" data-required="true" class="form-control">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Establecimiento</label>
                                <select id="ddlEstablecimiento" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Especialidad</label>
                                <select id="ddlEspecialidad" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Tipo de ingreso</label>
                                <select id="ddlAmbito" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <wuc:Paciente ID="wucPaciente" runat="server" />
                
                <div class="card">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Datos clínicos</strong> </h5>
                    </div>
                    <div class="card-body">
                        <div class="post">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>¿Pertenece al sistema AUGE?</label>
                                    <br />
                                    <input type="checkbox" id="switchAuge" data-label-width="1" data-on-text="SI" data-off-text="NO" data-on-color="info" data-off-color="warning" />
                                </div>
                                <div id="divAuge" class="col-md-4" style="display: none;">
                                    <label for="ddlAuge">Problema de salud AUGE</label>
                                    <select id="ddlAuge" class="form-control">
                                    </select>
                                </div>
                                <div id="divSubAuge" class="col-md-4" style="display: none;">
                                    <label>Subgrupo o subproblema de salud AUGE</label>
                                    <select id="ddlSubAuge" class="form-control">
                                        <option selected>-Seleccione-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label id="lbltxtDiagnostico">Diagnóstico</label>
                                    <textarea id="txtDiagnostico" maxlength="125" data-required="true" class="form-control" rows="5"></textarea>
                                </div>
                                <div class="col-md-4">
                                    <label id="lbltxtFundamentoDiag">Fundamentos del diagnóstico</label>
                                    <textarea id="txtFundamentoDiag" maxlength="125" data-required="true" class="form-control" rows="5"></textarea>
                                </div>
                                <div class="col-md-4">
                                    <label>¿Confirmar diagnóstico?</label>
                                    <select id="ddlTipoIPD" class="form-control" data-required="true">
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label id="lbltxtTratamiento">Tratamiento e indicaciones</label>
                                    <textarea id="txtTratamiento" maxlength="125" data-required="true" class="form-control" rows="5"></textarea>
                                </div>
                                <div class="col-md-4">
                                    <label>El tratamiento deberá iniciarse a mas tardar el</label>
                                    <input type="date" data-required="true" id="txtFechaTrat" required class="form-control" />
                                    <span style="color: crimson; display: none;" id="spanFecha">
                                        <i class="fa fa-times"></i><strong>La fecha debe ser mayor a la fecha de evento</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Previsión actual paciente</label>
                                    <select id="ddlPrevision" data-required="true" class="form-control">
                                    </select>
                                </div>
                                <div id="divPrevisionTramo" class="col-md-3">
                                    <label>Tramo</label>
                                    <select id="ddlPrevisionTramo" data-required="true" class="form-control">
                                    </select>
                                </div>
                                <div id="divIPDFecha" class="col-md-2 offset-1">
                                    <label>Fecha IPD</label>
                                    <input id="txtIPDFecha" type="date" data-required="true" required class="form-control" />
                                </div>
                                <div class="col-md-2">
                                    <label>Hora IPD</label>
                                    <input id="txtIPDHora" type="time" data-required="true" required class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Datos del profesional</strong> </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <label id="lblValidarRutPro">RUT</label>
                                <div class="input-group">
                                    <input id="txtRutPro" type="text" class="form-control disabled" disabled style="width: 110px;">
                                    <div class="input-group-prepend digito">
                                        <div class="input-group-text">-</div>
                                    </div>
                                    <input type="text" id="txtRutDigitoPro" class="form-control digito disabled" style="width: 10px" 
                                        disabled />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>Nombre</label>
                                <input type="text" id="txtNombrePro" placeholder="&nbsp;" class="form-control disabled" readonly />
                            </div>
                            <div class="col-md-3">
                                <label>Apellido paterno</label>
                                <input type="text" id="txtApellidoPPro" placeholder="&nbsp;" class="form-control disabled" readonly />
                            </div>
                            <div class="col-md-3">
                                <label>Apellido materno</label>
                                <input type="text" id="txtApellidoMPro" placeholder="&nbsp;" class="form-control disabled" readonly />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            
            <div class="card-footer">
                <button id="btnContinuar" class="btn btn-info float-right" type="submit">CONTINUAR</button>
            </div>

        </div>
    </div>
    
    <div class="modal fade bottom" id="mdlConstancia" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Constancia</p>
                </div>
                <div class="modal-body">
                    <ul class="list-group list-group-flush" id="divConstancia">
                        <li class="list-group-item">
                            <div class="text-center">
                                <i class="fa fa-clipboard-list fa-3x mb-3 animated fadeInUp"></i>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Profesional</label>
                                    <input type="text" id="txtConstanciaPro" placeholder="&nbsp;" class="form-control disabled" readonly />
                                </div>
                                <div class="col-md-4">
                                    <label>Rut</label>
                                    <input type="text" id="txtConstanciaProRut" placeholder="&nbsp;" class="form-control disabled" readonly />
                                </div>
                                <div class="col-md-2">
                                    <label>Fecha constancia</label>
                                    <input id="txtConstanciaFecha" type="date" required class="form-control" />
                                </div>
                                <div class="col-md-2">
                                    <label>Hora constancia</label>
                                    <input id="txtConstanciaHora" type="time" required class="form-control" />
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>¿El paciente se representa a si mismo?</label>
                                    <br />
                                    <input type="checkbox" id="switchTitular" data-label-width="1" data-on-text="SI" data-off-text="NO" data-on-color="info" data-off-color="warning" />
                                </div>
                                <div class="col-md-2">
                                    <label>Tipo representante</label>
                                    <select id="ddlConstanciaRepresentante" data-required="true" class="form-control">
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label>RUT representante</label>
                                    <div class="input-group">
                                        <input type="text" maxlength="8" id="txtRutRep" class="form-control" style="width: 95px">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">-</div>
                                        </div>
                                        <input type="text" id="txtRutDigitoRep" class="form-control" style="width: 10px">
                                    </div>
                                </div>
                                <div class="col-md-4 offset-1">
                                    <label>Nombre</label>
                                    <input type="text" id="txtNombreRep" placeholder="&nbsp;" class="form-control disabled" readonly />
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="modal-footer justify-content-center">
                    <button id="btnNuevaConstancia" class="btn btn-info">Guardar constancia</button>
                    <button id="btnOmitirConstancia" class="btn btn-secondary">Omitir constancia</button>
                </div>
            </div>
        </div>
    </div>
    

    <script src="<%= ResolveClientUrl("~/Script/ModuloIPD/NuevoIPD.js") + "?" + GetVersion() %>"></script>

</asp:Content>
