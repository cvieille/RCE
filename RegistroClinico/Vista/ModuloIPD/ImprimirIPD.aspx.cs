﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using RegistroClinico.Datos.DTO;

namespace RegistroClinico.Vista
{
    public partial class ImprimirIPD : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Valida sesión según los permisos que tenga el perfil
            NegSession negSession = new NegSession(Page);
            negSession.ValidarPermisosSession();
            string idDAU = Request.QueryString["id"];
            string usuarioLogeado = Session["LOGIN_USUARIO"].ToString();
            panelConstancia.Visible = false;

            //Javier
            try
            {
                HttpContext context = HttpContext.Current;
                string dominio = Funciones.GetUrlWebApi();

                idDAU = Request.QueryString["id"];
                usuarioLogeado = Session["LOGIN_USUARIO"].ToString();
                string url = string.Format("{0}RCE_IPD/Imprimir/{1}", dominio, idDAU);
                string token = string.Format("Bearer {0}", Session["TOKEN"]/*Page.Request.Cookies["DATA-TOKEN"].Value*/);
                string sJson = Funciones.ObtenerJsonWebApi(url, token);

                Root datosIPD = JsonConvert.DeserializeObject<Root>(sJson);

                DibujaDatos(datosIPD);
            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al imprimir IPD", ex.Message + ex.StackTrace + ex.InnerException);
            }
            Funciones funciones = new Funciones();
            string doc = "IPD"; //Agregar tt Nombre Paciente
            funciones.ExportarPDFWK(this.Page, doc, idDAU, usuarioLogeado);
        }
        public string GetHost()
        {
            return Funciones.GetHost(HttpContext.Current);
        }
        
        public void DibujaDatos(Root datosIPD)
        {
            Funciones fun = new Funciones();
            var edad = Funciones.CalculaEdadenAños(datosIPD.GEN_fec_nacimientoPaciente.ToString());

            servicioLbl.Text = datosIPD.GEN_nombreServicio_Salud;
            establecimientoLbl.Text = datosIPD.GEN_nombreEstablecimiento;
            especialidadLbl.Text = datosIPD.GEN_nombreEspecialidad;
            tipoIngresoLbl.Text = datosIPD.GEN_nombreAmbito;

            //Paciente
            rutPacienteLbl.Text = datosIPD.GEN_numero_documentoPaciente;
            nombrepacienteLbl.Text = datosIPD.GEN_nombrePaciente;
            direccionLbl.Text = datosIPD.GEN_dir_callePaciente;
            telefonoLbl.Text = datosIPD.GEN_telefonoPaciente;
            oTelefonoLbl.Text = datosIPD.GEN_otros_fonosPaciente;
            sexoLbl.Text = datosIPD.GEN_nomSexo;
            generoLbl.Text = datosIPD.GEN_Tipo_Genero.GEN_nombreTipo_Genero;
            fNacimientoLbl.Text = datosIPD.GEN_fec_nacimientoPaciente.ToString();
            edadLbl.Text = edad.ToString();

            //DatosClinicos
            if(datosIPD.RCE_Patologia_GES == null)
            {
                auge.Text = "NO";
            }
            else
            {
                auge.Text =  "SI";
                problemaAugeLbl.Text = datosIPD.RCE_Patologia_GES.RCE_descripcion_Patologia_GES;

            }

            diagnosticoLbl.Text = datosIPD.RCE_diagnosticoIPD;
            fundamentoLbl.Text = datosIPD.RCE_fundamentoIPD;
            tratamientoLbl.Text = datosIPD.RCE_tratamientoIPD;
            confirmacionLbl.Text = datosIPD.RCE_descripcionTipo_IPD;
            inicioTratamientolbl.Text = datosIPD.RCE_inicio_tratamientoIPD.ToString();

            //DatosProfesional
            rutProfesional.Text = datosIPD.GEN_numero_documentoPersonasProfesionalIPD;
            nombreProfesionalLbl.Text = datosIPD.GEN_nombrePersonasProfesionalIPD;

            /* 
             * Constancia 
             */
            if(datosIPD.RCE_Constancia_GES != null) {
                panelConstancia.Visible = true;

            System.Diagnostics.Debug.WriteLine("no es nulo");
            CinstitucionLbl.Text = datosIPD.GEN_nombreEstablecimiento;
            CdireccionLbl.Text = datosIPD.GEN_direccionEstablecimiento;
            CciudadLbl.Text = datosIPD.GEN_nombreCiudad;

            CrutPacienteLbl.Text = datosIPD.GEN_numero_documentoPaciente;
            CnombrepacienteLbl.Text = datosIPD.GEN_nombrePaciente;
            CdireccionPLbl.Text = datosIPD.GEN_dir_callePaciente;
            CtelefonoLbl.Text = datosIPD.GEN_telefonoPaciente;
            CoTelefonoLbl.Text = datosIPD.GEN_otros_fonosPaciente;
            CsexoLbl.Text = datosIPD.GEN_nomSexo;
            CgeneroLbl.Text = datosIPD.GEN_Tipo_Genero.GEN_nombreTipo_Genero;
            CfNacimientoLbl.Text = datosIPD.GEN_fec_nacimientoPaciente.ToString();
            CedadLbl.Text = edad.ToString();

            cConfirmacionLbl.Text = datosIPD.RCE_descripcionTipo_IPD;
            tipoRepresentanteLbl.Text = datosIPD.RCE_Constancia_GES.GEN_nombreTipo_Representante;
            CfechaConstanciaLbl.Text = datosIPD.RCE_Constancia_GES.RCE_fechaConstancia_GES.ToString();
            CnombreRepresentanteLbl.Text = datosIPD.GEN_nombreRepresentante_Constancia_GES;
            }else
            {
                System.Diagnostics.Debug.WriteLine("nulo");

                panelConstancia.Visible = false;
            }

        }

        protected void btnHidden_Click(object sender, EventArgs e)
        {
            string sCommand = Request.Form["__EVENTARGUMENT"];
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;
            JObject des = JsonConvert.DeserializeObject<JObject>(sCommand, settings);
            List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
        }

        //Clases
        public class RCEConstanciaGES
        {
            public int RCE_idConstancia_GES { get; set; }
            public DateTime RCE_fechaConstancia_GES { get; set; }
            public string GEN_nombreTipo_Representante { get; set; }
        }

        public class Root
        {
            public int RCE_idIPD { get; set; }
            public string GEN_nombreServicio_Salud { get; set; }
            public string GEN_rutEstablecimiento { get; set; }
            public string GEN_direccionEstablecimiento { get; set; }
            public string GEN_nombreCiudad { get; set; }
            public string GEN_nombreEstablecimiento { get; set; }
            public string GEN_nombreEspecialidad { get; set; }
            public string GEN_numero_documentoPaciente { get; set; }
            public string GEN_nombrePaciente { get; set; }
            public string GEN_dir_callePaciente { get; set; }
            public string GEN_dir_numeroPaciente { get; set; }
            public string GEN_telefonoPaciente { get; set; }
            public string GEN_otros_fonosPaciente { get; set; }
            public string GEN_emailPaciente { get; set; }
            public DateTime GEN_fec_nacimientoPaciente { get; set; }
            public string GEN_nomSexo { get; set; }
            public DateTime RCE_fechaIPD { get; set; }
            public string RCE_diagnosticoIPD { get; set; }
            public string RCE_fundamentoIPD { get; set; }
            public string RCE_tratamientoIPD { get; set; }
            public DateTime RCE_inicio_tratamientoIPD { get; set; }
            public string RCE_descripcionTipo_IPD { get; set; }
            public RCEConstanciaGES RCE_Constancia_GES { get; set; }
            public string GEN_nombreRepresentante_Constancia_GES { get; set; }
            public string GEN_nombrePersonasProfesionalIPD { get; set; }
            public string GEN_numero_documentoPersonasProfesionalIPD { get; set; }
            public string GEN_nombreAmbito { get; set; }
            public RCEPatologiaGES RCE_Patologia_GES { get; set; }

            public GENTipoGenero GEN_Tipo_Genero { get; set; }
        }

        public class GENTipoGenero
        {
            public int GEN_idTipo_Genero { get; set; }
            public string GEN_nombreTipo_Genero { get; set; }
        }

        public class RCEPatologiaGES
        {
            public int RCE_idPatologia_GES { get; set; }
            public string RCE_descripcion_Patologia_GES { get; set; }
        }
    }
}