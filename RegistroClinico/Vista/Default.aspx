﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RegistroClinico.Default" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    
    <script>

        localStorage.removeItem("sSession");

        $(document).ready(function () {
            //$('#ul_ayuda').show();
            sessionStorage.removeItem("ab");
            sessionStorage.removeItem("idMenu");
            $("#Content_btnLogin").data("modal", false);
            $(".form-simple").css("height", $(".form-simple").parent().parent().css("height"));
            ShowModalCargando(false);
            localStorage.setItem("urlWebApi", '<%= GetUrlWebApi() %>');
        });

    </script>

</asp:Content>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <section class="form-simple center-horizontal-vertical">
        <div id="divLogin" class="card login animated" style="width: 600px">
            <div class="text-center pt-4 grey lighten-2">
                <h4 class="deep-grey-text"><strong>Inicio de Sesión</strong></h4>
            </div>
            <div class="card-body mx-4">
                <asp:Panel runat="server" DefaultButton="btnLogin">
                    <label for="txtUsuario">Usuario</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input id="txtUsuario" type="text" class="form-control" maxlength="8" autocomplete="username" autofocus required />
                    </div>
                    <label for="txtUsuario">Contraseña</label>
                    <div class="input-group mb-3">

                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input id="txtClave" type="password" class="form-control" autocomplete="current-password" autofocus required />
                    </div>
                </asp:Panel>
                <div class="text-center mb-4">
                    <asp:Button runat="server" ID="btnLogin" CssClass="btn btn-primary btn-block"
                        Text="Iniciar Sesión" OnClientClick="return false;" />
                </div>
            </div>
            <div class="alert alert-info text-center" role="alert" style="margin-top: 10px;">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Se recomienda el uso de este sistema en Navegador Google Chrome.
            </div>
        </div>
    </section>
    <script src="<%= ResolveClientUrl("~/Script/Login.js") + "?" + GetVersion()  %>"></script>
</asp:Content>
