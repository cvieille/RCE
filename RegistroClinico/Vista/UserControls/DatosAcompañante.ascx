﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DatosAcompañante.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.DatosAcompañante" %>

<div class="row">
    <div class="col-md-2">
        <label>Identificación</label>
        <select id="sltIdentificacionAcompañante" class="form-control identificacion">
        </select>
    </div>
    <div class="col-md-3">
        <label for="sltIdentificacionAcompañante"></label>
        <div class="input-group">
            <input id="txtnumeroDocAcompañante" type="text" class="form-control numero-documento" style="width: 100px;"
                data-required="false" maxlength="15" />
            <div class="input-group-prepend digito">
                <div class="input-group-text">
                    <strong>-</strong>
                </div>
            </div>
            <input id="txtDigAcompañante" type="text" class="form-control digito text-center" maxlength="1" style="width: 15px;"
                placeholder="DV" />
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-md-3">
        <label for="txtNombreAcompañante">Nombre</label>
        <input id="txtNombreAcompañante" type="text" class="form-control datos-persona" maxlength="50" data-required="false"
            disabled="disabled" placeholder="Escriba nombre del cuidador" />
    </div>
    <div class="col-md-3">
        <label for="txtApePatAcompañante">Apellido paterno</label>
        <input id="txtApePatAcompañante" type="text" class="form-control datos-persona" maxlength="50" data-required="false"
            disabled="disabled" placeholder="Escriba apellido paterno del cuidador" />
    </div>
    <div class="col-md-3">
        <label for="txtApeMatAcompañante">Apellido Materno</label>
        <input id="txtApeMatAcompañante" type="text" class="form-control datos-persona" maxlength="50" disabled="disabled"
            placeholder="Escriba apellido materno del cuidador" />
    </div>
    <div class="col-md-3">
        <label for="txtTelAcompañante">Teléfono (Opcional)</label>
        <input id="txtTelAcompañante" type="text" class="form-control datos-persona" maxlength="50" disabled="disabled"
            placeholder="Escriba teléfono del cuidador" />
    </div>
    <div class="col-md-3">
        <label for="lblSexoAcompañante">Sexo</label>
        <select id="sltSexoAcompañante" class="form-control datos-persona"></select>
    </div>
    <div class="col-md-3">
        <label for="lblTipoRepresentante">Tipo Representante</label>
        <select id="sltTipoRepresentante" class="form-control datos-persona"></select>
    </div>
</div>

<script src="<%= ResolveClientUrl("~/Script/UserControls/DatosAcompañante.js") + "?" + GetVersion() %>"></script>
