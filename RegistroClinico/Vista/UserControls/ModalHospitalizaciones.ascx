﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalHospitalizaciones.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalHospitalizaciones" %>

<div id="mdlVerHospitalizaciones" class="modal show fade" role="dialog" aria-labelledby="mdlVerHospitalizaciones" 
    aria-hidden="true" tabindex="1">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header-info">
                <div class="row w-100">
                    <div class="col-md-6 col-lg-9">
                        <h4>Historial de Hospitalizaciones</h4>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <asp:Button runat="server" ID="btnExportarHisHospitalizacionModal" OnClick="btnExportarHisHospitalizacionModal_Click" Style="display: none;" />
                        <button id="lnkExportarHisHospitalizacionModal" class="btn btn-info btn-exportar form-control">
                            <i class="fa fa-file-excel-o" style="font-size: 18px;"></i>Exportar a excel 
                        </button>
                    </div>
                </div>
            </div>    
            <div class="modal-body">
                <h6>Paciente: <b><span id="nomPacModalVerHospitalizaciones"></span></b></h6>
                <hr/>
                <br/>
                <table id="tblHistorialHospitalizacionesModal" class="table table-striped table-bordered table-hover  nowrap dataTable no-footer" style="width: 100%;">
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-print fade" id="mdlImprimirHisHos" tabindex="-1">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i>Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameImpresionHis" frameborder="0" onload="$('#modalCargando').hide();"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $('#lnkExportarHisHospitalizacionModal').click(function () {
        let tblObj = $('#tblHistorialHospitalizacionesModal').DataTable();
        __doPostBack("<%= btnExportarHisHospitalizacionModal.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
    });
</script>

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalHospitalizaciones.js") + "?" + GetVersion() %>"></script>