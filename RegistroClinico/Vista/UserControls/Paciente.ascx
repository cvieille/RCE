﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Paciente.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.Paciente" %>
<%@ Register Src="~/Vista/UserControls/DatosAcompañante.ascx" TagName="DatosAcompañante" TagPrefix="wuc" %>


<div id="divDatosPaciente">

    <div class="card text-white">
        <div class="card-header bg-dark">
            <div class="row">
                <div class="col-md-10">
                    <h5 class="mb-0"><strong><i class="fa fa-wheelchair fa-lg mr-2"></i></strong>Datos del paciente</h5>
                </div>
                <div class="col-md-2 text-right">
                    <a id="lnbEvento" class="btn btn-primary" onclick="LnbEvento_Click()">
                        <i class="fa fa-eye"></i>Cambiar Evento
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-md-2">
                    <label>Identificación</label>
                    <select id="sltIdentificacion" class="form-control">
                    </select>
                </div>
                <div class="col-md-2">
                    <label id="lblIdentificacion"></label>
                    <div class="input-group">
                        <input id="txtnumerotPac" type="text" class="form-control" style="width: 100px;" maxlength="8">
                        <div class="input-group-prepend digito">
                            <div class="input-group-text">
                                <strong>-</strong>
                            </div>
                        </div>
                        <input type="text" id="txtDigitoPac" class="form-control digito" maxlength="1" style="width: 15px" placeholder="DV">
                    </div>
                </div>
            </div>
            <div id="divInfoPaciente">
                <div class="row mt-2">
                    <div class="col-md-3">
                        <label class="form">Nombre/es</label>
                        <input id="txtnombrePac" type="text" class="form-control" maxlength="50" data-required="true" />
                    </div>
                    <div class="col-md-3">
                        <label class="form">Apellido paterno</label>
                        <input id="txtApePat" class="form-control" type="text" maxlength="50" data-required="true" />
                    </div>
                    <div class="col-md-3">
                        <label class="form">Apellido materno</label>
                        <input id="txtApeMat" type="text" class="form-control" maxlength="50" />
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-md-3">
                        <label>Sexo</label>
                        <select id="sltsexoPac" class="form-control" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Genero</label>
                        <select id="sltgeneroPac" class="form-control" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label>Nombre social (Paciente responde a)</label>
                        <input id="txtNombreSocial" class="form-control" type="text" maxlength="50" />
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-2">
                        <label>Fecha de nacimiento</label>
                        <input id="txtFecNacPac" class="form-control" type="date" data-required="true" onblur="TxtFecNacPac_CalculaEdad()"
                            onkeydown="event.keyCode == 13 && TxtFecNacPac_CalculaEdad()" max="<%= DateTime.Now.ToString("yyyy-MM-dd") %>" min="<%= ("1900-01-01") %>" />
                    </div>
                    <div class="col-md-2">
                        <label>Edad</label>
                        <input id="txtEdadPac" type="text" class="form-control" disabled="disabled" />
                    </div>
                    <div class="col-md-6">
                        <label class="form">Dirección</label>
                        <input id="txtDireccion" class="form-control" type="text" maxlength="100" />
                    </div>
                    <div class="col-md-2">
                        <label class="form">Número</label>
                        <input id="txtNumDireccionPaciente" class="form-control" type="text" maxlength="50" />
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-3">
                        <label class="form">Teléfono</label>
                        <input id="txtTelefono" type="text" class="form-control" placeholder="Ej: +56 9 11335869" maxlength="50" />
                    </div>
                    <div class="col-md-3">
                        <label class="form">Otro teléfono</label>
                        <input id="txtOtroTelefono" placeholder="Ej: +56 9 11335869" type="text" class="form-control" maxlength="50" />
                    </div>
                    <div class="col-md-3">
                        <label class="form">Correo electrónico</label>
                        <input id="txtCorreoElectronico" placeholder="Ej: correo@dominio.com" type="email" class="form-control" maxlength="50" />
                    </div>
                </div>
                <div id="divPacienteAcompañante" class="row mt-2">
                    <div class="col-md-2">
                        <div class="form-check pl-0">
                            <label class="mr-2">Acompañante</label>
                            <input id="chkAcompañante" type="checkbox" class="bootstrapSwitch" data-on-text="SI" data-off-text="NO"
                                data-on-color="primary" data-off-color="primary" data-size="normal" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="mdlRutMaterno" role="dialog" data-backdrop="static" data-keyboard="false"
        aria-labelledby="mdlRutMaterno" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document"
            style="width: 1000px; min-width: 1000px;">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Rut materno</p>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info text-center" role="alert" style="margin-top: 10px; fon">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>&nbsp;Se han
                    encontrado pacientes con el rut indicado <b>por favor verifique que el paciente no esté
                        duplicado</b>.
                    </div>
                    <table id="tblIngresoExistente" class="table table-bordered table-sm table-hover"
                        style="width: 100%;">
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="btnNuevoPacienteRN" class="btn btn-secondary">Es un paciente nuevo</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card" id="divAcompañante">
    <div class="card-header bg-dark text-white">
        <h5><strong><i class="fas fa-user-friends fa-lg mr-2"></i></strong>Acompañante</h5>
    </div>
    <div class="card-body">
        <div class="mt-3">
            <wuc:DatosAcompañante ID="wucDatosAcompañante" runat="server" />
        </div>
    </div>
</div>


<script src="<%= ResolveClientUrl("~/Script/UserControls/Paciente.js") + "?" + GetVersion() %>"></script>
