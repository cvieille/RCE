﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CargadoDeArchivos.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.CargadoDeArchivos" %>

<link href="<%= ResolveClientUrl("~/Style/Uploader/jquery.dm-uploader.css") %>" rel="stylesheet" />
<script  type = "text/javascript" >
    function DescargarArchivo(data) {
        __doPostBack('<%= descargarArchivo.UniqueID %>', data);
    }
</script>
<asp:UpdatePanel ID="upd" runat="server">
    <ContentTemplate>
        <asp:Button ID="descargarArchivo" runat="server" style="display:none;" OnClick="descargarArchivo_Click" />
    </ContentTemplate>
    <Triggers><asp:PostBackTrigger ControlID="descargarArchivo" /></Triggers>
</asp:UpdatePanel>

<div class="row">
    <div class="col-md-12">                      
        <main role="main" class="container-fluid dm">
            <div class="row">
                <div id="divSubirArchivos" class="col-md-6 col-sm-12">
                    <div id="drag-and-drop-zone" class="dm-uploader p-5">  
                        <h3 class="mb-5 mt-5 text-muted"> Arrastre sus Archivo/os Aquí</h3>
                        <div class="btn btn-primary-dark btn-block mb-5">
                            <span>Examinar en este equipo.</span>
                            <input type="file" title='Click para agregar Archivos' class="btn-block" />
                        </div>
                    </div>
                </div>
                <div id="divListaArchivos" class="col-md-6 col-sm-12">
                    <div class="card card-primary h-100">   
                        <div class="card-header btn-primary-dark">
                            Lista de Archivo/os: <strong id="stgNumeroArchivos">0 Archivos.</strong>
                        </div>
                        <ul class="list-unstyled p-2 d-flex flex-column" id="files">
                            <li class="text-muted text-center empty">No hay archivos Cargados.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </main>

        <script src="<%= ResolveClientUrl("~/Script/Uploader/jquery.dm-uploader.js") %>"></script>
        <script src="<%= ResolveClientUrl("~/Script/UserControls/CargadoDeArchivos.js") + "?" + GetVersion() %>"></script>
        <script type="text/html" id="files-template">
            <li class="media-1 newFile">
                <div class="mb-1">
                    <div class="row">
                        <div class="col-md-10">
                            <strong><a>%%filename%%</a></strong> - Estado: <span class="text-muted">A la Espera</span> - Tamaño 
                            <strong>%%size%%</strong>
                        </div>
                        <div class="col-md-2 text-right">
                            <button type="button" style="padding-bottom:5px !important;" class="close cancel">x</button>
                        </div>
                    </div>
            
                    <div class="progress mb-2">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
                            role="progressbar"
                            style="width: 0%" 
                            aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
            
                    <hr class="mt-1 mb-1" />

                </div>
            </li>
        </script>
    </div>
</div>
    