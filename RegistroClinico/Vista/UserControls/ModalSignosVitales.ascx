﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalSignosVitales.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalSignosVitales" %>
<%@ Register Src="~/Vista/UserControls/SignosVitales.ascx" TagName="SignosVitales" TagPrefix="wuc" %>

<div id="mdlSignosVitales" class="modal fade ml-2 mr-2" tabindex="-1">
    <div class="modal-dialog modal-fluid modal-dialog-centered" role="document">
        <div class="modal-content ml-4 mr-4">
            <div class="modal-header">
                <h5 class="modal-title">Nombre de paciente:
                    <label id="lblPacienteSignosVitales"></label>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">x</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="divSignosVitales"></div>
                <div class='text-right mt-2'>
                    <a id='aGuardarSignoVital' class='btn btn-info'><i class="fa fa-save"></i> Guardar</a>
                </div>
                <!--<wuc:SignosVitales ID="wucSignosVitales" runat="server"/>-->
            </div>
        </div>
    </div>
</div>
<%--<script src="<%= ResolveClientUrl("~/Script/UserControls/SignosVitales.js") + "?"  %>"></script>--%>