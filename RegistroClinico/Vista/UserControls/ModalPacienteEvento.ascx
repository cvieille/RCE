﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalPacienteEvento.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalPacienteEvento" %>

<div class="modal fade top" id="mdlEventoAsignar" role="dialog" aria-labelledby="mdlEventoAsignar" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg modal-frame modal-top" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <p class="heading lead">Asignar Evento</p>
                <button type="button" class="close white-text" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div id="divEvento">
                            <table id="tblEventos" class="table table-bordered table-sm table-hover" 
                                style="font-size: smaller">
                            </table>
                        </div>
                        <button id="btnEventoNuevoAbrir" class="btn btn-secondary container-fluid" onclick="LinkNuevoEvento()">
                            Nuevo Evento
                        </button>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalPacienteEvento.js") + "?" + GetVersion() %>"></script>
