﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;

namespace RegistroClinico.Vista.UserControls
{
    public partial class ModalHospitalizaciones : Handlers.UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        protected void btnExportarHisHospitalizacionModal_Click(object sender, EventArgs e)
        {
            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.MissingMemberHandling = MissingMemberHandling.Ignore;

                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);
                //Todos Los exportar dependen del número de columnas en bandeja y o tablas...
                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["ID Hospitalización"] = Convert.ToString(des[i][0]),
                        ["Fecha"] = Convert.ToString(des[i][1]),
                        ["Categorización"] = Convert.ToString(des[i][2]),
                        ["Cama"] = Convert.ToString(des[i][3]),
                        ["Estado"] = Convert.ToString(des[i][4])
                    };
                    dic.Add(d);
                }

                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Historial hospitalización", dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Historial hospitalización", workbook);
            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al exportar Excel de historial de hospitalización.", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }
        }
    }
}