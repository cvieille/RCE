﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalHojasEvolucion.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalHojasEvolucion" %>
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalHojasEvolucion.js") + "?" + GetVersion() %>"></script>

<div id="mdlVerHojasEvolucion" class="modal show fade" role="dialog" aria-labelledby="mdlImprimir" 
    aria-hidden="true" tabindex="1">
    <div class="modal-dialog modal-xlg modal-dialog-centered">
        <div class="modal-content">
            <div id="divCarouselHos" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner">
                    <div class="modal-header modal-header-info">
                        <p class="heading lead" id="txtTitle">
                            Hojas de Evolución
                        </p>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true" class="white-text">x</span>
                        </button>
                    </div>
                    <div class="carousel-item active">                        
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Nombre de paciente:</label>
                                    <input id="txtPacienteHE" class="form-control" disabled="disabled">
                                </div>
                                <div class="col-md-2">
                                    <label>Cama actual:</label>
                                    <input id="txtCamaActualHE" class="form-control" disabled="disabled">
                                </div>
                            </div>
                            <br />
                            <div class="text-right mb-2">
                                <a id='linkNuevaHojaEvolucion' class='btn btn-info load-click' onclick='linkCrearNuevaHojaEvolucion(this);'>
                                    <i class='fa fa-plus'></i> NUEVA HOJA DE EVOLUCIÓN
                                </a>
                            </div>
                            <table id="tblHojasEvolucion" class="table table-bordered table-sm table-hover" style="font-size: smaller">
                            </table>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="modal-body">
                            <table id="tblMovHE" class="table table-bordered table-sm table-hover" style="font-size: smaller"></table>
                        </div>
                        <div class="modal-footer">
                            <a id="btnVolverCarousel" class="btn btn-default" onclick="$('#txtTitle').html('Hoja de Evolución'); $('#divCarouselHos').carousel(0);"><i class="fa fa-arrow-left"></i>&nbsp;Volver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-print fade" id="mdlImprimirHE" role="dialog" aria-labelledby="mdlImprimir" 
    aria-hidden="true" tabindex="2"
    style="z-index: 1051 !important;">
    <div class="modal-dialog modal-fluid" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <p class="heading lead">
                    <strong><i class="fa fa-print"></i>Impresión</strong>
                </p>
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true" class="white-text">x</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-body-print">
                    <iframe id="frameHojaEvolucion" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="mdlMovimientoHE" class="modal fade" role="dialog" aria-labelledby="mdlMovimiento" 
    aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            
        </div>        
    </div>
</div>