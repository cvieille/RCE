﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AtencionClinica.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.AtencionClinica" %>

<div id="divGestionSolicitudes" class="card">
    <div class="card-header bg-dark">
        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Gestión de Solicitudes</strong></h5>
    </div>
    <div class="card-body">
                                
        <div class="row">
            <div class="col-md-3">
                <label>Estamento/Profesión</label>
                <select id="sltProfesionEvolucionAtencion" class="form-control">
                </select>
            </div>
            <div class="col-md-9">
                <label>Profesional</label>
                <input id="txtProfesionalEvolucionAtencion" type="text" class="form-control" disabled="disabled" />
            </div>
        </div>

        <div id="divIndicadoresIndicaciones" class="row mt-3 mb-3">
            <div class="col-md-12 text-center">
                <div class="btn-group flex-wrap" role="group" aria-label="Basic example">
                    <a id="aProcedimientos" class="btn btn-atencion-clinica default text-center p-4">
                        <i class="fas fa-heartbeat fa-4x"></i><br />
                        <span class="mt-3">Procedimientos <span id="spProcedimientos" class="badge badge-light">0/0</span></span>
                    </a>
                    <a id="aMedicamentoBox" class="btn btn-atencion-clinica default text-center p-4">
                        <i class="fas fa-prescription-bottle-alt fa-4x"></i><br />
                        <span class="mt-3">Medicamentos <span id="spInsumosMedicamentos" class="badge badge-light">0/0</span></span>
                    </a>
                    <a id="aLaboratorio" class="btn btn-atencion-clinica default text-center p-4">
                        <i class="fas fa-microscope fa-4x"></i><br />
                        <span class="mt-3">Laboratorio <span id="spLaboratorio" class="badge badge-light">0/0</span></span>
                    </a>
                    <a id="aImagenologia" class="btn btn-atencion-clinica default text-center p-4">
                        <i class="fas fa-chalkboard-teacher fa-4x"></i><br />
                        <span class="mt-3">Imagenologia <span id="spExamenesImagenologia" class="badge badge-light">0/0</span></span>
                    </a>
                    <a id="aInterconsultor" class="btn btn-atencion-clinica default text-center p-4">
                        <i class="fas fa-user-md fa-4x"></i><br />
                        <span class="mt-3">Interconsultor <span id="spInterconsultor" class="badge badge-light">0/0</span></span>
                    </a>
                </div>
            </div>
        </div>

        <div id="divSolicitudInterconsultor" class="card border-dark mb-3">
            <div class="card-header bg-light">
                <h3 class="mt-1 mb-1">Solicitudes de Interconsultor</h3>
            </div>
            <div class="card-body">
                <table id="tblInterconsultores" style="width:100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Especialidad</th>
                            <th>Fecha solicitud</th>
                            <th>Solicitante</th>
                            <th>Fecha cierre</th>
                            <th>Interconsultor</th>
                            <th class='text-center'>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>

        <div id="divProcedimientos" class="card border-dark mb-3">
            <div class="card-header bg-light">
                <h3 class="mt-1 mb-1">Procedimientos seleccionados</h3>
            </div>
            <div class="card-body">
                <table id="tblProcedimientos" style="width:100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cartera Servicio</th>
                            <th>Observaciones adicionales</th>
                            <th>Fecha solicitud</th>
                            <th>Fecha cierre</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>

        <div id="divMedicamentos" class="card border-dark mb-3">
            <div class="card-header bg-light">
                <h3 class="mt-1 mb-1">Medicamentos</h3>
            </div>
            <div class="card-body">
                <table id="tblMedicamentosBox" style="width:100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Medicamento</th>
                            <th>Descripción</th>
                            <th>Fecha solicitud</th>
                            <th>Fecha cierre</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
                            
        <div id="divExamenesLaboratorio" class="card border-dark mb-3">
            <div class="card-header bg-light">
                <h3 class="mt-1 mb-1">Solicitudes de Exámenes de Laboratorio</h3>
            </div>
            <div class="card-body">
                <table id="tblExamenesLaboratorio" style="width:100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cartera Servicio</th>
                            <th>Observaciones Adicionales</th>
                            <th>Fecha solicitud</th>
                            <th>Fecha cierre</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>

        <div id="divExamenesImagenologia"  class="card border-dark mb-3">
            <div class="card-header bg-light">
                <h3 class="mt-1 mb-1">Solicitudes de Exámenes de imageonología</h3>
            </div>
            <div class="card-body">
                <table id="tblExamenesImagenologia" style="width:100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cartera Servicio</th>
                            <th>Observaciones Adicionales</th>
                            <th>Fecha solicitud</th>
                            <th>Fecha cierre</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>

    </div>
    
    <div class="modal fade" id="mdlInterconsultista" role="dialog" aria-labelledby="mdlInterconsultista" aria-hidden="true" tabindex="0">
        <div class="modal-dialog mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h3><strong>Solicitudes de Interconsulta</strong></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-3">
                            <label>Especialidad:</label>
                        </div>
                        <div class="col-md-9">
                            <select id="sltTipoInterconsultor" data-required="true" class="form-control">
                            </select>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-3">
                            <label>Solicitud:</label>
                        </div>
                        <div class="col-md-9">
                            <textarea id="txtDetalleSolicitudInterconsultor" data-required="true" rows="5" placeholder="Detalle su solicitud" 
                                maxlength="500" class="form-control"></textarea>
                        </div>
                    </div>

                    <div id="divDetalleAtencionInterconsultor" class="row">
                        <div class="col-md-3">
                            <label>Atención:</label>
                        </div>
                        <div class="col-md-9">
                            <textarea id="txtDetalleAtencionInterconsultor" rows="5" maxlength="1500"  placeholder="Describa lo realizado" 
                                class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="text-right mt-2">
                        <a id="aGuardarInterconsultor" onclick="agregarSolicitudInterconsultor();"  class="btn btn-success">
                            <i class="fa fa-save"></i> Guardar Solicitud
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--Modal imageneologia-->
    <div class="modal fade" id="mdlImagenologia" role="dialog" aria-labelledby="mdlExamenes" aria-hidden="true" tabindex="0">
        <div class="modal-dialog modal-fluid  modal-dialog-scrollable" role="document">
            <div class="modal-content">
                 <div class="modal-header modal-header-info">
                    <h2><strong>Exámenes imagenologia</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body pr-5 pl-5">
                     <!--- navbar-->
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="imagenologia-tab" data-toggle="tab" href="#imagenologiaComunes" role="tab" aria-controls="imagenologia" aria-selected="true">Exámenes imagenología más comunes</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="imagenologiaOtros-tab" data-toggle="tab" href="#imagenologiaOtros" role="tab" aria-controls="imagenologiaOtros" aria-selected="false">Otros exámenes</a>
                      </li>
                    </ul>
                    <!-- fin nav bar-->
                    <div class="tab-content" id="nav-tabContent">
                        <!--Examenes comunes-->
                        <div id="imagenologiaComunes" class="tab-pane fade show active"  role="tabpanel" aria-labelledby="Examenes">
                            <div id="divModalExamenesImagenologia" class="w-75" style="margin:10px auto;">

                            </div>
                        </div>
                        <div id="imagenologiaOtros" class="tab-pane fade"  role="tabpanel" aria-labelledby="Examenes">
                             <div class="w-50" id="divExamLabImg" style="margin:0 auto;">
                                <div class="row">
                                    <div class="col-md-10">
                                        <label>Filtrar exámenes imagenologia<b class="color-error">(*)</b></label>
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <div class="typeahead__query">
                                                    <input id="thExamenesImagenologia" name="hockey_v1[query]" type="search" placeholder="Buscar más exámenes"
                                                        autocomplete="off"  data-required="true" onclick="this.select();">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label>&nbsp;</label><br />
                                        <a href="#" id="addElementTableExamImg" class="btn btn-success"> <i class="fa fa-plus"></i> </a>
                                    </div>
                                </div>
                            </div>
                       
                        </div>
                    </div>
                    <!--Tabla examenes imagenologia-->
                    <div class="w-75" style="margin:10px auto;">
                        <table id="tblExamImg" class="table table-bordered">
                            <thead class="text-center">
                                <tr>
                                    <th scope="col">Nombre examen</th>
                                    <th scope="col">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyExamImg">

                            </tbody>
                        </table>
                    </div>
                    <div class="text-right mt-2">
                            <a id="aAceptarExamenesImagenologiaModal" class="btn btn-success btn-lg">Aceptar</a>
                    </div>
                    <!--Fin tabla examenes imagenologia-->
                </div>
            </div>
        </div>
    </div>
    <!--fin modal imageologia-->
    <!--Modal examenes laboratorio-->
    <div class="modal fade" id="mdlExamenes" role="dialog" aria-labelledby="mdlExamenes" aria-hidden="true" tabindex="0">
        <div class="modal-dialog modal-fluid  modal-dialog-scrollable mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Exámenes</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body pr-5 pl-5">
                    <h2 class="text-center mt-2 mb-2">Lista de exámenes</h2>
                    <!--- navbar-->
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="examenes-tab" data-toggle="tab" href="#examenesComunes" role="tab" aria-controls="Examenes" aria-selected="true">Exámenes más comunes</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="otrosexamenes-tab" data-toggle="tab" href="#otrosExamenes" role="tab" aria-controls="OtrosExamenes" aria-selected="false">Otros exámenes</a>
                      </li>
                    </ul>
                    <!-- fin nav bar-->
                    
                    <div class="tab-content" id="nav-tabContent">
                        <!--Examenes comunes-->
                        <div id="examenesComunes" class="tab-pane fade show active"  role="tabpanel" aria-labelledby="Examenes">
                            <div id="accordion"> 
                            </div>
                            <div id="divListaExamenesLaboratorioModal" class="row"></div>
                        </div>
                        <!--Otros exámenes-->
                        <div id="otrosExamenes" class="tab-pane fade" role="tabpanel" aria-labelledby="OtrosExamenes">
                            <div class="w-50" id="divExamLab" style="margin:0 auto;">
                                <div class="row">
                                    <div class="col-md-10">
                                        <label>Filtrar exámenes <b class="color-error">(*)</b></label>
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <div class="typeahead__query">
                                                    <input id="thExamenesLaboratorio" name="hockey_v1[query]" type="search" placeholder="Buscar más exámenes"
                                                        autocomplete="off"  data-required="true" onclick="this.select();">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label>&nbsp;</label><br />
                                        <a href="#" id="addElementTableExam" class="btn btn-success"> <i class="fa fa-plus"></i> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Tabla examenes laboratorio-->
                    <div class="w-75" style="margin:10px auto;">
                        <table id="tblExamLab" class="table table-bordered">
                            <thead class="text-center">
                                <tr>
                                    <th scope="col">Nombre examen</th>
                                    <th scope="col">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyExamLab">

                            </tbody>
                        </table>
                    </div>
                    <!--Fin tabla examenes laboratorio-->
                    <div class="text-right mt-2">
                            <a id="aAceptarExamenesLaboratorioModal" class="btn btn-success btn-lg">Aceptar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fin modal examenes laboratorio -->
    <!--Modal procedimientos-->
    <div class="modal fade" id="mdlProcedimientos" role="dialog" aria-labelledby="mdlProcedimientos" aria-hidden="true" tabindex="0">
        <div class="modal-dialog modal-fluid modal-dialog-scrollable mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Procedimientos</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body pr-1 pl-3">
                    <h2 class="text-center mt-2 mb-2">Lista de Procedimientos</h2>
                    <!--- navbar-->
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="procedimientos-tab" data-toggle="tab" href="#ProcedimientosComunes" role="tab" aria-controls="Procedimientos" aria-selected="true">Procedimientos más comunes</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="otrosprocedimientos-tab" data-toggle="tab" href="#OtrosProcedimientos" role="tab" aria-controls="otrosProcedimientos" aria-selected="false">Otros procedimientos</a>
                      </li>
                    </ul>
                    <!-- fin nav bar-->
                    <div class="tab-content" id="nav-tabContent">
                            <!--procedimientos comunes-->
                            <div id="ProcedimientosComunes" class="tab-pane fade show active"  role="tabpanel" aria-labelledby="Procedimientos">
                                <div id="accordion2"> 
                                </div>
                                <div id="divListaProcedimientosModal" class="row m-3"></div>
                            </div>
                            <!--Otros procedimientos-->
                            <div id="OtrosProcedimientos" class="tab-pane fade" role="tabpanel" aria-labelledby="otrosProcedimientos">
                                <div class="w-50" id="divProd" style="margin:0 auto;">
                                <div class="row">
                                    <div class="col-md-10">
                                        <label>Filtrar procedimientos <b class="color-error">(*)</b></label>
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <div class="typeahead__query">
                                                    <input id="thProcedimientos" name="hockey_v1[query]" type="search" placeholder="Buscar más procedimientos"
                                                        autocomplete="off"  data-required="true" onclick="this.select();">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label>&nbsp;</label><br />
                                        <a href="#" id="addElementTableProced" class="btn btn-success"> <i class="fa fa-plus"></i> </a>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!--Tabla examenes procedimientos-->
                            <div class="w-75" style="margin:10px auto;">
                                <table id="tblPod" class="table table-bordered">
                                    <thead class="text-center">
                                        <tr>
                                            <th scope="col">Nombre examen</th>
                                            <th scope="col">Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyProd">

                                    </tbody>
                                </table>
                            </div>
                            <!--Fin tabla examenes laboratorio-->
                            <div class="text-right mt-2">
                                <a id="aAceptarProcedimientosSeleccionados" class="btn btn-success btn-lg">Aceptar</a>
                            </div>
                     </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlDatosAdicionales" class="modal fade" role="dialog" aria-labelledby="mdlDatosAdicionales" aria-hidden="true" tabindex="1">
        <div class="modal-dialog modal-lg modal-dialog-scrollable mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h4><strong>Seleccione</strong></h4>
                    <button type="button" id="btnCerrarMdlDatosAdicionales" onclick="cerrarModalSobrePuesto(this)" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body p-4"></div>
                <div class="modal-footer p-4 text-right"></div>
            </div>
        </div>
    </div>
    
    <div id="mdlCerrarCarteraServicio" class="modal fade" aria-labelledby="mdlCerrarCarteraServicio" role="dialog" aria-hidden="true" tabindex="0">
        <div class="modal-dialog modal-lg modal-dialog-centered mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-warning">
                    <h4>Cierre de procedimiento</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div id="divDetalleCarteraArancel"></div>
                    <div id="divObservacionesCierre" class="mt-2">
                        <label>Observaciones</label>
                        <textarea id="txtObservacionesCarteraArancel" class="form-control" rows="6" placeholder="Escriba observaciones acerca del caso realizado"
                            maxlength="500">
                        </textarea>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <a id="aCerrarCarteraArancel" class="btn btn-info">
                        <i class="fas fa-check"></i> Cerrar Procedimiento
                    </a>
                    <a class="btn btn-warning" data-dismiss="modal" aria-label="Close">Cancelar</a>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlMedicamentos" class="modal show" role="dialog" aria-labelledby="mdlMedicamentos" aria-hidden="true" tabindex="0">
        <div class="modal-dialog modal-fluid mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Insumos y/o medicamentos</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body pr-5 pl-5">
                    <div id="divInsumosMedicamentos">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Medicamentos <b class="color-error">(*)</b></label>
                                <div class="typeahead__container">
                                    <div class="typeahead__field">
                                        <div class="typeahead__query">
                                            <input id="thMedicamentos" name="hockey_v1[query]" type="search" placeholder="Agregar/Buscar Medicamentos"
                                                autocomplete="off" onclick="this.select();" data-required="true">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-4">
                                <label>Dosís</label>
                                <input id="txtDosisMedicamento" class="form-control" placeholder="Especifique dosís"
                                    type="text" data-required="true" />
                            </div>
                            <div class="col-sm-4">
                                <label>Frecuencia</label>
                                <input id="txtFrecuenciaMedicamento" class="form-control" placeholder="Especifique dosís"
                                    type="text" data-required="true" />
                            </div>
                            <div class="col-sm-4">
                                <label>Via de Administración</label>
                                <select id="sltViaAdministracion" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>
                        <div class="text-right mt-2">
                            <a id="aGuardarEditarMedicamento" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Guardar Edición</a>
                            <a id="aAgregarMedicamento" class="btn btn-success btn-lg" data-accion="Agregar">Agregar <i class="fa fa-plus"></i></a>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-12">
                                <table id="tblMedicamentos" class='table table-hover' style='width: 100%;'>
                                    <thead class="thead-dark">
                                        <tr class="text-center">
                                            <th>Nombre y Descripción Medicamento</th>
                                            <th>Dosis</th>
                                            <th>Frecuencia</th>
                                            <th>Vía de administración</th>
                                            <th>Quitar</th>
                                        </tr>
                                    </thead>
                                    <tbody style="background-color: white !important;"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <a id="aAceptarMedicamento" class="btn btn-success btn-lg" onclick="$('#mdlMedicamentos').modal('hide');">Aceptar</a>
                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/AtencionClinica.js") + "?" + GetVersion() %>"></script>
</div>
