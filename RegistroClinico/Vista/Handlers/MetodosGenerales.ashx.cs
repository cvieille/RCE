﻿using System;
using System.Security.Cryptography;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using System.Web;
using System.IO;
using RegistroClinico.Negocio;

namespace RegistroClinico.Vista.Handlers
{

    /// <summary>
    /// Descripción breve de MetodosGenerales
    /// </summary>
    public class MetodosGenerales : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        #region IHttpHandler

        public void ProcessRequest(HttpContext context)
        {

            switch (context.Request["method"])
            {
                case "SetSession":
                    SetSession();
                    break;
                case "GuardarArchivo":
                    GuardarArchivo(context.Request["id"], context.Request["modulo"]);
                    break;
                case "CargarArchivos":
                    CargarArchivos(context.Request["id"], context.Request["modulo"]);
                    break;
                case "EliminarArchivos":
                    EliminarArchivos();
                    break;
                case "GuardarLog":
                    GuardarLog();
                    break;
                case "GetFechaActual":
                    GetFechaActual();
                    break;
                case "AddHorasFechaActual":
                    AddHorasFechaActual();
                    break;
                case "AddDiasFechaActual":
                    AddDiasFechaActual();
                    break;
                case "Encriptar":
                    aesEncrypt(context.Request["texto"].ToString());
                    break;
                case "Desencriptar":
                    aesDecrypt(context.Request["texto"].ToString());
                    break;
                case "GetDiferenciaEnDias":
                    GetDiferenciaEnDias();
                    break;
                case "ObtenerApiPdf":
                    ObtenerApiPdf(context.Request["formulario"].ToString(), int.Parse(context.Request["id"].ToString()));
                    break;
                case "getManuales":
                    getManuales();
                    break;
                case "getManual":
                    getManual(context.Request["nombre"].ToString());
                    break;
                case "GetUrlWebApi":
                    ObtenerUrlWebApi();
                    break;
                default:
                    throw new ArgumentException("No se a encontrado el método especificado.");
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region Session

        private void SetSession()
        {

            HttpContext contexto = HttpContext.Current;

            try
            {

                Dictionary<string, object> json = null;
                string s = "";

                using (var reader = new StreamReader(contexto.Request.InputStream))
                {
                    s = reader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<Dictionary<string, object>>(s);
                }

                contexto.Session.Timeout = 60;
                contexto.Session["TOKEN"] = json["TOKEN"].ToString();
                contexto.Session["CODIGO_PERFIL"] = json["CODIGO_PERFIL"].ToString();
                contexto.Session["PERFIL_USUARIO"] = json["PERFIL_USUARIO"].ToString();
                contexto.Session["LOGIN_USUARIO"] = json["LOGIN_USUARIO"].ToString();
                contexto.Session["NOMBRE_USUARIO"] = json["NOMBRE_USUARIO"].ToString();

                contexto.Response.Write("{\"status\":\"ok\"}");

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al Establecer Session.", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }

            contexto.Response.End();

        }

        #endregion

        #region Archivos

        private void GuardarArchivo(string id, string modulo)
        {
            HttpContext contexto = HttpContext.Current;
            try
            {

                HttpFileCollection ColeccionArchivos = contexto.Request.Files;
                string nombreArchivo = "";

                for (int i = 0; i < ColeccionArchivos.Count; i++)
                {

                    nombreArchivo = ColeccionArchivos[i].FileName;
                    string DatosArchivo = System.IO.Path.GetFileName(ColeccionArchivos[i].FileName);
                    string CarpetaParaGuardar = Funciones.GetPropiedad(ref contexto, "urlDocumentos") + "\\Modulo" + modulo + "\\" + id + "\\";

                    Directory.CreateDirectory(CarpetaParaGuardar);
                    ColeccionArchivos[i].SaveAs(CarpetaParaGuardar + DatosArchivo);

                    contexto.Response.ContentType = "application/json";
                    contexto.Response.Write("{\"status\":\"ok\",\"path\":\"" + nombreArchivo + "\"}");

                }
            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al subir archivo", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }

            contexto.Response.End();

        }

        private void EliminarArchivos()
        {

            HttpContext contexto = HttpContext.Current;

            try
            {

                List<Dictionary<string, string>> listArchivos = null;

                using (var reader = new StreamReader(contexto.Request.InputStream))
                {
                    listArchivos = JsonConvert.DeserializeObject<Dictionary<string, List<Dictionary<string, string>>>>(reader.ReadToEnd())["array"];
                }

                if (listArchivos.Count > 0)
                {
                    string carpetaRepositoria = Funciones.GetPropiedad(ref contexto, "urlDocumentos") + "Modulo" + listArchivos[0]["modulo"] + "\\" + listArchivos[0]["id"] + "\\";
                    if (Directory.Exists(carpetaRepositoria))
                    {
                        if (Directory.GetFiles(carpetaRepositoria).Length > 0)
                        {
                            foreach (Dictionary<string, string> file in listArchivos)
                            {
                                if (File.Exists(carpetaRepositoria + file["name"]))
                                    File.Delete(carpetaRepositoria + file["name"]);

                                if (Directory.GetFiles(carpetaRepositoria).Length == 0)
                                    Directory.Delete(carpetaRepositoria);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al eliminar archivos", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }

            contexto.Response.End();

        }


        private void CargarArchivos(string id, string modulo)
        {

            HttpContext contexto = HttpContext.Current;

            try
            {

                // \\10.6.180.236\Documentos\RCE\Documentos\ModuloHOS\2
                string carpetaRepositoria = Funciones.GetPropiedad(ref contexto, "urlDocumentos") + "\\Modulo" + modulo + "\\" + id + "\\";

                contexto.Response.ContentType = "application/json";

                if (Directory.Exists(carpetaRepositoria))
                {
                    string[] archivos = Directory.GetFiles(carpetaRepositoria);
                    if (archivos.Count() > 0)
                    {

                        List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

                        foreach (string path in archivos)
                        {
                            FileInfo archivo = new FileInfo(path);
                            Dictionary<string, object> jsonArchivo = new Dictionary<string, object>();
                            jsonArchivo.Add("id", id);
                            jsonArchivo.Add("modulo", modulo);
                            jsonArchivo.Add("name", archivo.Name);
                            jsonArchivo.Add("size", archivo.Length);
                            list.Add(jsonArchivo);
                        }

                        contexto.Response.Write(JsonConvert.SerializeObject(list));

                    }
                    else
                        contexto.Response.Write("[]");
                }
                else
                    contexto.Response.Write("[]");

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al cargar archivos", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }

            contexto.Response.End();

        }

        private void getManuales()
        {

            HttpContext contexto = HttpContext.Current;
            Funciones.Credentials();
            string dir = @"\\10.6.180.236\Documentos\RCE\Documentos\Manuales\";

            List<string[]> allFolder = new List<string[]>();
            DirSearch(dir, ref allFolder);

            string s = JsonConvert.SerializeObject(allFolder);
            
            contexto.Response.Write(s);
        }

        private void getManual(string nombre)
        {

            HttpContext contexto = HttpContext.Current;
            Funciones.Credentials();
            string dir = @"\\10.6.180.236\Documentos\RCE\Documentos\Manuales\";
            string pathFile = dir + nombre;
            byte[] pdf = System.IO.File.ReadAllBytes(pathFile);

            contexto.Response.ContentType = "application/pdf";
            contexto.Response.BufferOutput = true;
            contexto.Response.AddHeader("Content-Disposition", "inline;filename=" + nombre);
            contexto.Response.BinaryWrite(pdf);
            contexto.Response.Flush();

        }

        private void DirSearch(string dir, ref List<string[]> list)
        {
            try
            {
                DirectoryInfo directory = new DirectoryInfo(dir);
                FileInfo[] files = directory.GetFiles();
                IEnumerable<FileInfo> filtered = files.Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden));
                foreach (FileInfo file in filtered)
                {
                    list.Add(new string[] { file.Name });
                    //list.Add(new string[] { f });
                }
            }
            catch (System.Exception ex)
            {
                throw;
                //debe ser de consola en PROYECTO
                //Console.WriteLine(ex.Message);
            }
        }

        #endregion

        #region JSON

        private void GuardarLog()
        {

            HttpContext contexto = HttpContext.Current;

            try
            {

                Dictionary<string, object> json = null;
                string s = "";
                using (var reader = new StreamReader(contexto.Request.InputStream))
                {
                    s = reader.ReadToEnd();
                    json = JsonConvert.DeserializeObject<Dictionary<string, object>>(s);
                }

                NegLog.GuardarLog(json["title"].ToString(), json["message"].ToString());

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al escribir log desde el cliente", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }

            contexto.Response.End();

        }

        #endregion

        #region Encriptacion

        private string encryptString(string txt, byte[] key, byte[] iv)
        {
            Aes aes = Aes.Create();
            aes.Mode = CipherMode.CBC;

            aes.Key = key;
            aes.IV = iv;

            MemoryStream ms = new MemoryStream();

            ICryptoTransform aesEncryptor = aes.CreateEncryptor();

            CryptoStream cryptoStream = new CryptoStream(ms, aesEncryptor, CryptoStreamMode.Write);

            byte[] plainBytes = System.Text.Encoding.ASCII.GetBytes(txt);

            cryptoStream.Write(plainBytes, 0, plainBytes.Length);

            cryptoStream.FlushFinalBlock();

            byte[] cypherBytes = ms.ToArray();

            ms.Close();

            cryptoStream.Close();

            string cypherText = Convert.ToBase64String(cypherBytes, 0, cypherBytes.Length);

            return cypherText;

        }

        private string decryptString(string cipherText, byte[] key, byte[] iv)
        {

            Aes aes = Aes.Create();
            aes.Mode = CipherMode.CBC;

            aes.Key = key;
            aes.IV = iv;

            MemoryStream ms = new MemoryStream();

            ICryptoTransform aesDecryptor = aes.CreateDecryptor();

            CryptoStream cryptoStream = new CryptoStream(ms, aesDecryptor, CryptoStreamMode.Write);

            string plainText = String.Empty;

            try
            {

                byte[] cipherBytes = Convert.FromBase64String(cipherText);

                cryptoStream.Write(cipherBytes, 0, cipherBytes.Length);

                cryptoStream.FlushFinalBlock();

                byte[] plainBytes = ms.ToArray();

                plainText = System.Text.Encoding.ASCII.GetString(plainBytes, 0, plainBytes.Length);

            }
            finally
            {
                ms.Close();
                cryptoStream.Close();
            }

            return plainText;

        }

        private void aesEncrypt(string txt)
        {
            try
            {
                HttpContext contexto = HttpContext.Current;

                string pass = "$2b$10$054qoSwWVa9zUAjfVBcViOzEdijcHNkxq2Q.hL8.p6Zrcd8Oi8LQ6";

                SHA256 sha = SHA256Managed.Create();
                byte[] key = sha.ComputeHash(System.Text.Encoding.ASCII.GetBytes(pass));
                byte[] iv = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                string txtEncriptado = encryptString(txt, key, iv);

                contexto.Response.Write(txtEncriptado);
                contexto.Response.End();
            }
            catch (Exception ex)
            {
                string fail = ex.Message;
            }
        }

        private string aesDecrypt(string enc)
        {

            string pass = "$2b$10$054qoSwWVa9zUAjfVBcViOzEdijcHNkxq2Q.hL8.p6Zrcd8Oi8LQ6";

            enc = enc.Replace("", "+");

            SHA256 sha = SHA256Managed.Create();
            byte[] key = sha.ComputeHash(System.Text.Encoding.ASCII.GetBytes(pass));
            byte[] iv = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            string txtDesencriptado = decryptString(enc, key, iv);

            return txtDesencriptado;

        }

        #endregion

        #region Fechas

        private void GetDiferenciaEnDias()
        {

            HttpContext contexto = HttpContext.Current;
            Dictionary<string, object> json = new Dictionary<string, object>();

            try
            {

                DateTime fechaInicio = DateTime.Parse(contexto.Request["fechaInicio"].ToString());
                DateTime fechaFinal = DateTime.Parse(contexto.Request["fechaFinal"].ToString());

                // DIFERENCIA DE FECHA EN DIAS
                if (fechaInicio <= fechaFinal)
                    json.Add("diferenciaFechas", ((TimeSpan)(fechaFinal - fechaInicio)).Days);
                else
                    json.Add("diferenciaFechas", "Fechas Incorrectas");

                contexto.Response.Write(JsonConvert.SerializeObject(json));
                contexto.Response.End();

            }
            catch (Exception ex)
            {
                if (!json.ContainsKey("diferenciaFechas"))
                {
                    json.Add("diferenciaFechas", "Formato Fecha Incorrecto");
                    contexto.Response.Write(JsonConvert.SerializeObject(json));
                    contexto.Response.End();
                }
            }

        }

        private void GetFechaActual()
        {

            HttpContext contexto = HttpContext.Current;

            try
            {

                Dictionary<string, object> json = new Dictionary<string, object>();
                string fecha = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                json.Add("fecha", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                contexto.Response.Write(JsonConvert.SerializeObject(json));

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al obtener fecha actual", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }

            contexto.Response.End();

        }
        private void AddDiasFechaActual() 
        {

            HttpContext contexto = HttpContext.Current;

            try
            {

                DateTime fechaSeleccionada = DateTime.Parse(contexto.Request["fecha"].ToString());
                DateTime fecha = fechaSeleccionada.AddDays(int.Parse(contexto.Request["cant"].ToString()));
                Dictionary<string, object> json = new Dictionary<string, object>();
                json.Add("fecha", fecha.ToString("yyyy-MM-dd"));

                contexto.Response.Write(JsonConvert.SerializeObject(json));

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al agregar días a fecha actual", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }

            contexto.Response.End();
        }
        private void AddHorasFechaActual()
        {

            HttpContext contexto = HttpContext.Current;

            try
            {

                Dictionary<string, object> json = new Dictionary<string, object>();
                string fecha = DateTime.Now.AddHours(int.Parse(contexto.Request["cantHoras"])).ToString("yyyy-MM-dd HH:mm:ss");
                json.Add("fecha", fecha);

                contexto.Response.Write(JsonConvert.SerializeObject(json));

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al agregar horas a fecha actual", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }

            contexto.Response.End();

        }



        #endregion

        #region API PDF

        public void ObtenerUrlWebApi() {

            try
            {
                HttpContext contexto = HttpContext.Current;
                contexto.Response.Write(Funciones.GetUrlWebApi());
                contexto.Response.End();
            }
            catch (Exception ex) 
            {
                string fail = ex.Message + ex.StackTrace;
            }

        }

        public void ObtenerApiPdf(string formulario, int id)
        {

            try
            {

                HttpContext contexto = HttpContext.Current;
                string dominio = Funciones.GetPropiedad(ref contexto, "urlApiPdf").ToString();
                string url = dominio + "api/" + formulario + "/" + id + "/pdf";
                
                contexto.Response.ContentType = "application/pdf";
                contexto.Response.BufferOutput = true;
                contexto.Response.AddHeader("Content-Disposition", "inline;filename=hola.pdf");
                contexto.Response.BinaryWrite(ObtenerFileWebApi(url));
                contexto.Response.Flush();

            }
            catch (Exception ex)
            {
                string fail = ex.Message + ex.StackTrace;
            }

        }

        private byte[] ObtenerFileWebApi(string sUrl)
        {

            HttpContext contexto = HttpContext.Current;

            try
            {
                
                string token = "Bearer " + GetTokenApiPdf();
                System.Net.WebClient WebClient = new System.Net.WebClient();
                WebClient.Headers.Add("Authorization", token);
                WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

                return WebClient.DownloadData(sUrl);

            }
            catch (Exception ex)
            {
                string fail = ex.Message + ex.StackTrace;
            }

            return null;
        }

        private string GetTokenApiPdf()
        {

            HttpContext contexto = HttpContext.Current;

            try
            {
                if (Funciones.GetPropiedad(ref contexto, "tokenApiPdf").ToString() == "")
                {

                    string dominio = Funciones.GetPropiedad(ref contexto, "urlApiPdf").ToString();
                    string url = dominio + "oauth/token";

                    System.Net.WebClient webClient = new System.Net.WebClient();
                    System.Collections.Specialized.NameValueCollection parametros = new System.Collections.Specialized.NameValueCollection();
                    parametros.Add("grant_type", "client_credentials");
                    parametros.Add("client_id", "1");
                    parametros.Add("client_secret", "Xp2zY4rwPoBZQ3SyBADFD2QO1YmNJaWtTICPff1M");

                    byte[] responsebytes = webClient.UploadValues(url, "POST", parametros);
                    string responsebody = (new System.Text.UTF8Encoding()).GetString(responsebytes);

                    Dictionary<string, object> json = JsonConvert.DeserializeObject<Dictionary<string, object>>(responsebody);
                    Funciones.SetPropiedad(ref contexto, "tokenApiPdf", json["access_token"].ToString());

                }

                return Funciones.GetPropiedad(ref contexto, "tokenApiPdf").ToString();

            }
            catch (System.Net.WebException ex)
            {
                string fail = ex.Message + ex.StackTrace;
                System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)ex.Response;
                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    Funciones.SetPropiedad(ref contexto, "tokenApiPdf", "");
                    GetTokenApiPdf();
                }
            }

            return null;
        }

        #endregion

    }

}