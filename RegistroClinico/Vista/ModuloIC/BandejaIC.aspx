﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="BandejaIC.aspx.cs" Inherits="RegistroClinico.Vista.BandejaIC" %>
<%@ Register Src="~/Vista/UserControls/CargadoDeArchivos.ascx" TagName="CargadorArchivos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalHojasEvolucion.ascx" TagName="HojaEvolucion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    
    <link rel="stylesheet" href="../../Style/bootstrap-switch.css" />
    <link rel="stylesheet" href="../../Style/select2/select2.css">
    <link rel="stylesheet" href="../../Style/select2/select2-bootstrap4.css">
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/select2.full.js") %>"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#lnbExportar').click(function () {
                var tblObj = $('#tblIC').DataTable();
                __doPostBack("<%= lnbExportarH.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });
            $('#lnkExportarMov').click(function () {
                var tblObj = $('#tblMovimientos').DataTable();
                __doPostBack("<%= lnbExportarMovH.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />

    <input type="hidden" id="idIC" value="0" />
    <input type="hidden" id="iID" value="0" />


    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Interconsulta</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Bandeja Interconsulta</h3>
                </div>
                <div class="card-body">
                    <div class="post">
                        <button data-toggle="collapse" class="btn btn-outline-primary container-fluid"
                            data-target="#divForm" onclick="return false;">
                            FILTRAR BANDEJA</button>
                        <div id="divForm" class="collapse" style="margin: 20px;">
                            <h5>Paciente</h5>
                            <div class="row">
                                <div class="col-md-2 offset-1">
                                    <label>Rut</label>
                                    <input id="txtFiltroRut" type="text" maxlength="15" class="form-control" />
                                </div>
                                <div class="col-md-2">
                                    <label>Nombre</label>
                                    <input id="txtFiltroNombre" type="text" maxlength="50" class="form-control" />
                                </div>
                                <div class="col-md-3">
                                    <label>Apellido paterno</label>
                                    <input id="txtFiltroApe" type="text" maxlength="50" class="form-control" />
                                </div>
                                <div class="col-md-3">
                                    <label>Apellido materno</label>
                                    <input id="txtFiltroSApe" type="text" maxlength="50" class="form-control" />
                                </div>
                            </div>
                            <h5>Datos clínicos</h5>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Especialidad origen</label>
                                    <select id="ddlEspecialidadOrigen" class="form-control select2" style="width: 100%;">
                                    </select>
                                </div>
                                <div class="col-md-3" id="divEspecialidadDestino">
                                    <label>Especialidad destino</label>
                                    <select id="ddlEspecialidadDestino" class="form-control select2" style="width: 100%;">
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>¿Interconsulta GES?</label>
                                    <br />
                                    <input type="checkbox" id="switchGES" data-label-width="1" data-on-text="SI" data-off-text="NO" data-on-color="info" data-off-color="warning" />
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-3" id="divAuge" style="display:none;">
                                    <label>Grupo AUGE</label>
                                    <select id="ddlGrupo" class="form-control">
                                    </select>
                                </div>
                                <div class="col-md-3" id="divSubAuge" style="display:none;">
                                    <label>Subgrupo o subproblema de salud AUGE</label>
                                    <select id="ddlSubGrupo" class="form-control">
                                        <option value="0">-Seleccione-</option>
                                    </select>
                                </div>
                                <div id="divProfesional" class="col-md-3" style="display: none;">
                                    <label>Profesional médico</label>
                                    <select id="ddlMedicoIC" class="form-control">
                                    </select>
                                </div>
                               
                               
                            </div>

                            <div class="text-right">
                                <button id="btnFiltro" class="btn btn-success"><i class="fa fa-search"></i> Buscar</button>
                                <button id="btnLimpiarFiltro" class="btn btn-secondary text-white"><i class="fa fa-eraser"></i>Limpiar filtros</button>
                                
                            </div>
                        </div>
                    </div>
                    <div class="post">
                        <div class="text-right">
                            <asp:Button runat="server" ID="lnbExportarH" OnClick="lnbExportarH_Click" Style="display: none;" />
                            <a id="lnbExportar" class="btn btn-info waves-effect waves-light" href="#\">
                                <i class="fa fa-file-excel"></i> Exportar XLS
                            </a>
                            <a id="lnbNuevoIPD" class="btn btn-info" href="NuevoIC.aspx">
                                <i class="fa fa-plus"></i>Nueva interconsulta
                            </a>
                        </div>
                    </div>
                    <div class="post">
                        <div class="form-row">
                            <div class="col-md-12">
                                <table id="tblIC" class="table table-bordered table-sm table-hover" style="width: 100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    

    <div class="modal fade" id="mdlMovimiento" role="dialog" aria-labelledby="mdlMovimiento" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Movimientos</p>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <asp:Button runat="server" ID="lnbExportarMovH" OnClick="lnbExportarMovH_Click" style="display:none;"/>
                        <a id="lnkExportarMov" class="btn btn-info btn-sm ml-auto" href="#\">
                            Exportar a Excel <i class="fa fa-file-excel-o" style="font-size:18px;"></i>
                        </a>
                        <%--<asp:LinkButton runat="server" ID="lnkExportarMov" OnClick="lnkExportarMov_Click" CssClass="btn btn-info btn-sm ml-auto">
                            Exportar a Excel <i class="fa fa-file-excel-o" style="font-size:18px;"></i>
                        </asp:LinkButton>--%>
                    </div>
                    <table id="tblMovimientos" class="table table-bordered table-md table-hover" style="font-size: smaller"></table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlEspecialidad" role="dialog" aria-labelledby="mdlEspecialidad" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Cambiar Especialidad</p>
                </div>
                <div class="modal-body" >
                    <div class="row" id="divEspecialidades">
                        <div class="col-sm-6">
                            <div class="md-form">
                                <label class="active">Destino Actual Especialidad</label>
                                <div style="height: 10px;">&nbsp;</div>
                                <select id="ddlEspecialidad1" class="disabled" data-style="btn btn-blue-grey waves-effect" data-live-search="true" 
                                    data-size="5" title="Seleccione">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="md-form">
                                <label class="active">Destino Nueva Especialidad</label>
                                <div style="height: 10px;">&nbsp;</div>
                                <select id="ddlEspecialidad2" class="selectpicker" data-required="true" data-style="btn-primary-dark waves-effect" data-live-search="true" 
                                    data-size="5" title="Seleccione">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnAceptarEsp" class="btn btn-default" onclick="return false;">Aceptar</button>
                    <button class="btn btn-danger" onclick="$('#mdlEspecialidad').modal('hide'); return false;">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-print fade" id="mdlImprimir" role="dialog" aria-labelledby="mdlImprimir" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i> Impresión</strong> 
                    </p>
                    <button type="button" class="close" data-dismiss="modal" 
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameIC" frameborder="0" onload="$('#modalCargando').hide();" >
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlArchivosAdjuntos" class="modal fade" role="dialog" aria-labelledby="mdlArchivosAdjuntos"
        aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong>Archivos Adjuntos</strong> 
                    </p>
                    <button type="button" class="close" data-dismiss="modal" 
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <wuc:CargadorArchivos ID="wucCargadorArchivos" runat="server" />
                </div>
            </div>
        </div>
    </div>



    <%--<div class="modal fade" id="mdlRechazar" role="dialog" aria-labelledby="mdlRechazar" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong>Anular IC</strong> 
                    </p>
                    <button type="button" class="close" data-dismiss="modal" 
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="alert alert-warning" role="alert" style="color: #856404 !important; 
                            background-color: #fff3cd !important; border-color: #ffeeba !important; width:100%;">                         
                            <p class="mb-0">Una vez anulada esta interconsulta no la podrá volver a ver</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button id="btnAnular" class="btn btn-danger" onclick="return false;">Anular</button>
                    <button class="btn btn-default" onclick="$('#mdlRechazar').modal('hide'); return false;">Cancelar</button>
                </div>
            </div>
        </div>
    </div>--%>
    
    <script src="<%= ResolveClientUrl("~/Script/ModuloIC/BandejaIC.js") + "?" + GetVersion() %>"></script>
    <wuc:HojaEvolucion ID="mdlHojaEvolucion" runat="server" />

</asp:Content>
