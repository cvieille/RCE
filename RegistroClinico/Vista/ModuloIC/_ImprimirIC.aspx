﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/Impresion.Master" AutoEventWireup="true" CodeBehind="_ImprimirIC.aspx.cs" Inherits="RegistroClinico.Vista.ModuloIC._ImprimirIC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/SiteGeneral.css" rel="stylesheet" />
    <style>
        .md-form {
            margin-top: 20px !important;
            margin-bottom: 10px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="card">
        <div class="card-header white">
            <div class="row">
                <div class="col-md-4 text-left">
                    <img src="<%= ResolveClientUrl("~/Style/IMG/MINSAL.jpg") %>" style="width:100px;"/>
                </div>
                <div class="col-md-4 text-center">
                    <img src="<%= ResolveClientUrl("~/Style/IMG/ACRLOGO.jpg") %>" style="width:100px;"/>
                </div>
                <div class="col-md-4 text-right">
                    <img src="<%= ResolveClientUrl("~/Style/IMG/HOSPLOGO.jpg") %>" style="width:100px;"/>
                </div>
            </div>
        </div>
        <div class="card-body" style="padding:0px;">
            
            <div class="row" style="margin: 5px 0px 5px 0px;">
                <div class="col-md-2">
                    <br />
                </div>
                <div class="col-md-8">
                    <h4 class="text-center">
                        <strong>SOLICITUD DE INTERCONSULTA O DERIVACIÓN</strong>
                    </h4>
                </div>
                <div class="col-md-2 text-right">
                    <asp:Label ID="lblFechaCreacion" runat="server" Font-Bold="true" Text="19/09/2018">
                    </asp:Label>
                </div>
            </div>

            <div class="card">
                <div class="card-header" style="padding: 8px;">
                    <div class="row">
                        <div class="col-md-3">
                            <h5><strong>Enviado de</strong></h5>
                        </div>
                        <div class="col-md-9">
                            <h5 class="text-right">
                                <asp:Label ID="lblServicioSaludEstablecimientoEnviado" runat="server" 
                                    Text="Servicio de Salud Magallanes">
                                </asp:Label>
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="padding-bottom: 0px; padding-top:0px;">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form">
                                <asp:TextBox ID="txtUbicacionEnviado" runat="server" CssClass="form-control"
                                    Text="CAE ADULTO">
                                </asp:TextBox>
                                <label for="Content_txtUbicacionEnviado" class="active">
                                    <strong>Servicio</strong>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form">
                                <asp:TextBox ID="txtEspecialidadEnviado" runat="server" CssClass="form-control"
                                    Text="CIRUGÍA GENERAL">
                                </asp:TextBox>
                                <label for="Content_txtEspecialidadEnviado" class="active">
                                    <strong>Especialidad</strong>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" style="padding: 8px;">
                    <h5>
                        <strong>Datos Paciente</strong>
                    </h5>
                </div>
                <div class="card-body"  style="padding-bottom: 0px; padding-top:0px;">
                    
                    <div class="row">
                        <div class="col-md-3">
                            <div class="md-form">
                                <asp:TextBox ID="txtNumeroDocumentoPaciente" runat="server" CssClass="form-control"
                                    Text="18327192-k">
                                </asp:TextBox>
                                <label for="Content_txtNumeroDocumentoPaciente">
                                    <strong>
                                        <asp:Label id="lblNumeroDocumento" runat="server" Text="RUT"></asp:Label>
                                    </strong> 
                                </label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="md-form">
                                <asp:TextBox ID="txtNombrePaciente" runat="server" CssClass="form-control"
                                    Text="EXEQUIEL ARMANDO">
                                </asp:TextBox>
                                <label for="Content_txtNombresPaciente" class="active">
                                    <strong>Nombre Completo</strong>
                                </label>
                            </div> 
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <div class="md-form">
                                <asp:TextBox ID="txtSexo" runat="server" CssClass="form-control"
                                    Text="Masculino">
                                </asp:TextBox>
                                <label for="Content_txtSexo" class="active">
                                    <strong>Sexo</strong>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="md-form">
                                <asp:TextBox ID="txtFechaNacimiento" runat="server" CssClass="form-control"
                                    Text="26-01-1993">
                                </asp:TextBox>
                                <label for="Content_txtFechaNacimiento" class="active">
                                    <strong>Fecha de Nacimiento</strong>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="md-form">
                                <asp:TextBox ID="txtEdad" runat="server" CssClass="form-control"
                                    Text="25 años ">
                                </asp:TextBox>
                                <label for="Content_txtEdad" class="active">
                                    <strong>Edad</strong>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form">
                                <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control"
                                    Text="Pasaje German Carcamo Carrasco #01122">
                                </asp:TextBox>
                                <label for="Content_txtDireccion" class="active">
                                    <strong>Dirección</strong>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="md-form">
                                <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control"
                                    Text="957647526">
                                </asp:TextBox>
                                <label for="Content_txtTelefono" class="active">
                                    <strong>Teléfono</strong> 
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <asp:TextBox ID="txtOtroTelefono" runat="server" CssClass="form-control"
                                    Text="994947057">
                                </asp:TextBox>
                                <label for="Content_txtOtroTelefono" class="active">
                                    <strong>Otro teléfono</strong>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" style="padding: 8px;">
                    <div class="row">
                        <div class="col-md-3">
                            <h5><strong>Derivado a</strong></h5>
                        </div>
                        <div class="col-md-9">
                            <h5 class="text-right">
                                <asp:Label ID="lblServicioSaludEstablecimientoDerivado" runat="server" 
                                    Text="Servicio de Salud Magallanes">
                                </asp:Label>
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="padding-bottom: 0px; padding-top:0px;">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form">
                                <asp:TextBox ID="txtUbicacionDerivado" runat="server" CssClass="form-control"
                                    Text="CAE ADULTO">
                                </asp:TextBox>
                                <label for="Content_txtServicioDerivado" class="active">
                                    <strong>Servicio</strong>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form">
                                <asp:TextBox ID="txtEspecialidadDerivado" runat="server" CssClass="form-control"
                                    Text="CIRUGÍA GENERAL">
                                </asp:TextBox>
                                <label for="Content_txtEspecialidadDerivado" class="active">
                                    <strong>Especialidad</strong>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card" style="box-shadow: none;">
                <div class="card-header" style="padding: 8px;">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>
                                <strong>Datos Clínicos</strong>
                            </h5>
                        </div>
                        <div class="col-md-6 text-right">
                            <strong style="margin-right: 5px; padding-top:5px;">¿Sospecha de la salud AUGE?</strong>
                            <asp:Label ID="lblAugeSi" runat="server" CssClass="badge badge-pill" Font-Bold="true" 
                                Text="SI">
                            </asp:Label>
                            <asp:Label ID="lblAugeNo" runat="server" Font-Bold="true" CssClass="badge badge-pill" 
                                Text="NO">
                            </asp:Label>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="padding-bottom: 0px; padding-top:0px;">

                    <asp:Panel ID="pnlAuge1" runat="server">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="md-form">
                                    <asp:TextBox ID="txtTipoInterconsulta" runat="server" CssClass="form-control"
                                        Text="OTRO">
                                    </asp:TextBox>
                                    <label for="Content_txtTipoInterconsulta" class="active">
                                        <strong>Se deriva interconsulta para:</strong>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="md-form">
                                    <asp:TextBox ID="txtGrupoGes" runat="server" CssClass="form-control"
                                        Text="Grupo GES" placeholder="&nbsp;">
                                    </asp:TextBox>
                                    <label for="Content_txtGrupoGes" class="active">
                                       <strong>Grupo Patología GES</strong>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            
                            <asp:Panel ID="pnlOtros" runat="server" CssClass="col-md-6">
                                <div class="md-form">
                                    <asp:TextBox ID="txtOtros" runat="server" CssClass="form-control"
                                        Text="MUCHOS DETALLES">
                                    </asp:TextBox>
                                    <label for="Content_txtOtros" class="active">
                                        <strong>Detalles</strong>
                                    </label>
                                </div>
                            </asp:Panel>
                            
                            <asp:Panel ID="pnlSubGrupoGes" runat="server" CssClass="col-md-6">
                                <div class="md-form">
                                    <asp:TextBox ID="txtSubgrupoGes" runat="server" CssClass="form-control"
                                        Text="Sub-Grupo GES">
                                    </asp:TextBox>
                                    <label for="Content_txtSubgrupoGes" class="active">
                                       <strong>Sub-Grupo Patología GES</strong>
                                    </label>
                                </div>
                            </asp:Panel>

                        </div>

                    </asp:Panel>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="md-form">
                                <asp:TextBox ID="txtDiagnostico" runat="server" CssClass="md-textarea md-textarea-auto form-control" 
                                    Rows="2" TextMode="MultiLine" Text="COLE">
                                </asp:TextBox>
                                <label for="Content_txtDiagnostico" class="active">
                                    <strong>Diagnóstico Clínico</strong>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="md-form">
                                <asp:TextBox ID="txtSintomalogia" runat="server" CssClass="md-textarea md-textarea-auto form-control"
                                    Rows="2" TextMode="MultiLine" Text="COLE">
                                </asp:TextBox>
                                <label for="Content_txtSintomalogia" class="active">
                                    <strong>Principal Sintomatología</strong>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="md-form">
                                <asp:TextBox ID="txtSolicitante" runat="server" CssClass="md-textarea md-textarea-auto form-control"
                                    Rows="2" TextMode="MultiLine" Text="COLE">
                                </asp:TextBox>
                                <label for="Content_txtSolicitante" class="active">
                                    <strong>Se Solicita</strong>
                                </label>
                            </div>
                        </div>
                    </div>

                    <asp:Panel ID="pnlAuge2" runat="server" class="row"> 
                        <div class="col-md-6">
                            <div class="md-form">
                                <asp:TextBox ID="txtFundamentosDiagnosticoGes" runat="server" CssClass="md-textarea md-textarea-auto form-control"
                                    Rows="2" Text="FUNDAMENTOS DIAGNOSTICOS GES" placeholder="&nbsp;" TextMode="MultiLine">
                                </asp:TextBox>
                                <label for="Content_txtFundamentosDiagnosticoGes" class="active">
                                    <strong>Fundamentos del Diagnóstico</strong>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form">
                                <asp:TextBox ID="txttratamientosGes" runat="server" CssClass="md-textarea md-textarea-auto form-control"
                                    Rows="2" Text="TRATAMIENTOS DIAGNOSTICOS GES" placeholder="&nbsp;" TextMode="MultiLine">
                                </asp:TextBox>
                                <label for="Content_txttratamientosGes" class="active">
                                    <strong>Tratamiento</strong>
                                </label>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            
            <div class="text-center" style="margin-top: 50px;">
                <strong style="border-top: 2px solid black; padding: 0px 50px 0px 50px;">
                    FIRMA: <asp:Label ID="lblProfesional" runat="server"></asp:Label>
                </strong>
            </div>

        </div>
    </div>
</asp:Content>
