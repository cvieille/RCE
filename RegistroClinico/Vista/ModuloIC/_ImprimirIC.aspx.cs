﻿using Newtonsoft.Json;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RegistroClinico.Vista.ModuloIC
{
    public partial class _ImprimirIC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //Funciones.ValidaSesion(this);

            if (!IsPostBack && Session["ID_INTERCONSULTA"] != null)
            {

                int idIC = int.Parse(Session["ID_INTERCONSULTA"].ToString());
                NegRCE_Interconsulta negHE = new NegRCE_Interconsulta();

                List<Dictionary<string, object>> listJson =
                    (List<Dictionary<string, object>>)negHE.ObtenerJsonInterconsulta(idIC);

                //List<Dictionary<string, object>> listEnviado =
                //    JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(listJson[0]["GEN_UbicacionSolicitante"].ToString());

                //List<Dictionary<string, object>> listDerivado =
                //    JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(listJson[0]["GEN_UbicacionDestino"].ToString());

                List<Dictionary<string, object>> listProfesional =
                    JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(listJson[0]["GEN_nombreProfesionalInterconsulta"].ToString());

                //SERVICIO ENVIO
                lblServicioSaludEstablecimientoEnviado.Text = Convert.ToString(listJson[0]["GEN_nombreEstablecimientoOrigen"]);
                txtUbicacionEnviado.Text = Convert.ToString(listJson[0]["GEN_Servicio_SaludOrigen"]);
                txtEspecialidadEnviado.Text = Convert.ToString(listJson[0]["GEN_nombreEspecialidadOrigen"]);

                // PACIENTE
                txtNumeroDocumentoPaciente.Text = Convert.ToString(listJson[0]["GEN_numero_documentoPaciente"]);
                txtNombrePaciente.Text = Convert.ToString(listJson[0]["GEN_nombrePaciente"]);
                txtSexo.Text = Convert.ToString(listJson[0]["GEN_nomSexo"]);
                txtFechaNacimiento.Text = DateTime.Parse(listJson[0]["GEN_fec_nacimientoPaciente"].ToString()).ToString("dd-MM-yyyy");
                txtEdad.Text = new NegGEN_Paciente().ObtenerEdadPaciente(DateTime.Parse(listJson[0]["GEN_fec_nacimientoPaciente"].ToString()));
                txtDireccion.Text = Convert.ToString(listJson[0]["GEN_dir_callePaciente"]);
                txtTelefono.Text = Convert.ToString(listJson[0]["GEN_telefonoPaciente"]);
                txtOtroTelefono.Text = Convert.ToString(listJson[0]["GEN_otros_fonosPaciente"]);

                //SERVICIO DERIVADO
                lblServicioSaludEstablecimientoDerivado.Text = Convert.ToString(listJson[0]["GEN_nombreEstablecimientodestino"]);
                txtUbicacionDerivado.Text = Convert.ToString(listJson[0]["GEN_Servicio_SaludDestino"]);
                txtEspecialidadDerivado.Text = Convert.ToString(listJson[0]["GEN_nombreEspecialidadDestino"]);

                //DATOS CLÍNICOS
                txtDiagnostico.Text = Convert.ToString(listJson[0]["RCE_diagnosticoInterconsulta"]);
                txtSintomalogia.Text = Convert.ToString(listJson[0]["RCE_sintomatologiaInterconsulta"]);
                txtSolicitante.Text = Convert.ToString(listJson[0]["RCE_solicitudInterconsulta"]);

                if (listJson[0]["RCE_idInterconsulta_GES"] != null)
                {

                    List<Dictionary<string, object>> listDescripcionPatologiaGes =
                        JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(listJson[0]["RCE_descripcion_Patologia_GES"].ToString());

                    List<string> listPatologiaPadreGes =
                        JsonConvert.DeserializeObject<List<string>>(listDescripcionPatologiaGes[0]["RCE_descripcion_Patologia_GESPadre"].ToString());

                    string grupoGes = (listPatologiaPadreGes.Count > 0) ? listPatologiaPadreGes[0] :
                        listDescripcionPatologiaGes[0]["RCE_descripcion_Patologia_GESFinal"].ToString();

                    string subGrupoGes = (listPatologiaPadreGes.Count > 0) ?
                        listDescripcionPatologiaGes[0]["RCE_descripcion_Patologia_GESFinal"].ToString() : "";

                    //DATOS CLÍNICOS GES
                    lblAugeSi.CssClass += " badge-primary";
                    lblAugeNo.CssClass += " badge-light";
                    pnlAuge1.Visible = pnlAuge2.Visible = true;

                    txtTipoInterconsulta.Text = Convert.ToString(listJson[0]["RCE_DescripcionTipo_Interconsulta"]);
                    txtOtros.Text = Convert.ToString(listJson[0]["RCE_otro_tipoInterconsulta_GES"]);
                    pnlOtros.Visible = (txtOtros.Text != "");
                    txtGrupoGes.Text = grupoGes;
                    txtSubgrupoGes.Text = subGrupoGes;
                    pnlSubGrupoGes.Visible = (txtSubgrupoGes.Text != "");
                    txtFundamentosDiagnosticoGes.Text = Convert.ToString(listJson[0]["RCE_fundamentos_diagnosticoInterconsulta_GES"]);
                    txttratamientosGes.Text = Convert.ToString(listJson[0]["RCE_tratamientoInterconsulta_GES"]);

                }
                else
                {
                    lblAugeNo.CssClass += " badge-primary";
                    lblAugeSi.CssClass += " badge-light";
                    pnlAuge1.Visible = pnlAuge2.Visible = false;
                }

                //DATOS PROFESIONAL
                lblProfesional.Text = Convert.ToString(listProfesional[0]["GEN_nombrePersonasProfesional"]) +
                    " " + Convert.ToString(listProfesional[0]["GEN_numero_documentoPersonasProfesional"]);

                Session.Remove("ID_INTERCONSULTA");

                string sName = string.Join(" ", new string[] { "INTERCONSULTA", DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") });
                string sUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                Funciones.ExportarPDF(sUrl, this.Page, sName);

            }

        }
    }
}