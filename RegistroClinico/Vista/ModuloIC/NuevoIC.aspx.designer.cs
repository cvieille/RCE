﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace RegistroClinico.Vista.ModuloIC
{


    public partial class NuevoIC
    {

        /// <summary>
        /// Control wucMdlAlerta.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalAlerta wucMdlAlerta;

        /// <summary>
        /// Control wucMdlEventos.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalEvento wucMdlEventos;

        /// <summary>
        /// Control wucMdlPacienteEvento.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalPacienteEvento wucMdlPacienteEvento;

        /// <summary>
        /// Control lblFechaIC.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblFechaIC;

        /// <summary>
        /// Control wucPaciente.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.Paciente wucPaciente;

        /// <summary>
        /// Control wucCargadorArchivos.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.CargadoDeArchivos wucCargadorArchivos;
    }
}
