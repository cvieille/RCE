﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImprimirIC.aspx.cs" Inherits="RegistroClinico.Vista.ImprimirIC" MasterPageFile="~/Vista/MasterPages/Impresion.Master"%>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <style>
        .tabla-custom{
            table-layout: fixed;
            width: 100%;
        }

        .base td{
            width: 8.333%;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">

     <div class="panel">
        <div class="panel-heading">
            <div class="row">
                <asp:Label ID="prueba" runat="server" Text=""></asp:Label>
                <div class="col-xs-2">
                    <br/>
                </div>
                <div class="col-xs-8">
                    <h4 class="text-center"><strong> SOLICITUD DE INTERCONSULTA O DERIVACION</strong></h4>
                </div>
                <div class="col-xs-2">
                    <h4 class="text-right"><strong>N° Interconsulta:
                        <asp:Label ID="idInterconsulta" runat="server"></asp:Label></strong></h4>
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding: 0px;">

            <div class="estilo-titulo">
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px !important; padding-right: 0px;">
                        Datos de Origen
                    </div>
                    <div class="col-xs-5 text-right" style="padding-left: 0px !important; padding-right: 0px;">
                        <strong>Fecha de llegada:
                            <asp:Label ID="lblFechaLlegada" runat="server"></asp:Label></strong>
                    </div>
                </div>
            </div>

            <table id="tblIdentificaServicio" style="width: 100%; border: 1px solid black;">


                <tr>
                    <td class="fondo-titulos contenido-celda">Servicio de Salud</td>
                    <td class="contenido-celda">
                        <asp:Label ID="txtServicio" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Establecimiento</td>
                    <td class="contenido-celda">
                        <asp:Label ID="txtEstablecimiento" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Especialidad</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="txtEspecialidad" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>

            <div class="estilo-titulo">
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px !important; padding-right: 0px;">
                        Datos del Paciente
                    </div>
                </div>
            </div>

            <table id="tblIdentificaPaciente" style="width: 100%; border: 1px solid black;">


                <tr>
                    <td class="fondo-titulos contenido-celda">RUT</td>
                    <td class="contenido-celda">
                        <asp:Label ID="rutPaciente" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Nombre Completo</td>
                    <td class="contenido-celda">
                        <asp:Label ID="nombrePaciente" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Sexo</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="sexoPaciente" runat="server">
                        </asp:Label>
                    </td>
                </tr>

                <tr>
                    <td class="fondo-titulos contenido-celda">Fecha de nacimiento</td>
                    <td class="contenido-celda">
                        <asp:Label ID="fechaNacimiento" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Edad</td>
                    <td class="contenido-celda">
                        <asp:Label ID="edadPaciente" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Dirección</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="direccionPaciente" runat="server">
                        </asp:Label>
                    </td>
                </tr>

                <tr>
                    <td class="fondo-titulos contenido-celda">Teléfono</td>
                    <td class="contenido-celda">
                        <asp:Label ID="telefonoPaciente" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Otro Teléfono</td>
                    <td class="contenido-celda">
                        <asp:Label ID="otroTelefonoPaciente" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>

            

            <div class="estilo-titulo">
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px !important; padding-right: 0px;">
                        Datos DE DESTINO
                    </div>
                </div>
            </div>

            <table id="tblIdentificaDatosClinicos" style="width: 100%; border: 1px solid black;">


                <tr>
                    <td class="fondo-titulos contenido-celda">Servicio de Salud</td>
                    <td class="contenido-celda">
                        <asp:Label ID="txtServicioDestino" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Establecimiento</td>
                    <td class="contenido-celda">
                        <asp:Label ID="txtEstablecimientoDestino" runat="server">
                        </asp:Label>
                    </td>
                    <td class="fondo-titulos contenido-celda">Especialidad</td>
                    <td class="contenido-celda" colspan="2">
                        <asp:Label ID="txtEspecialidadDestino" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>


            <div class="estilo-titulo">Datos GES</div>
            <asp:Panel ID="noDatosGES" runat="server" Style="border: 1px solid black;">
                    <p class="text-center" style="padding: 15px 15px 15px 15px !important;">
                        <i class="fa fa-exclamation-triangle"></i>
                        Paciente no tiene Datos GES
                    </p>
            </asp:Panel>   

            <asp:Panel ID="pnlGES" runat="server">
                <table style="border: 1px solid black; width: 100%;">
                    <tr>
                        <td class="fondo-titulos contenido-celda">Derivado para</td>
                        <td class="contenido-celda">
                            <asp:Label ID="txtDerivado" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda">Grupo Patología GES</td>
                        <td class="contenido-celda">
                            <asp:Label ID="txtGrupoPatologiaPadre" runat="server">
                            </asp:Label>
                        </td>
                        <td class="fondo-titulos contenido-celda">SubGrupo Patología GES</td>
                        <td class="contenido-celda">
                            <asp:Label ID="txtGrupoPatologiaHijo" runat="server">
                            </asp:Label>
                        </td>
                    </tr>

                </table>
                
               </asp:Panel>
               <asp:Panel ID="GES1" runat="server" Style="border: 1px solid black;">
                    <p class="text-center" style="padding: 15px 15px 15px 15px !important;">
                        <i class="fa fa-exclamation-triangle"></i>
                        Fundamentos del diagnóstico:
                        <asp:Label ID="FundamentoGES" runat="server">
                            </asp:Label>
                    </p>
                </asp:Panel>            
                <asp:Panel ID="GES2" runat="server" Style="border: 1px solid black;">
                    <p class="text-center" style="padding: 15px 15px 15px 15px !important;">
                        <i class="fa fa-exclamation-triangle"></i>
                        Tratamiento e indicaciones:
                        <asp:Label ID="TratamientoGES" runat="server">
                            </asp:Label>
                    </p>
                </asp:Panel>

            <div class="estilo-titulo">
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px !important; padding-right: 0px;">
                        Datos Clínicos
                    </div>
                </div>
            </div>
                <asp:Panel ID="Panel3" runat="server" Style="border: 1px solid black;">
                    <p class="text-center" style="padding: 15px 15px 15px 15px !important;">
                        <i class="fa fa-exclamation-triangle"></i>
                       Diagnóstico Clínico: 
                        <asp:Label ID="diagnosticoClinico" runat="server">
                            </asp:Label>
                    </p>
                </asp:Panel>            
                <asp:Panel ID="Panel4" runat="server" Style="border: 1px solid black;">
                    <p class="text-center" style="padding: 15px 15px 15px 15px !important;">
                        <i class="fa fa-exclamation-triangle"></i>
                        Principal Sintomatología:
                        <asp:Label ID="principalSintomatologia" runat="server">
                            </asp:Label>
                    </p>
                </asp:Panel>
                <asp:Panel ID="Panel5" runat="server" Style="border: 1px solid black;">
                    <p class="text-center" style="padding: 15px 15px 15px 15px !important;">
                        <i class="fa fa-exclamation-triangle"></i>
                        Se Solicita
                           <asp:Label ID="solicitaInterconsulta" runat="server">
                               </asp:Label>

                    </p>
                </asp:Panel>

    </div>
</asp:Content>