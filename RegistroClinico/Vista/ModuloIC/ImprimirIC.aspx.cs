﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RegistroClinico.Vista
{
    public partial class ImprimirIC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Valida sesión según los permisos que tenga el perfil
            NegSession negSession = new NegSession(Page);
            negSession.ValidarPermisosSession();
            string idDAU = Request.QueryString["id"]; 
            string usuarioLogeado = Session["LOGIN_USUARIO"].ToString();
            System.Diagnostics.Debug.WriteLine("PRUEBA1");
            //Ocultar Paneles
            pnlGES.Visible = false;
            GES1.Visible = false;
            GES2.Visible = false;
            
            try
            {
                HttpContext context = HttpContext.Current;
                string dominio = Funciones.GetUrlWebApi();

                idDAU = Request.QueryString["id"];
                usuarioLogeado = Session["LOGIN_USUARIO"].ToString();
                string url = string.Format("{0}RCE_Interconsulta/Imprimir/{1}", dominio, idDAU);
                string token = string.Format("Bearer {0}", Session["TOKEN"]/*Page.Request.Cookies["DATA-TOKEN"].Value*/);
                string sJson = Funciones.ObtenerJsonWebApi(url, token);

                DatosInterconsulta datosInterconsulta = JsonConvert.DeserializeObject<DatosInterconsulta>(sJson);
                idDAU = Request.QueryString["id"];


                DibujaDatos(datosInterconsulta);
            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al imprimir IPD", ex.Message + ex.StackTrace + ex.InnerException);
            }
            
            Funciones funciones = new Funciones();
            string doc = "Hoja de Enfermería";
            funciones.ExportarPDFWK(this.Page, doc, idDAU, usuarioLogeado);
        }
        
        public void DibujaDatos(DatosInterconsulta datos)
        {

            Funciones fun = new Funciones();
            var edad = Funciones.CalculaEdadenAños(datos.GEN_fec_nacimientoPaciente.ToString());
            var patologiaGES = datos.RCE_descripcion_Patologia_GES;
            idInterconsulta.Text = datos.RCE_idInterconsulta.ToString();
            //Establecimiento
            txtServicio.Text = datos.GEN_Servicio_SaludOrigen; 
            txtEstablecimiento.Text = datos.GEN_nombreEstablecimientoOrigen;
            txtEspecialidad.Text = datos.GEN_nombreEspecialidadOrigen;

            //Paciente
            rutPaciente.Text = datos.GEN_numero_documentoPaciente;
            nombrePaciente.Text = datos.GEN_nombrePaciente;
            sexoPaciente.Text = datos.GEN_nomSexo;
            fechaNacimiento.Text = datos.GEN_fec_nacimientoPaciente.ToString();
            edadPaciente.Text = edad.ToString(); 
            direccionPaciente.Text = datos.GEN_dir_callePaciente ;
            telefonoPaciente.Text = datos.GEN_telefonoPaciente;
            otroTelefonoPaciente.Text = datos.GEN_otros_fonosPaciente;

            //Datos Destino
            txtServicioDestino.Text = datos.GEN_Servicio_SaludDestino;
            txtEstablecimientoDestino.Text = datos.GEN_nombreEstablecimientodestino;
            txtEspecialidadDestino.Text = datos.GEN_nombreEspecialidadDestino;

            if(datos.RCE_DescripcionTipo_Interconsulta != null) {
                pnlGES.Visible = true;
                GES1.Visible = true;
                GES2.Visible = true;
                noDatosGES.Visible = false;

            txtDerivado.Text = datos.RCE_DescripcionTipo_Interconsulta;
                if (patologiaGES.RCE_descripcion_Patologia_GESPadre.Count() >= 1) {
                    txtGrupoPatologiaPadre.Text = patologiaGES.RCE_descripcion_Patologia_GESPadre[0].ToString();
                    txtGrupoPatologiaHijo.Text = patologiaGES.RCE_descripcion_Patologia_GESFinal.ToString();
                }
                else {
                    txtGrupoPatologiaPadre.Text = patologiaGES.RCE_descripcion_Patologia_GESFinal.ToString();
                }

                FundamentoGES.Text = datos.RCE_fundamentos_diagnosticoInterconsulta_GES;
                TratamientoGES.Text = datos.RCE_tratamientoInterconsulta_GES;

            }

            //Datos Clinicos
            diagnosticoClinico.Text = datos.RCE_diagnosticoInterconsulta;
            principalSintomatologia.Text = datos.RCE_sintomatologiaInterconsulta;
            solicitaInterconsulta.Text = datos.RCE_solicitudInterconsulta;
        }
        
    }


    public class DatosIndex
    {
        public DatosInterconsulta prueba { get; set; }
    }
    public class DatosInterconsulta
    {
        public int RCE_idInterconsulta { get; set; }
        public string GEN_nombreIdentificacion { get; set; }
        public string GEN_numero_documentoPaciente { get; set; }
        public string GEN_nombrePaciente { get; set; }
        public string GEN_dir_callePaciente { get; set; }
        public string GEN_dir_numeroPaciente { get; set; }
        public string GEN_telefonoPaciente { get; set; }
        public string GEN_otros_fonosPaciente { get; set; }
        public string GEN_emailPaciente { get; set; }
        public DateTime GEN_fec_nacimientoPaciente { get; set; }
        public string GEN_nomSexo { get; set; }
        public DateTime RCE_fechaInterconsulta { get; set; }
        public Nullable<int> RCE_idInterconsulta_GES { get; set; }
        public string RCE_diagnosticoInterconsulta { get; set; }
        public string RCE_sintomatologiaInterconsulta { get; set; }
        public string RCE_solicitudInterconsulta { get; set; }
        public string RCE_fundamentos_diagnosticoInterconsulta_GES { get; set; }
        public string RCE_tratamientoInterconsulta_GES { get; set; }
        public string RCE_otros_tipoInterconsulta_GES { get; set; }
        public string GEN_nombreEstablecimientoOrigen { get; set; }
        public int GEN_idEspecialidad_origenInterconsulta { get; set; }
        public string GEN_nombreEstablecimientodestino { get; set; }
        public string GEN_Servicio_SaludDestino { get; set; }
        public string GEN_Servicio_SaludOrigen { get; set; }
        public int GEN_idEspecialidad_destinoInterconsulta { get; set; }
        public string GEN_nombreEspecialidadOrigen { get; set; }
        public string GEN_nombreEspecialidadDestino { get; set; }
        public string RCE_DescripcionTipo_Interconsulta { get; set; }

        public RCEDescripcionPatologiaGES RCE_descripcion_Patologia_GES { get; set; }
        public string RCE_otro_tipoInterconsulta_GES { get; set; }
        public string GEN_nombrePersonasProfesional { get; set; }
        public string GEN_numero_documentoPersonasProfesional { get; set; }
        
    }

    public class RCEDescripcionPatologiaGES
    {
        public string RCE_descripcion_Patologia_GESFinal { get; set; }
        public List<string> RCE_descripcion_Patologia_GESPadre { get; set; }

    }

}