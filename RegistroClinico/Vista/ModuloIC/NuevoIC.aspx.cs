﻿using System;
using System.Globalization;

namespace RegistroClinico.Vista.ModuloIC
{
    public partial class NuevoIC : Handlers.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack) {
                
                CultureInfo myCIintl = new CultureInfo("es-ES", false);

                DateTime dt = DateTime.Now;
                lblFechaIC.Text = dt.ToString("dddd dd 'de' MMMM 'del' yyyy", myCIintl).ToUpper();

            }

        }
        
    }
}