﻿
class menuLateral extends HTMLElement {
    constructor() {
        super();
    }
    connectedCallback() {

        ////////////////////////////////////////
        ///Dibuja Boton de inicio del Menu//////
        ////////////////////////////////////////
        let iconoHome = `
                        <div class='modulo'>
                            <a href="${ObtenerHost()}/Vista/inicio.aspx">
                                <i class="nav-icon fa fa-home"></i>
                                <p class='item'>Inicio</p>
                            </a>
                        </div>`;
        var menuPrincipal = iconoHome;
        //////////////////////////////////////////
        /////Dibuja Menu desde WEBAPI/////////////
        //////////////////////////////////////////

        if (GetToken() != null) {

            $.ajax({
                headers: { 'Authorization': GetToken() },
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Menu/PorPerfil`,
                async: false,
                success: function (data) {
                    let modulos = '';
                    $.each(data, function (i, nivel1) {
                        ////////////////////////////////////////////
                        ///Dibuja la estructura general del menu////
                        ////////////////////////////////////////////
                        modulos += `
                        <div>
                            <a href="${nivel1.GEN_contenidoMenu ?? "#"}"  >
                                <div id="${nivel1.GEN_idMenu}"
                                    class='modulo  ${nivel1.GEN_MenuHijos.length > 0 ? "hijos" : ""}'>
                                <i class="${nivel1.GEN_iconoMenu}"></i> 
                                <p class="item">${nivel1.GEN_tituloMenu}</p>
                                    ${nivel1.GEN_MenuHijos.length > 0 ?
                                    "<i id='i-" + nivel1.GEN_idMenu + "' style='float: right; margin-right:10px;'class='right fa fa-angle-left'></i>" : ''} 
                                </div>
                            </a>`;
                        ////////////////////////////////////////////
                        ///verifica si tiene hijos//////////////////
                        ////////////////////////////////////////////
                        if (nivel1.GEN_MenuHijos.length > 0) {
                            modulos += `<div style="padding:0px;">`;
                            modulos += `<ul id="${nivel1.GEN_idMenu}-s" class="submodulos">`;
                            $.each(nivel1.GEN_MenuHijos, function (i, nivel2) {
                                modulos += `
                                    <a href="${ObtenerHost() +  nivel2.GEN_contenidoMenuHijo ?? "#"}"  >
                                        <li id="${nivel2.GEN_idMenuHijo}" >
                                            <i class="${nivel2.GEN_iconoMenuHijo}"></i> 
                                            <p class="item">${nivel2.GEN_tituloMenuHijo}</p>
                                        </li>
                                    </a>`;
                            });
                            modulos += `</ul>`;
                        }

                        modulos += `</div></div> `;
                    });

                    menuPrincipal += modulos;
                }
            });

            this.innerHTML = `<style>
              /* CSS Local */         
            .modulo{
                border-top: 1px solid #4f5962;
            }
            .modulo:hover {
                background-color: grey;            
            }         
            .submodulos a:hover {
                color: White;            
            } 
            .modulo i{
                color: #C2C7D0  ;
                margin-top: 10px;
                margin-bottom: 10px;
                margin-left: 10px;
            }
            .item{
                display: inline-block;
            }
            .submodulos{
                padding-top: 10px;
                padding-left: 25px;
                background: rgba(255, 255, 255, 0.05);
                display:none;
            }
            .submodulos li{  
                list-style:none;
            }
        
            .menu-activo {
                background: #007bff;
                font-weight: bold;
                color:white;
                //border-radius: 5px;
            }
            </style > ` + menuPrincipal;
        }

    }
}
window.onload = function () {
    const menu = document.getElementsByClassName('hijos');
    for (var i = 0; i < menu.length; i++) {
        menu[i].addEventListener("click", function (e) {
            let divSubMenu = document.getElementById(this.id + "-s");
            if (divSubMenu.style.display != 'block') {
                divSubMenu.style.display = 'block';
                document.getElementById("i-"+this.id).style.transform = "rotate(-90deg)";
                this.classList.add("menu-activo");
            }
            else {
                divSubMenu.style.display = 'none';
                document.getElementById("i-" + this.id).style.transform = "rotate(360deg)";
                this.classList.remove("menu-activo");
            }

            divSubMenu.style.transition = "width 0.5ms ease-out, height 0.5ms ease-out;";
            e.preventDefault();
        });
    }
}

window.customElements.define("menu-lateral", menuLateral)